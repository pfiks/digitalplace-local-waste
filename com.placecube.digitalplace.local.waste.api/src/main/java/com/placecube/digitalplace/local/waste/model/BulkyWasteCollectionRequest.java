package com.placecube.digitalplace.local.waste.model;

public class BulkyWasteCollectionRequest extends WasteRequest {

	private String slotId;
	private String date;
	private String firstName;
	private String lastName;
	private String[] itemsForCollection;
	private String[] quantities;
	private String uprn;

	public BulkyWasteCollectionRequest() {
	}

	public String getSlotId() {
		return slotId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}

	public String[] getItemsForCollection() {
		return itemsForCollection;
	}

	public void setItemsForCollection(String[] itemsForCollection) {
		this.itemsForCollection = itemsForCollection;
	}

	public String[] getQuantities() {
		return quantities;
	}

	public void setQuantities(String[] quantities) {
		this.quantities = quantities;
	}

	@Override
	public String getUprn() {
		return uprn;
	}

	@Override
	public void setUprn(String uprn) {
		this.uprn = uprn;
	}

}
