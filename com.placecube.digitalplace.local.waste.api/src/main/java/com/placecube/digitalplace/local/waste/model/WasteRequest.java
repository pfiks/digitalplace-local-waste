package com.placecube.digitalplace.local.waste.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WasteRequest {

	public WasteRequest() {
	}

	private long classNameId;
	private String classPK;
	private String uprn;

	public long getClassNameId() {
		return classNameId;
	}

	public void setClassNameId(long classNameId) {
		this.classNameId = classNameId;
	}

	public String getClassPK() {
		return classPK;
	}

	public void setClassPK(String classPK) {
		this.classPK = classPK;
	}

	public String getUprn() {
		return uprn;
	}

	public void setUprn(String uprn) {
		this.uprn = uprn;
	}

}
