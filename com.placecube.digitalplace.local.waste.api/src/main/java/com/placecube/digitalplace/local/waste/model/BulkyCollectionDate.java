package com.placecube.digitalplace.local.waste.model;

public class BulkyCollectionDate {

	private final String dayNameOfWeek;

	private final int dayOfWeek;

	public BulkyCollectionDate(int dayOfWeek, String dayNameOfWeek) {
		this.dayOfWeek = dayOfWeek;
		this.dayNameOfWeek = dayNameOfWeek;
	}

	public String getDayNameOfWeek() {
		return dayNameOfWeek;
	}

	public int getDayOfWeek() {
		return dayOfWeek;
	}
}
