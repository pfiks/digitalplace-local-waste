package com.placecube.digitalplace.local.waste.model;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlRootElement;

import com.placecube.digitalplace.local.waste.constants.SubscriptionType;

@XmlRootElement
public class WasteSubscriptionResponse {

	private String emailAddress;
	private LocalDate endDate;
	private String firstName;
	private String lastName;
	private LocalDate startDate;
	private final String subscriptionRef;
	private final SubscriptionType subscriptionType;
	private String telephone;
	private String uprn;

	public WasteSubscriptionResponse(String subscriptionRef, SubscriptionType subscriptionType) {
		this.subscriptionRef = subscriptionRef;
		this.subscriptionType = subscriptionType;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public String getSubscriptionRef() {
		return subscriptionRef;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getUprn() {
		return uprn;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setUprn(String uprn) {
		this.uprn = uprn;
	}

}
