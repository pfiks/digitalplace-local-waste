package com.placecube.digitalplace.local.waste.constants;

public class WasteConstants {

	public static final int BULKY_DFLT_NUMBER_OF_DATES_TO_RETURN = 10;

	private WasteConstants() {

	}
}
