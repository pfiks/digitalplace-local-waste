package com.placecube.digitalplace.local.waste.model;

public class Bin {

	private final String binID;

	private final String binMissed;

	private final String binMissedDate;

	private final String binNumber;

	private final String collect;

	private final String completedDate;

	private final String deliver;

	private final String endDate;

	private final String paymentRef;

	private final String reportedDate;

	private final String startDate;

	private final String uprn;

	public Bin(String binID, String uprn, String binNumber, String startDate, String endDate, String reportedDate, String completedDate, String deliver, String collect, String paymentRef,
			String binMissed, String binMissedDate) {
		this.binID = binID;
		this.uprn = uprn;
		this.binNumber = binNumber;
		this.startDate = startDate;
		this.endDate = endDate;
		this.reportedDate = reportedDate;
		this.completedDate = completedDate;
		this.deliver = deliver;
		this.collect = collect;
		this.paymentRef = paymentRef;
		this.binMissed = binMissed;
		this.binMissedDate = binMissedDate;
	}

	public String getBinID() {
		return binID;
	}

	public String getBinMissed() {
		return binMissed;
	}

	public String getBinMissedDate() {
		return binMissedDate;
	}

	public String getBinNumber() {
		return binNumber;
	}

	public String getCollect() {
		return collect;
	}

	public String getCompletedDate() {
		return completedDate;
	}

	public String getDeliver() {
		return deliver;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getPaymentRef() {
		return paymentRef;
	}

	public String getReportedDate() {
		return reportedDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getUprn() {
		return uprn;
	}
	
}
