package com.placecube.digitalplace.local.waste.overrideaction;

import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;

public interface WasteServiceOverrideAction {

	String SERVICE_EXECUTION_PRIORITY = "waste.service.override.action.serviceExecutionPriority";

	String SERVICE_ID = "waste.service.override.action.serviceId";

	default WasteSubscriptionResponse executeOnAfterWasteSubscriptionResponseCreation(long companyId, WasteSubscriptionResponse wasteSubscriptionResponse) {
		return wasteSubscriptionResponse;
	}

}
