package com.placecube.digitalplace.local.waste.exception;

import com.liferay.portal.kernel.exception.PortalException;

public class WasteRetrievalException extends PortalException {

	private static final long serialVersionUID = 1L;

	public WasteRetrievalException() {
		super();
	}

	public WasteRetrievalException(String msg) {
		super(msg);
	}

	public WasteRetrievalException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public WasteRetrievalException(Throwable cause) {
		super(cause);
	}
}