package com.placecube.digitalplace.local.waste.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;

public interface WasteConnector {

	String createAssistedBinCollectionJob(long companyId, AssistedBinCollectionJob assistedBinCollectionJob) throws WasteRetrievalException;

	WasteSubscriptionResponse createBinSubscription(long companyId, String uprn, String serviceType, String firstName, String lastName, String emailAddress, String telephone, int numberOfBins)
			throws WasteRetrievalException;

	String createBulkyWasteCollectionJob(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, List<String> itemsToBeCollected, String locationOfItems,
			Date collectionDate) throws WasteRetrievalException;

	String createBulkyWasteCollectionJob(long companyId, BulkyWasteCollectionRequest bulkyWasteCollectionRequest, Date date) throws WasteRetrievalException;

	String createFlyTippingReport(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, String typeOfRubbish, String sizeOfRubbish, String details,
			String coordinates, String locationDetails, String dateOfIncident) throws WasteRetrievalException;

	WasteSubscriptionResponse createGardenWasteSubscription(long companyId, GardenWasteSubscription gardenWasteSubscription) throws WasteRetrievalException;

	String createMissedBinCollectionJob(long companyId, MissedBinCollectionJob missedBinCollectionJob) throws WasteRetrievalException;

	String createNewWasteContainerRequest(long companyId, NewContainerRequest newContainerRequest) throws WasteRetrievalException;

	boolean enabled(long companyId);

	List<WasteSubscriptionResponse> getAllWasteSubscriptions(long companyId, SubscriptionType subscriptionType) throws WasteRetrievalException;

	Set<BinCollection> getBinCollectionDatesByUPRN(long companyId, String uprn) throws WasteRetrievalException;

	Set<BinCollection> getBinCollectionDatesByUPRNAndService(long companyId, String uprn, String serviceName, String formInstanceId) throws WasteRetrievalException;

	Optional<BulkyCollectionDate> getBulkyCollectionDateByUPRN(long companyId, String uprn) throws WasteRetrievalException;

	List<Date> getBulkyCollectionSlotsByUPRNAndService(long companyId, String uprn, String serviceId, int numberOfDatesToReturn) throws WasteRetrievalException;

	String getMissedBinCollectionResponse(long companyId, String uprn, String date) throws WasteRetrievalException;

	String getName(long companyId);

	int howManyBinsSubscribed(long companyId, String uprn, String serviceType) throws WasteRetrievalException;

	int howManyBinsSubscribedForPeriod(long companyId, String uprn, String serviceType, String startDate, String endDate) throws WasteRetrievalException;

	@Deprecated
	boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws WasteRetrievalException;

	boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String binType, String formInstanceId)
			throws WasteRetrievalException;

	boolean isPropertyEligibleForWasteContainerRequest(long companyId, String uprn, String binType, String binSize, String formInstanceId) throws WasteRetrievalException;

	boolean isPropertyEligibleForBulkyWasteCollection(long companyId, String uprn, List<String> itemsForCollection, String formInstanceId) throws WasteRetrievalException;

	boolean isPropertyEligibleForMissedBinCollectionReport(long companyId, String uprn, String binType, String formInstanceId) throws WasteRetrievalException;

}
