package com.placecube.digitalplace.local.waste.model;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Map;

import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.Validator;

public class BinCollection {

	private final LocalDate collectionDate;

	private final LocalDate followingCollectionDate;

	private final String formattedCollectionDate;

	private final String frequency;

	private final Map<Locale, String> labelMap;

	private final String name;

	public BinCollection(BinCollectionBuilder binCollectionBuilder) {
		name = binCollectionBuilder.getName();
		collectionDate = binCollectionBuilder.getCollectionDate();
		labelMap = binCollectionBuilder.getLabelMap();
		formattedCollectionDate = binCollectionBuilder.getFormattedCollectionDate();
		frequency = binCollectionBuilder.getFrequency();
		followingCollectionDate = binCollectionBuilder.getFollowingCollectionDate();
	}

	public LocalDate getCollectionDate() {
		return collectionDate;
	}

	public LocalDate getFollowingCollectionDate() {
		return followingCollectionDate;
	}

	public String getFormattedCollectionDate() {
		return formattedCollectionDate;
	}

	public String getFrequency() {
		return frequency;
	}

	public String getLabel(Locale locale) {
		String label = labelMap.get(locale);

		if (Validator.isNull(label)) {
			label = labelMap.get(LocaleUtil.getDefault());
		}

		return label;
	}

	public String getName() {
		return name;
	}

	public DayOfWeek getDayOfWeek() {
		return collectionDate.getDayOfWeek();
	}

	public static class BinCollectionBuilder {

		private final LocalDate collectionDate;

		private LocalDate followingCollectionDate;

		private String formattedCollectionDate;

		private String frequency;

		private final Map<Locale, String> labelMap;

		private final String name;

		public BinCollectionBuilder(String name, LocalDate collectionDate, Map<Locale, String> labelMap) {
			this.name = name;
			this.collectionDate = collectionDate;
			this.labelMap = labelMap;
		}

		public BinCollection build() {
			return new BinCollection(this);
		}

		public BinCollectionBuilder followingCollectionDate(LocalDate date) {
			followingCollectionDate = date;
			return this;
		}

		public BinCollectionBuilder formattedCollectionDate(String date) {
			formattedCollectionDate = date;
			return this;
		}

		public BinCollectionBuilder frequency(String frequency) {
			this.frequency = frequency;
			return this;
		}

		public LocalDate getCollectionDate() {
			return collectionDate;
		}

		public LocalDate getFollowingCollectionDate() {
			return followingCollectionDate;
		}

		public String getFormattedCollectionDate() {
			return formattedCollectionDate;
		}

		public String getFrequency() {
			return frequency;
		}

		public Map<Locale, String> getLabelMap() {
			return labelMap;
		}

		public String getName() {
			return name;
		}
	}
}