package com.placecube.digitalplace.local.waste.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;

public interface WasteService {

	String createAssistedBinCollectionJob(long companyId, AssistedBinCollectionJob assistedBinCollectionJob) throws WasteRetrievalException;

	WasteSubscriptionResponse createBinSubscription(long companyId, String uprn, String serviceType, String firstName, String lastName, String emailAddress, String telephone, int numberOfBins)
			throws WasteRetrievalException;

	String createBulkyWasteCollectionJob(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, List<String> itemsToBeCollected, String locationOfItems,
			Date collectionDate) throws WasteRetrievalException;

	String createBulkyWasteCollectionJob(long companyId, BulkyWasteCollectionRequest bulkyWasteCollectionRequest, Date date) throws WasteRetrievalException;

	String createFlyTippingReport(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, String typeOfRubbish, String sizeOfRubbish, String details,
			String coordinates, String locationDetails, String dateOfIncident) throws WasteRetrievalException;

	WasteSubscriptionResponse createGardenWasteSubscription(long companyId, GardenWasteSubscription gardenWasteSubscription) throws WasteRetrievalException;

	String createMissedBinCollectionJob(long companyId, MissedBinCollectionJob missedBinCollectionJob) throws WasteRetrievalException;

	String createNewWasteContainerRequest(long companyId, NewContainerRequest newContainerRequest) throws WasteRetrievalException;

	List<WasteSubscriptionResponse> getAllWasteSubscriptions(long companyId, SubscriptionType subscriptionType) throws WasteRetrievalException;

	/**
	 * Retrieves a set of bin collections for the given property UPRN
	 *
	 * @param companyId The Liferay company id
	 * @param uprn Property UPRN
	 * @return Set of BinCollection for the property
	 * @throws WasteRetrievalException Thrown if there was an error when
	 *             retrieving the bin collections
	 */
	Set<BinCollection> getBinCollectionDatesByUPRN(long companyId, String uprn) throws WasteRetrievalException;

	/**
	 * Retrieves a set of bin collections for the given property UPRN and
	 * servoce name
	 *
	 * @param companyId The Liferay company id
	 * @param uprn Property UPRN
	 * @param serviceName the service name
	 * @param formInstanceId the form instance id
	 * @return Set of BinCollection for the property
	 * @throws WasteRetrievalException Thrown if there was an error when
	 *             retrieving the bin collections
	 */
	Set<BinCollection> getBinCollectionDatesByUPRNAndService(long companyId, String uprn, String serviceName, String formInstanceId) throws WasteRetrievalException;

	/**
	 * Retrieves a bin collection matching the name for the given property UPRN
	 *
	 * @param companyId The Liferay company id
	 * @param uprn Property UPRN
	 * @param name Name of bin type to return, e.g. recycling
	 * @return BinCollection matching name for the property
	 * @throws WasteRetrievalException Thrown if there was an error when
	 *             retrieving the bin collection
	 */
	Optional<BinCollection> getBinCollectionByUPRNAndName(long companyId, String uprn, String name) throws WasteRetrievalException;

	/**
	 * Retrieve the bulky collection date for the given property UPRN
	 *
	 * @param companyId The Liferay company id
	 * @param uprn Property UPRN
	 * @return the bulky collection date or empty for the property
	 * @throws WasteRetrievalException Thrown if there was an error when
	 *             retrieving the bulky collection date
	 */
	Optional<BulkyCollectionDate> getBulkyCollectionDateByUPRN(long companyId, String uprn) throws WasteRetrievalException;

	List<Date> getBulkyCollectionSlotsByUPRNAndService(long companyId, String uprn, String serviceId, int numberOfDatesToReturn) throws WasteRetrievalException;

	String getMissedBinCollectionResponse(long companyId, String uprn, String date) throws WasteRetrievalException;

	List<String> getWasteConnectorList(long companyId) throws WasteRetrievalException;

	int howManyBinsSubscribed(long companyId, String uprn, String serviceType) throws WasteRetrievalException;

	int howManyBinsSubscribedForPeriod(long companyId, String uprn, String serviceType, String startDate, String endDate) throws WasteRetrievalException;

	@Deprecated
	boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDate, String endDate) throws WasteRetrievalException;

	/**
	 * Checks if the property is eligible for a garden waste subscription
	 *
	 * @param companyId the company id
	 * @param uprn the property UPRN
	 * @param startDate the start date of the subscription
	 * @param endDate the end date of the subscription
	 * @param binType the bin type
	 * @param formInstanceId the form instance id
	 * @return true if the property is eligible, false otherwise
	 * @throws WasteRetrievalException if there is an error checking the
	 *             conditions
	 */
	boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDate, String endDate, String binType, String formInstanceId) throws WasteRetrievalException;

	/**
	 * Checks if the property is eligible for a bilky waste collection
	 *
	 * @param companyId the company id
	 * @param uprn the property UPRN
	 * @param itemsForCollection
	 * @param formInstanceId the form instance id
	 * @return true if the property is eligible, false otherwise
	 * @throws WasteRetrievalException if there is an error checking the
	 *             conditions
	 */
	boolean isPropertyEligibleForBulkyWasteCollection(long companyId, String uprn, List<String> itemsForCollection, String formInstanceId) throws WasteRetrievalException;

	/**
	 * Checks if the property is eligible for a missed bin collection report
	 *
	 * @param companyId the company id
	 * @param uprn the property UPRN
	 * @param binType the bin type
	 * @param formInstanceId the form instance id
	 * @return true if the property is eligible, false otherwise
	 * @throws WasteRetrievalException if there is an error checking the
	 *             conditions
	 */
	boolean isPropertyEligibleForMissedBinCollectionReport(long companyId, String uprn, String binType, String formInstanceId) throws WasteRetrievalException;

	/**
	 * Checks if the property is eligible for a waste container request
	 *
	 * @param companyId the company id
	 * @param uprn the property UPRN
	 * @param binType the bin type
	 * @param binSize the bin size
	 * @param formInstanceId the form instance id
	 * @return true if the property is eligible, false otherwise
	 * @throws WasteRetrievalException if there is an error checking the
	 *             conditions
	 */
	boolean isPropertyEligibleForWasteContainerRequest(long companyId, String uprn, String binType, String binSize, String formInstanceId) throws WasteRetrievalException;

}
