package com.placecube.digitalplace.local.waste.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GardenWasteSubscription extends WasteRequest {

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String telephone;
	private Integer numberSubscribedBins;
	private Integer numberRequestedNewBins;
	private Boolean deliverBinStickers;
	private String startDate;
	private String endDate;
	private String binType;
	private String newPayRef;
	private String gardenContainerDeliveryComments;
	private String crmGardenRef;

	public GardenWasteSubscription() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Integer getNumberSubscribedBins() {
		return numberSubscribedBins;
	}

	public void setNumberSubscribedBins(Integer numberSubscribedBins) {
		this.numberSubscribedBins = numberSubscribedBins;
	}

	public Integer getNumberRequestedNewBins() {
		return numberRequestedNewBins;
	}

	public void setNumberRequestedNewBins(Integer numberRequestedNewBins) {
		this.numberRequestedNewBins = numberRequestedNewBins;
	}

	public Boolean getDeliverBinStickers() {
		return deliverBinStickers;
	}

	public void setDeliverBinStickers(Boolean deliverBinStickers) {
		this.deliverBinStickers = deliverBinStickers;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getBinType() {
		return binType;
	}

	public void setBinType(String binType) {
		this.binType = binType;
	}

	public String getNewPayRef() {
		return newPayRef;
	}

	public void setNewPayRef(String newPayRef) {
		this.newPayRef = newPayRef;
	}

	public String getGardenContainerDeliveryComments() {
		return gardenContainerDeliveryComments;
	}

	public void setGardenContainerDeliveryComments(String gardenContainerDeliveryComments) {
		this.gardenContainerDeliveryComments = gardenContainerDeliveryComments;
	}

	public String getCrmGardenRef() {
		return crmGardenRef;
	}

	public void setCrmGardenRef(String crmGardenRef) {
		this.crmGardenRef = crmGardenRef;
	}

}
