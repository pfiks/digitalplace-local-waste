package com.placecube.digitalplace.local.waste.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AssistedBinCollectionJob extends WasteRequest {

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String telephone;
	private String startDate;
	private String endDate;
	private String binLocation;
	private Integer numberOfResidents;
	private boolean riskAssessmentRequired;
	private String riskAssessmentReason;

	public AssistedBinCollectionJob() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getBinLocation() {
		return binLocation;
	}

	public void setBinLocation(String binLocation) {
		this.binLocation = binLocation;
	}

	public Integer getNumberOfResidents() {
		return numberOfResidents;
	}

	public void setNumberOfResidents(Integer numberOfResidents) {
		this.numberOfResidents = numberOfResidents;
	}

	public boolean isRiskAssessmentRequired() {
		return riskAssessmentRequired;
	}

	public void setRiskAssessmentRequired(boolean riskAssessmentRequired) {
		this.riskAssessmentRequired = riskAssessmentRequired;
	}

	public String getRiskAssessmentReason() {
		return riskAssessmentReason;
	}

	public void setRiskAssessmentReason(String riskAssessmentReason) {
		this.riskAssessmentReason = riskAssessmentReason;
	}
}
