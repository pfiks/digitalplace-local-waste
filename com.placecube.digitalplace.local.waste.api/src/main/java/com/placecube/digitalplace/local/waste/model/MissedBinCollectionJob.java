package com.placecube.digitalplace.local.waste.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MissedBinCollectionJob extends WasteRequest {

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String telephone;
	private String binType;
	private String missedDate;

	public MissedBinCollectionJob() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getBinType() {
		return binType;
	}

	public void setBinType(String binType) {
		this.binType = binType;
	}

	public String getMissedDate() {
		return missedDate;
	}

	public void setMissedDate(String missedDate) {
		this.missedDate = missedDate;
	}
}
