package com.placecube.digitalplace.local.waste.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NewContainerRequest extends WasteRequest {

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String telephone;
	private String binType;
	private String binSize;
	private String reasonForRequest;
	private String additionalDetails;
	private boolean collectOldBin;

	public NewContainerRequest() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getBinType() {
		return binType;
	}

	public void setBinType(String binType) {
		this.binType = binType;
	}

	public String getBinSize() {
		return binSize;
	}

	public void setBinSize(String binSize) {
		this.binSize = binSize;
	}

	public String getReasonForRequest() {
		return reasonForRequest;
	}

	public void setReasonForRequest(String reasonForRequest) {
		this.reasonForRequest = reasonForRequest;
	}

	public String getAdditionalDetails() {
		return additionalDetails;
	}

	public void setAdditionalDetails(String additionalDetails) {
		this.additionalDetails = additionalDetails;
	}

	public boolean isCollectOldBin() {
		return collectOldBin;
	}

	public void setCollectOldBin(boolean collectOldBin) {
		this.collectOldBin = collectOldBin;
	}

}
