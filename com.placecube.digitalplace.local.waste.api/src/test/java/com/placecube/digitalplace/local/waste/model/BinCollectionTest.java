package com.placecube.digitalplace.local.waste.model;

import static org.junit.Assert.assertEquals;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.util.LocaleUtil;

public class BinCollectionTest extends PowerMockito {

	private static final String DEFAULT_LABEL = "default";

	private static final String FREQUENCY = "frequency";

	private static final String LABEL = "label";

	private static final String NAME = "name";

	private BinCollection binCollection;

	private Map<Locale, String> labelMap = new HashMap<>();

	private LocalDate today = LocalDate.now();

	@Test
	public void getDayOfWeek_WhenCollectionDateExists_ReturnDayOfWeek() {

		DayOfWeek result = binCollection.getDayOfWeek();

		assertEquals(today.getDayOfWeek(), result);
	}

	@Test
	public void getLabel_WhenMatchingLabelForLocale_ReturnLabel() {

		Locale locale = LocaleUtil.CANADA;

		String result = binCollection.getLabel(locale);

		assertEquals(LABEL, result);
	}

	@Test
	public void getLabel_WhenNoMatchingLabelForLocale_ReturnLabelForDefaultLocale() {

		Locale locale = LocaleUtil.BRAZIL;

		String result = binCollection.getLabel(locale);

		assertEquals(DEFAULT_LABEL, result);
	}

	@Before
	public void setup() {
		labelMap.put(LocaleUtil.getDefault(), DEFAULT_LABEL);
		labelMap.put(LocaleUtil.CANADA, LABEL);
		binCollection = new BinCollection.BinCollectionBuilder(NAME, today, labelMap).frequency(FREQUENCY).followingCollectionDate(today).build();
	}

}