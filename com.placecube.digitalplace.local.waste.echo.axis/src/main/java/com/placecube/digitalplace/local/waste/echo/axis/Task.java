/**
 * Task.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class Task  extends com.placecube.digitalplace.local.waste.echo.axis.ExtensibleObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.TaskAllocation allocation;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset completedDate;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset currentScheduledDate;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset originalScheduledDate;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref;

    private com.placecube.digitalplace.local.waste.echo.axis.TaskResolutionInfo resolution;

    private java.lang.Integer serviceId;

    private java.lang.Integer serviceTaskId;

    private java.lang.Integer serviceTaskScheduleId;

    private java.lang.Integer serviceUnitId;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef source;

    private com.placecube.digitalplace.local.waste.echo.axis.TaskStateInfo state;

    private java.lang.Integer taskTypeId;

    private com.placecube.digitalplace.local.waste.echo.axis.TimeBand timeBand;

    private com.placecube.digitalplace.local.waste.echo.axis.TaskPointInfo[] points;

    public Task() {
    }

    public Task(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data,
           com.placecube.digitalplace.local.waste.echo.axis.TaskAllocation allocation,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset completedDate,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset currentScheduledDate,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset originalScheduledDate,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref,
           com.placecube.digitalplace.local.waste.echo.axis.TaskResolutionInfo resolution,
           java.lang.Integer serviceId,
           java.lang.Integer serviceTaskId,
           java.lang.Integer serviceTaskScheduleId,
           java.lang.Integer serviceUnitId,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef source,
           com.placecube.digitalplace.local.waste.echo.axis.TaskStateInfo state,
           java.lang.Integer taskTypeId,
           com.placecube.digitalplace.local.waste.echo.axis.TimeBand timeBand,
           com.placecube.digitalplace.local.waste.echo.axis.TaskPointInfo[] points) {
        super(
            guid,
            id,
            data);
        this.allocation = allocation;
        this.completedDate = completedDate;
        this.currentScheduledDate = currentScheduledDate;
        this.originalScheduledDate = originalScheduledDate;
        this.ref = ref;
        this.resolution = resolution;
        this.serviceId = serviceId;
        this.serviceTaskId = serviceTaskId;
        this.serviceTaskScheduleId = serviceTaskScheduleId;
        this.serviceUnitId = serviceUnitId;
        this.source = source;
        this.state = state;
        this.taskTypeId = taskTypeId;
        this.timeBand = timeBand;
        this.points = points;
    }


    /**
     * Gets the allocation value for this Task.
     * 
     * @return allocation
     */
    public com.placecube.digitalplace.local.waste.echo.axis.TaskAllocation getAllocation() {
        return allocation;
    }


    /**
     * Sets the allocation value for this Task.
     * 
     * @param allocation
     */
    public void setAllocation(com.placecube.digitalplace.local.waste.echo.axis.TaskAllocation allocation) {
        this.allocation = allocation;
    }


    /**
     * Gets the completedDate value for this Task.
     * 
     * @return completedDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getCompletedDate() {
        return completedDate;
    }


    /**
     * Sets the completedDate value for this Task.
     * 
     * @param completedDate
     */
    public void setCompletedDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset completedDate) {
        this.completedDate = completedDate;
    }


    /**
     * Gets the currentScheduledDate value for this Task.
     * 
     * @return currentScheduledDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getCurrentScheduledDate() {
        return currentScheduledDate;
    }


    /**
     * Sets the currentScheduledDate value for this Task.
     * 
     * @param currentScheduledDate
     */
    public void setCurrentScheduledDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset currentScheduledDate) {
        this.currentScheduledDate = currentScheduledDate;
    }


    /**
     * Gets the originalScheduledDate value for this Task.
     * 
     * @return originalScheduledDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getOriginalScheduledDate() {
        return originalScheduledDate;
    }


    /**
     * Sets the originalScheduledDate value for this Task.
     * 
     * @param originalScheduledDate
     */
    public void setOriginalScheduledDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset originalScheduledDate) {
        this.originalScheduledDate = originalScheduledDate;
    }


    /**
     * Gets the ref value for this Task.
     * 
     * @return ref
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getRef() {
        return ref;
    }


    /**
     * Sets the ref value for this Task.
     * 
     * @param ref
     */
    public void setRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) {
        this.ref = ref;
    }


    /**
     * Gets the resolution value for this Task.
     * 
     * @return resolution
     */
    public com.placecube.digitalplace.local.waste.echo.axis.TaskResolutionInfo getResolution() {
        return resolution;
    }


    /**
     * Sets the resolution value for this Task.
     * 
     * @param resolution
     */
    public void setResolution(com.placecube.digitalplace.local.waste.echo.axis.TaskResolutionInfo resolution) {
        this.resolution = resolution;
    }


    /**
     * Gets the serviceId value for this Task.
     * 
     * @return serviceId
     */
    public java.lang.Integer getServiceId() {
        return serviceId;
    }


    /**
     * Sets the serviceId value for this Task.
     * 
     * @param serviceId
     */
    public void setServiceId(java.lang.Integer serviceId) {
        this.serviceId = serviceId;
    }


    /**
     * Gets the serviceTaskId value for this Task.
     * 
     * @return serviceTaskId
     */
    public java.lang.Integer getServiceTaskId() {
        return serviceTaskId;
    }


    /**
     * Sets the serviceTaskId value for this Task.
     * 
     * @param serviceTaskId
     */
    public void setServiceTaskId(java.lang.Integer serviceTaskId) {
        this.serviceTaskId = serviceTaskId;
    }


    /**
     * Gets the serviceTaskScheduleId value for this Task.
     * 
     * @return serviceTaskScheduleId
     */
    public java.lang.Integer getServiceTaskScheduleId() {
        return serviceTaskScheduleId;
    }


    /**
     * Sets the serviceTaskScheduleId value for this Task.
     * 
     * @param serviceTaskScheduleId
     */
    public void setServiceTaskScheduleId(java.lang.Integer serviceTaskScheduleId) {
        this.serviceTaskScheduleId = serviceTaskScheduleId;
    }


    /**
     * Gets the serviceUnitId value for this Task.
     * 
     * @return serviceUnitId
     */
    public java.lang.Integer getServiceUnitId() {
        return serviceUnitId;
    }


    /**
     * Sets the serviceUnitId value for this Task.
     * 
     * @param serviceUnitId
     */
    public void setServiceUnitId(java.lang.Integer serviceUnitId) {
        this.serviceUnitId = serviceUnitId;
    }


    /**
     * Gets the source value for this Task.
     * 
     * @return source
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getSource() {
        return source;
    }


    /**
     * Sets the source value for this Task.
     * 
     * @param source
     */
    public void setSource(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef source) {
        this.source = source;
    }


    /**
     * Gets the state value for this Task.
     * 
     * @return state
     */
    public com.placecube.digitalplace.local.waste.echo.axis.TaskStateInfo getState() {
        return state;
    }


    /**
     * Sets the state value for this Task.
     * 
     * @param state
     */
    public void setState(com.placecube.digitalplace.local.waste.echo.axis.TaskStateInfo state) {
        this.state = state;
    }


    /**
     * Gets the taskTypeId value for this Task.
     * 
     * @return taskTypeId
     */
    public java.lang.Integer getTaskTypeId() {
        return taskTypeId;
    }


    /**
     * Sets the taskTypeId value for this Task.
     * 
     * @param taskTypeId
     */
    public void setTaskTypeId(java.lang.Integer taskTypeId) {
        this.taskTypeId = taskTypeId;
    }


    /**
     * Gets the timeBand value for this Task.
     * 
     * @return timeBand
     */
    public com.placecube.digitalplace.local.waste.echo.axis.TimeBand getTimeBand() {
        return timeBand;
    }


    /**
     * Sets the timeBand value for this Task.
     * 
     * @param timeBand
     */
    public void setTimeBand(com.placecube.digitalplace.local.waste.echo.axis.TimeBand timeBand) {
        this.timeBand = timeBand;
    }


    /**
     * Gets the points value for this Task.
     * 
     * @return points
     */
    public com.placecube.digitalplace.local.waste.echo.axis.TaskPointInfo[] getPoints() {
        return points;
    }


    /**
     * Sets the points value for this Task.
     * 
     * @param points
     */
    public void setPoints(com.placecube.digitalplace.local.waste.echo.axis.TaskPointInfo[] points) {
        this.points = points;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Task)) return false;
        Task other = (Task) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.allocation==null && other.getAllocation()==null) || 
             (this.allocation!=null &&
              this.allocation.equals(other.getAllocation()))) &&
            ((this.completedDate==null && other.getCompletedDate()==null) || 
             (this.completedDate!=null &&
              this.completedDate.equals(other.getCompletedDate()))) &&
            ((this.currentScheduledDate==null && other.getCurrentScheduledDate()==null) || 
             (this.currentScheduledDate!=null &&
              this.currentScheduledDate.equals(other.getCurrentScheduledDate()))) &&
            ((this.originalScheduledDate==null && other.getOriginalScheduledDate()==null) || 
             (this.originalScheduledDate!=null &&
              this.originalScheduledDate.equals(other.getOriginalScheduledDate()))) &&
            ((this.ref==null && other.getRef()==null) || 
             (this.ref!=null &&
              this.ref.equals(other.getRef()))) &&
            ((this.resolution==null && other.getResolution()==null) || 
             (this.resolution!=null &&
              this.resolution.equals(other.getResolution()))) &&
            ((this.serviceId==null && other.getServiceId()==null) || 
             (this.serviceId!=null &&
              this.serviceId.equals(other.getServiceId()))) &&
            ((this.serviceTaskId==null && other.getServiceTaskId()==null) || 
             (this.serviceTaskId!=null &&
              this.serviceTaskId.equals(other.getServiceTaskId()))) &&
            ((this.serviceTaskScheduleId==null && other.getServiceTaskScheduleId()==null) || 
             (this.serviceTaskScheduleId!=null &&
              this.serviceTaskScheduleId.equals(other.getServiceTaskScheduleId()))) &&
            ((this.serviceUnitId==null && other.getServiceUnitId()==null) || 
             (this.serviceUnitId!=null &&
              this.serviceUnitId.equals(other.getServiceUnitId()))) &&
            ((this.source==null && other.getSource()==null) || 
             (this.source!=null &&
              this.source.equals(other.getSource()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.taskTypeId==null && other.getTaskTypeId()==null) || 
             (this.taskTypeId!=null &&
              this.taskTypeId.equals(other.getTaskTypeId()))) &&
            ((this.timeBand==null && other.getTimeBand()==null) || 
             (this.timeBand!=null &&
              this.timeBand.equals(other.getTimeBand()))) &&
            ((this.points==null && other.getPoints()==null) || 
             (this.points!=null &&
              java.util.Arrays.equals(this.points, other.getPoints())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAllocation() != null) {
            _hashCode += getAllocation().hashCode();
        }
        if (getCompletedDate() != null) {
            _hashCode += getCompletedDate().hashCode();
        }
        if (getCurrentScheduledDate() != null) {
            _hashCode += getCurrentScheduledDate().hashCode();
        }
        if (getOriginalScheduledDate() != null) {
            _hashCode += getOriginalScheduledDate().hashCode();
        }
        if (getRef() != null) {
            _hashCode += getRef().hashCode();
        }
        if (getResolution() != null) {
            _hashCode += getResolution().hashCode();
        }
        if (getServiceId() != null) {
            _hashCode += getServiceId().hashCode();
        }
        if (getServiceTaskId() != null) {
            _hashCode += getServiceTaskId().hashCode();
        }
        if (getServiceTaskScheduleId() != null) {
            _hashCode += getServiceTaskScheduleId().hashCode();
        }
        if (getServiceUnitId() != null) {
            _hashCode += getServiceUnitId().hashCode();
        }
        if (getSource() != null) {
            _hashCode += getSource().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getTaskTypeId() != null) {
            _hashCode += getTaskTypeId().hashCode();
        }
        if (getTimeBand() != null) {
            _hashCode += getTimeBand().hashCode();
        }
        if (getPoints() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPoints());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPoints(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Task.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Task"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Allocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskAllocation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("completedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CompletedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentScheduledDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CurrentScheduledDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalScheduledDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "OriginalScheduledDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ref");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Ref"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resolution");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Resolution"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskResolutionInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceTaskId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceTaskScheduleId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskScheduleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceUnitId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("source");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Source"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskStateInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeBand");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TimeBand"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TimeBand"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("points");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Points"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskPointInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskPointInfo"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
