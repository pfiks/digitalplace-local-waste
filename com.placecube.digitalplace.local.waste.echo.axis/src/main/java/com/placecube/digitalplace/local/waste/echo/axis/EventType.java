/**
 * EventType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class EventType  extends com.placecube.digitalplace.local.waste.echo.axis.ExtensibleType  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate;

    private java.lang.String prefix;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate;

    private com.placecube.digitalplace.local.waste.echo.axis.StateWorkflow workflow;

    public EventType() {
    }

    public EventType(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatatype[] datatypes,
           java.lang.String name,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate,
           java.lang.String prefix,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate,
           com.placecube.digitalplace.local.waste.echo.axis.StateWorkflow workflow) {
        super(
            guid,
            id,
            datatypes,
            name);
        this.endDate = endDate;
        this.prefix = prefix;
        this.startDate = startDate;
        this.workflow = workflow;
    }


    /**
     * Gets the endDate value for this EventType.
     * 
     * @return endDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this EventType.
     * 
     * @param endDate
     */
    public void setEndDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the prefix value for this EventType.
     * 
     * @return prefix
     */
    public java.lang.String getPrefix() {
        return prefix;
    }


    /**
     * Sets the prefix value for this EventType.
     * 
     * @param prefix
     */
    public void setPrefix(java.lang.String prefix) {
        this.prefix = prefix;
    }


    /**
     * Gets the startDate value for this EventType.
     * 
     * @return startDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this EventType.
     * 
     * @param startDate
     */
    public void setStartDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the workflow value for this EventType.
     * 
     * @return workflow
     */
    public com.placecube.digitalplace.local.waste.echo.axis.StateWorkflow getWorkflow() {
        return workflow;
    }


    /**
     * Sets the workflow value for this EventType.
     * 
     * @param workflow
     */
    public void setWorkflow(com.placecube.digitalplace.local.waste.echo.axis.StateWorkflow workflow) {
        this.workflow = workflow;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventType)) return false;
        EventType other = (EventType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.prefix==null && other.getPrefix()==null) || 
             (this.prefix!=null &&
              this.prefix.equals(other.getPrefix()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.workflow==null && other.getWorkflow()==null) || 
             (this.workflow!=null &&
              this.workflow.equals(other.getWorkflow())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getPrefix() != null) {
            _hashCode += getPrefix().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getWorkflow() != null) {
            _hashCode += getWorkflow().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prefix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Prefix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflow");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Workflow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateWorkflow"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
