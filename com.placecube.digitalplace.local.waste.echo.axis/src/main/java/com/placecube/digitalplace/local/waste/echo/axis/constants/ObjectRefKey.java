package com.placecube.digitalplace.local.waste.echo.axis.constants;

public enum ObjectRefKey {

	ID("Id"), UPRN("Uprn"), CLIENT_REFERENCE("ClientReference");

	private String key;

	private ObjectRefKey(String key) {
		this.key = key;
	}

	public String getStrVal() {
		return key;
	}
}
