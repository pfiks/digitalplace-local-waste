/**
 * StreetQuery.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class StreetQuery  implements java.io.Serializable {
    private java.lang.String postcodeOutward;

    private java.lang.String startsWith;

    public StreetQuery() {
    }

    public StreetQuery(
           java.lang.String postcodeOutward,
           java.lang.String startsWith) {
           this.postcodeOutward = postcodeOutward;
           this.startsWith = startsWith;
    }


    /**
     * Gets the postcodeOutward value for this StreetQuery.
     * 
     * @return postcodeOutward
     */
    public java.lang.String getPostcodeOutward() {
        return postcodeOutward;
    }


    /**
     * Sets the postcodeOutward value for this StreetQuery.
     * 
     * @param postcodeOutward
     */
    public void setPostcodeOutward(java.lang.String postcodeOutward) {
        this.postcodeOutward = postcodeOutward;
    }


    /**
     * Gets the startsWith value for this StreetQuery.
     * 
     * @return startsWith
     */
    public java.lang.String getStartsWith() {
        return startsWith;
    }


    /**
     * Sets the startsWith value for this StreetQuery.
     * 
     * @param startsWith
     */
    public void setStartsWith(java.lang.String startsWith) {
        this.startsWith = startsWith;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StreetQuery)) return false;
        StreetQuery other = (StreetQuery) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.postcodeOutward==null && other.getPostcodeOutward()==null) || 
             (this.postcodeOutward!=null &&
              this.postcodeOutward.equals(other.getPostcodeOutward()))) &&
            ((this.startsWith==null && other.getStartsWith()==null) || 
             (this.startsWith!=null &&
              this.startsWith.equals(other.getStartsWith())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPostcodeOutward() != null) {
            _hashCode += getPostcodeOutward().hashCode();
        }
        if (getStartsWith() != null) {
            _hashCode += getStartsWith().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StreetQuery.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StreetQuery"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postcodeOutward");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PostcodeOutward"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startsWith");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StartsWith"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
