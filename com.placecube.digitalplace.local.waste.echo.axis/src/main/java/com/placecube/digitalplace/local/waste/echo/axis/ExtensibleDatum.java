/**
 * ExtensibleDatum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ExtensibleDatum  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] childData;

    private java.lang.Integer datatypeId;

    private java.lang.String datatypeName;

    private java.lang.String value;

    public ExtensibleDatum() {
    }

    public ExtensibleDatum(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] childData,
           java.lang.Integer datatypeId,
           java.lang.String datatypeName,
           java.lang.String value) {
        super(
            guid,
            id);
        this.childData = childData;
        this.datatypeId = datatypeId;
        this.datatypeName = datatypeName;
        this.value = value;
    }


    /**
     * Gets the childData value for this ExtensibleDatum.
     * 
     * @return childData
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] getChildData() {
        return childData;
    }


    /**
     * Sets the childData value for this ExtensibleDatum.
     * 
     * @param childData
     */
    public void setChildData(com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] childData) {
        this.childData = childData;
    }


    /**
     * Gets the datatypeId value for this ExtensibleDatum.
     * 
     * @return datatypeId
     */
    public java.lang.Integer getDatatypeId() {
        return datatypeId;
    }


    /**
     * Sets the datatypeId value for this ExtensibleDatum.
     * 
     * @param datatypeId
     */
    public void setDatatypeId(java.lang.Integer datatypeId) {
        this.datatypeId = datatypeId;
    }


    /**
     * Gets the datatypeName value for this ExtensibleDatum.
     * 
     * @return datatypeName
     */
    public java.lang.String getDatatypeName() {
        return datatypeName;
    }


    /**
     * Sets the datatypeName value for this ExtensibleDatum.
     * 
     * @param datatypeName
     */
    public void setDatatypeName(java.lang.String datatypeName) {
        this.datatypeName = datatypeName;
    }


    /**
     * Gets the value value for this ExtensibleDatum.
     * 
     * @return value
     */
    public java.lang.String getValue() {
        return value;
    }


    /**
     * Sets the value value for this ExtensibleDatum.
     * 
     * @param value
     */
    public void setValue(java.lang.String value) {
        this.value = value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExtensibleDatum)) return false;
        ExtensibleDatum other = (ExtensibleDatum) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.childData==null && other.getChildData()==null) || 
             (this.childData!=null &&
              java.util.Arrays.equals(this.childData, other.getChildData()))) &&
            ((this.datatypeId==null && other.getDatatypeId()==null) || 
             (this.datatypeId!=null &&
              this.datatypeId.equals(other.getDatatypeId()))) &&
            ((this.datatypeName==null && other.getDatatypeName()==null) || 
             (this.datatypeName!=null &&
              this.datatypeName.equals(other.getDatatypeName()))) &&
            ((this.value==null && other.getValue()==null) || 
             (this.value!=null &&
              this.value.equals(other.getValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getChildData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getChildData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getChildData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDatatypeId() != null) {
            _hashCode += getDatatypeId().hashCode();
        }
        if (getDatatypeName() != null) {
            _hashCode += getDatatypeName().hashCode();
        }
        if (getValue() != null) {
            _hashCode += getValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExtensibleDatum.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatum"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("childData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ChildData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatum"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datatypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "DatatypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datatypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "DatatypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("value");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
