/**
 * GetEventTypesForServiceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetEventTypesForServiceResponse  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.EventType[] getEventTypesForServiceResult;

    public GetEventTypesForServiceResponse() {
    }

    public GetEventTypesForServiceResponse(
           com.placecube.digitalplace.local.waste.echo.axis.EventType[] getEventTypesForServiceResult) {
           this.getEventTypesForServiceResult = getEventTypesForServiceResult;
    }


    /**
     * Gets the getEventTypesForServiceResult value for this GetEventTypesForServiceResponse.
     * 
     * @return getEventTypesForServiceResult
     */
    public com.placecube.digitalplace.local.waste.echo.axis.EventType[] getGetEventTypesForServiceResult() {
        return getEventTypesForServiceResult;
    }


    /**
     * Sets the getEventTypesForServiceResult value for this GetEventTypesForServiceResponse.
     * 
     * @param getEventTypesForServiceResult
     */
    public void setGetEventTypesForServiceResult(com.placecube.digitalplace.local.waste.echo.axis.EventType[] getEventTypesForServiceResult) {
        this.getEventTypesForServiceResult = getEventTypesForServiceResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEventTypesForServiceResponse)) return false;
        GetEventTypesForServiceResponse other = (GetEventTypesForServiceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getEventTypesForServiceResult==null && other.getGetEventTypesForServiceResult()==null) || 
             (this.getEventTypesForServiceResult!=null &&
              java.util.Arrays.equals(this.getEventTypesForServiceResult, other.getGetEventTypesForServiceResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetEventTypesForServiceResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetEventTypesForServiceResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetEventTypesForServiceResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEventTypesForServiceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventTypesForServiceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getEventTypesForServiceResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventTypesForServiceResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
