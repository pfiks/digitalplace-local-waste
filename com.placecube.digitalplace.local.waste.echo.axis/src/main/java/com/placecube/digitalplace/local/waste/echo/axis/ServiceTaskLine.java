/**
 * ServiceTaskLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceTaskLine  extends com.placecube.digitalplace.local.waste.echo.axis.ExtensibleObject  implements java.io.Serializable {
    private java.lang.Integer assetTypeId;

    private java.lang.String assetTypeName;

    private java.lang.String coreTaskLineType;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate;

    private java.lang.Boolean isSerialised;

    private java.lang.String productDimension;

    private java.lang.Integer productId;

    private java.lang.String productName;

    private java.lang.Integer scheduledAssetQuantity;

    private java.lang.Double scheduledProductQuantity;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate;

    private java.lang.Integer taskLineTypeId;

    private java.lang.String taskLineTypeName;

    public ServiceTaskLine() {
    }

    public ServiceTaskLine(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data,
           java.lang.Integer assetTypeId,
           java.lang.String assetTypeName,
           java.lang.String coreTaskLineType,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate,
           java.lang.Boolean isSerialised,
           java.lang.String productDimension,
           java.lang.Integer productId,
           java.lang.String productName,
           java.lang.Integer scheduledAssetQuantity,
           java.lang.Double scheduledProductQuantity,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate,
           java.lang.Integer taskLineTypeId,
           java.lang.String taskLineTypeName) {
        super(
            guid,
            id,
            data);
        this.assetTypeId = assetTypeId;
        this.assetTypeName = assetTypeName;
        this.coreTaskLineType = coreTaskLineType;
        this.endDate = endDate;
        this.isSerialised = isSerialised;
        this.productDimension = productDimension;
        this.productId = productId;
        this.productName = productName;
        this.scheduledAssetQuantity = scheduledAssetQuantity;
        this.scheduledProductQuantity = scheduledProductQuantity;
        this.startDate = startDate;
        this.taskLineTypeId = taskLineTypeId;
        this.taskLineTypeName = taskLineTypeName;
    }


    /**
     * Gets the assetTypeId value for this ServiceTaskLine.
     * 
     * @return assetTypeId
     */
    public java.lang.Integer getAssetTypeId() {
        return assetTypeId;
    }


    /**
     * Sets the assetTypeId value for this ServiceTaskLine.
     * 
     * @param assetTypeId
     */
    public void setAssetTypeId(java.lang.Integer assetTypeId) {
        this.assetTypeId = assetTypeId;
    }


    /**
     * Gets the assetTypeName value for this ServiceTaskLine.
     * 
     * @return assetTypeName
     */
    public java.lang.String getAssetTypeName() {
        return assetTypeName;
    }


    /**
     * Sets the assetTypeName value for this ServiceTaskLine.
     * 
     * @param assetTypeName
     */
    public void setAssetTypeName(java.lang.String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }


    /**
     * Gets the coreTaskLineType value for this ServiceTaskLine.
     * 
     * @return coreTaskLineType
     */
    public java.lang.String getCoreTaskLineType() {
        return coreTaskLineType;
    }


    /**
     * Sets the coreTaskLineType value for this ServiceTaskLine.
     * 
     * @param coreTaskLineType
     */
    public void setCoreTaskLineType(java.lang.String coreTaskLineType) {
        this.coreTaskLineType = coreTaskLineType;
    }


    /**
     * Gets the endDate value for this ServiceTaskLine.
     * 
     * @return endDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this ServiceTaskLine.
     * 
     * @param endDate
     */
    public void setEndDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the isSerialised value for this ServiceTaskLine.
     * 
     * @return isSerialised
     */
    public java.lang.Boolean getIsSerialised() {
        return isSerialised;
    }


    /**
     * Sets the isSerialised value for this ServiceTaskLine.
     * 
     * @param isSerialised
     */
    public void setIsSerialised(java.lang.Boolean isSerialised) {
        this.isSerialised = isSerialised;
    }


    /**
     * Gets the productDimension value for this ServiceTaskLine.
     * 
     * @return productDimension
     */
    public java.lang.String getProductDimension() {
        return productDimension;
    }


    /**
     * Sets the productDimension value for this ServiceTaskLine.
     * 
     * @param productDimension
     */
    public void setProductDimension(java.lang.String productDimension) {
        this.productDimension = productDimension;
    }


    /**
     * Gets the productId value for this ServiceTaskLine.
     * 
     * @return productId
     */
    public java.lang.Integer getProductId() {
        return productId;
    }


    /**
     * Sets the productId value for this ServiceTaskLine.
     * 
     * @param productId
     */
    public void setProductId(java.lang.Integer productId) {
        this.productId = productId;
    }


    /**
     * Gets the productName value for this ServiceTaskLine.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this ServiceTaskLine.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the scheduledAssetQuantity value for this ServiceTaskLine.
     * 
     * @return scheduledAssetQuantity
     */
    public java.lang.Integer getScheduledAssetQuantity() {
        return scheduledAssetQuantity;
    }


    /**
     * Sets the scheduledAssetQuantity value for this ServiceTaskLine.
     * 
     * @param scheduledAssetQuantity
     */
    public void setScheduledAssetQuantity(java.lang.Integer scheduledAssetQuantity) {
        this.scheduledAssetQuantity = scheduledAssetQuantity;
    }


    /**
     * Gets the scheduledProductQuantity value for this ServiceTaskLine.
     * 
     * @return scheduledProductQuantity
     */
    public java.lang.Double getScheduledProductQuantity() {
        return scheduledProductQuantity;
    }


    /**
     * Sets the scheduledProductQuantity value for this ServiceTaskLine.
     * 
     * @param scheduledProductQuantity
     */
    public void setScheduledProductQuantity(java.lang.Double scheduledProductQuantity) {
        this.scheduledProductQuantity = scheduledProductQuantity;
    }


    /**
     * Gets the startDate value for this ServiceTaskLine.
     * 
     * @return startDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this ServiceTaskLine.
     * 
     * @param startDate
     */
    public void setStartDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the taskLineTypeId value for this ServiceTaskLine.
     * 
     * @return taskLineTypeId
     */
    public java.lang.Integer getTaskLineTypeId() {
        return taskLineTypeId;
    }


    /**
     * Sets the taskLineTypeId value for this ServiceTaskLine.
     * 
     * @param taskLineTypeId
     */
    public void setTaskLineTypeId(java.lang.Integer taskLineTypeId) {
        this.taskLineTypeId = taskLineTypeId;
    }


    /**
     * Gets the taskLineTypeName value for this ServiceTaskLine.
     * 
     * @return taskLineTypeName
     */
    public java.lang.String getTaskLineTypeName() {
        return taskLineTypeName;
    }


    /**
     * Sets the taskLineTypeName value for this ServiceTaskLine.
     * 
     * @param taskLineTypeName
     */
    public void setTaskLineTypeName(java.lang.String taskLineTypeName) {
        this.taskLineTypeName = taskLineTypeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceTaskLine)) return false;
        ServiceTaskLine other = (ServiceTaskLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.assetTypeId==null && other.getAssetTypeId()==null) || 
             (this.assetTypeId!=null &&
              this.assetTypeId.equals(other.getAssetTypeId()))) &&
            ((this.assetTypeName==null && other.getAssetTypeName()==null) || 
             (this.assetTypeName!=null &&
              this.assetTypeName.equals(other.getAssetTypeName()))) &&
            ((this.coreTaskLineType==null && other.getCoreTaskLineType()==null) || 
             (this.coreTaskLineType!=null &&
              this.coreTaskLineType.equals(other.getCoreTaskLineType()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.isSerialised==null && other.getIsSerialised()==null) || 
             (this.isSerialised!=null &&
              this.isSerialised.equals(other.getIsSerialised()))) &&
            ((this.productDimension==null && other.getProductDimension()==null) || 
             (this.productDimension!=null &&
              this.productDimension.equals(other.getProductDimension()))) &&
            ((this.productId==null && other.getProductId()==null) || 
             (this.productId!=null &&
              this.productId.equals(other.getProductId()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.scheduledAssetQuantity==null && other.getScheduledAssetQuantity()==null) || 
             (this.scheduledAssetQuantity!=null &&
              this.scheduledAssetQuantity.equals(other.getScheduledAssetQuantity()))) &&
            ((this.scheduledProductQuantity==null && other.getScheduledProductQuantity()==null) || 
             (this.scheduledProductQuantity!=null &&
              this.scheduledProductQuantity.equals(other.getScheduledProductQuantity()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.taskLineTypeId==null && other.getTaskLineTypeId()==null) || 
             (this.taskLineTypeId!=null &&
              this.taskLineTypeId.equals(other.getTaskLineTypeId()))) &&
            ((this.taskLineTypeName==null && other.getTaskLineTypeName()==null) || 
             (this.taskLineTypeName!=null &&
              this.taskLineTypeName.equals(other.getTaskLineTypeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAssetTypeId() != null) {
            _hashCode += getAssetTypeId().hashCode();
        }
        if (getAssetTypeName() != null) {
            _hashCode += getAssetTypeName().hashCode();
        }
        if (getCoreTaskLineType() != null) {
            _hashCode += getCoreTaskLineType().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getIsSerialised() != null) {
            _hashCode += getIsSerialised().hashCode();
        }
        if (getProductDimension() != null) {
            _hashCode += getProductDimension().hashCode();
        }
        if (getProductId() != null) {
            _hashCode += getProductId().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getScheduledAssetQuantity() != null) {
            _hashCode += getScheduledAssetQuantity().hashCode();
        }
        if (getScheduledProductQuantity() != null) {
            _hashCode += getScheduledProductQuantity().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getTaskLineTypeId() != null) {
            _hashCode += getTaskLineTypeId().hashCode();
        }
        if (getTaskLineTypeName() != null) {
            _hashCode += getTaskLineTypeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceTaskLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "AssetTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "AssetTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coreTaskLineType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CoreTaskLineType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isSerialised");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "IsSerialised"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productDimension");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ProductDimension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ProductName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduledAssetQuantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduledAssetQuantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduledProductQuantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduledProductQuantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskLineTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskLineTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskLineTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskLineTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
