/**
 * ServiceTask.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceTask  extends com.placecube.digitalplace.local.waste.echo.axis.ExtensibleObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate;

    private com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskSchedule[] serviceTaskSchedules;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate;

    private java.lang.Integer taskTypeId;

    private java.lang.String taskTypeName;

    private com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskLine[] serviceTaskLines;

    private java.lang.String notes;

    private java.lang.Integer taskIndicatorId;

    private java.lang.String scheduleDescription;

    public ServiceTask() {
    }

    public ServiceTask(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate,
           com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskSchedule[] serviceTaskSchedules,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate,
           java.lang.Integer taskTypeId,
           java.lang.String taskTypeName,
           com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskLine[] serviceTaskLines,
           java.lang.String notes,
           java.lang.Integer taskIndicatorId,
           java.lang.String scheduleDescription) {
        super(
            guid,
            id,
            data);
        this.endDate = endDate;
        this.serviceTaskSchedules = serviceTaskSchedules;
        this.startDate = startDate;
        this.taskTypeId = taskTypeId;
        this.taskTypeName = taskTypeName;
        this.serviceTaskLines = serviceTaskLines;
        this.notes = notes;
        this.taskIndicatorId = taskIndicatorId;
        this.scheduleDescription = scheduleDescription;
    }


    /**
     * Gets the endDate value for this ServiceTask.
     * 
     * @return endDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this ServiceTask.
     * 
     * @param endDate
     */
    public void setEndDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the serviceTaskSchedules value for this ServiceTask.
     * 
     * @return serviceTaskSchedules
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskSchedule[] getServiceTaskSchedules() {
        return serviceTaskSchedules;
    }


    /**
     * Sets the serviceTaskSchedules value for this ServiceTask.
     * 
     * @param serviceTaskSchedules
     */
    public void setServiceTaskSchedules(com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskSchedule[] serviceTaskSchedules) {
        this.serviceTaskSchedules = serviceTaskSchedules;
    }


    /**
     * Gets the startDate value for this ServiceTask.
     * 
     * @return startDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this ServiceTask.
     * 
     * @param startDate
     */
    public void setStartDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the taskTypeId value for this ServiceTask.
     * 
     * @return taskTypeId
     */
    public java.lang.Integer getTaskTypeId() {
        return taskTypeId;
    }


    /**
     * Sets the taskTypeId value for this ServiceTask.
     * 
     * @param taskTypeId
     */
    public void setTaskTypeId(java.lang.Integer taskTypeId) {
        this.taskTypeId = taskTypeId;
    }


    /**
     * Gets the taskTypeName value for this ServiceTask.
     * 
     * @return taskTypeName
     */
    public java.lang.String getTaskTypeName() {
        return taskTypeName;
    }


    /**
     * Sets the taskTypeName value for this ServiceTask.
     * 
     * @param taskTypeName
     */
    public void setTaskTypeName(java.lang.String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }


    /**
     * Gets the serviceTaskLines value for this ServiceTask.
     * 
     * @return serviceTaskLines
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskLine[] getServiceTaskLines() {
        return serviceTaskLines;
    }


    /**
     * Sets the serviceTaskLines value for this ServiceTask.
     * 
     * @param serviceTaskLines
     */
    public void setServiceTaskLines(com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskLine[] serviceTaskLines) {
        this.serviceTaskLines = serviceTaskLines;
    }


    /**
     * Gets the notes value for this ServiceTask.
     * 
     * @return notes
     */
    public java.lang.String getNotes() {
        return notes;
    }


    /**
     * Sets the notes value for this ServiceTask.
     * 
     * @param notes
     */
    public void setNotes(java.lang.String notes) {
        this.notes = notes;
    }


    /**
     * Gets the taskIndicatorId value for this ServiceTask.
     * 
     * @return taskIndicatorId
     */
    public java.lang.Integer getTaskIndicatorId() {
        return taskIndicatorId;
    }


    /**
     * Sets the taskIndicatorId value for this ServiceTask.
     * 
     * @param taskIndicatorId
     */
    public void setTaskIndicatorId(java.lang.Integer taskIndicatorId) {
        this.taskIndicatorId = taskIndicatorId;
    }


    /**
     * Gets the scheduleDescription value for this ServiceTask.
     * 
     * @return scheduleDescription
     */
    public java.lang.String getScheduleDescription() {
        return scheduleDescription;
    }


    /**
     * Sets the scheduleDescription value for this ServiceTask.
     * 
     * @param scheduleDescription
     */
    public void setScheduleDescription(java.lang.String scheduleDescription) {
        this.scheduleDescription = scheduleDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceTask)) return false;
        ServiceTask other = (ServiceTask) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.serviceTaskSchedules==null && other.getServiceTaskSchedules()==null) || 
             (this.serviceTaskSchedules!=null &&
              java.util.Arrays.equals(this.serviceTaskSchedules, other.getServiceTaskSchedules()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.taskTypeId==null && other.getTaskTypeId()==null) || 
             (this.taskTypeId!=null &&
              this.taskTypeId.equals(other.getTaskTypeId()))) &&
            ((this.taskTypeName==null && other.getTaskTypeName()==null) || 
             (this.taskTypeName!=null &&
              this.taskTypeName.equals(other.getTaskTypeName()))) &&
            ((this.serviceTaskLines==null && other.getServiceTaskLines()==null) || 
             (this.serviceTaskLines!=null &&
              java.util.Arrays.equals(this.serviceTaskLines, other.getServiceTaskLines()))) &&
            ((this.notes==null && other.getNotes()==null) || 
             (this.notes!=null &&
              this.notes.equals(other.getNotes()))) &&
            ((this.taskIndicatorId==null && other.getTaskIndicatorId()==null) || 
             (this.taskIndicatorId!=null &&
              this.taskIndicatorId.equals(other.getTaskIndicatorId()))) &&
            ((this.scheduleDescription==null && other.getScheduleDescription()==null) || 
             (this.scheduleDescription!=null &&
              this.scheduleDescription.equals(other.getScheduleDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getServiceTaskSchedules() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServiceTaskSchedules());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServiceTaskSchedules(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getTaskTypeId() != null) {
            _hashCode += getTaskTypeId().hashCode();
        }
        if (getTaskTypeName() != null) {
            _hashCode += getTaskTypeName().hashCode();
        }
        if (getServiceTaskLines() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServiceTaskLines());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServiceTaskLines(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNotes() != null) {
            _hashCode += getNotes().hashCode();
        }
        if (getTaskIndicatorId() != null) {
            _hashCode += getTaskIndicatorId().hashCode();
        }
        if (getScheduleDescription() != null) {
            _hashCode += getScheduleDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceTask.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTask"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceTaskSchedules");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskSchedules"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskSchedule"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskSchedule"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceTaskLines");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskLines"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskLine"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskLine"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Notes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskIndicatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskIndicatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduleDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduleDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
