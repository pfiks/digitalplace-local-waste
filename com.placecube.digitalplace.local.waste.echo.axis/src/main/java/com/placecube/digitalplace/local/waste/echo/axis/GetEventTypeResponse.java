/**
 * GetEventTypeResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetEventTypeResponse  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.EventType getEventTypeResult;

    public GetEventTypeResponse() {
    }

    public GetEventTypeResponse(
           com.placecube.digitalplace.local.waste.echo.axis.EventType getEventTypeResult) {
           this.getEventTypeResult = getEventTypeResult;
    }


    /**
     * Gets the getEventTypeResult value for this GetEventTypeResponse.
     * 
     * @return getEventTypeResult
     */
    public com.placecube.digitalplace.local.waste.echo.axis.EventType getGetEventTypeResult() {
        return getEventTypeResult;
    }


    /**
     * Sets the getEventTypeResult value for this GetEventTypeResponse.
     * 
     * @param getEventTypeResult
     */
    public void setGetEventTypeResult(com.placecube.digitalplace.local.waste.echo.axis.EventType getEventTypeResult) {
        this.getEventTypeResult = getEventTypeResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEventTypeResponse)) return false;
        GetEventTypeResponse other = (GetEventTypeResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getEventTypeResult==null && other.getGetEventTypeResult()==null) || 
             (this.getEventTypeResult!=null &&
              this.getEventTypeResult.equals(other.getGetEventTypeResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetEventTypeResult() != null) {
            _hashCode += getGetEventTypeResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEventTypeResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventTypeResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getEventTypeResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventTypeResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
