/**
 * GetServiceTaskInstancesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetServiceTaskInstancesResponse  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[] getServiceTaskInstancesResult;

    public GetServiceTaskInstancesResponse() {
    }

    public GetServiceTaskInstancesResponse(
           com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[] getServiceTaskInstancesResult) {
           this.getServiceTaskInstancesResult = getServiceTaskInstancesResult;
    }


    /**
     * Gets the getServiceTaskInstancesResult value for this GetServiceTaskInstancesResponse.
     * 
     * @return getServiceTaskInstancesResult
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[] getGetServiceTaskInstancesResult() {
        return getServiceTaskInstancesResult;
    }


    /**
     * Sets the getServiceTaskInstancesResult value for this GetServiceTaskInstancesResponse.
     * 
     * @param getServiceTaskInstancesResult
     */
    public void setGetServiceTaskInstancesResult(com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[] getServiceTaskInstancesResult) {
        this.getServiceTaskInstancesResult = getServiceTaskInstancesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetServiceTaskInstancesResponse)) return false;
        GetServiceTaskInstancesResponse other = (GetServiceTaskInstancesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getServiceTaskInstancesResult==null && other.getGetServiceTaskInstancesResult()==null) || 
             (this.getServiceTaskInstancesResult!=null &&
              java.util.Arrays.equals(this.getServiceTaskInstancesResult, other.getGetServiceTaskInstancesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetServiceTaskInstancesResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetServiceTaskInstancesResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetServiceTaskInstancesResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetServiceTaskInstancesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServiceTaskInstancesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getServiceTaskInstancesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServiceTaskInstancesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstances"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstances"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
