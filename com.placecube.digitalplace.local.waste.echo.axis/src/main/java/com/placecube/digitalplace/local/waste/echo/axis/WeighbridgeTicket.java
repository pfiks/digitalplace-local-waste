/**
 * WeighbridgeTicket.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class WeighbridgeTicket  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.PartyInfo destinationParty;

    private com.placecube.digitalplace.local.waste.echo.axis.SiteInfo destinationSite;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endTime;

    private com.placecube.digitalplace.local.waste.echo.axis.PartyInfo haulierParty;

    private com.placecube.digitalplace.local.waste.echo.axis.SiteInfo haulierSite;

    private com.placecube.digitalplace.local.waste.echo.axis.PartyInfo instigatorParty;

    private java.lang.String reference;

    private com.placecube.digitalplace.local.waste.echo.axis.ResourceInfo resource;

    private com.placecube.digitalplace.local.waste.echo.axis.PartyInfo sourceParty;

    private com.placecube.digitalplace.local.waste.echo.axis.SiteInfo sourceSite;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startTime;

    private com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeStationInfo station;

    private com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLine[] ticketLines;

    private com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketTypeInfo ticketType;

    public WeighbridgeTicket() {
    }

    public WeighbridgeTicket(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.PartyInfo destinationParty,
           com.placecube.digitalplace.local.waste.echo.axis.SiteInfo destinationSite,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endTime,
           com.placecube.digitalplace.local.waste.echo.axis.PartyInfo haulierParty,
           com.placecube.digitalplace.local.waste.echo.axis.SiteInfo haulierSite,
           com.placecube.digitalplace.local.waste.echo.axis.PartyInfo instigatorParty,
           java.lang.String reference,
           com.placecube.digitalplace.local.waste.echo.axis.ResourceInfo resource,
           com.placecube.digitalplace.local.waste.echo.axis.PartyInfo sourceParty,
           com.placecube.digitalplace.local.waste.echo.axis.SiteInfo sourceSite,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startTime,
           com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeStationInfo station,
           com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLine[] ticketLines,
           com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketTypeInfo ticketType) {
        super(
            guid,
            id);
        this.destinationParty = destinationParty;
        this.destinationSite = destinationSite;
        this.endTime = endTime;
        this.haulierParty = haulierParty;
        this.haulierSite = haulierSite;
        this.instigatorParty = instigatorParty;
        this.reference = reference;
        this.resource = resource;
        this.sourceParty = sourceParty;
        this.sourceSite = sourceSite;
        this.startTime = startTime;
        this.station = station;
        this.ticketLines = ticketLines;
        this.ticketType = ticketType;
    }


    /**
     * Gets the destinationParty value for this WeighbridgeTicket.
     * 
     * @return destinationParty
     */
    public com.placecube.digitalplace.local.waste.echo.axis.PartyInfo getDestinationParty() {
        return destinationParty;
    }


    /**
     * Sets the destinationParty value for this WeighbridgeTicket.
     * 
     * @param destinationParty
     */
    public void setDestinationParty(com.placecube.digitalplace.local.waste.echo.axis.PartyInfo destinationParty) {
        this.destinationParty = destinationParty;
    }


    /**
     * Gets the destinationSite value for this WeighbridgeTicket.
     * 
     * @return destinationSite
     */
    public com.placecube.digitalplace.local.waste.echo.axis.SiteInfo getDestinationSite() {
        return destinationSite;
    }


    /**
     * Sets the destinationSite value for this WeighbridgeTicket.
     * 
     * @param destinationSite
     */
    public void setDestinationSite(com.placecube.digitalplace.local.waste.echo.axis.SiteInfo destinationSite) {
        this.destinationSite = destinationSite;
    }


    /**
     * Gets the endTime value for this WeighbridgeTicket.
     * 
     * @return endTime
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEndTime() {
        return endTime;
    }


    /**
     * Sets the endTime value for this WeighbridgeTicket.
     * 
     * @param endTime
     */
    public void setEndTime(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endTime) {
        this.endTime = endTime;
    }


    /**
     * Gets the haulierParty value for this WeighbridgeTicket.
     * 
     * @return haulierParty
     */
    public com.placecube.digitalplace.local.waste.echo.axis.PartyInfo getHaulierParty() {
        return haulierParty;
    }


    /**
     * Sets the haulierParty value for this WeighbridgeTicket.
     * 
     * @param haulierParty
     */
    public void setHaulierParty(com.placecube.digitalplace.local.waste.echo.axis.PartyInfo haulierParty) {
        this.haulierParty = haulierParty;
    }


    /**
     * Gets the haulierSite value for this WeighbridgeTicket.
     * 
     * @return haulierSite
     */
    public com.placecube.digitalplace.local.waste.echo.axis.SiteInfo getHaulierSite() {
        return haulierSite;
    }


    /**
     * Sets the haulierSite value for this WeighbridgeTicket.
     * 
     * @param haulierSite
     */
    public void setHaulierSite(com.placecube.digitalplace.local.waste.echo.axis.SiteInfo haulierSite) {
        this.haulierSite = haulierSite;
    }


    /**
     * Gets the instigatorParty value for this WeighbridgeTicket.
     * 
     * @return instigatorParty
     */
    public com.placecube.digitalplace.local.waste.echo.axis.PartyInfo getInstigatorParty() {
        return instigatorParty;
    }


    /**
     * Sets the instigatorParty value for this WeighbridgeTicket.
     * 
     * @param instigatorParty
     */
    public void setInstigatorParty(com.placecube.digitalplace.local.waste.echo.axis.PartyInfo instigatorParty) {
        this.instigatorParty = instigatorParty;
    }


    /**
     * Gets the reference value for this WeighbridgeTicket.
     * 
     * @return reference
     */
    public java.lang.String getReference() {
        return reference;
    }


    /**
     * Sets the reference value for this WeighbridgeTicket.
     * 
     * @param reference
     */
    public void setReference(java.lang.String reference) {
        this.reference = reference;
    }


    /**
     * Gets the resource value for this WeighbridgeTicket.
     * 
     * @return resource
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ResourceInfo getResource() {
        return resource;
    }


    /**
     * Sets the resource value for this WeighbridgeTicket.
     * 
     * @param resource
     */
    public void setResource(com.placecube.digitalplace.local.waste.echo.axis.ResourceInfo resource) {
        this.resource = resource;
    }


    /**
     * Gets the sourceParty value for this WeighbridgeTicket.
     * 
     * @return sourceParty
     */
    public com.placecube.digitalplace.local.waste.echo.axis.PartyInfo getSourceParty() {
        return sourceParty;
    }


    /**
     * Sets the sourceParty value for this WeighbridgeTicket.
     * 
     * @param sourceParty
     */
    public void setSourceParty(com.placecube.digitalplace.local.waste.echo.axis.PartyInfo sourceParty) {
        this.sourceParty = sourceParty;
    }


    /**
     * Gets the sourceSite value for this WeighbridgeTicket.
     * 
     * @return sourceSite
     */
    public com.placecube.digitalplace.local.waste.echo.axis.SiteInfo getSourceSite() {
        return sourceSite;
    }


    /**
     * Sets the sourceSite value for this WeighbridgeTicket.
     * 
     * @param sourceSite
     */
    public void setSourceSite(com.placecube.digitalplace.local.waste.echo.axis.SiteInfo sourceSite) {
        this.sourceSite = sourceSite;
    }


    /**
     * Gets the startTime value for this WeighbridgeTicket.
     * 
     * @return startTime
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getStartTime() {
        return startTime;
    }


    /**
     * Sets the startTime value for this WeighbridgeTicket.
     * 
     * @param startTime
     */
    public void setStartTime(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startTime) {
        this.startTime = startTime;
    }


    /**
     * Gets the station value for this WeighbridgeTicket.
     * 
     * @return station
     */
    public com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeStationInfo getStation() {
        return station;
    }


    /**
     * Sets the station value for this WeighbridgeTicket.
     * 
     * @param station
     */
    public void setStation(com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeStationInfo station) {
        this.station = station;
    }


    /**
     * Gets the ticketLines value for this WeighbridgeTicket.
     * 
     * @return ticketLines
     */
    public com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLine[] getTicketLines() {
        return ticketLines;
    }


    /**
     * Sets the ticketLines value for this WeighbridgeTicket.
     * 
     * @param ticketLines
     */
    public void setTicketLines(com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLine[] ticketLines) {
        this.ticketLines = ticketLines;
    }


    /**
     * Gets the ticketType value for this WeighbridgeTicket.
     * 
     * @return ticketType
     */
    public com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketTypeInfo getTicketType() {
        return ticketType;
    }


    /**
     * Sets the ticketType value for this WeighbridgeTicket.
     * 
     * @param ticketType
     */
    public void setTicketType(com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketTypeInfo ticketType) {
        this.ticketType = ticketType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WeighbridgeTicket)) return false;
        WeighbridgeTicket other = (WeighbridgeTicket) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.destinationParty==null && other.getDestinationParty()==null) || 
             (this.destinationParty!=null &&
              this.destinationParty.equals(other.getDestinationParty()))) &&
            ((this.destinationSite==null && other.getDestinationSite()==null) || 
             (this.destinationSite!=null &&
              this.destinationSite.equals(other.getDestinationSite()))) &&
            ((this.endTime==null && other.getEndTime()==null) || 
             (this.endTime!=null &&
              this.endTime.equals(other.getEndTime()))) &&
            ((this.haulierParty==null && other.getHaulierParty()==null) || 
             (this.haulierParty!=null &&
              this.haulierParty.equals(other.getHaulierParty()))) &&
            ((this.haulierSite==null && other.getHaulierSite()==null) || 
             (this.haulierSite!=null &&
              this.haulierSite.equals(other.getHaulierSite()))) &&
            ((this.instigatorParty==null && other.getInstigatorParty()==null) || 
             (this.instigatorParty!=null &&
              this.instigatorParty.equals(other.getInstigatorParty()))) &&
            ((this.reference==null && other.getReference()==null) || 
             (this.reference!=null &&
              this.reference.equals(other.getReference()))) &&
            ((this.resource==null && other.getResource()==null) || 
             (this.resource!=null &&
              this.resource.equals(other.getResource()))) &&
            ((this.sourceParty==null && other.getSourceParty()==null) || 
             (this.sourceParty!=null &&
              this.sourceParty.equals(other.getSourceParty()))) &&
            ((this.sourceSite==null && other.getSourceSite()==null) || 
             (this.sourceSite!=null &&
              this.sourceSite.equals(other.getSourceSite()))) &&
            ((this.startTime==null && other.getStartTime()==null) || 
             (this.startTime!=null &&
              this.startTime.equals(other.getStartTime()))) &&
            ((this.station==null && other.getStation()==null) || 
             (this.station!=null &&
              this.station.equals(other.getStation()))) &&
            ((this.ticketLines==null && other.getTicketLines()==null) || 
             (this.ticketLines!=null &&
              java.util.Arrays.equals(this.ticketLines, other.getTicketLines()))) &&
            ((this.ticketType==null && other.getTicketType()==null) || 
             (this.ticketType!=null &&
              this.ticketType.equals(other.getTicketType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDestinationParty() != null) {
            _hashCode += getDestinationParty().hashCode();
        }
        if (getDestinationSite() != null) {
            _hashCode += getDestinationSite().hashCode();
        }
        if (getEndTime() != null) {
            _hashCode += getEndTime().hashCode();
        }
        if (getHaulierParty() != null) {
            _hashCode += getHaulierParty().hashCode();
        }
        if (getHaulierSite() != null) {
            _hashCode += getHaulierSite().hashCode();
        }
        if (getInstigatorParty() != null) {
            _hashCode += getInstigatorParty().hashCode();
        }
        if (getReference() != null) {
            _hashCode += getReference().hashCode();
        }
        if (getResource() != null) {
            _hashCode += getResource().hashCode();
        }
        if (getSourceParty() != null) {
            _hashCode += getSourceParty().hashCode();
        }
        if (getSourceSite() != null) {
            _hashCode += getSourceSite().hashCode();
        }
        if (getStartTime() != null) {
            _hashCode += getStartTime().hashCode();
        }
        if (getStation() != null) {
            _hashCode += getStation().hashCode();
        }
        if (getTicketLines() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTicketLines());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTicketLines(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTicketType() != null) {
            _hashCode += getTicketType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WeighbridgeTicket.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicket"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinationParty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "DestinationParty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PartyInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinationSite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "DestinationSite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SiteInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EndTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("haulierParty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "HaulierParty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PartyInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("haulierSite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "HaulierSite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SiteInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instigatorParty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "InstigatorParty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PartyInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Reference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resource");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Resource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResourceInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceParty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SourceParty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PartyInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceSite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SourceSite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SiteInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StartTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("station");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Station"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeStationInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketLines");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TicketLines"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketLine"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketLine"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TicketType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketTypeInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
