/**
 * GetEventCountForObjectResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetEventCountForObjectResponse  implements java.io.Serializable {
    private java.lang.Integer getEventCountForObjectResult;

    public GetEventCountForObjectResponse() {
    }

    public GetEventCountForObjectResponse(
           java.lang.Integer getEventCountForObjectResult) {
           this.getEventCountForObjectResult = getEventCountForObjectResult;
    }


    /**
     * Gets the getEventCountForObjectResult value for this GetEventCountForObjectResponse.
     * 
     * @return getEventCountForObjectResult
     */
    public java.lang.Integer getGetEventCountForObjectResult() {
        return getEventCountForObjectResult;
    }


    /**
     * Sets the getEventCountForObjectResult value for this GetEventCountForObjectResponse.
     * 
     * @param getEventCountForObjectResult
     */
    public void setGetEventCountForObjectResult(java.lang.Integer getEventCountForObjectResult) {
        this.getEventCountForObjectResult = getEventCountForObjectResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEventCountForObjectResponse)) return false;
        GetEventCountForObjectResponse other = (GetEventCountForObjectResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getEventCountForObjectResult==null && other.getGetEventCountForObjectResult()==null) || 
             (this.getEventCountForObjectResult!=null &&
              this.getEventCountForObjectResult.equals(other.getGetEventCountForObjectResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetEventCountForObjectResult() != null) {
            _hashCode += getGetEventCountForObjectResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEventCountForObjectResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventCountForObjectResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getEventCountForObjectResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventCountForObjectResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
