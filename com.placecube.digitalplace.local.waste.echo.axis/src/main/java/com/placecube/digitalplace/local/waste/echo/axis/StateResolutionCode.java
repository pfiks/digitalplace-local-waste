/**
 * StateResolutionCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class StateResolutionCode  extends com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode  implements java.io.Serializable {
    private java.lang.Integer impactCode;

    private java.lang.Boolean isDefault;

    private java.lang.Integer resolutionCodeId;

    public StateResolutionCode() {
    }

    public StateResolutionCode(
           java.lang.String guid,
           java.lang.Integer id,
           java.lang.String name,
           java.lang.Integer impactCode,
           java.lang.Boolean isDefault,
           java.lang.Integer resolutionCodeId) {
        super(
            guid,
            id,
            name);
        this.impactCode = impactCode;
        this.isDefault = isDefault;
        this.resolutionCodeId = resolutionCodeId;
    }


    /**
     * Gets the impactCode value for this StateResolutionCode.
     * 
     * @return impactCode
     */
    public java.lang.Integer getImpactCode() {
        return impactCode;
    }


    /**
     * Sets the impactCode value for this StateResolutionCode.
     * 
     * @param impactCode
     */
    public void setImpactCode(java.lang.Integer impactCode) {
        this.impactCode = impactCode;
    }


    /**
     * Gets the isDefault value for this StateResolutionCode.
     * 
     * @return isDefault
     */
    public java.lang.Boolean getIsDefault() {
        return isDefault;
    }


    /**
     * Sets the isDefault value for this StateResolutionCode.
     * 
     * @param isDefault
     */
    public void setIsDefault(java.lang.Boolean isDefault) {
        this.isDefault = isDefault;
    }


    /**
     * Gets the resolutionCodeId value for this StateResolutionCode.
     * 
     * @return resolutionCodeId
     */
    public java.lang.Integer getResolutionCodeId() {
        return resolutionCodeId;
    }


    /**
     * Sets the resolutionCodeId value for this StateResolutionCode.
     * 
     * @param resolutionCodeId
     */
    public void setResolutionCodeId(java.lang.Integer resolutionCodeId) {
        this.resolutionCodeId = resolutionCodeId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StateResolutionCode)) return false;
        StateResolutionCode other = (StateResolutionCode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.impactCode==null && other.getImpactCode()==null) || 
             (this.impactCode!=null &&
              this.impactCode.equals(other.getImpactCode()))) &&
            ((this.isDefault==null && other.getIsDefault()==null) || 
             (this.isDefault!=null &&
              this.isDefault.equals(other.getIsDefault()))) &&
            ((this.resolutionCodeId==null && other.getResolutionCodeId()==null) || 
             (this.resolutionCodeId!=null &&
              this.resolutionCodeId.equals(other.getResolutionCodeId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getImpactCode() != null) {
            _hashCode += getImpactCode().hashCode();
        }
        if (getIsDefault() != null) {
            _hashCode += getIsDefault().hashCode();
        }
        if (getResolutionCodeId() != null) {
            _hashCode += getResolutionCodeId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StateResolutionCode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateResolutionCode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impactCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ImpactCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isDefault");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "IsDefault"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resolutionCodeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCodeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
