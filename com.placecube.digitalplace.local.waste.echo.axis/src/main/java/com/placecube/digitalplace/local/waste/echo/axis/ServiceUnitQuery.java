/**
 * ServiceUnitQuery.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceUnitQuery  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset from;

    private java.lang.Boolean includeTaskInstances;

    private int[] serviceIds;

    private int[] taskTypeIds;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset to;

    public ServiceUnitQuery() {
    }

    public ServiceUnitQuery(
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset from,
           java.lang.Boolean includeTaskInstances,
           int[] serviceIds,
           int[] taskTypeIds,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset to) {
           this.from = from;
           this.includeTaskInstances = includeTaskInstances;
           this.serviceIds = serviceIds;
           this.taskTypeIds = taskTypeIds;
           this.to = to;
    }


    /**
     * Gets the from value for this ServiceUnitQuery.
     * 
     * @return from
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getFrom() {
        return from;
    }


    /**
     * Sets the from value for this ServiceUnitQuery.
     * 
     * @param from
     */
    public void setFrom(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset from) {
        this.from = from;
    }


    /**
     * Gets the includeTaskInstances value for this ServiceUnitQuery.
     * 
     * @return includeTaskInstances
     */
    public java.lang.Boolean getIncludeTaskInstances() {
        return includeTaskInstances;
    }


    /**
     * Sets the includeTaskInstances value for this ServiceUnitQuery.
     * 
     * @param includeTaskInstances
     */
    public void setIncludeTaskInstances(java.lang.Boolean includeTaskInstances) {
        this.includeTaskInstances = includeTaskInstances;
    }


    /**
     * Gets the serviceIds value for this ServiceUnitQuery.
     * 
     * @return serviceIds
     */
    public int[] getServiceIds() {
        return serviceIds;
    }


    /**
     * Sets the serviceIds value for this ServiceUnitQuery.
     * 
     * @param serviceIds
     */
    public void setServiceIds(int[] serviceIds) {
        this.serviceIds = serviceIds;
    }


    /**
     * Gets the taskTypeIds value for this ServiceUnitQuery.
     * 
     * @return taskTypeIds
     */
    public int[] getTaskTypeIds() {
        return taskTypeIds;
    }


    /**
     * Sets the taskTypeIds value for this ServiceUnitQuery.
     * 
     * @param taskTypeIds
     */
    public void setTaskTypeIds(int[] taskTypeIds) {
        this.taskTypeIds = taskTypeIds;
    }


    /**
     * Gets the to value for this ServiceUnitQuery.
     * 
     * @return to
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getTo() {
        return to;
    }


    /**
     * Sets the to value for this ServiceUnitQuery.
     * 
     * @param to
     */
    public void setTo(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset to) {
        this.to = to;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceUnitQuery)) return false;
        ServiceUnitQuery other = (ServiceUnitQuery) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.from==null && other.getFrom()==null) || 
             (this.from!=null &&
              this.from.equals(other.getFrom()))) &&
            ((this.includeTaskInstances==null && other.getIncludeTaskInstances()==null) || 
             (this.includeTaskInstances!=null &&
              this.includeTaskInstances.equals(other.getIncludeTaskInstances()))) &&
            ((this.serviceIds==null && other.getServiceIds()==null) || 
             (this.serviceIds!=null &&
              java.util.Arrays.equals(this.serviceIds, other.getServiceIds()))) &&
            ((this.taskTypeIds==null && other.getTaskTypeIds()==null) || 
             (this.taskTypeIds!=null &&
              java.util.Arrays.equals(this.taskTypeIds, other.getTaskTypeIds()))) &&
            ((this.to==null && other.getTo()==null) || 
             (this.to!=null &&
              this.to.equals(other.getTo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFrom() != null) {
            _hashCode += getFrom().hashCode();
        }
        if (getIncludeTaskInstances() != null) {
            _hashCode += getIncludeTaskInstances().hashCode();
        }
        if (getServiceIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServiceIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServiceIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTaskTypeIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTaskTypeIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTaskTypeIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTo() != null) {
            _hashCode += getTo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceUnitQuery.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitQuery"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("from");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "From"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeTaskInstances");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "IncludeTaskInstances"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceIds");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskTypeIds");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskTypeIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("to");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "To"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
