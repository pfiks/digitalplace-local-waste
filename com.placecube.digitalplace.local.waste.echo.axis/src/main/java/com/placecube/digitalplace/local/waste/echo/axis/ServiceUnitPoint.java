/**
 * ServiceUnitPoint.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceUnitPoint  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate;

    private java.lang.Boolean isPointOfService;

    private java.lang.Boolean isServicedPoint;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef pointRef;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate;

    public ServiceUnitPoint() {
    }

    public ServiceUnitPoint(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate,
           java.lang.Boolean isPointOfService,
           java.lang.Boolean isServicedPoint,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef pointRef,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate) {
        super(
            guid,
            id);
        this.endDate = endDate;
        this.isPointOfService = isPointOfService;
        this.isServicedPoint = isServicedPoint;
        this.pointRef = pointRef;
        this.startDate = startDate;
    }


    /**
     * Gets the endDate value for this ServiceUnitPoint.
     * 
     * @return endDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this ServiceUnitPoint.
     * 
     * @param endDate
     */
    public void setEndDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the isPointOfService value for this ServiceUnitPoint.
     * 
     * @return isPointOfService
     */
    public java.lang.Boolean getIsPointOfService() {
        return isPointOfService;
    }


    /**
     * Sets the isPointOfService value for this ServiceUnitPoint.
     * 
     * @param isPointOfService
     */
    public void setIsPointOfService(java.lang.Boolean isPointOfService) {
        this.isPointOfService = isPointOfService;
    }


    /**
     * Gets the isServicedPoint value for this ServiceUnitPoint.
     * 
     * @return isServicedPoint
     */
    public java.lang.Boolean getIsServicedPoint() {
        return isServicedPoint;
    }


    /**
     * Sets the isServicedPoint value for this ServiceUnitPoint.
     * 
     * @param isServicedPoint
     */
    public void setIsServicedPoint(java.lang.Boolean isServicedPoint) {
        this.isServicedPoint = isServicedPoint;
    }


    /**
     * Gets the pointRef value for this ServiceUnitPoint.
     * 
     * @return pointRef
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getPointRef() {
        return pointRef;
    }


    /**
     * Sets the pointRef value for this ServiceUnitPoint.
     * 
     * @param pointRef
     */
    public void setPointRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef pointRef) {
        this.pointRef = pointRef;
    }


    /**
     * Gets the startDate value for this ServiceUnitPoint.
     * 
     * @return startDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this ServiceUnitPoint.
     * 
     * @param startDate
     */
    public void setStartDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate) {
        this.startDate = startDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceUnitPoint)) return false;
        ServiceUnitPoint other = (ServiceUnitPoint) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.isPointOfService==null && other.getIsPointOfService()==null) || 
             (this.isPointOfService!=null &&
              this.isPointOfService.equals(other.getIsPointOfService()))) &&
            ((this.isServicedPoint==null && other.getIsServicedPoint()==null) || 
             (this.isServicedPoint!=null &&
              this.isServicedPoint.equals(other.getIsServicedPoint()))) &&
            ((this.pointRef==null && other.getPointRef()==null) || 
             (this.pointRef!=null &&
              this.pointRef.equals(other.getPointRef()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getIsPointOfService() != null) {
            _hashCode += getIsPointOfService().hashCode();
        }
        if (getIsServicedPoint() != null) {
            _hashCode += getIsServicedPoint().hashCode();
        }
        if (getPointRef() != null) {
            _hashCode += getPointRef().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceUnitPoint.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitPoint"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPointOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "IsPointOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isServicedPoint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "IsServicedPoint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
