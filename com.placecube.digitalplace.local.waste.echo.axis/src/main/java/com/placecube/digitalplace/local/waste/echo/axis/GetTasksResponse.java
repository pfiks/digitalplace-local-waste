/**
 * GetTasksResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetTasksResponse  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.Task[] getTasksResult;

    public GetTasksResponse() {
    }

    public GetTasksResponse(
           com.placecube.digitalplace.local.waste.echo.axis.Task[] getTasksResult) {
           this.getTasksResult = getTasksResult;
    }


    /**
     * Gets the getTasksResult value for this GetTasksResponse.
     * 
     * @return getTasksResult
     */
    public com.placecube.digitalplace.local.waste.echo.axis.Task[] getGetTasksResult() {
        return getTasksResult;
    }


    /**
     * Sets the getTasksResult value for this GetTasksResponse.
     * 
     * @param getTasksResult
     */
    public void setGetTasksResult(com.placecube.digitalplace.local.waste.echo.axis.Task[] getTasksResult) {
        this.getTasksResult = getTasksResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTasksResponse)) return false;
        GetTasksResponse other = (GetTasksResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getTasksResult==null && other.getGetTasksResult()==null) || 
             (this.getTasksResult!=null &&
              java.util.Arrays.equals(this.getTasksResult, other.getGetTasksResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetTasksResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetTasksResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetTasksResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTasksResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTasksResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTasksResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTasksResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Task"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Task"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
