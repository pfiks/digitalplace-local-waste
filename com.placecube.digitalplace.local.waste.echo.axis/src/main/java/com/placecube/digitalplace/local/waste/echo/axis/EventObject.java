/**
 * EventObject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class EventObject  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private java.lang.String eventObjectType;

    private java.lang.String objectDescription;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef;

    private com.placecube.digitalplace.local.waste.echo.axis.GeoPoint location;

    public EventObject() {
    }

    public EventObject(
           java.lang.String guid,
           java.lang.Integer id,
           java.lang.String eventObjectType,
           java.lang.String objectDescription,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef,
           com.placecube.digitalplace.local.waste.echo.axis.GeoPoint location) {
        super(
            guid,
            id);
        this.eventObjectType = eventObjectType;
        this.objectDescription = objectDescription;
        this.objectRef = objectRef;
        this.location = location;
    }


    /**
     * Gets the eventObjectType value for this EventObject.
     * 
     * @return eventObjectType
     */
    public java.lang.String getEventObjectType() {
        return eventObjectType;
    }


    /**
     * Sets the eventObjectType value for this EventObject.
     * 
     * @param eventObjectType
     */
    public void setEventObjectType(java.lang.String eventObjectType) {
        this.eventObjectType = eventObjectType;
    }


    /**
     * Gets the objectDescription value for this EventObject.
     * 
     * @return objectDescription
     */
    public java.lang.String getObjectDescription() {
        return objectDescription;
    }


    /**
     * Sets the objectDescription value for this EventObject.
     * 
     * @param objectDescription
     */
    public void setObjectDescription(java.lang.String objectDescription) {
        this.objectDescription = objectDescription;
    }


    /**
     * Gets the objectRef value for this EventObject.
     * 
     * @return objectRef
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getObjectRef() {
        return objectRef;
    }


    /**
     * Sets the objectRef value for this EventObject.
     * 
     * @param objectRef
     */
    public void setObjectRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef) {
        this.objectRef = objectRef;
    }


    /**
     * Gets the location value for this EventObject.
     * 
     * @return location
     */
    public com.placecube.digitalplace.local.waste.echo.axis.GeoPoint getLocation() {
        return location;
    }


    /**
     * Sets the location value for this EventObject.
     * 
     * @param location
     */
    public void setLocation(com.placecube.digitalplace.local.waste.echo.axis.GeoPoint location) {
        this.location = location;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventObject)) return false;
        EventObject other = (EventObject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.eventObjectType==null && other.getEventObjectType()==null) || 
             (this.eventObjectType!=null &&
              this.eventObjectType.equals(other.getEventObjectType()))) &&
            ((this.objectDescription==null && other.getObjectDescription()==null) || 
             (this.objectDescription!=null &&
              this.objectDescription.equals(other.getObjectDescription()))) &&
            ((this.objectRef==null && other.getObjectRef()==null) || 
             (this.objectRef!=null &&
              this.objectRef.equals(other.getObjectRef()))) &&
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEventObjectType() != null) {
            _hashCode += getEventObjectType().hashCode();
        }
        if (getObjectDescription() != null) {
            _hashCode += getObjectDescription().hashCode();
        }
        if (getObjectRef() != null) {
            _hashCode += getObjectRef().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventObject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventObject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventObjectType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventObjectType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("objectDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("objectRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Location"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GeoPoint"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
