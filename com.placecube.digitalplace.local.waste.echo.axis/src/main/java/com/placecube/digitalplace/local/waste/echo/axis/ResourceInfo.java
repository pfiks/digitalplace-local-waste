/**
 * ResourceInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ResourceInfo  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private java.lang.String name;

    private java.lang.Integer resourceTypeId;

    private java.lang.String resourceTypeName;

    public ResourceInfo() {
    }

    public ResourceInfo(
           java.lang.String guid,
           java.lang.Integer id,
           java.lang.String name,
           java.lang.Integer resourceTypeId,
           java.lang.String resourceTypeName) {
        super(
            guid,
            id);
        this.name = name;
        this.resourceTypeId = resourceTypeId;
        this.resourceTypeName = resourceTypeName;
    }


    /**
     * Gets the name value for this ResourceInfo.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this ResourceInfo.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the resourceTypeId value for this ResourceInfo.
     * 
     * @return resourceTypeId
     */
    public java.lang.Integer getResourceTypeId() {
        return resourceTypeId;
    }


    /**
     * Sets the resourceTypeId value for this ResourceInfo.
     * 
     * @param resourceTypeId
     */
    public void setResourceTypeId(java.lang.Integer resourceTypeId) {
        this.resourceTypeId = resourceTypeId;
    }


    /**
     * Gets the resourceTypeName value for this ResourceInfo.
     * 
     * @return resourceTypeName
     */
    public java.lang.String getResourceTypeName() {
        return resourceTypeName;
    }


    /**
     * Sets the resourceTypeName value for this ResourceInfo.
     * 
     * @param resourceTypeName
     */
    public void setResourceTypeName(java.lang.String resourceTypeName) {
        this.resourceTypeName = resourceTypeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResourceInfo)) return false;
        ResourceInfo other = (ResourceInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.resourceTypeId==null && other.getResourceTypeId()==null) || 
             (this.resourceTypeId!=null &&
              this.resourceTypeId.equals(other.getResourceTypeId()))) &&
            ((this.resourceTypeName==null && other.getResourceTypeName()==null) || 
             (this.resourceTypeName!=null &&
              this.resourceTypeName.equals(other.getResourceTypeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getResourceTypeId() != null) {
            _hashCode += getResourceTypeId().hashCode();
        }
        if (getResourceTypeName() != null) {
            _hashCode += getResourceTypeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResourceInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResourceInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resourceTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResourceTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resourceTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResourceTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
