/**
 * TaskPointInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class TaskPointInfo  implements java.io.Serializable {
    private java.lang.String description;

    private java.lang.Boolean isPointOfService;

    private java.lang.Boolean isServicedPoint;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] pointRefs;

    public TaskPointInfo() {
    }

    public TaskPointInfo(
           java.lang.String description,
           java.lang.Boolean isPointOfService,
           java.lang.Boolean isServicedPoint,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] pointRefs) {
           this.description = description;
           this.isPointOfService = isPointOfService;
           this.isServicedPoint = isServicedPoint;
           this.pointRefs = pointRefs;
    }


    /**
     * Gets the description value for this TaskPointInfo.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this TaskPointInfo.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the isPointOfService value for this TaskPointInfo.
     * 
     * @return isPointOfService
     */
    public java.lang.Boolean getIsPointOfService() {
        return isPointOfService;
    }


    /**
     * Sets the isPointOfService value for this TaskPointInfo.
     * 
     * @param isPointOfService
     */
    public void setIsPointOfService(java.lang.Boolean isPointOfService) {
        this.isPointOfService = isPointOfService;
    }


    /**
     * Gets the isServicedPoint value for this TaskPointInfo.
     * 
     * @return isServicedPoint
     */
    public java.lang.Boolean getIsServicedPoint() {
        return isServicedPoint;
    }


    /**
     * Sets the isServicedPoint value for this TaskPointInfo.
     * 
     * @param isServicedPoint
     */
    public void setIsServicedPoint(java.lang.Boolean isServicedPoint) {
        this.isServicedPoint = isServicedPoint;
    }


    /**
     * Gets the pointRefs value for this TaskPointInfo.
     * 
     * @return pointRefs
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] getPointRefs() {
        return pointRefs;
    }


    /**
     * Sets the pointRefs value for this TaskPointInfo.
     * 
     * @param pointRefs
     */
    public void setPointRefs(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] pointRefs) {
        this.pointRefs = pointRefs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TaskPointInfo)) return false;
        TaskPointInfo other = (TaskPointInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.isPointOfService==null && other.getIsPointOfService()==null) || 
             (this.isPointOfService!=null &&
              this.isPointOfService.equals(other.getIsPointOfService()))) &&
            ((this.isServicedPoint==null && other.getIsServicedPoint()==null) || 
             (this.isServicedPoint!=null &&
              this.isServicedPoint.equals(other.getIsServicedPoint()))) &&
            ((this.pointRefs==null && other.getPointRefs()==null) || 
             (this.pointRefs!=null &&
              java.util.Arrays.equals(this.pointRefs, other.getPointRefs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getIsPointOfService() != null) {
            _hashCode += getIsPointOfService().hashCode();
        }
        if (getIsServicedPoint() != null) {
            _hashCode += getIsServicedPoint().hashCode();
        }
        if (getPointRefs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPointRefs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPointRefs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TaskPointInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskPointInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPointOfService");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "IsPointOfService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isServicedPoint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "IsServicedPoint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointRefs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointRefs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
