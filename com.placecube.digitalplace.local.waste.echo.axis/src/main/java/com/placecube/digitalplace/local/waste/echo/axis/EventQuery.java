/**
 * EventQuery.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class EventQuery  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef eventTypeRef;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset from;

    public EventQuery() {
    }

    public EventQuery(
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef eventTypeRef,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset from) {
           this.eventTypeRef = eventTypeRef;
           this.from = from;
    }


    /**
     * Gets the eventTypeRef value for this EventQuery.
     * 
     * @return eventTypeRef
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getEventTypeRef() {
        return eventTypeRef;
    }


    /**
     * Sets the eventTypeRef value for this EventQuery.
     * 
     * @param eventTypeRef
     */
    public void setEventTypeRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef eventTypeRef) {
        this.eventTypeRef = eventTypeRef;
    }


    /**
     * Gets the from value for this EventQuery.
     * 
     * @return from
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getFrom() {
        return from;
    }


    /**
     * Sets the from value for this EventQuery.
     * 
     * @param from
     */
    public void setFrom(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset from) {
        this.from = from;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventQuery)) return false;
        EventQuery other = (EventQuery) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.eventTypeRef==null && other.getEventTypeRef()==null) || 
             (this.eventTypeRef!=null &&
              this.eventTypeRef.equals(other.getEventTypeRef()))) &&
            ((this.from==null && other.getFrom()==null) || 
             (this.from!=null &&
              this.from.equals(other.getFrom())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEventTypeRef() != null) {
            _hashCode += getEventTypeRef().hashCode();
        }
        if (getFrom() != null) {
            _hashCode += getFrom().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventQuery.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventQuery"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventTypeRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventTypeRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("from");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "From"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
