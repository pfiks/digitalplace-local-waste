/**
 * Service_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public interface Service_PortType extends java.rmi.Remote {
    public com.placecube.digitalplace.local.waste.echo.axis.CodeFile getCodeFile(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Code getCode(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[] getResolutionCodes() throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Street[] findStreets(com.placecube.digitalplace.local.waste.echo.axis.StreetQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Street getStreet(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.PointInfo[] findPoints(com.placecube.digitalplace.local.waste.echo.axis.PointQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Point getPoint(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.PointAddress getPointAddress(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Point[] getPointSegmentsForStreet(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef streetRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.EventType getEventType(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.EventType[] getEventTypesForService(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef serviceRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.PostEventResult postEvent(com.placecube.digitalplace.local.waste.echo.axis.Event event) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.PerformEventActionResult performEventAction(com.placecube.digitalplace.local.waste.echo.axis.EventAction action) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Event[] getEventsForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef, com.placecube.digitalplace.local.waste.echo.axis.EventQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Event getEvent(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Inspection getInspection(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceSummary[] getServiceSummaryForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public java.lang.Integer getEventCountForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef, com.placecube.digitalplace.local.waste.echo.axis.EventQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.ReservedTaskInfo[] reserveAvailableSlotsForEvent(com.placecube.digitalplace.local.waste.echo.axis.Event event, com.placecube.digitalplace.local.waste.echo.axis.ReservationParameters parameters) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public void cancelReservedSlotsForEvent(java.lang.String eventGuid) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[] getPointAddressTypes() throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceGroup[] getServicesForContract(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef contractRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.TaskType getTaskType(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.TaskType[] getTaskTypesForService(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef serviceRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit[] getServiceUnitsForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef, com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[] getServiceTaskInstances(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] serviceTaskRefs, com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstanceQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Task[] getTasks(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] taskRefs, com.placecube.digitalplace.local.waste.echo.axis.TaskOptions options) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.Task[] getTasksForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef, com.placecube.digitalplace.local.waste.echo.axis.TaskOptions options) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public void addSubscription(com.placecube.digitalplace.local.waste.echo.axis.SubscriptionParameters parameters) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
    public com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicket getWeighbridgeTicket(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault;
}
