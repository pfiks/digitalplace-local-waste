/**
 * Service_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public interface Service_Service extends javax.xml.rpc.Service {
    public java.lang.String getServiceHttpsAddress();

    public com.placecube.digitalplace.local.waste.echo.axis.Service_PortType getServiceHttps() throws javax.xml.rpc.ServiceException;

    public com.placecube.digitalplace.local.waste.echo.axis.Service_PortType getServiceHttps(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
