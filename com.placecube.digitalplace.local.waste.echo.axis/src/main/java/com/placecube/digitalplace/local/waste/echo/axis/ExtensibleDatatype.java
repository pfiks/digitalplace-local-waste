/**
 * ExtensibleDatatype.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ExtensibleDatatype  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatatype[] childDatatypes;

    private java.lang.String coreDatatype;

    private java.lang.Boolean isArray;

    private java.lang.String name;

    private java.lang.String valueDomain;

    private java.lang.String valueDomainType;

    public ExtensibleDatatype() {
    }

    public ExtensibleDatatype(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatatype[] childDatatypes,
           java.lang.String coreDatatype,
           java.lang.Boolean isArray,
           java.lang.String name,
           java.lang.String valueDomain,
           java.lang.String valueDomainType) {
        super(
            guid,
            id);
        this.childDatatypes = childDatatypes;
        this.coreDatatype = coreDatatype;
        this.isArray = isArray;
        this.name = name;
        this.valueDomain = valueDomain;
        this.valueDomainType = valueDomainType;
    }


    /**
     * Gets the childDatatypes value for this ExtensibleDatatype.
     * 
     * @return childDatatypes
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatatype[] getChildDatatypes() {
        return childDatatypes;
    }


    /**
     * Sets the childDatatypes value for this ExtensibleDatatype.
     * 
     * @param childDatatypes
     */
    public void setChildDatatypes(com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatatype[] childDatatypes) {
        this.childDatatypes = childDatatypes;
    }


    /**
     * Gets the coreDatatype value for this ExtensibleDatatype.
     * 
     * @return coreDatatype
     */
    public java.lang.String getCoreDatatype() {
        return coreDatatype;
    }


    /**
     * Sets the coreDatatype value for this ExtensibleDatatype.
     * 
     * @param coreDatatype
     */
    public void setCoreDatatype(java.lang.String coreDatatype) {
        this.coreDatatype = coreDatatype;
    }


    /**
     * Gets the isArray value for this ExtensibleDatatype.
     * 
     * @return isArray
     */
    public java.lang.Boolean getIsArray() {
        return isArray;
    }


    /**
     * Sets the isArray value for this ExtensibleDatatype.
     * 
     * @param isArray
     */
    public void setIsArray(java.lang.Boolean isArray) {
        this.isArray = isArray;
    }


    /**
     * Gets the name value for this ExtensibleDatatype.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this ExtensibleDatatype.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the valueDomain value for this ExtensibleDatatype.
     * 
     * @return valueDomain
     */
    public java.lang.String getValueDomain() {
        return valueDomain;
    }


    /**
     * Sets the valueDomain value for this ExtensibleDatatype.
     * 
     * @param valueDomain
     */
    public void setValueDomain(java.lang.String valueDomain) {
        this.valueDomain = valueDomain;
    }


    /**
     * Gets the valueDomainType value for this ExtensibleDatatype.
     * 
     * @return valueDomainType
     */
    public java.lang.String getValueDomainType() {
        return valueDomainType;
    }


    /**
     * Sets the valueDomainType value for this ExtensibleDatatype.
     * 
     * @param valueDomainType
     */
    public void setValueDomainType(java.lang.String valueDomainType) {
        this.valueDomainType = valueDomainType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExtensibleDatatype)) return false;
        ExtensibleDatatype other = (ExtensibleDatatype) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.childDatatypes==null && other.getChildDatatypes()==null) || 
             (this.childDatatypes!=null &&
              java.util.Arrays.equals(this.childDatatypes, other.getChildDatatypes()))) &&
            ((this.coreDatatype==null && other.getCoreDatatype()==null) || 
             (this.coreDatatype!=null &&
              this.coreDatatype.equals(other.getCoreDatatype()))) &&
            ((this.isArray==null && other.getIsArray()==null) || 
             (this.isArray!=null &&
              this.isArray.equals(other.getIsArray()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.valueDomain==null && other.getValueDomain()==null) || 
             (this.valueDomain!=null &&
              this.valueDomain.equals(other.getValueDomain()))) &&
            ((this.valueDomainType==null && other.getValueDomainType()==null) || 
             (this.valueDomainType!=null &&
              this.valueDomainType.equals(other.getValueDomainType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getChildDatatypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getChildDatatypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getChildDatatypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCoreDatatype() != null) {
            _hashCode += getCoreDatatype().hashCode();
        }
        if (getIsArray() != null) {
            _hashCode += getIsArray().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getValueDomain() != null) {
            _hashCode += getValueDomain().hashCode();
        }
        if (getValueDomainType() != null) {
            _hashCode += getValueDomainType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExtensibleDatatype.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatatype"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("childDatatypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ChildDatatypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatatype"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatatype"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coreDatatype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CoreDatatype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isArray");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "IsArray"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valueDomain");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ValueDomain"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valueDomainType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ValueDomainType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
