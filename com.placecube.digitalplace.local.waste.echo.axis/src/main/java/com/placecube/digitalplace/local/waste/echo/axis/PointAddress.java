/**
 * PointAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class PointAddress  extends com.placecube.digitalplace.local.waste.echo.axis.Point  implements java.io.Serializable {
    private java.lang.Integer llpgLogicalStatus;

    private com.placecube.digitalplace.local.waste.echo.axis.PointAddressType pointAddressType;

    private java.lang.Integer pointSegmentId;

    private java.lang.Integer streetId;

    public PointAddress() {
    }

    public PointAddress(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data,
           com.placecube.digitalplace.local.waste.echo.axis.GeoPoint[] coordinates,
           java.lang.String description,
           java.lang.String pointType,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef sharedRef,
           java.lang.Integer llpgLogicalStatus,
           com.placecube.digitalplace.local.waste.echo.axis.PointAddressType pointAddressType,
           java.lang.Integer pointSegmentId,
           java.lang.Integer streetId) {
        super(
            guid,
            id,
            data,
            coordinates,
            description,
            pointType,
            sharedRef);
        this.llpgLogicalStatus = llpgLogicalStatus;
        this.pointAddressType = pointAddressType;
        this.pointSegmentId = pointSegmentId;
        this.streetId = streetId;
    }


    /**
     * Gets the llpgLogicalStatus value for this PointAddress.
     * 
     * @return llpgLogicalStatus
     */
    public java.lang.Integer getLlpgLogicalStatus() {
        return llpgLogicalStatus;
    }


    /**
     * Sets the llpgLogicalStatus value for this PointAddress.
     * 
     * @param llpgLogicalStatus
     */
    public void setLlpgLogicalStatus(java.lang.Integer llpgLogicalStatus) {
        this.llpgLogicalStatus = llpgLogicalStatus;
    }


    /**
     * Gets the pointAddressType value for this PointAddress.
     * 
     * @return pointAddressType
     */
    public com.placecube.digitalplace.local.waste.echo.axis.PointAddressType getPointAddressType() {
        return pointAddressType;
    }


    /**
     * Sets the pointAddressType value for this PointAddress.
     * 
     * @param pointAddressType
     */
    public void setPointAddressType(com.placecube.digitalplace.local.waste.echo.axis.PointAddressType pointAddressType) {
        this.pointAddressType = pointAddressType;
    }


    /**
     * Gets the pointSegmentId value for this PointAddress.
     * 
     * @return pointSegmentId
     */
    public java.lang.Integer getPointSegmentId() {
        return pointSegmentId;
    }


    /**
     * Sets the pointSegmentId value for this PointAddress.
     * 
     * @param pointSegmentId
     */
    public void setPointSegmentId(java.lang.Integer pointSegmentId) {
        this.pointSegmentId = pointSegmentId;
    }


    /**
     * Gets the streetId value for this PointAddress.
     * 
     * @return streetId
     */
    public java.lang.Integer getStreetId() {
        return streetId;
    }


    /**
     * Sets the streetId value for this PointAddress.
     * 
     * @param streetId
     */
    public void setStreetId(java.lang.Integer streetId) {
        this.streetId = streetId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PointAddress)) return false;
        PointAddress other = (PointAddress) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.llpgLogicalStatus==null && other.getLlpgLogicalStatus()==null) || 
             (this.llpgLogicalStatus!=null &&
              this.llpgLogicalStatus.equals(other.getLlpgLogicalStatus()))) &&
            ((this.pointAddressType==null && other.getPointAddressType()==null) || 
             (this.pointAddressType!=null &&
              this.pointAddressType.equals(other.getPointAddressType()))) &&
            ((this.pointSegmentId==null && other.getPointSegmentId()==null) || 
             (this.pointSegmentId!=null &&
              this.pointSegmentId.equals(other.getPointSegmentId()))) &&
            ((this.streetId==null && other.getStreetId()==null) || 
             (this.streetId!=null &&
              this.streetId.equals(other.getStreetId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLlpgLogicalStatus() != null) {
            _hashCode += getLlpgLogicalStatus().hashCode();
        }
        if (getPointAddressType() != null) {
            _hashCode += getPointAddressType().hashCode();
        }
        if (getPointSegmentId() != null) {
            _hashCode += getPointSegmentId().hashCode();
        }
        if (getStreetId() != null) {
            _hashCode += getStreetId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PointAddress.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddress"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("llpgLogicalStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "LlpgLogicalStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointAddressType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddressType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddressType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointSegmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointSegmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("streetId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StreetId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
