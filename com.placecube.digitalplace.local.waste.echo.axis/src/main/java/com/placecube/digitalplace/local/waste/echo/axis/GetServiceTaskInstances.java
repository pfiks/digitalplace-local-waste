/**
 * GetServiceTaskInstances.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetServiceTaskInstances  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] serviceTaskRefs;

    private com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstanceQuery query;

    public GetServiceTaskInstances() {
    }

    public GetServiceTaskInstances(
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] serviceTaskRefs,
           com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstanceQuery query) {
           this.serviceTaskRefs = serviceTaskRefs;
           this.query = query;
    }


    /**
     * Gets the serviceTaskRefs value for this GetServiceTaskInstances.
     * 
     * @return serviceTaskRefs
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] getServiceTaskRefs() {
        return serviceTaskRefs;
    }


    /**
     * Sets the serviceTaskRefs value for this GetServiceTaskInstances.
     * 
     * @param serviceTaskRefs
     */
    public void setServiceTaskRefs(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] serviceTaskRefs) {
        this.serviceTaskRefs = serviceTaskRefs;
    }


    /**
     * Gets the query value for this GetServiceTaskInstances.
     * 
     * @return query
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstanceQuery getQuery() {
        return query;
    }


    /**
     * Sets the query value for this GetServiceTaskInstances.
     * 
     * @param query
     */
    public void setQuery(com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstanceQuery query) {
        this.query = query;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetServiceTaskInstances)) return false;
        GetServiceTaskInstances other = (GetServiceTaskInstances) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.serviceTaskRefs==null && other.getServiceTaskRefs()==null) || 
             (this.serviceTaskRefs!=null &&
              java.util.Arrays.equals(this.serviceTaskRefs, other.getServiceTaskRefs()))) &&
            ((this.query==null && other.getQuery()==null) || 
             (this.query!=null &&
              this.query.equals(other.getQuery())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getServiceTaskRefs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServiceTaskRefs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServiceTaskRefs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getQuery() != null) {
            _hashCode += getQuery().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetServiceTaskInstances.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServiceTaskInstances"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceTaskRefs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "serviceTaskRefs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("query");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "query"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstanceQuery"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
