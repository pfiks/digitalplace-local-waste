/**
 * GetPointSegmentsForStreet.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetPointSegmentsForStreet  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef streetRef;

    public GetPointSegmentsForStreet() {
    }

    public GetPointSegmentsForStreet(
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef streetRef) {
           this.streetRef = streetRef;
    }


    /**
     * Gets the streetRef value for this GetPointSegmentsForStreet.
     * 
     * @return streetRef
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getStreetRef() {
        return streetRef;
    }


    /**
     * Sets the streetRef value for this GetPointSegmentsForStreet.
     * 
     * @param streetRef
     */
    public void setStreetRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef streetRef) {
        this.streetRef = streetRef;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPointSegmentsForStreet)) return false;
        GetPointSegmentsForStreet other = (GetPointSegmentsForStreet) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.streetRef==null && other.getStreetRef()==null) || 
             (this.streetRef!=null &&
              this.streetRef.equals(other.getStreetRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStreetRef() != null) {
            _hashCode += getStreetRef().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPointSegmentsForStreet.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointSegmentsForStreet"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("streetRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "streetRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
