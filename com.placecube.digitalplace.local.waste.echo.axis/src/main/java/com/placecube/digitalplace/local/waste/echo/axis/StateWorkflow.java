/**
 * StateWorkflow.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class StateWorkflow  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private java.lang.Boolean hasActions;

    private com.placecube.digitalplace.local.waste.echo.axis.State[] states;

    private java.lang.Boolean useNextStates;

    public StateWorkflow() {
    }

    public StateWorkflow(
           java.lang.String guid,
           java.lang.Integer id,
           java.lang.Boolean hasActions,
           com.placecube.digitalplace.local.waste.echo.axis.State[] states,
           java.lang.Boolean useNextStates) {
        super(
            guid,
            id);
        this.hasActions = hasActions;
        this.states = states;
        this.useNextStates = useNextStates;
    }


    /**
     * Gets the hasActions value for this StateWorkflow.
     * 
     * @return hasActions
     */
    public java.lang.Boolean getHasActions() {
        return hasActions;
    }


    /**
     * Sets the hasActions value for this StateWorkflow.
     * 
     * @param hasActions
     */
    public void setHasActions(java.lang.Boolean hasActions) {
        this.hasActions = hasActions;
    }


    /**
     * Gets the states value for this StateWorkflow.
     * 
     * @return states
     */
    public com.placecube.digitalplace.local.waste.echo.axis.State[] getStates() {
        return states;
    }


    /**
     * Sets the states value for this StateWorkflow.
     * 
     * @param states
     */
    public void setStates(com.placecube.digitalplace.local.waste.echo.axis.State[] states) {
        this.states = states;
    }


    /**
     * Gets the useNextStates value for this StateWorkflow.
     * 
     * @return useNextStates
     */
    public java.lang.Boolean getUseNextStates() {
        return useNextStates;
    }


    /**
     * Sets the useNextStates value for this StateWorkflow.
     * 
     * @param useNextStates
     */
    public void setUseNextStates(java.lang.Boolean useNextStates) {
        this.useNextStates = useNextStates;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StateWorkflow)) return false;
        StateWorkflow other = (StateWorkflow) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.hasActions==null && other.getHasActions()==null) || 
             (this.hasActions!=null &&
              this.hasActions.equals(other.getHasActions()))) &&
            ((this.states==null && other.getStates()==null) || 
             (this.states!=null &&
              java.util.Arrays.equals(this.states, other.getStates()))) &&
            ((this.useNextStates==null && other.getUseNextStates()==null) || 
             (this.useNextStates!=null &&
              this.useNextStates.equals(other.getUseNextStates())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getHasActions() != null) {
            _hashCode += getHasActions().hashCode();
        }
        if (getStates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUseNextStates() != null) {
            _hashCode += getUseNextStates().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StateWorkflow.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateWorkflow"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hasActions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "HasActions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("states");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "States"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "State"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "State"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("useNextStates");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "UseNextStates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
