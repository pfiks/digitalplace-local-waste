/**
 * ServiceHttpsStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceHttpsStub extends org.apache.axis.client.Stub implements com.placecube.digitalplace.local.waste.echo.axis.Service_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[30];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCodeFile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CodeFile"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.CodeFile.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetCodeFileResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCode");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Code"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Code.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetCodeResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetResolutionCodes");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfResolutionCode"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetResolutionCodesResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCode"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("FindStreets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "query"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StreetQuery"), com.placecube.digitalplace.local.waste.echo.axis.StreetQuery.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfStreet"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Street[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "FindStreetsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Street"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetStreet");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Street"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Street.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetStreetResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("FindPoints");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "query"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointQuery"), com.placecube.digitalplace.local.waste.echo.axis.PointQuery.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfPointInfo"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.PointInfo[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "FindPointsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointInfo"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPoint");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Point"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Point.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPointResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPointAddress");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddress"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.PointAddress.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPointAddressResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPointSegmentsForStreet");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "streetRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfPoint"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Point[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPointSegmentsForStreetResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Point"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetEventType");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.EventType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventTypeResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetEventTypesForService");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "serviceRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfEventType"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.EventType[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventTypesForServiceResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PostEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "event"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event"), com.placecube.digitalplace.local.waste.echo.axis.Event.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PostEventResult"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.PostEventResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PostEventResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PerformEventAction");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "action"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventAction"), com.placecube.digitalplace.local.waste.echo.axis.EventAction.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PerformEventActionResult"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.PerformEventActionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PerformEventActionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetEventsForObject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "objectRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "query"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventQuery"), com.placecube.digitalplace.local.waste.echo.axis.EventQuery.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfEvent"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Event[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventsForObjectResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Event.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetInspection");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Inspection"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Inspection.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetInspectionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetServiceSummaryForObject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "objectRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceSummary"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.ServiceSummary[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServiceSummaryForObjectResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceSummary"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetEventCountForObject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "objectRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "query"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventQuery"), com.placecube.digitalplace.local.waste.echo.axis.EventQuery.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        oper.setReturnClass(java.lang.Integer.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventCountForObjectResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ReserveAvailableSlotsForEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "event"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event"), com.placecube.digitalplace.local.waste.echo.axis.Event.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservationParameters"), com.placecube.digitalplace.local.waste.echo.axis.ReservationParameters.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfReservedTaskInfo"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.ReservedTaskInfo[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReserveAvailableSlotsForEventResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservedTaskInfo"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CancelReservedSlotsForEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "eventGuid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPointAddressTypes");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfPointAddressType"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPointAddressTypesResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddressType"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetServicesForContract");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "contractRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceGroup"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.ServiceGroup[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServicesForContractResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceGroup"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTaskType");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskType"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.TaskType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTaskTypeResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTaskTypesForService");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "serviceRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfTaskType"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.TaskType[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTaskTypesForServiceResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskType"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetServiceUnitsForObject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "objectRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "query"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitQuery"), com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitQuery.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceUnit"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServiceUnitsForObjectResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnit"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetServiceTaskInstances");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "serviceTaskRefs"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "query"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstanceQuery"), com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstanceQuery.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceTaskInstances"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServiceTaskInstancesResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstances"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTasks");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "taskRefs"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "options"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskOptions"), com.placecube.digitalplace.local.waste.echo.axis.TaskOptions.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfTask"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Task[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTasksResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Task"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTasksForObject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "objectRef"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "options"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskOptions"), com.placecube.digitalplace.local.waste.echo.axis.TaskOptions.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfTask"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.Task[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTasksForObjectResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Task"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddSubscription");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SubscriptionParameters"), com.placecube.digitalplace.local.waste.echo.axis.SubscriptionParameters.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetWeighbridgeTicket");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ref"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"), com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicket"));
        oper.setReturnClass(com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicket.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetWeighbridgeTicketResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"),
                      "com.twistedfish.www.xmlns.echo.api.v1.EchoApiFault",
                      new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault"), 
                      true
                     ));
        _operations[29] = oper;

    }

    public ServiceHttpsStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public ServiceHttpsStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public ServiceHttpsStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfanyType");
            cachedSerQNames.add(qName);
            cls = java.lang.Object[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "anyType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfint");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfanyType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
            cachedSerQNames.add(qName);
            cls = org.apache.axis.types.Duration.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">AddSubscription");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.AddSubscription.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">AddSubscriptionResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.AddSubscriptionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">CancelReservedSlotsForEvent");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.CancelReservedSlotsForEvent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">CancelReservedSlotsForEventResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.CancelReservedSlotsForEventResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">FindPoints");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.FindPoints.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">FindPointsResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.FindPointsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">FindStreets");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.FindStreets.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">FindStreetsResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.FindStreetsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetCode");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetCodeFile");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetCodeFile.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetCodeFileResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetCodeFileResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetCodeResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetCodeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEvent");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEvent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventCountForObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventCountForObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventCountForObjectResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventCountForObjectResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventsForObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventsForObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventsForObjectResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventsForObjectResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventTypeResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventTypeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventTypesForService");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventTypesForService.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetEventTypesForServiceResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetEventTypesForServiceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetInspection");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetInspection.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetInspectionResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetInspectionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPoint");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetPoint.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointAddress");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetPointAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointAddressResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetPointAddressResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointAddressTypes");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetPointAddressTypes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointAddressTypesResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetPointAddressTypesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetPointResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointSegmentsForStreet");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetPointSegmentsForStreet.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointSegmentsForStreetResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetPointSegmentsForStreetResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetResolutionCodes");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetResolutionCodes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetResolutionCodesResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetResolutionCodesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServicesForContract");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetServicesForContract.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServicesForContractResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetServicesForContractResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServiceSummaryForObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetServiceSummaryForObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServiceSummaryForObjectResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetServiceSummaryForObjectResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServiceTaskInstances");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetServiceTaskInstances.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServiceTaskInstancesResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetServiceTaskInstancesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServiceUnitsForObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetServiceUnitsForObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetServiceUnitsForObjectResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetServiceUnitsForObjectResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetStreet");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetStreet.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetStreetResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetStreetResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTasks");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetTasks.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTasksForObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetTasksForObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTasksForObjectResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetTasksForObjectResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTasksResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetTasksResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTaskType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetTaskType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTaskTypeResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetTaskTypeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTaskTypesForService");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetTaskTypesForService.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTaskTypesForServiceResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetTaskTypesForServiceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetWeighbridgeTicket");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetWeighbridgeTicket.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetWeighbridgeTicketResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GetWeighbridgeTicketResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">PerformEventAction");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PerformEventAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">PerformEventActionResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PerformEventActionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">PostEvent");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PostEvent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">PostEventResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PostEventResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">ReserveAvailableSlotsForEvent");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ReserveAvailableSlotsForEvent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">ReserveAvailableSlotsForEventResponse");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ReserveAvailableSlotsForEventResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ActionType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ActionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfActionType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ActionType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ActionType");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ActionType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfCode");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Code[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Code");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Code");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfEvent");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Event[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfEventObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.EventObject[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventObject");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventObject");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfEventType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.EventType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfExtensibleDatatype");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatatype[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatatype");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatatype");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfExtensibleDatum");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatum");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatum");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfGeoPoint");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GeoPoint[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GeoPoint");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GeoPoint");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfObjectHistory");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ObjectHistory[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectHistory");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectHistory");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfObjectRef");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfPoint");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Point[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Point");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Point");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfPointAddressType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddressType");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddressType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfPointInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PointInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointInfo");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfReservedSlot");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ReservedSlot[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservedSlot");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservedSlot");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfReservedTaskInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ReservedTaskInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservedTaskInfo");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservedTaskInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfResolutionCode");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCode");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCode");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfScheduledTaskInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduledTaskInfo");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduledTaskInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfService");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Service_Type[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Service");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Service");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceGroup");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceGroup[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceGroup");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceGroup");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceSummary");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceSummary[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceSummary");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceSummary");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceTask");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTask[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTask");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTask");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceTaskInstances");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstances");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstances");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceTaskLine");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskLine[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskLine");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskLine");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceTaskSchedule");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskSchedule[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskSchedule");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskSchedule");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceUnit");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnit");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnit");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfServiceUnitPoint");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitPoint[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitPoint");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitPoint");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfState");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.State[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "State");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "State");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfStateResolutionCode");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.StateResolutionCode[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateResolutionCode");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateResolutionCode");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfStreet");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Street[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Street");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Street");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfTask");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Task[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Task");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Task");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfTaskPointInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TaskPointInfo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskPointInfo");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskPointInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfTaskType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TaskType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskType");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ArrayOfWeighbridgeTicketLine");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLine[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketLine");
            qName2 = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketLine");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Code");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Code.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CodeFile");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.CodeFile.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoApiFault");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EchoObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.EchoObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Event.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventAction");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.EventAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.EventObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventQuery");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.EventQuery.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.EventType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatatype");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatatype.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatum");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleObject");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ExtensibleObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ExtensibleType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GeoPoint");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.GeoPoint.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Inspection");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Inspection.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectHistory");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ObjectHistory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ObjectInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ObjectRef.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PartyInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PartyInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PerformEventActionResult");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PerformEventActionResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Point");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Point.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddress");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PointAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddressType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PointAddressType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PointInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointQuery");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PointQuery.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PostEventResult");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.PostEventResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ProductInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ProductInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservationParameters");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ReservationParameters.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservedSlot");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ReservedSlot.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservedTaskInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ReservedTaskInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCode");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResourceInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ResourceInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduledTaskInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Service");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Service_Type.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceGroup");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceGroup.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceSummary");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceSummary.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTask");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTask.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstanceQuery");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstanceQuery.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskInstances");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskLine");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskLine.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskSchedule");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskSchedule.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskScheduleAllocation");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskScheduleAllocation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnit");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitPoint");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitPoint.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitQuery");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitQuery.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SiteInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.SiteInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "State");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.State.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateResolutionCode");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.StateResolutionCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateWorkflow");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.StateWorkflow.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Street");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Street.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StreetQuery");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.StreetQuery.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SubscriptionParameters");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.SubscriptionParameters.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Task");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.Task.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskAllocation");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TaskAllocation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskOptions");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TaskOptions.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskPointInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TaskPointInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskResolutionInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TaskResolutionInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskStateInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TaskStateInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskType");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TaskType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TimeBand");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.TimeBand.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeStationInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeStationInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicket");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicket.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketLine");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLine.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketLineTypeInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLineTypeInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketTypeInfo");
            cachedSerQNames.add(qName);
            cls = com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketTypeInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.placecube.digitalplace.local.waste.echo.axis.CodeFile getCodeFile(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetCodeFile");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetCodeFile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.CodeFile) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.CodeFile) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.CodeFile.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Code getCode(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetCode");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetCode"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Code) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Code) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Code.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[] getResolutionCodes() throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetResolutionCodes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetResolutionCodes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Street[] findStreets(com.placecube.digitalplace.local.waste.echo.axis.StreetQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/FindStreets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "FindStreets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {query});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Street[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Street[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Street[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Street getStreet(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetStreet");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetStreet"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Street) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Street) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Street.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.PointInfo[] findPoints(com.placecube.digitalplace.local.waste.echo.axis.PointQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/FindPoints");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "FindPoints"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {query});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.PointInfo[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.PointInfo[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.PointInfo[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Point getPoint(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetPoint");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPoint"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Point) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Point) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Point.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.PointAddress getPointAddress(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetPointAddress");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPointAddress"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.PointAddress) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.PointAddress) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.PointAddress.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Point[] getPointSegmentsForStreet(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef streetRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetPointSegmentsForStreet");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPointSegmentsForStreet"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {streetRef});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Point[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Point[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Point[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.EventType getEventType(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetEventType");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventType"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.EventType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.EventType) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.EventType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.EventType[] getEventTypesForService(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef serviceRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetEventTypesForService");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventTypesForService"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {serviceRef});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.EventType[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.EventType[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.EventType[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.PostEventResult postEvent(com.placecube.digitalplace.local.waste.echo.axis.Event event) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/PostEvent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PostEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {event});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.PostEventResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.PostEventResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.PostEventResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.PerformEventActionResult performEventAction(com.placecube.digitalplace.local.waste.echo.axis.EventAction action) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/PerformEventAction");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PerformEventAction"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {action});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.PerformEventActionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.PerformEventActionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.PerformEventActionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Event[] getEventsForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef, com.placecube.digitalplace.local.waste.echo.axis.EventQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetEventsForObject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventsForObject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {objectRef, query});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Event[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Event[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Event[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Event getEvent(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetEvent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Event) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Event) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Event.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Inspection getInspection(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetInspection");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetInspection"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Inspection) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Inspection) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Inspection.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.ServiceSummary[] getServiceSummaryForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetServiceSummaryForObject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServiceSummaryForObject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {objectRef});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.ServiceSummary[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.ServiceSummary[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.ServiceSummary[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public java.lang.Integer getEventCountForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef, com.placecube.digitalplace.local.waste.echo.axis.EventQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetEventCountForObject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetEventCountForObject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {objectRef, query});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.Integer) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.Integer) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.Integer.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.ReservedTaskInfo[] reserveAvailableSlotsForEvent(com.placecube.digitalplace.local.waste.echo.axis.Event event, com.placecube.digitalplace.local.waste.echo.axis.ReservationParameters parameters) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/ReserveAvailableSlotsForEvent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReserveAvailableSlotsForEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {event, parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.ReservedTaskInfo[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.ReservedTaskInfo[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.ReservedTaskInfo[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void cancelReservedSlotsForEvent(java.lang.String eventGuid) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/CancelReservedSlotsForEvent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CancelReservedSlotsForEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {eventGuid});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[] getPointAddressTypes() throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetPointAddressTypes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPointAddressTypes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.ServiceGroup[] getServicesForContract(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef contractRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetServicesForContract");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServicesForContract"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {contractRef});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.ServiceGroup[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.ServiceGroup[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.ServiceGroup[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.TaskType getTaskType(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetTaskType");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTaskType"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.TaskType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.TaskType) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.TaskType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.TaskType[] getTaskTypesForService(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef serviceRef) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetTaskTypesForService");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTaskTypesForService"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {serviceRef});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.TaskType[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.TaskType[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.TaskType[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit[] getServiceUnitsForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef, com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetServiceUnitsForObject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServiceUnitsForObject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {objectRef, query});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  	System.out.println("*** AXISFAULT - START ***");
  	System.out.println("Request:");
  	System.out.println(_call.getMessageContext().getRequestMessage().getSOAPPartAsString());
  	System.out.println("*** AXISFAULT - END ***");
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[] getServiceTaskInstances(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] serviceTaskRefs, com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstanceQuery query) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetServiceTaskInstances");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetServiceTaskInstances"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {serviceTaskRefs, query});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskInstances[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Task[] getTasks(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] taskRefs, com.placecube.digitalplace.local.waste.echo.axis.TaskOptions options) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetTasks");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTasks"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {taskRefs, options});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Task[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Task[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Task[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.Task[] getTasksForObject(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef, com.placecube.digitalplace.local.waste.echo.axis.TaskOptions options) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetTasksForObject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetTasksForObject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {objectRef, options});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.Task[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.Task[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.Task[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void addSubscription(com.placecube.digitalplace.local.waste.echo.axis.SubscriptionParameters parameters) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/AddSubscription");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "AddSubscription"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicket getWeighbridgeTicket(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) throws java.rmi.RemoteException, com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetWeighbridgeTicket");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetWeighbridgeTicket"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ref});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicket) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicket) org.apache.axis.utils.JavaUtils.convert(_resp, com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicket.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) {
              throw (com.placecube.digitalplace.local.waste.echo.axis.EchoApiFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
