/**
 * ServiceTaskSchedule.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceTaskSchedule  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskScheduleAllocation allocation;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate;

    private java.lang.Boolean hasOwnSchedule;

    private com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo lastInstance;

    private com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo nextInstance;

    private java.lang.String scheduleDescription;

    private java.lang.Integer scheduleId;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate;

    private com.placecube.digitalplace.local.waste.echo.axis.TimeBand timeBand;

    public ServiceTaskSchedule() {
    }

    public ServiceTaskSchedule(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskScheduleAllocation allocation,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate,
           java.lang.Boolean hasOwnSchedule,
           com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo lastInstance,
           com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo nextInstance,
           java.lang.String scheduleDescription,
           java.lang.Integer scheduleId,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate,
           com.placecube.digitalplace.local.waste.echo.axis.TimeBand timeBand) {
        super(
            guid,
            id);
        this.allocation = allocation;
        this.endDate = endDate;
        this.hasOwnSchedule = hasOwnSchedule;
        this.lastInstance = lastInstance;
        this.nextInstance = nextInstance;
        this.scheduleDescription = scheduleDescription;
        this.scheduleId = scheduleId;
        this.startDate = startDate;
        this.timeBand = timeBand;
    }


    /**
     * Gets the allocation value for this ServiceTaskSchedule.
     * 
     * @return allocation
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskScheduleAllocation getAllocation() {
        return allocation;
    }


    /**
     * Sets the allocation value for this ServiceTaskSchedule.
     * 
     * @param allocation
     */
    public void setAllocation(com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskScheduleAllocation allocation) {
        this.allocation = allocation;
    }


    /**
     * Gets the endDate value for this ServiceTaskSchedule.
     * 
     * @return endDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this ServiceTaskSchedule.
     * 
     * @param endDate
     */
    public void setEndDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the hasOwnSchedule value for this ServiceTaskSchedule.
     * 
     * @return hasOwnSchedule
     */
    public java.lang.Boolean getHasOwnSchedule() {
        return hasOwnSchedule;
    }


    /**
     * Sets the hasOwnSchedule value for this ServiceTaskSchedule.
     * 
     * @param hasOwnSchedule
     */
    public void setHasOwnSchedule(java.lang.Boolean hasOwnSchedule) {
        this.hasOwnSchedule = hasOwnSchedule;
    }


    /**
     * Gets the lastInstance value for this ServiceTaskSchedule.
     * 
     * @return lastInstance
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo getLastInstance() {
        return lastInstance;
    }


    /**
     * Sets the lastInstance value for this ServiceTaskSchedule.
     * 
     * @param lastInstance
     */
    public void setLastInstance(com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo lastInstance) {
        this.lastInstance = lastInstance;
    }


    /**
     * Gets the nextInstance value for this ServiceTaskSchedule.
     * 
     * @return nextInstance
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo getNextInstance() {
        return nextInstance;
    }


    /**
     * Sets the nextInstance value for this ServiceTaskSchedule.
     * 
     * @param nextInstance
     */
    public void setNextInstance(com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo nextInstance) {
        this.nextInstance = nextInstance;
    }


    /**
     * Gets the scheduleDescription value for this ServiceTaskSchedule.
     * 
     * @return scheduleDescription
     */
    public java.lang.String getScheduleDescription() {
        return scheduleDescription;
    }


    /**
     * Sets the scheduleDescription value for this ServiceTaskSchedule.
     * 
     * @param scheduleDescription
     */
    public void setScheduleDescription(java.lang.String scheduleDescription) {
        this.scheduleDescription = scheduleDescription;
    }


    /**
     * Gets the scheduleId value for this ServiceTaskSchedule.
     * 
     * @return scheduleId
     */
    public java.lang.Integer getScheduleId() {
        return scheduleId;
    }


    /**
     * Sets the scheduleId value for this ServiceTaskSchedule.
     * 
     * @param scheduleId
     */
    public void setScheduleId(java.lang.Integer scheduleId) {
        this.scheduleId = scheduleId;
    }


    /**
     * Gets the startDate value for this ServiceTaskSchedule.
     * 
     * @return startDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this ServiceTaskSchedule.
     * 
     * @param startDate
     */
    public void setStartDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the timeBand value for this ServiceTaskSchedule.
     * 
     * @return timeBand
     */
    public com.placecube.digitalplace.local.waste.echo.axis.TimeBand getTimeBand() {
        return timeBand;
    }


    /**
     * Sets the timeBand value for this ServiceTaskSchedule.
     * 
     * @param timeBand
     */
    public void setTimeBand(com.placecube.digitalplace.local.waste.echo.axis.TimeBand timeBand) {
        this.timeBand = timeBand;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceTaskSchedule)) return false;
        ServiceTaskSchedule other = (ServiceTaskSchedule) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.allocation==null && other.getAllocation()==null) || 
             (this.allocation!=null &&
              this.allocation.equals(other.getAllocation()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.hasOwnSchedule==null && other.getHasOwnSchedule()==null) || 
             (this.hasOwnSchedule!=null &&
              this.hasOwnSchedule.equals(other.getHasOwnSchedule()))) &&
            ((this.lastInstance==null && other.getLastInstance()==null) || 
             (this.lastInstance!=null &&
              this.lastInstance.equals(other.getLastInstance()))) &&
            ((this.nextInstance==null && other.getNextInstance()==null) || 
             (this.nextInstance!=null &&
              this.nextInstance.equals(other.getNextInstance()))) &&
            ((this.scheduleDescription==null && other.getScheduleDescription()==null) || 
             (this.scheduleDescription!=null &&
              this.scheduleDescription.equals(other.getScheduleDescription()))) &&
            ((this.scheduleId==null && other.getScheduleId()==null) || 
             (this.scheduleId!=null &&
              this.scheduleId.equals(other.getScheduleId()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.timeBand==null && other.getTimeBand()==null) || 
             (this.timeBand!=null &&
              this.timeBand.equals(other.getTimeBand())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAllocation() != null) {
            _hashCode += getAllocation().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getHasOwnSchedule() != null) {
            _hashCode += getHasOwnSchedule().hashCode();
        }
        if (getLastInstance() != null) {
            _hashCode += getLastInstance().hashCode();
        }
        if (getNextInstance() != null) {
            _hashCode += getNextInstance().hashCode();
        }
        if (getScheduleDescription() != null) {
            _hashCode += getScheduleDescription().hashCode();
        }
        if (getScheduleId() != null) {
            _hashCode += getScheduleId().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getTimeBand() != null) {
            _hashCode += getTimeBand().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceTaskSchedule.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskSchedule"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Allocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskScheduleAllocation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hasOwnSchedule");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "HasOwnSchedule"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastInstance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "LastInstance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduledTaskInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nextInstance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "NextInstance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduledTaskInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduleDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduleDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduleId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeBand");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TimeBand"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TimeBand"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
