package com.placecube.digitalplace.local.waste.echo.axis.constants;

public enum ObjectRefType {

	POINT_ADDRESS("PointAddress"), POINT_NODE("PointNode"), POINT_SEGMENT("PointSegment"), POINT_AREA("PointArea");

	private String type;

	private ObjectRefType(String type) {
		this.type = type;
	}

	public String getStrVal() {
		return type;
	}
}
