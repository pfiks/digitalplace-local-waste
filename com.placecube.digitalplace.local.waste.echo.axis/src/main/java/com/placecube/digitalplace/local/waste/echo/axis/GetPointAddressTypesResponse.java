/**
 * GetPointAddressTypesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetPointAddressTypesResponse  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[] getPointAddressTypesResult;

    public GetPointAddressTypesResponse() {
    }

    public GetPointAddressTypesResponse(
           com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[] getPointAddressTypesResult) {
           this.getPointAddressTypesResult = getPointAddressTypesResult;
    }


    /**
     * Gets the getPointAddressTypesResult value for this GetPointAddressTypesResponse.
     * 
     * @return getPointAddressTypesResult
     */
    public com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[] getGetPointAddressTypesResult() {
        return getPointAddressTypesResult;
    }


    /**
     * Sets the getPointAddressTypesResult value for this GetPointAddressTypesResponse.
     * 
     * @param getPointAddressTypesResult
     */
    public void setGetPointAddressTypesResult(com.placecube.digitalplace.local.waste.echo.axis.PointAddressType[] getPointAddressTypesResult) {
        this.getPointAddressTypesResult = getPointAddressTypesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPointAddressTypesResponse)) return false;
        GetPointAddressTypesResponse other = (GetPointAddressTypesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPointAddressTypesResult==null && other.getGetPointAddressTypesResult()==null) || 
             (this.getPointAddressTypesResult!=null &&
              java.util.Arrays.equals(this.getPointAddressTypesResult, other.getGetPointAddressTypesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPointAddressTypesResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPointAddressTypesResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPointAddressTypesResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPointAddressTypesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetPointAddressTypesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPointAddressTypesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetPointAddressTypesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddressType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointAddressType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
