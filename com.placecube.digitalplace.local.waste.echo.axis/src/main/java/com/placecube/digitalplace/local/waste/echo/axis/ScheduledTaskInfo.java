/**
 * ScheduledTaskInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ScheduledTaskInfo  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset currentScheduledDate;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset originalScheduledDate;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref;

    public ScheduledTaskInfo() {
    }

    public ScheduledTaskInfo(
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset currentScheduledDate,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset originalScheduledDate,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) {
           this.currentScheduledDate = currentScheduledDate;
           this.originalScheduledDate = originalScheduledDate;
           this.ref = ref;
    }


    /**
     * Gets the currentScheduledDate value for this ScheduledTaskInfo.
     * 
     * @return currentScheduledDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getCurrentScheduledDate() {
        return currentScheduledDate;
    }


    /**
     * Sets the currentScheduledDate value for this ScheduledTaskInfo.
     * 
     * @param currentScheduledDate
     */
    public void setCurrentScheduledDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset currentScheduledDate) {
        this.currentScheduledDate = currentScheduledDate;
    }


    /**
     * Gets the originalScheduledDate value for this ScheduledTaskInfo.
     * 
     * @return originalScheduledDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getOriginalScheduledDate() {
        return originalScheduledDate;
    }


    /**
     * Sets the originalScheduledDate value for this ScheduledTaskInfo.
     * 
     * @param originalScheduledDate
     */
    public void setOriginalScheduledDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset originalScheduledDate) {
        this.originalScheduledDate = originalScheduledDate;
    }


    /**
     * Gets the ref value for this ScheduledTaskInfo.
     * 
     * @return ref
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getRef() {
        return ref;
    }


    /**
     * Sets the ref value for this ScheduledTaskInfo.
     * 
     * @param ref
     */
    public void setRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef ref) {
        this.ref = ref;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScheduledTaskInfo)) return false;
        ScheduledTaskInfo other = (ScheduledTaskInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.currentScheduledDate==null && other.getCurrentScheduledDate()==null) || 
             (this.currentScheduledDate!=null &&
              this.currentScheduledDate.equals(other.getCurrentScheduledDate()))) &&
            ((this.originalScheduledDate==null && other.getOriginalScheduledDate()==null) || 
             (this.originalScheduledDate!=null &&
              this.originalScheduledDate.equals(other.getOriginalScheduledDate()))) &&
            ((this.ref==null && other.getRef()==null) || 
             (this.ref!=null &&
              this.ref.equals(other.getRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCurrentScheduledDate() != null) {
            _hashCode += getCurrentScheduledDate().hashCode();
        }
        if (getOriginalScheduledDate() != null) {
            _hashCode += getOriginalScheduledDate().hashCode();
        }
        if (getRef() != null) {
            _hashCode += getRef().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScheduledTaskInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduledTaskInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentScheduledDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CurrentScheduledDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalScheduledDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "OriginalScheduledDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ref");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Ref"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
