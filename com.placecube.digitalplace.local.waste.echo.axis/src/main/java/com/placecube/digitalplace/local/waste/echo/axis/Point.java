/**
 * Point.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class Point  extends com.placecube.digitalplace.local.waste.echo.axis.ExtensibleObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.GeoPoint[] coordinates;

    private java.lang.String description;

    private java.lang.String pointType;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef sharedRef;

    public Point() {
    }

    public Point(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data,
           com.placecube.digitalplace.local.waste.echo.axis.GeoPoint[] coordinates,
           java.lang.String description,
           java.lang.String pointType,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef sharedRef) {
        super(
            guid,
            id,
            data);
        this.coordinates = coordinates;
        this.description = description;
        this.pointType = pointType;
        this.sharedRef = sharedRef;
    }


    /**
     * Gets the coordinates value for this Point.
     * 
     * @return coordinates
     */
    public com.placecube.digitalplace.local.waste.echo.axis.GeoPoint[] getCoordinates() {
        return coordinates;
    }


    /**
     * Sets the coordinates value for this Point.
     * 
     * @param coordinates
     */
    public void setCoordinates(com.placecube.digitalplace.local.waste.echo.axis.GeoPoint[] coordinates) {
        this.coordinates = coordinates;
    }


    /**
     * Gets the description value for this Point.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Point.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the pointType value for this Point.
     * 
     * @return pointType
     */
    public java.lang.String getPointType() {
        return pointType;
    }


    /**
     * Sets the pointType value for this Point.
     * 
     * @param pointType
     */
    public void setPointType(java.lang.String pointType) {
        this.pointType = pointType;
    }


    /**
     * Gets the sharedRef value for this Point.
     * 
     * @return sharedRef
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getSharedRef() {
        return sharedRef;
    }


    /**
     * Sets the sharedRef value for this Point.
     * 
     * @param sharedRef
     */
    public void setSharedRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef sharedRef) {
        this.sharedRef = sharedRef;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Point)) return false;
        Point other = (Point) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.coordinates==null && other.getCoordinates()==null) || 
             (this.coordinates!=null &&
              java.util.Arrays.equals(this.coordinates, other.getCoordinates()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.pointType==null && other.getPointType()==null) || 
             (this.pointType!=null &&
              this.pointType.equals(other.getPointType()))) &&
            ((this.sharedRef==null && other.getSharedRef()==null) || 
             (this.sharedRef!=null &&
              this.sharedRef.equals(other.getSharedRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCoordinates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCoordinates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCoordinates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getPointType() != null) {
            _hashCode += getPointType().hashCode();
        }
        if (getSharedRef() != null) {
            _hashCode += getSharedRef().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Point.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Point"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coordinates");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Coordinates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GeoPoint"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GeoPoint"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sharedRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SharedRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
