/**
 * Event.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class Event  extends com.placecube.digitalplace.local.waste.echo.axis.ExtensibleObject  implements java.io.Serializable {
    private java.lang.Integer allocatedContractUnitId;

    private java.lang.Integer allocatedUserId;

    private java.lang.String clientReference;

    private java.lang.String description;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset dueDate;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset eventDate;

    private com.placecube.digitalplace.local.waste.echo.axis.EventObject[] eventObjects;

    private java.lang.Integer eventStateId;

    private java.lang.Integer eventTypeId;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectHistory[] history;

    private java.lang.Integer lastActionId;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset lastUpdatedDate;

    private java.lang.Integer resolutionCodeId;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset resolvedDate;

    private java.lang.Integer serviceId;

    private java.lang.String[] taskReservations;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] taskRefs;

    public Event() {
    }

    public Event(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data,
           java.lang.Integer allocatedContractUnitId,
           java.lang.Integer allocatedUserId,
           java.lang.String clientReference,
           java.lang.String description,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset dueDate,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset eventDate,
           com.placecube.digitalplace.local.waste.echo.axis.EventObject[] eventObjects,
           java.lang.Integer eventStateId,
           java.lang.Integer eventTypeId,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectHistory[] history,
           java.lang.Integer lastActionId,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset lastUpdatedDate,
           java.lang.Integer resolutionCodeId,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset resolvedDate,
           java.lang.Integer serviceId,
           java.lang.String[] taskReservations,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] taskRefs) {
        super(
            guid,
            id,
            data);
        this.allocatedContractUnitId = allocatedContractUnitId;
        this.allocatedUserId = allocatedUserId;
        this.clientReference = clientReference;
        this.description = description;
        this.dueDate = dueDate;
        this.endDate = endDate;
        this.eventDate = eventDate;
        this.eventObjects = eventObjects;
        this.eventStateId = eventStateId;
        this.eventTypeId = eventTypeId;
        this.history = history;
        this.lastActionId = lastActionId;
        this.lastUpdatedDate = lastUpdatedDate;
        this.resolutionCodeId = resolutionCodeId;
        this.resolvedDate = resolvedDate;
        this.serviceId = serviceId;
        this.taskReservations = taskReservations;
        this.taskRefs = taskRefs;
    }


    /**
     * Gets the allocatedContractUnitId value for this Event.
     * 
     * @return allocatedContractUnitId
     */
    public java.lang.Integer getAllocatedContractUnitId() {
        return allocatedContractUnitId;
    }


    /**
     * Sets the allocatedContractUnitId value for this Event.
     * 
     * @param allocatedContractUnitId
     */
    public void setAllocatedContractUnitId(java.lang.Integer allocatedContractUnitId) {
        this.allocatedContractUnitId = allocatedContractUnitId;
    }


    /**
     * Gets the allocatedUserId value for this Event.
     * 
     * @return allocatedUserId
     */
    public java.lang.Integer getAllocatedUserId() {
        return allocatedUserId;
    }


    /**
     * Sets the allocatedUserId value for this Event.
     * 
     * @param allocatedUserId
     */
    public void setAllocatedUserId(java.lang.Integer allocatedUserId) {
        this.allocatedUserId = allocatedUserId;
    }


    /**
     * Gets the clientReference value for this Event.
     * 
     * @return clientReference
     */
    public java.lang.String getClientReference() {
        return clientReference;
    }


    /**
     * Sets the clientReference value for this Event.
     * 
     * @param clientReference
     */
    public void setClientReference(java.lang.String clientReference) {
        this.clientReference = clientReference;
    }


    /**
     * Gets the description value for this Event.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Event.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the dueDate value for this Event.
     * 
     * @return dueDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getDueDate() {
        return dueDate;
    }


    /**
     * Sets the dueDate value for this Event.
     * 
     * @param dueDate
     */
    public void setDueDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset dueDate) {
        this.dueDate = dueDate;
    }


    /**
     * Gets the endDate value for this Event.
     * 
     * @return endDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this Event.
     * 
     * @param endDate
     */
    public void setEndDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the eventDate value for this Event.
     * 
     * @return eventDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEventDate() {
        return eventDate;
    }


    /**
     * Sets the eventDate value for this Event.
     * 
     * @param eventDate
     */
    public void setEventDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset eventDate) {
        this.eventDate = eventDate;
    }


    /**
     * Gets the eventObjects value for this Event.
     * 
     * @return eventObjects
     */
    public com.placecube.digitalplace.local.waste.echo.axis.EventObject[] getEventObjects() {
        return eventObjects;
    }


    /**
     * Sets the eventObjects value for this Event.
     * 
     * @param eventObjects
     */
    public void setEventObjects(com.placecube.digitalplace.local.waste.echo.axis.EventObject[] eventObjects) {
        this.eventObjects = eventObjects;
    }


    /**
     * Gets the eventStateId value for this Event.
     * 
     * @return eventStateId
     */
    public java.lang.Integer getEventStateId() {
        return eventStateId;
    }


    /**
     * Sets the eventStateId value for this Event.
     * 
     * @param eventStateId
     */
    public void setEventStateId(java.lang.Integer eventStateId) {
        this.eventStateId = eventStateId;
    }


    /**
     * Gets the eventTypeId value for this Event.
     * 
     * @return eventTypeId
     */
    public java.lang.Integer getEventTypeId() {
        return eventTypeId;
    }


    /**
     * Sets the eventTypeId value for this Event.
     * 
     * @param eventTypeId
     */
    public void setEventTypeId(java.lang.Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }


    /**
     * Gets the history value for this Event.
     * 
     * @return history
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectHistory[] getHistory() {
        return history;
    }


    /**
     * Sets the history value for this Event.
     * 
     * @param history
     */
    public void setHistory(com.placecube.digitalplace.local.waste.echo.axis.ObjectHistory[] history) {
        this.history = history;
    }


    /**
     * Gets the lastActionId value for this Event.
     * 
     * @return lastActionId
     */
    public java.lang.Integer getLastActionId() {
        return lastActionId;
    }


    /**
     * Sets the lastActionId value for this Event.
     * 
     * @param lastActionId
     */
    public void setLastActionId(java.lang.Integer lastActionId) {
        this.lastActionId = lastActionId;
    }


    /**
     * Gets the lastUpdatedDate value for this Event.
     * 
     * @return lastUpdatedDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    /**
     * Sets the lastUpdatedDate value for this Event.
     * 
     * @param lastUpdatedDate
     */
    public void setLastUpdatedDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }


    /**
     * Gets the resolutionCodeId value for this Event.
     * 
     * @return resolutionCodeId
     */
    public java.lang.Integer getResolutionCodeId() {
        return resolutionCodeId;
    }


    /**
     * Sets the resolutionCodeId value for this Event.
     * 
     * @param resolutionCodeId
     */
    public void setResolutionCodeId(java.lang.Integer resolutionCodeId) {
        this.resolutionCodeId = resolutionCodeId;
    }


    /**
     * Gets the resolvedDate value for this Event.
     * 
     * @return resolvedDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getResolvedDate() {
        return resolvedDate;
    }


    /**
     * Sets the resolvedDate value for this Event.
     * 
     * @param resolvedDate
     */
    public void setResolvedDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset resolvedDate) {
        this.resolvedDate = resolvedDate;
    }


    /**
     * Gets the serviceId value for this Event.
     * 
     * @return serviceId
     */
    public java.lang.Integer getServiceId() {
        return serviceId;
    }


    /**
     * Sets the serviceId value for this Event.
     * 
     * @param serviceId
     */
    public void setServiceId(java.lang.Integer serviceId) {
        this.serviceId = serviceId;
    }


    /**
     * Gets the taskReservations value for this Event.
     * 
     * @return taskReservations
     */
    public java.lang.String[] getTaskReservations() {
        return taskReservations;
    }


    /**
     * Sets the taskReservations value for this Event.
     * 
     * @param taskReservations
     */
    public void setTaskReservations(java.lang.String[] taskReservations) {
        this.taskReservations = taskReservations;
    }


    /**
     * Gets the taskRefs value for this Event.
     * 
     * @return taskRefs
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] getTaskRefs() {
        return taskRefs;
    }


    /**
     * Sets the taskRefs value for this Event.
     * 
     * @param taskRefs
     */
    public void setTaskRefs(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] taskRefs) {
        this.taskRefs = taskRefs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Event)) return false;
        Event other = (Event) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.allocatedContractUnitId==null && other.getAllocatedContractUnitId()==null) || 
             (this.allocatedContractUnitId!=null &&
              this.allocatedContractUnitId.equals(other.getAllocatedContractUnitId()))) &&
            ((this.allocatedUserId==null && other.getAllocatedUserId()==null) || 
             (this.allocatedUserId!=null &&
              this.allocatedUserId.equals(other.getAllocatedUserId()))) &&
            ((this.clientReference==null && other.getClientReference()==null) || 
             (this.clientReference!=null &&
              this.clientReference.equals(other.getClientReference()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.dueDate==null && other.getDueDate()==null) || 
             (this.dueDate!=null &&
              this.dueDate.equals(other.getDueDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.eventDate==null && other.getEventDate()==null) || 
             (this.eventDate!=null &&
              this.eventDate.equals(other.getEventDate()))) &&
            ((this.eventObjects==null && other.getEventObjects()==null) || 
             (this.eventObjects!=null &&
              java.util.Arrays.equals(this.eventObjects, other.getEventObjects()))) &&
            ((this.eventStateId==null && other.getEventStateId()==null) || 
             (this.eventStateId!=null &&
              this.eventStateId.equals(other.getEventStateId()))) &&
            ((this.eventTypeId==null && other.getEventTypeId()==null) || 
             (this.eventTypeId!=null &&
              this.eventTypeId.equals(other.getEventTypeId()))) &&
            ((this.history==null && other.getHistory()==null) || 
             (this.history!=null &&
              java.util.Arrays.equals(this.history, other.getHistory()))) &&
            ((this.lastActionId==null && other.getLastActionId()==null) || 
             (this.lastActionId!=null &&
              this.lastActionId.equals(other.getLastActionId()))) &&
            ((this.lastUpdatedDate==null && other.getLastUpdatedDate()==null) || 
             (this.lastUpdatedDate!=null &&
              this.lastUpdatedDate.equals(other.getLastUpdatedDate()))) &&
            ((this.resolutionCodeId==null && other.getResolutionCodeId()==null) || 
             (this.resolutionCodeId!=null &&
              this.resolutionCodeId.equals(other.getResolutionCodeId()))) &&
            ((this.resolvedDate==null && other.getResolvedDate()==null) || 
             (this.resolvedDate!=null &&
              this.resolvedDate.equals(other.getResolvedDate()))) &&
            ((this.serviceId==null && other.getServiceId()==null) || 
             (this.serviceId!=null &&
              this.serviceId.equals(other.getServiceId()))) &&
            ((this.taskReservations==null && other.getTaskReservations()==null) || 
             (this.taskReservations!=null &&
              java.util.Arrays.equals(this.taskReservations, other.getTaskReservations()))) &&
            ((this.taskRefs==null && other.getTaskRefs()==null) || 
             (this.taskRefs!=null &&
              java.util.Arrays.equals(this.taskRefs, other.getTaskRefs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAllocatedContractUnitId() != null) {
            _hashCode += getAllocatedContractUnitId().hashCode();
        }
        if (getAllocatedUserId() != null) {
            _hashCode += getAllocatedUserId().hashCode();
        }
        if (getClientReference() != null) {
            _hashCode += getClientReference().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getDueDate() != null) {
            _hashCode += getDueDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getEventDate() != null) {
            _hashCode += getEventDate().hashCode();
        }
        if (getEventObjects() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEventObjects());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEventObjects(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEventStateId() != null) {
            _hashCode += getEventStateId().hashCode();
        }
        if (getEventTypeId() != null) {
            _hashCode += getEventTypeId().hashCode();
        }
        if (getHistory() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHistory());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHistory(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLastActionId() != null) {
            _hashCode += getLastActionId().hashCode();
        }
        if (getLastUpdatedDate() != null) {
            _hashCode += getLastUpdatedDate().hashCode();
        }
        if (getResolutionCodeId() != null) {
            _hashCode += getResolutionCodeId().hashCode();
        }
        if (getResolvedDate() != null) {
            _hashCode += getResolvedDate().hashCode();
        }
        if (getServiceId() != null) {
            _hashCode += getServiceId().hashCode();
        }
        if (getTaskReservations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTaskReservations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTaskReservations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTaskRefs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTaskRefs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTaskRefs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Event.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocatedContractUnitId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "AllocatedContractUnitId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allocatedUserId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "AllocatedUserId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ClientReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dueDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "DueDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventObjects");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventObjects"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventObject"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventObject"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventStateId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventStateId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("history");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "History"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectHistory"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectHistory"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastActionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "LastActionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdatedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "LastUpdatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resolutionCodeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCodeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resolvedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolvedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskReservations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskReservations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskRefs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskRefs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
