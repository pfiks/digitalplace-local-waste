/**
 * WeighbridgeTicketLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class WeighbridgeTicketLine  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset grossDateTime;

    private java.lang.Double grossWeight;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset netDateTime;

    private java.lang.Double netWeight;

    private com.placecube.digitalplace.local.waste.echo.axis.ProductInfo product;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset tareDateTime;

    private java.lang.Double tareWeight;

    private com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLineTypeInfo ticketLineType;

    public WeighbridgeTicketLine() {
    }

    public WeighbridgeTicketLine(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset grossDateTime,
           java.lang.Double grossWeight,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset netDateTime,
           java.lang.Double netWeight,
           com.placecube.digitalplace.local.waste.echo.axis.ProductInfo product,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset tareDateTime,
           java.lang.Double tareWeight,
           com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLineTypeInfo ticketLineType) {
        super(
            guid,
            id);
        this.grossDateTime = grossDateTime;
        this.grossWeight = grossWeight;
        this.netDateTime = netDateTime;
        this.netWeight = netWeight;
        this.product = product;
        this.tareDateTime = tareDateTime;
        this.tareWeight = tareWeight;
        this.ticketLineType = ticketLineType;
    }


    /**
     * Gets the grossDateTime value for this WeighbridgeTicketLine.
     * 
     * @return grossDateTime
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getGrossDateTime() {
        return grossDateTime;
    }


    /**
     * Sets the grossDateTime value for this WeighbridgeTicketLine.
     * 
     * @param grossDateTime
     */
    public void setGrossDateTime(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset grossDateTime) {
        this.grossDateTime = grossDateTime;
    }


    /**
     * Gets the grossWeight value for this WeighbridgeTicketLine.
     * 
     * @return grossWeight
     */
    public java.lang.Double getGrossWeight() {
        return grossWeight;
    }


    /**
     * Sets the grossWeight value for this WeighbridgeTicketLine.
     * 
     * @param grossWeight
     */
    public void setGrossWeight(java.lang.Double grossWeight) {
        this.grossWeight = grossWeight;
    }


    /**
     * Gets the netDateTime value for this WeighbridgeTicketLine.
     * 
     * @return netDateTime
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getNetDateTime() {
        return netDateTime;
    }


    /**
     * Sets the netDateTime value for this WeighbridgeTicketLine.
     * 
     * @param netDateTime
     */
    public void setNetDateTime(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset netDateTime) {
        this.netDateTime = netDateTime;
    }


    /**
     * Gets the netWeight value for this WeighbridgeTicketLine.
     * 
     * @return netWeight
     */
    public java.lang.Double getNetWeight() {
        return netWeight;
    }


    /**
     * Sets the netWeight value for this WeighbridgeTicketLine.
     * 
     * @param netWeight
     */
    public void setNetWeight(java.lang.Double netWeight) {
        this.netWeight = netWeight;
    }


    /**
     * Gets the product value for this WeighbridgeTicketLine.
     * 
     * @return product
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ProductInfo getProduct() {
        return product;
    }


    /**
     * Sets the product value for this WeighbridgeTicketLine.
     * 
     * @param product
     */
    public void setProduct(com.placecube.digitalplace.local.waste.echo.axis.ProductInfo product) {
        this.product = product;
    }


    /**
     * Gets the tareDateTime value for this WeighbridgeTicketLine.
     * 
     * @return tareDateTime
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getTareDateTime() {
        return tareDateTime;
    }


    /**
     * Sets the tareDateTime value for this WeighbridgeTicketLine.
     * 
     * @param tareDateTime
     */
    public void setTareDateTime(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset tareDateTime) {
        this.tareDateTime = tareDateTime;
    }


    /**
     * Gets the tareWeight value for this WeighbridgeTicketLine.
     * 
     * @return tareWeight
     */
    public java.lang.Double getTareWeight() {
        return tareWeight;
    }


    /**
     * Sets the tareWeight value for this WeighbridgeTicketLine.
     * 
     * @param tareWeight
     */
    public void setTareWeight(java.lang.Double tareWeight) {
        this.tareWeight = tareWeight;
    }


    /**
     * Gets the ticketLineType value for this WeighbridgeTicketLine.
     * 
     * @return ticketLineType
     */
    public com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLineTypeInfo getTicketLineType() {
        return ticketLineType;
    }


    /**
     * Sets the ticketLineType value for this WeighbridgeTicketLine.
     * 
     * @param ticketLineType
     */
    public void setTicketLineType(com.placecube.digitalplace.local.waste.echo.axis.WeighbridgeTicketLineTypeInfo ticketLineType) {
        this.ticketLineType = ticketLineType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WeighbridgeTicketLine)) return false;
        WeighbridgeTicketLine other = (WeighbridgeTicketLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.grossDateTime==null && other.getGrossDateTime()==null) || 
             (this.grossDateTime!=null &&
              this.grossDateTime.equals(other.getGrossDateTime()))) &&
            ((this.grossWeight==null && other.getGrossWeight()==null) || 
             (this.grossWeight!=null &&
              this.grossWeight.equals(other.getGrossWeight()))) &&
            ((this.netDateTime==null && other.getNetDateTime()==null) || 
             (this.netDateTime!=null &&
              this.netDateTime.equals(other.getNetDateTime()))) &&
            ((this.netWeight==null && other.getNetWeight()==null) || 
             (this.netWeight!=null &&
              this.netWeight.equals(other.getNetWeight()))) &&
            ((this.product==null && other.getProduct()==null) || 
             (this.product!=null &&
              this.product.equals(other.getProduct()))) &&
            ((this.tareDateTime==null && other.getTareDateTime()==null) || 
             (this.tareDateTime!=null &&
              this.tareDateTime.equals(other.getTareDateTime()))) &&
            ((this.tareWeight==null && other.getTareWeight()==null) || 
             (this.tareWeight!=null &&
              this.tareWeight.equals(other.getTareWeight()))) &&
            ((this.ticketLineType==null && other.getTicketLineType()==null) || 
             (this.ticketLineType!=null &&
              this.ticketLineType.equals(other.getTicketLineType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGrossDateTime() != null) {
            _hashCode += getGrossDateTime().hashCode();
        }
        if (getGrossWeight() != null) {
            _hashCode += getGrossWeight().hashCode();
        }
        if (getNetDateTime() != null) {
            _hashCode += getNetDateTime().hashCode();
        }
        if (getNetWeight() != null) {
            _hashCode += getNetWeight().hashCode();
        }
        if (getProduct() != null) {
            _hashCode += getProduct().hashCode();
        }
        if (getTareDateTime() != null) {
            _hashCode += getTareDateTime().hashCode();
        }
        if (getTareWeight() != null) {
            _hashCode += getTareWeight().hashCode();
        }
        if (getTicketLineType() != null) {
            _hashCode += getTicketLineType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WeighbridgeTicketLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("grossDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GrossDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("grossWeight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GrossWeight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("netDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "NetDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("netWeight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "NetWeight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Product"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ProductInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tareDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TareDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tareWeight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TareWeight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketLineType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TicketLineType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "WeighbridgeTicketLineTypeInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
