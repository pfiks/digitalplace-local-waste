/**
 * ServiceSummary.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceSummary  implements java.io.Serializable {
    private java.lang.String description;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset lastDate;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset nextDate;

    private java.lang.Integer resolutionCodeId;

    private java.lang.String resolutionCodeName;

    private java.lang.String scheduleDescription;

    private java.lang.Integer serviceId;

    private java.lang.String serviceName;

    private java.lang.Integer serviceUnitId;

    private java.lang.Integer stateId;

    private java.lang.String stateName;

    private java.lang.Integer taskTypeId;

    private java.lang.String taskTypeName;

    public ServiceSummary() {
    }

    public ServiceSummary(
           java.lang.String description,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset lastDate,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset nextDate,
           java.lang.Integer resolutionCodeId,
           java.lang.String resolutionCodeName,
           java.lang.String scheduleDescription,
           java.lang.Integer serviceId,
           java.lang.String serviceName,
           java.lang.Integer serviceUnitId,
           java.lang.Integer stateId,
           java.lang.String stateName,
           java.lang.Integer taskTypeId,
           java.lang.String taskTypeName) {
           this.description = description;
           this.lastDate = lastDate;
           this.nextDate = nextDate;
           this.resolutionCodeId = resolutionCodeId;
           this.resolutionCodeName = resolutionCodeName;
           this.scheduleDescription = scheduleDescription;
           this.serviceId = serviceId;
           this.serviceName = serviceName;
           this.serviceUnitId = serviceUnitId;
           this.stateId = stateId;
           this.stateName = stateName;
           this.taskTypeId = taskTypeId;
           this.taskTypeName = taskTypeName;
    }


    /**
     * Gets the description value for this ServiceSummary.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this ServiceSummary.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the lastDate value for this ServiceSummary.
     * 
     * @return lastDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getLastDate() {
        return lastDate;
    }


    /**
     * Sets the lastDate value for this ServiceSummary.
     * 
     * @param lastDate
     */
    public void setLastDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset lastDate) {
        this.lastDate = lastDate;
    }


    /**
     * Gets the nextDate value for this ServiceSummary.
     * 
     * @return nextDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getNextDate() {
        return nextDate;
    }


    /**
     * Sets the nextDate value for this ServiceSummary.
     * 
     * @param nextDate
     */
    public void setNextDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset nextDate) {
        this.nextDate = nextDate;
    }


    /**
     * Gets the resolutionCodeId value for this ServiceSummary.
     * 
     * @return resolutionCodeId
     */
    public java.lang.Integer getResolutionCodeId() {
        return resolutionCodeId;
    }


    /**
     * Sets the resolutionCodeId value for this ServiceSummary.
     * 
     * @param resolutionCodeId
     */
    public void setResolutionCodeId(java.lang.Integer resolutionCodeId) {
        this.resolutionCodeId = resolutionCodeId;
    }


    /**
     * Gets the resolutionCodeName value for this ServiceSummary.
     * 
     * @return resolutionCodeName
     */
    public java.lang.String getResolutionCodeName() {
        return resolutionCodeName;
    }


    /**
     * Sets the resolutionCodeName value for this ServiceSummary.
     * 
     * @param resolutionCodeName
     */
    public void setResolutionCodeName(java.lang.String resolutionCodeName) {
        this.resolutionCodeName = resolutionCodeName;
    }


    /**
     * Gets the scheduleDescription value for this ServiceSummary.
     * 
     * @return scheduleDescription
     */
    public java.lang.String getScheduleDescription() {
        return scheduleDescription;
    }


    /**
     * Sets the scheduleDescription value for this ServiceSummary.
     * 
     * @param scheduleDescription
     */
    public void setScheduleDescription(java.lang.String scheduleDescription) {
        this.scheduleDescription = scheduleDescription;
    }


    /**
     * Gets the serviceId value for this ServiceSummary.
     * 
     * @return serviceId
     */
    public java.lang.Integer getServiceId() {
        return serviceId;
    }


    /**
     * Sets the serviceId value for this ServiceSummary.
     * 
     * @param serviceId
     */
    public void setServiceId(java.lang.Integer serviceId) {
        this.serviceId = serviceId;
    }


    /**
     * Gets the serviceName value for this ServiceSummary.
     * 
     * @return serviceName
     */
    public java.lang.String getServiceName() {
        return serviceName;
    }


    /**
     * Sets the serviceName value for this ServiceSummary.
     * 
     * @param serviceName
     */
    public void setServiceName(java.lang.String serviceName) {
        this.serviceName = serviceName;
    }


    /**
     * Gets the serviceUnitId value for this ServiceSummary.
     * 
     * @return serviceUnitId
     */
    public java.lang.Integer getServiceUnitId() {
        return serviceUnitId;
    }


    /**
     * Sets the serviceUnitId value for this ServiceSummary.
     * 
     * @param serviceUnitId
     */
    public void setServiceUnitId(java.lang.Integer serviceUnitId) {
        this.serviceUnitId = serviceUnitId;
    }


    /**
     * Gets the stateId value for this ServiceSummary.
     * 
     * @return stateId
     */
    public java.lang.Integer getStateId() {
        return stateId;
    }


    /**
     * Sets the stateId value for this ServiceSummary.
     * 
     * @param stateId
     */
    public void setStateId(java.lang.Integer stateId) {
        this.stateId = stateId;
    }


    /**
     * Gets the stateName value for this ServiceSummary.
     * 
     * @return stateName
     */
    public java.lang.String getStateName() {
        return stateName;
    }


    /**
     * Sets the stateName value for this ServiceSummary.
     * 
     * @param stateName
     */
    public void setStateName(java.lang.String stateName) {
        this.stateName = stateName;
    }


    /**
     * Gets the taskTypeId value for this ServiceSummary.
     * 
     * @return taskTypeId
     */
    public java.lang.Integer getTaskTypeId() {
        return taskTypeId;
    }


    /**
     * Sets the taskTypeId value for this ServiceSummary.
     * 
     * @param taskTypeId
     */
    public void setTaskTypeId(java.lang.Integer taskTypeId) {
        this.taskTypeId = taskTypeId;
    }


    /**
     * Gets the taskTypeName value for this ServiceSummary.
     * 
     * @return taskTypeName
     */
    public java.lang.String getTaskTypeName() {
        return taskTypeName;
    }


    /**
     * Sets the taskTypeName value for this ServiceSummary.
     * 
     * @param taskTypeName
     */
    public void setTaskTypeName(java.lang.String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceSummary)) return false;
        ServiceSummary other = (ServiceSummary) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.lastDate==null && other.getLastDate()==null) || 
             (this.lastDate!=null &&
              this.lastDate.equals(other.getLastDate()))) &&
            ((this.nextDate==null && other.getNextDate()==null) || 
             (this.nextDate!=null &&
              this.nextDate.equals(other.getNextDate()))) &&
            ((this.resolutionCodeId==null && other.getResolutionCodeId()==null) || 
             (this.resolutionCodeId!=null &&
              this.resolutionCodeId.equals(other.getResolutionCodeId()))) &&
            ((this.resolutionCodeName==null && other.getResolutionCodeName()==null) || 
             (this.resolutionCodeName!=null &&
              this.resolutionCodeName.equals(other.getResolutionCodeName()))) &&
            ((this.scheduleDescription==null && other.getScheduleDescription()==null) || 
             (this.scheduleDescription!=null &&
              this.scheduleDescription.equals(other.getScheduleDescription()))) &&
            ((this.serviceId==null && other.getServiceId()==null) || 
             (this.serviceId!=null &&
              this.serviceId.equals(other.getServiceId()))) &&
            ((this.serviceName==null && other.getServiceName()==null) || 
             (this.serviceName!=null &&
              this.serviceName.equals(other.getServiceName()))) &&
            ((this.serviceUnitId==null && other.getServiceUnitId()==null) || 
             (this.serviceUnitId!=null &&
              this.serviceUnitId.equals(other.getServiceUnitId()))) &&
            ((this.stateId==null && other.getStateId()==null) || 
             (this.stateId!=null &&
              this.stateId.equals(other.getStateId()))) &&
            ((this.stateName==null && other.getStateName()==null) || 
             (this.stateName!=null &&
              this.stateName.equals(other.getStateName()))) &&
            ((this.taskTypeId==null && other.getTaskTypeId()==null) || 
             (this.taskTypeId!=null &&
              this.taskTypeId.equals(other.getTaskTypeId()))) &&
            ((this.taskTypeName==null && other.getTaskTypeName()==null) || 
             (this.taskTypeName!=null &&
              this.taskTypeName.equals(other.getTaskTypeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getLastDate() != null) {
            _hashCode += getLastDate().hashCode();
        }
        if (getNextDate() != null) {
            _hashCode += getNextDate().hashCode();
        }
        if (getResolutionCodeId() != null) {
            _hashCode += getResolutionCodeId().hashCode();
        }
        if (getResolutionCodeName() != null) {
            _hashCode += getResolutionCodeName().hashCode();
        }
        if (getScheduleDescription() != null) {
            _hashCode += getScheduleDescription().hashCode();
        }
        if (getServiceId() != null) {
            _hashCode += getServiceId().hashCode();
        }
        if (getServiceName() != null) {
            _hashCode += getServiceName().hashCode();
        }
        if (getServiceUnitId() != null) {
            _hashCode += getServiceUnitId().hashCode();
        }
        if (getStateId() != null) {
            _hashCode += getStateId().hashCode();
        }
        if (getStateName() != null) {
            _hashCode += getStateName().hashCode();
        }
        if (getTaskTypeId() != null) {
            _hashCode += getTaskTypeId().hashCode();
        }
        if (getTaskTypeName() != null) {
            _hashCode += getTaskTypeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceSummary.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceSummary"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "LastDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nextDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "NextDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resolutionCodeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCodeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resolutionCodeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCodeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduleDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ScheduleDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceUnitId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
