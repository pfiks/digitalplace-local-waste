/**
 * ServiceTaskScheduleAllocation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceTaskScheduleAllocation  implements java.io.Serializable {
    private java.lang.Integer roundGroupId;

    private java.lang.String roundGroupName;

    private java.lang.Integer roundId;

    private java.lang.Integer roundLegId;

    private java.lang.String roundLegName;

    private java.lang.String roundName;

    private java.lang.String type;

    public ServiceTaskScheduleAllocation() {
    }

    public ServiceTaskScheduleAllocation(
           java.lang.Integer roundGroupId,
           java.lang.String roundGroupName,
           java.lang.Integer roundId,
           java.lang.Integer roundLegId,
           java.lang.String roundLegName,
           java.lang.String roundName,
           java.lang.String type) {
           this.roundGroupId = roundGroupId;
           this.roundGroupName = roundGroupName;
           this.roundId = roundId;
           this.roundLegId = roundLegId;
           this.roundLegName = roundLegName;
           this.roundName = roundName;
           this.type = type;
    }


    /**
     * Gets the roundGroupId value for this ServiceTaskScheduleAllocation.
     * 
     * @return roundGroupId
     */
    public java.lang.Integer getRoundGroupId() {
        return roundGroupId;
    }


    /**
     * Sets the roundGroupId value for this ServiceTaskScheduleAllocation.
     * 
     * @param roundGroupId
     */
    public void setRoundGroupId(java.lang.Integer roundGroupId) {
        this.roundGroupId = roundGroupId;
    }


    /**
     * Gets the roundGroupName value for this ServiceTaskScheduleAllocation.
     * 
     * @return roundGroupName
     */
    public java.lang.String getRoundGroupName() {
        return roundGroupName;
    }


    /**
     * Sets the roundGroupName value for this ServiceTaskScheduleAllocation.
     * 
     * @param roundGroupName
     */
    public void setRoundGroupName(java.lang.String roundGroupName) {
        this.roundGroupName = roundGroupName;
    }


    /**
     * Gets the roundId value for this ServiceTaskScheduleAllocation.
     * 
     * @return roundId
     */
    public java.lang.Integer getRoundId() {
        return roundId;
    }


    /**
     * Sets the roundId value for this ServiceTaskScheduleAllocation.
     * 
     * @param roundId
     */
    public void setRoundId(java.lang.Integer roundId) {
        this.roundId = roundId;
    }


    /**
     * Gets the roundLegId value for this ServiceTaskScheduleAllocation.
     * 
     * @return roundLegId
     */
    public java.lang.Integer getRoundLegId() {
        return roundLegId;
    }


    /**
     * Sets the roundLegId value for this ServiceTaskScheduleAllocation.
     * 
     * @param roundLegId
     */
    public void setRoundLegId(java.lang.Integer roundLegId) {
        this.roundLegId = roundLegId;
    }


    /**
     * Gets the roundLegName value for this ServiceTaskScheduleAllocation.
     * 
     * @return roundLegName
     */
    public java.lang.String getRoundLegName() {
        return roundLegName;
    }


    /**
     * Sets the roundLegName value for this ServiceTaskScheduleAllocation.
     * 
     * @param roundLegName
     */
    public void setRoundLegName(java.lang.String roundLegName) {
        this.roundLegName = roundLegName;
    }


    /**
     * Gets the roundName value for this ServiceTaskScheduleAllocation.
     * 
     * @return roundName
     */
    public java.lang.String getRoundName() {
        return roundName;
    }


    /**
     * Sets the roundName value for this ServiceTaskScheduleAllocation.
     * 
     * @param roundName
     */
    public void setRoundName(java.lang.String roundName) {
        this.roundName = roundName;
    }


    /**
     * Gets the type value for this ServiceTaskScheduleAllocation.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this ServiceTaskScheduleAllocation.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceTaskScheduleAllocation)) return false;
        ServiceTaskScheduleAllocation other = (ServiceTaskScheduleAllocation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.roundGroupId==null && other.getRoundGroupId()==null) || 
             (this.roundGroupId!=null &&
              this.roundGroupId.equals(other.getRoundGroupId()))) &&
            ((this.roundGroupName==null && other.getRoundGroupName()==null) || 
             (this.roundGroupName!=null &&
              this.roundGroupName.equals(other.getRoundGroupName()))) &&
            ((this.roundId==null && other.getRoundId()==null) || 
             (this.roundId!=null &&
              this.roundId.equals(other.getRoundId()))) &&
            ((this.roundLegId==null && other.getRoundLegId()==null) || 
             (this.roundLegId!=null &&
              this.roundLegId.equals(other.getRoundLegId()))) &&
            ((this.roundLegName==null && other.getRoundLegName()==null) || 
             (this.roundLegName!=null &&
              this.roundLegName.equals(other.getRoundLegName()))) &&
            ((this.roundName==null && other.getRoundName()==null) || 
             (this.roundName!=null &&
              this.roundName.equals(other.getRoundName()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRoundGroupId() != null) {
            _hashCode += getRoundGroupId().hashCode();
        }
        if (getRoundGroupName() != null) {
            _hashCode += getRoundGroupName().hashCode();
        }
        if (getRoundId() != null) {
            _hashCode += getRoundId().hashCode();
        }
        if (getRoundLegId() != null) {
            _hashCode += getRoundLegId().hashCode();
        }
        if (getRoundLegName() != null) {
            _hashCode += getRoundLegName().hashCode();
        }
        if (getRoundName() != null) {
            _hashCode += getRoundName().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceTaskScheduleAllocation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTaskScheduleAllocation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roundGroupId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "RoundGroupId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roundGroupName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "RoundGroupName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roundId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "RoundId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roundLegId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "RoundLegId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roundLegName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "RoundLegName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("roundName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "RoundName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
