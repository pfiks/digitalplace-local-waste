/**
 * PointQuery.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class PointQuery  implements java.io.Serializable {
    private java.lang.String pointType;

    private java.lang.String postcode;

    private java.lang.String streetName;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] typeRefs;

    private com.placecube.digitalplace.local.waste.echo.axis.GeoPoint near;

    public PointQuery() {
    }

    public PointQuery(
           java.lang.String pointType,
           java.lang.String postcode,
           java.lang.String streetName,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] typeRefs,
           com.placecube.digitalplace.local.waste.echo.axis.GeoPoint near) {
           this.pointType = pointType;
           this.postcode = postcode;
           this.streetName = streetName;
           this.typeRefs = typeRefs;
           this.near = near;
    }


    /**
     * Gets the pointType value for this PointQuery.
     * 
     * @return pointType
     */
    public java.lang.String getPointType() {
        return pointType;
    }


    /**
     * Sets the pointType value for this PointQuery.
     * 
     * @param pointType
     */
    public void setPointType(java.lang.String pointType) {
        this.pointType = pointType;
    }


    /**
     * Gets the postcode value for this PointQuery.
     * 
     * @return postcode
     */
    public java.lang.String getPostcode() {
        return postcode;
    }


    /**
     * Sets the postcode value for this PointQuery.
     * 
     * @param postcode
     */
    public void setPostcode(java.lang.String postcode) {
        this.postcode = postcode;
    }


    /**
     * Gets the streetName value for this PointQuery.
     * 
     * @return streetName
     */
    public java.lang.String getStreetName() {
        return streetName;
    }


    /**
     * Sets the streetName value for this PointQuery.
     * 
     * @param streetName
     */
    public void setStreetName(java.lang.String streetName) {
        this.streetName = streetName;
    }


    /**
     * Gets the typeRefs value for this PointQuery.
     * 
     * @return typeRefs
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] getTypeRefs() {
        return typeRefs;
    }


    /**
     * Sets the typeRefs value for this PointQuery.
     * 
     * @param typeRefs
     */
    public void setTypeRefs(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef[] typeRefs) {
        this.typeRefs = typeRefs;
    }


    /**
     * Gets the near value for this PointQuery.
     * 
     * @return near
     */
    public com.placecube.digitalplace.local.waste.echo.axis.GeoPoint getNear() {
        return near;
    }


    /**
     * Sets the near value for this PointQuery.
     * 
     * @param near
     */
    public void setNear(com.placecube.digitalplace.local.waste.echo.axis.GeoPoint near) {
        this.near = near;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PointQuery)) return false;
        PointQuery other = (PointQuery) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pointType==null && other.getPointType()==null) || 
             (this.pointType!=null &&
              this.pointType.equals(other.getPointType()))) &&
            ((this.postcode==null && other.getPostcode()==null) || 
             (this.postcode!=null &&
              this.postcode.equals(other.getPostcode()))) &&
            ((this.streetName==null && other.getStreetName()==null) || 
             (this.streetName!=null &&
              this.streetName.equals(other.getStreetName()))) &&
            ((this.typeRefs==null && other.getTypeRefs()==null) || 
             (this.typeRefs!=null &&
              java.util.Arrays.equals(this.typeRefs, other.getTypeRefs()))) &&
            ((this.near==null && other.getNear()==null) || 
             (this.near!=null &&
              this.near.equals(other.getNear())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPointType() != null) {
            _hashCode += getPointType().hashCode();
        }
        if (getPostcode() != null) {
            _hashCode += getPostcode().hashCode();
        }
        if (getStreetName() != null) {
            _hashCode += getStreetName().hashCode();
        }
        if (getTypeRefs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTypeRefs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTypeRefs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNear() != null) {
            _hashCode += getNear().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PointQuery.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointQuery"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Postcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("streetName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StreetName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeRefs");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TypeRefs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("near");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Near"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GeoPoint"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
