/**
 * ServiceUnit.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ServiceUnit  extends com.placecube.digitalplace.local.waste.echo.axis.ExtensibleObject  implements java.io.Serializable {
    private java.lang.String clientReference;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate;

    private java.lang.String name;

    private java.lang.Integer serviceId;

    private java.lang.String serviceName;

    private com.placecube.digitalplace.local.waste.echo.axis.ServiceTask[] serviceTasks;

    private com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitPoint[] serviceUnitPoints;

    private java.lang.Integer serviceUnitTypeId;

    private com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate;

    public ServiceUnit() {
    }

    public ServiceUnit(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data,
           java.lang.String clientReference,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate,
           java.lang.String name,
           java.lang.Integer serviceId,
           java.lang.String serviceName,
           com.placecube.digitalplace.local.waste.echo.axis.ServiceTask[] serviceTasks,
           com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitPoint[] serviceUnitPoints,
           java.lang.Integer serviceUnitTypeId,
           com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate) {
        super(
            guid,
            id,
            data);
        this.clientReference = clientReference;
        this.endDate = endDate;
        this.name = name;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.serviceTasks = serviceTasks;
        this.serviceUnitPoints = serviceUnitPoints;
        this.serviceUnitTypeId = serviceUnitTypeId;
        this.startDate = startDate;
    }


    /**
     * Gets the clientReference value for this ServiceUnit.
     * 
     * @return clientReference
     */
    public java.lang.String getClientReference() {
        return clientReference;
    }


    /**
     * Sets the clientReference value for this ServiceUnit.
     * 
     * @param clientReference
     */
    public void setClientReference(java.lang.String clientReference) {
        this.clientReference = clientReference;
    }


    /**
     * Gets the endDate value for this ServiceUnit.
     * 
     * @return endDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this ServiceUnit.
     * 
     * @param endDate
     */
    public void setEndDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the name value for this ServiceUnit.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this ServiceUnit.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the serviceId value for this ServiceUnit.
     * 
     * @return serviceId
     */
    public java.lang.Integer getServiceId() {
        return serviceId;
    }


    /**
     * Sets the serviceId value for this ServiceUnit.
     * 
     * @param serviceId
     */
    public void setServiceId(java.lang.Integer serviceId) {
        this.serviceId = serviceId;
    }


    /**
     * Gets the serviceName value for this ServiceUnit.
     * 
     * @return serviceName
     */
    public java.lang.String getServiceName() {
        return serviceName;
    }


    /**
     * Sets the serviceName value for this ServiceUnit.
     * 
     * @param serviceName
     */
    public void setServiceName(java.lang.String serviceName) {
        this.serviceName = serviceName;
    }


    /**
     * Gets the serviceTasks value for this ServiceUnit.
     * 
     * @return serviceTasks
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceTask[] getServiceTasks() {
        return serviceTasks;
    }


    /**
     * Sets the serviceTasks value for this ServiceUnit.
     * 
     * @param serviceTasks
     */
    public void setServiceTasks(com.placecube.digitalplace.local.waste.echo.axis.ServiceTask[] serviceTasks) {
        this.serviceTasks = serviceTasks;
    }


    /**
     * Gets the serviceUnitPoints value for this ServiceUnit.
     * 
     * @return serviceUnitPoints
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitPoint[] getServiceUnitPoints() {
        return serviceUnitPoints;
    }


    /**
     * Sets the serviceUnitPoints value for this ServiceUnit.
     * 
     * @param serviceUnitPoints
     */
    public void setServiceUnitPoints(com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitPoint[] serviceUnitPoints) {
        this.serviceUnitPoints = serviceUnitPoints;
    }


    /**
     * Gets the serviceUnitTypeId value for this ServiceUnit.
     * 
     * @return serviceUnitTypeId
     */
    public java.lang.Integer getServiceUnitTypeId() {
        return serviceUnitTypeId;
    }


    /**
     * Sets the serviceUnitTypeId value for this ServiceUnit.
     * 
     * @param serviceUnitTypeId
     */
    public void setServiceUnitTypeId(java.lang.Integer serviceUnitTypeId) {
        this.serviceUnitTypeId = serviceUnitTypeId;
    }


    /**
     * Gets the startDate value for this ServiceUnit.
     * 
     * @return startDate
     */
    public com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this ServiceUnit.
     * 
     * @param startDate
     */
    public void setStartDate(com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset startDate) {
        this.startDate = startDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceUnit)) return false;
        ServiceUnit other = (ServiceUnit) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.clientReference==null && other.getClientReference()==null) || 
             (this.clientReference!=null &&
              this.clientReference.equals(other.getClientReference()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.serviceId==null && other.getServiceId()==null) || 
             (this.serviceId!=null &&
              this.serviceId.equals(other.getServiceId()))) &&
            ((this.serviceName==null && other.getServiceName()==null) || 
             (this.serviceName!=null &&
              this.serviceName.equals(other.getServiceName()))) &&
            ((this.serviceTasks==null && other.getServiceTasks()==null) || 
             (this.serviceTasks!=null &&
              java.util.Arrays.equals(this.serviceTasks, other.getServiceTasks()))) &&
            ((this.serviceUnitPoints==null && other.getServiceUnitPoints()==null) || 
             (this.serviceUnitPoints!=null &&
              java.util.Arrays.equals(this.serviceUnitPoints, other.getServiceUnitPoints()))) &&
            ((this.serviceUnitTypeId==null && other.getServiceUnitTypeId()==null) || 
             (this.serviceUnitTypeId!=null &&
              this.serviceUnitTypeId.equals(other.getServiceUnitTypeId()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getClientReference() != null) {
            _hashCode += getClientReference().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getServiceId() != null) {
            _hashCode += getServiceId().hashCode();
        }
        if (getServiceName() != null) {
            _hashCode += getServiceName().hashCode();
        }
        if (getServiceTasks() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServiceTasks());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServiceTasks(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getServiceUnitPoints() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getServiceUnitPoints());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getServiceUnitPoints(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getServiceUnitTypeId() != null) {
            _hashCode += getServiceUnitTypeId().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceUnit.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnit"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ClientReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceTasks");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTasks"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTask"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceTask"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceUnitPoints");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitPoints"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitPoint"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitPoint"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceUnitTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ServiceUnitTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
