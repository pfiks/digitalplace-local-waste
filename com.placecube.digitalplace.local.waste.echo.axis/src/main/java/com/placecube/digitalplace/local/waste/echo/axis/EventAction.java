/**
 * EventAction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class EventAction  implements java.io.Serializable {
    private java.lang.Integer actionTypeId;

    private com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef eventRef;

    public EventAction() {
    }

    public EventAction(
           java.lang.Integer actionTypeId,
           com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef eventRef) {
           this.actionTypeId = actionTypeId;
           this.data = data;
           this.eventRef = eventRef;
    }


    /**
     * Gets the actionTypeId value for this EventAction.
     * 
     * @return actionTypeId
     */
    public java.lang.Integer getActionTypeId() {
        return actionTypeId;
    }


    /**
     * Sets the actionTypeId value for this EventAction.
     * 
     * @param actionTypeId
     */
    public void setActionTypeId(java.lang.Integer actionTypeId) {
        this.actionTypeId = actionTypeId;
    }


    /**
     * Gets the data value for this EventAction.
     * 
     * @return data
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] getData() {
        return data;
    }


    /**
     * Sets the data value for this EventAction.
     * 
     * @param data
     */
    public void setData(com.placecube.digitalplace.local.waste.echo.axis.ExtensibleDatum[] data) {
        this.data = data;
    }


    /**
     * Gets the eventRef value for this EventAction.
     * 
     * @return eventRef
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getEventRef() {
        return eventRef;
    }


    /**
     * Sets the eventRef value for this EventAction.
     * 
     * @param eventRef
     */
    public void setEventRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef eventRef) {
        this.eventRef = eventRef;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventAction)) return false;
        EventAction other = (EventAction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actionTypeId==null && other.getActionTypeId()==null) || 
             (this.actionTypeId!=null &&
              this.actionTypeId.equals(other.getActionTypeId()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              java.util.Arrays.equals(this.data, other.getData()))) &&
            ((this.eventRef==null && other.getEventRef()==null) || 
             (this.eventRef!=null &&
              this.eventRef.equals(other.getEventRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActionTypeId() != null) {
            _hashCode += getActionTypeId().hashCode();
        }
        if (getData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEventRef() != null) {
            _hashCode += getEventRef().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventAction.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventAction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ActionTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ExtensibleDatum"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "EventRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
