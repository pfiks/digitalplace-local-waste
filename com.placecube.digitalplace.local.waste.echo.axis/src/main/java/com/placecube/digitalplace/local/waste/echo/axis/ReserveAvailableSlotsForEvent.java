/**
 * ReserveAvailableSlotsForEvent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class ReserveAvailableSlotsForEvent  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.Event event;

    private com.placecube.digitalplace.local.waste.echo.axis.ReservationParameters parameters;

    public ReserveAvailableSlotsForEvent() {
    }

    public ReserveAvailableSlotsForEvent(
           com.placecube.digitalplace.local.waste.echo.axis.Event event,
           com.placecube.digitalplace.local.waste.echo.axis.ReservationParameters parameters) {
           this.event = event;
           this.parameters = parameters;
    }


    /**
     * Gets the event value for this ReserveAvailableSlotsForEvent.
     * 
     * @return event
     */
    public com.placecube.digitalplace.local.waste.echo.axis.Event getEvent() {
        return event;
    }


    /**
     * Sets the event value for this ReserveAvailableSlotsForEvent.
     * 
     * @param event
     */
    public void setEvent(com.placecube.digitalplace.local.waste.echo.axis.Event event) {
        this.event = event;
    }


    /**
     * Gets the parameters value for this ReserveAvailableSlotsForEvent.
     * 
     * @return parameters
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ReservationParameters getParameters() {
        return parameters;
    }


    /**
     * Sets the parameters value for this ReserveAvailableSlotsForEvent.
     * 
     * @param parameters
     */
    public void setParameters(com.placecube.digitalplace.local.waste.echo.axis.ReservationParameters parameters) {
        this.parameters = parameters;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReserveAvailableSlotsForEvent)) return false;
        ReserveAvailableSlotsForEvent other = (ReserveAvailableSlotsForEvent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.event==null && other.getEvent()==null) || 
             (this.event!=null &&
              this.event.equals(other.getEvent()))) &&
            ((this.parameters==null && other.getParameters()==null) || 
             (this.parameters!=null &&
              this.parameters.equals(other.getParameters())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEvent() != null) {
            _hashCode += getEvent().hashCode();
        }
        if (getParameters() != null) {
            _hashCode += getParameters().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReserveAvailableSlotsForEvent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">ReserveAvailableSlotsForEvent"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("event");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "event"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Event"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameters");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "parameters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ReservationParameters"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
