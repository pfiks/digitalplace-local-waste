/**
 * PointInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class PointInfo  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private java.lang.String description;

    private java.lang.String pointType;

    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef sharedRef;

    public PointInfo() {
    }

    public PointInfo(
           java.lang.String guid,
           java.lang.Integer id,
           java.lang.String description,
           java.lang.String pointType,
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef sharedRef) {
        super(
            guid,
            id);
        this.description = description;
        this.pointType = pointType;
        this.sharedRef = sharedRef;
    }


    /**
     * Gets the description value for this PointInfo.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this PointInfo.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the pointType value for this PointInfo.
     * 
     * @return pointType
     */
    public java.lang.String getPointType() {
        return pointType;
    }


    /**
     * Sets the pointType value for this PointInfo.
     * 
     * @param pointType
     */
    public void setPointType(java.lang.String pointType) {
        this.pointType = pointType;
    }


    /**
     * Gets the sharedRef value for this PointInfo.
     * 
     * @return sharedRef
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getSharedRef() {
        return sharedRef;
    }


    /**
     * Sets the sharedRef value for this PointInfo.
     * 
     * @param sharedRef
     */
    public void setSharedRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef sharedRef) {
        this.sharedRef = sharedRef;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PointInfo)) return false;
        PointInfo other = (PointInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.pointType==null && other.getPointType()==null) || 
             (this.pointType!=null &&
              this.pointType.equals(other.getPointType()))) &&
            ((this.sharedRef==null && other.getSharedRef()==null) || 
             (this.sharedRef!=null &&
              this.sharedRef.equals(other.getSharedRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getPointType() != null) {
            _hashCode += getPointType().hashCode();
        }
        if (getSharedRef() != null) {
            _hashCode += getSharedRef().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PointInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "PointType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sharedRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "SharedRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
