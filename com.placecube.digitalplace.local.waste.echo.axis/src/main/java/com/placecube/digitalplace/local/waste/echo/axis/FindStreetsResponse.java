/**
 * FindStreetsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class FindStreetsResponse  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.Street[] findStreetsResult;

    public FindStreetsResponse() {
    }

    public FindStreetsResponse(
           com.placecube.digitalplace.local.waste.echo.axis.Street[] findStreetsResult) {
           this.findStreetsResult = findStreetsResult;
    }


    /**
     * Gets the findStreetsResult value for this FindStreetsResponse.
     * 
     * @return findStreetsResult
     */
    public com.placecube.digitalplace.local.waste.echo.axis.Street[] getFindStreetsResult() {
        return findStreetsResult;
    }


    /**
     * Sets the findStreetsResult value for this FindStreetsResponse.
     * 
     * @param findStreetsResult
     */
    public void setFindStreetsResult(com.placecube.digitalplace.local.waste.echo.axis.Street[] findStreetsResult) {
        this.findStreetsResult = findStreetsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindStreetsResponse)) return false;
        FindStreetsResponse other = (FindStreetsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.findStreetsResult==null && other.getFindStreetsResult()==null) || 
             (this.findStreetsResult!=null &&
              java.util.Arrays.equals(this.findStreetsResult, other.getFindStreetsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFindStreetsResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFindStreetsResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFindStreetsResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindStreetsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">FindStreetsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("findStreetsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "FindStreetsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Street"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Street"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
