/**
 * State.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class State  extends com.placecube.digitalplace.local.waste.echo.axis.EchoObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ActionType[] actions;

    private java.lang.String coreState;

    private java.lang.String name;

    private int[] nextStateIds;

    private com.placecube.digitalplace.local.waste.echo.axis.StateResolutionCode[] resolutionCodes;

    public State() {
    }

    public State(
           java.lang.String guid,
           java.lang.Integer id,
           com.placecube.digitalplace.local.waste.echo.axis.ActionType[] actions,
           java.lang.String coreState,
           java.lang.String name,
           int[] nextStateIds,
           com.placecube.digitalplace.local.waste.echo.axis.StateResolutionCode[] resolutionCodes) {
        super(
            guid,
            id);
        this.actions = actions;
        this.coreState = coreState;
        this.name = name;
        this.nextStateIds = nextStateIds;
        this.resolutionCodes = resolutionCodes;
    }


    /**
     * Gets the actions value for this State.
     * 
     * @return actions
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ActionType[] getActions() {
        return actions;
    }


    /**
     * Sets the actions value for this State.
     * 
     * @param actions
     */
    public void setActions(com.placecube.digitalplace.local.waste.echo.axis.ActionType[] actions) {
        this.actions = actions;
    }


    /**
     * Gets the coreState value for this State.
     * 
     * @return coreState
     */
    public java.lang.String getCoreState() {
        return coreState;
    }


    /**
     * Sets the coreState value for this State.
     * 
     * @param coreState
     */
    public void setCoreState(java.lang.String coreState) {
        this.coreState = coreState;
    }


    /**
     * Gets the name value for this State.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this State.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the nextStateIds value for this State.
     * 
     * @return nextStateIds
     */
    public int[] getNextStateIds() {
        return nextStateIds;
    }


    /**
     * Sets the nextStateIds value for this State.
     * 
     * @param nextStateIds
     */
    public void setNextStateIds(int[] nextStateIds) {
        this.nextStateIds = nextStateIds;
    }


    /**
     * Gets the resolutionCodes value for this State.
     * 
     * @return resolutionCodes
     */
    public com.placecube.digitalplace.local.waste.echo.axis.StateResolutionCode[] getResolutionCodes() {
        return resolutionCodes;
    }


    /**
     * Sets the resolutionCodes value for this State.
     * 
     * @param resolutionCodes
     */
    public void setResolutionCodes(com.placecube.digitalplace.local.waste.echo.axis.StateResolutionCode[] resolutionCodes) {
        this.resolutionCodes = resolutionCodes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof State)) return false;
        State other = (State) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.actions==null && other.getActions()==null) || 
             (this.actions!=null &&
              java.util.Arrays.equals(this.actions, other.getActions()))) &&
            ((this.coreState==null && other.getCoreState()==null) || 
             (this.coreState!=null &&
              this.coreState.equals(other.getCoreState()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.nextStateIds==null && other.getNextStateIds()==null) || 
             (this.nextStateIds!=null &&
              java.util.Arrays.equals(this.nextStateIds, other.getNextStateIds()))) &&
            ((this.resolutionCodes==null && other.getResolutionCodes()==null) || 
             (this.resolutionCodes!=null &&
              java.util.Arrays.equals(this.resolutionCodes, other.getResolutionCodes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getActions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getActions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getActions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCoreState() != null) {
            _hashCode += getCoreState().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getNextStateIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNextStateIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNextStateIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResolutionCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResolutionCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResolutionCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(State.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "State"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Actions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ActionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ActionType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coreState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "CoreState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nextStateIds");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "NextStateIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resolutionCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateResolutionCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "StateResolutionCode"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
