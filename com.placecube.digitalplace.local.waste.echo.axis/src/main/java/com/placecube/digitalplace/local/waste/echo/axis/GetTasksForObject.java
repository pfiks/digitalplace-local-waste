/**
 * GetTasksForObject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetTasksForObject  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef;

    private com.placecube.digitalplace.local.waste.echo.axis.TaskOptions options;

    public GetTasksForObject() {
    }

    public GetTasksForObject(
           com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef,
           com.placecube.digitalplace.local.waste.echo.axis.TaskOptions options) {
           this.objectRef = objectRef;
           this.options = options;
    }


    /**
     * Gets the objectRef value for this GetTasksForObject.
     * 
     * @return objectRef
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ObjectRef getObjectRef() {
        return objectRef;
    }


    /**
     * Sets the objectRef value for this GetTasksForObject.
     * 
     * @param objectRef
     */
    public void setObjectRef(com.placecube.digitalplace.local.waste.echo.axis.ObjectRef objectRef) {
        this.objectRef = objectRef;
    }


    /**
     * Gets the options value for this GetTasksForObject.
     * 
     * @return options
     */
    public com.placecube.digitalplace.local.waste.echo.axis.TaskOptions getOptions() {
        return options;
    }


    /**
     * Sets the options value for this GetTasksForObject.
     * 
     * @param options
     */
    public void setOptions(com.placecube.digitalplace.local.waste.echo.axis.TaskOptions options) {
        this.options = options;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTasksForObject)) return false;
        GetTasksForObject other = (GetTasksForObject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.objectRef==null && other.getObjectRef()==null) || 
             (this.objectRef!=null &&
              this.objectRef.equals(other.getObjectRef()))) &&
            ((this.options==null && other.getOptions()==null) || 
             (this.options!=null &&
              this.options.equals(other.getOptions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getObjectRef() != null) {
            _hashCode += getObjectRef().hashCode();
        }
        if (getOptions() != null) {
            _hashCode += getOptions().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTasksForObject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetTasksForObject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("objectRef");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "objectRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ObjectRef"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("options");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "options"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "TaskOptions"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
