/**
 * GetResolutionCodesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.placecube.digitalplace.local.waste.echo.axis;

public class GetResolutionCodesResponse  implements java.io.Serializable {
    private com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[] getResolutionCodesResult;

    public GetResolutionCodesResponse() {
    }

    public GetResolutionCodesResponse(
           com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[] getResolutionCodesResult) {
           this.getResolutionCodesResult = getResolutionCodesResult;
    }


    /**
     * Gets the getResolutionCodesResult value for this GetResolutionCodesResponse.
     * 
     * @return getResolutionCodesResult
     */
    public com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[] getGetResolutionCodesResult() {
        return getResolutionCodesResult;
    }


    /**
     * Sets the getResolutionCodesResult value for this GetResolutionCodesResponse.
     * 
     * @param getResolutionCodesResult
     */
    public void setGetResolutionCodesResult(com.placecube.digitalplace.local.waste.echo.axis.ResolutionCode[] getResolutionCodesResult) {
        this.getResolutionCodesResult = getResolutionCodesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetResolutionCodesResponse)) return false;
        GetResolutionCodesResponse other = (GetResolutionCodesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getResolutionCodesResult==null && other.getGetResolutionCodesResult()==null) || 
             (this.getResolutionCodesResult!=null &&
              java.util.Arrays.equals(this.getResolutionCodesResult, other.getGetResolutionCodesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetResolutionCodesResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetResolutionCodesResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetResolutionCodesResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetResolutionCodesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", ">GetResolutionCodesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getResolutionCodesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "GetResolutionCodesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.twistedfish.com/xmlns/echo/api/v1", "ResolutionCode"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
