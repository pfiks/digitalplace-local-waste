#!/bin/bash

# Define namespace-package mappings
namespace_packages=(
    "http://webservices.whitespacews.com/=com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews,"
    "http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Enums=com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums,"
    "http://schemas.datacontract.org/2004/07/=com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums,"
    "http://webservices.whitespacews.com/API=com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.webservices,"
    "http://webservices.whitespacews.com/API/=com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.webservices,"
    "http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Inputs=com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs,"
    "http://schemas.microsoft.com/2003/10/Serialization/=com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization,"
    "http://schemas.microsoft.com/2003/10/Serialization/Arrays=com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays,"
    "http://schemas.datacontract.org/2004/07=com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract,"
    "http://axis2.apache.org=com.placecube.digitalplace.local.waste.whitespace.axis2.apache,"
    "http://webservices.whitespacews.com/Contacts=com.placecube.digitalplace.local.waste.whitespace.axis2.whitespacews.contacts"
)

read -p "Enter the bin location of the axis2 installation (no trailing slash): " axis2_bin_folder

if [[ ! -z "$axis2_bin_folder" && -e $axis2_bin_folder ]]
then
	wsdl_location="${PWD}/whitespace.wsdl"
	cd $axis2_bin_folder
	ns2p_args=" -ns2p "
	for mapping in "${namespace_packages[@]}"; do
	    ns2p_args+=$mapping
	done

	
	command_string="$axis2_bin_folder/wsdl2java.sh -uri $wsdl_location -p com.placecube.digitalplace.local.waste.whitespace.axis2 -d adb -s -u $ns2p_args"

	echo "Running command:$command_string"
	$command_string
else
    echo "The code generator folder does not exist."
fi
