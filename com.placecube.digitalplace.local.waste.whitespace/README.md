# Digital Place Waste - Whitespace
## Introduction
This project contains an axis2 client for Whitespace webservices and a Liferay REST service to expose the Axis2 client calls.

## Generating the code
* Download the Axis2 code generation binary - https://axis.apache.org/axis2/java/core/download.html
* Extract this to a local folder and note the path
* Then, open a terminal to the "com.placecube.digitalplace.local.waste.whitespace/generate" folder
* Run "./generate.sh"
* When prompted enter the path of the folder where the Axis2 code generator binary was extracted
* The code generator will now run and place the code in <your_extract_folder>/src

## Copying the generated code to the relevant package
We need to copy the generate code from the src folder as follows:-
* The contents of "src/axis2" should replace the contents of "com.placecube.digitalplace.local.waste.whitespace/src/main/java/axis2"
* The contents of "src/com/placecube/digitalplace/local/waste/whitespace/axis2/" should replace the contents of "com.placecube.digitalplace.local.waste.whitespace/src/main/java/com/placecube/digitalplace/local/waste/whitespace/axis2"

## Custom namespace mappings
The "generate.sh" bash script contains a list of the custom namespace mappings.  These mappings ensure code from different namespaces defined in the wsdl, is generated in a specific java package.  If the whitespace wsdl has been updated; it is possible there may be new namespaces or some namespace removed.  If this is the case, then the generate.sh script needs to be modified to add/remove namespace mappings.  Add/remove the mappings from the array "namespace_packages".  Items should be separated by a comma.  The last item should omit the comma.

## Dependencies
Ensure the Axis2 gradle dependency version numbers match the version number the client code was was generated with.
E.g. If you download the code generation binary and it is version 1.8.2 then the gradle versions in build.gradle should be the same like the below dependencies:

	compileInclude group: 'org.apache.axis2', name: 'axis2-xmlbeans-codegen', version: '1.8.2'
	compileInclude group: 'org.apache.axis2', name: 'axis2-xmlbeans', version: '1.8.2'
	compileInclude group: 'org.apache.axis2', name: 'axis2-transport-http', version: '1.8.2'
	compileInclude group: 'org.apache.axis2', name: 'axis2-transport-local', version: '1.8.2'
	compileInclude group: 'org.apache.axis2', name: 'axis2-jaxws', version: '1.8.2'


