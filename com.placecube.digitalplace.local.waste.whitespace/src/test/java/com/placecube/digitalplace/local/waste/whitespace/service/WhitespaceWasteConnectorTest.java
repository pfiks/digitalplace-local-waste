package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ApiAdHocRoundInstance;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionByUprnAndDateResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionSlotsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteService;
import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;
import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetContext;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.WorksheetValidator;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceDateUtil;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WhitespaceDateUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WhitespaceWasteConnectorTest extends PowerMockito {

	private static final Set<BinCollection> BIN_COLLECTIONS = new HashSet<>();
	private static final Date DATE = new Date();
	private static final int QUANTITY = 3;
	private static final int NUMBER_OF_DATES_TO_RETURN = 10;
	private static final long CLASS_PK = 10;
	private static final long COMPANY_ID = 10;
	private static final String UPRN = "12345678";
	private static final String BIN_SIZE = "";
	private static final String BIN_TYPE = "";
	private static final String FIRST_NAME = "first";
	private static final String FORM_INSTANCE_ID = "5432";
	private static final String LAST_NAME = "last";
	private static final String SERVICE_TYPE = "type";
	private static final String SERVICE_ID = "54333";

	@InjectMocks
	private WhitespaceWasteConnector whitespaceWasteConnector;

	@Mock
	private GetCollectionByUprnAndDateResponse mockGetCollectionByUprnAndDateResponse;

	@Mock
	private ResponseParserService mockResponseParserService;

	@Mock
	private WhitespaceRequestService mockWhitespaceRequestService;

	@Mock
	private WhitespaceService mockWhitespaceService;

	@Mock
	private WhitespaceConfigurationService mockWhitespaceConfigurationService;

	@Mock
	private WhitespaceWasteService mockWhitespaceWasteService;

	@Mock
	private WhitespaceWorksheetService mockWhitespaceWorksheetService;

	@Mock
	private NewContainerRequest mockNewContainerRequest;

	@Mock
	private GardenWasteSubscription mockGardenWasteSubscription;

	@Mock
	private GetSiteCollectionsResponse mockGetSiteCollectionsResponse;

	@Mock
	private WasteSubscriptionResponse mockWasteSubscriptionResponse;

	@Mock
	private GetCollectionSlotsResponse mockGetCollectionSlotsResponse;

	@Mock
	private ObjectFactoryService mockObjectFactoryService;

	@Mock
	private ApiAdHocRoundInstance mockApiAdHocRoundInstance;

	@Mock
	private SiteService mockSiteService;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private WorksheetContext mockWorksheetContext;

	@Mock
	private BulkyWasteCollectionRequest mockBulkyWasteCollectionRequest;

	@Mock
	private WorksheetContext.WorksheetContextBuilder mockWorksheetContextBuilder;

	@Mock
	private WorksheetValidator mockWorksheetValidator;

	@Mock
	private WhitespaceWasteServiceUtil mockWhitespaceWasteServiceUtil;

	@Mock
	private MissedBinCollectionJob mockMissedBinCollectionJob;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordLocalService;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private BinCollection mockBinCollection;

	@Before
	public void activateSetup() {
		mockStatic(WhitespaceDateUtil.class);
	}

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockWhitespaceConfigurationService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = whitespaceWasteConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockWhitespaceConfigurationService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = whitespaceWasteConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRN_WhenCollectionDatesRetrievalFails_ThenthrowsWasteRetrievalException() throws Exception {

		String nextCollectionFromDate = "2022-11-02";
		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(nextCollectionFromDate);
		when(mockWhitespaceRequestService.getCollectionByUprnAndDate(COMPANY_ID, UPRN, nextCollectionFromDate)).thenThrow(new Exception());

		whitespaceWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenNoErrors_ThenReturnBinCollections() throws Exception {

		String nextCollectionFromDate = "2022-11-02";
		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(nextCollectionFromDate);
		when(mockWhitespaceRequestService.getCollectionByUprnAndDate(COMPANY_ID, UPRN, nextCollectionFromDate)).thenReturn(mockGetCollectionByUprnAndDateResponse);
		when(mockResponseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID)).thenReturn(BIN_COLLECTIONS);

		Set<BinCollection> result = whitespaceWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);

		assertEquals(BIN_COLLECTIONS, result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRNAndService_WhenCollectionDatesRetrievalFails_ThenthrowsWasteRetrievalException() throws Exception {

		String nextCollectionFromDate = "2022-11-02";
		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(nextCollectionFromDate);
		when(mockWhitespaceRequestService.getCollectionByUprnAndDate(COMPANY_ID, UPRN, nextCollectionFromDate)).thenThrow(new Exception());

		whitespaceWasteConnector.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID);
	}

	@Test
	public void getBinCollectionDatesByUPRNAndService_WhenNoErrors_ThenReturnBinCollectionsFilteredByServiceName() throws Exception {
		final Set<BinCollection> filteredBinCollections = new HashSet<>();
		filteredBinCollections.add(mockBinCollection);

		String nextCollectionFromDate = "2022-11-02";
		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(nextCollectionFromDate);
		when(mockWhitespaceRequestService.getCollectionByUprnAndDate(COMPANY_ID, UPRN, nextCollectionFromDate)).thenReturn(mockGetCollectionByUprnAndDateResponse);
		when(mockResponseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID)).thenReturn(BIN_COLLECTIONS);
		when(mockWhitespaceWasteService.filterBinCollectionsByService(COMPANY_ID, SERVICE_TYPE, FORM_INSTANCE_ID, BIN_COLLECTIONS)).thenReturn(filteredBinCollections);

		Set<BinCollection> result = whitespaceWasteConnector.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID);

		assertThat(result, sameInstance(filteredBinCollections));
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenServiceNameAndFormInstanceIdAreNotNull_ThenReturnsFilteredBinCollectionsByService() throws Exception {
		final Set<BinCollection> filteredBinCollections = new HashSet<>();
		filteredBinCollections.add(mockBinCollection);

		String nextCollectionFromDate = "2022-11-02";
		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(nextCollectionFromDate);
		when(mockWhitespaceRequestService.getCollectionByUprnAndDate(COMPANY_ID, UPRN, nextCollectionFromDate)).thenReturn(mockGetCollectionByUprnAndDateResponse);
		when(mockResponseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID)).thenReturn(BIN_COLLECTIONS);
		when(mockWhitespaceWasteService.filterBinCollectionsByService(COMPANY_ID, SERVICE_TYPE, FORM_INSTANCE_ID, BIN_COLLECTIONS)).thenReturn(filteredBinCollections);

		Set<BinCollection> result = whitespaceWasteConnector.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID);

		assertThat(result, sameInstance(filteredBinCollections));
	}

	@Test
	public void createNewWasteContainerRequest_WhenNoErrors_ThenCreatesWorksheetAndReturnsWorksheetReference() throws Exception {
		String worksheetMessage = "New waste container request";
		final String worksheetRef = "ref";
		mockNewContainerRequest();

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFormInstanceRecordId(CLASS_PK)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(BIN_TYPE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Arrays.asList(BIN_SIZE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFirstName(FIRST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setLastName(LAST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setQuantities(Arrays.asList(1))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setWorksheetMessage(worksheetMessage)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);
		when(mockWhitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext)).thenReturn(worksheetRef);

		String result = whitespaceWasteConnector.createNewWasteContainerRequest(COMPANY_ID, mockNewContainerRequest);

		assertThat(result, equalTo(worksheetRef));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createNewWasteContainerRequest_WhenWorksheetCreationFails_ThenThrowsWasteRetrievalException() throws Exception {
		String worksheetMessage = "New waste container request";
		mockNewContainerRequest();

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFormInstanceRecordId(CLASS_PK)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(BIN_TYPE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Arrays.asList(BIN_SIZE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFirstName(FIRST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setLastName(LAST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setQuantities(Arrays.asList(1))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setWorksheetMessage(worksheetMessage)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext)).thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.createNewWasteContainerRequest(COMPANY_ID, mockNewContainerRequest);
	}

	@Test
	public void createGardenWasteSubscription_WhenNoErrors_ThenCreatesWorksheetAndReturnsWasteSubscriptionResponse() throws Exception {
		String worksheetMessage = "New garden waste subscription request";
		final String worksheetRef = "ref";
		mockGardenWasteSubscription();

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFormInstanceRecordId(CLASS_PK)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(BIN_TYPE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFirstName(FIRST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setLastName(LAST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setQuantities(Arrays.asList(QUANTITY))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setWorksheetMessage(worksheetMessage)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);
		when(mockWhitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext)).thenReturn(worksheetRef);

		when(mockWhitespaceWasteService.createWasteSubscriptionResponse(worksheetRef, SubscriptionType.GARDEN_WASTE)).thenReturn(mockWasteSubscriptionResponse);

		WasteSubscriptionResponse result = whitespaceWasteConnector.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription);

		assertThat(result, equalTo(mockWasteSubscriptionResponse));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createGardenWasteSubscription_WhenWorksheetCreationFails_ThenThrowsWasteRetrievalException() throws Exception {
		String worksheetMessage = "New garden waste subscription request";
		mockGardenWasteSubscription();

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFormInstanceRecordId(CLASS_PK)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(BIN_TYPE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFirstName(FIRST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setLastName(LAST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setQuantities(Arrays.asList(QUANTITY))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setWorksheetMessage(worksheetMessage)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);
		when(mockWhitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext)).thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription);
	}

	@Test
	public void howManyBinsSubscribed_WhenNoErrors_ThenReturnsTotalNumberOfBinsSubscribed() throws Exception {
		final int numberOfBins = 4;
		SiteService[] siteServices = new SiteService[] { mockSiteService };
		when(mockWhitespaceRequestService.getSiteCollections(COMPANY_ID, null, UPRN, true)).thenReturn(mockGetSiteCollectionsResponse);
		when(mockResponseParserService.parseGetSiteCollectionsResponse(mockGetSiteCollectionsResponse)).thenReturn(siteServices);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceId(COMPANY_ID, SERVICE_TYPE)).thenReturn(SERVICE_ID);
		when(mockWhitespaceWasteService.getTotalSiteServicesQuantities(siteServices, Integer.parseInt(SERVICE_ID))).thenReturn(numberOfBins);

		int result = whitespaceWasteConnector.howManyBinsSubscribed(COMPANY_ID, UPRN, SERVICE_TYPE);

		assertThat(result, equalTo(numberOfBins));
	}

	@Test(expected = WasteRetrievalException.class)
	public void howManyBinsSubscribed_WhenSiteCollectionsRetrievalFails_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockWhitespaceRequestService.getSiteCollections(COMPANY_ID, null, UPRN, true)).thenThrow(new Exception());
		whitespaceWasteConnector.howManyBinsSubscribed(COMPANY_ID, UPRN, SERVICE_TYPE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void howManyBinsSubscribed_WhenServiceTypeConfigurationRetrievalFails_ThenThrowsWasteRetrievalException() throws Exception {
		SiteService[] siteServices = new SiteService[] { mockSiteService };
		when(mockWhitespaceRequestService.getSiteCollections(COMPANY_ID, null, UPRN, true)).thenReturn(mockGetSiteCollectionsResponse);
		when(mockResponseParserService.parseGetSiteCollectionsResponse(mockGetSiteCollectionsResponse)).thenReturn(siteServices);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceId(COMPANY_ID, SERVICE_TYPE)).thenThrow(new ConfigurationException());

		whitespaceWasteConnector.howManyBinsSubscribed(COMPANY_ID, UPRN, SERVICE_TYPE);
	}

	@Test
	public void getBulkyCollectionSlotsByUPRNAndService_WhenThereAreFreeSlots_ThenReturnsSlotDate() throws Exception {
		final String date = "date";
		Date slotDate = new Date();

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(date);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), date, null)).thenReturn(mockGetCollectionSlotsResponse);

		when(mockResponseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse)).thenReturn(new ApiAdHocRoundInstance[] { mockApiAdHocRoundInstance });
		when(mockWhitespaceWasteService.getWasteBulkyCollectionDateIfFreeSlots(mockApiAdHocRoundInstance)).thenReturn(Optional.of(slotDate));

		List<Date> result = whitespaceWasteConnector.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0), sameInstance(slotDate));
	}

	@Test
	public void getBulkyCollectionSlotsByUPRNAndService_WhenThereAreNoFreeSlots_ThenDoesNotReturnsSlotDate() throws Exception {
		final String date = "date";

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(date);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), date, null)).thenReturn(mockGetCollectionSlotsResponse);

		when(mockResponseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse)).thenReturn(new ApiAdHocRoundInstance[] { mockApiAdHocRoundInstance });
		when(mockWhitespaceWasteService.getWasteBulkyCollectionDateIfFreeSlots(mockApiAdHocRoundInstance)).thenReturn(Optional.empty());

		List<Date> result = whitespaceWasteConnector.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionSlotsByUPRNAndService_WhenSlotsRetrievalFails_ThenThrowsWasteRetrievalException() throws Exception {
		final String date = "date";

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(date);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), date, null)).thenThrow(new Exception());

		whitespaceWasteConnector.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionSlotsByUPRNAndService_WhenCollectionSlotsResponseIsNotValid_ThenThrowsWasteRetrievalException() throws Exception {
		final String date = "date";

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(date);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), date, null)).thenReturn(mockGetCollectionSlotsResponse);
		when(mockResponseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse)).thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);
	}

	@Test
	public void createBulkyWasteCollectionJob_WhenNoErrors_ThenCreatesBulkyWasteCollectionWorksheet() throws Exception {
		final String DATE_STRING = "4 Apr 2024";
		int adHocRoundInstanceId = 324;
		String worksheetRef = "ref";
		String[] itemsForCollection = new String[] { "bed", "chair" };
		String[] quantities = new String[] { "1", "2" };

		Map<String, Integer> itemsToQuantities = new LinkedHashMap<>();
		itemsToQuantities.put("bed", 1);
		itemsToQuantities.put("chair", 2);

		when(mockBulkyWasteCollectionRequest.getClassPK()).thenReturn(String.valueOf(CLASS_PK));
		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(UPRN);
		when(mockBulkyWasteCollectionRequest.getFirstName()).thenReturn(FIRST_NAME);
		when(mockBulkyWasteCollectionRequest.getLastName()).thenReturn(LAST_NAME);
		when(mockBulkyWasteCollectionRequest.getItemsForCollection()).thenReturn(new String[] {});
		when(mockBulkyWasteCollectionRequest.getQuantities()).thenReturn(new String[] {});

		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(CLASS_PK)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(Long.parseLong(FORM_INSTANCE_ID));
		when(mockWhitespaceConfigurationService.getConfiguredDefaultWhitespaceService(COMPANY_ID, Long.parseLong(FORM_INSTANCE_ID))).thenReturn(mockWhitespaceService);
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(DATE_STRING);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), DATE_STRING, null))
				.thenReturn(mockGetCollectionSlotsResponse);

		when(mockResponseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse)).thenReturn(new ApiAdHocRoundInstance[] { mockApiAdHocRoundInstance });
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DATE);
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceDate()).thenReturn(calendar);
		when(mockApiAdHocRoundInstance.getSlotsFree()).thenReturn(2);
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceID()).thenReturn(adHocRoundInstanceId);

		when(mockBulkyWasteCollectionRequest.getItemsForCollection()).thenReturn(itemsForCollection);
		when(mockBulkyWasteCollectionRequest.getQuantities()).thenReturn(quantities);

		when(mockWhitespaceWasteService.getItemsToQuantitiesMap(itemsForCollection, quantities)).thenReturn(itemsToQuantities);

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFormInstanceRecordId(CLASS_PK)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(itemsForCollection))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFirstName(FIRST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setLastName(LAST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setQuantities(Arrays.asList(1, 2))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setWorksheetMessage("New bulky waste collection request")).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setAdHocRoundInstanceId(adHocRoundInstanceId)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);
		when(mockWhitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext)).thenReturn(worksheetRef);

		String result = whitespaceWasteConnector.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, DATE);

		assertThat(result, equalTo(worksheetRef));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenNoMatchingSlotForSelectedDate_ThenThrowsWasteRetrievalException() throws Exception {
		final String DATE_STRING = "4 Apr 2024";

		when(mockBulkyWasteCollectionRequest.getClassPK()).thenReturn(String.valueOf(CLASS_PK));
		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(UPRN);
		when(mockBulkyWasteCollectionRequest.getFirstName()).thenReturn(FIRST_NAME);
		when(mockBulkyWasteCollectionRequest.getLastName()).thenReturn(LAST_NAME);
		when(mockBulkyWasteCollectionRequest.getItemsForCollection()).thenReturn(new String[] {});
		when(mockBulkyWasteCollectionRequest.getQuantities()).thenReturn(new String[] {});

		when(mockWhitespaceConfigurationService.getConfiguredDefaultWhitespaceService(COMPANY_ID, CLASS_PK)).thenReturn(mockWhitespaceService);
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(DATE_STRING);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), DATE_STRING, null))
				.thenReturn(mockGetCollectionSlotsResponse);

		when(mockResponseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse)).thenReturn(new ApiAdHocRoundInstance[] { mockApiAdHocRoundInstance });
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -1);
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceDate()).thenReturn(calendar);

		whitespaceWasteConnector.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenSlotsDontHaveAValidDate_ThenThrowsWasteRetrievalException() throws Exception {
		final String DATE_STRING = "4 Apr 2024";

		when(mockBulkyWasteCollectionRequest.getClassPK()).thenReturn(String.valueOf(CLASS_PK));
		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(UPRN);
		when(mockBulkyWasteCollectionRequest.getFirstName()).thenReturn(FIRST_NAME);
		when(mockBulkyWasteCollectionRequest.getLastName()).thenReturn(LAST_NAME);
		when(mockBulkyWasteCollectionRequest.getItemsForCollection()).thenReturn(new String[] {});
		when(mockBulkyWasteCollectionRequest.getQuantities()).thenReturn(new String[] {});

		when(mockWhitespaceConfigurationService.getConfiguredDefaultWhitespaceService(COMPANY_ID, CLASS_PK)).thenReturn(mockWhitespaceService);
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(DATE_STRING);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), DATE_STRING, null))
				.thenReturn(mockGetCollectionSlotsResponse);

		when(mockResponseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse)).thenReturn(new ApiAdHocRoundInstance[] { mockApiAdHocRoundInstance });
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceDate()).thenReturn(null);

		whitespaceWasteConnector.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenNoFreeSlotsInSelectedDate_ThenThrowsWasteRetrievalException() throws Exception {
		final String DATE_STRING = "4 Apr 2024";

		when(mockBulkyWasteCollectionRequest.getClassPK()).thenReturn(String.valueOf(CLASS_PK));
		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(UPRN);
		when(mockBulkyWasteCollectionRequest.getFirstName()).thenReturn(FIRST_NAME);
		when(mockBulkyWasteCollectionRequest.getLastName()).thenReturn(LAST_NAME);
		when(mockBulkyWasteCollectionRequest.getItemsForCollection()).thenReturn(new String[] {});
		when(mockBulkyWasteCollectionRequest.getQuantities()).thenReturn(new String[] {});

		when(mockWhitespaceConfigurationService.getConfiguredDefaultWhitespaceService(COMPANY_ID, CLASS_PK)).thenReturn(mockWhitespaceService);
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(DATE_STRING);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), DATE_STRING, null))
				.thenReturn(mockGetCollectionSlotsResponse);

		when(mockResponseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse)).thenReturn(new ApiAdHocRoundInstance[] { mockApiAdHocRoundInstance });
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DATE);
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceDate()).thenReturn(calendar);
		when(mockApiAdHocRoundInstance.getSlotsFree()).thenReturn(0);

		whitespaceWasteConnector.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenWorksheetCreationFails_ThenThrowsWasteRetrievalException() throws Exception {
		final String DATE_STRING = "4 Apr 2024";
		int adHocRoundInstanceId = 324;
		String[] itemsForCollection = new String[] { "bed", "chair" };
		String[] quantities = new String[] { "1", "2" };
		Map<String, Integer> itemsToQuantities = new LinkedHashMap<>();
		itemsToQuantities.put("bed", 1);
		itemsToQuantities.put("chair", 2);

		when(mockBulkyWasteCollectionRequest.getClassPK()).thenReturn(String.valueOf(CLASS_PK));
		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(UPRN);
		when(mockBulkyWasteCollectionRequest.getFirstName()).thenReturn(FIRST_NAME);
		when(mockBulkyWasteCollectionRequest.getLastName()).thenReturn(LAST_NAME);
		when(mockBulkyWasteCollectionRequest.getItemsForCollection()).thenReturn(new String[] {});
		when(mockBulkyWasteCollectionRequest.getQuantities()).thenReturn(new String[] {});

		when(mockWhitespaceConfigurationService.getConfiguredDefaultWhitespaceService(COMPANY_ID, CLASS_PK)).thenReturn(mockWhitespaceService);
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);

		when(WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now())).thenReturn(DATE_STRING);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, UPRN, SERVICE_ID, null, String.valueOf(NUMBER_OF_DATES_TO_RETURN), DATE_STRING, null))
				.thenReturn(mockGetCollectionSlotsResponse);

		when(mockResponseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse)).thenReturn(new ApiAdHocRoundInstance[] { mockApiAdHocRoundInstance });
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DATE);
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceDate()).thenReturn(calendar);
		when(mockApiAdHocRoundInstance.getSlotsFree()).thenReturn(2);
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceID()).thenReturn(adHocRoundInstanceId);

		when(mockBulkyWasteCollectionRequest.getItemsForCollection()).thenReturn(itemsForCollection);
		when(mockBulkyWasteCollectionRequest.getQuantities()).thenReturn(quantities);

		when(mockWhitespaceWasteService.getItemsToQuantitiesMap(itemsForCollection, quantities)).thenReturn(itemsToQuantities);

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(itemsForCollection))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFirstName(FIRST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setLastName(LAST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setQuantities(Arrays.asList(1, 2))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setWorksheetMessage("New bulky waste collection request")).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setAdHocRoundInstanceId(adHocRoundInstanceId)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);
		when(mockWhitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext)).thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, DATE);
	}

	@Test
	public void createMissedBinCollectionJob_WhenNoErrors_ThenCreatesMissedBinCollectionWorksheet() throws Exception {
		String worksheetMessage = "New missed bin collection request";
		final String worksheetRef = "ref";
		mockMissedBinCollectionJob();

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFormInstanceRecordId(CLASS_PK)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFirstName(FIRST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setLastName(LAST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setQuantities(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setWorksheetMessage(worksheetMessage)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);
		when(mockWhitespaceWorksheetService.createWorksheet(mockWorksheetContext, SERVICE_TYPE)).thenReturn(worksheetRef);

		String result = whitespaceWasteConnector.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob);

		assertThat(result, equalTo(worksheetRef));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createMissedBinCollectionJob_WhenWorksheetCreationFails_ThenThrowsWasteRetrievalException() throws Exception {
		String worksheetMessage = "New missed bin collection request";
		mockMissedBinCollectionJob();

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFormInstanceRecordId(CLASS_PK)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setFirstName(FIRST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setLastName(LAST_NAME)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setQuantities(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setWorksheetMessage(worksheetMessage)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.createWorksheet(mockWorksheetContext, SERVICE_TYPE)).thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isPropertyEligibleForBulkyWasteCollection_WhenNoErrors_ThenReturnsValidationResultForDefaultServiceWorksheetCreation(boolean isValid) throws WasteRetrievalException {
		List<String> itemsForCollection = Arrays.asList("item1", "item2");

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(itemsForCollection)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, Long.parseLong(FORM_INSTANCE_ID), Optional.empty())).thenReturn(isValid);

		boolean result = whitespaceWasteConnector.isPropertyEligibleForBulkyWasteCollection(COMPANY_ID, UPRN, itemsForCollection, FORM_INSTANCE_ID);

		assertThat(result, equalTo(isValid));
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForBulkyWasteCollection_WhenValidationCheckThrowsError_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		List<String> itemsForCollection = Arrays.asList("item1", "item2");

		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(itemsForCollection)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, Long.parseLong(FORM_INSTANCE_ID), Optional.empty()))
				.thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.isPropertyEligibleForBulkyWasteCollection(COMPANY_ID, UPRN, itemsForCollection, FORM_INSTANCE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isPropertyEligibleForWasteContainerRequest_WhenNoErrors_ThenReturnsValidationResultForDefaultServiceWorksheetCreation(boolean isValid) throws WasteRetrievalException {
		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(BIN_TYPE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Arrays.asList(BIN_SIZE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, Long.parseLong(FORM_INSTANCE_ID), Optional.empty())).thenReturn(isValid);

		boolean result = whitespaceWasteConnector.isPropertyEligibleForWasteContainerRequest(COMPANY_ID, UPRN, BIN_TYPE, BIN_SIZE, FORM_INSTANCE_ID);

		assertThat(result, equalTo(isValid));
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForWasteContainerRequest_WhenValidationCheckThrowsError_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(BIN_TYPE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Arrays.asList(BIN_SIZE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, Long.parseLong(FORM_INSTANCE_ID), Optional.empty()))
				.thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.isPropertyEligibleForWasteContainerRequest(COMPANY_ID, UPRN, BIN_TYPE, BIN_SIZE, FORM_INSTANCE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isPropertyEligibleForGardenWasteSubscription_WhenNoErrors_ThenReturnsValidationResultForDefaultServiceWorksheetCreation(boolean isValid) throws WasteRetrievalException {
		when(mockObjectFactoryService.createSameFinancialYearValidator(mockWhitespaceWasteServiceUtil)).thenReturn(mockWorksheetValidator);
		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(BIN_TYPE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, Long.parseLong(FORM_INSTANCE_ID), Optional.of(mockWorksheetValidator))).thenReturn(isValid);

		boolean result = whitespaceWasteConnector.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, null, null, BIN_TYPE, FORM_INSTANCE_ID);

		assertThat(result, equalTo(isValid));
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForGardenWasteSubscription_WhenValidationCheckThrowsError_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		when(mockObjectFactoryService.createSameFinancialYearValidator(mockWhitespaceWasteServiceUtil)).thenReturn(mockWorksheetValidator);
		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Arrays.asList(BIN_TYPE))).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, Long.parseLong(FORM_INSTANCE_ID), Optional.of(mockWorksheetValidator)))
				.thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, null, null, BIN_TYPE, FORM_INSTANCE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isPropertyEligibleForMissedBinCollectionReport_WhenNoErrors_ThenReturnsValidationResultForSelectedServiceWorksheetCreation(boolean isValid) throws WasteRetrievalException {
		when(mockObjectFactoryService.createTwoLastWeeksValidator(mockWhitespaceWasteServiceUtil)).thenReturn(mockWorksheetValidator);
		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.isWorksheetContextValidForService(mockWorksheetContext, Long.parseLong(FORM_INSTANCE_ID), BIN_TYPE, Optional.of(mockWorksheetValidator)))
				.thenReturn(isValid);

		boolean result = whitespaceWasteConnector.isPropertyEligibleForMissedBinCollectionReport(COMPANY_ID, UPRN, BIN_TYPE, FORM_INSTANCE_ID);

		assertThat(result, equalTo(isValid));
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForMissedBinCollectionReport_WhenValidationCheckThrowsError_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		when(mockObjectFactoryService.createTwoLastWeeksValidator(mockWhitespaceWasteServiceUtil)).thenReturn(mockWorksheetValidator);
		when(mockObjectFactoryService.getWorksheetContextBuilder(COMPANY_ID, UPRN)).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinTypes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.setBinSizes(Collections.emptyList())).thenReturn(mockWorksheetContextBuilder);
		when(mockWorksheetContextBuilder.build()).thenReturn(mockWorksheetContext);

		when(mockWhitespaceWorksheetService.isWorksheetContextValidForService(mockWorksheetContext, Long.parseLong(FORM_INSTANCE_ID), BIN_TYPE, Optional.of(mockWorksheetValidator)))
				.thenThrow(new WasteRetrievalException());

		whitespaceWasteConnector.isPropertyEligibleForMissedBinCollectionReport(COMPANY_ID, UPRN, BIN_TYPE, FORM_INSTANCE_ID);
	}

	private void mockMissedBinCollectionJob() {
		when(mockMissedBinCollectionJob.getClassPK()).thenReturn(String.valueOf(CLASS_PK));
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(UPRN);
		when(mockMissedBinCollectionJob.getFirstName()).thenReturn(FIRST_NAME);
		when(mockMissedBinCollectionJob.getLastName()).thenReturn(LAST_NAME);
		when(mockMissedBinCollectionJob.getBinType()).thenReturn(SERVICE_TYPE);
	}

	private void mockNewContainerRequest() {
		when(mockNewContainerRequest.getClassPK()).thenReturn(String.valueOf(CLASS_PK));
		when(mockNewContainerRequest.getBinType()).thenReturn(BIN_TYPE);
		when(mockNewContainerRequest.getBinSize()).thenReturn(BIN_SIZE);
		when(mockNewContainerRequest.getUprn()).thenReturn(UPRN);
		when(mockNewContainerRequest.getFirstName()).thenReturn(FIRST_NAME);
		when(mockNewContainerRequest.getLastName()).thenReturn(LAST_NAME);
	}

	private void mockGardenWasteSubscription() {
		when(mockGardenWasteSubscription.getClassPK()).thenReturn(String.valueOf(CLASS_PK));
		when(mockGardenWasteSubscription.getBinType()).thenReturn(BIN_TYPE);
		when(mockGardenWasteSubscription.getUprn()).thenReturn(UPRN);
		when(mockGardenWasteSubscription.getFirstName()).thenReturn(FIRST_NAME);
		when(mockGardenWasteSubscription.getLastName()).thenReturn(LAST_NAME);
		when(mockGardenWasteSubscription.getNumberRequestedNewBins()).thenReturn(QUANTITY);
	}
}
