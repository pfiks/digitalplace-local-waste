package com.placecube.digitalplace.local.waste.whitespace.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class WhitespaceServiceTest {

	@Test
	public void new_whenNoErrors_ThenSetsPropertiesFromMapIntoService() {
		final String serviceId = "3242";
		final String collectionServiceName = "garden collection";

		final String serviceItemKey1 = "House";
		final String serviceItemKey2 = "Garden";

		final String serviceItemId1 = "654643";
		final String serviceItemId2 = "45";
		final String serviceItemId3 = "435";
		final String serviceItemId4 = "123";

		final String serviceItemId1Size1 = "20";
		final String serviceItemId1Size2 = "50";

		final String serviceItemId2Size1 = "40";
		final String serviceItemId2Size2 = "10";

		Map<String, Object> servicePropertiesMap = new HashMap<>();

		Map<String, Object> serviceItemsMap = new HashMap<>();

		Map<String, Object> serviceItem1Map = new HashMap<>();
		serviceItem1Map.put(serviceItemId1Size1, serviceItemId1);
		serviceItem1Map.put(serviceItemId1Size2, serviceItemId2);

		Map<String, Object> serviceItem2Map = new HashMap<>();
		serviceItem2Map.put(serviceItemId2Size1, serviceItemId3);
		serviceItem2Map.put(serviceItemId2Size2, serviceItemId4);

		serviceItemsMap.put(serviceItemKey1, serviceItem1Map);
		serviceItemsMap.put(serviceItemKey2, serviceItem2Map);

		servicePropertiesMap.put("serviceId", serviceId);
		servicePropertiesMap.put("collectionServiceName", collectionServiceName);
		servicePropertiesMap.put("serviceItemMappings", serviceItemsMap);

		WhitespaceService service = new WhitespaceService(servicePropertiesMap);

		assertThat(service.getServiceId(), equalTo(serviceId));
		assertThat(service.getCollectionServiceName(), equalTo(collectionServiceName));
		assertThat(service.getServiceItemMappings(serviceItemKey1).size(), equalTo(2));
		assertThat(service.getServiceItemMappings(serviceItemKey1).get(serviceItemId1Size1), equalTo(serviceItemId1));
		assertThat(service.getServiceItemMappings(serviceItemKey1).get(serviceItemId1Size2), equalTo(serviceItemId2));
		assertThat(service.getServiceItemMappings(serviceItemKey2).size(), equalTo(2));
		assertThat(service.getServiceItemMappings(serviceItemKey2).get(serviceItemId2Size1), equalTo(serviceItemId3));
		assertThat(service.getServiceItemMappings(serviceItemKey2).get(serviceItemId2Size2), equalTo(serviceItemId4));

	}
}
