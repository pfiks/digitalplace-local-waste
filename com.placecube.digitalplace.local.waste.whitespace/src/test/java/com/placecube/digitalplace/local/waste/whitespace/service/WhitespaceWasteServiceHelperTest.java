package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.WorksheetServiceItem;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocaleUtil.class })
public class WhitespaceWasteServiceHelperTest {

	private static final int WORKSHEET_ID = 23244;
	private static final int SERVICE_ITEM_ID = 8765;
	private static final long COMPANY_ID = 10;
	private static final long FORM_INSTANCE_ID = 433L;

	@InjectMocks
	private WhitespaceWasteServiceHelper whitespaceWasteServiceHelper;

	@Mock
	private WhitespaceRequestService mockWhiteSpaceRequestService;

	@Mock
	private DDMFormInstanceLocalService mockDDMFormInstanceLocalService;

	@Mock
	private ResponseParserService mockResponseParserService;

	@Mock
	private GetWorksheetDetailServiceItemsResponse mockGetWorksheetDetailServiceItemsResponse;

	@Mock
	private WorksheetServiceItem mockWorksheetServiceItem;

	@Mock
	private Worksheet mockWorksheet;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMFormField mockDDMFormField;

	@Mock
	private DDMFormFieldOptions mockDDMFormFieldOptions;

	@Mock
	private LocalizedValue mockLocalizedValue;

	@Before
	public void setUp() {
		mockStatic(LocaleUtil.class);
	}

	@Test
	public void anyServiceItemMatchesWorksheetItems_WhenWorksheetContainsAnyItemWithServiceItemId_ThenReturnsTrue() throws Exception {
		when(mockWorksheet.getWorksheetID()).thenReturn(WORKSHEET_ID);
		when(mockWhiteSpaceRequestService.getWorksheetDetailServiceItems(COMPANY_ID, String.valueOf(WORKSHEET_ID), null)).thenReturn(mockGetWorksheetDetailServiceItemsResponse);
		when(mockResponseParserService.parseGetWorksheetDetailServiceItemsResponse(mockGetWorksheetDetailServiceItemsResponse)).thenReturn(new WorksheetServiceItem[] { mockWorksheetServiceItem });
		when(mockWorksheetServiceItem.getServiceItemID()).thenReturn(SERVICE_ITEM_ID);

		assertThat(whitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, Arrays.asList(String.valueOf(SERVICE_ITEM_ID), "534")), equalTo(true));
	}

	@Test
	public void anyServiceItemMatchesWorksheetItems_WhenWorksheetDoesNotContainItemWithServiceItemIds_ThenReturnsFalse() throws Exception {
		when(mockWorksheet.getWorksheetID()).thenReturn(WORKSHEET_ID);
		when(mockWhiteSpaceRequestService.getWorksheetDetailServiceItems(COMPANY_ID, String.valueOf(WORKSHEET_ID), null)).thenReturn(mockGetWorksheetDetailServiceItemsResponse);
		when(mockResponseParserService.parseGetWorksheetDetailServiceItemsResponse(mockGetWorksheetDetailServiceItemsResponse)).thenReturn(new WorksheetServiceItem[] { mockWorksheetServiceItem });
		when(mockWorksheetServiceItem.getServiceItemID()).thenReturn(SERVICE_ITEM_ID + 100);

		assertThat(whitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, Arrays.asList(String.valueOf(SERVICE_ITEM_ID))), equalTo(false));
	}

	@Test(expected = Exception.class)
	public void anyServiceItemMatchesWorksheetItems_WhenWorksheetServiceItemDetailsRetrievalFails_ThenThrowsException() throws Exception {
		when(mockWorksheet.getWorksheetID()).thenReturn(WORKSHEET_ID);
		when(mockWhiteSpaceRequestService.getWorksheetDetailServiceItems(COMPANY_ID, String.valueOf(WORKSHEET_ID), null)).thenThrow(new Exception());

		whitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, Arrays.asList(String.valueOf(SERVICE_ITEM_ID)));
	}

	@Test
	public void anyServiceItemMatchesWorksheetItems_WhenServiceItemsListIsEmpty_ThenReturnsTrue() throws Exception {
		assertThat(whitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, Collections.emptyList()), equalTo(true));
	}

	@Test
	public void anyServiceItemMatchesWorksheetItems_WhenServiceItemsListIsNull_ThenReturnsTrue() throws Exception {
		assertThat(whitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, null), equalTo(true));
	}

	@Test
	public void getFormFieldValueOptionReference_WhenOptionIsFoundForLocalisedValue_ThenReturnsOptionReference() throws Exception {
		final String optionKey = "optionKey";
		final String value = "value";
		final String reference = "ref";

		Map<String, LocalizedValue> optionsMap = new HashMap<>();
		optionsMap.put(optionKey, mockLocalizedValue);

		when(mockDDMFormInstanceLocalService.getDDMFormInstance(FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getDDMFormFields(false)).thenReturn(Arrays.asList(mockDDMFormField));
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(mockDDMFormFieldOptions);
		when(mockDDMFormFieldOptions.getOptions()).thenReturn(optionsMap);
		when(LocaleUtil.getDefault()).thenReturn(Locale.UK);
		when(mockLocalizedValue.getString(Locale.UK)).thenReturn(value);
		when(mockDDMFormFieldOptions.getOptionReference(optionKey)).thenReturn(reference);

		Optional<String> result = whitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, value);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), equalTo(reference));
	}

	@Test
	public void getFormFieldValueOptionReference_WhenNoOptionIsFoundForLocalisedValue_ThenReturnsEmptyOptional() throws Exception {
		final String optionKey = "optionKey";
		final String value = "value";

		Map<String, LocalizedValue> optionsMap = new HashMap<>();
		optionsMap.put(optionKey, mockLocalizedValue);

		when(mockDDMFormInstanceLocalService.getDDMFormInstance(FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getDDMFormFields(false)).thenReturn(Arrays.asList(mockDDMFormField));
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(mockDDMFormFieldOptions);
		when(mockDDMFormFieldOptions.getOptions()).thenReturn(optionsMap);
		when(LocaleUtil.getDefault()).thenReturn(Locale.UK);
		when(mockLocalizedValue.getString(Locale.UK)).thenReturn("another value");

		Optional<String> result = whitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, value);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getFormFieldValueOptionReference_WhenFieldHasNoOptions_ThenReturnsEmptyOptional() throws Exception {
		final String value = "value";

		when(mockDDMFormInstanceLocalService.getDDMFormInstance(FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getDDMFormFields(false)).thenReturn(Arrays.asList(mockDDMFormField));
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(mockDDMFormFieldOptions);
		when(mockDDMFormFieldOptions.getOptions()).thenReturn(new HashMap<>());

		Optional<String> result = whitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, value);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getFormFieldValueOptionReference_WhenFieldDDMFormFieldOptionsObjectIsNull_ThenReturnsEmptyOptional() throws Exception {
		final String value = "value";

		when(mockDDMFormInstanceLocalService.getDDMFormInstance(FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getDDMFormFields(false)).thenReturn(Arrays.asList(mockDDMFormField));
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(null);

		Optional<String> result = whitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, value);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getFormFieldValueOptionReference_WhenFormHasNoFields_ThenReturnsEmptyOptional() throws Exception {
		final String value = "value";

		when(mockDDMFormInstanceLocalService.getDDMFormInstance(FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getDDMFormFields(false)).thenReturn(Collections.emptyList());

		Optional<String> result = whitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, value);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getFormFieldValueOptionReference_WhenDDMFormRetrievalFails_ThenReturnsEmptyOptional() throws Exception {
		final String value = "value";

		when(mockDDMFormInstanceLocalService.getDDMFormInstance(FORM_INSTANCE_ID)).thenThrow(new PortalException());

		Optional<String> result = whitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, value);

		assertThat(result.isPresent(), equalTo(false));
	}

}
