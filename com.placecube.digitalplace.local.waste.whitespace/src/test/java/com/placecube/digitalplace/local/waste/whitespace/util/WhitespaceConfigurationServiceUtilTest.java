package com.placecube.digitalplace.local.waste.whitespace.util;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;

@RunWith(PowerMockRunner.class)
public class WhitespaceConfigurationServiceUtilTest extends PowerMockito {

	@InjectMocks
	private WhitespaceConfigurationServiceUtil whitespaceConfigurationServiceUtil;

	@Test
	public void getConfiguredWhitespaceService_WhenServicePropertiesArePresent_ThenReturnsServiceMappings() {
		Map<String, Object> serviceItemMappings = new HashMap<>();
		serviceItemMappings.put("serviceItemKey", new HashMap<>());

		Map<String, Object> properties = new HashMap<>();
		properties.put("serviceId", "3242342");
		properties.put("serviceItemMappings", serviceItemMappings);

		Map<String, Object> defaultServiceMappings = new HashMap<>();
		defaultServiceMappings.put("default", properties);

		Map<String, Object> serviceMappings = new HashMap<>();
		serviceMappings.put("serviceMappings", defaultServiceMappings);

		Map<String, WhitespaceService> result = whitespaceConfigurationServiceUtil.getWhitespaceServicesFromFormConfiguration(serviceMappings);

		assertThat(result.size(), equalTo(1));

		WhitespaceService defaultService = result.get("default");

		assertThat(defaultService.getServiceId(), notNullValue());
		assertThat(defaultService.getServiceItemMappings("serviceItemKey"), notNullValue());
	}

	@Test
	public void getConfiguredWhitespaceService_WhenServicePropertiesAreNotPresent_ThenDoesNotAddServiceMappings() {
		Map<String, Object> defaultServiceMappings = new HashMap<>();
		defaultServiceMappings.put("default", null);

		Map<String, Object> serviceMappings = new HashMap<>();
		serviceMappings.put("serviceMappings", defaultServiceMappings);

		Map<String, WhitespaceService> result = whitespaceConfigurationServiceUtil.getWhitespaceServicesFromFormConfiguration(serviceMappings);

		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void getConfiguredWhitespaceService_WhenServiceMappingsAreNotPresent_ThenDoesNotAddServiceMappings() {
		Map<String, Object> serviceMappings = new HashMap<>();

		Map<String, WhitespaceService> result = whitespaceConfigurationServiceUtil.getWhitespaceServicesFromFormConfiguration(serviceMappings);

		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void getServiceIdForServiceTypeFromFormConfiguration_WhenServiceTypeIsPresentInConfiguration_ThenReturnsServiceIdForServiceType() {
		final String serviceType = "serviceType";
		final String serviceId = "3242342";

		Map<String, Object> properties = new HashMap<>();
		properties.put("serviceId", serviceId);
		properties.put("serviceType", serviceType);

		Map<String, Object> defaultServiceMappings = new HashMap<>();
		defaultServiceMappings.put("default", properties);

		Map<String, Object> serviceMappings = new HashMap<>();
		serviceMappings.put("serviceMappings", defaultServiceMappings);

		Optional<String> result = whitespaceConfigurationServiceUtil.getServiceIdForServiceTypeFromFormConfiguration(serviceMappings, serviceType);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), equalTo(serviceId));
	}

	@Test
	public void getServiceIdForServiceTypeFromFormConfiguration_WhenServiceTypeIsPresentInConfigurationAndServiceIdPropertyDoesNotExist_ThenReturnsEmptyOptional() {
		final String serviceType = "serviceType";

		Map<String, Object> properties = new HashMap<>();
		properties.put("serviceType", serviceType);

		Map<String, Object> defaultServiceMappings = new HashMap<>();
		defaultServiceMappings.put("default", properties);

		Map<String, Object> serviceMappings = new HashMap<>();
		serviceMappings.put("serviceMappings", defaultServiceMappings);

		Optional<String> result = whitespaceConfigurationServiceUtil.getServiceIdForServiceTypeFromFormConfiguration(serviceMappings, serviceType);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getServiceIdForServiceTypeFromFormConfiguration_WhenServiceTypeIsNotFound_ThenReturnsEmptyOptional() {
		final String serviceType = "serviceType";

		Map<String, Object> properties = new HashMap<>();

		Map<String, Object> defaultServiceMappings = new HashMap<>();
		defaultServiceMappings.put("default", properties);

		Map<String, Object> serviceMappings = new HashMap<>();
		serviceMappings.put("serviceMappings", defaultServiceMappings);

		Optional<String> result = whitespaceConfigurationServiceUtil.getServiceIdForServiceTypeFromFormConfiguration(serviceMappings, serviceType);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getServiceIdForServiceTypeFromFormConfiguration_WhenServiceTypeParameterIsEmpty_ThenReturnsEmptyOptional() {
		final String serviceType = "serviceType";
		final String serviceId = "3242342";

		Map<String, Object> properties = new HashMap<>();
		properties.put("serviceId", serviceId);
		properties.put("serviceType", serviceType);

		Map<String, Object> defaultServiceMappings = new HashMap<>();
		defaultServiceMappings.put("default", properties);

		Map<String, Object> serviceMappings = new HashMap<>();
		serviceMappings.put("serviceMappings", defaultServiceMappings);

		Optional<String> result = whitespaceConfigurationServiceUtil.getServiceIdForServiceTypeFromFormConfiguration(serviceMappings, null);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getServiceIdForServiceTypeFromFormConfiguration_WhenServiceMappingsAreNotPresentInConfiguration_ThenReturnsEmptyOptional() {
		final String serviceType = "serviceType";

		Map<String, Object> serviceMappings = new HashMap<>();

		Optional<String> result = whitespaceConfigurationServiceUtil.getServiceIdForServiceTypeFromFormConfiguration(serviceMappings, serviceType);

		assertThat(result.isPresent(), equalTo(false));
	}
}
