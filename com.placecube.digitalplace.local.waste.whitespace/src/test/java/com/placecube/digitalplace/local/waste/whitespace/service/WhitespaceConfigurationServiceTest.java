package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.waste.whitespace.configuration.WhitespaceCompanyConfiguration;
import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceConfigurationServiceUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WhitespaceConfigurationServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final long FORM_INSTANCE_ID = 23221L;
	private static final String PASSWORD = "PASSWORD";
	private static final String SERVICE_TYPE = "serviceType";
	private static final String SERVICE_KEY = "serviceKey";
	private static final String USERNAME = "USERNAME";
	private static final String JSON_CONFIG = "{json}";

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private WhitespaceCompanyConfiguration mockWhitespaceCompanyConfiguration;

	@Mock
	private WhitespaceService mockWhitespaceService;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private WhitespaceConfigurationServiceUtil mockWhitespaceConfigurationServiceUtil;

	@InjectMocks
	private WhitespaceConfigurationService whitespaceConfigurationService;

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenErrorGettingConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		whitespaceConfigurationService.getConfiguration(COMPANY_ID);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsConfiguration() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);

		assertThat(whitespaceConfigurationService.getConfiguration(COMPANY_ID), sameInstance(mockWhitespaceCompanyConfiguration));
	}

	@Test(expected = ConfigurationException.class)
	public void getPassword_WhenPasswordIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockWhitespaceCompanyConfiguration.password()).thenReturn(StringPool.BLANK);

		whitespaceConfigurationService.getPassword(mockWhitespaceCompanyConfiguration);
	}

	@Test
	public void getPassword_WhenPasswordIsNotBlank_ThenReturnsPassword() throws Exception {
		when(mockWhitespaceCompanyConfiguration.password()).thenReturn(PASSWORD);

		String result = whitespaceConfigurationService.getPassword(mockWhitespaceCompanyConfiguration);

		assertEquals(PASSWORD, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getUsername_WhenUsernameIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockWhitespaceCompanyConfiguration.username()).thenReturn(StringPool.BLANK);

		whitespaceConfigurationService.getUsername(mockWhitespaceCompanyConfiguration);
	}

	@Test
	public void getUsername_WhenUsernameIsNotBlank_ThenReturnsUsername() throws Exception {
		when(mockWhitespaceCompanyConfiguration.username()).thenReturn(USERNAME);

		String result = whitespaceConfigurationService.getUsername(mockWhitespaceCompanyConfiguration);

		assertEquals(USERNAME, result);
	}

	@Test(expected = ConfigurationException.class)
	public void isEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		whitespaceConfigurationService.isEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = whitespaceConfigurationService.isEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getConfiguredDefaultWhitespaceService_WhenFormToServiceMappingConfigContainsDefaultServiceConfigurationForForm_ThenReturnsServiceForForm() throws Exception {
		Map<String, Object> properties = new HashMap<>();

		Map<String, Object> servicesConfig = new HashMap<>();
		servicesConfig.put(String.valueOf(FORM_INSTANCE_ID), properties);

		Map<String, WhitespaceService> serviceMappings = new HashMap<>();
		serviceMappings.put("default", mockWhitespaceService);

		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);
		when(mockJSONFactory.looseDeserialize(JSON_CONFIG, Map.class)).thenReturn(servicesConfig);
		when(mockWhitespaceConfigurationServiceUtil.getWhitespaceServicesFromFormConfiguration(properties)).thenReturn(serviceMappings);

		WhitespaceService result = whitespaceConfigurationService.getConfiguredDefaultWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID);

		assertThat(result, sameInstance(mockWhitespaceService));
	}

	@Test
	public void getConfiguredWhitespaceService_WhenFormToServiceMappingConfigContainsServiceConfigurationForForm_ThenReturnsServiceForForm() throws Exception {
		Map<String, Object> properties = new HashMap<>();

		Map<String, Object> servicesConfig = new HashMap<>();
		servicesConfig.put(String.valueOf(FORM_INSTANCE_ID), properties);

		Map<String, WhitespaceService> serviceMappings = new HashMap<>();
		serviceMappings.put(SERVICE_KEY, mockWhitespaceService);

		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);
		when(mockJSONFactory.looseDeserialize(JSON_CONFIG, Map.class)).thenReturn(servicesConfig);
		when(mockWhitespaceConfigurationServiceUtil.getWhitespaceServicesFromFormConfiguration(properties)).thenReturn(serviceMappings);

		WhitespaceService result = whitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, SERVICE_KEY);

		assertThat(result, sameInstance(mockWhitespaceService));
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguredWhitespaceService_WhenFormToServiceMappingConfigDoesNotContainFormInstanceConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);
		when(mockJSONFactory.looseDeserialize(JSON_CONFIG, Map.class)).thenReturn(new HashMap<>());

		whitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, SERVICE_KEY);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguredWhitespaceService_WhenFormConfigurationDoesNotContainServiceKeyConfiguration_ThenThrowsConfigurationException() throws Exception {
		Map<String, Object> properties = new HashMap<>();

		Map<String, Object> servicesConfig = new HashMap<>();
		servicesConfig.put(String.valueOf(FORM_INSTANCE_ID), properties);

		Map<String, WhitespaceService> serviceMappings = new HashMap<>();
		serviceMappings.put("anotherService", mockWhitespaceService);

		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);
		when(mockJSONFactory.looseDeserialize(JSON_CONFIG, Map.class)).thenReturn(servicesConfig);
		when(mockWhitespaceConfigurationServiceUtil.getWhitespaceServicesFromFormConfiguration(properties)).thenReturn(serviceMappings);

		whitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, SERVICE_KEY);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguredWhitespaceService_WhenWhitespaceConfigurationRetrievalFails_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		whitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, SERVICE_KEY);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguredWhitespaceServiceMappingsByFormInstanceId_WhenThereIsNoFormConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);
		when(mockJSONFactory.looseDeserialize(JSON_CONFIG, Map.class)).thenReturn(new HashMap<>());

		whitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID);
	}

	@Test
	public void getConfiguredWhitespaceServiceMappingsByFormInstanceId_WhenThereIsFormConfiguration_ThenReturnsSeviceMappingsForFormConfiguration() throws Exception {
		Map<String, Object> properties = new HashMap<>();

		Map<String, Object> servicesConfig = new HashMap<>();
		servicesConfig.put(String.valueOf(FORM_INSTANCE_ID), properties);

		Map<String, WhitespaceService> serviceMappings = new HashMap<>();
		serviceMappings.put(SERVICE_KEY, mockWhitespaceService);

		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);
		when(mockJSONFactory.looseDeserialize(JSON_CONFIG, Map.class)).thenReturn(servicesConfig);
		when(mockWhitespaceConfigurationServiceUtil.getWhitespaceServicesFromFormConfiguration(properties)).thenReturn(serviceMappings);

		Map<String, WhitespaceService> result = whitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID);

		assertThat(result, equalTo(serviceMappings));
	}

	@Test
	public void getConfiguredWhitespaceServiceId_WhenServiceTypeHasBeenConfiguredAndServiceIdPropertyIsPresent_ThenReturnsServiceId() throws Exception {
		final String serviceId = "3242342";

		Map<String, Object> formConfig = new HashMap<>();
		formConfig.put("serviceMappings", new HashMap<>());

		Map<String, Object> formsConfig = new HashMap<>();
		formsConfig.put(String.valueOf(FORM_INSTANCE_ID), formConfig);

		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);
		when(mockJSONFactory.looseDeserialize(JSON_CONFIG, Map.class)).thenReturn(formsConfig);
		when(mockWhitespaceConfigurationServiceUtil.getServiceIdForServiceTypeFromFormConfiguration(formConfig, SERVICE_TYPE)).thenReturn(Optional.of(serviceId));

		String result = whitespaceConfigurationService.getConfiguredWhitespaceServiceId(COMPANY_ID, SERVICE_TYPE);

		assertThat(result, equalTo(serviceId));
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguredWhitespaceServiceId_WhenServiceTypeCannotBeFound_ThenThrowsConfigurationException() throws Exception {
		Map<String, Object> formConfig = new HashMap<>();
		formConfig.put("serviceMappings", new HashMap<>());

		Map<String, Object> formsConfig = new HashMap<>();
		formsConfig.put(String.valueOf(FORM_INSTANCE_ID), formConfig);

		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);
		when(mockJSONFactory.looseDeserialize(JSON_CONFIG, Map.class)).thenReturn(formsConfig);
		when(mockWhitespaceConfigurationServiceUtil.getServiceIdForServiceTypeFromFormConfiguration(formConfig, SERVICE_TYPE)).thenReturn(Optional.empty());

		whitespaceConfigurationService.getConfiguredWhitespaceServiceId(COMPANY_ID, SERVICE_TYPE);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguredWhitespaceServiceId_WhenFormsConfigurationsAreMissing_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.formToServiceMapping()).thenReturn(JSON_CONFIG);

		whitespaceConfigurationService.getConfiguredWhitespaceServiceId(COMPANY_ID, SERVICE_TYPE);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguredWhitespaceServiceId_WhenWhitespaceConfigurationRetrievalFails_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		whitespaceConfigurationService.getConfiguredWhitespaceServiceId(COMPANY_ID, SERVICE_TYPE);
	}
}
