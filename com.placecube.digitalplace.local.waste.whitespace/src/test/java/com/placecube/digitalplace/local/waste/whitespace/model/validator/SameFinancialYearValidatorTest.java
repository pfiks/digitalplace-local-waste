package com.placecube.digitalplace.local.waste.whitespace.model.validator;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.time.Clock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SameFinancialYearValidatorTest {

	@Mock
	private Worksheet mockWorksheet;

	@Mock
	private WhitespaceWasteServiceUtil mockWhitespaceWasteServiceUtil;

	@InjectMocks
	private SameFinancialYearValidator sameFinancialYearValidator;

	@Test
	@Parameters({ "true", "false" })
	public void isValid_WhenNoErrors_ThenReturnsOpositeToIsCurrentFinancialYear(boolean currentFinancialYear) {
		when(mockWhitespaceWasteServiceUtil.isCurrentFinancialYear(Clock.systemDefaultZone(), mockWorksheet)).thenReturn(currentFinancialYear);

		assertThat(sameFinancialYearValidator.isValid(mockWorksheet), equalTo(!currentFinancialYear));
	}
}
