package com.placecube.digitalplace.local.waste.whitespace.application;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteAttachmentResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteContactResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteLogResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteServiceItemRoundScheduleResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteServiceNotificationResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddWorksheetAttachmentResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddWorksheetNotesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CancelWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CreateWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.DeleteSiteContactResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.DeleteSiteServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.DeleteSiteServiceItemRoundScheduleResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetAccountSiteIdResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetActiveAddressesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetAddressesByCoordinatesRadiusResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetAddressesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionByUprnAndDateResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionSlotsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetFullSiteCollectionsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetFullWorksheetDetailsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetInCabLogsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetLogsSearchResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetNotificationsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServicesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteAttachmentsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteAvailableRoundsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionExtraDetailsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteContactsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteContractsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteFlagsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteIdResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteIncidentsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteInfoResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteLogsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteNotificationsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteServiceItemRoundSchedulesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteWorksheetsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSitesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetStreetsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWalkNumbersResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetAttachmentsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetChargeMatrixResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailEventsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailExtraInfoFieldsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailNotesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetExtraInfoFieldsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetRolesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetsByReferenceResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ProgressWorkflowResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateSiteContactResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateSiteServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateSiteServiceNotificationResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateWorkflowEventDateResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfstring;
import com.placecube.digitalplace.local.waste.whitespace.model.ServiceItemInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.ServicePropertyInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.SiteCollectionExtraDetailInputExt;
import com.placecube.digitalplace.local.waste.whitespace.service.JsonResponseParserService;
import com.placecube.digitalplace.local.waste.whitespace.service.ObjectFactoryService;
import com.placecube.digitalplace.local.waste.whitespace.service.WhitespaceRequestService;

@RunWith(PowerMockRunner.class)
public class WhitespaceRESTApplicationTest extends PowerMockito {

	private static final long COMPANY_ID = 89764l;

	private static final ArrayOfstring ARRAY_OF_STRING_1 = new ArrayOfstring();
	private static final ArrayOfstring ARRAY_OF_STRING_2 = new ArrayOfstring();
	private static final ArrayOfstring ARRAY_OF_STRING_3 = new ArrayOfstring();

	private static final String PARAM1 = "PARAM1";
	private static final String PARAM10 = "PARAM10";
	private static final String PARAM11 = "PARAM11";
	private static final String PARAM12 = "PARAM12";
	private static final String PARAM13 = "PARAM13";
	private static final String PARAM14 = "PARAM14";
	private static final String PARAM15 = "PARAM15";
	private static final String PARAM16 = "PARAM16";
	private static final String PARAM17 = "PARAM17";
	private static final String PARAM18 = "PARAM18";
	private static final String PARAM19 = "PARAM19";
	private static final String PARAM2 = "PARAM2";
	private static final String PARAM20 = "PARAM20";
	private static final String PARAM21 = "PARAM21";
	private static final String PARAM3 = "PARAM3";
	private static final String PARAM4 = "PARAM4";
	private static final String PARAM5 = "PARAM5";
	private static final String PARAM6 = "PARAM6";
	private static final String PARAM7 = "PARAM7";
	private static final String PARAM8 = "PARAM8";
	private static final String PARAM9 = "PARAM9";
	private static final String RESPONSE = "[{\"answer\":true}]";

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JsonResponseParserService mockJsonResponseParserService;

	@Mock
	private ObjectFactoryService mockObjectFatoryService;

	@Mock
	private WhitespaceRequestService mockWhitespaceRequestService;

	@InjectMocks
	private WhitespaceRESTApplication whitespaceRESTApplication = new WhitespaceRESTApplication();

	@Before
	public void activate() {
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);

	}

	@Test
	public void addSiteAttachment_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		AddSiteAttachmentResponse mockResponse = mock(AddSiteAttachmentResponse.class);
		when(mockWhitespaceRequestService.addSiteAttachment(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.addSiteAttachment(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void addSiteContact_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		AddSiteContactResponse mockResponse = mock(AddSiteContactResponse.class);
		when(mockWhitespaceRequestService.addSiteContact(COMPANY_ID, PARAM1, PARAM2, true, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.addSiteContact(PARAM1, PARAM2, true, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void addSiteLog_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		AddSiteLogResponse mockResponse = mock(AddSiteLogResponse.class);
		when(mockWhitespaceRequestService.addSiteLog(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.addSiteLog(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void addSiteServiceItem_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		AddSiteServiceItemResponse mockResponse = mock(AddSiteServiceItemResponse.class);
		when(mockWhitespaceRequestService.addSiteServiceItem(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.addSiteServiceItem(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void addSiteServiceItemRoundSchedule_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		AddSiteServiceItemRoundScheduleResponse mockResponse = mock(AddSiteServiceItemRoundScheduleResponse.class);
		when(mockWhitespaceRequestService.addSiteServiceItemRoundSchedule(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.addSiteServiceItemRoundSchedule(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void addSiteServiceNotification_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		AddSiteServiceNotificationResponse mockResponse = mock(AddSiteServiceNotificationResponse.class);
		when(mockWhitespaceRequestService.addSiteServiceNotification(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.addSiteServiceNotification(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void addWorksheetAttachment_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		AddWorksheetAttachmentResponse mockResponse = mock(AddWorksheetAttachmentResponse.class);
		when(mockWhitespaceRequestService.addWorksheetAttachment(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.addWorksheetAttachment(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void addWorksheetNotes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		AddWorksheetNotesResponse mockResponse = mock(AddWorksheetNotesResponse.class);
		when(mockWhitespaceRequestService.addWorksheetNotes(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.addWorksheetNotes(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void cancelWorksheet_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		CancelWorksheetResponse mockResponse = mock(CancelWorksheetResponse.class);
		when(mockWhitespaceRequestService.cancelWorksheet(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.cancelWorksheet(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createWorksheet_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		CreateWorksheetResponse mockResponse = mock(CreateWorksheetResponse.class);
		ServiceItemInputExt mockServiceItemInput = mock(ServiceItemInputExt.class);
		ServicePropertyInputExt mockServicePropertyInputExt = mock(ServicePropertyInputExt.class);
		when(mockWhitespaceRequestService.createWorksheet(COMPANY_ID, mockServicePropertyInputExt, mockServiceItemInput, PARAM1, PARAM2, PARAM3, PARAM4, 1, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9,
				PARAM10, PARAM11, PARAM12, PARAM13, PARAM14, PARAM15, PARAM16)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.createWorksheet(mockServiceItemInput, mockServicePropertyInputExt, PARAM1, PARAM2, PARAM3, PARAM4, 1, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9,
				PARAM10, PARAM11, PARAM12, PARAM13, PARAM14, PARAM15, PARAM16, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void deleteSiteContact_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		DeleteSiteContactResponse mockResponse = mock(DeleteSiteContactResponse.class);
		when(mockWhitespaceRequestService.deleteSiteContact(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.deleteSiteContact(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void deleteSiteServiceItem_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		DeleteSiteServiceItemResponse mockResponse = mock(DeleteSiteServiceItemResponse.class);
		when(mockWhitespaceRequestService.deleteSiteServiceItem(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.deleteSiteServiceItem(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void deleteSiteServiceItemRoundSchedule_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		DeleteSiteServiceItemRoundScheduleResponse mockResponse = mock(DeleteSiteServiceItemRoundScheduleResponse.class);
		when(mockWhitespaceRequestService.deleteSiteServiceItemRoundSchedule(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.deleteSiteServiceItemRoundSchedule(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getAccountSiteId_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetAccountSiteIdResponse mockResponse = mock(GetAccountSiteIdResponse.class);
		when(mockWhitespaceRequestService.getAccountSiteId(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getAccountSiteId(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getActiveAddresses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetActiveAddressesResponse mockResponse = mock(GetActiveAddressesResponse.class);
		when(mockWhitespaceRequestService.getActiveAddresses(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getActiveAddresses(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getAddresses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetAddressesResponse mockResponse = mock(GetAddressesResponse.class);
		when(mockWhitespaceRequestService.getAddresses(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getAddresses(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getAddressesByCoordinatesRadius_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetAddressesByCoordinatesRadiusResponse mockResponse = mock(GetAddressesByCoordinatesRadiusResponse.class);
		when(mockWhitespaceRequestService.getAddressesByCoordinatesRadius(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getAddressesByCoordinatesRadius(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getCollectionByUprnAndDate_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetCollectionByUprnAndDateResponse mockResponse = mock(GetCollectionByUprnAndDateResponse.class);
		String nextCollectionFromDate = "2022-11-02";
		when(mockWhitespaceRequestService.getCollectionByUprnAndDate(COMPANY_ID, PARAM1, nextCollectionFromDate)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getCollectionByUprnAndDate(PARAM1, nextCollectionFromDate, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getCollectionSlots_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetCollectionSlotsResponse mockResponse = mock(GetCollectionSlotsResponse.class);
		when(mockWhitespaceRequestService.getCollectionSlots(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getCollectionSlots(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFullSiteCollections_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetFullSiteCollectionsResponse mockResponse = mock(GetFullSiteCollectionsResponse.class);
		when(mockWhitespaceRequestService.getFullSiteCollections(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getFullSiteCollections(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFullWorksheetDetails_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetFullWorksheetDetailsResponse mockResponse = mock(GetFullWorksheetDetailsResponse.class);
		when(mockWhitespaceRequestService.getFullWorksheetDetails(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getFullWorksheetDetails(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getInCabLogs_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetInCabLogsResponse mockResponse = mock(GetInCabLogsResponse.class);
		List<String> PARAM_LIST1 = new ArrayList<>();
		List<String> PARAM_LIST2 = new ArrayList<>();
		List<String> PARAM_LIST3 = new ArrayList<>();
		PARAM_LIST1.add(PARAM1);
		PARAM_LIST1.add(PARAM2);
		PARAM_LIST2.add(PARAM2);
		PARAM_LIST2.add(PARAM3);
		PARAM_LIST3.add(PARAM4);
		PARAM_LIST3.add(PARAM5);
		when(mockWhitespaceRequestService.getInCabLogs(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, ARRAY_OF_STRING_1, ARRAY_OF_STRING_2, ARRAY_OF_STRING_3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);
		when(mockObjectFatoryService.getArrayOfstring(PARAM_LIST1)).thenReturn(ARRAY_OF_STRING_1);
		when(mockObjectFatoryService.getArrayOfstring(PARAM_LIST2)).thenReturn(ARRAY_OF_STRING_2);
		when(mockObjectFatoryService.getArrayOfstring(PARAM_LIST3)).thenReturn(ARRAY_OF_STRING_3);

		String response = whitespaceRESTApplication.getInCabLogs(PARAM1, PARAM2, PARAM3, PARAM4, PARAM_LIST1, PARAM_LIST2, PARAM_LIST3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getLogsSearch_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetLogsSearchResponse mockResponse = mock(GetLogsSearchResponse.class);
		List<String> PARAM_LIST1 = new ArrayList<>();
		PARAM_LIST1.add(PARAM1);
		PARAM_LIST1.add(PARAM2);

		when(mockObjectFatoryService.getArrayOfstring(PARAM_LIST1)).thenReturn(ARRAY_OF_STRING_1);
		when(mockWhitespaceRequestService.getLogsSearch(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, ARRAY_OF_STRING_1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getLogsSearch(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM_LIST1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getNotifications_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetNotificationsResponse mockResponse = mock(GetNotificationsResponse.class);
		when(mockWhitespaceRequestService.getNotifications(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getNotifications(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceItems_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetServiceItemsResponse mockResponse = mock(GetServiceItemsResponse.class);
		when(mockWhitespaceRequestService.getServiceItems(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getServiceItems(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServices_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetServicesResponse mockResponse = mock(GetServicesResponse.class);
		when(mockWhitespaceRequestService.getServices(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getServices(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteAttachments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteAttachmentsResponse mockResponse = mock(GetSiteAttachmentsResponse.class);
		when(mockWhitespaceRequestService.getSiteAttachments(COMPANY_ID, PARAM1, PARAM2, true)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteAttachments(PARAM1, PARAM2, true, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteAvailableRounds_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteAvailableRoundsResponse mockResponse = mock(GetSiteAvailableRoundsResponse.class);
		when(mockWhitespaceRequestService.getSiteAvailableRounds(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteAvailableRounds(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteCollectionExtraDetails_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteCollectionExtraDetailsResponse mockResponse = mock(GetSiteCollectionExtraDetailsResponse.class);
		when(mockWhitespaceRequestService.getSiteCollectionExtraDetails(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteCollectionExtraDetails(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteCollections_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteCollectionsResponse mockResponse = mock(GetSiteCollectionsResponse.class);
		when(mockWhitespaceRequestService.getSiteCollections(COMPANY_ID, PARAM1, PARAM2, true)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteCollections(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteContacts_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteContactsResponse mockResponse = mock(GetSiteContactsResponse.class);
		when(mockWhitespaceRequestService.getSiteContacts(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteContacts(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteContracts_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteContractsResponse mockResponse = mock(GetSiteContractsResponse.class);
		when(mockWhitespaceRequestService.getSiteContracts(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteContracts(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteFlags_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteFlagsResponse mockResponse = mock(GetSiteFlagsResponse.class);
		when(mockWhitespaceRequestService.getSiteFlags(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteFlags(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteId_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteIdResponse mockResponse = mock(GetSiteIdResponse.class);
		when(mockWhitespaceRequestService.getSiteId(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteId(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteIncidents_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteIncidentsResponse mockResponse = mock(GetSiteIncidentsResponse.class);
		when(mockWhitespaceRequestService.getSiteIncidents(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteIncidents(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteInfo_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteInfoResponse mockResponse = mock(GetSiteInfoResponse.class);
		when(mockWhitespaceRequestService.getSiteInfo(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteInfo(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteLogs_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteLogsResponse mockResponse = mock(GetSiteLogsResponse.class);
		when(mockWhitespaceRequestService.getSiteLogs(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteLogs(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteNotifications_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteNotificationsResponse mockResponse = mock(GetSiteNotificationsResponse.class);
		when(mockWhitespaceRequestService.getSiteNotifications(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteNotifications(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSites_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSitesResponse mockResponse = mock(GetSitesResponse.class);
		when(mockWhitespaceRequestService.getSites(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSites(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteServiceItemRoundSchedules_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteServiceItemRoundSchedulesResponse mockResponse = mock(GetSiteServiceItemRoundSchedulesResponse.class);
		when(mockWhitespaceRequestService.getSiteServiceItemRoundSchedules(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteServiceItemRoundSchedules(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteWorksheets_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetSiteWorksheetsResponse mockResponse = mock(GetSiteWorksheetsResponse.class);
		when(mockWhitespaceRequestService.getSiteWorksheets(COMPANY_ID, PARAM1, PARAM2, PARAM3, true, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getSiteWorksheets(PARAM1, PARAM2, PARAM3, true, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getStreets_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetStreetsResponse mockResponse = mock(GetStreetsResponse.class);
		when(mockWhitespaceRequestService.getStreets(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getStreets(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWalkNumbers_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWalkNumbersResponse mockResponse = mock(GetWalkNumbersResponse.class);
		when(mockWhitespaceRequestService.getWalkNumbers(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWalkNumbers(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetAttachments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetAttachmentsResponse mockResponse = mock(GetWorksheetAttachmentsResponse.class);
		when(mockWhitespaceRequestService.getWorksheetAttachments(COMPANY_ID, PARAM1, PARAM2, true)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetAttachments(PARAM1, PARAM2, true, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetChargeMatrix_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetChargeMatrixResponse mockResponse = mock(GetWorksheetChargeMatrixResponse.class);
		when(mockWhitespaceRequestService.getWorksheetChargeMatrix(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetChargeMatrix(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetDetailEvents_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetDetailEventsResponse mockResponse = mock(GetWorksheetDetailEventsResponse.class);
		when(mockWhitespaceRequestService.getWorksheetDetailEvents(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetDetailEvents(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetDetailExtraInfoFields_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetDetailExtraInfoFieldsResponse mockResponse = mock(GetWorksheetDetailExtraInfoFieldsResponse.class);
		when(mockWhitespaceRequestService.getWorksheetDetailExtraInfoFields(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetDetailExtraInfoFields(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetDetailNotes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetDetailNotesResponse mockResponse = mock(GetWorksheetDetailNotesResponse.class);
		when(mockWhitespaceRequestService.getWorksheetDetailNotes(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetDetailNotes(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetDetails_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetDetailsResponse mockResponse = mock(GetWorksheetDetailsResponse.class);
		when(mockWhitespaceRequestService.getWorksheetDetails(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetDetails(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetDetailServiceItems_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetDetailServiceItemsResponse mockResponse = mock(GetWorksheetDetailServiceItemsResponse.class);
		when(mockWhitespaceRequestService.getWorksheetDetailServiceItems(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetDetailServiceItems(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetExtraInfoFields_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetExtraInfoFieldsResponse mockResponse = mock(GetWorksheetExtraInfoFieldsResponse.class);
		when(mockWhitespaceRequestService.getWorksheetExtraInfoFields(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetExtraInfoFields(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetRoles_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetRolesResponse mockResponse = mock(GetWorksheetRolesResponse.class);
		when(mockWhitespaceRequestService.getWorksheetRoles(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetRoles(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetsByReference_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetsByReferenceResponse mockResponse = mock(GetWorksheetsByReferenceResponse.class);
		when(mockWhitespaceRequestService.getWorksheetsByReference(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetsByReference(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorksheetServiceItems_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		GetWorksheetServiceItemsResponse mockResponse = mock(GetWorksheetServiceItemsResponse.class);
		when(mockWhitespaceRequestService.getWorksheetServiceItems(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.getWorksheetServiceItems(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void progressWorkflow_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ProgressWorkflowResponse mockResponse = mock(ProgressWorkflowResponse.class);
		when(mockWhitespaceRequestService.progressWorkflow(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.progressWorkflow(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void updateSiteContact_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		UpdateSiteContactResponse mockResponse = mock(UpdateSiteContactResponse.class);
		when(mockWhitespaceRequestService.updateSiteContact(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, true, PARAM7, PARAM8)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.updateSiteContact(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, true, PARAM7, PARAM8, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void updateSiteServiceItem_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		UpdateSiteServiceItemResponse mockResponse = mock(UpdateSiteServiceItemResponse.class);
		SiteCollectionExtraDetailInputExt siteCollectionExtraDetailInputExt = mock(SiteCollectionExtraDetailInputExt.class);
		when(mockWhitespaceRequestService.updateSiteServiceItem(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, siteCollectionExtraDetailInputExt)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.updateSiteServiceItem(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, siteCollectionExtraDetailInputExt, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void updateSiteServiceNotification_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		UpdateSiteServiceNotificationResponse mockResponse = mock(UpdateSiteServiceNotificationResponse.class);
		when(mockWhitespaceRequestService.updateSiteServiceNotification(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.updateSiteServiceNotification(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void updateWorkflowEventDate_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		UpdateWorkflowEventDateResponse mockResponse = mock(UpdateWorkflowEventDateResponse.class);
		when(mockWhitespaceRequestService.updateWorkflowEventDate(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.updateWorkflowEventDate(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void updateWorksheet_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		UpdateWorksheetResponse mockResponse = mock(UpdateWorksheetResponse.class);
		ServicePropertyInputExt mockServicePropertyInputExt = mock(ServicePropertyInputExt.class);
		when(mockWhitespaceRequestService.updateWorksheet(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12, PARAM13, PARAM14, PARAM15,
				PARAM16, PARAM17, PARAM18, PARAM19, PARAM20, PARAM21, mockServicePropertyInputExt)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = whitespaceRESTApplication.updateWorksheet(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12, PARAM13, PARAM14, PARAM15,
				PARAM16, PARAM17, PARAM18, PARAM19, PARAM20, PARAM21, mockServicePropertyInputExt, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

}
