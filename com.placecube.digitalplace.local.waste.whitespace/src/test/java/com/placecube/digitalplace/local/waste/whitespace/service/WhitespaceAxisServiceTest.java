package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.local.waste.whitespace.axis2.WSAPIServiceStub;
import com.placecube.digitalplace.local.waste.whitespace.configuration.WhitespaceCompanyConfiguration;

public class WhitespaceAxisServiceTest extends PowerMockito {

	@Mock
	private WhitespaceCompanyConfiguration mockWhitespaceCompanyConfiguration;

	@Mock
	private WhitespaceConfigurationService mockWhitespaceWasteService;

	@InjectMocks
	private WhitespaceAxisService whitespaceAxisService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getWhitespaceAxisService_WhenNoErrors_ThenReturnsWSAPIStub() throws Exception {

		long companyId = 10;
		String url = "https://msbapi-municipal-uat.whitespacews.com/WSAPIService.svc";
		when(mockWhitespaceWasteService.getConfiguration(companyId)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.whitespaceBaseUrl()).thenReturn(url);

		WSAPIServiceStub result = whitespaceAxisService.getWhitespaceAxisService(companyId);

		assertThat(result, instanceOf(WSAPIServiceStub.class));
	}

}