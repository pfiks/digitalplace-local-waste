package com.placecube.digitalplace.local.waste.whitespace.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.service.ResponseParserService;
import com.placecube.digitalplace.local.waste.whitespace.service.WhitespaceRequestService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WhitespaceWasteServiceUtilTest {

	@InjectMocks
	private WhitespaceWasteServiceUtil whitespaceWasteServiceUtil;

	@Mock
	private WhitespaceRequestService mockWhiteSpaceRequestService;

	@Mock
	private ResponseParserService mockResponseParserService;

	@Mock
	private Worksheet mockWorksheet;

	@Test
	public void isCurrentFinancialYear_WhenWorksheetCreatedDateIsNull_ThenReturnsTrue() {
		assertThat(whitespaceWasteServiceUtil.isCurrentFinancialYear(Clock.systemDefaultZone(), mockWorksheet), equalTo(true));
	}

	@Test
	public void isCurrentFinancialYear_WhenFinancialYearStartIsInCurrentNaturalYearAndWorksheetCreatedDateIsAfterFinancialYearStart_ThenReturnsTrue() {
		Instant instant = Instant.parse("2024-10-08T01:20:00.00Z");
		final Clock fixedClock = Clock.fixed(instant, ZoneId.systemDefault());

		Instant createdDateInstant = Instant.parse("2024-06-01T00:00:00.00Z");
		Calendar createdDateCalendar = GregorianCalendar.from(createdDateInstant.atZone(ZoneId.systemDefault()));

		when(mockWorksheet.getWorksheetCreatedDate()).thenReturn(createdDateCalendar);

		assertThat(whitespaceWasteServiceUtil.isCurrentFinancialYear(fixedClock, mockWorksheet), equalTo(true));
	}

	@Test
	public void isCurrentFinancialYear_WhenFinancialYearStartIsInCurrentNaturalYearAndWorksheetCreatedDateIsBeforeFinancialYearStart_ThenReturnsFalse() {
		Instant instant = Instant.parse("2024-10-08T01:20:00.00Z");
		final Clock fixedClock = Clock.fixed(instant, ZoneId.systemDefault());

		Instant createdDateInstant = Instant.parse("2024-02-01T00:00:00.00Z");
		Calendar createdDateCalendar = GregorianCalendar.from(createdDateInstant.atZone(ZoneId.systemDefault()));

		when(mockWorksheet.getWorksheetCreatedDate()).thenReturn(createdDateCalendar);

		assertThat(whitespaceWasteServiceUtil.isCurrentFinancialYear(fixedClock, mockWorksheet), equalTo(false));
	}

	@Test
	public void isCurrentFinancialYear_WhenFinancialYearStartIsInCurrentNaturalYearAndWorksheetCreatedDateIsFinancialYearStart_ThenReturnsTrue() {
		Instant instant = Instant.parse("2024-10-08T01:20:00.00Z");
		final Clock fixedClock = Clock.fixed(instant, ZoneId.systemDefault());

		Instant createdDateInstant = Instant.parse("2024-04-06T00:00:00.00Z");
		Calendar createdDateCalendar = GregorianCalendar.from(createdDateInstant.atZone(ZoneId.systemDefault()));

		when(mockWorksheet.getWorksheetCreatedDate()).thenReturn(createdDateCalendar);

		assertThat(whitespaceWasteServiceUtil.isCurrentFinancialYear(fixedClock, mockWorksheet), equalTo(true));
	}

	@Test
	public void isCurrentFinancialYear_WhenFinancialYearStartIsInPreviousNaturalYearAndWorksheetCreatedDateIsAfterFinancialYearStart_ThenReturnsTrue() {
		Instant instant = Instant.parse("2024-01-08T01:20:00.00Z");
		final Clock fixedClock = Clock.fixed(instant, ZoneId.systemDefault());

		Instant createdDateInstant = Instant.parse("2023-06-01T00:00:00.00Z");
		Calendar createdDateCalendar = GregorianCalendar.from(createdDateInstant.atZone(ZoneId.systemDefault()));

		when(mockWorksheet.getWorksheetCreatedDate()).thenReturn(createdDateCalendar);

		assertThat(whitespaceWasteServiceUtil.isCurrentFinancialYear(fixedClock, mockWorksheet), equalTo(true));
	}

	@Test
	public void isCurrentFinancialYear_WhenFinancialYearStartIsInPreviousNaturalYearAndWorksheetCreatedDateIsBeforeFinancialYearStart_ThenReturnsFalse() {
		Instant instant = Instant.parse("2024-01-08T01:20:00.00Z");
		final Clock fixedClock = Clock.fixed(instant, ZoneId.systemDefault());

		Instant createdDateInstant = Instant.parse("2023-02-01T00:00:00.00Z");
		Calendar createdDateCalendar = GregorianCalendar.from(createdDateInstant.atZone(ZoneId.systemDefault()));

		when(mockWorksheet.getWorksheetCreatedDate()).thenReturn(createdDateCalendar);

		assertThat(whitespaceWasteServiceUtil.isCurrentFinancialYear(fixedClock, mockWorksheet), equalTo(false));
	}

	@Test
	public void isCurrentFinancialYear_WhenFinancialYearStartIsInPreviousNaturalYearAndWorksheetCreatedDateIsFinancialYearStart_ThenReturnsTrue() {
		Instant instant = Instant.parse("2024-01-08T01:20:00.00Z");
		final Clock fixedClock = Clock.fixed(instant, ZoneId.systemDefault());

		Instant createdDateInstant = Instant.parse("2023-04-06T00:00:00.00Z");
		Calendar createdDateCalendar = GregorianCalendar.from(createdDateInstant.atZone(ZoneId.systemDefault()));

		when(mockWorksheet.getWorksheetCreatedDate()).thenReturn(createdDateCalendar);

		assertThat(whitespaceWasteServiceUtil.isCurrentFinancialYear(fixedClock, mockWorksheet), equalTo(true));
	}

	@Test
	public void isWorksheetCreatedDateInTwoLastWeeks_WhenWorksheetCreatedDateIsWithinLastTwoWeeks_ThenReturnsTrue() {
		Instant instant = Instant.parse("2024-01-16T01:20:00.00Z");
		final Clock fixedClock = Clock.fixed(instant, ZoneId.systemDefault());

		Instant createdDateInstant = Instant.parse("2024-01-02T00:00:00.00Z");
		Calendar createdDateCalendar = GregorianCalendar.from(createdDateInstant.atZone(ZoneId.systemDefault()));

		when(mockWorksheet.getWorksheetCreatedDate()).thenReturn(createdDateCalendar);

		assertThat(whitespaceWasteServiceUtil.isWorksheetCreatedDateInTwoLastWeeks(fixedClock, mockWorksheet), equalTo(true));
	}

	@Test
	public void isWorksheetCreatedDateInTwoLastWeeks_WhenWorksheetCreatedDateIsNotWithinLastTwoWeeks_ThenReturnsFalse() {
		Instant instant = Instant.parse("2024-01-16T01:20:00.00Z");
		final Clock fixedClock = Clock.fixed(instant, ZoneId.systemDefault());

		Instant createdDateInstant = Instant.parse("2024-01-01T00:00:00.00Z");
		Calendar createdDateCalendar = GregorianCalendar.from(createdDateInstant.atZone(ZoneId.systemDefault()));

		when(mockWorksheet.getWorksheetCreatedDate()).thenReturn(createdDateCalendar);

		assertThat(whitespaceWasteServiceUtil.isWorksheetCreatedDateInTwoLastWeeks(fixedClock, mockWorksheet), equalTo(false));
	}

	@Test
	public void isWorksheetCreatedDateInTwoLastWeeks_WhenWorksheetCreatedDateIsNull_ThenReturnsTrue() {
		final Clock fixedClock = Clock.systemUTC();

		assertThat(whitespaceWasteServiceUtil.isWorksheetCreatedDateInTwoLastWeeks(fixedClock, mockWorksheet), equalTo(true));
	}

	@Test
	public void isBeforeClockDate_IfParameterDateIsBeforeClockDate_ThenReturnsTrue() {

		final Clock fixedClock = Clock.fixed(new Date().toInstant(), ZoneId.systemDefault());

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -1);

		assertThat(whitespaceWasteServiceUtil.isBeforeClockDate(fixedClock, calendar.getTime()), equalTo(true));
	}

	@Test
	public void isBeforeClockDate_IfParameterDateIsSameDateAsClock_ThenReturnsFalse() {

		final Clock fixedClock = Clock.fixed(new Date().toInstant(), ZoneId.systemDefault());

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.SECOND, -1);

		assertThat(whitespaceWasteServiceUtil.isBeforeClockDate(fixedClock, calendar.getTime()), equalTo(false));
	}

	@Test
	public void isBeforeClockDate_IfParameterDateIsAfterClockDate_ThenReturnsFalse() {

		final Clock fixedClock = Clock.fixed(new Date().toInstant(), ZoneId.systemDefault());

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 1);

		assertThat(whitespaceWasteServiceUtil.isBeforeClockDate(fixedClock, calendar.getTime()), equalTo(false));
	}

	@Test
	@Parameters({ "MonFort1,Monday fornightly", "MonFort2,Monday fornightly", "TueFort1,Tuesday fornightly", "TueFort2,Tuesday fornightly", "WedFort1,Wednesday fornightly",
			"WedFort2,Wednesday fornightly", "ThuFort2,Thursday fornightly", "ThuFort2,Thursday fornightly", "FriFort1,Friday fornightly", "FriFort2,Friday fornightly", "SatFort1,Saturday fornightly",
			"SatFort2,Saturday fornightly", "SunFort1,Sunday fornightly", "SunFort2,Sunday fornightly", "GardenMonFort1,Monday fornightly", "GardenMonFort2,Monday fornightly",
			"Monday fornightly,Monday fornightly" })
	public void formatSchedule_WhenNoErrors_ThenReturnsFormattedSchedule(String schedule, String formattedSchedule) {

		assertThat(whitespaceWasteServiceUtil.formatSchedule(schedule), equalTo(formattedSchedule));
	}

}
