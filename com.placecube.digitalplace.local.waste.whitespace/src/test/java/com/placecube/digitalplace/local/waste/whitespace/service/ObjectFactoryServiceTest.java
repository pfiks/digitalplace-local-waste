package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetContext;
import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetContext.WorksheetContextBuilder;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

public class ObjectFactoryServiceTest {

	private ObjectFactoryService objectFactoryService;

	@Mock
	private WhitespaceWasteServiceUtil mockWhitespaceWasteServiceUtil;

	@Before
	public void activateSetup() {
		objectFactoryService = new ObjectFactoryService();
	}

	@Test
	public void getWorksheetContextBuilder_WhenNoErrors_ThenCreatesWorksheetContextBuilderWithProperties() {
		final long companyId = 234L;
		final String uprn = "234234324";

		WorksheetContextBuilder result = objectFactoryService.getWorksheetContextBuilder(companyId, uprn);

		assertThat(result, notNullValue());

		WorksheetContext context = result.build();

		assertThat(context.getCompanyId(), equalTo(companyId));
		assertThat(context.getUprn(), equalTo(uprn));
	}

	@Test
	public void createSameFinancialYearValidator_WhenNoErrors_ThenCreatesSameFinancialYearValidator() {
		assertThat(objectFactoryService.createSameFinancialYearValidator(mockWhitespaceWasteServiceUtil), notNullValue());
	}

	@Test
	public void createSameFinancialYearValidator_WhenNoErrors_ThenCreatesTwoLastWeeksValidator() {
		assertThat(objectFactoryService.createSameFinancialYearValidator(mockWhitespaceWasteServiceUtil), notNullValue());
	}

}
