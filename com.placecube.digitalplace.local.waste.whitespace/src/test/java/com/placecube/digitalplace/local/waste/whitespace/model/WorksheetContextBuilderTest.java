package com.placecube.digitalplace.local.waste.whitespace.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetContext.WorksheetContextBuilder;

public class WorksheetContextBuilderTest {

	@Test
	public void build_WhenNoErrors_ThenBuildsWorksheetContextWithProperties() {
		final long companyId = 3214L;
		final long formInstanceId = 22L;
		final String uprn = "345435";
		final List<String> binTypes = Arrays.asList("type");
		final List<String> binSizes = Arrays.asList("size");
		final String firstName = "first";
		final String lastName = "last";
		final List<Integer> quantities = Arrays.asList(1);
		final String worksheetMessage = "message";
		final int adHocRoundInstanceId = 45;

		WorksheetContextBuilder builder = new WorksheetContextBuilder(companyId, uprn) //
				.setFormInstanceRecordId(formInstanceId)//
				.setBinTypes(binTypes) //
				.setBinSizes(binSizes) //
				.setFirstName(firstName) //
				.setLastName(lastName) //
				.setQuantities(quantities) //
				.setWorksheetMessage(worksheetMessage) //
				.setAdHocRoundInstanceId(adHocRoundInstanceId);

		WorksheetContext context = builder.build();

		assertThat(context.getCompanyId(), equalTo(companyId));
		assertThat(context.getFormInstanceRecordId(), equalTo(formInstanceId));
		assertThat(context.getUprn(), equalTo(uprn));
		assertThat(context.getBinTypes(), sameInstance(binTypes));
		assertThat(context.getBinSizes(), sameInstance(binSizes));
		assertThat(context.getFirstName(), equalTo(firstName));
		assertThat(context.getLastName(), equalTo(lastName));
		assertThat(context.getQuantities(), sameInstance(quantities));
		assertThat(context.getWorksheetMessage(), equalTo(worksheetMessage));
		assertThat(context.getAdHocRoundInstanceId(), equalTo(adHocRoundInstanceId));

	}
}
