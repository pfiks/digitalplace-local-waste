package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CreateWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.model.ServiceItemInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;
import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetContext;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.WorksheetValidator;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WhitespaceWorksheetServiceTest {

	private static final List<Integer> QUANTITIES = Arrays.asList(3);
	private static final long COMPANY_ID = 10;
	private static final String UPRN = "12345678";
	private static final List<String> BIN_SIZES = Arrays.asList("240");
	private static final List<String> BIN_TYPES = Arrays.asList("cardboard");
	private static final String FIRST_NAME = "first";
	private static final String LAST_NAME = "last";
	private static final String SERVICE_ID = "435321";
	private static final String SERVICE_KEY = "serviceKey";
	private static final String SERVICE_NAME = "serviceName";
	private static final String SERVICE_ITEM_ID = "34232";
	private static final int SLOT_ID = 322;
	private static final String WORKSHEET_MESSAGE = "New waste container request";

	private static final long FORM_INSTANCE_ID = 8764L;
	private static final long FORM_INSTANCE_RECORD_ID = 23423L;

	@InjectMocks
	private WhitespaceWorksheetService whitespaceWorksheetService;

	@Mock
	private ResponseParserService mockResponseParserService;

	@Mock
	private WhitespaceConfigurationService mockWhitespaceConfigurationService;

	@Mock
	private WhitespaceWasteService mockWhitespaceWasteService;

	@Mock
	private WhitespaceRequestService mockWhitespaceRequestService;

	@Mock
	private WhitespaceService mockWhitespaceService;

	@Mock
	private ServiceItem mockServiceItem;

	@Mock
	private Service mockService;

	@Mock
	private ServiceItemInputExt mockServiceItemInputExt;

	@Mock
	private CreateWorksheetResponse mockCreateWorksheetResponse;

	@Mock
	private WorksheetContext mockWorksheetContext;

	@Mock
	private WorksheetValidator mockWorksheetValidator;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordLocalService;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Test
	public void createWorksheet_WhenNoErrors_ThenCreatesWorksheetForConfiguredServiceKey() throws Exception {
		final String worksheetRef = "ref";

		mockWorksheetContext();

		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);

		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, SERVICE_KEY)).thenReturn(mockWhitespaceService);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockWhitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID)).thenReturn(mockServiceItem);

		when(mockWhitespaceWasteService.createServiceItemInputExt(Arrays.asList(mockServiceItem), QUANTITIES)).thenReturn(mockServiceItemInputExt);

		String workLocationName = FIRST_NAME + StringPool.SPACE + LAST_NAME;

		when(mockWhitespaceRequestService.createWorksheet(COMPANY_ID, null, mockServiceItemInputExt, UPRN, null, SERVICE_ID, null, null, null, String.valueOf(SLOT_ID), null, null, null, null,
				workLocationName, null, WORKSHEET_MESSAGE, String.valueOf(FORM_INSTANCE_RECORD_ID), null, null)).thenReturn(mockCreateWorksheetResponse);

		when(mockResponseParserService.parseCreateWorksheetResponse(mockCreateWorksheetResponse)).thenReturn(worksheetRef);

		String result = whitespaceWorksheetService.createWorksheet(mockWorksheetContext, SERVICE_KEY);

		assertThat(result, equalTo(worksheetRef));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createWorksheet_WhenConfiguredWasteServiceRetrievalFails_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, SERVICE_KEY)).thenThrow(new ConfigurationException());

		whitespaceWorksheetService.createWorksheet(mockWorksheetContext, SERVICE_KEY);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createWorksheet_WhenWorksheetCreationFails_ThenThrowsWasteRetrievalException() throws Exception {
		mockWorksheetContext();

		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, SERVICE_KEY)).thenReturn(mockWhitespaceService);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockWhitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID)).thenReturn(mockServiceItem);

		when(mockWhitespaceWasteService.createServiceItemInputExt(Arrays.asList(mockServiceItem), QUANTITIES)).thenReturn(mockServiceItemInputExt);

		String worksheetMessage = "New waste container request";
		String workLocationName = FIRST_NAME + StringPool.SPACE + LAST_NAME;

		when(mockWhitespaceRequestService.createWorksheet(COMPANY_ID, null, mockServiceItemInputExt, UPRN, null, SERVICE_ID, null, null, null, String.valueOf(SLOT_ID), null, null, null, null,
				workLocationName, null, worksheetMessage, String.valueOf(FORM_INSTANCE_RECORD_ID), null, null)).thenThrow(new Exception());

		whitespaceWorksheetService.createWorksheet(mockWorksheetContext, SERVICE_KEY);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createWorksheet_WhenWorksheetCreationResponseParseFails_ThenThrowsWasteRetrievalException() throws Exception {
		mockWorksheetContext();

		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, SERVICE_KEY)).thenReturn(mockWhitespaceService);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockWhitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID)).thenReturn(mockServiceItem);

		when(mockWhitespaceWasteService.createServiceItemInputExt(Arrays.asList(mockServiceItem), QUANTITIES)).thenReturn(mockServiceItemInputExt);

		String worksheetMessage = "New waste container request";
		String workLocationName = FIRST_NAME + StringPool.SPACE + LAST_NAME;

		when(mockWhitespaceRequestService.createWorksheet(COMPANY_ID, null, mockServiceItemInputExt, UPRN, null, SERVICE_ID, null, null, null, String.valueOf(SLOT_ID), null, null, null, null,
				workLocationName, null, worksheetMessage, String.valueOf(FORM_INSTANCE_RECORD_ID), null, null)).thenReturn(mockCreateWorksheetResponse);

		when(mockResponseParserService.parseCreateWorksheetResponse(mockCreateWorksheetResponse)).thenThrow(new WasteRetrievalException());

		whitespaceWorksheetService.createWorksheet(mockWorksheetContext, SERVICE_KEY);
	}

	@Test
	public void createWorksheetForDefaultService_WhenNoErrors_ThenCreatesWorksheetForDefaultServiceKey() throws Exception {
		final String worksheetRef = "ref";

		mockWorksheetContext();

		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, "default")).thenReturn(mockWhitespaceService);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockWhitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID)).thenReturn(mockServiceItem);

		when(mockWhitespaceWasteService.createServiceItemInputExt(Arrays.asList(mockServiceItem), QUANTITIES)).thenReturn(mockServiceItemInputExt);

		String workLocationName = FIRST_NAME + StringPool.SPACE + LAST_NAME;

		when(mockWhitespaceRequestService.createWorksheet(COMPANY_ID, null, mockServiceItemInputExt, UPRN, null, SERVICE_ID, null, null, null, String.valueOf(SLOT_ID), null, null, null, null,
				workLocationName, null, WORKSHEET_MESSAGE, String.valueOf(FORM_INSTANCE_RECORD_ID), null, null)).thenReturn(mockCreateWorksheetResponse);

		when(mockResponseParserService.parseCreateWorksheetResponse(mockCreateWorksheetResponse)).thenReturn(worksheetRef);

		String result = whitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext);

		assertThat(result, equalTo(worksheetRef));
	}

	@Test
	public void createWorksheetForDefaultService_WhenConfiguredBinSizesDontMatchBinTypes_ThenCreatesWorksheetForDefaultServiceKeyAndDefaultBinSize() throws Exception {
		final String worksheetRef = "ref";

		mockWorksheetContext();
		when(mockWorksheetContext.getBinSizes()).thenReturn(Collections.emptyList());

		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, "default")).thenReturn(mockWhitespaceService);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), StringPool.BLANK, FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockWhitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID)).thenReturn(mockServiceItem);

		when(mockWhitespaceWasteService.createServiceItemInputExt(Arrays.asList(mockServiceItem), QUANTITIES)).thenReturn(mockServiceItemInputExt);

		String workLocationName = FIRST_NAME + StringPool.SPACE + LAST_NAME;

		when(mockWhitespaceRequestService.createWorksheet(COMPANY_ID, null, mockServiceItemInputExt, UPRN, null, SERVICE_ID, null, null, null, String.valueOf(SLOT_ID), null, null, null, null,
				workLocationName, null, WORKSHEET_MESSAGE, String.valueOf(FORM_INSTANCE_RECORD_ID), null, null)).thenReturn(mockCreateWorksheetResponse);

		when(mockResponseParserService.parseCreateWorksheetResponse(mockCreateWorksheetResponse)).thenReturn(worksheetRef);

		String result = whitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext);

		assertThat(result, equalTo(worksheetRef));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createWorksheetForDefaultService_WhenConfiguredWasteServiceRetrievalFails_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, "default")).thenThrow(new ConfigurationException());

		whitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createWorksheetForDefaultService_WhenWorksheetCreationFails_ThenThrowsWasteRetrievalException() throws Exception {
		mockWorksheetContext();

		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceService(COMPANY_ID, FORM_INSTANCE_ID, "default")).thenReturn(mockWhitespaceService);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockWhitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID)).thenReturn(mockServiceItem);

		when(mockWhitespaceWasteService.createServiceItemInputExt(Arrays.asList(mockServiceItem), QUANTITIES)).thenReturn(mockServiceItemInputExt);

		String worksheetMessage = "New waste container request";
		String workLocationName = FIRST_NAME + StringPool.SPACE + LAST_NAME;

		when(mockWhitespaceRequestService.createWorksheet(COMPANY_ID, null, mockServiceItemInputExt, UPRN, null, SERVICE_ID, null, null, null, String.valueOf(SLOT_ID), null, null, null, null,
				workLocationName, null, worksheetMessage, String.valueOf(FORM_INSTANCE_RECORD_ID), null, null)).thenThrow(new Exception());

		whitespaceWorksheetService.createWorksheetForDefaultService(mockWorksheetContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isWorksheetContextValidForDefaultService_WhenWhitespaceServiceExists_ThenReturnsOpenWorksheetCheckForServiceAndServiceItems(boolean isValid) throws Exception {
		mockWorksheetContext();

		when(mockWorksheetContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWorksheetContext.getUprn()).thenReturn(UPRN);
		Map<String, WhitespaceService> serviceMappings = new HashMap<>();
		serviceMappings.put("default", mockWhitespaceService);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(serviceMappings);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockService.getServiceName()).thenReturn(SERVICE_NAME);

		when(mockWhitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator)))
				.thenReturn(isValid);

		boolean result = whitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, FORM_INSTANCE_ID, Optional.of(mockWorksheetValidator));

		assertThat(result, equalTo(!isValid));
	}

	@Test(expected = WasteRetrievalException.class)
	public void isWorksheetContextValidForDefaultService_WhenConfiguredWhitespaceServiceDoesNotExist_ThenThrowsWasteRetrievalException() throws Exception {
		mockWorksheetContext();

		when(mockWorksheetContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWorksheetContext.getUprn()).thenReturn(UPRN);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(new HashMap<>());

		whitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, FORM_INSTANCE_ID, Optional.of(mockWorksheetValidator));
	}

	@Test(expected = WasteRetrievalException.class)
	public void isWorksheetContextValidForDefaultService_WhenWorksheetCheckFails_ThenThrowsWasteRetrievalException() throws Exception {
		mockWorksheetContext();

		when(mockWorksheetContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWorksheetContext.getUprn()).thenReturn(UPRN);
		Map<String, WhitespaceService> serviceMappings = new HashMap<>();
		serviceMappings.put("default", mockWhitespaceService);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(serviceMappings);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockService.getServiceName()).thenReturn(SERVICE_NAME);

		when(mockWhitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator)))
				.thenThrow(new Exception());

		whitespaceWorksheetService.isWorksheetContextValidForDefaultService(mockWorksheetContext, FORM_INSTANCE_ID, Optional.of(mockWorksheetValidator));
	}

	@Test
	@Parameters({ "true", "false" })
	public void isWorksheetContextValidForService_WhenWhitespaceServiceExists_ThenReturnsOpenWorksheetCheckForServiceAndServiceItems(boolean isValid) throws Exception {
		mockWorksheetContext();

		when(mockWorksheetContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWorksheetContext.getUprn()).thenReturn(UPRN);
		Map<String, WhitespaceService> serviceMappings = new HashMap<>();
		serviceMappings.put(SERVICE_KEY, mockWhitespaceService);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(serviceMappings);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockService.getServiceName()).thenReturn(SERVICE_NAME);

		when(mockWhitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator)))
				.thenReturn(isValid);

		boolean result = whitespaceWorksheetService.isWorksheetContextValidForService(mockWorksheetContext, FORM_INSTANCE_ID, SERVICE_KEY, Optional.of(mockWorksheetValidator));

		assertThat(result, equalTo(!isValid));
	}

	@Test(expected = WasteRetrievalException.class)
	public void isWorksheetContextValidForService_WhenConfiguredWhitespaceServiceDoesNotExist_ThenThrowsWasteRetrievalException() throws Exception {
		mockWorksheetContext();

		when(mockWorksheetContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWorksheetContext.getUprn()).thenReturn(UPRN);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(new HashMap<>());

		whitespaceWorksheetService.isWorksheetContextValidForService(mockWorksheetContext, FORM_INSTANCE_ID, SERVICE_KEY, Optional.of(mockWorksheetValidator));
	}

	@Test(expected = WasteRetrievalException.class)
	public void isWorksheetContextValidForService_WhenWorksheetCheckFails_ThenThrowsWasteRetrievalException() throws Exception {
		mockWorksheetContext();

		when(mockWorksheetContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWorksheetContext.getUprn()).thenReturn(UPRN);
		Map<String, WhitespaceService> serviceMappings = new HashMap<>();
		serviceMappings.put(SERVICE_KEY, mockWhitespaceService);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(serviceMappings);
		when(mockWhitespaceWasteService.getServiceItemId(mockWhitespaceService, BIN_TYPES.get(0), BIN_SIZES.get(0), FORM_INSTANCE_ID)).thenReturn(SERVICE_ITEM_ID);
		when(mockWhitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService)).thenReturn(mockService);
		when(mockService.getServiceName()).thenReturn(SERVICE_NAME);

		when(mockWhitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator)))
				.thenThrow(new Exception());

		whitespaceWorksheetService.isWorksheetContextValidForService(mockWorksheetContext, FORM_INSTANCE_ID, SERVICE_KEY, Optional.of(mockWorksheetValidator));
	}

	private void mockWorksheetContext() {
		when(mockWorksheetContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWorksheetContext.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockWorksheetContext.getUprn()).thenReturn(UPRN);
		when(mockWorksheetContext.getBinTypes()).thenReturn(BIN_TYPES);
		when(mockWorksheetContext.getBinSizes()).thenReturn(BIN_SIZES);
		when(mockWorksheetContext.getFirstName()).thenReturn(FIRST_NAME);
		when(mockWorksheetContext.getLastName()).thenReturn(LAST_NAME);
		when(mockWorksheetContext.getQuantities()).thenReturn(QUANTITIES);
		when(mockWorksheetContext.getWorksheetMessage()).thenReturn(WORKSHEET_MESSAGE);
		when(mockWorksheetContext.getAdHocRoundInstanceId()).thenReturn(SLOT_ID);
	}
}