package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONFactory;
import com.placecube.digitalplace.local.waste.whitespace.axis2.WSAPIServiceStub;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.WorksheetFilter;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.WorksheetSortOrder;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.ArrayOfInputCreateWorksheetInputServiceItemInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.ArrayOfInputCreateWorksheetInputServicePropertyInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.ArrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.*;
import com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfstring;
import com.placecube.digitalplace.local.waste.whitespace.model.ServiceItemInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.ServicePropertyInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.SiteCollectionExtraDetailInputExt;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WhitespaceRequestServiceTest extends PowerMockito {

	@InjectMocks
	private WhitespaceRequestService whiteSpaceRequestService;

	private static final ArrayOfstring ARRAY_OF_STRING_1 = new ArrayOfstring();
	private static final ArrayOfstring ARRAY_OF_STRING_2 = new ArrayOfstring();
	private static final ArrayOfstring ARRAY_OF_STRING_3 = new ArrayOfstring();
	private static final long COMPANY_ID = 89764l;
	private static final String PARAM1 = "PARAM1";
	private static final String PARAM10 = "PARAM10";
	private static final String PARAM11 = "PARAM11";
	private static final String PARAM12 = "PARAM12";
	private static final String PARAM13 = "PARAM13";
	private static final String PARAM14 = "PARAM14";
	private static final String PARAM15 = "PARAM15";
	private static final String PARAM16 = "PARAM16";
	private static final String PARAM17 = "PARAM17";
	private static final String PARAM18 = "PARAM18";
	private static final String PARAM19 = "PARAM19";
	private static final String PARAM2 = "PARAM2";
	private static final String PARAM20 = "PARAM20";
	private static final String PARAM21 = "PARAM21";
	private static final String PARAM3 = "PARAM3";
	private static final String PARAM4 = "PARAM4";
	private static final String PARAM5 = "PARAM5";
	private static final String PARAM6 = "PARAM6";
	private static final String PARAM7 = "PARAM7";
	private static final String PARAM8 = "PARAM8";
	private static final String PARAM9 = "PARAM9";

	@Mock
	private ObjectFactoryService mockObjectFactoryService;

	@Mock
	private WhitespaceAxisService mockWhitespaceAxisService;

	@Mock
	private WSAPIServiceStub mockWSAPIServiceStub;

	@Mock
	private JSONFactory mockJSONFactory;

	@Before
	public void activate() throws Exception {
		when(mockWhitespaceAxisService.getWhitespaceAxisService(COMPANY_ID)).thenReturn(mockWSAPIServiceStub);
	}

	@Test
	public void addSiteAttachment_WhenNoError_ThenReturnAttachmentResponse() throws Exception {
		AddSiteAttachmentResponse mockResponse = mock(AddSiteAttachmentResponse.class);
		AddSiteAttachment mockAddSiteAttachment = mock(AddSiteAttachment.class);
		InputAddSiteAttachmentBase64Input mockInput = mock(InputAddSiteAttachmentBase64Input.class);
		when(mockObjectFactoryService.getAddSiteAttachment()).thenReturn(mockAddSiteAttachment);
		when(mockObjectFactoryService.getInputAddSiteAttachmentBase64Input()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.addSiteAttachment(mockAddSiteAttachment)).thenReturn(mockResponse);

		AddSiteAttachmentResponse response = whiteSpaceRequestService.addSiteAttachment(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setSiteId(PARAM2);
		verify(mockInput, times(1)).setFileName(PARAM3);
		verify(mockInput, times(1)).setFileExtension(PARAM4);
		verify(mockInput, times(1)).setBase64String(PARAM5);
		verify(mockAddSiteAttachment, times(1)).setAddSiteAttachmentInput(mockInput);
		assertEquals(mockResponse, response);

	}

	@Test
	public void addSiteContact_WhenNoError_ThenReturnAddSiteContactResponse() throws Exception {
		AddSiteContactResponse mockResponse = mock(AddSiteContactResponse.class);
		AddSiteContact mockAddSiteContact = mock(AddSiteContact.class);
		InputAddSiteContactInput mockInput = mock(InputAddSiteContactInput.class);
		when(mockObjectFactoryService.getAddSiteContact()).thenReturn(mockAddSiteContact);
		when(mockObjectFactoryService.getInputAddSiteContactInput()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.addSiteContact(mockAddSiteContact)).thenReturn(mockResponse);

		AddSiteContactResponse response = whiteSpaceRequestService.addSiteContact(COMPANY_ID, PARAM1, PARAM2, true, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7);
		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setSiteId(PARAM2);
		verify(mockInput, times(1)).setIsMainContact(true);
		verify(mockInput, times(1)).setContactName(PARAM3);
		verify(mockInput, times(1)).setContactEmail(PARAM4);
		verify(mockInput, times(1)).setContactTelephoneNumber(PARAM5);
		verify(mockInput, times(1)).setContactMobileNumber(PARAM6);
		verify(mockInput, times(1)).setContactFaxNumber(PARAM7);
		verify(mockAddSiteContact, times(1)).setAddSiteContactInput(mockInput);
		assertEquals(mockResponse, response);
	}

	@Test
	public void addSiteLog_WhenNoError_ThenReturnLogResponse() throws Exception {
		AddSiteLog mockAddSiteLog = mock(AddSiteLog.class);
		InputAddLogInput mockInput = mock(InputAddLogInput.class);
		AddSiteLogResponse mockResponse = mock(AddSiteLogResponse.class);
		when(mockObjectFactoryService.getAddSiteLog()).thenReturn(mockAddSiteLog);
		when(mockObjectFactoryService.getInputAddLogInput()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.addSiteLog(mockAddSiteLog)).thenReturn(mockResponse);

		AddSiteLogResponse response = whiteSpaceRequestService.addSiteLog(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setLogText(PARAM2);
		verify(mockInput, times(1)).setAccountSiteId(PARAM3);
		verify(mockAddSiteLog, times(1)).setAddLogInput(mockInput);
		assertEquals(mockResponse, response);
	}

	@Test
	public void addSiteServiceItem_WhenNoError_ThenReturnAddSiteServiceItemResponse() throws Exception {
		InputAddSiteServiceItemInput mockInput = mock(InputAddSiteServiceItemInput.class);
		AddSiteServiceItemResponse mockResponse = mock(AddSiteServiceItemResponse.class);
		AddSiteServiceItem mockInputAddSiteServiceItemInput = mock(AddSiteServiceItem.class);
		GetSiteCollectionExtraDetails mockInput2 = mock(GetSiteCollectionExtraDetails.class);
		GetSiteCollectionExtraDetailsResponse mockResponse2 = mock(GetSiteCollectionExtraDetailsResponse.class);
		SiteServiceSiteServiceItemServiceItemPropertyResponse mockSiteServiceSiteServiceItemServiceItemPropertyResponse = mock(SiteServiceSiteServiceItemServiceItemPropertyResponse.class);
		ArrayOfSiteServiceSiteServiceItemServiceItemProperty mockArrayOfSiteServiceSiteServiceItemServiceItemProperty = mock(ArrayOfSiteServiceSiteServiceItemServiceItemProperty.class);
		InputGetSiteServiceSiteServiceItemServiceItemPropertyInput mockInputGetSiteServiceSiteServiceItemServiceItemPropertyInput = new InputGetSiteServiceSiteServiceItemServiceItemPropertyInput();

		when(mockObjectFactoryService.getInputAddSiteServiceItemInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getAddSiteServiceItem()).thenReturn(mockInputAddSiteServiceItemInput);
		when(mockObjectFactoryService.getInputGetSiteServiceSiteServiceItemServiceItemPropertyInput()).thenReturn(mockInputGetSiteServiceSiteServiceItemServiceItemPropertyInput);
		when(mockWSAPIServiceStub.addSiteServiceItem(mockInputAddSiteServiceItemInput)).thenReturn(mockResponse);
		when(mockObjectFactoryService.getGetSiteCollectionExtraDetails()).thenReturn(mockInput2);
		when(mockWSAPIServiceStub.getSiteCollectionExtraDetails(mockInput2)).thenReturn(mockResponse2);
		when(mockResponse2.getGetSiteCollectionExtraDetailsResult()).thenReturn(mockSiteServiceSiteServiceItemServiceItemPropertyResponse);
		when(mockSiteServiceSiteServiceItemServiceItemPropertyResponse.getSiteServiceSiteServiceItemServiceItemProperties()).thenReturn(mockArrayOfSiteServiceSiteServiceItemServiceItemProperty);

		AddSiteServiceItemResponse response = whiteSpaceRequestService.addSiteServiceItem(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setSiteId(PARAM2);
		verify(mockInput, times(1)).setServiceId(PARAM3);
		verify(mockInput, times(1)).setServiceItemId(PARAM4);
		verify(mockInput, times(1)).setServiceItemQuantity(PARAM5);
		verify(mockInput, times(1)).setServiceItemValidFrom(PARAM6);
		verify(mockInput, times(1)).setServiceItemValidTo(PARAM7);
		verify(mockInputAddSiteServiceItemInput, times(1)).setAddSiteServiceItemInput(mockInput);
		assertEquals(mockResponse, response);
	}

	@Test
	public void addSiteServiceItemRoundSchedule_WhenNoError_ThenReturnWSResponse() throws Exception {
		AddSiteServiceItemRoundScheduleResponse mockResponse = mock(AddSiteServiceItemRoundScheduleResponse.class);
		AddSiteServiceItemRoundSchedule mockAddSiteServiceItemRoundSchedule = mock(AddSiteServiceItemRoundSchedule.class);
		InputAddSiteServiceItemRoundScheduleInput mockInput = mock(InputAddSiteServiceItemRoundScheduleInput.class);
		when(mockObjectFactoryService.getAddSiteServiceItemRoundSchedule()).thenReturn(mockAddSiteServiceItemRoundSchedule);
		when(mockObjectFactoryService.getInputAddSiteServiceItemRoundScheduleInput()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.addSiteServiceItemRoundSchedule(mockAddSiteServiceItemRoundSchedule)).thenReturn(mockResponse);

		AddSiteServiceItemRoundScheduleResponse response = whiteSpaceRequestService.addSiteServiceItemRoundSchedule(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteServiceId(PARAM1);
		verify(mockInput, times(1)).setRoundRoundAreaServiceScheduleId(PARAM2);
		verify(mockAddSiteServiceItemRoundSchedule, times(1)).setAddSiteServiceItemRoundScheduleInput(mockInput);
		assertEquals(mockResponse, response);
	}

	@Test
	public void addSiteServiceNotification_WhenNoError_ThenReturnWSResponse() throws Exception {
		AddSiteServiceNotificationResponse mockResponse = mock(AddSiteServiceNotificationResponse.class);
		AddSiteServiceNotification mockAddSiteServiceNotification = mock(AddSiteServiceNotification.class);
		InputAddSiteServiceNotificationInput mockInput = mock(InputAddSiteServiceNotificationInput.class);
		when(mockObjectFactoryService.getAddSiteServiceNotification()).thenReturn(mockAddSiteServiceNotification);
		when(mockObjectFactoryService.getInputAddSiteServiceNotificationInput()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.addSiteServiceNotification(mockAddSiteServiceNotification)).thenReturn(mockResponse);

		AddSiteServiceNotificationResponse response = whiteSpaceRequestService.addSiteServiceNotification(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setSiteId(PARAM2);
		verify(mockInput, times(1)).setNotificationId(PARAM3);
		verify(mockInput, times(1)).setServiceId(PARAM4);
		verify(mockInput, times(1)).setSiteServiceNotificationNotes(PARAM5);
		verify(mockInput, times(1)).setSiteServiceNotificationValidFrom(PARAM6);
		verify(mockInput, times(1)).setSiteServiceNotificationValidTo(PARAM7);
		verify(mockAddSiteServiceNotification, times(1)).setAddSiteServiceNotificationInput(mockInput);
		assertEquals(mockResponse, response);
	}

	@Test
	public void addWorksheetAttachment_WhenNoError_ThenReturnAttachmentResponse() throws Exception {
		AddWorksheetAttachmentResponse mockResponse = mock(AddWorksheetAttachmentResponse.class);
		AddWorksheetAttachment mockAddWorksheetAttachment = mock(AddWorksheetAttachment.class);
		InputAddWorksheetAttachmentBase64Input mockInput = mock(InputAddWorksheetAttachmentBase64Input.class);
		when(mockObjectFactoryService.getAddWorksheetAttachment()).thenReturn(mockAddWorksheetAttachment);
		when(mockObjectFactoryService.getInputAddWorksheetAttachmentBase64Input()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.addWorksheetAttachment(mockAddWorksheetAttachment)).thenReturn(mockResponse);

		AddWorksheetAttachmentResponse response = whiteSpaceRequestService.addWorksheetAttachment(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setFileName(PARAM2);
		verify(mockInput, times(1)).setFileExtension(PARAM3);
		verify(mockInput, times(1)).setBase64String(PARAM4);
		verify(mockInput, times(1)).setWorksheetRef(PARAM5);
		verify(mockAddWorksheetAttachment, times(1)).setAddWorksheetAttachmentInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void addWorksheetNotes_WhenNoError_ThenReturnWSResponse() throws Exception {
		AddWorksheetNotesResponse mockResponse = mock(AddWorksheetNotesResponse.class);
		AddWorksheetNotes mockAddWorksheetNotes = mock(AddWorksheetNotes.class);
		InputAddWorksheetNotesInput mockInput = mock(InputAddWorksheetNotesInput.class);
		when(mockObjectFactoryService.getAddWorksheetNotes()).thenReturn(mockAddWorksheetNotes);
		when(mockObjectFactoryService.getInputAddWorksheetNotesInput()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.addWorksheetNotes(mockAddWorksheetNotes)).thenReturn(mockResponse);

		AddWorksheetNotesResponse response = whiteSpaceRequestService.addWorksheetNotes(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockInput, times(1)).setNoteText(PARAM3);
		verify(mockAddWorksheetNotes, times(1)).setAddWorksheetNotesInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void cancelWorksheet_WhenNoError_ThenReturnWSResponse() throws Exception {
		CancelWorksheetResponse mockResponse = mock(CancelWorksheetResponse.class);
		CancelWorksheet mockCancelWorksheet = mock(CancelWorksheet.class);
		InputWorksheetInput mockInput = mock(InputWorksheetInput.class);
		when(mockObjectFactoryService.getCancelWorksheet()).thenReturn(mockCancelWorksheet);
		when(mockObjectFactoryService.getInputWorksheetInput()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.cancelWorksheet(mockCancelWorksheet)).thenReturn(mockResponse);

		CancelWorksheetResponse response = whiteSpaceRequestService.cancelWorksheet(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockCancelWorksheet, times(1)).setCancelWorksheetInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void createWorksheet_WhenNoError_ThenReturnCreateWorksheetResponse() throws Exception {
		CreateWorksheetResponse mockResponse = mock(CreateWorksheetResponse.class);
		CreateWorksheet mockCreateWorksheet = mock(CreateWorksheet.class);
		InputCreateWorksheetInput mockInput = mock(InputCreateWorksheetInput.class);
		ServiceItemInputExt mockServiceItemInputExt = mock(ServiceItemInputExt.class);
		ServicePropertyInputExt mockServicePropertyInputExt = mock(ServicePropertyInputExt.class);
		ArrayOfInputCreateWorksheetInputServicePropertyInput arrayOfInputCreateWorksheetInputServicePropertyInput = new ArrayOfInputCreateWorksheetInputServicePropertyInput();
		ArrayOfInputCreateWorksheetInputServiceItemInput arrayOfInputCreateWorksheetInputServiceItemInput = new ArrayOfInputCreateWorksheetInputServiceItemInput();

		when(mockObjectFactoryService.getCreateWorksheet()).thenReturn(mockCreateWorksheet);
		when(mockObjectFactoryService.getInputCreateWorksheetInput()).thenReturn(mockInput);
		when(mockWSAPIServiceStub.createWorksheet(mockCreateWorksheet)).thenReturn(mockResponse);
		when(mockServiceItemInputExt.getServiceItemInputs()).thenReturn(arrayOfInputCreateWorksheetInputServiceItemInput);
		when(mockServicePropertyInputExt.getServicePropertyInputs()).thenReturn(arrayOfInputCreateWorksheetInputServicePropertyInput);

		CreateWorksheetResponse response = whiteSpaceRequestService.createWorksheet(COMPANY_ID, mockServicePropertyInputExt, mockServiceItemInputExt, PARAM1, PARAM2, PARAM3, PARAM4, 1, PARAM5, PARAM6,
				PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12, PARAM13, PARAM14, PARAM15, PARAM16);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setUsrn(PARAM2);
		verify(mockInput, times(1)).setServiceId(PARAM3);
		verify(mockInput, times(1)).setAccountSiteId(PARAM4);
		verify(mockInput, times(1)).setRoleId(1);
		verify(mockInput, times(1)).setRoleName(PARAM5);
		verify(mockInput, times(1)).setAdHocRoundInstanceId(PARAM6);
		verify(mockInput, times(1)).setWorkLocationAddress(PARAM7);
		verify(mockInput, times(1)).setWorkLocationEasting(PARAM8);
		verify(mockInput, times(1)).setWorkLocationLatitude(PARAM9);
		verify(mockInput, times(1)).setWorkLocationLongitude(PARAM10);
		verify(mockInput, times(1)).setWorkLocationName(PARAM11);
		verify(mockInput, times(1)).setWorkLocationNorthing(PARAM12);
		verify(mockInput, times(1)).setWorksheetMessage(PARAM13);
		verify(mockInput, times(1)).setWorksheetReference(PARAM14);
		verify(mockInput, times(1)).setWorksheetDueDate(PARAM15);
		verify(mockInput, times(1)).setWorksheetStartDate(PARAM16);
		verify(mockInput, times(1)).setServicePropertyInputs(arrayOfInputCreateWorksheetInputServicePropertyInput);
		verify(mockInput, times(1)).setServiceItemInputs(arrayOfInputCreateWorksheetInputServiceItemInput);
		verify(mockCreateWorksheet, times(1)).setWorksheetInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void deleteSiteContact_WhenNoError_ThenReturnWSResponse() throws Exception {
		DeleteSiteContactResponse mockResponse = mock(DeleteSiteContactResponse.class);
		InputDeleteSiteContactInput mockInput = mock(InputDeleteSiteContactInput.class);
		DeleteSiteContact mockDeleteSiteContact = mock(DeleteSiteContact.class);
		when(mockObjectFactoryService.getInputDeleteSiteContactInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getDeleteSiteContact()).thenReturn(mockDeleteSiteContact);
		when(mockWSAPIServiceStub.deleteSiteContact(mockDeleteSiteContact)).thenReturn(mockResponse);

		DeleteSiteContactResponse response = whiteSpaceRequestService.deleteSiteContact(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setContactId(PARAM1);
		verify(mockDeleteSiteContact, times(1)).setDeleteSiteContactInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void deleteSiteServiceItem_WhenNoError_ThenReturnWSResponse() throws Exception {
		DeleteSiteServiceItem mockDeleteSiteServiceItem = mock(DeleteSiteServiceItem.class);
		DeleteSiteServiceItemResponse mockResponse = mock(DeleteSiteServiceItemResponse.class);
		InputDeleteSiteServiceItemInput mockInput = mock(InputDeleteSiteServiceItemInput.class);
		when(mockObjectFactoryService.getInputDeleteSiteServiceItemInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getDeleteSiteServiceItem()).thenReturn(mockDeleteSiteServiceItem);
		when(mockWSAPIServiceStub.deleteSiteServiceItem(mockDeleteSiteServiceItem)).thenReturn(mockResponse);

		DeleteSiteServiceItemResponse response = whiteSpaceRequestService.deleteSiteServiceItem(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setSiteServiceId(PARAM1);
		verify(mockDeleteSiteServiceItem, times(1)).setDeleteSiteServiceItemInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void deleteSiteServiceItemRoundSchedule_WhenNoError_ThenReturnWSResponse() throws Exception {
		DeleteSiteServiceItemRoundScheduleResponse mockResponse = mock(DeleteSiteServiceItemRoundScheduleResponse.class);
		InputDeleteSiteServiceItemRoundScheduleInput mockInput = mock(InputDeleteSiteServiceItemRoundScheduleInput.class);
		DeleteSiteServiceItemRoundSchedule mockDeleteSiteServiceItemRoundSchedule = mock(DeleteSiteServiceItemRoundSchedule.class);
		when(mockObjectFactoryService.getInputDeleteSiteServiceItemRoundScheduleInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getDeleteSiteServiceItemRoundSchedule()).thenReturn(mockDeleteSiteServiceItemRoundSchedule);
		when(mockWSAPIServiceStub.deleteSiteServiceItemRoundSchedule(mockDeleteSiteServiceItemRoundSchedule)).thenReturn(mockResponse);

		DeleteSiteServiceItemRoundScheduleResponse response = whiteSpaceRequestService.deleteSiteServiceItemRoundSchedule(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteServiceId(PARAM1);
		verify(mockInput, times(1)).setRoundRoundAreaServiceScheduleId(PARAM2);
		verify(mockDeleteSiteServiceItemRoundSchedule, times(1)).setDeleteSiteServiceItemRoundScheduleInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getAccountSiteId_WhenNoError_ThenReturnSiteIDResponse() throws Exception {
		GetAccountSiteIdResponse mockResponse = mock(GetAccountSiteIdResponse.class);
		InputGetSiteInput mockInput = mock(InputGetSiteInput.class);
		GetAccountSiteId mockGetAccountSiteId = mock(GetAccountSiteId.class);
		when(mockObjectFactoryService.getInputGetSiteInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetAccountSiteId()).thenReturn(mockGetAccountSiteId);
		when(mockWSAPIServiceStub.getAccountSiteId(mockGetAccountSiteId)).thenReturn(mockResponse);

		GetAccountSiteIdResponse response = whiteSpaceRequestService.getAccountSiteId(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockGetAccountSiteId, times(1)).setSiteInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getActiveAddresses_WhenNoError_ThenReturnAddressResponse() throws Exception {
		GetActiveAddressesResponse mockResponse = mock(GetActiveAddressesResponse.class);
		GetAddressInput mockInput = mock(GetAddressInput.class);
		GetActiveAddresses mockGetActiveAddresses = mock(GetActiveAddresses.class);
		when(mockObjectFactoryService.getGetAddressInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetActiveAddresses()).thenReturn(mockGetActiveAddresses);
		when(mockWSAPIServiceStub.getActiveAddresses(mockGetActiveAddresses)).thenReturn(mockResponse);

		GetActiveAddressesResponse response = whiteSpaceRequestService.getActiveAddresses(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7);

		verify(mockInput, times(1)).setAddressLine1(PARAM1);
		verify(mockInput, times(1)).setAddressLine2(PARAM2);
		verify(mockInput, times(1)).setAddressNameNumber(PARAM3);
		verify(mockInput, times(1)).setCounty(PARAM4);
		verify(mockInput, times(1)).setCountry(PARAM5);
		verify(mockInput, times(1)).setPostcode(PARAM6);
		verify(mockInput, times(1)).setTown(PARAM7);
		verify(mockGetActiveAddresses, times(1)).setGetActiveAddressInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getAddresses_WhenNoError_ThenReturnAddressResponse() throws Exception {
		GetAddressesResponse mockResponse = mock(GetAddressesResponse.class);
		GetAddressInput mockInput = mock(GetAddressInput.class);
		GetAddresses mockGetAddresses = mock(GetAddresses.class);
		when(mockObjectFactoryService.getGetAddressInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetAddresses()).thenReturn(mockGetAddresses);
		when(mockWSAPIServiceStub.getAddresses(mockGetAddresses)).thenReturn(mockResponse);

		GetAddressesResponse response = whiteSpaceRequestService.getAddresses(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7);

		verify(mockInput, times(1)).setAddressLine1(PARAM1);
		verify(mockInput, times(1)).setAddressLine2(PARAM2);
		verify(mockInput, times(1)).setAddressNameNumber(PARAM3);
		verify(mockInput, times(1)).setCounty(PARAM4);
		verify(mockInput, times(1)).setCountry(PARAM5);
		verify(mockInput, times(1)).setPostcode(PARAM6);
		verify(mockInput, times(1)).setTown(PARAM7);
		verify(mockGetAddresses, times(1)).setGetAddressInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getAddressesByCoordinatesRadius_WhenNoError_ThenReturnAddressResponse() throws Exception {
		GetAddressesByCoordinatesRadiusResponse mockResponse = mock(GetAddressesByCoordinatesRadiusResponse.class);
		GetAddressesByCoordinatesRadiusInput mockInput = mock(GetAddressesByCoordinatesRadiusInput.class);
		GetAddressesByCoordinatesRadius mockGetAddressesByCoordinatesRadius = mock(GetAddressesByCoordinatesRadius.class);
		when(mockObjectFactoryService.getGetAddressesByCoordinatesRadiusInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetAddressesByCoordinatesRadius()).thenReturn(mockGetAddressesByCoordinatesRadius);
		when(mockWSAPIServiceStub.getAddressesByCoordinatesRadius(mockGetAddressesByCoordinatesRadius)).thenReturn(mockResponse);

		GetAddressesByCoordinatesRadiusResponse response = whiteSpaceRequestService.getAddressesByCoordinatesRadius(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setLatitude(PARAM1);
		verify(mockInput, times(1)).setLongitude(PARAM2);
		verify(mockInput, times(1)).setRadiusMetres(PARAM3);
		verify(mockGetAddressesByCoordinatesRadius, times(1)).setGetAddressesByCoordinatesRadiusInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getCollectionByUprnAndDate_WhenCollectionDateIsFormatted_ThenReturnCollectionResponse() throws Exception {
		String nextCollectionFromDate = "11/02/2023";
		GetCollectionByUprnAndDateResponse mockResponse = mock(GetCollectionByUprnAndDateResponse.class);
		GetCollectionByUprnAndDateInput mockInput = mock(GetCollectionByUprnAndDateInput.class);
		GetCollectionByUprnAndDate mockGetCollectionByUprnAndDate = mock(GetCollectionByUprnAndDate.class);
		when(mockObjectFactoryService.getGetCollectionByUprnAndDateInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetCollectionByUprnAndDate()).thenReturn(mockGetCollectionByUprnAndDate);
		when(mockWSAPIServiceStub.getCollectionByUprnAndDate(mockGetCollectionByUprnAndDate)).thenReturn(mockResponse);

		GetCollectionByUprnAndDateResponse response = whiteSpaceRequestService.getCollectionByUprnAndDate(COMPANY_ID, PARAM1, nextCollectionFromDate);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setNextCollectionFromDate(nextCollectionFromDate);
		verify(mockGetCollectionByUprnAndDate, times(1)).setGetCollectionByUprnAndDateInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getCollectionByUprnAndDate_WhenErrorParsingCollectionDate_ThenNoExceptionIsThrown() throws Exception {
		String nextCollectionFromDate = "11-02-2023 00:55";
		GetCollectionByUprnAndDateResponse mockResponse = mock(GetCollectionByUprnAndDateResponse.class);
		GetCollectionByUprnAndDateInput mockInput = mock(GetCollectionByUprnAndDateInput.class);
		GetCollectionByUprnAndDate mockGetCollectionByUprnAndDate = mock(GetCollectionByUprnAndDate.class);
		when(mockObjectFactoryService.getGetCollectionByUprnAndDateInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetCollectionByUprnAndDate()).thenReturn(mockGetCollectionByUprnAndDate);
		when(mockWSAPIServiceStub.getCollectionByUprnAndDate(mockGetCollectionByUprnAndDate)).thenReturn(mockResponse);
		GetCollectionByUprnAndDateResponse response = null;
		try {
			response = whiteSpaceRequestService.getCollectionByUprnAndDate(COMPANY_ID, PARAM1, nextCollectionFromDate);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setNextCollectionFromDate(nextCollectionFromDate);
		verify(mockGetCollectionByUprnAndDate, times(1)).setGetCollectionByUprnAndDateInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getCollectionSlots_WhenNoError_ThenReturnAdHocRoundInstanceResponse() throws Exception {
		GetCollectionSlotsResponse mockResponse = mock(GetCollectionSlotsResponse.class);
		InputGetCollectionSlotsInput mockInput = mock(InputGetCollectionSlotsInput.class);
		GetCollectionSlots mockGetCollectionSlots = mock(GetCollectionSlots.class);
		when(mockObjectFactoryService.getInputGetCollectionSlotsInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetCollectionSlots()).thenReturn(mockGetCollectionSlots);
		when(mockWSAPIServiceStub.getCollectionSlots(mockGetCollectionSlots)).thenReturn(mockResponse);

		GetCollectionSlotsResponse response = whiteSpaceRequestService.getCollectionSlots(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setServiceId(PARAM2);
		verify(mockInput, times(1)).setSiteId(PARAM3);
		verify(mockInput, times(1)).setListCount(PARAM4);
		verify(mockInput, times(1)).setNextCollectionFromDate(PARAM5);
		verify(mockInput, times(1)).setNextCollectionToDate(PARAM6);
		verify(mockGetCollectionSlots, times(1)).setCollectionSlotsInputInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getFullSiteCollections_WhenNoError_ThenReturnFullSiteServiceResponse() throws Exception {
		GetFullSiteCollectionsResponse mockResponse = mock(GetFullSiteCollectionsResponse.class);
		InputGetSiteServiceInput mockInput = mock(InputGetSiteServiceInput.class);
		GetFullSiteCollections mockGetFullSiteCollections = mock(GetFullSiteCollections.class);
		when(mockObjectFactoryService.getInputGetSiteServiceInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetFullSiteCollections()).thenReturn(mockGetFullSiteCollections);
		when(mockWSAPIServiceStub.getFullSiteCollections(mockGetFullSiteCollections)).thenReturn(mockResponse);

		GetFullSiteCollectionsResponse response = whiteSpaceRequestService.getFullSiteCollections(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setSiteId(PARAM2);
		verify(mockGetFullSiteCollections, times(1)).setSiteserviceInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getFullWorksheetDetails_WhenNoError_ThenReturnFullWorksheetDetailResponse() throws Exception {
		GetFullWorksheetDetailsResponse mockResponse = mock(GetFullWorksheetDetailsResponse.class);
		InputWorksheetInput mockInput = mock(InputWorksheetInput.class);
		GetFullWorksheetDetails mockGetFullWorksheetDetails = mock(GetFullWorksheetDetails.class);
		when(mockObjectFactoryService.getInputWorksheetInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetFullWorksheetDetails()).thenReturn(mockGetFullWorksheetDetails);
		when(mockWSAPIServiceStub.getFullWorksheetDetails(mockGetFullWorksheetDetails)).thenReturn(mockResponse);

		GetFullWorksheetDetailsResponse response = whiteSpaceRequestService.getFullWorksheetDetails(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockGetFullWorksheetDetails, times(1)).setFullworksheetDetailsInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getInCabLogs_WhenNoError_ThenReturnInCabLogResponse() throws Exception {
		GetInCabLogsResponse mockResponse = mock(GetInCabLogsResponse.class);
		InputGetInCabLogsInput mockInput = mock(InputGetInCabLogsInput.class);
		GetInCabLogs mockGetInCabLogs = mock(GetInCabLogs.class);
		when(mockObjectFactoryService.getInputGetInCabLogsInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetInCabLogs()).thenReturn(mockGetInCabLogs);
		when(mockWSAPIServiceStub.getInCabLogs(mockGetInCabLogs)).thenReturn(mockResponse);

		GetInCabLogsResponse response = whiteSpaceRequestService.getInCabLogs(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, ARRAY_OF_STRING_1, ARRAY_OF_STRING_2, ARRAY_OF_STRING_3);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setUsrn(PARAM2);
		verify(mockInput, times(1)).setLogFromDate(PARAM3);
		verify(mockInput, times(1)).setLogToDate(PARAM4);
		verify(mockInput, times(1)).setLogTypeID(ARRAY_OF_STRING_1);
		verify(mockInput, times(1)).setReason(ARRAY_OF_STRING_2);
		verify(mockInput, times(1)).setWorksheetType(ARRAY_OF_STRING_3);
		verify(mockGetInCabLogs, times(1)).setInCabLogInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getLogsSearch_WhenNoError_ThenReturnLogResponse() throws Exception {
		GetLogsSearchResponse mockResponse = mock(GetLogsSearchResponse.class);
		InputGetLogSearchInput mockInput = mock(InputGetLogSearchInput.class);
		GetLogsSearch mockGetLogsSearch = mock(GetLogsSearch.class);
		when(mockObjectFactoryService.getInputGetLogSearchInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetLogsSearch()).thenReturn(mockGetLogsSearch);
		when(mockWSAPIServiceStub.getLogsSearch(mockGetLogsSearch)).thenReturn(mockResponse);

		GetLogsSearchResponse response = whiteSpaceRequestService.getLogsSearch(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, ARRAY_OF_STRING_1);

		verify(mockInput, times(1)).setAccountSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockInput, times(1)).setLogFromDate(PARAM3);
		verify(mockInput, times(1)).setLogToDate(PARAM4);
		verify(mockInput, times(1)).setLogMessageSearchString(PARAM5);
		verify(mockInput, times(1)).setLogReferenceSearchString(PARAM6);
		verify(mockInput, times(1)).setLogSubjectSearchString(PARAM7);
		verify(mockInput, times(1)).setLogTypeID(ARRAY_OF_STRING_1);
		verify(mockGetLogsSearch, times(1)).setLogInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getNotifications_WhenNoError_ThenReturnNotificationResponse() throws Exception {
		GetNotificationsResponse mockResponse = mock(GetNotificationsResponse.class);
		InputGetNotificationInput mockInput = mock(InputGetNotificationInput.class);
		GetNotifications mockGetNotifications = mock(GetNotifications.class);
		when(mockObjectFactoryService.getInputGetNotificationInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetNotifications()).thenReturn(mockGetNotifications);
		when(mockWSAPIServiceStub.getNotifications(mockGetNotifications)).thenReturn(mockResponse);

		GetNotificationsResponse response = whiteSpaceRequestService.getNotifications(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setNotificationId(PARAM1);
		verify(mockGetNotifications, times(1)).setNotificationInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getServiceItems_WhenNoError_ThenReturnServiceItemResponse() throws Exception {
		GetServiceItemsResponse mockResponse = mock(GetServiceItemsResponse.class);
		InputGetServiceItemInput mockInput = mock(InputGetServiceItemInput.class);
		GetServiceItems mockGetServiceItems = mock(GetServiceItems.class);
		when(mockObjectFactoryService.getInputGetServiceItemInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetServiceItems()).thenReturn(mockGetServiceItems);
		when(mockWSAPIServiceStub.getServiceItems(mockGetServiceItems)).thenReturn(mockResponse);

		GetServiceItemsResponse response = whiteSpaceRequestService.getServiceItems(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setServiceId(PARAM1);
		verify(mockInput, times(1)).setServiceItemId(PARAM2);
		verify(mockGetServiceItems, times(1)).setServiceItemInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getServices_WhenNoError_ThenReturnServiceResponse() throws Exception {
		GetServicesResponse mockResponse = mock(GetServicesResponse.class);
		InputGetServiceInput mockInput = mock(InputGetServiceInput.class);
		GetServices mockGetServices = mock(GetServices.class);
		when(mockObjectFactoryService.getInputGetServiceInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetServices()).thenReturn(mockGetServices);
		when(mockWSAPIServiceStub.getServices(mockGetServices)).thenReturn(mockResponse);

		GetServicesResponse response = whiteSpaceRequestService.getServices(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setServiceId(PARAM1);
		verify(mockGetServices, times(1)).setServiceInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteAttachments_WhenNoError_ThenReturnAttachmentFileResponse() throws Exception {
		GetSiteAttachmentsResponse mockResponse = mock(GetSiteAttachmentsResponse.class);
		InputGetSiteAttachmentInput mockInput = mock(InputGetSiteAttachmentInput.class);
		GetSiteAttachments mockGetSiteAttachments = mock(GetSiteAttachments.class);
		when(mockObjectFactoryService.getInputGetSiteAttachmentInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteAttachments()).thenReturn(mockGetSiteAttachments);
		when(mockWSAPIServiceStub.getSiteAttachments(mockGetSiteAttachments)).thenReturn(mockResponse);

		GetSiteAttachmentsResponse response = whiteSpaceRequestService.getSiteAttachments(COMPANY_ID, PARAM1, PARAM2, true);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockInput, times(1)).setIncludeData(true);
		verify(mockGetSiteAttachments, times(1)).setGetSiteAttachmentsInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteAvailableRounds_WhenNoError_ThenReturnServiceScheduleResponse() throws Exception {
		GetSiteAvailableRoundsResponse mockResponse = mock(GetSiteAvailableRoundsResponse.class);
		InputGetServiceScheduleInput mockInput = mock(InputGetServiceScheduleInput.class);
		GetSiteAvailableRounds mockGetSiteAvailableRounds = mock(GetSiteAvailableRounds.class);
		when(mockObjectFactoryService.getInputGetServiceScheduleInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteAvailableRounds()).thenReturn(mockGetSiteAvailableRounds);
		when(mockWSAPIServiceStub.getSiteAvailableRounds(mockGetSiteAvailableRounds)).thenReturn(mockResponse);

		GetSiteAvailableRoundsResponse response = whiteSpaceRequestService.getSiteAvailableRounds(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetSiteAvailableRounds, times(1)).setServicescheduleInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteCollectionExtraDetails_WhenNoError_ThenReturnSiteServiceSiteServiceItemServiceItemPropertyResponse() throws Exception {
		GetSiteCollectionExtraDetailsResponse mockResponse = mock(GetSiteCollectionExtraDetailsResponse.class);
		InputGetSiteServiceSiteServiceItemServiceItemPropertyInput mockInput = mock(InputGetSiteServiceSiteServiceItemServiceItemPropertyInput.class);
		GetSiteCollectionExtraDetails mockGetSiteCollectionExtraDetails = mock(GetSiteCollectionExtraDetails.class);
		when(mockObjectFactoryService.getInputGetSiteServiceSiteServiceItemServiceItemPropertyInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteCollectionExtraDetails()).thenReturn(mockGetSiteCollectionExtraDetails);
		when(mockWSAPIServiceStub.getSiteCollectionExtraDetails(mockGetSiteCollectionExtraDetails)).thenReturn(mockResponse);

		GetSiteCollectionExtraDetailsResponse response = whiteSpaceRequestService.getSiteCollectionExtraDetails(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setSiteServiceId(PARAM1);
		verify(mockGetSiteCollectionExtraDetails, times(1)).setSsssipInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSiteCollections_WhenNoError_ThenReturnSiteServiceResponse(boolean includeCommercial) throws Exception {
		GetSiteCollectionsResponse mockResponse = mock(GetSiteCollectionsResponse.class);
		InputGetSiteServiceInput mockInput = mock(InputGetSiteServiceInput.class);
		GetSiteCollections mockGetSiteCollections = mock(GetSiteCollections.class);
		when(mockObjectFactoryService.getInputGetSiteServiceInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteCollections()).thenReturn(mockGetSiteCollections);
		when(mockWSAPIServiceStub.getSiteCollections(mockGetSiteCollections)).thenReturn(mockResponse);

		GetSiteCollectionsResponse response = whiteSpaceRequestService.getSiteCollections(COMPANY_ID, PARAM1, PARAM2, includeCommercial);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockInput, times(1)).setIncludeCommercial(includeCommercial);
		verify(mockGetSiteCollections, times(1)).setSiteserviceInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteContacts_WhenNoError_ThenReturnContactResponse() throws Exception {
		GetSiteContactsResponse mockResponse = mock(GetSiteContactsResponse.class);
		InputGetSiteContactInput mockInput = mock(InputGetSiteContactInput.class);
		GetSiteContacts mockGetSiteContacts = mock(GetSiteContacts.class);
		when(mockObjectFactoryService.getInputGetSiteContactInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteContacts()).thenReturn(mockGetSiteContacts);
		when(mockWSAPIServiceStub.getSiteContacts(mockGetSiteContacts)).thenReturn(mockResponse);

		GetSiteContactsResponse response = whiteSpaceRequestService.getSiteContacts(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetSiteContacts, times(1)).setSiteContactInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteContracts_WhenNoError_ThenReturnSiteContractResponse() throws Exception {
		GetSiteContractsResponse mockResponse = mock(GetSiteContractsResponse.class);
		InputGetSiteContractInput mockInput = mock(InputGetSiteContractInput.class);
		GetSiteContracts mockGetSiteContracts = mock(GetSiteContracts.class);
		when(mockObjectFactoryService.getInputGetSiteContractInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteContracts()).thenReturn(mockGetSiteContracts);
		when(mockWSAPIServiceStub.getSiteContracts(mockGetSiteContracts)).thenReturn(mockResponse);

		GetSiteContractsResponse response = whiteSpaceRequestService.getSiteContracts(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetSiteContracts, times(1)).setSitecontractInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteFlags_WhenNoError_ThenReturnSiteContractResponse() throws Exception {
		GetSiteFlagsResponse mockResponse = mock(GetSiteFlagsResponse.class);
		InputGetSiteFlagInput mockInput = mock(InputGetSiteFlagInput.class);
		GetSiteFlags mockGetSiteFlags = mock(GetSiteFlags.class);
		when(mockObjectFactoryService.getInputGetSiteFlagInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteFlags()).thenReturn(mockGetSiteFlags);
		when(mockWSAPIServiceStub.getSiteFlags(mockGetSiteFlags)).thenReturn(mockResponse);

		GetSiteFlagsResponse response = whiteSpaceRequestService.getSiteFlags(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetSiteFlags, times(1)).setSiteflagInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteId_WhenNoError_ThenReturnAccountSiteIDResponse() throws Exception {
		GetSiteIdResponse mockResponse = mock(GetSiteIdResponse.class);
		InputGetAccountSiteInput mockInput = mock(InputGetAccountSiteInput.class);
		GetSiteId mockGetSiteId = mock(GetSiteId.class);
		when(mockObjectFactoryService.getInputGetAccountSiteInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteId()).thenReturn(mockGetSiteId);
		when(mockWSAPIServiceStub.getSiteId(mockGetSiteId)).thenReturn(mockResponse);

		GetSiteIdResponse response = whiteSpaceRequestService.getSiteId(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setAccountSiteId(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockGetSiteId, times(1)).setSiteInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteIncidents_WhenNoError_ThenReturnAccountSiteIDResponse() throws Exception {
		GetSiteIncidentsResponse mockResponse = mock(GetSiteIncidentsResponse.class);
		InputGetRoundIncidentInput mockInput = mock(InputGetRoundIncidentInput.class);
		GetSiteIncidents mockGetSiteIncidents = mock(GetSiteIncidents.class);
		when(mockObjectFactoryService.getInputGetRoundIncidentInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteIncidents()).thenReturn(mockGetSiteIncidents);
		when(mockWSAPIServiceStub.getSiteIncidents(mockGetSiteIncidents)).thenReturn(mockResponse);

		GetSiteIncidentsResponse response = whiteSpaceRequestService.getSiteIncidents(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetSiteIncidents, times(1)).setRoundIncidentInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteInfo_WhenNoError_ThenReturnSiteResponse() throws Exception {
		GetSiteInfoResponse mockResponse = mock(GetSiteInfoResponse.class);
		InputGetSiteInfoInput mockInput = mock(InputGetSiteInfoInput.class);
		GetSiteInfo mockGetSiteInfo = mock(GetSiteInfo.class);
		when(mockObjectFactoryService.getInputGetSiteInfoInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteInfo()).thenReturn(mockGetSiteInfo);
		when(mockWSAPIServiceStub.getSiteInfo(mockGetSiteInfo)).thenReturn(mockResponse);

		GetSiteInfoResponse response = whiteSpaceRequestService.getSiteInfo(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setAccountSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetSiteInfo, times(1)).setSiteInfoInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteLogs_WhenNoError_ThenReturnSiteResponse() throws Exception {
		GetSiteLogsResponse mockResponse = mock(GetSiteLogsResponse.class);
		InputGetLogInput mockInput = mock(InputGetLogInput.class);
		GetSiteLogs mockGetSiteLogs = mock(GetSiteLogs.class);
		when(mockObjectFactoryService.getInputGetLogInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteLogs()).thenReturn(mockGetSiteLogs);
		when(mockWSAPIServiceStub.getSiteLogs(mockGetSiteLogs)).thenReturn(mockResponse);

		GetSiteLogsResponse response = whiteSpaceRequestService.getSiteLogs(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setAccountSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetSiteLogs, times(1)).setLogInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteNotifications_WhenNoError_ThenReturnSiteServiceNotificationResponse() throws Exception {
		GetSiteNotificationsResponse mockResponse = mock(GetSiteNotificationsResponse.class);
		InputGetSiteServiceNotificationInput mockInput = mock(InputGetSiteServiceNotificationInput.class);
		GetSiteNotifications mockGetSiteNotifications = mock(GetSiteNotifications.class);
		when(mockObjectFactoryService.getInputGetSiteServiceNotificationInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteNotifications()).thenReturn(mockGetSiteNotifications);
		when(mockWSAPIServiceStub.getSiteNotifications(mockGetSiteNotifications)).thenReturn(mockResponse);

		GetSiteNotificationsResponse response = whiteSpaceRequestService.getSiteNotifications(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetSiteNotifications, times(1)).setSiteservicenotificationInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSites_WhenNoError_ThenReturnSiteArrayResponse() throws Exception {
		GetSitesResponse mockResponse = mock(GetSitesResponse.class);
		InputGetSitesInput mockInput = mock(InputGetSitesInput.class);
		GetSites mockGetSites = mock(GetSites.class);
		when(mockObjectFactoryService.getInputGetSitesInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSites()).thenReturn(mockGetSites);
		when(mockWSAPIServiceStub.getSites(mockGetSites)).thenReturn(mockResponse);

		GetSitesResponse response = whiteSpaceRequestService.getSites(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockGetSites, times(1)).setSitesInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteServiceItemRoundSchedules_WhenNoError_ThenReturnRRASSContractRoundResponse() throws Exception {
		GetSiteServiceItemRoundSchedulesResponse mockResponse = mock(GetSiteServiceItemRoundSchedulesResponse.class);
		InputGetSiteServiceItemRoundScheduleInput mockInput = mock(InputGetSiteServiceItemRoundScheduleInput.class);
		GetSiteServiceItemRoundSchedules mockGetSiteServiceItemRoundSchedules = mock(GetSiteServiceItemRoundSchedules.class);
		when(mockObjectFactoryService.getInputGetSiteServiceItemRoundScheduleInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetSiteServiceItemRoundSchedules()).thenReturn(mockGetSiteServiceItemRoundSchedules);
		when(mockWSAPIServiceStub.getSiteServiceItemRoundSchedules(mockGetSiteServiceItemRoundSchedules)).thenReturn(mockResponse);

		GetSiteServiceItemRoundSchedulesResponse response = whiteSpaceRequestService.getSiteServiceItemRoundSchedules(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setSiteServiceId(PARAM1);
		verify(mockGetSiteServiceItemRoundSchedules, times(1)).setSiteServiceItemRoundScheduleInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getSiteWorksheets_WhenNoError_ThenReturnWorksheetResponse() throws Exception {
		GetSiteWorksheetsResponse mockResponse = mock(GetSiteWorksheetsResponse.class);
		InputGetWorksheetInput mockInput = mock(InputGetWorksheetInput.class);
		WorksheetFilter mockWorksheetFilter = mock(WorksheetFilter.class);
		WorksheetSortOrder mockWorksheetSortOrder = mock(WorksheetSortOrder.class);
		GetSiteWorksheets mockGetSiteWorksheets = mock(GetSiteWorksheets.class);
		when(mockObjectFactoryService.getInputGetWorksheetInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getWorksheetFilter(PARAM4)).thenReturn(mockWorksheetFilter);
		when(mockObjectFactoryService.getWorksheetSortOrder(PARAM5)).thenReturn(mockWorksheetSortOrder);
		when(mockObjectFactoryService.getGetSiteWorksheets()).thenReturn(mockGetSiteWorksheets);
		when(mockWSAPIServiceStub.getSiteWorksheets(mockGetSiteWorksheets)).thenReturn(mockResponse);

		GetSiteWorksheetsResponse response = whiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, PARAM1, PARAM2, PARAM3, true, PARAM4, PARAM5);

		verify(mockInput, times(1)).setUprn(PARAM1);
		verify(mockInput, times(1)).setAccountSiteId(PARAM2);
		verify(mockInput, times(1)).setWorksheetFilterString(PARAM3);
		verify(mockInput, times(1)).setOrderAscending(true);
		verify(mockInput, times(1)).setWorksheetFilter(mockWorksheetFilter);
		verify(mockInput, times(1)).setWorksheetSortOrder(mockWorksheetSortOrder);
		verify(mockGetSiteWorksheets, times(1)).setWorksheetInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getStreets_WhenNoError_ThenReturnStreetResponse() throws Exception {
		GetStreetsResponse mockResponse = mock(GetStreetsResponse.class);
		InputStreetInput mockInput = mock(InputStreetInput.class);
		GetStreets mockGetStreets = mock(GetStreets.class);
		when(mockObjectFactoryService.getInputStreetInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetStreets()).thenReturn(mockGetStreets);
		when(mockWSAPIServiceStub.getStreets(mockGetStreets)).thenReturn(mockResponse);

		GetStreetsResponse response = whiteSpaceRequestService.getStreets(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setPostcode(PARAM1);
		verify(mockInput, times(1)).setStreetName(PARAM2);
		verify(mockInput, times(1)).setTownName(PARAM3);
		verify(mockGetStreets, times(1)).setGetStreetInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWalkNumbers_WhenNoError_ThenReturnWalkNumberResponse() throws Exception {
		GetWalkNumbersResponse mockResponse = mock(GetWalkNumbersResponse.class);
		InputGetWalkNumbersInput mockInput = mock(InputGetWalkNumbersInput.class);
		GetWalkNumbers mockGetWalkNumbers = mock(GetWalkNumbers.class);
		when(mockObjectFactoryService.getInputGetWalkNumbersInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWalkNumbers()).thenReturn(mockGetWalkNumbers);
		when(mockWSAPIServiceStub.getWalkNumbers(mockGetWalkNumbers)).thenReturn(mockResponse);

		GetWalkNumbersResponse response = whiteSpaceRequestService.getWalkNumbers(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setUprn(PARAM2);
		verify(mockGetWalkNumbers, times(1)).setGetWalkNumbersInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetAttachments_WhenNoError_ThenReturnAttachmentFileResponse() throws Exception {
		GetWorksheetAttachmentsResponse mockResponse = mock(GetWorksheetAttachmentsResponse.class);
		InputGetWorksheetAttachmentInput mockInput = mock(InputGetWorksheetAttachmentInput.class);
		GetWorksheetAttachments mockGetWorksheetAttachments = mock(GetWorksheetAttachments.class);
		when(mockObjectFactoryService.getInputGetWorksheetAttachmentInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetAttachments()).thenReturn(mockGetWorksheetAttachments);
		when(mockWSAPIServiceStub.getWorksheetAttachments(mockGetWorksheetAttachments)).thenReturn(mockResponse);

		GetWorksheetAttachmentsResponse response = whiteSpaceRequestService.getWorksheetAttachments(COMPANY_ID, PARAM1, PARAM2, true);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockInput, times(1)).setIncludeData(true);
		verify(mockGetWorksheetAttachments, times(1)).setGetWorksheetAttachmentsInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetChargeMatrix_WhenNoError_ThenReturnServiceItemChargeResponse() throws Exception {
		GetWorksheetChargeMatrixResponse mockResponse = mock(GetWorksheetChargeMatrixResponse.class);
		InputServiceAndServiceItemInput mockInput = mock(InputServiceAndServiceItemInput.class);
		GetWorksheetChargeMatrix mockGetWorksheetChargeMatrix = mock(GetWorksheetChargeMatrix.class);
		when(mockObjectFactoryService.getInputServiceAndServiceItemInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetChargeMatrix()).thenReturn(mockGetWorksheetChargeMatrix);
		when(mockWSAPIServiceStub.getWorksheetChargeMatrix(mockGetWorksheetChargeMatrix)).thenReturn(mockResponse);

		GetWorksheetChargeMatrixResponse response = whiteSpaceRequestService.getWorksheetChargeMatrix(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setServiceId(PARAM1);
		verify(mockInput, times(1)).setServiceItemId(PARAM2);
		verify(mockGetWorksheetChargeMatrix, times(1)).setWorksheetChargeMatrixInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetDetailEvents_WhenNoError_ThenReturnServiceItemChargeResponse() throws Exception {
		GetWorksheetDetailEventsResponse mockResponse = mock(GetWorksheetDetailEventsResponse.class);
		InputGetWorksheetDetailEventsInput mockInput = mock(InputGetWorksheetDetailEventsInput.class);
		GetWorksheetDetailEvents mockGetWorksheetDetailEvents = mock(GetWorksheetDetailEvents.class);
		when(mockObjectFactoryService.getInputGetWorksheetDetailEventsInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetDetailEvents()).thenReturn(mockGetWorksheetDetailEvents);
		when(mockWSAPIServiceStub.getWorksheetDetailEvents(mockGetWorksheetDetailEvents)).thenReturn(mockResponse);

		GetWorksheetDetailEventsResponse response = whiteSpaceRequestService.getWorksheetDetailEvents(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockGetWorksheetDetailEvents, times(1)).setWorksheetDetailEventsInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetDetailExtraInfoFields_WhenNoError_ThenReturnWorksheetServicePropertyResponse() throws Exception {
		GetWorksheetDetailExtraInfoFieldsResponse mockResponse = mock(GetWorksheetDetailExtraInfoFieldsResponse.class);
		InputGetWorksheetDetailExtraInfoFieldsInput mockInput = mock(InputGetWorksheetDetailExtraInfoFieldsInput.class);
		GetWorksheetDetailExtraInfoFields mockGetWorksheetDetailExtraInfoFields = mock(GetWorksheetDetailExtraInfoFields.class);
		when(mockObjectFactoryService.getInputGetWorksheetDetailExtraInfoFieldsInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetDetailExtraInfoFields()).thenReturn(mockGetWorksheetDetailExtraInfoFields);
		when(mockWSAPIServiceStub.getWorksheetDetailExtraInfoFields(mockGetWorksheetDetailExtraInfoFields)).thenReturn(mockResponse);

		GetWorksheetDetailExtraInfoFieldsResponse response = whiteSpaceRequestService.getWorksheetDetailExtraInfoFields(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockGetWorksheetDetailExtraInfoFields, times(1)).setWorksheetDetailExtraInfoFieldsInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetDetailNotes_WhenNoError_ThenReturnWorksheetNoteResponse() throws Exception {
		GetWorksheetDetailNotesResponse mockResponse = mock(GetWorksheetDetailNotesResponse.class);
		InputGetWorksheetDetailNotesInput mockInput = mock(InputGetWorksheetDetailNotesInput.class);
		GetWorksheetDetailNotes mockGetWorksheetDetailNotes = mock(GetWorksheetDetailNotes.class);
		when(mockObjectFactoryService.getInputGetWorksheetDetailNotesInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetDetailNotes()).thenReturn(mockGetWorksheetDetailNotes);
		when(mockWSAPIServiceStub.getWorksheetDetailNotes(mockGetWorksheetDetailNotes)).thenReturn(mockResponse);

		GetWorksheetDetailNotesResponse response = whiteSpaceRequestService.getWorksheetDetailNotes(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockGetWorksheetDetailNotes, times(1)).setWorksheetDetailsNotesInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetDetails_WhenNoError_ThenReturnWorksheetDetailResponse() throws Exception {
		GetWorksheetDetailsResponse mockResponse = mock(GetWorksheetDetailsResponse.class);
		InputGetWorksheetDetailsInput mockInput = mock(InputGetWorksheetDetailsInput.class);
		GetWorksheetDetails mockGetWorksheetDetails = mock(GetWorksheetDetails.class);
		when(mockObjectFactoryService.getInputGetWorksheetDetailsInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetDetails()).thenReturn(mockGetWorksheetDetails);
		when(mockWSAPIServiceStub.getWorksheetDetails(mockGetWorksheetDetails)).thenReturn(mockResponse);

		GetWorksheetDetailsResponse response = whiteSpaceRequestService.getWorksheetDetails(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockGetWorksheetDetails, times(1)).setWorksheetDetailsInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetDetailServiceItems_WhenNoError_ThenReturnWorksheetServiceItemResponse() throws Exception {
		GetWorksheetDetailServiceItemsResponse mockResponse = mock(GetWorksheetDetailServiceItemsResponse.class);
		InputGetWorksheetDetailServiceItemsInput mockInput = mock(InputGetWorksheetDetailServiceItemsInput.class);
		GetWorksheetDetailServiceItems mockGetWorksheetDetailServiceItems = mock(GetWorksheetDetailServiceItems.class);
		when(mockObjectFactoryService.getInputGetWorksheetDetailServiceItemsInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetDetailServiceItems()).thenReturn(mockGetWorksheetDetailServiceItems);
		when(mockWSAPIServiceStub.getWorksheetDetailServiceItems(mockGetWorksheetDetailServiceItems)).thenReturn(mockResponse);

		GetWorksheetDetailServiceItemsResponse response = whiteSpaceRequestService.getWorksheetDetailServiceItems(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockGetWorksheetDetailServiceItems, times(1)).setWorksheetDetailServiceItemsInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetExtraInfoFields_WhenNoError_ThenReturnGetWorksheetExtraInfoFieldsResponseGetWorksheetExtraInfoFieldsResult() throws Exception {
		GetWorksheetExtraInfoFieldsResponse mockResponse = mock(GetWorksheetExtraInfoFieldsResponse.class);
		InputGetServicePropertyInput mockInput = mock(InputGetServicePropertyInput.class);
		GetWorksheetExtraInfoFields mockGetWorksheetExtraInfoFields = mock(GetWorksheetExtraInfoFields.class);
		when(mockObjectFactoryService.getInputGetServicePropertyInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetExtraInfoFields()).thenReturn(mockGetWorksheetExtraInfoFields);
		when(mockWSAPIServiceStub.getWorksheetExtraInfoFields(mockGetWorksheetExtraInfoFields)).thenReturn(mockResponse);

		GetWorksheetExtraInfoFieldsResponse response = whiteSpaceRequestService.getWorksheetExtraInfoFields(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setServiceId(PARAM1);
		verify(mockGetWorksheetExtraInfoFields, times(1)).setServicepropertyInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetRoles_WhenNoError_ThenReturnServiceAssignedToResponse() throws Exception {
		GetWorksheetRolesResponse mockResponse = mock(GetWorksheetRolesResponse.class);
		InputGetWorksheetRolesInput mockInput = mock(InputGetWorksheetRolesInput.class);
		GetWorksheetRoles mockGetWorksheetRoles = mock(GetWorksheetRoles.class);
		when(mockObjectFactoryService.getInputGetWorksheetRolesInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetRoles()).thenReturn(mockGetWorksheetRoles);
		when(mockWSAPIServiceStub.getWorksheetRoles(mockGetWorksheetRoles)).thenReturn(mockResponse);

		GetWorksheetRolesResponse response = whiteSpaceRequestService.getWorksheetRoles(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setServiceId(PARAM1);
		verify(mockGetWorksheetRoles, times(1)).setWorksheetRolesInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetsByReference_WhenNoError_ThenReturnWorksheetResponse() throws Exception {
		GetWorksheetsByReferenceResponse mockResponse = mock(GetWorksheetsByReferenceResponse.class);
		InputGetWorksheetsByRefInput mockInput = mock(InputGetWorksheetsByRefInput.class);
		GetWorksheetsByReference mockGetWorksheetsByReference = mock(GetWorksheetsByReference.class);
		when(mockObjectFactoryService.getInputGetWorksheetsByRefInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetsByReference()).thenReturn(mockGetWorksheetsByReference);
		when(mockWSAPIServiceStub.getWorksheetsByReference(mockGetWorksheetsByReference)).thenReturn(mockResponse);

		GetWorksheetsByReferenceResponse response = whiteSpaceRequestService.getWorksheetsByReference(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setWorksheetReference(PARAM1);
		verify(mockGetWorksheetsByReference, times(1)).setWorksheetsByRefInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void getWorksheetServiceItems_WhenNoError_ThenReturnServiceItemWorksheetResponse() throws Exception {
		GetWorksheetServiceItemsResponse mockResponse = mock(GetWorksheetServiceItemsResponse.class);
		InputGetWorksheetServiceItemInput mockInput = mock(InputGetWorksheetServiceItemInput.class);
		GetWorksheetServiceItems mockGetWorksheetServiceItems = mock(GetWorksheetServiceItems.class);
		when(mockObjectFactoryService.getInputGetWorksheetServiceItemInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getGetWorksheetServiceItems()).thenReturn(mockGetWorksheetServiceItems);
		when(mockWSAPIServiceStub.getWorksheetServiceItems(mockGetWorksheetServiceItems)).thenReturn(mockResponse);

		GetWorksheetServiceItemsResponse response = whiteSpaceRequestService.getWorksheetServiceItems(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setSiteId(PARAM1);
		verify(mockInput, times(1)).setServiceId(PARAM2);
		verify(mockInput, times(1)).setUprn(PARAM3);
		verify(mockGetWorksheetServiceItems, times(1)).setWorksheetServiceItemInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void progressWorkflow_WhenNoError_ThenReturnWSResponse() throws Exception {
		ProgressWorkflowResponse mockResponse = mock(ProgressWorkflowResponse.class);
		InputProgressWorkflowInput mockInput = mock(InputProgressWorkflowInput.class);
		ProgressWorkflow mockProgressWorkflow = mock(ProgressWorkflow.class);
		when(mockObjectFactoryService.getInputProgressWorkflowInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getProgressWorkflow()).thenReturn(mockProgressWorkflow);
		when(mockWSAPIServiceStub.progressWorkflow(mockProgressWorkflow)).thenReturn(mockResponse);

		ProgressWorkflowResponse response = whiteSpaceRequestService.progressWorkflow(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setWorksheetId(PARAM1);
		verify(mockInput, times(1)).setWorksheetRef(PARAM2);
		verify(mockInput, times(1)).setEventName(PARAM3);
		verify(mockProgressWorkflow, times(1)).setProgressWorkflowInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void updateSiteContact_WhenNoError_ThenReturnWSResponse() throws Exception {
		UpdateSiteContactResponse mockResponse = mock(UpdateSiteContactResponse.class);
		InputUpdateSiteContactInput mockInput = mock(InputUpdateSiteContactInput.class);
		UpdateSiteContact mockUpdateSiteContact = mock(UpdateSiteContact.class);
		when(mockObjectFactoryService.getInputUpdateSiteContactInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getUpdateSiteContact()).thenReturn(mockUpdateSiteContact);
		when(mockWSAPIServiceStub.updateSiteContact(mockUpdateSiteContact)).thenReturn(mockResponse);

		UpdateSiteContactResponse response = whiteSpaceRequestService.updateSiteContact(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, true, PARAM7, PARAM8);

		verify(mockInput, times(1)).setContactEmail(PARAM1);
		verify(mockInput, times(1)).setContactId(PARAM2);
		verify(mockInput, times(1)).setContactName(PARAM3);
		verify(mockInput, times(1)).setContactFaxNumber(PARAM4);
		verify(mockInput, times(1)).setContactMobileNumber(PARAM5);
		verify(mockInput, times(1)).setContactTelephoneNumber(PARAM6);
		verify(mockInput, times(1)).setIsMainContact(true);
		verify(mockInput, times(1)).setSiteId(PARAM7);
		verify(mockInput, times(1)).setUprn(PARAM8);
		verify(mockUpdateSiteContact, times(1)).setUpdateSiteContactInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void updateSiteServiceItem_WhenNoError_ThenReturnWSResponse() throws Exception {
		SiteCollectionExtraDetailInputExt siteCollectionExtraDetailInputExt = mock(SiteCollectionExtraDetailInputExt.class);
		ArrayOfInputSiteCollectionExtraDetailInput arrayOfInputSiteCollectionExtraDetailInput = new ArrayOfInputSiteCollectionExtraDetailInput();
		UpdateSiteServiceItemResponse mockResponse = mock(UpdateSiteServiceItemResponse.class);
		InputUpdateSiteServiceItemInput mockInput = mock(InputUpdateSiteServiceItemInput.class);
		UpdateSiteServiceItem mockUpdateSiteServiceItem = mock(UpdateSiteServiceItem.class);
		when(mockObjectFactoryService.getInputUpdateSiteServiceItemInput()).thenReturn(mockInput);
		when(siteCollectionExtraDetailInputExt.getSiteCollectionExtraDetails()).thenReturn(arrayOfInputSiteCollectionExtraDetailInput);
		when(mockObjectFactoryService.getUpdateSiteServiceItem()).thenReturn(mockUpdateSiteServiceItem);
		when(mockWSAPIServiceStub.updateSiteServiceItem(mockUpdateSiteServiceItem)).thenReturn(mockResponse);

		UpdateSiteServiceItemResponse response = whiteSpaceRequestService.updateSiteServiceItem(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, siteCollectionExtraDetailInputExt);

		verify(mockInput, times(1)).setSiteServiceId(PARAM1);
		verify(mockInput, times(1)).setServiceItemQuantity(PARAM2);
		verify(mockInput, times(1)).setServiceItemValidFrom(PARAM3);
		verify(mockInput, times(1)).setServiceItemValidTo(PARAM4);
		verify(mockInput, times(1)).setChargeTypeId(PARAM5);
		verify(mockInput, times(1)).setExtraDetailInputs(arrayOfInputSiteCollectionExtraDetailInput);
		verify(mockUpdateSiteServiceItem, times(1)).setUpdateSiteServiceItemInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void updateSiteServiceNotification_WhenNoError_ThenReturnWSResponse() throws Exception {
		UpdateSiteServiceNotificationResponse mockResponse = mock(UpdateSiteServiceNotificationResponse.class);
		InputUpdateSiteServiceNotificationInput mockInput = mock(InputUpdateSiteServiceNotificationInput.class);
		UpdateSiteServiceNotification mockUpdateSiteServiceNotification = mock(UpdateSiteServiceNotification.class);
		when(mockObjectFactoryService.getInputUpdateSiteServiceNotificationInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getUpdateSiteServiceNotification()).thenReturn(mockUpdateSiteServiceNotification);
		when(mockWSAPIServiceStub.updateSiteServiceNotification(mockUpdateSiteServiceNotification)).thenReturn(mockResponse);

		UpdateSiteServiceNotificationResponse response = whiteSpaceRequestService.updateSiteServiceNotification(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8);

		verify(mockInput, times(1)).setNotificationId(PARAM1);
		verify(mockInput, times(1)).setSiteServiceNotificationNotes(PARAM2);
		verify(mockInput, times(1)).setServiceId(PARAM3);
		verify(mockInput, times(1)).setSiteId(PARAM4);
		verify(mockInput, times(1)).setSiteServiceNotificationId(PARAM5);
		verify(mockInput, times(1)).setSiteServiceNotificationValidFrom(PARAM6);
		verify(mockInput, times(1)).setSiteServiceNotificationValidTo(PARAM7);
		verify(mockInput, times(1)).setUprn(PARAM8);
		verify(mockUpdateSiteServiceNotification, times(1)).setUpdateSiteServiceNotificationInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void updateWorkflowEventDate_WhenNoError_ThenReturnWSResponse() throws Exception {
		UpdateWorkflowEventDateResponse mockResponse = mock(UpdateWorkflowEventDateResponse.class);
		InputUpdateWorkflowEventDateInput mockInput = mock(InputUpdateWorkflowEventDateInput.class);
		UpdateWorkflowEventDate mockUpdateWorkflowEventDate = mock(UpdateWorkflowEventDate.class);
		when(mockObjectFactoryService.getInputUpdateWorkflowEventDateInput()).thenReturn(mockInput);
		when(mockObjectFactoryService.getUpdateWorkflowEventDate()).thenReturn(mockUpdateWorkflowEventDate);
		when(mockWSAPIServiceStub.updateWorkflowEventDate(mockUpdateWorkflowEventDate)).thenReturn(mockResponse);

		UpdateWorkflowEventDateResponse response = whiteSpaceRequestService.updateWorkflowEventDate(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);

		verify(mockInput, times(1)).setEventDate(PARAM1);
		verify(mockInput, times(1)).setEventName(PARAM2);
		verify(mockInput, times(1)).setWorksheetId(PARAM3);
		verify(mockInput, times(1)).setWorksheetRef(PARAM4);
		verify(mockInput, times(1)).setStateName(PARAM5);
		verify(mockUpdateWorkflowEventDate, times(1)).setUpdateWorkflowEventDateInput(mockInput);
		assertEquals(response, mockResponse);
	}

	@Test
	public void updateWorksheet_WhenNoError_ThenReturnWorksheetDetailResponse() throws Exception {
		ServicePropertyInputExt mockServiceItemInputExt = mock(ServicePropertyInputExt.class);
		InputUpdateWorksheetDetailInput mockInput = mock(InputUpdateWorksheetDetailInput.class);
		UpdateWorksheet mockUpdateWorksheet = mock(UpdateWorksheet.class);
		UpdateWorksheetResponse mockResponse = mock(UpdateWorksheetResponse.class);
		ArrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput arrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput = new ArrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput();

		when(mockWSAPIServiceStub.updateWorksheet(mockUpdateWorksheet)).thenReturn(mockResponse);
		when(mockObjectFactoryService.getInputUpdateWorksheetDetailInput()).thenReturn(mockInput);
		when(mockServiceItemInputExt.getServicePropertyUpdateInputs()).thenReturn(arrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput);
		when(mockObjectFactoryService.getUpdateWorksheet()).thenReturn(mockUpdateWorksheet);

		UpdateWorksheetResponse response = whiteSpaceRequestService.updateWorksheet(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12,
				PARAM13, PARAM14, PARAM15, PARAM16, PARAM17, PARAM18, PARAM19, PARAM20, PARAM21, mockServiceItemInputExt);

		verify(mockInput, times(1)).setWorkLocationAddress(PARAM1);
		verify(mockInput, times(1)).setWorkLocationAddressID(PARAM2);
		verify(mockInput, times(1)).setWorkLocationEasting(PARAM3);
		verify(mockInput, times(1)).setWorkLocationLatitude(PARAM4);
		verify(mockInput, times(1)).setWorkLocationLongitude(PARAM5);
		verify(mockInput, times(1)).setWorkLocationName(PARAM6);
		verify(mockInput, times(1)).setWorkLocationNorthing(PARAM7);
		verify(mockInput, times(1)).setWorkLocationText(PARAM8);
		verify(mockInput, times(1)).setWorksheetApprovedDate(PARAM9);
		verify(mockInput, times(1)).setWorksheetAssignedToID(PARAM10);
		verify(mockInput, times(1)).setWorksheetAssignedToTypeID(PARAM11);
		verify(mockInput, times(1)).setWorksheetCompletedDate(PARAM12);
		verify(mockInput, times(1)).setWorksheetDueDate(PARAM13);
		verify(mockInput, times(1)).setWorksheetEscallatedDate(PARAM14);
		verify(mockInput, times(1)).setWorksheetExpiryDate(PARAM15);
		verify(mockInput, times(1)).setWorksheetMessage(PARAM16);
		verify(mockInput, times(1)).setWorksheetImportanceID(PARAM17);
		verify(mockInput, times(1)).setWorksheetId(PARAM18);
		verify(mockInput, times(1)).setWorksheetPaymentDate(PARAM19);
		verify(mockInput, times(1)).setWorksheetReference(PARAM20);
		verify(mockInput, times(1)).setWorksheetSubject(PARAM21);
		verify(mockInput, times(1)).setServicePropertyInputs(arrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput);
		verify(mockUpdateWorksheet, times(1)).setWorksheetDetailInput(mockInput);
		assertEquals(response, mockResponse);
	}

}