package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONFactory;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.InputCreateWorksheetInputServiceItemInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ApiAdHocRoundInstance;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServicesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteWorksheetsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteService;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.model.ServiceItemInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.WorksheetValidator;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WhitespaceWasteServiceTest {

	private static final String SERVICE_ID = "3242";
	private static final String SERVICE_NAME = "service";
	private static final String SERVICE_ITEM_ID = "8765";
	private static final String SERVICE_ITEM_NAME = "serviceItem";
	private static final String SERVICE_ITEM_OPTION_REFERENCE = "optionRef";
	private static final String SERVICE_ITEM_SIZE = "20";
	private static final String UPRN = "3486656534";
	private static final long COMPANY_ID = 10;
	private static final long FORM_INSTANCE_ID = 43435L;

	@InjectMocks
	private WhitespaceWasteService whitespaceWasteService;

	@Mock
	private ApiAdHocRoundInstance mockApiAdHocRoundInstance;

	@Mock
	private WhitespaceConfigurationService mockWhitespaceConfigurationService;

	@Mock
	private WhitespaceRequestService mockWhiteSpaceRequestService;

	@Mock
	private ResponseParserService mockResponseParserService;

	@Mock
	private WhitespaceWasteServiceUtil mockWhitespaceWasteServiceUtil;

	@Mock
	private WhitespaceWasteServiceHelper mockWhitespaceWasteServiceHelper;

	@Mock
	private GetServicesResponse mockGetServicesResponse;

	@Mock
	private GetServiceItemsResponse mockGetServiceItemsResponse;

	@Mock
	private GetSiteWorksheetsResponse mockGetSiteWorksheetsResponse;

	@Mock
	private WhitespaceService mockWhitespaceService;

	@Mock
	private ServiceItem mockServiceItem;

	@Mock
	private Worksheet mockWorksheet;

	@Mock
	private Service mockService;

	@Mock
	private SiteService mockSiteServiceOne;

	@Mock
	private SiteService mockSiteServiceTwo;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private WorksheetValidator mockWorksheetValidator;

	@Mock
	private BinCollection mockBinCollection;

	@Test
	public void getWhitespaceService_WhenServiceExists_ThenReturnsFirstWhitespaceServiceFound() throws Exception {
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockWhiteSpaceRequestService.getServices(COMPANY_ID, SERVICE_ID)).thenReturn(mockGetServicesResponse);
		when(mockResponseParserService.parseGetServicesResponse(mockGetServicesResponse))
				.thenReturn(new com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service[] { mockService });

		com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service result = whitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService);

		assertThat(result, sameInstance(mockService));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getWhitespaceService_WhenServiceDoesNotExist_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockWhiteSpaceRequestService.getServices(COMPANY_ID, SERVICE_ID)).thenReturn(mockGetServicesResponse);
		when(mockResponseParserService.parseGetServicesResponse(mockGetServicesResponse))
				.thenReturn(new com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service[] {});

		whitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService);
	}

	@Test(expected = Exception.class)
	public void getWhitespaceService_WhenServicesRetrievalFails_ThenThrowsException() throws Exception {
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockWhiteSpaceRequestService.getServices(COMPANY_ID, SERVICE_ID)).thenThrow(new Exception());

		whitespaceWasteService.getWhitespaceService(COMPANY_ID, mockWhitespaceService);
	}

	@Test
	public void getWhitespaceServiceItem_WhenServiceItemExists_ThenReturnsFirstWhitespaceServiceFound() throws Exception {
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockWhiteSpaceRequestService.getServiceItems(COMPANY_ID, SERVICE_ID, SERVICE_ITEM_ID)).thenReturn(mockGetServiceItemsResponse);
		when(mockResponseParserService.parseGetServiceItemsResponse(mockGetServiceItemsResponse)).thenReturn(new ServiceItem[] { mockServiceItem });

		ServiceItem result = whitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID);

		assertThat(result, sameInstance(mockServiceItem));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getWhitespaceServiceItem_WhenServiceItemDoesNotExist_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockWhiteSpaceRequestService.getServiceItems(COMPANY_ID, SERVICE_ID, SERVICE_ITEM_ID)).thenReturn(mockGetServiceItemsResponse);
		when(mockResponseParserService.parseGetServiceItemsResponse(mockGetServiceItemsResponse)).thenReturn(new ServiceItem[] {});

		whitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID);
	}

	@Test(expected = Exception.class)
	public void getWhitespaceServiceItem_WhenServiceItemsRetrievalFails_ThenThrowsException() throws Exception {
		when(mockWhitespaceService.getServiceId()).thenReturn(SERVICE_ID);
		when(mockWhiteSpaceRequestService.getServiceItems(COMPANY_ID, SERVICE_ID, SERVICE_ITEM_ID)).thenThrow(new Exception());

		whitespaceWasteService.getWhitespaceServiceItem(COMPANY_ID, mockWhitespaceService, SERVICE_ITEM_ID);
	}

	@Test
	public void getServiceItemId_WhenServiceItemNameAndSizeExistInServiceMappings_ThenReturnsServiceItemId() throws Exception {
		Map<String, String> serviceItemMappings = new HashMap<>();
		serviceItemMappings.put(SERVICE_ITEM_SIZE, SERVICE_ITEM_ID);

		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(serviceItemMappings);
		String result = whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, SERVICE_ITEM_SIZE, FORM_INSTANCE_ID);

		assertThat(result, equalTo(SERVICE_ITEM_ID));
	}

	@Test
	public void getServiceItemId_WhenServiceItemNameExistsInMappingsAndSizeIsNull_ThenReturnsFirstServiceItemId() throws Exception {
		Map<String, String> serviceItemMappings = new HashMap<>();
		serviceItemMappings.put(SERVICE_ITEM_SIZE, SERVICE_ITEM_ID);

		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(serviceItemMappings);
		String result = whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, null, FORM_INSTANCE_ID);

		assertThat(result, equalTo(SERVICE_ITEM_ID));
	}

	@Test
	public void getServiceItemId_WhenServiceItemNameOptionReferenceAndSizeExistInServiceMappings_ThenReturnsServiceItemIdForOptionReference() throws Exception {
		Map<String, String> serviceItemMappings = new HashMap<>();
		serviceItemMappings.put(SERVICE_ITEM_SIZE, SERVICE_ITEM_ID);

		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(null);
		when(mockWhitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, SERVICE_ITEM_NAME)).thenReturn(Optional.of(SERVICE_ITEM_OPTION_REFERENCE));
		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_OPTION_REFERENCE)).thenReturn(serviceItemMappings);

		String result = whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, SERVICE_ITEM_SIZE, FORM_INSTANCE_ID);

		assertThat(result, equalTo(SERVICE_ITEM_ID));
	}

	@Test
	public void getServiceItemId_WhenServiceItemNameAndSizeOptionReferenceExistInServiceMappings_ThenReturnsServiceItemIdForSizeOptionReference() throws Exception {
		Map<String, String> serviceItemMappings = new HashMap<>();
		serviceItemMappings.put(SERVICE_ITEM_OPTION_REFERENCE, SERVICE_ITEM_ID);

		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(serviceItemMappings);
		when(mockWhitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, SERVICE_ITEM_SIZE)).thenReturn(Optional.of(SERVICE_ITEM_OPTION_REFERENCE));

		String result = whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, SERVICE_ITEM_SIZE, FORM_INSTANCE_ID);

		assertThat(result, equalTo(SERVICE_ITEM_ID));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceItemId_WhenServiceItemNameDoesNotExistInServiceMappingsAndServiceItemOptionReferenceIsNotPresent_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(null);
		when(mockWhitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, SERVICE_ITEM_NAME)).thenReturn(Optional.empty());
		whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, SERVICE_ITEM_SIZE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceItemId_WhenNeitherServiceItemNameNorOptionReferenceExistInServiceMappings_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(null);
		when(mockWhitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, SERVICE_ITEM_NAME)).thenReturn(Optional.of(SERVICE_ITEM_OPTION_REFERENCE));
		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_OPTION_REFERENCE)).thenReturn(null);
		whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, SERVICE_ITEM_SIZE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceItemId_WhenServiceItemSizeDoesNotExistInServiceMappingsAndSizeOptionReferenceIsNotPresent_ThenThrowsWasteRetrievalException() throws Exception {
		Map<String, String> serviceItemMappings = new HashMap<>();
		serviceItemMappings.put("other size", SERVICE_ITEM_ID);

		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(serviceItemMappings);
		when(mockWhitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, SERVICE_ITEM_SIZE)).thenReturn(Optional.empty());
		whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, SERVICE_ITEM_SIZE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceItemId_WhenNeitherServiceItemSizeNorSizeOptionReferenceExistInServiceMappings_ThenThrowsWasteRetrievalException() throws Exception {
		Map<String, String> serviceItemMappings = new HashMap<>();
		serviceItemMappings.put("other size", SERVICE_ITEM_ID);

		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(serviceItemMappings);
		when(mockWhitespaceWasteServiceHelper.getFormFieldValueOptionReference(FORM_INSTANCE_ID, SERVICE_ITEM_SIZE)).thenReturn(Optional.of("unknown size"));
		whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, SERVICE_ITEM_SIZE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceItemId_WhenServiceMappingsDontContainSizes_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockWhitespaceService.getServiceItemMappings(SERVICE_ITEM_NAME)).thenReturn(new HashMap<>());
		whitespaceWasteService.getServiceItemId(mockWhitespaceService, SERVICE_ITEM_NAME, SERVICE_ITEM_SIZE, FORM_INSTANCE_ID);
	}

	@Test
	public void existsOpenWorksheetByUprnServiceAndServiceItems_WhenValidatorIsNotPresentAndOpenWorksheetForServiceWithServiceItemIsFound_ThenReturnsTrue() throws Exception {
		when(mockWhiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, UPRN, null, SERVICE_NAME, true, "WorksheetSubject", null)).thenReturn(mockGetSiteWorksheetsResponse);

		when(mockResponseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse)).thenReturn(new Worksheet[] { mockWorksheet });
		when(mockWorksheet.getWorksheetStatusName()).thenReturn("Open");
		when(mockWhitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, Arrays.asList(SERVICE_ITEM_ID))).thenReturn(true);

		assertThat(whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.empty()), equalTo(true));
	}

	@Test
	public void existsOpenWorksheetByUprnServiceAndServiceItems_WhenValidatorIsPresentAndOpenWorksheetForServiceWithServiceItemIsFoundAndValidatorCheckReturnsFalse_ThenReturnsTrue() throws Exception {
		when(mockWhiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, UPRN, null, SERVICE_NAME, true, "WorksheetSubject", null)).thenReturn(mockGetSiteWorksheetsResponse);

		when(mockResponseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse)).thenReturn(new Worksheet[] { mockWorksheet });
		when(mockWorksheet.getWorksheetStatusName()).thenReturn("Open");
		when(mockWhitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, Arrays.asList(SERVICE_ITEM_ID))).thenReturn(true);
		when(mockWorksheetValidator.isValid(mockWorksheet)).thenReturn(false);

		assertThat(whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator)),
				equalTo(true));
	}

	@Test
	@Parameters({ "true", "false" })
	public void existsOpenWorksheetByUprnServiceAndServiceItems_WhenNoOpenWorksheetsForService_ThenReturnsFalse(boolean worksheetValid) throws Exception {
		when(mockWhiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, UPRN, null, SERVICE_NAME, true, "WorksheetSubject", null)).thenReturn(mockGetSiteWorksheetsResponse);

		when(mockResponseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse)).thenReturn(new Worksheet[] { mockWorksheet });
		when(mockWorksheet.getWorksheetStatusName()).thenReturn("Cancelled");
		when(mockWorksheetValidator.isValid(mockWorksheet)).thenReturn(worksheetValid);

		assertThat(whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator)),
				equalTo(false));
	}

	@Test
	public void existsOpenWorksheetByUprnServiceAndServiceItems_WhenValidatorIsPresentAndValidationCheckPassesAndOpenWorksheetForService_ThenReturnsFalse() throws Exception {
		when(mockWhiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, UPRN, null, SERVICE_NAME, true, "WorksheetSubject", null)).thenReturn(mockGetSiteWorksheetsResponse);

		when(mockResponseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse)).thenReturn(new Worksheet[] { mockWorksheet });
		when(mockWorksheet.getWorksheetStatusName()).thenReturn("Open");
		when(mockWorksheetValidator.isValid(mockWorksheet)).thenReturn(true);

		assertThat(whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator)),
				equalTo(false));

	}

	@Test
	public void existsOpenWorksheetByUprnServiceAndServiceItems_WhenValidatorIsPresentAndValidationCheckFailsAndOpenWorksheetForServiceButNoServiceItemIsFound_ThenReturnsFalse() throws Exception {
		when(mockWhiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, UPRN, null, SERVICE_NAME, true, "WorksheetSubject", null)).thenReturn(mockGetSiteWorksheetsResponse);

		when(mockResponseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse)).thenReturn(new Worksheet[] { mockWorksheet });
		when(mockWorksheet.getWorksheetStatusName()).thenReturn("Open");
		when(mockWorksheetValidator.isValid(mockWorksheet)).thenReturn(false);

		assertThat(whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator)),
				equalTo(false));

	}

	@Test
	public void existsOpenWorksheetByUprnServiceAndServiceItems_WhenValidatorIsNotPresentAndOpenWorksheetForServiceButNoServiceItemIsFound_ThenReturnsFalse() throws Exception {
		when(mockWhiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, UPRN, null, SERVICE_NAME, true, "WorksheetSubject", null)).thenReturn(mockGetSiteWorksheetsResponse);

		when(mockResponseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse)).thenReturn(new Worksheet[] { mockWorksheet });
		when(mockWorksheet.getWorksheetStatusName()).thenReturn("Open");
		when(mockWhitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, Arrays.asList(SERVICE_ITEM_ID))).thenReturn(false);

		boolean result = whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.empty());

		assertThat(result, equalTo(false));

	}

	@Test(expected = Exception.class)
	public void existsOpenWorksheetByUprnServiceAndServiceItems_WhenWorksheetsRetrievalFails_ThenThrowsException() throws Exception {
		when(mockWhiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, UPRN, null, SERVICE_NAME, true, "WorksheetSubject", null)).thenThrow(new Exception());
		whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.of(mockWorksheetValidator));

	}

	@Test(expected = Exception.class)
	public void existsOpenWorksheetByUprnServiceAndServiceItems_WhenValidatorIsNotPresentAndWorksheetServiteItemsCheckFails_ThenThrowsException() throws Exception {
		when(mockWhiteSpaceRequestService.getSiteWorksheets(COMPANY_ID, UPRN, null, SERVICE_NAME, true, "WorksheetSubject", null)).thenReturn(mockGetSiteWorksheetsResponse);

		when(mockResponseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse)).thenReturn(new Worksheet[] { mockWorksheet });
		when(mockWorksheet.getWorksheetStatusName()).thenReturn("Open");
		when(mockWhitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(COMPANY_ID, mockWorksheet, Arrays.asList(SERVICE_ITEM_ID))).thenThrow(new Exception());

		whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(COMPANY_ID, UPRN, SERVICE_NAME, Arrays.asList(SERVICE_ITEM_ID), Optional.empty());
	}

	@Test
	public void createServiceItemInputExt_WhenNoErrors_ThenReturnsServiceItemInputExtForServiceItemsAndQuantities() throws Exception {
		final int quantity = 6;
		when(mockServiceItem.getServiceItemID()).thenReturn(Integer.valueOf(SERVICE_ITEM_ID));
		when(mockServiceItem.getServiceItemName()).thenReturn(SERVICE_ITEM_NAME);

		ServiceItemInputExt result = whitespaceWasteService.createServiceItemInputExt(Arrays.asList(mockServiceItem), Arrays.asList(quantity));

		InputCreateWorksheetInputServiceItemInput[] inputCreateWorksheetInputServiceItemInputs = result.getServiceItemInputs().getInputCreateWorksheetInputServiceItemInput();

		assertThat(inputCreateWorksheetInputServiceItemInputs.length, equalTo(1));
		assertThat(inputCreateWorksheetInputServiceItemInputs[0].getServiceItemId(), equalTo(Integer.parseInt(SERVICE_ITEM_ID)));
		assertThat(inputCreateWorksheetInputServiceItemInputs[0].getServiceItemName(), equalTo(SERVICE_ITEM_NAME));
		assertThat(inputCreateWorksheetInputServiceItemInputs[0].getServiceItemQuantity(), equalTo(new BigDecimal(quantity)));
	}

	@Test
	public void getTotalSiteServicesQuantities_WhenSiteServiceIdMatchesServiceIdAndSiteServiceIsValid_ThenSumsAllQuantities() {
		int serviceId = Integer.parseInt(SERVICE_ID);
		Calendar siteServiceValidTo = Calendar.getInstance();

		when(mockSiteServiceOne.getServiceID()).thenReturn(serviceId);
		when(mockSiteServiceOne.getQuantity()).thenReturn(1);
		when(mockSiteServiceOne.getSiteServiceValidTo()).thenReturn(siteServiceValidTo);

		when(mockSiteServiceTwo.getServiceID()).thenReturn(serviceId);
		when(mockSiteServiceTwo.getQuantity()).thenReturn(2);
		when(mockSiteServiceTwo.getSiteServiceValidTo()).thenReturn(siteServiceValidTo);

		when(mockWhitespaceWasteServiceUtil.isBeforeClockDate(Clock.systemDefaultZone(), siteServiceValidTo.getTime())).thenReturn(false);

		int result = whitespaceWasteService.getTotalSiteServicesQuantities(new SiteService[] { mockSiteServiceOne, mockSiteServiceTwo }, serviceId);

		assertThat(result, equalTo(3));
	}

	@Test
	public void getTotalSiteServicesQuantities_WhenSiteServiceIdMatchesServiceIdAndSiteServiceIsNotValid_ThenDoesNotSumQuantity() {
		int serviceId = Integer.parseInt(SERVICE_ID);
		Calendar siteServiceValidTo = Calendar.getInstance();

		when(mockSiteServiceOne.getServiceID()).thenReturn(serviceId);
		when(mockSiteServiceOne.getSiteServiceValidTo()).thenReturn(siteServiceValidTo);

		when(mockSiteServiceTwo.getServiceID()).thenReturn(serviceId);
		when(mockSiteServiceTwo.getSiteServiceValidTo()).thenReturn(siteServiceValidTo);

		when(mockWhitespaceWasteServiceUtil.isBeforeClockDate(Clock.systemDefaultZone(), siteServiceValidTo.getTime())).thenReturn(true);

		int result = whitespaceWasteService.getTotalSiteServicesQuantities(new SiteService[] { mockSiteServiceOne, mockSiteServiceTwo }, serviceId);

		assertThat(result, equalTo(0));
	}

	@Test
	public void getTotalSiteServicesQuantities_WhenSiteServiceIdMatchesServiceIdAndSiteServiceValidToDateIsDefaultDate_ThenDoesNotSumQuantity() {
		int serviceId = Integer.parseInt(SERVICE_ID);
		Calendar siteServiceValidTo = new Calendar.Builder().setDate(1, 1, 1).setTimeOfDay(0, 0, 0).build();

		when(mockSiteServiceOne.getServiceID()).thenReturn(serviceId);
		when(mockSiteServiceOne.getSiteServiceValidTo()).thenReturn(siteServiceValidTo);

		when(mockSiteServiceTwo.getServiceID()).thenReturn(serviceId);
		when(mockSiteServiceTwo.getSiteServiceValidTo()).thenReturn(siteServiceValidTo);

		int result = whitespaceWasteService.getTotalSiteServicesQuantities(new SiteService[] { mockSiteServiceOne, mockSiteServiceTwo }, serviceId);

		assertThat(result, equalTo(0));
	}

	@Test
	public void getTotalSiteServicesQuantities_WhenSiteServiceIdDoesNotMatchServiceId_ThenDoesNotSumQuantity() {
		int serviceId = Integer.parseInt(SERVICE_ID);
		Calendar siteServiceValidTo = Calendar.getInstance();

		when(mockSiteServiceOne.getServiceID()).thenReturn(serviceId + 1);
		when(mockSiteServiceTwo.getServiceID()).thenReturn(serviceId + 1);

		when(mockWhitespaceWasteServiceUtil.isBeforeClockDate(Clock.systemDefaultZone(), siteServiceValidTo.getTime())).thenReturn(true);

		int result = whitespaceWasteService.getTotalSiteServicesQuantities(new SiteService[] { mockSiteServiceOne, mockSiteServiceTwo }, serviceId);

		assertThat(result, equalTo(0));
	}

	@Test
	public void createWasteSubscriptionResponse_WhenNoErrors_ThenReturnsSubscriptionResponseWithReferenceAndType() {
		final String subscriptionRef = "ref";
		final SubscriptionType subscriptionType = SubscriptionType.GARDEN_WASTE;

		WasteSubscriptionResponse result = whitespaceWasteService.createWasteSubscriptionResponse(subscriptionRef, subscriptionType);

		assertThat(result.getSubscriptionRef(), equalTo(subscriptionRef));
		assertThat(result.getSubscriptionType(), equalTo(subscriptionType));
	}

	@Test
	public void getWasteBulkyCollectionDateIfFreeSlots_WhenDateIsNotNullAndThereAreFreeSlots_ThenReturnsDate() {
		Calendar calendar = Calendar.getInstance();

		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceDate()).thenReturn(calendar);
		when(mockApiAdHocRoundInstance.getSlotsFree()).thenReturn(1);

		Optional<Date> result = whitespaceWasteService.getWasteBulkyCollectionDateIfFreeSlots(mockApiAdHocRoundInstance);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), equalTo(calendar.getTime()));
	}

	@Test
	public void getWasteBulkyCollectionDateIfFreeSlots_WhenDateIsNull_ThenReturnsEmptyOptional() {
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceDate()).thenReturn(null);

		Optional<Date> result = whitespaceWasteService.getWasteBulkyCollectionDateIfFreeSlots(mockApiAdHocRoundInstance);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getWasteBulkyCollectionDateIfFreeSlots_WhenDateIsNotNullAndThereAreNoFreeSlots_ThenReturnsEmptyOptional() {
		when(mockApiAdHocRoundInstance.getAdHocRoundInstanceDate()).thenReturn(Calendar.getInstance());
		when(mockApiAdHocRoundInstance.getSlotsFree()).thenReturn(0);

		Optional<Date> result = whitespaceWasteService.getWasteBulkyCollectionDateIfFreeSlots(mockApiAdHocRoundInstance);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getItemsToQuantitiesMap_WhenNoErrors_ThenReturnsMapOfTypesToQuantities() throws Exception {
		String[] binTypes = { "type1", "type2", "type1", "type2" };
		String[] quantities = { "1", "2", "3", "1" };

		Map<String, Integer> result = whitespaceWasteService.getItemsToQuantitiesMap(binTypes, quantities);

		assertThat(result.get("type1"), equalTo(4));
		assertThat(result.get("type2"), equalTo(3));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getItemsToQuantitiesMap_WhenNumberOfValidTypesAndQuantitiesIsDifferent_ThenThrowsWasteRetrievalException() throws Exception {
		String[] binTypes = { "type1", "type2", "" };
		String[] quantities = { "", "2", "" };

		whitespaceWasteService.getItemsToQuantitiesMap(binTypes, quantities);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getItemsToQuantitiesMap_WhenQuantityCantBeParsedToInt__ThenThrowsWasteRetrievalException() throws Exception {
		String[] binTypes = { "type1" };
		String[] quantities = { "test" };

		whitespaceWasteService.getItemsToQuantitiesMap(binTypes, quantities);

	}

	@Test
	public void filterBinCollectionsByService_WhenBinCollectionNameMatchesConfiguredCollectionServiceName_ThenReturnsBinCollection() throws Exception {
		final String collectionServiceName = "collection service name";

		Set<BinCollection> binCollections = new HashSet<>();
		binCollections.add(mockBinCollection);
		Map<String, WhitespaceService> whitespaceServicesMap = new HashMap<>();
		whitespaceServicesMap.put(SERVICE_NAME, mockWhitespaceService);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(whitespaceServicesMap);
		when(mockWhitespaceService.getCollectionServiceName()).thenReturn(collectionServiceName);
		when(mockBinCollection.getName()).thenReturn(collectionServiceName);

		Set<BinCollection> results = whitespaceWasteService.filterBinCollectionsByService(COMPANY_ID, SERVICE_NAME, String.valueOf(FORM_INSTANCE_ID), binCollections);

		List<BinCollection> binCollectionsList = new ArrayList<>(results);
		assertThat(binCollectionsList.size(), equalTo(1));
		assertThat(binCollectionsList.get(0), sameInstance(mockBinCollection));
	}

	@Test
	public void filterBinCollectionsByService_WhenBinCollectionNameDoesNotMatchConfiguredCollectionServiceName_ThenDoesNotReturnBinCollection() throws Exception {
		final String collectionServiceName = "collection service name";

		Set<BinCollection> binCollections = new HashSet<>();
		binCollections.add(mockBinCollection);
		Map<String, WhitespaceService> whitespaceServicesMap = new HashMap<>();
		whitespaceServicesMap.put(SERVICE_NAME, mockWhitespaceService);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(whitespaceServicesMap);
		when(mockWhitespaceService.getCollectionServiceName()).thenReturn(collectionServiceName);
		when(mockBinCollection.getName()).thenReturn("another service");

		Set<BinCollection> results = whitespaceWasteService.filterBinCollectionsByService(COMPANY_ID, SERVICE_NAME, String.valueOf(FORM_INSTANCE_ID), binCollections);

		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void filterBinCollectionsByService_WhenServiceIsNotConfigured_ThenReturnsUnfilteredBinCollection() throws Exception {
		Set<BinCollection> binCollections = new HashSet<>();
		binCollections.add(mockBinCollection);
		Map<String, WhitespaceService> whitespaceServicesMap = new HashMap<>();

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(whitespaceServicesMap);

		Set<BinCollection> results = whitespaceWasteService.filterBinCollectionsByService(COMPANY_ID, SERVICE_NAME, String.valueOf(FORM_INSTANCE_ID), binCollections);

		List<BinCollection> binCollectionsList = new ArrayList<>(results);
		assertThat(binCollectionsList.size(), equalTo(1));
		assertThat(binCollectionsList.get(0), sameInstance(mockBinCollection));
	}

	@Test
	public void filterBinCollectionsByService_WhenCollectionServiceNameIsNotConfigured_ThenReturnsUnfilteredBinCollection() throws Exception {
		final String collectionServiceName = "collection service name";

		Set<BinCollection> binCollections = new HashSet<>();
		binCollections.add(mockBinCollection);
		Map<String, WhitespaceService> whitespaceServicesMap = new HashMap<>();
		whitespaceServicesMap.put(SERVICE_NAME, mockWhitespaceService);

		when(mockWhitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(COMPANY_ID, FORM_INSTANCE_ID)).thenReturn(whitespaceServicesMap);
		when(mockWhitespaceService.getCollectionServiceName()).thenReturn(null);
		when(mockBinCollection.getName()).thenReturn(collectionServiceName);

		Set<BinCollection> results = whitespaceWasteService.filterBinCollectionsByService(COMPANY_ID, SERVICE_NAME, String.valueOf(FORM_INSTANCE_ID), binCollections);

		List<BinCollection> binCollectionsList = new ArrayList<>(results);
		assertThat(binCollectionsList.size(), equalTo(1));
		assertThat(binCollectionsList.get(0), sameInstance(mockBinCollection));
	}

}
