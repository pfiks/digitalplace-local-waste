package com.placecube.digitalplace.local.waste.whitespace.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AdHocRoundInstanceResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ApiAdHocRoundInstance;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfApiAdHocRoundInstance;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfCollection;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfService;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfSiteService;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfWorksheet;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfWorksheetServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Collection;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CollectionResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CreateWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionByUprnAndDateResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionSlotsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServicesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteWorksheetsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteService;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteServiceResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.WorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.WorksheetServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.WorksheetServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfanyType;
import com.placecube.digitalplace.local.waste.whitespace.configuration.WhitespaceCompanyConfiguration;
import com.placecube.digitalplace.local.waste.whitespace.constants.WhitespaceWebServiceConstants;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

@RunWith(PowerMockRunner.class)
public class ResponseParserServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	private static final String DAY = "day";

	private static final String FORTNIGHTLY = "Fortnightly";

	private static final String MAPPED_SERVICE_NAME = "mappedServiceName";

	private static final String SCHEDULE = "schedule";

	private static final String SCHEDULE_MONFORT2 = "MonFort2";

	private static final String SERVICE_NAME = "serviceName";

	private static final String SHORT_DATE = "01/01/2022";

	@Mock
	private Collection mockCollection;

	@Mock
	private CollectionResponse mockCollectionResponse;

	@Mock
	private GetCollectionByUprnAndDateResponse mockGetCollectionByUprnAndDateResponse;

	@Mock
	private WhitespaceCompanyConfiguration mockWhitespaceCompanyConfiguration;

	@Mock
	private WhitespaceConfigurationService mockWhitespaceWasteService;

	@Mock
	private CreateWorksheetResponse mockCreateWorksheetResponse;

	@Mock
	private com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.webservices.CreateWorksheetResponse mockWebServiceCreateWorksheetResponse;

	@Mock
	private ArrayOfanyType mockArrayOfanyType;

	@Mock
	private GetServicesResponse mockGetServicesResponse;

	@Mock
	private ServiceResponse mockServiceResponse;

	@Mock
	private ArrayOfService mockArrayOfService;

	@Mock
	private Service mockService;

	@Mock
	private GetServiceItemsResponse mockGetServiceItemsResponse;

	@Mock
	private ServiceItemResponse mockServiceItemResponse;

	@Mock
	private GetSiteWorksheetsResponse mockGetSiteWorksheetsResponse;

	@Mock
	private WorksheetResponse mockWorksheetResponse;

	@Mock
	private ArrayOfWorksheet mockArrayOfWorksheet;

	@Mock
	private Worksheet mockWorksheet;

	@Mock
	private ArrayOfServiceItem mockArrayOfServiceItem;

	@Mock
	private ServiceItem mockServiceItem;

	@Mock
	private AdHocRoundInstanceResponse mockAdHocRoundInstanceResponse;

	@Mock
	private ArrayOfApiAdHocRoundInstance mockArrayOfApiAdHocRoundInstance;

	@Mock
	private ApiAdHocRoundInstance mockApiAdHocRoundInstance;

	@Mock
	private GetWorksheetDetailServiceItemsResponse mockGetWorksheetDetailServiceItemsResponse;

	@Mock
	private WorksheetServiceItemResponse mockWorksheetServiceItemResponse;

	@Mock
	private ArrayOfWorksheetServiceItem mockArrayOfWorksheetServiceItem;

	@Mock
	private WorksheetServiceItem mockWorksheetServiceItem;

	@Mock
	private GetCollectionSlotsResponse mockGetCollectionSlotsResponse;

	@Mock
	private GetSiteCollectionsResponse mockGetSiteCollectionsResponse;

	@Mock
	private SiteServiceResponse mockSiteServiceResponse;

	@Mock
	private ArrayOfSiteService mockArrayOfSiteService;

	@Mock
	private SiteService mockSiteService;

	@Mock
	private WhitespaceWasteServiceUtil mockWhitespaceWasteServiceUtil;

	@InjectMocks
	private ResponseParserService responseParserService;

	@Test
	public void getBinCollections_WhenErrorGettingConfiguration_ThenReturnsBinCollectionWithDefaultValues() throws ConfigurationException {

		ArrayOfCollection collections = new ArrayOfCollection();
		collections.addCollection(mockCollection);

		when(mockCollection.getDate()).thenReturn(SHORT_DATE);
		when(mockGetCollectionByUprnAndDateResponse.getGetCollectionByUprnAndDateResult()).thenReturn(mockCollectionResponse);
		when(mockCollectionResponse.getCollections()).thenReturn(collections);
		when(mockWhitespaceWasteService.getConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());
		when(mockCollection.getService()).thenReturn(SERVICE_NAME);
		when(mockCollection.getDay()).thenReturn(DAY);
		when(mockCollection.getSchedule()).thenReturn(SCHEDULE);

		Set<BinCollection> result = responseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID);

		assertTrue(result.stream().anyMatch(binCollection -> SERVICE_NAME.equals(binCollection.getName())));
		assertTrue(result.stream().anyMatch(binCollection -> (DAY + StringPool.SPACE + SCHEDULE).equals(binCollection.getFormattedCollectionDate())));
	}

	@Test
	public void getBinCollections_WhenHasCollections_ThenReturnsBinCollection() throws ConfigurationException {

		ArrayOfCollection collections = new ArrayOfCollection();
		collections.addCollection(mockCollection);

		when(mockCollection.getDate()).thenReturn(SHORT_DATE);
		when(mockGetCollectionByUprnAndDateResponse.getGetCollectionByUprnAndDateResult()).thenReturn(mockCollectionResponse);
		when(mockCollectionResponse.getCollections()).thenReturn(collections);
		when(mockWhitespaceWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.keyValuePairMapping()).thenReturn(StringPool.BLANK);

		Set<BinCollection> result = responseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID);

		assertFalse(result.isEmpty());
	}

	@Test
	public void getBinCollections_WhenHasCollectionsAndDateHasTimeStamp_ThenReturnsBinCollection() throws ConfigurationException {

		ArrayOfCollection collections = new ArrayOfCollection();
		collections.addCollection(mockCollection);

		when(mockCollection.getDate()).thenReturn("01/01/2022 00: 00 : 00");
		when(mockGetCollectionByUprnAndDateResponse.getGetCollectionByUprnAndDateResult()).thenReturn(mockCollectionResponse);
		when(mockCollectionResponse.getCollections()).thenReturn(collections);
		when(mockWhitespaceWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.keyValuePairMapping()).thenReturn(StringPool.BLANK);

		Set<BinCollection> result = responseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID);

		assertFalse(result.isEmpty());
	}

	@Test
	public void getBinCollections_WhenKeyPairValueMappingHasValues_ThenReturnsBinCollectionWithMappedValues() throws ConfigurationException {
		String KEY_VALUE_MAPPING = SCHEDULE_MONFORT2 + "=" + FORTNIGHTLY + "\n" + SERVICE_NAME + "=" + MAPPED_SERVICE_NAME;

		ArrayOfCollection collections = new ArrayOfCollection();
		collections.addCollection(mockCollection);

		when(mockGetCollectionByUprnAndDateResponse.getGetCollectionByUprnAndDateResult()).thenReturn(mockCollectionResponse);
		when(mockCollectionResponse.getCollections()).thenReturn(collections);
		when(mockCollection.getDate()).thenReturn(SHORT_DATE);
		when(mockWhitespaceWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.keyValuePairMapping()).thenReturn(KEY_VALUE_MAPPING);
		when(mockCollection.getService()).thenReturn(SERVICE_NAME);
		when(mockCollection.getDay()).thenReturn(DAY);
		when(mockCollection.getSchedule()).thenReturn(SCHEDULE_MONFORT2);

		Set<BinCollection> result = responseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID);

		assertTrue(result.stream().anyMatch(binCollection -> MAPPED_SERVICE_NAME.equals(binCollection.getName())));
		assertTrue(result.stream().anyMatch(binCollection -> (DAY + StringPool.SPACE + FORTNIGHTLY).equals(binCollection.getFormattedCollectionDate())));
	}

	@Test
	public void getBinCollections_WhenKeyPairValueMappingIsEmpty_ThenReturnsBinCollectionWithDefaultValues() throws ConfigurationException {

		ArrayOfCollection collections = new ArrayOfCollection();
		collections.addCollection(mockCollection);
		when(mockCollection.getDate()).thenReturn(SHORT_DATE);
		when(mockGetCollectionByUprnAndDateResponse.getGetCollectionByUprnAndDateResult()).thenReturn(mockCollectionResponse);
		when(mockCollectionResponse.getCollections()).thenReturn(collections);
		when(mockWhitespaceWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.keyValuePairMapping()).thenReturn(StringPool.BLANK);
		when(mockCollection.getService()).thenReturn(SERVICE_NAME);
		when(mockCollection.getDay()).thenReturn(DAY);
		when(mockCollection.getSchedule()).thenReturn(SCHEDULE);

		Set<BinCollection> result = responseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID);

		assertTrue(result.stream().anyMatch(binCollection -> SERVICE_NAME.equals(binCollection.getName())));
		assertTrue(result.stream().anyMatch(binCollection -> (DAY + StringPool.SPACE + SCHEDULE).equals(binCollection.getFormattedCollectionDate())));
	}

	@Test
	public void getBinCollections_WhenHasCollections_ThenReturnsBinCollectionWithFormattedFrequency() throws ConfigurationException {

		ArrayOfCollection collections = new ArrayOfCollection();
		collections.addCollection(mockCollection);

		when(mockCollection.getDate()).thenReturn(SHORT_DATE);
		when(mockGetCollectionByUprnAndDateResponse.getGetCollectionByUprnAndDateResult()).thenReturn(mockCollectionResponse);
		when(mockCollectionResponse.getCollections()).thenReturn(collections);
		when(mockWhitespaceWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockWhitespaceCompanyConfiguration);
		when(mockWhitespaceCompanyConfiguration.keyValuePairMapping()).thenReturn(StringPool.BLANK);
		when(mockCollection.getSchedule()).thenReturn(SCHEDULE);
		when(mockWhitespaceWasteServiceUtil.formatSchedule(SCHEDULE)).thenReturn(FORTNIGHTLY);

		Set<BinCollection> result = responseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID);

		BinCollection[] binCollectionsArray = result.toArray(BinCollection[]::new);

		assertThat(binCollectionsArray.length, equalTo(1));
		assertThat(binCollectionsArray[0].getFrequency(), equalTo(FORTNIGHTLY));
	}

	@Test
	public void getBinCollections_WhenNoCollections_ThenReturnsEmptySet() {
		Set<BinCollection> result = responseParserService.parseBinCollections(mockGetCollectionByUprnAndDateResponse, COMPANY_ID);

		assertTrue(result.isEmpty());
	}

	@Test(expected = WasteRetrievalException.class)
	public void parseCreateWorksheetResponse_WhenResultSuccessFlagIsFalse_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockCreateWorksheetResponse.getCreateWorksheetResult()).thenReturn(mockWebServiceCreateWorksheetResponse);
		when(mockWebServiceCreateWorksheetResponse.getSuccessFlag()).thenReturn(false);

		responseParserService.parseCreateWorksheetResponse(mockCreateWorksheetResponse);
	}

	@Test(expected = WasteRetrievalException.class)
	public void parseCreateWorksheetResponse_WhenResultSuccessFlagIsTrueAndNoResults_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockCreateWorksheetResponse.getCreateWorksheetResult()).thenReturn(mockWebServiceCreateWorksheetResponse);
		when(mockWebServiceCreateWorksheetResponse.getSuccessFlag()).thenReturn(true);
		when(mockWebServiceCreateWorksheetResponse.getWorksheetResponse()).thenReturn(mockArrayOfanyType);
		when(mockArrayOfanyType.getAnyType()).thenReturn(new Object[] {});

		responseParserService.parseCreateWorksheetResponse(mockCreateWorksheetResponse);
	}

	@Test
	public void parseCreateWorksheetResponse_WhenResultSuccessFlagIsTrueThereAreResults_ThenReturnsFirstResult() throws Exception {
		String reference = "23423";
		when(mockCreateWorksheetResponse.getCreateWorksheetResult()).thenReturn(mockWebServiceCreateWorksheetResponse);
		when(mockWebServiceCreateWorksheetResponse.getSuccessFlag()).thenReturn(true);
		when(mockWebServiceCreateWorksheetResponse.getWorksheetResponse()).thenReturn(mockArrayOfanyType);
		when(mockArrayOfanyType.getAnyType()).thenReturn(new Object[] { reference, "" });

		String result = responseParserService.parseCreateWorksheetResponse(mockCreateWorksheetResponse);

		assertThat(result, equalTo(reference));
	}

	@Test(expected = WasteRetrievalException.class)
	public void parseGetServicesResponse_WhenResultSuccessFlagIsFalse_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockGetServicesResponse.getGetServicesResult()).thenReturn(mockServiceResponse);
		when(mockServiceResponse.getSuccessFlag()).thenReturn(false);

		responseParserService.parseGetServicesResponse(mockGetServicesResponse);
	}

	@Test
	public void parseGetServicesResponse_WhenResultSuccessFlagIsTrueAndServicesArrayObjectIsNull_ThenReturnsEmptyArray() throws Exception {
		when(mockGetServicesResponse.getGetServicesResult()).thenReturn(mockServiceResponse);
		when(mockServiceResponse.getSuccessFlag()).thenReturn(true);
		when(mockServiceResponse.getServices()).thenReturn(null);

		Service[] result = responseParserService.parseGetServicesResponse(mockGetServicesResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetServicesResponse_WhenResultSuccessFlagIsTrueAndServicesArrayIsNull_ThenReturnsEmptyArray() throws Exception {
		when(mockGetServicesResponse.getGetServicesResult()).thenReturn(mockServiceResponse);
		when(mockServiceResponse.getSuccessFlag()).thenReturn(true);
		when(mockServiceResponse.getServices()).thenReturn(mockArrayOfService);
		when(mockArrayOfService.getService()).thenReturn(null);

		Service[] result = responseParserService.parseGetServicesResponse(mockGetServicesResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetServicesResponse_WhenResultSuccessFlagIsTrue_ThenReturnsServices() throws Exception {
		when(mockGetServicesResponse.getGetServicesResult()).thenReturn(mockServiceResponse);
		when(mockServiceResponse.getSuccessFlag()).thenReturn(true);
		when(mockServiceResponse.getServices()).thenReturn(mockArrayOfService);
		when(mockArrayOfService.getService()).thenReturn(new Service[] { mockService });

		Service[] result = responseParserService.parseGetServicesResponse(mockGetServicesResponse);

		assertThat(result.length, equalTo(1));
		assertThat(result[0], sameInstance(mockService));
	}

	@Test(expected = WasteRetrievalException.class)
	public void parseGetServiceItemsResponse_WhenResultSuccessFlagIsFalse_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockGetServiceItemsResponse.getGetServiceItemsResult()).thenReturn(mockServiceItemResponse);
		when(mockServiceItemResponse.getSuccessFlag()).thenReturn(false);

		responseParserService.parseGetServiceItemsResponse(mockGetServiceItemsResponse);
	}

	@Test
	public void parseGetServiceItemsResponse_WhenResultSuccessFlagIsTrueAndServiceItemsArrayObjectIsNull_ThenReturnsEmptyArray() throws Exception {
		when(mockGetServiceItemsResponse.getGetServiceItemsResult()).thenReturn(mockServiceItemResponse);
		when(mockServiceItemResponse.getSuccessFlag()).thenReturn(true);
		when(mockServiceItemResponse.getServiceItems()).thenReturn(null);

		ServiceItem[] result = responseParserService.parseGetServiceItemsResponse(mockGetServiceItemsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetServiceItemsResponse_WhenResultSuccessFlagIsTrueAndArrayOfServiceItemsIsNull_ThenReturnsEmptyArray() throws Exception {
		when(mockGetServiceItemsResponse.getGetServiceItemsResult()).thenReturn(mockServiceItemResponse);
		when(mockServiceItemResponse.getSuccessFlag()).thenReturn(true);
		when(mockServiceItemResponse.getServiceItems()).thenReturn(mockArrayOfServiceItem);
		when(mockArrayOfServiceItem.getServiceItem()).thenReturn(null);

		ServiceItem[] result = responseParserService.parseGetServiceItemsResponse(mockGetServiceItemsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetServiceItemsResponse_WhenResultSuccessFlagIsTrue_ThenReturnsServiceItems() throws Exception {
		when(mockGetServiceItemsResponse.getGetServiceItemsResult()).thenReturn(mockServiceItemResponse);
		when(mockServiceItemResponse.getSuccessFlag()).thenReturn(true);
		when(mockServiceItemResponse.getServiceItems()).thenReturn(mockArrayOfServiceItem);
		when(mockArrayOfServiceItem.getServiceItem()).thenReturn(new ServiceItem[] { mockServiceItem });

		ServiceItem[] result = responseParserService.parseGetServiceItemsResponse(mockGetServiceItemsResponse);

		assertThat(result.length, equalTo(1));
		assertThat(result[0], sameInstance(mockServiceItem));
	}

	@Test
	public void parseGetSiteWorksheetsResponse_WhenResultSuccessFlagIsTrue_ThenReturnsWorksheets() throws WasteRetrievalException {
		when(mockGetSiteWorksheetsResponse.getGetSiteWorksheetsResult()).thenReturn(mockWorksheetResponse);
		when(mockWorksheetResponse.getSuccessFlag()).thenReturn(true);
		when(mockWorksheetResponse.getWorksheets()).thenReturn(mockArrayOfWorksheet);
		when(mockArrayOfWorksheet.getWorksheet()).thenReturn(new Worksheet[] { mockWorksheet });

		Worksheet[] result = responseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse);

		assertThat(result.length, equalTo(1));
		assertThat(result[0], sameInstance(mockWorksheet));
	}

	@Test
	public void parseGetSiteWorksheetsResponse_WhenResultSuccessFlagIsTrueAndArrayOfWorksheetsIsNull_ThenReturnsEmptyArray() throws WasteRetrievalException {
		when(mockGetSiteWorksheetsResponse.getGetSiteWorksheetsResult()).thenReturn(mockWorksheetResponse);
		when(mockWorksheetResponse.getSuccessFlag()).thenReturn(true);
		when(mockWorksheetResponse.getWorksheets()).thenReturn(mockArrayOfWorksheet);
		when(mockArrayOfWorksheet.getWorksheet()).thenReturn(null);

		Worksheet[] result = responseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test(expected = WasteRetrievalException.class)
	public void parseGetSiteWorksheetsResponse_WhenResultSuccessFlagIsFalse_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockGetSiteWorksheetsResponse.getGetSiteWorksheetsResult()).thenReturn(mockWorksheetResponse);
		when(mockWorksheetResponse.getSuccessFlag()).thenReturn(false);

		responseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse);
	}

	@Test
	public void parseGetSiteWorksheetsResponse_WhenResultSuccessFlagIsTrueAndWorksheetArrayObjectIsNull_ThenReturnsEmptyArray() throws Exception {
		when(mockGetSiteWorksheetsResponse.getGetSiteWorksheetsResult()).thenReturn(mockWorksheetResponse);
		when(mockWorksheetResponse.getSuccessFlag()).thenReturn(true);
		when(mockWorksheetResponse.getWorksheets()).thenReturn(null);

		Worksheet[] result = responseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetSiteWorksheetsResponse_WhenResultSuccessFlagIsFalseAndErrorCodeIsNoResults_ThenReturnsEmptyArray() throws Exception {
		when(mockGetSiteWorksheetsResponse.getGetSiteWorksheetsResult()).thenReturn(mockWorksheetResponse);
		when(mockWorksheetResponse.getSuccessFlag()).thenReturn(false);
		when(mockWorksheetResponse.getErrorCode()).thenReturn(WhitespaceWebServiceConstants.NO_RESULTS_ERROR_CODE);

		Worksheet[] result = responseParserService.parseGetSiteWorksheetsResponse(mockGetSiteWorksheetsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetWorksheetDetailServiceItemsResponse_WhenResultSuccessFlagIsTrue_ThenReturnsWorksheetServiceItems() throws WasteRetrievalException {
		when(mockGetWorksheetDetailServiceItemsResponse.getGetWorksheetDetailServiceItemsResult()).thenReturn(mockWorksheetServiceItemResponse);
		when(mockWorksheetServiceItemResponse.getSuccessFlag()).thenReturn(true);
		when(mockWorksheetServiceItemResponse.getWorksheetserviceitems()).thenReturn(mockArrayOfWorksheetServiceItem);
		when(mockArrayOfWorksheetServiceItem.getWorksheetServiceItem()).thenReturn(new WorksheetServiceItem[] { mockWorksheetServiceItem });

		WorksheetServiceItem[] result = responseParserService.parseGetWorksheetDetailServiceItemsResponse(mockGetWorksheetDetailServiceItemsResponse);

		assertThat(result.length, equalTo(1));
		assertThat(result[0], sameInstance(mockWorksheetServiceItem));
	}

	@Test
	public void parseGetWorksheetDetailServiceItemsResponse_WhenResultSuccessFlagIsTrueAndArrayOfWorksheetDetailsIsNull_ThenReturnsEmptyArray() throws WasteRetrievalException {
		when(mockGetWorksheetDetailServiceItemsResponse.getGetWorksheetDetailServiceItemsResult()).thenReturn(mockWorksheetServiceItemResponse);
		when(mockWorksheetServiceItemResponse.getSuccessFlag()).thenReturn(true);
		when(mockWorksheetServiceItemResponse.getWorksheetserviceitems()).thenReturn(mockArrayOfWorksheetServiceItem);
		when(mockArrayOfWorksheetServiceItem.getWorksheetServiceItem()).thenReturn(null);

		WorksheetServiceItem[] result = responseParserService.parseGetWorksheetDetailServiceItemsResponse(mockGetWorksheetDetailServiceItemsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetWorksheetDetailServiceItemsResponse_WhenResultSuccessFlagIsFalseAndErrorCodeIsNoResults_ThenReturnsEmptyArray() throws Exception {
		when(mockGetWorksheetDetailServiceItemsResponse.getGetWorksheetDetailServiceItemsResult()).thenReturn(mockWorksheetServiceItemResponse);
		when(mockWorksheetServiceItemResponse.getSuccessFlag()).thenReturn(false);
		when(mockWorksheetServiceItemResponse.getErrorCode()).thenReturn(WhitespaceWebServiceConstants.NO_RESULTS_ERROR_CODE);

		WorksheetServiceItem[] result = responseParserService.parseGetWorksheetDetailServiceItemsResponse(mockGetWorksheetDetailServiceItemsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test(expected = WasteRetrievalException.class)
	public void parseGetWorksheetDetailServiceItemsResponse_WhenResultSuccessFlagIsFalseAndErrorCodeIsDifferentFromNoResults_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockGetWorksheetDetailServiceItemsResponse.getGetWorksheetDetailServiceItemsResult()).thenReturn(mockWorksheetServiceItemResponse);
		when(mockWorksheetServiceItemResponse.getSuccessFlag()).thenReturn(false);

		responseParserService.parseGetWorksheetDetailServiceItemsResponse(mockGetWorksheetDetailServiceItemsResponse);
	}

	@Test
	public void parseGetWorksheetDetailServiceItemsResponse_WhenResultSuccessFlagIsTrueAndWorksheetDetailsArrayObjectIsNull_ThenReturnsEmptyArray() throws Exception {
		when(mockGetWorksheetDetailServiceItemsResponse.getGetWorksheetDetailServiceItemsResult()).thenReturn(mockWorksheetServiceItemResponse);
		when(mockWorksheetServiceItemResponse.getSuccessFlag()).thenReturn(true);
		when(mockWorksheetServiceItemResponse.getWorksheetserviceitems()).thenReturn(null);

		responseParserService.parseGetWorksheetDetailServiceItemsResponse(mockGetWorksheetDetailServiceItemsResponse);

		WorksheetServiceItem[] result = responseParserService.parseGetWorksheetDetailServiceItemsResponse(mockGetWorksheetDetailServiceItemsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetSiteCollectionsResponse_WhenResultSuccessFlagIsTrue_ThenReturnsSiteServices() throws WasteRetrievalException {
		when(mockGetSiteCollectionsResponse.getGetSiteCollectionsResult()).thenReturn(mockSiteServiceResponse);
		when(mockSiteServiceResponse.getSuccessFlag()).thenReturn(true);
		when(mockSiteServiceResponse.getSiteServices()).thenReturn(mockArrayOfSiteService);
		when(mockArrayOfSiteService.getSiteService()).thenReturn(new SiteService[] { mockSiteService });

		SiteService[] result = responseParserService.parseGetSiteCollectionsResponse(mockGetSiteCollectionsResponse);

		assertThat(result.length, equalTo(1));
		assertThat(result[0], sameInstance(mockSiteService));
	}

	@Test
	public void parseGetSiteCollectionsResponse_WhenResultSuccessFlagIsTrueAndArrayOfSiteServicesIsNull_ThenReturnsEmptyArray() throws WasteRetrievalException {
		when(mockGetSiteCollectionsResponse.getGetSiteCollectionsResult()).thenReturn(mockSiteServiceResponse);
		when(mockSiteServiceResponse.getSuccessFlag()).thenReturn(true);
		when(mockSiteServiceResponse.getSiteServices()).thenReturn(mockArrayOfSiteService);
		when(mockArrayOfSiteService.getSiteService()).thenReturn(null);

		SiteService[] result = responseParserService.parseGetSiteCollectionsResponse(mockGetSiteCollectionsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test(expected = WasteRetrievalException.class)
	public void parseGetSiteCollectionsResponse_WhenResultSuccessFlagIsFalseAndErrorCodeIsNotNoResults_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockGetSiteCollectionsResponse.getGetSiteCollectionsResult()).thenReturn(mockSiteServiceResponse);
		when(mockSiteServiceResponse.getSuccessFlag()).thenReturn(false);
		when(mockSiteServiceResponse.getErrorCode()).thenReturn(0);

		responseParserService.parseGetSiteCollectionsResponse(mockGetSiteCollectionsResponse);
	}

	@Test
	public void parseGetSiteCollectionsResponse_WhenResultSuccessFlagIsFalseAndErrorCodeIsNoResults_ThenReturnsEmptyArray() throws Exception {
		when(mockGetSiteCollectionsResponse.getGetSiteCollectionsResult()).thenReturn(mockSiteServiceResponse);
		when(mockSiteServiceResponse.getSuccessFlag()).thenReturn(false);
		when(mockSiteServiceResponse.getErrorCode()).thenReturn(WhitespaceWebServiceConstants.NO_RESULTS_ERROR_CODE);

		SiteService[] result = responseParserService.parseGetSiteCollectionsResponse(mockGetSiteCollectionsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetSiteCollectionsResponse_WhenResultSuccessFlagIsTrueAndSiteServicesArrayObjectIsNull_ThenReturnsEmptyArray() throws Exception {
		when(mockGetSiteCollectionsResponse.getGetSiteCollectionsResult()).thenReturn(mockSiteServiceResponse);
		when(mockSiteServiceResponse.getSuccessFlag()).thenReturn(true);
		when(mockSiteServiceResponse.getSiteServices()).thenReturn(null);

		SiteService[] result = responseParserService.parseGetSiteCollectionsResponse(mockGetSiteCollectionsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetCollectionSlotsResponse_WhenResponseIsSuccessAndContainsArrayOfAdHocRounds_ThenReturnsArrayOfAdHocRounds() throws Exception {
		when(mockGetCollectionSlotsResponse.getGetCollectionSlotsResult()).thenReturn(mockAdHocRoundInstanceResponse);
		when(mockAdHocRoundInstanceResponse.getSuccessFlag()).thenReturn(true);
		when(mockAdHocRoundInstanceResponse.getApiAdHocRoundInstances()).thenReturn(mockArrayOfApiAdHocRoundInstance);
		when(mockArrayOfApiAdHocRoundInstance.getApiAdHocRoundInstance()).thenReturn(new ApiAdHocRoundInstance[] { mockApiAdHocRoundInstance });

		ApiAdHocRoundInstance[] result = responseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse);

		assertThat(result.length, equalTo(1));
		assertThat(result[0], sameInstance(mockApiAdHocRoundInstance));
	}

	@Test
	public void parseGetCollectionSlotsResponse_WhenResponseIsSuccessAndContainsNullArrayOfAdHocRounds_ThenReturnsEmptyArray() throws Exception {
		when(mockGetCollectionSlotsResponse.getGetCollectionSlotsResult()).thenReturn(mockAdHocRoundInstanceResponse);
		when(mockAdHocRoundInstanceResponse.getSuccessFlag()).thenReturn(true);
		when(mockAdHocRoundInstanceResponse.getApiAdHocRoundInstances()).thenReturn(mockArrayOfApiAdHocRoundInstance);
		when(mockArrayOfApiAdHocRoundInstance.getApiAdHocRoundInstance()).thenReturn(null);

		ApiAdHocRoundInstance[] result = responseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetCollectionSlotsResponse_WhenResponseIsSuccessAndContainsNullArrayOObject_ThenReturnsEmptyArray() throws Exception {
		when(mockGetCollectionSlotsResponse.getGetCollectionSlotsResult()).thenReturn(mockAdHocRoundInstanceResponse);
		when(mockAdHocRoundInstanceResponse.getSuccessFlag()).thenReturn(true);
		when(mockAdHocRoundInstanceResponse.getApiAdHocRoundInstances()).thenReturn(null);

		ApiAdHocRoundInstance[] result = responseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void parseGetCollectionSlotsResponse_WhenResponseIsNotSuccessErrorCodeIsNoResults_ThenReturnsEmptyArray() throws Exception {
		when(mockGetCollectionSlotsResponse.getGetCollectionSlotsResult()).thenReturn(mockAdHocRoundInstanceResponse);
		when(mockAdHocRoundInstanceResponse.getSuccessFlag()).thenReturn(false);
		when(mockAdHocRoundInstanceResponse.getErrorCode()).thenReturn(WhitespaceWebServiceConstants.NO_RESULTS_ERROR_CODE);

		ApiAdHocRoundInstance[] result = responseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse);

		assertThat(result.length, equalTo(0));
	}

	@Test(expected = WasteRetrievalException.class)
	public void parseGetCollectionSlotsResponse_WhenResponseIsNotSuccessAndErrorCodeIsNotNoResults_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockGetCollectionSlotsResponse.getGetCollectionSlotsResult()).thenReturn(mockAdHocRoundInstanceResponse);
		when(mockAdHocRoundInstanceResponse.getSuccessFlag()).thenReturn(false);
		when(mockAdHocRoundInstanceResponse.getErrorCode()).thenReturn(100);

		responseParserService.parseGetCollectionSlotsResponse(mockGetCollectionSlotsResponse);
	}
}
