/**
 * JobSoapBasic.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package axis2.apache.org;

/** JobSoapBasic bean class */
@SuppressWarnings({"unchecked", "unused"})
public class JobSoapBasic extends axis2.apache.org.BaseSoap
    implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = JobSoapBasic
  Namespace URI =
  Namespace Prefix =
  */

  /** field for Id */
  protected int localId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIdTracker = false;

  public boolean isIdSpecified() {
    return localIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getId() {
    return localId;
  }

  /**
   * Auto generated setter method
   *
   * @param param Id
   */
  public void setId(int param) {

    // setting primitive attribute tracker to true
    localIdTracker = param != java.lang.Integer.MIN_VALUE;

    this.localId = param;
  }

  /** field for Reference */
  protected java.lang.String localReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localReferenceTracker = false;

  public boolean isReferenceSpecified() {
    return localReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getReference() {
    return localReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param Reference
   */
  public void setReference(java.lang.String param) {
    localReferenceTracker = true;

    this.localReference = param;
  }

  /** field for Title */
  protected java.lang.String localTitle;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTitleTracker = false;

  public boolean isTitleSpecified() {
    return localTitleTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTitle() {
    return localTitle;
  }

  /**
   * Auto generated setter method
   *
   * @param param Title
   */
  public void setTitle(java.lang.String param) {
    localTitleTracker = true;

    this.localTitle = param;
  }

  /** field for Tags */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .arrays.ArrayOfstring
      localTags;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTagsTracker = false;

  public boolean isTagsSpecified() {
    return localTagsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfstring
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .arrays.ArrayOfstring
      getTags() {
    return localTags;
  }

  /**
   * Auto generated setter method
   *
   * @param param Tags
   */
  public void setTags(
      com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays
              .ArrayOfstring
          param) {
    localTagsTracker = true;

    this.localTags = param;
  }

  /** field for DateCreated */
  protected java.util.Calendar localDateCreated;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDateCreatedTracker = false;

  public boolean isDateCreatedSpecified() {
    return localDateCreatedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateCreated() {
    return localDateCreated;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateCreated
   */
  public void setDateCreated(java.util.Calendar param) {
    localDateCreatedTracker = param != null;

    this.localDateCreated = param;
  }

  /** field for DateModified */
  protected java.util.Calendar localDateModified;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDateModifiedTracker = false;

  public boolean isDateModifiedSpecified() {
    return localDateModifiedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateModified() {
    return localDateModified;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateModified
   */
  public void setDateModified(java.util.Calendar param) {
    localDateModifiedTracker = true;

    this.localDateModified = param;
  }

  /** field for DispatchDate */
  protected java.util.Calendar localDispatchDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDispatchDateTracker = false;

  public boolean isDispatchDateSpecified() {
    return localDispatchDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDispatchDate() {
    return localDispatchDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param DispatchDate
   */
  public void setDispatchDate(java.util.Calendar param) {
    localDispatchDateTracker = true;

    this.localDispatchDate = param;
  }

  /** field for DueDate */
  protected java.util.Calendar localDueDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDueDateTracker = false;

  public boolean isDueDateSpecified() {
    return localDueDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDueDate() {
    return localDueDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param DueDate
   */
  public void setDueDate(java.util.Calendar param) {
    localDueDateTracker = true;

    this.localDueDate = param;
  }

  /** field for IconResourceKey */
  protected java.lang.String localIconResourceKey;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIconResourceKeyTracker = false;

  public boolean isIconResourceKeySpecified() {
    return localIconResourceKeyTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getIconResourceKey() {
    return localIconResourceKey;
  }

  /**
   * Auto generated setter method
   *
   * @param param IconResourceKey
   */
  public void setIconResourceKey(java.lang.String param) {
    localIconResourceKeyTracker = true;

    this.localIconResourceKey = param;
  }

  /** field for IsPublished */
  protected boolean localIsPublished;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsPublishedTracker = false;

  public boolean isIsPublishedSpecified() {
    return localIsPublishedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsPublished() {
    return localIsPublished;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsPublished
   */
  public void setIsPublished(boolean param) {
    localIsPublishedTracker = true;

    this.localIsPublished = param;
  }

  /** field for CurrentStatus */
  protected java.lang.String localCurrentStatus;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCurrentStatusTracker = false;

  public boolean isCurrentStatusSpecified() {
    return localCurrentStatusTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getCurrentStatus() {
    return localCurrentStatus;
  }

  /**
   * Auto generated setter method
   *
   * @param param CurrentStatus
   */
  public void setCurrentStatus(java.lang.String param) {
    localCurrentStatusTracker = true;

    this.localCurrentStatus = param;
  }

  /** field for ConfirmSubmit */
  protected boolean localConfirmSubmit;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localConfirmSubmitTracker = false;

  public boolean isConfirmSubmitSpecified() {
    return localConfirmSubmitTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getConfirmSubmit() {
    return localConfirmSubmit;
  }

  /**
   * Auto generated setter method
   *
   * @param param ConfirmSubmit
   */
  public void setConfirmSubmit(boolean param) {

    // setting primitive attribute tracker to true
    localConfirmSubmitTracker = true;

    this.localConfirmSubmit = param;
  }

  /** field for IsDeleted */
  protected boolean localIsDeleted;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsDeletedTracker = false;

  public boolean isIsDeletedSpecified() {
    return localIsDeletedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsDeleted() {
    return localIsDeleted;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsDeleted
   */
  public void setIsDeleted(boolean param) {

    // setting primitive attribute tracker to true
    localIsDeletedTracker = true;

    this.localIsDeleted = param;
  }

  /** field for CompletedBy */
  protected java.lang.String localCompletedBy;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCompletedByTracker = false;

  public boolean isCompletedBySpecified() {
    return localCompletedByTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getCompletedBy() {
    return localCompletedBy;
  }

  /**
   * Auto generated setter method
   *
   * @param param CompletedBy
   */
  public void setCompletedBy(java.lang.String param) {
    localCompletedByTracker = true;

    this.localCompletedBy = param;
  }

  /** field for ParentId */
  protected int localParentId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localParentIdTracker = false;

  public boolean isParentIdSpecified() {
    return localParentIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getParentId() {
    return localParentId;
  }

  /**
   * Auto generated setter method
   *
   * @param param ParentId
   */
  public void setParentId(int param) {
    localParentIdTracker = true;

    this.localParentId = param;
  }

  /** field for DependencyKey */
  protected java.lang.String localDependencyKey;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDependencyKeyTracker = false;

  public boolean isDependencyKeySpecified() {
    return localDependencyKeyTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getDependencyKey() {
    return localDependencyKey;
  }

  /**
   * Auto generated setter method
   *
   * @param param DependencyKey
   */
  public void setDependencyKey(java.lang.String param) {
    localDependencyKeyTracker = true;

    this.localDependencyKey = param;
  }

  /** field for IsTemplate */
  protected boolean localIsTemplate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsTemplateTracker = false;

  public boolean isIsTemplateSpecified() {
    return localIsTemplateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsTemplate() {
    return localIsTemplate;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsTemplate
   */
  public void setIsTemplate(boolean param) {

    // setting primitive attribute tracker to true
    localIsTemplateTracker = true;

    this.localIsTemplate = param;
  }

  /** field for Expires */
  protected java.util.Calendar localExpires;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExpiresTracker = false;

  public boolean isExpiresSpecified() {
    return localExpiresTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getExpires() {
    return localExpires;
  }

  /**
   * Auto generated setter method
   *
   * @param param Expires
   */
  public void setExpires(java.util.Calendar param) {
    localExpiresTracker = true;

    this.localExpires = param;
  }

  /** field for IsArchived */
  protected boolean localIsArchived;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsArchivedTracker = false;

  public boolean isIsArchivedSpecified() {
    return localIsArchivedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsArchived() {
    return localIsArchived;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsArchived
   */
  public void setIsArchived(boolean param) {

    // setting primitive attribute tracker to true
    localIsArchivedTracker = true;

    this.localIsArchived = param;
  }

  /** field for DateArchived */
  protected java.util.Calendar localDateArchived;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDateArchivedTracker = false;

  public boolean isDateArchivedSpecified() {
    return localDateArchivedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateArchived() {
    return localDateArchived;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateArchived
   */
  public void setDateArchived(java.util.Calendar param) {
    localDateArchivedTracker = true;

    this.localDateArchived = param;
  }

  /** field for IsCompleted */
  protected boolean localIsCompleted;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsCompletedTracker = false;

  public boolean isIsCompletedSpecified() {
    return localIsCompletedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsCompleted() {
    return localIsCompleted;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsCompleted
   */
  public void setIsCompleted(boolean param) {

    // setting primitive attribute tracker to true
    localIsCompletedTracker = true;

    this.localIsCompleted = param;
  }

  /** field for DateCompleted */
  protected java.util.Calendar localDateCompleted;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDateCompletedTracker = false;

  public boolean isDateCompletedSpecified() {
    return localDateCompletedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateCompleted() {
    return localDateCompleted;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateCompleted
   */
  public void setDateCompleted(java.util.Calendar param) {
    localDateCompletedTracker = true;

    this.localDateCompleted = param;
  }

  /** field for WorkLocation */
  protected axis2.apache.org.JobSoapWorkLocation localWorkLocation;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkLocationTracker = false;

  public boolean isWorkLocationSpecified() {
    return localWorkLocationTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return axis2.apache.org.JobSoapWorkLocation
   */
  public axis2.apache.org.JobSoapWorkLocation getWorkLocation() {
    return localWorkLocation;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkLocation
   */
  public void setWorkLocation(axis2.apache.org.JobSoapWorkLocation param) {
    localWorkLocationTracker = true;

    this.localWorkLocation = param;
  }

  /** field for UsersOnJob */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .arrays.ArrayOfstring
      localUsersOnJob;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUsersOnJobTracker = false;

  public boolean isUsersOnJobSpecified() {
    return localUsersOnJobTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfstring
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .arrays.ArrayOfstring
      getUsersOnJob() {
    return localUsersOnJob;
  }

  /**
   * Auto generated setter method
   *
   * @param param UsersOnJob
   */
  public void setUsersOnJob(
      com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays
              .ArrayOfstring
          param) {
    localUsersOnJobTracker = true;

    this.localUsersOnJob = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    java.lang.String namespacePrefix = registerPrefix(xmlWriter, "");
    if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
      writeAttribute(
          "xsi",
          "http://www.w3.org/2001/XMLSchema-instance",
          "type",
          namespacePrefix + ":JobSoapBasic",
          xmlWriter);
    } else {
      writeAttribute(
          "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "JobSoapBasic", xmlWriter);
    }

    if (localIdTracker) {
      namespace = "";
      writeStartElement(null, namespace, "Id", xmlWriter);

      if (localId == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("Id cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
      }

      xmlWriter.writeEndElement();
    }
    if (localReferenceTracker) {
      namespace = "";
      writeStartElement(null, namespace, "Reference", xmlWriter);

      if (localReference == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localTitleTracker) {
      namespace = "";
      writeStartElement(null, namespace, "Title", xmlWriter);

      if (localTitle == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localTitle);
      }

      xmlWriter.writeEndElement();
    }
    if (localTagsTracker) {
      if (localTags == null) {

        writeStartElement(null, "", "Tags", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localTags.serialize(new javax.xml.namespace.QName("", "Tags"), xmlWriter);
      }
    }
    if (localDateCreatedTracker) {
      namespace = "";
      writeStartElement(null, namespace, "DateCreated", xmlWriter);

      if (localDateCreated == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("DateCreated cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateCreated));
      }

      xmlWriter.writeEndElement();
    }
    if (localDateModifiedTracker) {
      namespace = "";
      writeStartElement(null, namespace, "DateModified", xmlWriter);

      if (localDateModified == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateModified));
      }

      xmlWriter.writeEndElement();
    }
    if (localDispatchDateTracker) {
      namespace = "";
      writeStartElement(null, namespace, "DispatchDate", xmlWriter);

      if (localDispatchDate == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDispatchDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localDueDateTracker) {
      namespace = "";
      writeStartElement(null, namespace, "DueDate", xmlWriter);

      if (localDueDate == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDueDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localIconResourceKeyTracker) {
      namespace = "";
      writeStartElement(null, namespace, "IconResourceKey", xmlWriter);

      if (localIconResourceKey == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localIconResourceKey);
      }

      xmlWriter.writeEndElement();
    }
    if (localIsPublishedTracker) {
      namespace = "";
      writeStartElement(null, namespace, "IsPublished", xmlWriter);

      if (false) {

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsPublished));
      }

      xmlWriter.writeEndElement();
    }
    if (localCurrentStatusTracker) {
      namespace = "";
      writeStartElement(null, namespace, "CurrentStatus", xmlWriter);

      if (localCurrentStatus == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localCurrentStatus);
      }

      xmlWriter.writeEndElement();
    }
    if (localConfirmSubmitTracker) {
      namespace = "";
      writeStartElement(null, namespace, "ConfirmSubmit", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ConfirmSubmit cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localConfirmSubmit));
      }

      xmlWriter.writeEndElement();
    }
    if (localIsDeletedTracker) {
      namespace = "";
      writeStartElement(null, namespace, "IsDeleted", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("IsDeleted cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDeleted));
      }

      xmlWriter.writeEndElement();
    }
    if (localCompletedByTracker) {
      namespace = "";
      writeStartElement(null, namespace, "CompletedBy", xmlWriter);

      if (localCompletedBy == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localCompletedBy);
      }

      xmlWriter.writeEndElement();
    }
    if (localParentIdTracker) {
      namespace = "";
      writeStartElement(null, namespace, "ParentId", xmlWriter);

      if (localParentId == java.lang.Integer.MIN_VALUE) {

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParentId));
      }

      xmlWriter.writeEndElement();
    }
    if (localDependencyKeyTracker) {
      namespace = "";
      writeStartElement(null, namespace, "DependencyKey", xmlWriter);

      if (localDependencyKey == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localDependencyKey);
      }

      xmlWriter.writeEndElement();
    }
    if (localIsTemplateTracker) {
      namespace = "";
      writeStartElement(null, namespace, "IsTemplate", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("IsTemplate cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTemplate));
      }

      xmlWriter.writeEndElement();
    }
    if (localExpiresTracker) {
      namespace = "";
      writeStartElement(null, namespace, "Expires", xmlWriter);

      if (localExpires == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpires));
      }

      xmlWriter.writeEndElement();
    }
    if (localIsArchivedTracker) {
      namespace = "";
      writeStartElement(null, namespace, "IsArchived", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("IsArchived cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsArchived));
      }

      xmlWriter.writeEndElement();
    }
    if (localDateArchivedTracker) {
      namespace = "";
      writeStartElement(null, namespace, "DateArchived", xmlWriter);

      if (localDateArchived == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateArchived));
      }

      xmlWriter.writeEndElement();
    }
    if (localIsCompletedTracker) {
      namespace = "";
      writeStartElement(null, namespace, "IsCompleted", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("IsCompleted cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsCompleted));
      }

      xmlWriter.writeEndElement();
    }
    if (localDateCompletedTracker) {
      namespace = "";
      writeStartElement(null, namespace, "DateCompleted", xmlWriter);

      if (localDateCompleted == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateCompleted));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkLocationTracker) {
      if (localWorkLocation == null) {

        writeStartElement(null, "", "WorkLocation", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localWorkLocation.serialize(new javax.xml.namespace.QName("", "WorkLocation"), xmlWriter);
      }
    }
    if (localUsersOnJobTracker) {
      if (localUsersOnJob == null) {

        writeStartElement(null, "", "UsersOnJob", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localUsersOnJob.serialize(new javax.xml.namespace.QName("", "UsersOnJob"), xmlWriter);
      }
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("")) {
      return "";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static JobSoapBasic parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      JobSoapBasic object = new JobSoapBasic();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"JobSoapBasic".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (JobSoapBasic)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "Id").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Id" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setId(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "Reference").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setReference(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "Title").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setTitle(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "Tags").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setTags(null);
            reader.next();

            reader.next();

          } else {

            object.setTags(
                com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas
                    .serialization.arrays.ArrayOfstring.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "DateCreated").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateCreated" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateCreated(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "DateModified").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setDateModified(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "DispatchDate").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setDispatchDate(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "DueDate").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setDueDate(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "IconResourceKey").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setIconResourceKey(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "IsPublished").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setIsPublished(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "CurrentStatus").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setCurrentStatus(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "ConfirmSubmit").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ConfirmSubmit" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setConfirmSubmit(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "IsDeleted").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IsDeleted" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIsDeleted(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "CompletedBy").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setCompletedBy(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "ParentId").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setParentId(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          } else {

            object.setParentId(java.lang.Integer.MIN_VALUE);

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

          object.setParentId(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "DependencyKey").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setDependencyKey(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "IsTemplate").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IsTemplate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIsTemplate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "Expires").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setExpires(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "IsArchived").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IsArchived" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIsArchived(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "DateArchived").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setDateArchived(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "IsCompleted").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IsCompleted" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIsCompleted(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "DateCompleted").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setDateCompleted(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "WorkLocation").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setWorkLocation(null);
            reader.next();

            reader.next();

          } else {

            object.setWorkLocation(axis2.apache.org.JobSoapWorkLocation.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("", "UsersOnJob").equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setUsersOnJob(null);
            reader.next();

            reader.next();

          } else {

            object.setUsersOnJob(
                com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas
                    .serialization.arrays.ArrayOfstring.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
