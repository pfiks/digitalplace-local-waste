
package com.placecube.digitalplace.local.waste.whitespace.util;

import java.text.ParsePosition;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class WhitespaceDateUtil {

	private static DateTimeFormatter dd_MM_yyyy_formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	private static DateTimeFormatter yyyy_MM_dd_formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public static String formatDate_dd_MM_yyyy(LocalDate sourceDate) {
		return sourceDate.format(dd_MM_yyyy_formatter);
	}

	public static String formatDate_from_yyyy_MM_dd_to_dd_MM_yyyy(String sourceDate) {
		return dd_MM_yyyy_formatter.format(yyyy_MM_dd_formatter.parse(sourceDate));
	}

	public static LocalDate parseDate_dd_MM_yyyy(String sourceDate) {
		TemporalAccessor parse = dd_MM_yyyy_formatter.parse(sourceDate, new ParsePosition(0));
		return LocalDate.from(parse);
	}

}
