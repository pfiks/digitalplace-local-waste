package com.placecube.digitalplace.local.waste.whitespace.model.validator;

import java.time.Clock;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

public class SameFinancialYearValidator implements WorksheetValidator {

	private WhitespaceWasteServiceUtil whitespaceWasteServiceUtil;

	public SameFinancialYearValidator(WhitespaceWasteServiceUtil whitespaceWasteServiceUtil) {
		this.whitespaceWasteServiceUtil = whitespaceWasteServiceUtil;
	}

	@Override
	public boolean isValid(Worksheet worksheet) {
		return !whitespaceWasteServiceUtil.isCurrentFinancialYear(Clock.systemDefaultZone(), worksheet);
	}
}
