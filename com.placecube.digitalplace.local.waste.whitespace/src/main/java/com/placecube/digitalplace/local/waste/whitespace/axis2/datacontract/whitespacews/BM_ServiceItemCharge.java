/**
 * BM_ServiceItemCharge.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** BM_ServiceItemCharge bean class */
@SuppressWarnings({"unchecked", "unused"})
public class BM_ServiceItemCharge implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = BM_ServiceItemCharge
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for ServiceItemChargeID */
  protected int localServiceItemChargeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemChargeIDTracker = false;

  public boolean isServiceItemChargeIDSpecified() {
    return localServiceItemChargeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceItemChargeID() {
    return localServiceItemChargeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemChargeID
   */
  public void setServiceItemChargeID(int param) {

    // setting primitive attribute tracker to true
    localServiceItemChargeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceItemChargeID = param;
  }

  /** field for ServiceID */
  protected int localServiceID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIDTracker = false;

  public boolean isServiceIDSpecified() {
    return localServiceIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceID() {
    return localServiceID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceID
   */
  public void setServiceID(int param) {

    // setting primitive attribute tracker to true
    localServiceIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceID = param;
  }

  /** field for ServiceName */
  protected java.lang.String localServiceName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceNameTracker = false;

  public boolean isServiceNameSpecified() {
    return localServiceNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceName() {
    return localServiceName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceName
   */
  public void setServiceName(java.lang.String param) {
    localServiceNameTracker = true;

    this.localServiceName = param;
  }

  /** field for ServiceItemID */
  protected int localServiceItemID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemIDTracker = false;

  public boolean isServiceItemIDSpecified() {
    return localServiceItemIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceItemID() {
    return localServiceItemID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemID
   */
  public void setServiceItemID(int param) {

    // setting primitive attribute tracker to true
    localServiceItemIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceItemID = param;
  }

  /** field for ServiceItemName */
  protected java.lang.String localServiceItemName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemNameTracker = false;

  public boolean isServiceItemNameSpecified() {
    return localServiceItemNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemName() {
    return localServiceItemName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemName
   */
  public void setServiceItemName(java.lang.String param) {
    localServiceItemNameTracker = true;

    this.localServiceItemName = param;
  }

  /** field for ServiceChargeTypeID */
  protected int localServiceChargeTypeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceChargeTypeIDTracker = false;

  public boolean isServiceChargeTypeIDSpecified() {
    return localServiceChargeTypeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceChargeTypeID() {
    return localServiceChargeTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceChargeTypeID
   */
  public void setServiceChargeTypeID(int param) {

    // setting primitive attribute tracker to true
    localServiceChargeTypeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceChargeTypeID = param;
  }

  /** field for ServiceChargeTypeName */
  protected java.lang.String localServiceChargeTypeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceChargeTypeNameTracker = false;

  public boolean isServiceChargeTypeNameSpecified() {
    return localServiceChargeTypeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceChargeTypeName() {
    return localServiceChargeTypeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceChargeTypeName
   */
  public void setServiceChargeTypeName(java.lang.String param) {
    localServiceChargeTypeNameTracker = true;

    this.localServiceChargeTypeName = param;
  }

  /** field for ServiceItemChargeAmount */
  protected java.math.BigDecimal localServiceItemChargeAmount;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemChargeAmountTracker = false;

  public boolean isServiceItemChargeAmountSpecified() {
    return localServiceItemChargeAmountTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getServiceItemChargeAmount() {
    return localServiceItemChargeAmount;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemChargeAmount
   */
  public void setServiceItemChargeAmount(java.math.BigDecimal param) {
    localServiceItemChargeAmountTracker = param != null;

    this.localServiceItemChargeAmount = param;
  }

  /** field for ServiceItemCostAmount */
  protected java.math.BigDecimal localServiceItemCostAmount;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemCostAmountTracker = false;

  public boolean isServiceItemCostAmountSpecified() {
    return localServiceItemCostAmountTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getServiceItemCostAmount() {
    return localServiceItemCostAmount;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemCostAmount
   */
  public void setServiceItemCostAmount(java.math.BigDecimal param) {
    localServiceItemCostAmountTracker = param != null;

    this.localServiceItemCostAmount = param;
  }

  /** field for ServiceItemChargeIsOneOff */
  protected boolean localServiceItemChargeIsOneOff;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemChargeIsOneOffTracker = false;

  public boolean isServiceItemChargeIsOneOffSpecified() {
    return localServiceItemChargeIsOneOffTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceItemChargeIsOneOff() {
    return localServiceItemChargeIsOneOff;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemChargeIsOneOff
   */
  public void setServiceItemChargeIsOneOff(boolean param) {

    // setting primitive attribute tracker to true
    localServiceItemChargeIsOneOffTracker = true;

    this.localServiceItemChargeIsOneOff = param;
  }

  /** field for ServiceItemChargeScheduleID */
  protected int localServiceItemChargeScheduleID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemChargeScheduleIDTracker = false;

  public boolean isServiceItemChargeScheduleIDSpecified() {
    return localServiceItemChargeScheduleIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceItemChargeScheduleID() {
    return localServiceItemChargeScheduleID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemChargeScheduleID
   */
  public void setServiceItemChargeScheduleID(int param) {

    // setting primitive attribute tracker to true
    localServiceItemChargeScheduleIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceItemChargeScheduleID = param;
  }

  /** field for ServiceItemChargeScheduleName */
  protected java.lang.String localServiceItemChargeScheduleName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemChargeScheduleNameTracker = false;

  public boolean isServiceItemChargeScheduleNameSpecified() {
    return localServiceItemChargeScheduleNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemChargeScheduleName() {
    return localServiceItemChargeScheduleName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemChargeScheduleName
   */
  public void setServiceItemChargeScheduleName(java.lang.String param) {
    localServiceItemChargeScheduleNameTracker = true;

    this.localServiceItemChargeScheduleName = param;
  }

  /** field for ServiceItemChargeValidFrom */
  protected java.util.Calendar localServiceItemChargeValidFrom;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemChargeValidFromTracker = false;

  public boolean isServiceItemChargeValidFromSpecified() {
    return localServiceItemChargeValidFromTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getServiceItemChargeValidFrom() {
    return localServiceItemChargeValidFrom;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemChargeValidFrom
   */
  public void setServiceItemChargeValidFrom(java.util.Calendar param) {
    localServiceItemChargeValidFromTracker = param != null;

    this.localServiceItemChargeValidFrom = param;
  }

  /** field for ServiceItemChargeValidTo */
  protected java.util.Calendar localServiceItemChargeValidTo;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemChargeValidToTracker = false;

  public boolean isServiceItemChargeValidToSpecified() {
    return localServiceItemChargeValidToTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getServiceItemChargeValidTo() {
    return localServiceItemChargeValidTo;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemChargeValidTo
   */
  public void setServiceItemChargeValidTo(java.util.Calendar param) {
    localServiceItemChargeValidToTracker = param != null;

    this.localServiceItemChargeValidTo = param;
  }

  /** field for FMSNominalCodeID */
  protected int localFMSNominalCodeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFMSNominalCodeIDTracker = false;

  public boolean isFMSNominalCodeIDSpecified() {
    return localFMSNominalCodeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getFMSNominalCodeID() {
    return localFMSNominalCodeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param FMSNominalCodeID
   */
  public void setFMSNominalCodeID(int param) {

    // setting primitive attribute tracker to true
    localFMSNominalCodeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localFMSNominalCodeID = param;
  }

  /** field for FMSNominalCodeName */
  protected java.lang.String localFMSNominalCodeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFMSNominalCodeNameTracker = false;

  public boolean isFMSNominalCodeNameSpecified() {
    return localFMSNominalCodeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getFMSNominalCodeName() {
    return localFMSNominalCodeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param FMSNominalCodeName
   */
  public void setFMSNominalCodeName(java.lang.String param) {
    localFMSNominalCodeNameTracker = true;

    this.localFMSNominalCodeName = param;
  }

  /** field for FMSNominalCodeDescription */
  protected java.lang.String localFMSNominalCodeDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFMSNominalCodeDescriptionTracker = false;

  public boolean isFMSNominalCodeDescriptionSpecified() {
    return localFMSNominalCodeDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getFMSNominalCodeDescription() {
    return localFMSNominalCodeDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param FMSNominalCodeDescription
   */
  public void setFMSNominalCodeDescription(java.lang.String param) {
    localFMSNominalCodeDescriptionTracker = true;

    this.localFMSNominalCodeDescription = param;
  }

  /** field for FMSProductCodeID */
  protected int localFMSProductCodeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFMSProductCodeIDTracker = false;

  public boolean isFMSProductCodeIDSpecified() {
    return localFMSProductCodeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getFMSProductCodeID() {
    return localFMSProductCodeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param FMSProductCodeID
   */
  public void setFMSProductCodeID(int param) {

    // setting primitive attribute tracker to true
    localFMSProductCodeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localFMSProductCodeID = param;
  }

  /** field for FMSProductCodeName */
  protected java.lang.String localFMSProductCodeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFMSProductCodeNameTracker = false;

  public boolean isFMSProductCodeNameSpecified() {
    return localFMSProductCodeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getFMSProductCodeName() {
    return localFMSProductCodeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param FMSProductCodeName
   */
  public void setFMSProductCodeName(java.lang.String param) {
    localFMSProductCodeNameTracker = true;

    this.localFMSProductCodeName = param;
  }

  /** field for FMSProductCodeDescription */
  protected java.lang.String localFMSProductCodeDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFMSProductCodeDescriptionTracker = false;

  public boolean isFMSProductCodeDescriptionSpecified() {
    return localFMSProductCodeDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getFMSProductCodeDescription() {
    return localFMSProductCodeDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param FMSProductCodeDescription
   */
  public void setFMSProductCodeDescription(java.lang.String param) {
    localFMSProductCodeDescriptionTracker = true;

    this.localFMSProductCodeDescription = param;
  }

  /** field for VATCodeCode */
  protected java.lang.String localVATCodeCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localVATCodeCodeTracker = false;

  public boolean isVATCodeCodeSpecified() {
    return localVATCodeCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getVATCodeCode() {
    return localVATCodeCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param VATCodeCode
   */
  public void setVATCodeCode(java.lang.String param) {
    localVATCodeCodeTracker = true;

    this.localVATCodeCode = param;
  }

  /** field for VATCodeName */
  protected java.lang.String localVATCodeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localVATCodeNameTracker = false;

  public boolean isVATCodeNameSpecified() {
    return localVATCodeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getVATCodeName() {
    return localVATCodeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param VATCodeName
   */
  public void setVATCodeName(java.lang.String param) {
    localVATCodeNameTracker = true;

    this.localVATCodeName = param;
  }

  /** field for VATCodeRate */
  protected double localVATCodeRate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localVATCodeRateTracker = false;

  public boolean isVATCodeRateSpecified() {
    return localVATCodeRateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getVATCodeRate() {
    return localVATCodeRate;
  }

  /**
   * Auto generated setter method
   *
   * @param param VATCodeRate
   */
  public void setVATCodeRate(double param) {

    // setting primitive attribute tracker to true
    localVATCodeRateTracker = !java.lang.Double.isNaN(param);

    this.localVATCodeRate = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":BM_ServiceItemCharge",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "BM_ServiceItemCharge",
            xmlWriter);
      }
    }
    if (localServiceItemChargeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemChargeID", xmlWriter);

      if (localServiceItemChargeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceItemChargeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceItemChargeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceID", xmlWriter);

      if (localServiceID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceName", xmlWriter);

      if (localServiceName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemID", xmlWriter);

      if (localServiceItemID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceItemID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceItemID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemName", xmlWriter);

      if (localServiceItemName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceChargeTypeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceChargeTypeID", xmlWriter);

      if (localServiceChargeTypeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceChargeTypeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceChargeTypeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceChargeTypeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceChargeTypeName", xmlWriter);

      if (localServiceChargeTypeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceChargeTypeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemChargeAmountTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemChargeAmount", xmlWriter);

      if (localServiceItemChargeAmount == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceItemChargeAmount cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceItemChargeAmount));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemCostAmountTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemCostAmount", xmlWriter);

      if (localServiceItemCostAmount == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceItemCostAmount cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceItemCostAmount));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemChargeIsOneOffTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemChargeIsOneOff", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceItemChargeIsOneOff cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceItemChargeIsOneOff));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemChargeScheduleIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemChargeScheduleID", xmlWriter);

      if (localServiceItemChargeScheduleID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceItemChargeScheduleID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceItemChargeScheduleID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemChargeScheduleNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemChargeScheduleName", xmlWriter);

      if (localServiceItemChargeScheduleName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemChargeScheduleName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemChargeValidFromTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemChargeValidFrom", xmlWriter);

      if (localServiceItemChargeValidFrom == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceItemChargeValidFrom cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceItemChargeValidFrom));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemChargeValidToTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemChargeValidTo", xmlWriter);

      if (localServiceItemChargeValidTo == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceItemChargeValidTo cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceItemChargeValidTo));
      }

      xmlWriter.writeEndElement();
    }
    if (localFMSNominalCodeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "FMSNominalCodeID", xmlWriter);

      if (localFMSNominalCodeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("FMSNominalCodeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localFMSNominalCodeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localFMSNominalCodeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "FMSNominalCodeName", xmlWriter);

      if (localFMSNominalCodeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localFMSNominalCodeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localFMSNominalCodeDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "FMSNominalCodeDescription", xmlWriter);

      if (localFMSNominalCodeDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localFMSNominalCodeDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localFMSProductCodeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "FMSProductCodeID", xmlWriter);

      if (localFMSProductCodeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("FMSProductCodeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localFMSProductCodeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localFMSProductCodeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "FMSProductCodeName", xmlWriter);

      if (localFMSProductCodeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localFMSProductCodeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localFMSProductCodeDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "FMSProductCodeDescription", xmlWriter);

      if (localFMSProductCodeDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localFMSProductCodeDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localVATCodeCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "VATCodeCode", xmlWriter);

      if (localVATCodeCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localVATCodeCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localVATCodeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "VATCodeName", xmlWriter);

      if (localVATCodeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localVATCodeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localVATCodeRateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "VATCodeRate", xmlWriter);

      if (java.lang.Double.isNaN(localVATCodeRate)) {

        throw new org.apache.axis2.databinding.ADBException("VATCodeRate cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVATCodeRate));
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static BM_ServiceItemCharge parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      BM_ServiceItemCharge object = new BM_ServiceItemCharge();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"BM_ServiceItemCharge".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (BM_ServiceItemCharge)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemChargeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemChargeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemChargeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceItemChargeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceItemID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceChargeTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceChargeTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceChargeTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceChargeTypeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceChargeTypeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceChargeTypeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemChargeAmount")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemChargeAmount" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemChargeAmount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemCostAmount")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemCostAmount" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemCostAmount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemChargeIsOneOff")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemChargeIsOneOff" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemChargeIsOneOff(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemChargeScheduleID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemChargeScheduleID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemChargeScheduleID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceItemChargeScheduleID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemChargeScheduleName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemChargeScheduleName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemChargeValidFrom")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemChargeValidFrom" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemChargeValidFrom(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemChargeValidTo")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemChargeValidTo" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemChargeValidTo(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "FMSNominalCodeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "FMSNominalCodeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setFMSNominalCodeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setFMSNominalCodeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "FMSNominalCodeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setFMSNominalCodeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "FMSNominalCodeDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setFMSNominalCodeDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "FMSProductCodeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "FMSProductCodeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setFMSProductCodeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setFMSProductCodeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "FMSProductCodeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setFMSProductCodeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "FMSProductCodeDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setFMSProductCodeDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "VATCodeCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setVATCodeCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "VATCodeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setVATCodeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "VATCodeRate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "VATCodeRate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setVATCodeRate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setVATCodeRate(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
