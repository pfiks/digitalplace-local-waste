/**
 * InputCreateWorksheetInput.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** InputCreateWorksheetInput bean class */
@SuppressWarnings({"unchecked", "unused"})
public class InputCreateWorksheetInput implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = Input.CreateWorksheetInput
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for Uprn */
  protected java.lang.String localUprn;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUprnTracker = false;

  public boolean isUprnSpecified() {
    return localUprnTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getUprn() {
    return localUprn;
  }

  /**
   * Auto generated setter method
   *
   * @param param Uprn
   */
  public void setUprn(java.lang.String param) {
    localUprnTracker = true;

    this.localUprn = param;
  }

  /** field for AccountSiteId */
  protected java.lang.String localAccountSiteId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteIdTracker = false;

  public boolean isAccountSiteIdSpecified() {
    return localAccountSiteIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAccountSiteId() {
    return localAccountSiteId;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteId
   */
  public void setAccountSiteId(java.lang.String param) {
    localAccountSiteIdTracker = true;

    this.localAccountSiteId = param;
  }

  /** field for ServiceId */
  protected java.lang.String localServiceId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIdTracker = false;

  public boolean isServiceIdSpecified() {
    return localServiceIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceId() {
    return localServiceId;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceId
   */
  public void setServiceId(java.lang.String param) {
    localServiceIdTracker = true;

    this.localServiceId = param;
  }

  /** field for Usrn */
  protected java.lang.String localUsrn;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUsrnTracker = false;

  public boolean isUsrnSpecified() {
    return localUsrnTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getUsrn() {
    return localUsrn;
  }

  /**
   * Auto generated setter method
   *
   * @param param Usrn
   */
  public void setUsrn(java.lang.String param) {
    localUsrnTracker = true;

    this.localUsrn = param;
  }

  /** field for WorksheetReference */
  protected java.lang.String localWorksheetReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetReferenceTracker = false;

  public boolean isWorksheetReferenceSpecified() {
    return localWorksheetReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetReference() {
    return localWorksheetReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetReference
   */
  public void setWorksheetReference(java.lang.String param) {
    localWorksheetReferenceTracker = true;

    this.localWorksheetReference = param;
  }

  /** field for WorksheetMessage */
  protected java.lang.String localWorksheetMessage;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetMessageTracker = false;

  public boolean isWorksheetMessageSpecified() {
    return localWorksheetMessageTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetMessage() {
    return localWorksheetMessage;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetMessage
   */
  public void setWorksheetMessage(java.lang.String param) {
    localWorksheetMessageTracker = true;

    this.localWorksheetMessage = param;
  }

  /** field for WorkLocationAddress */
  protected java.lang.String localWorkLocationAddress;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkLocationAddressTracker = false;

  public boolean isWorkLocationAddressSpecified() {
    return localWorkLocationAddressTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkLocationAddress() {
    return localWorkLocationAddress;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkLocationAddress
   */
  public void setWorkLocationAddress(java.lang.String param) {
    localWorkLocationAddressTracker = true;

    this.localWorkLocationAddress = param;
  }

  /** field for WorkLocationName */
  protected java.lang.String localWorkLocationName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkLocationNameTracker = false;

  public boolean isWorkLocationNameSpecified() {
    return localWorkLocationNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkLocationName() {
    return localWorkLocationName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkLocationName
   */
  public void setWorkLocationName(java.lang.String param) {
    localWorkLocationNameTracker = true;

    this.localWorkLocationName = param;
  }

  /** field for WorkLocationLatitude */
  protected java.lang.String localWorkLocationLatitude;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkLocationLatitudeTracker = false;

  public boolean isWorkLocationLatitudeSpecified() {
    return localWorkLocationLatitudeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkLocationLatitude() {
    return localWorkLocationLatitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkLocationLatitude
   */
  public void setWorkLocationLatitude(java.lang.String param) {
    localWorkLocationLatitudeTracker = true;

    this.localWorkLocationLatitude = param;
  }

  /** field for WorkLocationLongitude */
  protected java.lang.String localWorkLocationLongitude;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkLocationLongitudeTracker = false;

  public boolean isWorkLocationLongitudeSpecified() {
    return localWorkLocationLongitudeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkLocationLongitude() {
    return localWorkLocationLongitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkLocationLongitude
   */
  public void setWorkLocationLongitude(java.lang.String param) {
    localWorkLocationLongitudeTracker = true;

    this.localWorkLocationLongitude = param;
  }

  /** field for WorkLocationNorthing */
  protected java.lang.String localWorkLocationNorthing;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkLocationNorthingTracker = false;

  public boolean isWorkLocationNorthingSpecified() {
    return localWorkLocationNorthingTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkLocationNorthing() {
    return localWorkLocationNorthing;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkLocationNorthing
   */
  public void setWorkLocationNorthing(java.lang.String param) {
    localWorkLocationNorthingTracker = true;

    this.localWorkLocationNorthing = param;
  }

  /** field for WorkLocationEasting */
  protected java.lang.String localWorkLocationEasting;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkLocationEastingTracker = false;

  public boolean isWorkLocationEastingSpecified() {
    return localWorkLocationEastingTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkLocationEasting() {
    return localWorkLocationEasting;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkLocationEasting
   */
  public void setWorkLocationEasting(java.lang.String param) {
    localWorkLocationEastingTracker = true;

    this.localWorkLocationEasting = param;
  }

  /** field for WorksheetStartDate */
  protected java.lang.String localWorksheetStartDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetStartDateTracker = false;

  public boolean isWorksheetStartDateSpecified() {
    return localWorksheetStartDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetStartDate() {
    return localWorksheetStartDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetStartDate
   */
  public void setWorksheetStartDate(java.lang.String param) {
    localWorksheetStartDateTracker = true;

    this.localWorksheetStartDate = param;
  }

  /** field for WorksheetDueDate */
  protected java.lang.String localWorksheetDueDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetDueDateTracker = false;

  public boolean isWorksheetDueDateSpecified() {
    return localWorksheetDueDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetDueDate() {
    return localWorksheetDueDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetDueDate
   */
  public void setWorksheetDueDate(java.lang.String param) {
    localWorksheetDueDateTracker = true;

    this.localWorksheetDueDate = param;
  }

  /** field for ServiceItemInputs */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .ArrayOfInputCreateWorksheetInputServiceItemInput
      localServiceItemInputs;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemInputsTracker = false;

  public boolean isServiceItemInputsSpecified() {
    return localServiceItemInputsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.ArrayOfInputCreateWorksheetInputServiceItemInput
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .ArrayOfInputCreateWorksheetInputServiceItemInput
      getServiceItemInputs() {
    return localServiceItemInputs;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemInputs
   */
  public void setServiceItemInputs(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
              .ArrayOfInputCreateWorksheetInputServiceItemInput
          param) {
    localServiceItemInputsTracker = true;

    this.localServiceItemInputs = param;
  }

  /** field for ServicePropertyInputs */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .ArrayOfInputCreateWorksheetInputServicePropertyInput
      localServicePropertyInputs;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServicePropertyInputsTracker = false;

  public boolean isServicePropertyInputsSpecified() {
    return localServicePropertyInputsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.ArrayOfInputCreateWorksheetInputServicePropertyInput
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .ArrayOfInputCreateWorksheetInputServicePropertyInput
      getServicePropertyInputs() {
    return localServicePropertyInputs;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServicePropertyInputs
   */
  public void setServicePropertyInputs(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
              .ArrayOfInputCreateWorksheetInputServicePropertyInput
          param) {
    localServicePropertyInputsTracker = true;

    this.localServicePropertyInputs = param;
  }

  /** field for AdHocRoundInstanceId */
  protected java.lang.String localAdHocRoundInstanceId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAdHocRoundInstanceIdTracker = false;

  public boolean isAdHocRoundInstanceIdSpecified() {
    return localAdHocRoundInstanceIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAdHocRoundInstanceId() {
    return localAdHocRoundInstanceId;
  }

  /**
   * Auto generated setter method
   *
   * @param param AdHocRoundInstanceId
   */
  public void setAdHocRoundInstanceId(java.lang.String param) {
    localAdHocRoundInstanceIdTracker = true;

    this.localAdHocRoundInstanceId = param;
  }

  /** field for RoleId */
  protected int localRoleId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRoleIdTracker = false;

  public boolean isRoleIdSpecified() {
    return localRoleIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getRoleId() {
    return localRoleId;
  }

  /**
   * Auto generated setter method
   *
   * @param param RoleId
   */
  public void setRoleId(int param) {

    // setting primitive attribute tracker to true
    localRoleIdTracker = param != java.lang.Integer.MIN_VALUE;

    this.localRoleId = param;
  }

  /** field for RoleName */
  protected java.lang.String localRoleName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRoleNameTracker = false;

  public boolean isRoleNameSpecified() {
    return localRoleNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getRoleName() {
    return localRoleName;
  }

  /**
   * Auto generated setter method
   *
   * @param param RoleName
   */
  public void setRoleName(java.lang.String param) {
    localRoleNameTracker = true;

    this.localRoleName = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Input.CreateWorksheetInput",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "Input.CreateWorksheetInput",
            xmlWriter);
      }
    }
    if (localUprnTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "Uprn", xmlWriter);

      if (localUprn == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localUprn);
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteIdTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteId", xmlWriter);

      if (localAccountSiteId == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localAccountSiteId);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIdTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceId", xmlWriter);

      if (localServiceId == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceId);
      }

      xmlWriter.writeEndElement();
    }
    if (localUsrnTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "Usrn", xmlWriter);

      if (localUsrn == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localUsrn);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetReferenceTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetReference", xmlWriter);

      if (localWorksheetReference == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetMessageTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetMessage", xmlWriter);

      if (localWorksheetMessage == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetMessage);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkLocationAddressTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkLocationAddress", xmlWriter);

      if (localWorkLocationAddress == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkLocationAddress);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkLocationNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkLocationName", xmlWriter);

      if (localWorkLocationName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkLocationName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkLocationLatitudeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkLocationLatitude", xmlWriter);

      if (localWorkLocationLatitude == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkLocationLatitude);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkLocationLongitudeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkLocationLongitude", xmlWriter);

      if (localWorkLocationLongitude == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkLocationLongitude);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkLocationNorthingTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkLocationNorthing", xmlWriter);

      if (localWorkLocationNorthing == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkLocationNorthing);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkLocationEastingTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkLocationEasting", xmlWriter);

      if (localWorkLocationEasting == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkLocationEasting);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetStartDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetStartDate", xmlWriter);

      if (localWorksheetStartDate == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetStartDate);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetDueDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetDueDate", xmlWriter);

      if (localWorksheetDueDate == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetDueDate);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemInputsTracker) {
      if (localServiceItemInputs == null) {

        writeStartElement(
            null, "http://webservices.whitespacews.com/", "ServiceItemInputs", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localServiceItemInputs.serialize(
            new javax.xml.namespace.QName(
                "http://webservices.whitespacews.com/", "ServiceItemInputs"),
            xmlWriter);
      }
    }
    if (localServicePropertyInputsTracker) {
      if (localServicePropertyInputs == null) {

        writeStartElement(
            null, "http://webservices.whitespacews.com/", "ServicePropertyInputs", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localServicePropertyInputs.serialize(
            new javax.xml.namespace.QName(
                "http://webservices.whitespacews.com/", "ServicePropertyInputs"),
            xmlWriter);
      }
    }
    if (localAdHocRoundInstanceIdTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AdHocRoundInstanceId", xmlWriter);

      if (localAdHocRoundInstanceId == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localAdHocRoundInstanceId);
      }

      xmlWriter.writeEndElement();
    }
    if (localRoleIdTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "RoleId", xmlWriter);

      if (localRoleId == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("RoleId cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRoleId));
      }

      xmlWriter.writeEndElement();
    }
    if (localRoleNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "RoleName", xmlWriter);

      if (localRoleName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localRoleName);
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static InputCreateWorksheetInput parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      InputCreateWorksheetInput object = new InputCreateWorksheetInput();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Input.CreateWorksheetInput".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (InputCreateWorksheetInput)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Uprn")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setUprn(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteId")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setAccountSiteId(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceId")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceId(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Usrn")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setUsrn(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetReference(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetMessage")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetMessage(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkLocationAddress")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkLocationAddress(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkLocationName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkLocationName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkLocationLatitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkLocationLatitude(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkLocationLongitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkLocationLongitude(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkLocationNorthing")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkLocationNorthing(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkLocationEasting")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkLocationEasting(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetStartDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetStartDate(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetDueDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetDueDate(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemInputs")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setServiceItemInputs(null);
            reader.next();

            reader.next();

          } else {

            object.setServiceItemInputs(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
                    .ArrayOfInputCreateWorksheetInputServiceItemInput.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServicePropertyInputs")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setServicePropertyInputs(null);
            reader.next();

            reader.next();

          } else {

            object.setServicePropertyInputs(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
                    .ArrayOfInputCreateWorksheetInputServicePropertyInput.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AdHocRoundInstanceId")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setAdHocRoundInstanceId(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "RoleId")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "RoleId" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setRoleId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setRoleId(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "RoleName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setRoleName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
