package com.placecube.digitalplace.local.waste.whitespace.exception;


@SuppressWarnings("serial")
public class WhitespaceParameterException extends Exception {
	
	public WhitespaceParameterException(String message) {
		super(message);
	}

}
