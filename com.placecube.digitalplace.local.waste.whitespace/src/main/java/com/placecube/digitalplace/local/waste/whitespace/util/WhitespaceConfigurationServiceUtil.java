package com.placecube.digitalplace.local.waste.whitespace.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;

@Component(immediate = true, service = WhitespaceConfigurationServiceUtil.class)
public class WhitespaceConfigurationServiceUtil {

	public Map<String, WhitespaceService> getWhitespaceServicesFromFormConfiguration(Map<String, Object> formConfiguration) {
		Map<String, WhitespaceService> whitespaceServiceMap = new HashMap<>();

		Map<String, Object> serviceMappings = (Map<String, Object>) formConfiguration.get("serviceMappings");

		if (serviceMappings != null) {
			for (Entry<String, Object> entry : serviceMappings.entrySet()) {
				Map<String, Object> serviceProperties = (Map<String, Object>) entry.getValue();

				if (serviceProperties != null) {
					whitespaceServiceMap.put(entry.getKey(), new WhitespaceService(serviceProperties));
				}
			}
		}

		return whitespaceServiceMap;
	}

	public Optional<String> getServiceIdForServiceTypeFromFormConfiguration(Map<String, Object> formConfiguration, String serviceType) {
		Map<String, Object> serviceMappings = (Map<String, Object>) formConfiguration.get("serviceMappings");

		if (serviceMappings != null) {
			return serviceMappings.entrySet().stream().filter(x -> isServiceType(x, serviceType)).map(this::getServiceIdProperty).filter(Objects::nonNull).findFirst();
		} else {
			return Optional.empty();
		}
	}

	private boolean isServiceType(Entry<String, Object> serviceConfiguration, String serviceType) {
		Map<String, Object> serviceProperties = (Map<String, Object>) serviceConfiguration.getValue();
		return serviceType != null && serviceType.equals(serviceProperties.get("serviceType"));
	}

	private String getServiceIdProperty(Entry<String, Object> serviceConfiguration) {
		Map<String, Object> serviceProperties = (Map<String, Object>) serviceConfiguration.getValue();
		return (String) serviceProperties.get("serviceId");
	}
}
