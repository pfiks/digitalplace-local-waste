package com.placecube.digitalplace.local.waste.whitespace.model.validator;

import java.time.Clock;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

public class TwoLastWeeksValidator implements WorksheetValidator {

	private WhitespaceWasteServiceUtil whitespaceWasteServiceUtil;

	public TwoLastWeeksValidator(WhitespaceWasteServiceUtil whitespaceWasteServiceUtil) {
		this.whitespaceWasteServiceUtil = whitespaceWasteServiceUtil;
	}

	@Override
	public boolean isValid(Worksheet worksheet) {
		return !whitespaceWasteServiceUtil.isWorksheetCreatedDateInTwoLastWeeks(Clock.systemDefaultZone(), worksheet);
	}
}
