package com.placecube.digitalplace.local.waste.whitespace.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.waste.whitespace.configuration.WhitespaceCompanyConfiguration", localization = "content/Language", name = "waste-whitespace")
public interface WhitespaceCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "https://msbapi-municipal-uat.whitespacews.com", name = "whitespace-base-url", description = "whitespace-base-url-help")
	String whitespaceBaseUrl();

	@Meta.AD(required = false, deflt = "", name = "username")
	String username();

	@Meta.AD(required = false, deflt = "", name = "password", type = Meta.Type.Password)
	String password();

	@Meta.AD(required = false, deflt = "", name = "key-value-pair-mapping-name", description = "key-value-pair-mapping-description")
	String keyValuePairMapping();

	@Meta.AD(required = false, deflt = "", name = "form-to-service-mapping-name", description = "form-to-service-mapping-description")
	String formToServiceMapping();

}
