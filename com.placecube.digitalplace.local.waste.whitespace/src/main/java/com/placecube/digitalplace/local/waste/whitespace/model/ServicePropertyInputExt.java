package com.placecube.digitalplace.local.waste.whitespace.model;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;

import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.ArrayOfInputCreateWorksheetInputServicePropertyInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.ArrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.InputCreateWorksheetInputServicePropertyInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.InputUpdateWorksheetDetailInputUpdateServicePropertyInput;
import com.placecube.digitalplace.local.waste.whitespace.exception.WhitespaceParameterException;

public class ServicePropertyInputExt {

	private List<String> propertyValues = new ArrayList<>();

	private List<String> servicePropertyIds = new ArrayList<>();

	public ArrayOfInputCreateWorksheetInputServicePropertyInput getServicePropertyInputs() throws WhitespaceParameterException {
		if (propertyValues.size() != servicePropertyIds.size()) {
			throw new WhitespaceParameterException("The ServicePropertyInput parameter counts don't match.");
		}

		ArrayOfInputCreateWorksheetInputServicePropertyInput arrayOfInputCreateWorksheetInputServicePropertyInput = new ArrayOfInputCreateWorksheetInputServicePropertyInput();

		for (int i = 0; i < propertyValues.size(); i++) {
			InputCreateWorksheetInputServicePropertyInput inputCreateWorksheetInputServicePropertyInput = new InputCreateWorksheetInputServicePropertyInput();
			inputCreateWorksheetInputServicePropertyInput.setServicePropertyId(GetterUtil.get(servicePropertyIds.get(i), 0));
			inputCreateWorksheetInputServicePropertyInput.setServicePropertyValue(propertyValues.get(i));
			arrayOfInputCreateWorksheetInputServicePropertyInput.addInputCreateWorksheetInputServicePropertyInput(inputCreateWorksheetInputServicePropertyInput);
		}

		return arrayOfInputCreateWorksheetInputServicePropertyInput;
	}

	public ArrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput getServicePropertyUpdateInputs() throws WhitespaceParameterException {
		if (propertyValues.size() != servicePropertyIds.size()) {
			throw new WhitespaceParameterException("The ServicePropertyInput parameter counts don't match.");
		}

		ArrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput arrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput = new ArrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput();

		for (int i = 0; i < propertyValues.size(); i++) {
			InputUpdateWorksheetDetailInputUpdateServicePropertyInput inputUpdateWorksheetDetailInputUpdateServicePropertyInput = new InputUpdateWorksheetDetailInputUpdateServicePropertyInput();
			inputUpdateWorksheetDetailInputUpdateServicePropertyInput.setServicePropertyId(GetterUtil.get(servicePropertyIds.get(i), 0));
			inputUpdateWorksheetDetailInputUpdateServicePropertyInput.setServicePropertyValue(propertyValues.get(i));
			arrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput.addInputUpdateWorksheetDetailInputUpdateServicePropertyInput(inputUpdateWorksheetDetailInputUpdateServicePropertyInput);
		}

		return arrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput;
	}

	@FormParam("servicePropertyId")
	public void setServicePropertyIds(List<String> servicePropertyIds) {
		this.servicePropertyIds = servicePropertyIds;
	}

	@FormParam("servicePropertyValue")
	public void setServicePropertyValues(List<String> propertyValues) {
		this.propertyValues = propertyValues;
	}

}
