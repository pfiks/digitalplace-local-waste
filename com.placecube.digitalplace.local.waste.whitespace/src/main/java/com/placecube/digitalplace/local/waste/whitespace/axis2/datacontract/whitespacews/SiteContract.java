/**
 * SiteContract.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** SiteContract bean class */
@SuppressWarnings({"unchecked", "unused"})
public class SiteContract implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = SiteContract
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for SiteContractID */
  protected int localSiteContractID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteContractIDTracker = false;

  public boolean isSiteContractIDSpecified() {
    return localSiteContractIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteContractID() {
    return localSiteContractID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteContractID
   */
  public void setSiteContractID(int param) {

    // setting primitive attribute tracker to true
    localSiteContractIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSiteContractID = param;
  }

  /** field for SiteID */
  protected int localSiteID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteIDTracker = false;

  public boolean isSiteIDSpecified() {
    return localSiteIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteID() {
    return localSiteID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteID
   */
  public void setSiteID(int param) {

    // setting primitive attribute tracker to true
    localSiteIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSiteID = param;
  }

  /** field for AccountSiteID */
  protected int localAccountSiteID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteIDTracker = false;

  public boolean isAccountSiteIDSpecified() {
    return localAccountSiteIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAccountSiteID() {
    return localAccountSiteID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteID
   */
  public void setAccountSiteID(int param) {

    // setting primitive attribute tracker to true
    localAccountSiteIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAccountSiteID = param;
  }

  /** field for ContractID */
  protected int localContractID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractIDTracker = false;

  public boolean isContractIDSpecified() {
    return localContractIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getContractID() {
    return localContractID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractID
   */
  public void setContractID(int param) {

    // setting primitive attribute tracker to true
    localContractIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localContractID = param;
  }

  /** field for ContractTypeID */
  protected int localContractTypeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractTypeIDTracker = false;

  public boolean isContractTypeIDSpecified() {
    return localContractTypeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getContractTypeID() {
    return localContractTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractTypeID
   */
  public void setContractTypeID(int param) {

    // setting primitive attribute tracker to true
    localContractTypeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localContractTypeID = param;
  }

  /** field for ContractTypeName */
  protected java.lang.String localContractTypeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractTypeNameTracker = false;

  public boolean isContractTypeNameSpecified() {
    return localContractTypeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractTypeName() {
    return localContractTypeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractTypeName
   */
  public void setContractTypeName(java.lang.String param) {
    localContractTypeNameTracker = true;

    this.localContractTypeName = param;
  }

  /** field for ContractStatusCode */
  protected java.lang.String localContractStatusCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractStatusCodeTracker = false;

  public boolean isContractStatusCodeSpecified() {
    return localContractStatusCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractStatusCode() {
    return localContractStatusCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractStatusCode
   */
  public void setContractStatusCode(java.lang.String param) {
    localContractStatusCodeTracker = true;

    this.localContractStatusCode = param;
  }

  /** field for ContractStatusDescription */
  protected java.lang.String localContractStatusDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractStatusDescriptionTracker = false;

  public boolean isContractStatusDescriptionSpecified() {
    return localContractStatusDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractStatusDescription() {
    return localContractStatusDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractStatusDescription
   */
  public void setContractStatusDescription(java.lang.String param) {
    localContractStatusDescriptionTracker = true;

    this.localContractStatusDescription = param;
  }

  /** field for ContractName */
  protected java.lang.String localContractName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractNameTracker = false;

  public boolean isContractNameSpecified() {
    return localContractNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractName() {
    return localContractName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractName
   */
  public void setContractName(java.lang.String param) {
    localContractNameTracker = true;

    this.localContractName = param;
  }

  /** field for SiteContractValidFrom */
  protected java.util.Calendar localSiteContractValidFrom;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteContractValidFromTracker = false;

  public boolean isSiteContractValidFromSpecified() {
    return localSiteContractValidFromTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getSiteContractValidFrom() {
    return localSiteContractValidFrom;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteContractValidFrom
   */
  public void setSiteContractValidFrom(java.util.Calendar param) {
    localSiteContractValidFromTracker = param != null;

    this.localSiteContractValidFrom = param;
  }

  /** field for SiteContractValidTo */
  protected java.util.Calendar localSiteContractValidTo;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteContractValidToTracker = false;

  public boolean isSiteContractValidToSpecified() {
    return localSiteContractValidToTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getSiteContractValidTo() {
    return localSiteContractValidTo;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteContractValidTo
   */
  public void setSiteContractValidTo(java.util.Calendar param) {
    localSiteContractValidToTracker = param != null;

    this.localSiteContractValidTo = param;
  }

  /** field for ScheduleID */
  protected int localScheduleID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localScheduleIDTracker = false;

  public boolean isScheduleIDSpecified() {
    return localScheduleIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getScheduleID() {
    return localScheduleID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ScheduleID
   */
  public void setScheduleID(int param) {

    // setting primitive attribute tracker to true
    localScheduleIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localScheduleID = param;
  }

  /** field for ScheduleName */
  protected java.lang.String localScheduleName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localScheduleNameTracker = false;

  public boolean isScheduleNameSpecified() {
    return localScheduleNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getScheduleName() {
    return localScheduleName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ScheduleName
   */
  public void setScheduleName(java.lang.String param) {
    localScheduleNameTracker = true;

    this.localScheduleName = param;
  }

  /** field for PaymentMethodID */
  protected int localPaymentMethodID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentMethodIDTracker = false;

  public boolean isPaymentMethodIDSpecified() {
    return localPaymentMethodIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getPaymentMethodID() {
    return localPaymentMethodID;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentMethodID
   */
  public void setPaymentMethodID(int param) {

    // setting primitive attribute tracker to true
    localPaymentMethodIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localPaymentMethodID = param;
  }

  /** field for PaymentMethodName */
  protected java.lang.String localPaymentMethodName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentMethodNameTracker = false;

  public boolean isPaymentMethodNameSpecified() {
    return localPaymentMethodNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPaymentMethodName() {
    return localPaymentMethodName;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentMethodName
   */
  public void setPaymentMethodName(java.lang.String param) {
    localPaymentMethodNameTracker = true;

    this.localPaymentMethodName = param;
  }

  /** field for SiteContractGUID */
  protected java.lang.String localSiteContractGUID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteContractGUIDTracker = false;

  public boolean isSiteContractGUIDSpecified() {
    return localSiteContractGUIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteContractGUID() {
    return localSiteContractGUID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteContractGUID
   */
  public void setSiteContractGUID(java.lang.String param) {
    localSiteContractGUIDTracker = true;

    this.localSiteContractGUID = param;
  }

  /** field for ContractReference */
  protected java.lang.String localContractReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractReferenceTracker = false;

  public boolean isContractReferenceSpecified() {
    return localContractReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractReference() {
    return localContractReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractReference
   */
  public void setContractReference(java.lang.String param) {
    localContractReferenceTracker = true;

    this.localContractReference = param;
  }

  /** field for ContractPONumber */
  protected java.lang.String localContractPONumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractPONumberTracker = false;

  public boolean isContractPONumberSpecified() {
    return localContractPONumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractPONumber() {
    return localContractPONumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractPONumber
   */
  public void setContractPONumber(java.lang.String param) {
    localContractPONumberTracker = true;

    this.localContractPONumber = param;
  }

  /** field for SiteContractNotes */
  protected java.lang.String localSiteContractNotes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteContractNotesTracker = false;

  public boolean isSiteContractNotesSpecified() {
    return localSiteContractNotesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteContractNotes() {
    return localSiteContractNotes;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteContractNotes
   */
  public void setSiteContractNotes(java.lang.String param) {
    localSiteContractNotesTracker = true;

    this.localSiteContractNotes = param;
  }

  /** field for SiteAddress */
  protected java.lang.String localSiteAddress;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteAddressTracker = false;

  public boolean isSiteAddressSpecified() {
    return localSiteAddressTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteAddress() {
    return localSiteAddress;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteAddress
   */
  public void setSiteAddress(java.lang.String param) {
    localSiteAddressTracker = true;

    this.localSiteAddress = param;
  }

  /** field for SelectedEndDate */
  protected java.util.Calendar localSelectedEndDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSelectedEndDateTracker = false;

  public boolean isSelectedEndDateSpecified() {
    return localSelectedEndDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getSelectedEndDate() {
    return localSelectedEndDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param SelectedEndDate
   */
  public void setSelectedEndDate(java.util.Calendar param) {
    localSelectedEndDateTracker = param != null;

    this.localSelectedEndDate = param;
  }

  /** field for InvoiceToDate */
  protected java.util.Calendar localInvoiceToDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localInvoiceToDateTracker = false;

  public boolean isInvoiceToDateSpecified() {
    return localInvoiceToDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getInvoiceToDate() {
    return localInvoiceToDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param InvoiceToDate
   */
  public void setInvoiceToDate(java.util.Calendar param) {
    localInvoiceToDateTracker = param != null;

    this.localInvoiceToDate = param;
  }

  /** field for WorkflowInstanceID */
  protected int localWorkflowInstanceID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowInstanceIDTracker = false;

  public boolean isWorkflowInstanceIDSpecified() {
    return localWorkflowInstanceIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorkflowInstanceID() {
    return localWorkflowInstanceID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowInstanceID
   */
  public void setWorkflowInstanceID(int param) {

    // setting primitive attribute tracker to true
    localWorkflowInstanceIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorkflowInstanceID = param;
  }

  /** field for InstanceGuid */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .Guid
      localInstanceGuid;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localInstanceGuidTracker = false;

  public boolean isInstanceGuidSpecified() {
    return localInstanceGuidTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.Guid
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .Guid
      getInstanceGuid() {
    return localInstanceGuid;
  }

  /**
   * Auto generated setter method
   *
   * @param param InstanceGuid
   */
  public void setInstanceGuid(
      com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.Guid
          param) {
    localInstanceGuidTracker = param != null;

    this.localInstanceGuid = param;
  }

  /** field for WorkflowFileID */
  protected int localWorkflowFileID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowFileIDTracker = false;

  public boolean isWorkflowFileIDSpecified() {
    return localWorkflowFileIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorkflowFileID() {
    return localWorkflowFileID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowFileID
   */
  public void setWorkflowFileID(int param) {

    // setting primitive attribute tracker to true
    localWorkflowFileIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorkflowFileID = param;
  }

  /** field for WorkflowFileName */
  protected java.lang.String localWorkflowFileName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowFileNameTracker = false;

  public boolean isWorkflowFileNameSpecified() {
    return localWorkflowFileNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkflowFileName() {
    return localWorkflowFileName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowFileName
   */
  public void setWorkflowFileName(java.lang.String param) {
    localWorkflowFileNameTracker = true;

    this.localWorkflowFileName = param;
  }

  /** field for InstanceCurrentState */
  protected java.lang.String localInstanceCurrentState;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localInstanceCurrentStateTracker = false;

  public boolean isInstanceCurrentStateSpecified() {
    return localInstanceCurrentStateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getInstanceCurrentState() {
    return localInstanceCurrentState;
  }

  /**
   * Auto generated setter method
   *
   * @param param InstanceCurrentState
   */
  public void setInstanceCurrentState(java.lang.String param) {
    localInstanceCurrentStateTracker = true;

    this.localInstanceCurrentState = param;
  }

  /** field for CustomerStatusID */
  protected int localCustomerStatusID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCustomerStatusIDTracker = false;

  public boolean isCustomerStatusIDSpecified() {
    return localCustomerStatusIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getCustomerStatusID() {
    return localCustomerStatusID;
  }

  /**
   * Auto generated setter method
   *
   * @param param CustomerStatusID
   */
  public void setCustomerStatusID(int param) {

    // setting primitive attribute tracker to true
    localCustomerStatusIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localCustomerStatusID = param;
  }

  /** field for CustomerStatusName */
  protected java.lang.String localCustomerStatusName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCustomerStatusNameTracker = false;

  public boolean isCustomerStatusNameSpecified() {
    return localCustomerStatusNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getCustomerStatusName() {
    return localCustomerStatusName;
  }

  /**
   * Auto generated setter method
   *
   * @param param CustomerStatusName
   */
  public void setCustomerStatusName(java.lang.String param) {
    localCustomerStatusNameTracker = true;

    this.localCustomerStatusName = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":SiteContract",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "SiteContract", xmlWriter);
      }
    }
    if (localSiteContractIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteContractID", xmlWriter);

      if (localSiteContractID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SiteContractID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteContractID));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteID", xmlWriter);

      if (localSiteID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SiteID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteID));
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteID", xmlWriter);

      if (localAccountSiteID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AccountSiteID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAccountSiteID));
      }

      xmlWriter.writeEndElement();
    }
    if (localContractIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractID", xmlWriter);

      if (localContractID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ContractID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContractID));
      }

      xmlWriter.writeEndElement();
    }
    if (localContractTypeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractTypeID", xmlWriter);

      if (localContractTypeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ContractTypeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContractTypeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localContractTypeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractTypeName", xmlWriter);

      if (localContractTypeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractTypeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractStatusCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractStatusCode", xmlWriter);

      if (localContractStatusCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractStatusCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractStatusDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractStatusDescription", xmlWriter);

      if (localContractStatusDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractStatusDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractName", xmlWriter);

      if (localContractName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractName);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteContractValidFromTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteContractValidFrom", xmlWriter);

      if (localSiteContractValidFrom == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "SiteContractValidFrom cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localSiteContractValidFrom));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteContractValidToTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteContractValidTo", xmlWriter);

      if (localSiteContractValidTo == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("SiteContractValidTo cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localSiteContractValidTo));
      }

      xmlWriter.writeEndElement();
    }
    if (localScheduleIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ScheduleID", xmlWriter);

      if (localScheduleID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ScheduleID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScheduleID));
      }

      xmlWriter.writeEndElement();
    }
    if (localScheduleNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ScheduleName", xmlWriter);

      if (localScheduleName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localScheduleName);
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentMethodIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PaymentMethodID", xmlWriter);

      if (localPaymentMethodID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("PaymentMethodID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPaymentMethodID));
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentMethodNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PaymentMethodName", xmlWriter);

      if (localPaymentMethodName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localPaymentMethodName);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteContractGUIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteContractGUID", xmlWriter);

      if (localSiteContractGUID == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteContractGUID);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractReferenceTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractReference", xmlWriter);

      if (localContractReference == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractPONumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractPONumber", xmlWriter);

      if (localContractPONumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractPONumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteContractNotesTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteContractNotes", xmlWriter);

      if (localSiteContractNotes == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteContractNotes);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteAddressTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteAddress", xmlWriter);

      if (localSiteAddress == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteAddress);
      }

      xmlWriter.writeEndElement();
    }
    if (localSelectedEndDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SelectedEndDate", xmlWriter);

      if (localSelectedEndDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("SelectedEndDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSelectedEndDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localInvoiceToDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "InvoiceToDate", xmlWriter);

      if (localInvoiceToDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("InvoiceToDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInvoiceToDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowInstanceIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowInstanceID", xmlWriter);

      if (localWorkflowInstanceID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorkflowInstanceID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorkflowInstanceID));
      }

      xmlWriter.writeEndElement();
    }
    if (localInstanceGuidTracker) {
      if (localInstanceGuid == null) {
        throw new org.apache.axis2.databinding.ADBException("InstanceGuid cannot be null!!");
      }
      localInstanceGuid.serialize(
          new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "InstanceGuid"),
          xmlWriter);
    }
    if (localWorkflowFileIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowFileID", xmlWriter);

      if (localWorkflowFileID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorkflowFileID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorkflowFileID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowFileNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowFileName", xmlWriter);

      if (localWorkflowFileName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkflowFileName);
      }

      xmlWriter.writeEndElement();
    }
    if (localInstanceCurrentStateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "InstanceCurrentState", xmlWriter);

      if (localInstanceCurrentState == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localInstanceCurrentState);
      }

      xmlWriter.writeEndElement();
    }
    if (localCustomerStatusIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "CustomerStatusID", xmlWriter);

      if (localCustomerStatusID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("CustomerStatusID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localCustomerStatusID));
      }

      xmlWriter.writeEndElement();
    }
    if (localCustomerStatusNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "CustomerStatusName", xmlWriter);

      if (localCustomerStatusName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localCustomerStatusName);
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static SiteContract parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      SiteContract object = new SiteContract();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"SiteContract".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (SiteContract)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteContractID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteContractID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteContractID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteContractID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountSiteID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountSiteID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAccountSiteID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContractID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContractID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContractID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setContractID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContractTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContractTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setContractTypeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractTypeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractTypeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractStatusCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractStatusCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractStatusDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractStatusDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContractName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteContractValidFrom")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteContractValidFrom" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteContractValidFrom(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteContractValidTo")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteContractValidTo" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteContractValidTo(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ScheduleID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ScheduleID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setScheduleID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setScheduleID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ScheduleName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setScheduleName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PaymentMethodID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PaymentMethodID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPaymentMethodID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setPaymentMethodID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PaymentMethodName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setPaymentMethodName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteContractGUID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteContractGUID(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractReference(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractPONumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractPONumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteContractNotes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteContractNotes(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteAddress")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteAddress(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SelectedEndDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SelectedEndDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSelectedEndDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "InvoiceToDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "InvoiceToDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setInvoiceToDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowInstanceID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkflowInstanceID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkflowInstanceID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorkflowInstanceID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "InstanceGuid")
                .equals(reader.getName())) {

          object.setInstanceGuid(
              com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas
                  .serialization.Guid.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowFileID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkflowFileID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkflowFileID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorkflowFileID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowFileName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkflowFileName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "InstanceCurrentState")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setInstanceCurrentState(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "CustomerStatusID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "CustomerStatusID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCustomerStatusID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setCustomerStatusID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "CustomerStatusName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setCustomerStatusName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
