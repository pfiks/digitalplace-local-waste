/**
 * WorksheetResult.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** WorksheetResult bean class */
@SuppressWarnings({"unchecked", "unused"})
public class WorksheetResult implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = WorksheetResult
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for WorksheetID */
  protected int localWorksheetID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetIDTracker = false;

  public boolean isWorksheetIDSpecified() {
    return localWorksheetIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetID() {
    return localWorksheetID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetID
   */
  public void setWorksheetID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetID = param;
  }

  /** field for WorksheetCreatedBy */
  protected java.lang.String localWorksheetCreatedBy;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetCreatedByTracker = false;

  public boolean isWorksheetCreatedBySpecified() {
    return localWorksheetCreatedByTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetCreatedBy() {
    return localWorksheetCreatedBy;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetCreatedBy
   */
  public void setWorksheetCreatedBy(java.lang.String param) {
    localWorksheetCreatedByTracker = true;

    this.localWorksheetCreatedBy = param;
  }

  /** field for WorksheetAssignedToID */
  protected int localWorksheetAssignedToID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetAssignedToIDTracker = false;

  public boolean isWorksheetAssignedToIDSpecified() {
    return localWorksheetAssignedToIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetAssignedToID() {
    return localWorksheetAssignedToID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetAssignedToID
   */
  public void setWorksheetAssignedToID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetAssignedToIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetAssignedToID = param;
  }

  /** field for WorksheetAssignedToTypeID */
  protected int localWorksheetAssignedToTypeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetAssignedToTypeIDTracker = false;

  public boolean isWorksheetAssignedToTypeIDSpecified() {
    return localWorksheetAssignedToTypeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetAssignedToTypeID() {
    return localWorksheetAssignedToTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetAssignedToTypeID
   */
  public void setWorksheetAssignedToTypeID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetAssignedToTypeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetAssignedToTypeID = param;
  }

  /** field for WorksheetAssignedToName */
  protected java.lang.String localWorksheetAssignedToName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetAssignedToNameTracker = false;

  public boolean isWorksheetAssignedToNameSpecified() {
    return localWorksheetAssignedToNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetAssignedToName() {
    return localWorksheetAssignedToName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetAssignedToName
   */
  public void setWorksheetAssignedToName(java.lang.String param) {
    localWorksheetAssignedToNameTracker = true;

    this.localWorksheetAssignedToName = param;
  }

  /** field for WorksheetSubject */
  protected java.lang.String localWorksheetSubject;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetSubjectTracker = false;

  public boolean isWorksheetSubjectSpecified() {
    return localWorksheetSubjectTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetSubject() {
    return localWorksheetSubject;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetSubject
   */
  public void setWorksheetSubject(java.lang.String param) {
    localWorksheetSubjectTracker = true;

    this.localWorksheetSubject = param;
  }

  /** field for WorksheetType */
  protected java.lang.String localWorksheetType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetTypeTracker = false;

  public boolean isWorksheetTypeSpecified() {
    return localWorksheetTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetType() {
    return localWorksheetType;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetType
   */
  public void setWorksheetType(java.lang.String param) {
    localWorksheetTypeTracker = true;

    this.localWorksheetType = param;
  }

  /** field for WorksheetMessage */
  protected java.lang.String localWorksheetMessage;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetMessageTracker = false;

  public boolean isWorksheetMessageSpecified() {
    return localWorksheetMessageTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetMessage() {
    return localWorksheetMessage;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetMessage
   */
  public void setWorksheetMessage(java.lang.String param) {
    localWorksheetMessageTracker = true;

    this.localWorksheetMessage = param;
  }

  /** field for WorksheetReportedByContactID */
  protected int localWorksheetReportedByContactID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetReportedByContactIDTracker = false;

  public boolean isWorksheetReportedByContactIDSpecified() {
    return localWorksheetReportedByContactIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetReportedByContactID() {
    return localWorksheetReportedByContactID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetReportedByContactID
   */
  public void setWorksheetReportedByContactID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetReportedByContactIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetReportedByContactID = param;
  }

  /** field for WorksheetReportedByName */
  protected java.lang.String localWorksheetReportedByName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetReportedByNameTracker = false;

  public boolean isWorksheetReportedByNameSpecified() {
    return localWorksheetReportedByNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetReportedByName() {
    return localWorksheetReportedByName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetReportedByName
   */
  public void setWorksheetReportedByName(java.lang.String param) {
    localWorksheetReportedByNameTracker = true;

    this.localWorksheetReportedByName = param;
  }

  /** field for WorksheetReportedByAddressID */
  protected int localWorksheetReportedByAddressID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetReportedByAddressIDTracker = false;

  public boolean isWorksheetReportedByAddressIDSpecified() {
    return localWorksheetReportedByAddressIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetReportedByAddressID() {
    return localWorksheetReportedByAddressID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetReportedByAddressID
   */
  public void setWorksheetReportedByAddressID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetReportedByAddressIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetReportedByAddressID = param;
  }

  /** field for WorksheetReportedByAddress */
  protected java.lang.String localWorksheetReportedByAddress;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetReportedByAddressTracker = false;

  public boolean isWorksheetReportedByAddressSpecified() {
    return localWorksheetReportedByAddressTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetReportedByAddress() {
    return localWorksheetReportedByAddress;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetReportedByAddress
   */
  public void setWorksheetReportedByAddress(java.lang.String param) {
    localWorksheetReportedByAddressTracker = true;

    this.localWorksheetReportedByAddress = param;
  }

  /** field for WorksheetWorkLocationAddressID */
  protected int localWorksheetWorkLocationAddressID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetWorkLocationAddressIDTracker = false;

  public boolean isWorksheetWorkLocationAddressIDSpecified() {
    return localWorksheetWorkLocationAddressIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetWorkLocationAddressID() {
    return localWorksheetWorkLocationAddressID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetWorkLocationAddressID
   */
  public void setWorksheetWorkLocationAddressID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetWorkLocationAddressIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetWorkLocationAddressID = param;
  }

  /** field for WorksheetWorkLocationName */
  protected java.lang.String localWorksheetWorkLocationName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetWorkLocationNameTracker = false;

  public boolean isWorksheetWorkLocationNameSpecified() {
    return localWorksheetWorkLocationNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetWorkLocationName() {
    return localWorksheetWorkLocationName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetWorkLocationName
   */
  public void setWorksheetWorkLocationName(java.lang.String param) {
    localWorksheetWorkLocationNameTracker = true;

    this.localWorksheetWorkLocationName = param;
  }

  /** field for WorksheetWorkLocationAddress */
  protected java.lang.String localWorksheetWorkLocationAddress;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetWorkLocationAddressTracker = false;

  public boolean isWorksheetWorkLocationAddressSpecified() {
    return localWorksheetWorkLocationAddressTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetWorkLocationAddress() {
    return localWorksheetWorkLocationAddress;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetWorkLocationAddress
   */
  public void setWorksheetWorkLocationAddress(java.lang.String param) {
    localWorksheetWorkLocationAddressTracker = true;

    this.localWorksheetWorkLocationAddress = param;
  }

  /** field for WorksheetChargeName */
  protected java.lang.String localWorksheetChargeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetChargeNameTracker = false;

  public boolean isWorksheetChargeNameSpecified() {
    return localWorksheetChargeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetChargeName() {
    return localWorksheetChargeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetChargeName
   */
  public void setWorksheetChargeName(java.lang.String param) {
    localWorksheetChargeNameTracker = true;

    this.localWorksheetChargeName = param;
  }

  /** field for WorksheetChargeAddressID */
  protected int localWorksheetChargeAddressID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetChargeAddressIDTracker = false;

  public boolean isWorksheetChargeAddressIDSpecified() {
    return localWorksheetChargeAddressIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetChargeAddressID() {
    return localWorksheetChargeAddressID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetChargeAddressID
   */
  public void setWorksheetChargeAddressID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetChargeAddressIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetChargeAddressID = param;
  }

  /** field for WorksheetChargeAddress */
  protected java.lang.String localWorksheetChargeAddress;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetChargeAddressTracker = false;

  public boolean isWorksheetChargeAddressSpecified() {
    return localWorksheetChargeAddressTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetChargeAddress() {
    return localWorksheetChargeAddress;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetChargeAddress
   */
  public void setWorksheetChargeAddress(java.lang.String param) {
    localWorksheetChargeAddressTracker = true;

    this.localWorksheetChargeAddress = param;
  }

  /** field for WorksheetCreatedDate */
  protected java.util.Calendar localWorksheetCreatedDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetCreatedDateTracker = false;

  public boolean isWorksheetCreatedDateSpecified() {
    return localWorksheetCreatedDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetCreatedDate() {
    return localWorksheetCreatedDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetCreatedDate
   */
  public void setWorksheetCreatedDate(java.util.Calendar param) {
    localWorksheetCreatedDateTracker = param != null;

    this.localWorksheetCreatedDate = param;
  }

  /** field for WorksheetStartDate */
  protected java.util.Calendar localWorksheetStartDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetStartDateTracker = false;

  public boolean isWorksheetStartDateSpecified() {
    return localWorksheetStartDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetStartDate() {
    return localWorksheetStartDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetStartDate
   */
  public void setWorksheetStartDate(java.util.Calendar param) {
    localWorksheetStartDateTracker = param != null;

    this.localWorksheetStartDate = param;
  }

  /** field for WorksheetDueByDate */
  protected java.util.Calendar localWorksheetDueByDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetDueByDateTracker = false;

  public boolean isWorksheetDueByDateSpecified() {
    return localWorksheetDueByDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetDueByDate() {
    return localWorksheetDueByDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetDueByDate
   */
  public void setWorksheetDueByDate(java.util.Calendar param) {
    localWorksheetDueByDateTracker = param != null;

    this.localWorksheetDueByDate = param;
  }

  /** field for WorksheetEscallatedDate */
  protected java.util.Calendar localWorksheetEscallatedDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetEscallatedDateTracker = false;

  public boolean isWorksheetEscallatedDateSpecified() {
    return localWorksheetEscallatedDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetEscallatedDate() {
    return localWorksheetEscallatedDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetEscallatedDate
   */
  public void setWorksheetEscallatedDate(java.util.Calendar param) {
    localWorksheetEscallatedDateTracker = param != null;

    this.localWorksheetEscallatedDate = param;
  }

  /** field for WorksheetCompletedDate */
  protected java.util.Calendar localWorksheetCompletedDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetCompletedDateTracker = false;

  public boolean isWorksheetCompletedDateSpecified() {
    return localWorksheetCompletedDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetCompletedDate() {
    return localWorksheetCompletedDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetCompletedDate
   */
  public void setWorksheetCompletedDate(java.util.Calendar param) {
    localWorksheetCompletedDateTracker = param != null;

    this.localWorksheetCompletedDate = param;
  }

  /** field for WorksheetPaymentDate */
  protected java.util.Calendar localWorksheetPaymentDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetPaymentDateTracker = false;

  public boolean isWorksheetPaymentDateSpecified() {
    return localWorksheetPaymentDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetPaymentDate() {
    return localWorksheetPaymentDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetPaymentDate
   */
  public void setWorksheetPaymentDate(java.util.Calendar param) {
    localWorksheetPaymentDateTracker = param != null;

    this.localWorksheetPaymentDate = param;
  }

  /** field for WorksheetExpiryDate */
  protected java.util.Calendar localWorksheetExpiryDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetExpiryDateTracker = false;

  public boolean isWorksheetExpiryDateSpecified() {
    return localWorksheetExpiryDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetExpiryDate() {
    return localWorksheetExpiryDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetExpiryDate
   */
  public void setWorksheetExpiryDate(java.util.Calendar param) {
    localWorksheetExpiryDateTracker = param != null;

    this.localWorksheetExpiryDate = param;
  }

  /** field for WorksheetApprovedDate */
  protected java.util.Calendar localWorksheetApprovedDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetApprovedDateTracker = false;

  public boolean isWorksheetApprovedDateSpecified() {
    return localWorksheetApprovedDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetApprovedDate() {
    return localWorksheetApprovedDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetApprovedDate
   */
  public void setWorksheetApprovedDate(java.util.Calendar param) {
    localWorksheetApprovedDateTracker = param != null;

    this.localWorksheetApprovedDate = param;
  }

  /** field for WorksheetPaymentMethodID */
  protected int localWorksheetPaymentMethodID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetPaymentMethodIDTracker = false;

  public boolean isWorksheetPaymentMethodIDSpecified() {
    return localWorksheetPaymentMethodIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetPaymentMethodID() {
    return localWorksheetPaymentMethodID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetPaymentMethodID
   */
  public void setWorksheetPaymentMethodID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetPaymentMethodIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetPaymentMethodID = param;
  }

  /** field for WorksheetPaymentReference */
  protected java.lang.String localWorksheetPaymentReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetPaymentReferenceTracker = false;

  public boolean isWorksheetPaymentReferenceSpecified() {
    return localWorksheetPaymentReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetPaymentReference() {
    return localWorksheetPaymentReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetPaymentReference
   */
  public void setWorksheetPaymentReference(java.lang.String param) {
    localWorksheetPaymentReferenceTracker = true;

    this.localWorksheetPaymentReference = param;
  }

  /** field for WorksheetImportanceID */
  protected int localWorksheetImportanceID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetImportanceIDTracker = false;

  public boolean isWorksheetImportanceIDSpecified() {
    return localWorksheetImportanceIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetImportanceID() {
    return localWorksheetImportanceID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetImportanceID
   */
  public void setWorksheetImportanceID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetImportanceIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetImportanceID = param;
  }

  /** field for WorksheetImportanceName */
  protected java.lang.String localWorksheetImportanceName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetImportanceNameTracker = false;

  public boolean isWorksheetImportanceNameSpecified() {
    return localWorksheetImportanceNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetImportanceName() {
    return localWorksheetImportanceName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetImportanceName
   */
  public void setWorksheetImportanceName(java.lang.String param) {
    localWorksheetImportanceNameTracker = true;

    this.localWorksheetImportanceName = param;
  }

  /** field for WorksheetStatusID */
  protected int localWorksheetStatusID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetStatusIDTracker = false;

  public boolean isWorksheetStatusIDSpecified() {
    return localWorksheetStatusIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetStatusID() {
    return localWorksheetStatusID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetStatusID
   */
  public void setWorksheetStatusID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetStatusIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetStatusID = param;
  }

  /** field for WorksheetStatusName */
  protected java.lang.String localWorksheetStatusName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetStatusNameTracker = false;

  public boolean isWorksheetStatusNameSpecified() {
    return localWorksheetStatusNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetStatusName() {
    return localWorksheetStatusName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetStatusName
   */
  public void setWorksheetStatusName(java.lang.String param) {
    localWorksheetStatusNameTracker = true;

    this.localWorksheetStatusName = param;
  }

  /** field for WorksheetCompletionNotes */
  protected java.lang.String localWorksheetCompletionNotes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetCompletionNotesTracker = false;

  public boolean isWorksheetCompletionNotesSpecified() {
    return localWorksheetCompletionNotesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetCompletionNotes() {
    return localWorksheetCompletionNotes;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetCompletionNotes
   */
  public void setWorksheetCompletionNotes(java.lang.String param) {
    localWorksheetCompletionNotesTracker = true;

    this.localWorksheetCompletionNotes = param;
  }

  /** field for WorksheetGuid */
  protected java.lang.String localWorksheetGuid;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetGuidTracker = false;

  public boolean isWorksheetGuidSpecified() {
    return localWorksheetGuidTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetGuid() {
    return localWorksheetGuid;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetGuid
   */
  public void setWorksheetGuid(java.lang.String param) {
    localWorksheetGuidTracker = true;

    this.localWorksheetGuid = param;
  }

  /** field for WorksheetRef */
  protected java.lang.String localWorksheetRef;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetRefTracker = false;

  public boolean isWorksheetRefSpecified() {
    return localWorksheetRefTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetRef() {
    return localWorksheetRef;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetRef
   */
  public void setWorksheetRef(java.lang.String param) {
    localWorksheetRefTracker = true;

    this.localWorksheetRef = param;
  }

  /** field for WorksheetParentID */
  protected int localWorksheetParentID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetParentIDTracker = false;

  public boolean isWorksheetParentIDSpecified() {
    return localWorksheetParentIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetParentID() {
    return localWorksheetParentID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetParentID
   */
  public void setWorksheetParentID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetParentIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetParentID = param;
  }

  /** field for WorksheetWorkLocationText */
  protected java.lang.String localWorksheetWorkLocationText;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetWorkLocationTextTracker = false;

  public boolean isWorksheetWorkLocationTextSpecified() {
    return localWorksheetWorkLocationTextTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetWorkLocationText() {
    return localWorksheetWorkLocationText;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetWorkLocationText
   */
  public void setWorksheetWorkLocationText(java.lang.String param) {
    localWorksheetWorkLocationTextTracker = true;

    this.localWorksheetWorkLocationText = param;
  }

  /** field for RecordPosition */
  protected int localRecordPosition;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordPositionTracker = false;

  public boolean isRecordPositionSpecified() {
    return localRecordPositionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getRecordPosition() {
    return localRecordPosition;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordPosition
   */
  public void setRecordPosition(int param) {

    // setting primitive attribute tracker to true
    localRecordPositionTracker = param != java.lang.Integer.MIN_VALUE;

    this.localRecordPosition = param;
  }

  /** field for ImportationReference */
  protected java.lang.String localImportationReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localImportationReferenceTracker = false;

  public boolean isImportationReferenceSpecified() {
    return localImportationReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getImportationReference() {
    return localImportationReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param ImportationReference
   */
  public void setImportationReference(java.lang.String param) {
    localImportationReferenceTracker = true;

    this.localImportationReference = param;
  }

  /** field for WorksheetWorkLocationLatitude */
  protected double localWorksheetWorkLocationLatitude;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetWorkLocationLatitudeTracker = false;

  public boolean isWorksheetWorkLocationLatitudeSpecified() {
    return localWorksheetWorkLocationLatitudeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getWorksheetWorkLocationLatitude() {
    return localWorksheetWorkLocationLatitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetWorkLocationLatitude
   */
  public void setWorksheetWorkLocationLatitude(double param) {

    // setting primitive attribute tracker to true
    localWorksheetWorkLocationLatitudeTracker = !java.lang.Double.isNaN(param);

    this.localWorksheetWorkLocationLatitude = param;
  }

  /** field for WorksheetWorkLocationLongitude */
  protected double localWorksheetWorkLocationLongitude;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetWorkLocationLongitudeTracker = false;

  public boolean isWorksheetWorkLocationLongitudeSpecified() {
    return localWorksheetWorkLocationLongitudeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getWorksheetWorkLocationLongitude() {
    return localWorksheetWorkLocationLongitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetWorkLocationLongitude
   */
  public void setWorksheetWorkLocationLongitude(double param) {

    // setting primitive attribute tracker to true
    localWorksheetWorkLocationLongitudeTracker = !java.lang.Double.isNaN(param);

    this.localWorksheetWorkLocationLongitude = param;
  }

  /** field for WorksheetWorkLocationNorthing */
  protected double localWorksheetWorkLocationNorthing;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetWorkLocationNorthingTracker = false;

  public boolean isWorksheetWorkLocationNorthingSpecified() {
    return localWorksheetWorkLocationNorthingTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getWorksheetWorkLocationNorthing() {
    return localWorksheetWorkLocationNorthing;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetWorkLocationNorthing
   */
  public void setWorksheetWorkLocationNorthing(double param) {

    // setting primitive attribute tracker to true
    localWorksheetWorkLocationNorthingTracker = !java.lang.Double.isNaN(param);

    this.localWorksheetWorkLocationNorthing = param;
  }

  /** field for WorksheetWorkLocationEasting */
  protected double localWorksheetWorkLocationEasting;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetWorkLocationEastingTracker = false;

  public boolean isWorksheetWorkLocationEastingSpecified() {
    return localWorksheetWorkLocationEastingTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getWorksheetWorkLocationEasting() {
    return localWorksheetWorkLocationEasting;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetWorkLocationEasting
   */
  public void setWorksheetWorkLocationEasting(double param) {

    // setting primitive attribute tracker to true
    localWorksheetWorkLocationEastingTracker = !java.lang.Double.isNaN(param);

    this.localWorksheetWorkLocationEasting = param;
  }

  /** field for ForAction */
  protected boolean localForAction;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localForActionTracker = false;

  public boolean isForActionSpecified() {
    return localForActionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getForAction() {
    return localForAction;
  }

  /**
   * Auto generated setter method
   *
   * @param param ForAction
   */
  public void setForAction(boolean param) {

    // setting primitive attribute tracker to true
    localForActionTracker = true;

    this.localForAction = param;
  }

  /** field for WorksheetLastEventName */
  protected java.lang.String localWorksheetLastEventName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetLastEventNameTracker = false;

  public boolean isWorksheetLastEventNameSpecified() {
    return localWorksheetLastEventNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetLastEventName() {
    return localWorksheetLastEventName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetLastEventName
   */
  public void setWorksheetLastEventName(java.lang.String param) {
    localWorksheetLastEventNameTracker = true;

    this.localWorksheetLastEventName = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":WorksheetResult",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "WorksheetResult",
            xmlWriter);
      }
    }
    if (localWorksheetIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetID", xmlWriter);

      if (localWorksheetID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorksheetID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorksheetID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetCreatedByTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetCreatedBy", xmlWriter);

      if (localWorksheetCreatedBy == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetCreatedBy);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetAssignedToIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetAssignedToID", xmlWriter);

      if (localWorksheetAssignedToID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetAssignedToID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetAssignedToID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetAssignedToTypeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetAssignedToTypeID", xmlWriter);

      if (localWorksheetAssignedToTypeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetAssignedToTypeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetAssignedToTypeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetAssignedToNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetAssignedToName", xmlWriter);

      if (localWorksheetAssignedToName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetAssignedToName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetSubjectTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetSubject", xmlWriter);

      if (localWorksheetSubject == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetSubject);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetTypeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetType", xmlWriter);

      if (localWorksheetType == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetType);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetMessageTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetMessage", xmlWriter);

      if (localWorksheetMessage == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetMessage);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetReportedByContactIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetReportedByContactID", xmlWriter);

      if (localWorksheetReportedByContactID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetReportedByContactID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetReportedByContactID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetReportedByNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetReportedByName", xmlWriter);

      if (localWorksheetReportedByName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetReportedByName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetReportedByAddressIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetReportedByAddressID", xmlWriter);

      if (localWorksheetReportedByAddressID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetReportedByAddressID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetReportedByAddressID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetReportedByAddressTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetReportedByAddress", xmlWriter);

      if (localWorksheetReportedByAddress == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetReportedByAddress);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetWorkLocationAddressIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetWorkLocationAddressID", xmlWriter);

      if (localWorksheetWorkLocationAddressID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetWorkLocationAddressID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetWorkLocationAddressID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetWorkLocationNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetWorkLocationName", xmlWriter);

      if (localWorksheetWorkLocationName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetWorkLocationName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetWorkLocationAddressTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetWorkLocationAddress", xmlWriter);

      if (localWorksheetWorkLocationAddress == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetWorkLocationAddress);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetChargeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetChargeName", xmlWriter);

      if (localWorksheetChargeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetChargeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetChargeAddressIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetChargeAddressID", xmlWriter);

      if (localWorksheetChargeAddressID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetChargeAddressID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetChargeAddressID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetChargeAddressTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetChargeAddress", xmlWriter);

      if (localWorksheetChargeAddress == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetChargeAddress);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetCreatedDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetCreatedDate", xmlWriter);

      if (localWorksheetCreatedDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetCreatedDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetCreatedDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetStartDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetStartDate", xmlWriter);

      if (localWorksheetStartDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WorksheetStartDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetStartDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetDueByDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetDueByDate", xmlWriter);

      if (localWorksheetDueByDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WorksheetDueByDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetDueByDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetEscallatedDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetEscallatedDate", xmlWriter);

      if (localWorksheetEscallatedDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetEscallatedDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetEscallatedDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetCompletedDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetCompletedDate", xmlWriter);

      if (localWorksheetCompletedDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetCompletedDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetCompletedDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetPaymentDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetPaymentDate", xmlWriter);

      if (localWorksheetPaymentDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetPaymentDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetPaymentDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetExpiryDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetExpiryDate", xmlWriter);

      if (localWorksheetExpiryDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WorksheetExpiryDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetExpiryDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetApprovedDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetApprovedDate", xmlWriter);

      if (localWorksheetApprovedDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetApprovedDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetApprovedDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetPaymentMethodIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetPaymentMethodID", xmlWriter);

      if (localWorksheetPaymentMethodID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetPaymentMethodID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetPaymentMethodID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetPaymentReferenceTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetPaymentReference", xmlWriter);

      if (localWorksheetPaymentReference == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetPaymentReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetImportanceIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetImportanceID", xmlWriter);

      if (localWorksheetImportanceID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetImportanceID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetImportanceID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetImportanceNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetImportanceName", xmlWriter);

      if (localWorksheetImportanceName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetImportanceName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetStatusIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetStatusID", xmlWriter);

      if (localWorksheetStatusID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorksheetStatusID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetStatusID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetStatusNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetStatusName", xmlWriter);

      if (localWorksheetStatusName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetStatusName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetCompletionNotesTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetCompletionNotes", xmlWriter);

      if (localWorksheetCompletionNotes == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetCompletionNotes);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetGuidTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetGuid", xmlWriter);

      if (localWorksheetGuid == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetGuid);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetRefTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetRef", xmlWriter);

      if (localWorksheetRef == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetRef);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetParentIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetParentID", xmlWriter);

      if (localWorksheetParentID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorksheetParentID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetParentID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetWorkLocationTextTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetWorkLocationText", xmlWriter);

      if (localWorksheetWorkLocationText == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetWorkLocationText);
      }

      xmlWriter.writeEndElement();
    }
    if (localRecordPositionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "RecordPosition", xmlWriter);

      if (localRecordPosition == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("RecordPosition cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordPosition));
      }

      xmlWriter.writeEndElement();
    }
    if (localImportationReferenceTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ImportationReference", xmlWriter);

      if (localImportationReference == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localImportationReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetWorkLocationLatitudeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetWorkLocationLatitude", xmlWriter);

      if (java.lang.Double.isNaN(localWorksheetWorkLocationLatitude)) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetWorkLocationLatitude cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetWorkLocationLatitude));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetWorkLocationLongitudeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetWorkLocationLongitude", xmlWriter);

      if (java.lang.Double.isNaN(localWorksheetWorkLocationLongitude)) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetWorkLocationLongitude cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetWorkLocationLongitude));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetWorkLocationNorthingTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetWorkLocationNorthing", xmlWriter);

      if (java.lang.Double.isNaN(localWorksheetWorkLocationNorthing)) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetWorkLocationNorthing cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetWorkLocationNorthing));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetWorkLocationEastingTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetWorkLocationEasting", xmlWriter);

      if (java.lang.Double.isNaN(localWorksheetWorkLocationEasting)) {

        throw new org.apache.axis2.databinding.ADBException(
            "WorksheetWorkLocationEasting cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetWorkLocationEasting));
      }

      xmlWriter.writeEndElement();
    }
    if (localForActionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ForAction", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ForAction cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localForAction));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetLastEventNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetLastEventName", xmlWriter);

      if (localWorksheetLastEventName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetLastEventName);
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static WorksheetResult parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      WorksheetResult object = new WorksheetResult();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"WorksheetResult".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (WorksheetResult)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorksheetID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetCreatedBy")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetCreatedBy(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetAssignedToID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetAssignedToID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetAssignedToID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetAssignedToID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetAssignedToTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetAssignedToTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetAssignedToTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetAssignedToTypeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetAssignedToName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetAssignedToName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetSubject")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetSubject(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetType(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetMessage")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetMessage(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetReportedByContactID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetReportedByContactID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetReportedByContactID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetReportedByContactID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetReportedByName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetReportedByName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetReportedByAddressID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetReportedByAddressID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetReportedByAddressID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetReportedByAddressID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetReportedByAddress")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetReportedByAddress(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetWorkLocationAddressID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetWorkLocationAddressID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetWorkLocationAddressID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetWorkLocationAddressID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetWorkLocationName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetWorkLocationName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetWorkLocationAddress")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetWorkLocationAddress(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetChargeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetChargeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetChargeAddressID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetChargeAddressID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetChargeAddressID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetChargeAddressID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetChargeAddress")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetChargeAddress(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetCreatedDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetCreatedDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetCreatedDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetStartDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetStartDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetStartDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetDueByDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetDueByDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetDueByDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetEscallatedDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetEscallatedDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetEscallatedDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetCompletedDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetCompletedDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetCompletedDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetPaymentDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetPaymentDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetPaymentDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetExpiryDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetExpiryDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetExpiryDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetApprovedDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetApprovedDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetApprovedDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetPaymentMethodID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetPaymentMethodID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetPaymentMethodID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetPaymentMethodID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetPaymentReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetPaymentReference(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetImportanceID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetImportanceID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetImportanceID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetImportanceID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetImportanceName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetImportanceName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetStatusID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetStatusID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetStatusID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetStatusID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetStatusName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetStatusName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetCompletionNotes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetCompletionNotes(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetGuid")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetGuid(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorksheetRef")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetRef(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetParentID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetParentID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetParentID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetParentID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetWorkLocationText")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetWorkLocationText(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "RecordPosition")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "RecordPosition" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setRecordPosition(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setRecordPosition(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ImportationReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setImportationReference(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetWorkLocationLatitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetWorkLocationLatitude" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetWorkLocationLatitude(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetWorkLocationLatitude(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetWorkLocationLongitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetWorkLocationLongitude" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetWorkLocationLongitude(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetWorkLocationLongitude(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetWorkLocationNorthing")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetWorkLocationNorthing" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetWorkLocationNorthing(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetWorkLocationNorthing(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetWorkLocationEasting")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetWorkLocationEasting" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetWorkLocationEasting(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetWorkLocationEasting(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ForAction")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ForAction" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setForAction(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetLastEventName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetLastEventName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
