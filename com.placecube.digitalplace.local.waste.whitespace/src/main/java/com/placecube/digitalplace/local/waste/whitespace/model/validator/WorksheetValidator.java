package com.placecube.digitalplace.local.waste.whitespace.model.validator;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;

public interface WorksheetValidator {

	boolean isValid(Worksheet worksheet);
}
