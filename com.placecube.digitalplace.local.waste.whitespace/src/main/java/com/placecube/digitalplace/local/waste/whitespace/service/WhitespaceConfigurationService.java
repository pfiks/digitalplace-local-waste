package com.placecube.digitalplace.local.waste.whitespace.service;

import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.whitespace.configuration.WhitespaceCompanyConfiguration;
import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceConfigurationServiceUtil;

@Component(immediate = true, service = WhitespaceConfigurationService.class)
public class WhitespaceConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private WhitespaceConfigurationServiceUtil whitespaceConfigurationServiceUtil;

	@Reference
	private JSONFactory jsonFactory;

	public WhitespaceCompanyConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, companyId);
	}

	public String getPassword(WhitespaceCompanyConfiguration configuration) throws ConfigurationException {
		String password = configuration.password();

		if (Validator.isNull(password)) {
			throw new ConfigurationException("Password configuration cannot be empty");
		}

		return password;
	}

	public String getUsername(WhitespaceCompanyConfiguration configuration) throws ConfigurationException {
		String username = configuration.username();

		if (Validator.isNull(username)) {
			throw new ConfigurationException("Username configuration cannot be empty");
		}

		return username;
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		WhitespaceCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}

	public String getConfiguredWhitespaceServiceId(long companyId, String serviceType) throws PortalException {
		WhitespaceCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, companyId);

		Map<String, Object> formConfigurations = jsonFactory.looseDeserialize(configuration.formToServiceMapping(), Map.class);

		if (formConfigurations != null) {
			Optional<String> serviceId = formConfigurations.entrySet().stream()
					.map(x -> whitespaceConfigurationServiceUtil.getServiceIdForServiceTypeFromFormConfiguration((Map<String, Object>) x.getValue(), serviceType)).flatMap(Optional::stream)
					.findFirst();

			return serviceId.orElseThrow(() -> new ConfigurationException("Service type:  " + serviceType + " not configured in connector"));
		} else {
			throw new ConfigurationException("No form configurations found");
		}
	}

	public WhitespaceService getConfiguredDefaultWhitespaceService(long companyId, long formInstanceId) throws PortalException {
		return getConfiguredWhitespaceService(companyId, formInstanceId, "default");
	}

	public WhitespaceService getConfiguredWhitespaceService(long companyId, long formInstanceId, String serviceKey) throws PortalException {

		Map<String, WhitespaceService> whitespaceServiceMap = getConfiguredWhitespaceServiceMappingsByFormInstanceId(companyId, formInstanceId);

		WhitespaceService whitespaceService = whitespaceServiceMap.get(serviceKey);

		if (whitespaceService == null) {
			throw new ConfigurationException("No configured default service found for form instance: " + formInstanceId);
		}

		return whitespaceService;
	}

	public Map<String, WhitespaceService> getConfiguredWhitespaceServiceMappingsByFormInstanceId(long companyId, long formInstanceId) throws PortalException {
		WhitespaceCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(WhitespaceCompanyConfiguration.class, companyId);

		Map<String, Object> services = jsonFactory.looseDeserialize(configuration.formToServiceMapping(), Map.class);

		Map<String, Object> formConfiguration = (Map<String, Object>) services.get(String.valueOf(formInstanceId));

		if (formConfiguration != null) {
			return whitespaceConfigurationServiceUtil.getWhitespaceServicesFromFormConfiguration(formConfiguration);

		} else {
			throw new ConfigurationException("No services configured for form instance id: " + formInstanceId);
		}
	}

}
