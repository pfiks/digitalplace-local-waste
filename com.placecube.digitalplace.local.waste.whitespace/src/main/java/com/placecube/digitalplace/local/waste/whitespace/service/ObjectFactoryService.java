package com.placecube.digitalplace.local.waste.whitespace.service;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.WorksheetFilter;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.WorksheetSortOrder;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.*;
import com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfstring;
import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetContext.WorksheetContextBuilder;
import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetFilterWrapper;
import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetSortOrderWrapper;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.SameFinancialYearValidator;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.TwoLastWeeksValidator;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.WorksheetValidator;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

@Component(immediate = true, service = ObjectFactoryService.class)
public class ObjectFactoryService {

	public InputGetAccountSiteInput getInputGetAccountSiteInput() {
		return new InputGetAccountSiteInput();
	}

	public InputAddLogInput getInputAddLogInput() {
		return new InputAddLogInput();
	}

	public GetAddressesByCoordinatesRadiusInput getGetAddressesByCoordinatesRadiusInput() {
		return new GetAddressesByCoordinatesRadiusInput();
	}

	public GetAddressInput getGetAddressInput() {
		return new GetAddressInput();
	}

	public AddSiteAttachment getAddSiteAttachment() {
		return new AddSiteAttachment();
	}

	public InputAddSiteAttachmentBase64Input getInputAddSiteAttachmentBase64Input() {
		return new InputAddSiteAttachmentBase64Input();
	}

	public AddSiteContact getAddSiteContact() {
		return new AddSiteContact();
	}

	public InputAddSiteContactInput getInputAddSiteContactInput() {
		return new InputAddSiteContactInput();
	}

	public AddSiteLog getAddSiteLog() {
		return new AddSiteLog();
	}

	public AddSiteServiceItem getAddSiteServiceItem() {
		return new AddSiteServiceItem();
	}

	public InputAddSiteServiceItemInput getInputAddSiteServiceItemInput() {
		return new InputAddSiteServiceItemInput();
	}

	public AddSiteServiceItemRoundSchedule getAddSiteServiceItemRoundSchedule() {
		return new AddSiteServiceItemRoundSchedule();
	}

	public InputAddSiteServiceItemRoundScheduleInput getInputAddSiteServiceItemRoundScheduleInput() {
		return new InputAddSiteServiceItemRoundScheduleInput();
	}

	public AddSiteServiceNotification getAddSiteServiceNotification() {
		return new AddSiteServiceNotification();
	}

	public InputAddSiteServiceNotificationInput getInputAddSiteServiceNotificationInput() {
		return new InputAddSiteServiceNotificationInput();
	}

	public AddWorksheetAttachment getAddWorksheetAttachment() {
		return new AddWorksheetAttachment();
	}

	public InputAddWorksheetAttachmentBase64Input getInputAddWorksheetAttachmentBase64Input() {
		return new InputAddWorksheetAttachmentBase64Input();
	}

	public AddWorksheetNotes getAddWorksheetNotes() {
		return new AddWorksheetNotes();
	}

	public InputAddWorksheetNotesInput getInputAddWorksheetNotesInput() {
		return new InputAddWorksheetNotesInput();
	}

	public ArrayOfstring getArrayOfstring(List<String> list) {
		ArrayOfstring arrayOfString = new ArrayOfstring();
		list.forEach(arrayOfString::addString);

		return arrayOfString;
	}

	public CancelWorksheet getCancelWorksheet() {
		return new CancelWorksheet();
	}

	public GetCollectionByUprnAndDateInput getGetCollectionByUprnAndDateInput() {
		return new GetCollectionByUprnAndDateInput();
	}

	public InputGetCollectionSlotsInput getInputGetCollectionSlotsInput() {
		return new InputGetCollectionSlotsInput();
	}

	public CreateWorksheet getCreateWorksheet() {
		return new CreateWorksheet();
	}

	public InputCreateWorksheetInput getInputCreateWorksheetInput() {
		return new InputCreateWorksheetInput();
	}

	public DeleteSiteContact getDeleteSiteContact() {
		return new DeleteSiteContact();
	}

	public InputDeleteSiteContactInput getInputDeleteSiteContactInput() {
		return new InputDeleteSiteContactInput();
	}

	public DeleteSiteServiceItem getDeleteSiteServiceItem() {
		return new DeleteSiteServiceItem();
	}

	public InputDeleteSiteServiceItemInput getInputDeleteSiteServiceItemInput() {
		return new InputDeleteSiteServiceItemInput();
	}

	public DeleteSiteServiceItemRoundSchedule getDeleteSiteServiceItemRoundSchedule() {
		return new DeleteSiteServiceItemRoundSchedule();
	}

	public InputDeleteSiteServiceItemRoundScheduleInput getInputDeleteSiteServiceItemRoundScheduleInput() {
		return new InputDeleteSiteServiceItemRoundScheduleInput();
	}

	public GetAccountSiteId getGetAccountSiteId() {
		return new GetAccountSiteId();
	}

	public GetActiveAddresses getGetActiveAddresses() {
		return new GetActiveAddresses();
	}

	public GetAddresses getGetAddresses() {
		return new GetAddresses();
	}

	public GetAddressesByCoordinatesRadius getGetAddressesByCoordinatesRadius() {
		return new GetAddressesByCoordinatesRadius();
	}

	public GetCollectionByUprnAndDate getGetCollectionByUprnAndDate() {
		return new GetCollectionByUprnAndDate();
	}

	public GetCollectionSlots getGetCollectionSlots() {
		return new GetCollectionSlots();
	}

	public GetFullSiteCollections getGetFullSiteCollections() {
		return new GetFullSiteCollections();
	}

	public GetFullWorksheetDetails getGetFullWorksheetDetails() {
		return new GetFullWorksheetDetails();
	}

	public GetInCabLogs getGetInCabLogs() {
		return new GetInCabLogs();
	}

	public GetLogsSearch getGetLogsSearch() {
		return new GetLogsSearch();
	}

	public GetNotifications getGetNotifications() {
		return new GetNotifications();
	}

	public GetServiceItems getGetServiceItems() {
		return new GetServiceItems();
	}

	public InputGetServicePropertyInput getInputGetServicePropertyInput() {
		return new InputGetServicePropertyInput();
	}

	public GetServices getGetServices() {
		return new GetServices();
	}

	public GetSiteAttachments getGetSiteAttachments() {
		return new GetSiteAttachments();
	}

	public GetSiteAvailableRounds getGetSiteAvailableRounds() {
		return new GetSiteAvailableRounds();
	}

	public GetSiteCollectionExtraDetails getGetSiteCollectionExtraDetails() {
		return new GetSiteCollectionExtraDetails();
	}

	public GetSiteCollections getGetSiteCollections() {
		return new GetSiteCollections();
	}

	public GetSiteContacts getGetSiteContacts() {
		return new GetSiteContacts();
	}

	public GetSiteContracts getGetSiteContracts() {
		return new GetSiteContracts();
	}

	public GetSiteFlags getGetSiteFlags() {
		return new GetSiteFlags();
	}

	public GetSiteId getGetSiteId() {
		return new GetSiteId();
	}

	public GetSiteIncidents getGetSiteIncidents() {
		return new GetSiteIncidents();
	}

	public GetSiteInfo getGetSiteInfo() {
		return new GetSiteInfo();
	}

	public GetSiteLogs getGetSiteLogs() {
		return new GetSiteLogs();
	}

	public GetSiteNotifications getGetSiteNotifications() {
		return new GetSiteNotifications();
	}

	public GetSites getGetSites() {
		return new GetSites();
	}

	public GetSiteServiceItemRoundSchedules getGetSiteServiceItemRoundSchedules() {
		return new GetSiteServiceItemRoundSchedules();
	}

	public GetSiteWorksheets getGetSiteWorksheets() {
		return new GetSiteWorksheets();
	}

	public GetStreets getGetStreets() {
		return new GetStreets();
	}

	public GetWalkNumbers getGetWalkNumbers() {
		return new GetWalkNumbers();
	}

	public GetWorksheetAttachments getGetWorksheetAttachments() {
		return new GetWorksheetAttachments();
	}

	public GetWorksheetChargeMatrix getGetWorksheetChargeMatrix() {
		return new GetWorksheetChargeMatrix();
	}

	public GetWorksheetDetailEvents getGetWorksheetDetailEvents() {
		return new GetWorksheetDetailEvents();
	}

	public GetWorksheetDetailExtraInfoFields getGetWorksheetDetailExtraInfoFields() {
		return new GetWorksheetDetailExtraInfoFields();
	}

	public GetWorksheetDetailNotes getGetWorksheetDetailNotes() {
		return new GetWorksheetDetailNotes();
	}

	public GetWorksheetDetails getGetWorksheetDetails() {
		return new GetWorksheetDetails();
	}

	public GetWorksheetDetailServiceItems getGetWorksheetDetailServiceItems() {
		return new GetWorksheetDetailServiceItems();
	}

	public GetWorksheetExtraInfoFields getGetWorksheetExtraInfoFields() {
		return new GetWorksheetExtraInfoFields();
	}

	public InputGetWorksheetInput getInputGetWorksheetInput() {
		return new InputGetWorksheetInput();
	}

	public GetWorksheetRoles getGetWorksheetRoles() {
		return new GetWorksheetRoles();
	}

	public GetWorksheetsByReference getGetWorksheetsByReference() {
		return new GetWorksheetsByReference();
	}

	public GetWorksheetServiceItems getGetWorksheetServiceItems() {
		return new GetWorksheetServiceItems();
	}

	public InputGetInCabLogsInput getInputGetInCabLogsInput() {
		return new InputGetInCabLogsInput();
	}

	public InputGetLogInput getInputGetLogInput() {
		return new InputGetLogInput();
	}

	public InputGetLogSearchInput getInputGetLogSearchInput() {
		return new InputGetLogSearchInput();
	}

	public InputGetNotificationInput getInputGetNotificationInput() {
		return new InputGetNotificationInput();
	}

	public ProgressWorkflow getProgressWorkflow() {
		return new ProgressWorkflow();
	}

	public InputProgressWorkflowInput getInputProgressWorkflowInput() {
		return new InputProgressWorkflowInput();
	}

	public InputGetRoundIncidentInput getInputGetRoundIncidentInput() {
		return new InputGetRoundIncidentInput();
	}

	public InputServiceAndServiceItemInput getInputServiceAndServiceItemInput() {
		return new InputServiceAndServiceItemInput();
	}

	public InputGetServiceInput getInputGetServiceInput() {
		return new InputGetServiceInput();
	}

	public InputGetServiceItemInput getInputGetServiceItemInput() {
		return new InputGetServiceItemInput();
	}

	public InputGetServiceScheduleInput getInputGetServiceScheduleInput() {
		return new InputGetServiceScheduleInput();
	}

	public InputGetSiteAttachmentInput getInputGetSiteAttachmentInput() {
		return new InputGetSiteAttachmentInput();
	}

	public InputGetSiteContactInput getInputGetSiteContactInput() {
		return new InputGetSiteContactInput();
	}

	public InputGetSiteContractInput getInputGetSiteContractInput() {
		return new InputGetSiteContractInput();
	}

	public InputGetSiteFlagInput getInputGetSiteFlagInput() {
		return new InputGetSiteFlagInput();
	}

	public InputGetSiteInfoInput getInputGetSiteInfoInput() {
		return new InputGetSiteInfoInput();
	}

	public InputGetSiteInput getInputGetSiteInput() {
		return new InputGetSiteInput();
	}

	public InputGetSiteServiceInput getInputGetSiteServiceInput() {
		return new InputGetSiteServiceInput();
	}

	public InputGetSiteServiceItemRoundScheduleInput getInputGetSiteServiceItemRoundScheduleInput() {
		return new InputGetSiteServiceItemRoundScheduleInput();
	}

	public InputGetSiteServiceNotificationInput getInputGetSiteServiceNotificationInput() {
		return new InputGetSiteServiceNotificationInput();
	}

	public InputGetSiteServiceSiteServiceItemServiceItemPropertyInput getInputGetSiteServiceSiteServiceItemServiceItemPropertyInput() {
		return new InputGetSiteServiceSiteServiceItemServiceItemPropertyInput();
	}

	public InputGetSitesInput getInputGetSitesInput() {
		return new InputGetSitesInput();
	}

	public InputStreetInput getInputStreetInput() {
		return new InputStreetInput();
	}

	public UpdateSiteContact getUpdateSiteContact() {
		return new UpdateSiteContact();
	}

	public InputUpdateSiteContactInput getInputUpdateSiteContactInput() {
		return new InputUpdateSiteContactInput();
	}

	public UpdateSiteServiceItem getUpdateSiteServiceItem() {
		return new UpdateSiteServiceItem();
	}

	public InputUpdateSiteServiceItemInput getInputUpdateSiteServiceItemInput() {
		return new InputUpdateSiteServiceItemInput();
	}

	public UpdateSiteServiceNotification getUpdateSiteServiceNotification() {
		return new UpdateSiteServiceNotification();
	}

	public InputUpdateSiteServiceNotificationInput getInputUpdateSiteServiceNotificationInput() {
		return new InputUpdateSiteServiceNotificationInput();
	}

	public UpdateWorkflowEventDate getUpdateWorkflowEventDate() {
		return new UpdateWorkflowEventDate();
	}

	public InputUpdateWorkflowEventDateInput getInputUpdateWorkflowEventDateInput() {
		return new InputUpdateWorkflowEventDateInput();
	}

	public UpdateWorksheet getUpdateWorksheet() {
		return new UpdateWorksheet();
	}

	public InputUpdateWorksheetDetailInput getInputUpdateWorksheetDetailInput() {
		return new InputUpdateWorksheetDetailInput();
	}

	public InputGetWalkNumbersInput getInputGetWalkNumbersInput() {
		return new InputGetWalkNumbersInput();
	}

	public InputGetWorksheetAttachmentInput getInputGetWorksheetAttachmentInput() {
		return new InputGetWorksheetAttachmentInput();
	}

	public InputGetWorksheetDetailEventsInput getInputGetWorksheetDetailEventsInput() {
		return new InputGetWorksheetDetailEventsInput();
	}

	public InputGetWorksheetDetailExtraInfoFieldsInput getInputGetWorksheetDetailExtraInfoFieldsInput() {
		return new InputGetWorksheetDetailExtraInfoFieldsInput();
	}

	public InputGetWorksheetDetailNotesInput getInputGetWorksheetDetailNotesInput() {
		return new InputGetWorksheetDetailNotesInput();
	}

	public InputGetWorksheetDetailServiceItemsInput getInputGetWorksheetDetailServiceItemsInput() {
		return new InputGetWorksheetDetailServiceItemsInput();
	}

	public InputGetWorksheetDetailsInput getInputGetWorksheetDetailsInput() {
		return new InputGetWorksheetDetailsInput();
	}

	public WorksheetFilter getWorksheetFilter(String value) {
		return new WorksheetFilterWrapper(value);
	}

	public InputWorksheetInput getInputWorksheetInput() {
		return new InputWorksheetInput();
	}

	public InputGetWorksheetRolesInput getInputGetWorksheetRolesInput() {
		return new InputGetWorksheetRolesInput();
	}

	public InputGetWorksheetsByRefInput getInputGetWorksheetsByRefInput() {
		return new InputGetWorksheetsByRefInput();
	}

	public InputGetWorksheetServiceItemInput getInputGetWorksheetServiceItemInput() {
		return new InputGetWorksheetServiceItemInput();
	}

	public WorksheetSortOrder getWorksheetSortOrder(String value) {
		return new WorksheetSortOrderWrapper(value);
	}

	public WorksheetContextBuilder getWorksheetContextBuilder(long companyId, String uprn) {
		return new WorksheetContextBuilder(companyId, uprn);
	}

	public WorksheetValidator createSameFinancialYearValidator(WhitespaceWasteServiceUtil whitespaceWasteServiceUtil) {
		return new SameFinancialYearValidator(whitespaceWasteServiceUtil);
	}

	public WorksheetValidator createTwoLastWeeksValidator(WhitespaceWasteServiceUtil whitespaceWasteServiceUtil) {
		return new TwoLastWeeksValidator(whitespaceWasteServiceUtil);
	}

}