package com.placecube.digitalplace.local.waste.whitespace.constants;

public class WhitespaceWebServiceConstants {

	public static final int NO_RESULTS_ERROR_CODE = 6;

	private WhitespaceWebServiceConstants() {

	}
}
