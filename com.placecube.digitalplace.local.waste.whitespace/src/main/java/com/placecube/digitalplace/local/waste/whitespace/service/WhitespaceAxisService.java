package com.placecube.digitalplace.local.waste.whitespace.service;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.AxisFault;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.placecube.digitalplace.local.waste.whitespace.axis2.WSAPIServiceStub;
import com.placecube.digitalplace.local.waste.whitespace.configuration.WhitespaceCompanyConfiguration;
import com.placecube.digitalplace.local.waste.whitespace.constants.WhitespaceConfigConstants;
import com.placecube.digitalplace.local.waste.whitespace.handler.IncomingWSSecurityHandler;

@Component(immediate = true, service = WhitespaceAxisService.class)
public class WhitespaceAxisService {

	private static final String SECURITY_NAMESPACE_URI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

	@Reference
	private WhitespaceConfigurationService whitespaceWasteService;

	public WSAPIServiceStub getWhitespaceAxisService(long companyId) throws ConfigurationException, AxisFault {
		WhitespaceCompanyConfiguration configuration = whitespaceWasteService.getConfiguration(companyId);
		String endPointUrl = String.format("%s%s%s", HttpComponentsUtil.fixPath(configuration.whitespaceBaseUrl(), true, true), StringPool.FORWARD_SLASH,
				WhitespaceConfigConstants.END_POINT_SERVICE_NAME);

		WSAPIServiceStub wsapiServiceStub = new WSAPIServiceStub(endPointUrl);
		addBasicAuthHeader(wsapiServiceStub, configuration.username(), configuration.password());
		setIncomingWSSecurityHandler(wsapiServiceStub);

		return wsapiServiceStub;
	}

	private void setIncomingWSSecurityHandler(WSAPIServiceStub wsapiServiceStub) {
		wsapiServiceStub._getServiceClient().getAxisConfiguration().getInFlowPhases().get(0).getHandlers().add(new IncomingWSSecurityHandler());
	}

	private void addBasicAuthHeader(WSAPIServiceStub wsapiServiceStub, String username, String password) {
		OMFactory omFactory = OMAbstractFactory.getOMFactory();
		QName qName = new QName(SECURITY_NAMESPACE_URI, "Security", "wsse");
		OMNamespace userTokenNs = omFactory.createOMNamespace(SECURITY_NAMESPACE_URI, "wsse");
		OMElement omSecurityElement = omFactory.createOMElement(qName, null);

		OMNamespace omNamespace = omFactory.createOMNamespace("http://schemas.xmlsoap.org/soap/envelope/", "soapenv");
		omSecurityElement.addAttribute("mustUnderstand", "1", omNamespace);

		OMElement omusertoken = omFactory.createOMElement("UsernameToken", userTokenNs);
		omSecurityElement.addChild(omusertoken);

		OMElement omuserName = omFactory.createOMElement("Username", userTokenNs);
		omuserName.setText(username);
		omusertoken.addChild(omuserName);

		OMElement omPassword = omFactory.createOMElement("Password", userTokenNs);
		omPassword.setText(password);
		omusertoken.addChild(omPassword);

		wsapiServiceStub._getServiceClient().addHeader(omSecurityElement);
	}

}