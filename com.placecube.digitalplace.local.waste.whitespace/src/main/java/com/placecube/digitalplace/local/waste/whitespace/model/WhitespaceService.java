package com.placecube.digitalplace.local.waste.whitespace.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.liferay.portal.kernel.util.GetterUtil;

public class WhitespaceService {

	private String serviceId;

	private String collectionServiceName;

	private Map<String, Map<String, String>> serviceItemMappings;

	public WhitespaceService(Map<String, Object> properties) {
		serviceId = String.valueOf(properties.get("serviceId"));

		serviceItemMappings = new HashMap<>();

		Map<String, Object> serviceItemsMap = (Map<String, Object>) properties.get("serviceItemMappings");

		if (serviceItemsMap != null) {
			for (Entry<String, Object> serviceItem : serviceItemsMap.entrySet()) {
				serviceItemMappings.put(serviceItem.getKey(), (Map<String, String>) serviceItem.getValue());
			}
		}

		collectionServiceName = GetterUtil.getString(properties.get("collectionServiceName"));
	}

	public String getServiceId() {
		return serviceId;
	}

	public Map<String, String> getServiceItemMappings(String name) {
		return serviceItemMappings.get(name);
	}

	public String getCollectionServiceName() {
		return collectionServiceName;
	}
}
