/**
 * ServiceProperty.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** ServiceProperty bean class */
@SuppressWarnings({"unchecked", "unused"})
public class ServiceProperty implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ServiceProperty
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for ServicePropertyID */
  protected int localServicePropertyID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServicePropertyIDTracker = false;

  public boolean isServicePropertyIDSpecified() {
    return localServicePropertyIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServicePropertyID() {
    return localServicePropertyID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServicePropertyID
   */
  public void setServicePropertyID(int param) {

    // setting primitive attribute tracker to true
    localServicePropertyIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServicePropertyID = param;
  }

  /** field for ServicePropertyName */
  protected java.lang.String localServicePropertyName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServicePropertyNameTracker = false;

  public boolean isServicePropertyNameSpecified() {
    return localServicePropertyNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServicePropertyName() {
    return localServicePropertyName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServicePropertyName
   */
  public void setServicePropertyName(java.lang.String param) {
    localServicePropertyNameTracker = true;

    this.localServicePropertyName = param;
  }

  /** field for ServicePropertyDescription */
  protected java.lang.String localServicePropertyDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServicePropertyDescriptionTracker = false;

  public boolean isServicePropertyDescriptionSpecified() {
    return localServicePropertyDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServicePropertyDescription() {
    return localServicePropertyDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServicePropertyDescription
   */
  public void setServicePropertyDescription(java.lang.String param) {
    localServicePropertyDescriptionTracker = true;

    this.localServicePropertyDescription = param;
  }

  /** field for ServicePropertyTypeID */
  protected int localServicePropertyTypeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServicePropertyTypeIDTracker = false;

  public boolean isServicePropertyTypeIDSpecified() {
    return localServicePropertyTypeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServicePropertyTypeID() {
    return localServicePropertyTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServicePropertyTypeID
   */
  public void setServicePropertyTypeID(int param) {

    // setting primitive attribute tracker to true
    localServicePropertyTypeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServicePropertyTypeID = param;
  }

  /** field for ServicePropertyTypeName */
  protected java.lang.String localServicePropertyTypeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServicePropertyTypeNameTracker = false;

  public boolean isServicePropertyTypeNameSpecified() {
    return localServicePropertyTypeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServicePropertyTypeName() {
    return localServicePropertyTypeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServicePropertyTypeName
   */
  public void setServicePropertyTypeName(java.lang.String param) {
    localServicePropertyTypeNameTracker = true;

    this.localServicePropertyTypeName = param;
  }

  /** field for ServicePropertyDefaultValue */
  protected java.lang.String localServicePropertyDefaultValue;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServicePropertyDefaultValueTracker = false;

  public boolean isServicePropertyDefaultValueSpecified() {
    return localServicePropertyDefaultValueTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServicePropertyDefaultValue() {
    return localServicePropertyDefaultValue;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServicePropertyDefaultValue
   */
  public void setServicePropertyDefaultValue(java.lang.String param) {
    localServicePropertyDefaultValueTracker = true;

    this.localServicePropertyDefaultValue = param;
  }

  /** field for ForMobile */
  protected boolean localForMobile;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localForMobileTracker = false;

  public boolean isForMobileSpecified() {
    return localForMobileTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getForMobile() {
    return localForMobile;
  }

  /**
   * Auto generated setter method
   *
   * @param param ForMobile
   */
  public void setForMobile(boolean param) {

    // setting primitive attribute tracker to true
    localForMobileTracker = true;

    this.localForMobile = param;
  }

  /** field for ForPowersuite */
  protected boolean localForPowersuite;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localForPowersuiteTracker = false;

  public boolean isForPowersuiteSpecified() {
    return localForPowersuiteTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getForPowersuite() {
    return localForPowersuite;
  }

  /**
   * Auto generated setter method
   *
   * @param param ForPowersuite
   */
  public void setForPowersuite(boolean param) {

    // setting primitive attribute tracker to true
    localForPowersuiteTracker = true;

    this.localForPowersuite = param;
  }

  /** field for ForMoP */
  protected boolean localForMoP;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localForMoPTracker = false;

  public boolean isForMoPSpecified() {
    return localForMoPTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getForMoP() {
    return localForMoP;
  }

  /**
   * Auto generated setter method
   *
   * @param param ForMoP
   */
  public void setForMoP(boolean param) {

    // setting primitive attribute tracker to true
    localForMoPTracker = true;

    this.localForMoP = param;
  }

  /** field for IsReadOnly */
  protected boolean localIsReadOnly;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsReadOnlyTracker = false;

  public boolean isIsReadOnlySpecified() {
    return localIsReadOnlyTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsReadOnly() {
    return localIsReadOnly;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsReadOnly
   */
  public void setIsReadOnly(boolean param) {

    // setting primitive attribute tracker to true
    localIsReadOnlyTracker = true;

    this.localIsReadOnly = param;
  }

  /** field for IsCreateCompulsory */
  protected boolean localIsCreateCompulsory;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsCreateCompulsoryTracker = false;

  public boolean isIsCreateCompulsorySpecified() {
    return localIsCreateCompulsoryTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsCreateCompulsory() {
    return localIsCreateCompulsory;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsCreateCompulsory
   */
  public void setIsCreateCompulsory(boolean param) {

    // setting primitive attribute tracker to true
    localIsCreateCompulsoryTracker = true;

    this.localIsCreateCompulsory = param;
  }

  /** field for IsUpdateCompulsory */
  protected boolean localIsUpdateCompulsory;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsUpdateCompulsoryTracker = false;

  public boolean isIsUpdateCompulsorySpecified() {
    return localIsUpdateCompulsoryTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsUpdateCompulsory() {
    return localIsUpdateCompulsory;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsUpdateCompulsory
   */
  public void setIsUpdateCompulsory(boolean param) {

    // setting primitive attribute tracker to true
    localIsUpdateCompulsoryTracker = true;

    this.localIsUpdateCompulsory = param;
  }

  /** field for ServicePropertyOrder */
  protected int localServicePropertyOrder;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServicePropertyOrderTracker = false;

  public boolean isServicePropertyOrderSpecified() {
    return localServicePropertyOrderTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServicePropertyOrder() {
    return localServicePropertyOrder;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServicePropertyOrder
   */
  public void setServicePropertyOrder(int param) {

    // setting primitive attribute tracker to true
    localServicePropertyOrderTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServicePropertyOrder = param;
  }

  /** field for AuthorityID */
  protected int localAuthorityID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAuthorityIDTracker = false;

  public boolean isAuthorityIDSpecified() {
    return localAuthorityIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAuthorityID() {
    return localAuthorityID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AuthorityID
   */
  public void setAuthorityID(int param) {

    // setting primitive attribute tracker to true
    localAuthorityIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAuthorityID = param;
  }

  /** field for AuthorityName */
  protected java.lang.String localAuthorityName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAuthorityNameTracker = false;

  public boolean isAuthorityNameSpecified() {
    return localAuthorityNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAuthorityName() {
    return localAuthorityName;
  }

  /**
   * Auto generated setter method
   *
   * @param param AuthorityName
   */
  public void setAuthorityName(java.lang.String param) {
    localAuthorityNameTracker = true;

    this.localAuthorityName = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ServiceProperty",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ServiceProperty",
            xmlWriter);
      }
    }
    if (localServicePropertyIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServicePropertyID", xmlWriter);

      if (localServicePropertyID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServicePropertyID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServicePropertyID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServicePropertyNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServicePropertyName", xmlWriter);

      if (localServicePropertyName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServicePropertyName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServicePropertyDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServicePropertyDescription", xmlWriter);

      if (localServicePropertyDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServicePropertyDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localServicePropertyTypeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServicePropertyTypeID", xmlWriter);

      if (localServicePropertyTypeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "ServicePropertyTypeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServicePropertyTypeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServicePropertyTypeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServicePropertyTypeName", xmlWriter);

      if (localServicePropertyTypeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServicePropertyTypeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServicePropertyDefaultValueTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServicePropertyDefaultValue", xmlWriter);

      if (localServicePropertyDefaultValue == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServicePropertyDefaultValue);
      }

      xmlWriter.writeEndElement();
    }
    if (localForMobileTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ForMobile", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ForMobile cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localForMobile));
      }

      xmlWriter.writeEndElement();
    }
    if (localForPowersuiteTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ForPowersuite", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ForPowersuite cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localForPowersuite));
      }

      xmlWriter.writeEndElement();
    }
    if (localForMoPTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ForMoP", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ForMoP cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localForMoP));
      }

      xmlWriter.writeEndElement();
    }
    if (localIsReadOnlyTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "IsReadOnly", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("IsReadOnly cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsReadOnly));
      }

      xmlWriter.writeEndElement();
    }
    if (localIsCreateCompulsoryTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "IsCreateCompulsory", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("IsCreateCompulsory cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localIsCreateCompulsory));
      }

      xmlWriter.writeEndElement();
    }
    if (localIsUpdateCompulsoryTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "IsUpdateCompulsory", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("IsUpdateCompulsory cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localIsUpdateCompulsory));
      }

      xmlWriter.writeEndElement();
    }
    if (localServicePropertyOrderTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServicePropertyOrder", xmlWriter);

      if (localServicePropertyOrder == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException(
            "ServicePropertyOrder cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServicePropertyOrder));
      }

      xmlWriter.writeEndElement();
    }
    if (localAuthorityIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AuthorityID", xmlWriter);

      if (localAuthorityID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AuthorityID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAuthorityID));
      }

      xmlWriter.writeEndElement();
    }
    if (localAuthorityNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AuthorityName", xmlWriter);

      if (localAuthorityName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localAuthorityName);
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static ServiceProperty parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      ServiceProperty object = new ServiceProperty();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ServiceProperty".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (ServiceProperty)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServicePropertyID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServicePropertyID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServicePropertyID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServicePropertyID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServicePropertyName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServicePropertyName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServicePropertyDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServicePropertyDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServicePropertyTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServicePropertyTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServicePropertyTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServicePropertyTypeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServicePropertyTypeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServicePropertyTypeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServicePropertyDefaultValue")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServicePropertyDefaultValue(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ForMobile")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ForMobile" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setForMobile(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ForPowersuite")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ForPowersuite" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setForPowersuite(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ForMoP")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ForMoP" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setForMoP(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "IsReadOnly")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IsReadOnly" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIsReadOnly(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "IsCreateCompulsory")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IsCreateCompulsory" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIsCreateCompulsory(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "IsUpdateCompulsory")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IsUpdateCompulsory" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIsUpdateCompulsory(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServicePropertyOrder")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServicePropertyOrder" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServicePropertyOrder(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServicePropertyOrder(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "AuthorityID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AuthorityID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAuthorityID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAuthorityID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AuthorityName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setAuthorityName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
