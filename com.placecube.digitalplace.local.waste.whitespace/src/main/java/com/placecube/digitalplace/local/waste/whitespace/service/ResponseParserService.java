package com.placecube.digitalplace.local.waste.whitespace.service;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AdHocRoundInstanceResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ApiAdHocRoundInstance;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfApiAdHocRoundInstance;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfCollection;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfService;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfSiteService;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfWorksheet;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfWorksheetServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Collection;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CollectionResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CreateWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionByUprnAndDateResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionSlotsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServicesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteWorksheetsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteService;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteServiceResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.WorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.WorksheetServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.WorksheetServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfanyType;
import com.placecube.digitalplace.local.waste.whitespace.configuration.WhitespaceCompanyConfiguration;
import com.placecube.digitalplace.local.waste.whitespace.constants.WhitespaceWebServiceConstants;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceDateUtil;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

@Component(immediate = true, service = ResponseParserService.class)
public class ResponseParserService {

	private static final Log LOG = LogFactoryUtil.getLog(ResponseParserService.class);

	@Reference
	private WhitespaceConfigurationService whitespaceConfigurationService;

	@Reference
	private WhitespaceWasteServiceUtil whitespaceWasteServiceUtil;

	public Set<BinCollection> parseBinCollections(GetCollectionByUprnAndDateResponse getCollectionByUprnAndDateResponse, long companyId) {

		Set<BinCollection> binCollections = new LinkedHashSet<>();

		CollectionResponse collectionResponse = getCollectionByUprnAndDateResponse.getGetCollectionByUprnAndDateResult();
		if (Validator.isNotNull(collectionResponse)) {
			ArrayOfCollection arrayOfCollection = collectionResponse.getCollections();
			if (Validator.isNotNull(arrayOfCollection)) {
				Collection[] collections = arrayOfCollection.getCollection();

				if (Validator.isNotNull(collections)) {
					Locale locale = LocaleUtil.getDefault();
					Map<String, String> keyValuePairMap = getConfigurationKeyValuePairMap(companyId);
					for (Collection collection : collections) {
						String id = keyValuePairMap.getOrDefault(collection.getService(), collection.getService());
						String schedule = keyValuePairMap.getOrDefault(collection.getSchedule(), collection.getSchedule());

						Map<Locale, String> labelMap = new HashMap<>();
						labelMap.put(locale, collection.getService());

						BinCollection binCollection = new BinCollection.BinCollectionBuilder(id, WhitespaceDateUtil.parseDate_dd_MM_yyyy(collection.getDate()), labelMap)
								.formattedCollectionDate(collection.getDay() + StringPool.SPACE + schedule).frequency(whitespaceWasteServiceUtil.formatSchedule(schedule)).build();

						binCollections.add(binCollection);
					}
				}
			}
		}

		return binCollections;
	}

	public String parseCreateWorksheetResponse(CreateWorksheetResponse createWorksheetResponse) throws WasteRetrievalException {
		com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.webservices.CreateWorksheetResponse result = createWorksheetResponse.getCreateWorksheetResult();
		if (result.getSuccessFlag()) {
			ArrayOfanyType array = result.getWorksheetResponse();

			if (array.getAnyType().length > 0) {
				return (String) array.getAnyType()[0];
			} else {
				throw new WasteRetrievalException("Error creating worksheet: unexpected number of elements returned");
			}
		} else {
			throw new WasteRetrievalException("Worksheet could not be created: " + result.getErrorDescription());

		}

	}

	public Service[] parseGetServicesResponse(GetServicesResponse getServicesResponse) throws WasteRetrievalException {
		ServiceResponse serviceResponse = getServicesResponse.getGetServicesResult();

		if (serviceResponse.getSuccessFlag()) {
			ArrayOfService services = serviceResponse.getServices();

			if (services != null) {
				return services.getService() != null ? services.getService() : new Service[] {};
			} else {
				return new Service[] {};
			}
		} else {
			throw new WasteRetrievalException("Services could not be retrieved: " + serviceResponse.getErrorDescription());
		}
	}

	public ServiceItem[] parseGetServiceItemsResponse(GetServiceItemsResponse getServiceItemsResponse) throws WasteRetrievalException {

		ServiceItemResponse serviceItemResponse = getServiceItemsResponse.getGetServiceItemsResult();

		if (serviceItemResponse.getSuccessFlag()) {
			ArrayOfServiceItem serviceItems = serviceItemResponse.getServiceItems();

			if (serviceItems != null) {
				return serviceItems.getServiceItem() != null ? serviceItems.getServiceItem() : new ServiceItem[] {};
			} else {
				return new ServiceItem[] {};
			}
		} else {
			throw new WasteRetrievalException("Service items could not be retrieved: " + serviceItemResponse.getErrorDescription());
		}
	}

	public Worksheet[] parseGetSiteWorksheetsResponse(GetSiteWorksheetsResponse getSiteWorksheetsResponse) throws WasteRetrievalException {
		WorksheetResponse worksheetResponse = getSiteWorksheetsResponse.getGetSiteWorksheetsResult();

		if (worksheetResponse.getSuccessFlag()) {
			ArrayOfWorksheet worksheetsArray = worksheetResponse.getWorksheets();
			if (worksheetsArray != null) {
				return worksheetsArray.getWorksheet() != null ? worksheetsArray.getWorksheet() : new Worksheet[] {};
			} else {
				return new Worksheet[] {};
			}
		} else if (worksheetResponse.getErrorCode() == WhitespaceWebServiceConstants.NO_RESULTS_ERROR_CODE) {
			return new Worksheet[] {};
		}

		throw new WasteRetrievalException("Worksheets could not be retrieved" + worksheetResponse.getErrorDescription());
	}

	public WorksheetServiceItem[] parseGetWorksheetDetailServiceItemsResponse(GetWorksheetDetailServiceItemsResponse getWorksheetDetailServiceItemsResponse) throws WasteRetrievalException {
		WorksheetServiceItemResponse worksheetItemResponse = getWorksheetDetailServiceItemsResponse.getGetWorksheetDetailServiceItemsResult();

		if (worksheetItemResponse.getSuccessFlag()) {
			ArrayOfWorksheetServiceItem worksheetsServiceItemsArray = worksheetItemResponse.getWorksheetserviceitems();

			if (worksheetsServiceItemsArray != null) {
				return worksheetsServiceItemsArray.getWorksheetServiceItem() != null ? worksheetsServiceItemsArray.getWorksheetServiceItem() : new WorksheetServiceItem[] {};
			} else {
				return new WorksheetServiceItem[] {};
			}
		} else if (worksheetItemResponse.getErrorCode() == WhitespaceWebServiceConstants.NO_RESULTS_ERROR_CODE) {
			return new WorksheetServiceItem[] {};
		}

		throw new WasteRetrievalException("Worksheet service items could not be retrieved" + worksheetItemResponse.getErrorDescription());
	}

	public SiteService[] parseGetSiteCollectionsResponse(GetSiteCollectionsResponse getSiteCollectionsResponse) throws WasteRetrievalException {
		SiteServiceResponse siteServiceResponse = getSiteCollectionsResponse.getGetSiteCollectionsResult();

		if (siteServiceResponse.getSuccessFlag()) {
			ArrayOfSiteService siteServicesArray = siteServiceResponse.getSiteServices();

			if (siteServicesArray != null) {
				return siteServicesArray.getSiteService() != null ? siteServicesArray.getSiteService() : new SiteService[] {};
			} else {
				return new SiteService[] {};
			}
		} else if (siteServiceResponse.getErrorCode() == WhitespaceWebServiceConstants.NO_RESULTS_ERROR_CODE) {
			return new SiteService[] {};
		}

		throw new WasteRetrievalException("Site services could not be retrieved" + siteServiceResponse.getErrorDescription());
	}

	public ApiAdHocRoundInstance[] parseGetCollectionSlotsResponse(GetCollectionSlotsResponse getCollectionSlotsResponse) throws WasteRetrievalException {
		AdHocRoundInstanceResponse adHocRoundInstanceResponse = getCollectionSlotsResponse.getGetCollectionSlotsResult();

		if (adHocRoundInstanceResponse.getSuccessFlag()) {
			ArrayOfApiAdHocRoundInstance adHocRoundInstancesArray = adHocRoundInstanceResponse.getApiAdHocRoundInstances();

			if (adHocRoundInstancesArray != null) {
				return adHocRoundInstancesArray.getApiAdHocRoundInstance() != null ? adHocRoundInstancesArray.getApiAdHocRoundInstance() : new ApiAdHocRoundInstance[] {};
			} else {
				return new ApiAdHocRoundInstance[] {};
			}
		} else if (adHocRoundInstanceResponse.getErrorCode() == WhitespaceWebServiceConstants.NO_RESULTS_ERROR_CODE) {
			return new ApiAdHocRoundInstance[] {};
		}

		throw new WasteRetrievalException("Collection slots could not be retrieved" + adHocRoundInstanceResponse.getErrorDescription());
	}

	private Map<String, String> getConfigurationKeyValuePairMap(long companyId) {
		Map<String, String> keyValuePairMap = new HashMap<>();
		try {
			WhitespaceCompanyConfiguration configuration = whitespaceConfigurationService.getConfiguration(companyId);
			String[] keyValuePair = StringUtil.splitLines(configuration.keyValuePairMapping());

			for (String keyValue : keyValuePair) {
				String[] pair = keyValue.split(StringPool.EQUAL);
				if (pair.length > 1) {
					keyValuePairMap.put(pair[0], pair[1]);
				}
			}
		} catch (ConfigurationException e) {
			LOG.error(e);
		}
		return keyValuePairMap;
	}

}
