/**
 * Contact.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** Contact bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Contact implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = Contact
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for ContactID */
  protected int localContactID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactIDTracker = false;

  public boolean isContactIDSpecified() {
    return localContactIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getContactID() {
    return localContactID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactID
   */
  public void setContactID(int param) {

    // setting primitive attribute tracker to true
    localContactIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localContactID = param;
  }

  /** field for ContactName */
  protected java.lang.String localContactName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactNameTracker = false;

  public boolean isContactNameSpecified() {
    return localContactNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactName() {
    return localContactName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactName
   */
  public void setContactName(java.lang.String param) {
    localContactNameTracker = true;

    this.localContactName = param;
  }

  /** field for ContactCode */
  protected java.lang.String localContactCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactCodeTracker = false;

  public boolean isContactCodeSpecified() {
    return localContactCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactCode() {
    return localContactCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactCode
   */
  public void setContactCode(java.lang.String param) {
    localContactCodeTracker = true;

    this.localContactCode = param;
  }

  /** field for ContactHREF */
  protected java.lang.String localContactHREF;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactHREFTracker = false;

  public boolean isContactHREFSpecified() {
    return localContactHREFTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactHREF() {
    return localContactHREF;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactHREF
   */
  public void setContactHREF(java.lang.String param) {
    localContactHREFTracker = true;

    this.localContactHREF = param;
  }

  /** field for ContactTitle */
  protected java.lang.String localContactTitle;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactTitleTracker = false;

  public boolean isContactTitleSpecified() {
    return localContactTitleTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactTitle() {
    return localContactTitle;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactTitle
   */
  public void setContactTitle(java.lang.String param) {
    localContactTitleTracker = true;

    this.localContactTitle = param;
  }

  /** field for ContactFirstName */
  protected java.lang.String localContactFirstName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactFirstNameTracker = false;

  public boolean isContactFirstNameSpecified() {
    return localContactFirstNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactFirstName() {
    return localContactFirstName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactFirstName
   */
  public void setContactFirstName(java.lang.String param) {
    localContactFirstNameTracker = true;

    this.localContactFirstName = param;
  }

  /** field for ContactMiddleName */
  protected java.lang.String localContactMiddleName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactMiddleNameTracker = false;

  public boolean isContactMiddleNameSpecified() {
    return localContactMiddleNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactMiddleName() {
    return localContactMiddleName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactMiddleName
   */
  public void setContactMiddleName(java.lang.String param) {
    localContactMiddleNameTracker = true;

    this.localContactMiddleName = param;
  }

  /** field for ContactLastName */
  protected java.lang.String localContactLastName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactLastNameTracker = false;

  public boolean isContactLastNameSpecified() {
    return localContactLastNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactLastName() {
    return localContactLastName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactLastName
   */
  public void setContactLastName(java.lang.String param) {
    localContactLastNameTracker = true;

    this.localContactLastName = param;
  }

  /** field for ContactLocation */
  protected java.lang.String localContactLocation;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactLocationTracker = false;

  public boolean isContactLocationSpecified() {
    return localContactLocationTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactLocation() {
    return localContactLocation;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactLocation
   */
  public void setContactLocation(java.lang.String param) {
    localContactLocationTracker = true;

    this.localContactLocation = param;
  }

  /** field for ContactSalesZone */
  protected java.lang.String localContactSalesZone;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactSalesZoneTracker = false;

  public boolean isContactSalesZoneSpecified() {
    return localContactSalesZoneTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactSalesZone() {
    return localContactSalesZone;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactSalesZone
   */
  public void setContactSalesZone(java.lang.String param) {
    localContactSalesZoneTracker = true;

    this.localContactSalesZone = param;
  }

  /** field for UPRN */
  protected java.math.BigDecimal localUPRN;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUPRNTracker = false;

  public boolean isUPRNSpecified() {
    return localUPRNTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUPRN() {
    return localUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRN
   */
  public void setUPRN(java.math.BigDecimal param) {
    localUPRNTracker = param != null;

    this.localUPRN = param;
  }

  /** field for ContactTelephoneNumber */
  protected java.lang.String localContactTelephoneNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactTelephoneNumberTracker = false;

  public boolean isContactTelephoneNumberSpecified() {
    return localContactTelephoneNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactTelephoneNumber() {
    return localContactTelephoneNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactTelephoneNumber
   */
  public void setContactTelephoneNumber(java.lang.String param) {
    localContactTelephoneNumberTracker = true;

    this.localContactTelephoneNumber = param;
  }

  /** field for ContactMobileNumber */
  protected java.lang.String localContactMobileNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactMobileNumberTracker = false;

  public boolean isContactMobileNumberSpecified() {
    return localContactMobileNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactMobileNumber() {
    return localContactMobileNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactMobileNumber
   */
  public void setContactMobileNumber(java.lang.String param) {
    localContactMobileNumberTracker = true;

    this.localContactMobileNumber = param;
  }

  /** field for ContactFaxNumber */
  protected java.lang.String localContactFaxNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactFaxNumberTracker = false;

  public boolean isContactFaxNumberSpecified() {
    return localContactFaxNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactFaxNumber() {
    return localContactFaxNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactFaxNumber
   */
  public void setContactFaxNumber(java.lang.String param) {
    localContactFaxNumberTracker = true;

    this.localContactFaxNumber = param;
  }

  /** field for ContactEmail */
  protected java.lang.String localContactEmail;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactEmailTracker = false;

  public boolean isContactEmailSpecified() {
    return localContactEmailTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactEmail() {
    return localContactEmail;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactEmail
   */
  public void setContactEmail(java.lang.String param) {
    localContactEmailTracker = true;

    this.localContactEmail = param;
  }

  /** field for MainContact */
  protected boolean localMainContact;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localMainContactTracker = false;

  public boolean isMainContactSpecified() {
    return localMainContactTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getMainContact() {
    return localMainContact;
  }

  /**
   * Auto generated setter method
   *
   * @param param MainContact
   */
  public void setMainContact(boolean param) {

    // setting primitive attribute tracker to true
    localMainContactTracker = true;

    this.localMainContact = param;
  }

  /** field for Site */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
      localSite;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteTracker = false;

  public boolean isSiteSpecified() {
    return localSiteTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
      getSite() {
    return localSite;
  }

  /**
   * Auto generated setter method
   *
   * @param param Site
   */
  public void setSite(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
          param) {
    localSiteTracker = true;

    this.localSite = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Contact",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Contact", xmlWriter);
      }
    }
    if (localContactIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactID", xmlWriter);

      if (localContactID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ContactID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContactID));
      }

      xmlWriter.writeEndElement();
    }
    if (localContactNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactName", xmlWriter);

      if (localContactName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactCode", xmlWriter);

      if (localContactCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactHREFTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactHREF", xmlWriter);

      if (localContactHREF == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactHREF);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactTitleTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactTitle", xmlWriter);

      if (localContactTitle == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactTitle);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactFirstNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactFirstName", xmlWriter);

      if (localContactFirstName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactFirstName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactMiddleNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactMiddleName", xmlWriter);

      if (localContactMiddleName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactMiddleName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactLastNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactLastName", xmlWriter);

      if (localContactLastName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactLastName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactLocationTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactLocation", xmlWriter);

      if (localContactLocation == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactLocation);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactSalesZoneTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactSalesZone", xmlWriter);

      if (localContactSalesZone == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactSalesZone);
      }

      xmlWriter.writeEndElement();
    }
    if (localUPRNTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "UPRN", xmlWriter);

      if (localUPRN == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("UPRN cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
      }

      xmlWriter.writeEndElement();
    }
    if (localContactTelephoneNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactTelephoneNumber", xmlWriter);

      if (localContactTelephoneNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactTelephoneNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactMobileNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactMobileNumber", xmlWriter);

      if (localContactMobileNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactMobileNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactFaxNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactFaxNumber", xmlWriter);

      if (localContactFaxNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactFaxNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactEmailTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactEmail", xmlWriter);

      if (localContactEmail == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactEmail);
      }

      xmlWriter.writeEndElement();
    }
    if (localMainContactTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "MainContact", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("MainContact cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMainContact));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteTracker) {
      if (localSite == null) {

        writeStartElement(null, "http://webservices.whitespacews.com/", "Site", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localSite.serialize(
            new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Site"),
            xmlWriter);
      }
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Contact parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Contact object = new Contact();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Contact".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Contact)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setContactID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactHREF")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactHREF(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactTitle")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactTitle(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactFirstName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactFirstName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactMiddleName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactMiddleName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactLastName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactLastName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactLocation")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactLocation(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactSalesZone")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactSalesZone(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UPRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUPRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactTelephoneNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactTelephoneNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactMobileNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactMobileNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactFaxNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactFaxNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactEmail")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactEmail(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "MainContact")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "MainContact" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setMainContact(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Site")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setSite(null);
            reader.next();

            reader.next();

          } else {

            object.setSite(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                    .Site.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
