package com.placecube.digitalplace.local.waste.whitespace.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CreateWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.model.ServiceItemInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;
import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetContext;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.WorksheetValidator;

@Component(immediate = true, service = WhitespaceWorksheetService.class)
public class WhitespaceWorksheetService {

	@Reference
	private ResponseParserService responseParserService;

	@Reference
	private WhitespaceConfigurationService whitespaceConfigurationService;

	@Reference
	private WhitespaceWasteService whitespaceWasteService;

	@Reference
	private WhitespaceRequestService whiteSpaceRequestService;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	public String createWorksheetForDefaultService(WorksheetContext worksheetContext) throws WasteRetrievalException {
		return createWorksheet(worksheetContext, "default");
	}

	public String createWorksheet(WorksheetContext worksheetContext, String serviceKey) throws WasteRetrievalException {
		try {
			long companyId = worksheetContext.getCompanyId();
			String uprn = worksheetContext.getUprn();
			long formInstanceRecordId = worksheetContext.getFormInstanceRecordId();

			DDMFormInstanceRecord ddmFormInstanceRecord = ddmFormInstanceRecordLocalService.getDDMFormInstanceRecord(formInstanceRecordId);

			long formInstanceId = ddmFormInstanceRecord.getFormInstanceId();

			WhitespaceService whitespaceService = whitespaceConfigurationService.getConfiguredWhitespaceService(companyId, formInstanceId, serviceKey);
			List<String> serviceItemIds = getServiceItemIds(whitespaceService, worksheetContext.getBinTypes(), worksheetContext.getBinSizes(), formInstanceId);

			String worksheetReference = String.valueOf(formInstanceRecordId);
			String workLocationName = worksheetContext.getFirstName() + StringPool.SPACE + worksheetContext.getLastName();

			List<ServiceItem> whitespaceServiceItems = getServiceItems(companyId, whitespaceService, serviceItemIds);

			ServiceItemInputExt serviceItemInputExt = whitespaceWasteService.createServiceItemInputExt(whitespaceServiceItems, worksheetContext.getQuantities());

			CreateWorksheetResponse createWorksheetResponse = whiteSpaceRequestService.createWorksheet(companyId, null, serviceItemInputExt, uprn, null, whitespaceService.getServiceId(), null, null,
					null, String.valueOf(worksheetContext.getAdHocRoundInstanceId()), null, null, null, null, workLocationName, null, worksheetContext.getWorksheetMessage(), worksheetReference, null,
					null);

			return responseParserService.parseCreateWorksheetResponse(createWorksheetResponse);
		} catch (WasteRetrievalException e) {
			throw e;
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}
	}

	public boolean isWorksheetContextValidForDefaultService(WorksheetContext worksheetContext, long formInstanceId, Optional<WorksheetValidator> worksheetValidator) throws WasteRetrievalException {
		return isWorksheetContextValidForWorksheetCreation(worksheetContext, "default", formInstanceId, worksheetValidator);
	}

	public boolean isWorksheetContextValidForService(WorksheetContext worksheetContext, long formInstanceId, String serviceKey, Optional<WorksheetValidator> worksheetValidator) throws WasteRetrievalException {
		return isWorksheetContextValidForWorksheetCreation(worksheetContext, serviceKey, formInstanceId, worksheetValidator);
	}

	private boolean isWorksheetContextValidForWorksheetCreation(WorksheetContext worksheetContext, String serviceKey, long formInstanceId, Optional<WorksheetValidator> worksheetValidator)
			throws WasteRetrievalException {
		try {
			long companyId = worksheetContext.getCompanyId();
			String uprn = worksheetContext.getUprn();

			Map<String, WhitespaceService> serviceMappings = whitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(companyId, formInstanceId);

			WhitespaceService whitespaceService = serviceMappings.get(serviceKey);

			if (whitespaceService != null) {
				List<String> serviceItemIds = getServiceItemIds(whitespaceService, worksheetContext.getBinTypes(), worksheetContext.getBinSizes(), formInstanceId);

				Service service = whitespaceWasteService.getWhitespaceService(companyId, whitespaceService);

				return !whitespaceWasteService.existsOpenWorksheetByUprnServiceAndServiceItems(companyId, uprn, service.getServiceName(), serviceItemIds, worksheetValidator);
			} else {
				throw new WasteRetrievalException("Service " + serviceKey + " is not configured properly");
			}
		} catch (WasteRetrievalException e) {
			throw e;
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}
	}

	private List<ServiceItem> getServiceItems(long companyId, WhitespaceService whitespaceService, List<String> serviceItemIds) throws Exception {
		ArrayList<ServiceItem> serviceItems = new ArrayList<>();
		for (String serviceItemId : serviceItemIds) {
			serviceItems.add(whitespaceWasteService.getWhitespaceServiceItem(companyId, whitespaceService, serviceItemId));
		}
		return serviceItems;

	}

	private List<String> getServiceItemIds(WhitespaceService whitespaceService, List<String> binTypes, List<String> binSizes, long formInstanceId) throws WasteRetrievalException {
		ArrayList<String> serviceItemIds = new ArrayList<>();

		for (int i = 0; i < binTypes.size(); i++) {
			String binSize = binSizes.size() > i ? binSizes.get(i) : StringPool.BLANK;
			serviceItemIds.add(whitespaceWasteService.getServiceItemId(whitespaceService, binTypes.get(i), binSize, formInstanceId));
		}

		return serviceItemIds;
	}
}
