package com.placecube.digitalplace.local.waste.whitespace.service;

import java.time.format.DateTimeParseException;

import org.apache.axis2.AxisFault;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.whitespace.axis2.WSAPIServiceStub;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.WorksheetFilter;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.WorksheetSortOrder;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.*;
import com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfstring;
import com.placecube.digitalplace.local.waste.whitespace.model.ServiceItemInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.ServicePropertyInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.SiteCollectionExtraDetailInputExt;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceDateUtil;

@Component(immediate = true, service = WhitespaceRequestService.class)
public class WhitespaceRequestService {

	private static final Log LOG = LogFactoryUtil.getLog(WhitespaceRequestService.class);

	@Reference
	private JsonResponseParserService jsonResponseParserService;

	@Reference
	private ObjectFactoryService objectFactoryService;

	@Reference
	private WhitespaceAxisService whitespaceAxisService;

	@Reference
	private JSONFactory jsonFactory;

	public UpdateSiteServiceNotificationResponse updateSiteServiceNotification(long companyId, String notificationId, String siteServiceNotificationNotes, String serviceId, String siteId,
			String siteServiceNotificationId, String siteServiceNotificationValidFrom, String siteServiceNotificationValidTo, String uprn) throws Exception {

		InputUpdateSiteServiceNotificationInput input = objectFactoryService.getInputUpdateSiteServiceNotificationInput();
		input.setNotificationId(notificationId);
		input.setSiteServiceNotificationNotes(siteServiceNotificationNotes);
		input.setServiceId(serviceId);
		input.setSiteId(siteId);
		input.setSiteServiceNotificationId(siteServiceNotificationId);
		input.setSiteServiceNotificationValidFrom(siteServiceNotificationValidFrom);
		input.setSiteServiceNotificationValidTo(siteServiceNotificationValidTo);
		input.setUprn(uprn);

		UpdateSiteServiceNotification updateSiteServiceNotification = objectFactoryService.getUpdateSiteServiceNotification();
		updateSiteServiceNotification.setUpdateSiteServiceNotificationInput(input);

		return getWhitespaceService(companyId).updateSiteServiceNotification(updateSiteServiceNotification);
	}

	public AddSiteAttachmentResponse addSiteAttachment(long companyId, String uprn, String siteId, String fileName, String fileExtension, String base64String) throws Exception {

		AddSiteAttachment addSiteAttachment = objectFactoryService.getAddSiteAttachment();
		InputAddSiteAttachmentBase64Input input = objectFactoryService.getInputAddSiteAttachmentBase64Input();
		input.setUprn(uprn);
		input.setSiteId(siteId);
		input.setFileName(fileName);
		input.setFileExtension(fileExtension);
		input.setBase64String(base64String);

		addSiteAttachment.setAddSiteAttachmentInput(input);

		return getWhitespaceService(companyId).addSiteAttachment(addSiteAttachment);
	}

	public AddSiteContactResponse addSiteContact(long companyId, String uprn, String siteId, boolean isMainContact, String contactName, String contactEmail, String contactTelephoneNumber,
			String contactMobileNumber, String contactFaxNumber) throws Exception {

		AddSiteContact addSiteContact = objectFactoryService.getAddSiteContact();
		InputAddSiteContactInput input = objectFactoryService.getInputAddSiteContactInput();
		input.setUprn(uprn);
		input.setSiteId(siteId);
		input.setIsMainContact(isMainContact);
		input.setContactName(contactName);
		input.setContactEmail(contactEmail);
		input.setContactTelephoneNumber(contactTelephoneNumber);
		input.setContactMobileNumber(contactMobileNumber);
		input.setContactFaxNumber(contactFaxNumber);

		addSiteContact.setAddSiteContactInput(input);

		return getWhitespaceService(companyId).addSiteContact(addSiteContact);
	}

	public AddSiteLogResponse addSiteLog(long companyId, String uprn, String logText, String accountSiteId) throws Exception {

		AddSiteLog addSiteLog = objectFactoryService.getAddSiteLog();
		InputAddLogInput input = objectFactoryService.getInputAddLogInput();
		input.setUprn(uprn);
		input.setLogText(Validator.isNull(logText) ? StringPool.BLANK : logText);
		input.setAccountSiteId(accountSiteId);

		addSiteLog.setAddLogInput(input);

		return getWhitespaceService(companyId).addSiteLog(addSiteLog);
	}

	public AddSiteServiceItemResponse addSiteServiceItem(long companyId, String uprn, String siteId, String serviceId, String serviceItemId, String serviceItemQuantity, String serviceItemValidFrom,
			String serviceItemValidTo) throws Exception {

		InputAddSiteServiceItemInput input = objectFactoryService.getInputAddSiteServiceItemInput();
		input.setUprn(uprn);
		input.setSiteId(siteId);
		input.setServiceId(serviceId);
		input.setServiceItemId(serviceItemId);
		input.setServiceItemQuantity(serviceItemQuantity);
		input.setServiceItemValidFrom(serviceItemValidFrom);
		input.setServiceItemValidTo(serviceItemValidTo);

		GetSiteCollectionExtraDetailsResponse getSiteCollectionExtraDetailsResponse = getSiteCollectionExtraDetails(companyId, serviceId);

		ArrayOfInputSiteCollectionExtraDetailInput arrayOfInputSiteCollectionExtraDetailInput = SiteCollectionExtraDetailInputExt
				.getSiteCollectionExtraDetailsFromGetSiteCollectionExtraDetailsResponse(getSiteCollectionExtraDetailsResponse);
		input.setExtraDetailInputs(arrayOfInputSiteCollectionExtraDetailInput);

		AddSiteServiceItem addSiteServiceItem = objectFactoryService.getAddSiteServiceItem();
		addSiteServiceItem.setAddSiteServiceItemInput(input);

		return getWhitespaceService(companyId).addSiteServiceItem(addSiteServiceItem);
	}

	public AddSiteServiceItemRoundScheduleResponse addSiteServiceItemRoundSchedule(long companyId, String siteServiceId, String roundRoundAreaServiceScheduleId) throws Exception {

		AddSiteServiceItemRoundSchedule addSiteServiceItemRoundSchedule = objectFactoryService.getAddSiteServiceItemRoundSchedule();
		InputAddSiteServiceItemRoundScheduleInput input = objectFactoryService.getInputAddSiteServiceItemRoundScheduleInput();
		input.setSiteServiceId(siteServiceId);
		input.setRoundRoundAreaServiceScheduleId(roundRoundAreaServiceScheduleId);

		addSiteServiceItemRoundSchedule.setAddSiteServiceItemRoundScheduleInput(input);

		return getWhitespaceService(companyId).addSiteServiceItemRoundSchedule(addSiteServiceItemRoundSchedule);
	}

	public AddSiteServiceNotificationResponse addSiteServiceNotification(long companyId, String uprn, String siteId, String notificationId, String serviceId, String siteServiceNotificationNotes,
			String serviceNotificationValidFrom, String siteServiceNotificationValidTo) throws Exception {

		AddSiteServiceNotification addSiteServiceNotification = objectFactoryService.getAddSiteServiceNotification();
		InputAddSiteServiceNotificationInput input = objectFactoryService.getInputAddSiteServiceNotificationInput();
		input.setUprn(uprn);
		input.setSiteId(siteId);
		input.setNotificationId(notificationId);
		input.setServiceId(serviceId);
		input.setSiteServiceNotificationNotes(siteServiceNotificationNotes);
		input.setSiteServiceNotificationValidFrom(serviceNotificationValidFrom);
		input.setSiteServiceNotificationValidTo(siteServiceNotificationValidTo);

		addSiteServiceNotification.setAddSiteServiceNotificationInput(input);

		return getWhitespaceService(companyId).addSiteServiceNotification(addSiteServiceNotification);
	}

	public AddWorksheetAttachmentResponse addWorksheetAttachment(long companyId, String worksheetId, String fileName, String fileExtension, String base64String, String worksheetRef) throws Exception {

		AddWorksheetAttachment addWorksheetAttachment = objectFactoryService.getAddWorksheetAttachment();
		InputAddWorksheetAttachmentBase64Input input = objectFactoryService.getInputAddWorksheetAttachmentBase64Input();
		input.setWorksheetId(worksheetId);
		input.setFileName(fileName);
		input.setFileExtension(fileExtension);
		input.setBase64String(base64String);
		input.setWorksheetRef(worksheetRef);

		addWorksheetAttachment.setAddWorksheetAttachmentInput(input);

		return getWhitespaceService(companyId).addWorksheetAttachment(addWorksheetAttachment);
	}

	public AddWorksheetNotesResponse addWorksheetNotes(long companyId, String worksheetId, String worksheetRef, String noteText) throws Exception {

		AddWorksheetNotes addWorksheetNotes = objectFactoryService.getAddWorksheetNotes();
		InputAddWorksheetNotesInput input = objectFactoryService.getInputAddWorksheetNotesInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		input.setNoteText(noteText);

		addWorksheetNotes.setAddWorksheetNotesInput(input);

		return getWhitespaceService(companyId).addWorksheetNotes(addWorksheetNotes);
	}

	public CancelWorksheetResponse cancelWorksheet(long companyId, String worksheetId, String worksheetRef) throws Exception {

		CancelWorksheet cancelWorksheet = objectFactoryService.getCancelWorksheet();
		InputWorksheetInput input = objectFactoryService.getInputWorksheetInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);

		cancelWorksheet.setCancelWorksheetInput(input);

		return getWhitespaceService(companyId).cancelWorksheet(cancelWorksheet);
	}

	public CreateWorksheetResponse createWorksheet(long companyId, ServicePropertyInputExt serviceProperties, ServiceItemInputExt serviceItems, String uprn, String usrn, String serviceId,
			String accountSiteId, Integer roleId, String roleName, String adHocRoundInstanceId, String workLocationAddress, String workLocationEasting, String workLocationLatitude,
			String workLocationLongitude, String workLocationName, String workLocationNorthing, String worksheetMessage, String worksheetReference, String worksheetDueDate, String worksheetStartDate)
			throws Exception {
		long start = System.currentTimeMillis();

		CreateWorksheet createWorksheet = objectFactoryService.getCreateWorksheet();
		InputCreateWorksheetInput input = objectFactoryService.getInputCreateWorksheetInput();
		input.setUprn(uprn);
		input.setUsrn(usrn);
		input.setServiceId(serviceId);
		input.setAccountSiteId(accountSiteId);
		if (Validator.isNotNull(roleId)) {
			input.setRoleId(roleId);
		}
		input.setRoleName(roleName);
		input.setAdHocRoundInstanceId(adHocRoundInstanceId);
		input.setWorkLocationAddress(workLocationAddress);
		input.setWorkLocationEasting(workLocationEasting);
		input.setWorkLocationLatitude(workLocationLatitude);
		input.setWorkLocationLongitude(workLocationLongitude);
		input.setWorkLocationName(workLocationName);
		input.setWorkLocationNorthing(workLocationNorthing);
		input.setWorksheetMessage(worksheetMessage);
		input.setWorksheetReference(worksheetReference);
		input.setWorksheetDueDate(worksheetDueDate);
		input.setWorksheetStartDate(worksheetStartDate);
		if (Validator.isNotNull(serviceProperties)) {
			input.setServicePropertyInputs(serviceProperties.getServicePropertyInputs());
		}

		if (Validator.isNotNull(serviceItems)) {
			input.setServiceItemInputs(serviceItems.getServiceItemInputs());
		}

		createWorksheet.setWorksheetInput(input);

		LOG.debug("Whitespace Create worksheet request: " + jsonFactory.serialize(createWorksheet));

		CreateWorksheetResponse response = getWhitespaceService(companyId).createWorksheet(createWorksheet);

		LOG.debug("Whitespace Create worksheet response: " + jsonFactory.serialize(response));

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Whitespace Create worksheet execution time: " + timeElapsed);

		return response;
	}

	public DeleteSiteContactResponse deleteSiteContact(long companyId, String contactId) throws Exception {

		InputDeleteSiteContactInput input = objectFactoryService.getInputDeleteSiteContactInput();
		input.setContactId(contactId);
		DeleteSiteContact deleteSiteContact = objectFactoryService.getDeleteSiteContact();
		deleteSiteContact.setDeleteSiteContactInput(input);

		return getWhitespaceService(companyId).deleteSiteContact(deleteSiteContact);
	}

	public DeleteSiteServiceItemResponse deleteSiteServiceItem(long companyId, String siteServiceId) throws Exception {

		InputDeleteSiteServiceItemInput input = objectFactoryService.getInputDeleteSiteServiceItemInput();
		input.setSiteServiceId(siteServiceId);
		DeleteSiteServiceItem deleteSiteServiceItem = objectFactoryService.getDeleteSiteServiceItem();
		deleteSiteServiceItem.setDeleteSiteServiceItemInput(input);

		return getWhitespaceService(companyId).deleteSiteServiceItem(deleteSiteServiceItem);
	}

	public DeleteSiteServiceItemRoundScheduleResponse deleteSiteServiceItemRoundSchedule(long companyId, String siteServiceId, String roundRoundAreaServiceScheduleId) throws Exception {

		InputDeleteSiteServiceItemRoundScheduleInput input = objectFactoryService.getInputDeleteSiteServiceItemRoundScheduleInput();
		input.setSiteServiceId(siteServiceId);
		input.setRoundRoundAreaServiceScheduleId(roundRoundAreaServiceScheduleId);
		DeleteSiteServiceItemRoundSchedule deleteSiteServiceItemRoundSchedule = objectFactoryService.getDeleteSiteServiceItemRoundSchedule();
		deleteSiteServiceItemRoundSchedule.setDeleteSiteServiceItemRoundScheduleInput(input);

		return getWhitespaceService(companyId).deleteSiteServiceItemRoundSchedule(deleteSiteServiceItemRoundSchedule);
	}

	public GetAccountSiteIdResponse getAccountSiteId(long companyId, String siteId, String uprn) throws Exception {

		InputGetSiteInput input = objectFactoryService.getInputGetSiteInput();
		input.setSiteId(siteId);
		input.setUPRN(uprn);
		GetAccountSiteId getAccountSiteId = objectFactoryService.getGetAccountSiteId();
		getAccountSiteId.setSiteInput(input);

		return getWhitespaceService(companyId).getAccountSiteId(getAccountSiteId);
	}

	public GetActiveAddressesResponse getActiveAddresses(long companyId, String addressLine1, String addressLine2, String addressNameNumber, String county, String country, String postalCode,
			String town) throws Exception {

		GetAddressInput input = objectFactoryService.getGetAddressInput();
		input.setAddressLine1(addressLine1);
		input.setAddressLine2(addressLine2);
		input.setAddressNameNumber(addressNameNumber);
		input.setCounty(county);
		input.setCountry(country);
		input.setPostcode(postalCode);
		input.setTown(town);
		GetActiveAddresses getActiveAddresses = objectFactoryService.getGetActiveAddresses();
		getActiveAddresses.setGetActiveAddressInput(input);

		return getWhitespaceService(companyId).getActiveAddresses(getActiveAddresses);
	}

	public GetAddressesResponse getAddresses(long companyId, String addressLine1, String addressLine2, String addressNameNumber, String county, String country, String postalCode, String town)
			throws Exception {

		GetAddressInput input = objectFactoryService.getGetAddressInput();
		input.setAddressLine1(addressLine1);
		input.setAddressLine2(addressLine2);
		input.setAddressNameNumber(addressNameNumber);
		input.setCounty(county);
		input.setCountry(country);
		input.setPostcode(postalCode);
		input.setTown(town);
		GetAddresses getAddresses = objectFactoryService.getGetAddresses();
		getAddresses.setGetAddressInput(input);

		return getWhitespaceService(companyId).getAddresses(getAddresses);
	}

	public GetAddressesByCoordinatesRadiusResponse getAddressesByCoordinatesRadius(long companyId, String latitude, String longitude, String radiusMetres) throws Exception {

		GetAddressesByCoordinatesRadiusInput input = objectFactoryService.getGetAddressesByCoordinatesRadiusInput();
		input.setLatitude(latitude);
		input.setLongitude(longitude);
		input.setRadiusMetres(radiusMetres);
		GetAddressesByCoordinatesRadius getAddressesByCoordinatesRadius = objectFactoryService.getGetAddressesByCoordinatesRadius();
		getAddressesByCoordinatesRadius.setGetAddressesByCoordinatesRadiusInput(input);

		return getWhitespaceService(companyId).getAddressesByCoordinatesRadius(getAddressesByCoordinatesRadius);
	}

	public GetCollectionByUprnAndDateResponse getCollectionByUprnAndDate(long companyId, String uprn, String nextCollectionFromDate) throws Exception {

		GetCollectionByUprnAndDateInput input = objectFactoryService.getGetCollectionByUprnAndDateInput();
		try {
			nextCollectionFromDate = WhitespaceDateUtil.formatDate_from_yyyy_MM_dd_to_dd_MM_yyyy(nextCollectionFromDate);
		} catch (DateTimeParseException e) {
			// do nothing
		}

		input.setNextCollectionFromDate(nextCollectionFromDate);
		input.setUprn(uprn);
		GetCollectionByUprnAndDate getCollectionByUprnAndDate = objectFactoryService.getGetCollectionByUprnAndDate();
		getCollectionByUprnAndDate.setGetCollectionByUprnAndDateInput(input);

		return getWhitespaceService(companyId).getCollectionByUprnAndDate(getCollectionByUprnAndDate);

	}

	public GetCollectionSlotsResponse getCollectionSlots(long companyId, String uprn, String serviceId, String siteId, String listCount, String nextCollectionFromDate, String nextCollectionToDate)
			throws Exception {

		InputGetCollectionSlotsInput input = objectFactoryService.getInputGetCollectionSlotsInput();
		input.setUprn(uprn);
		input.setServiceId(serviceId);
		input.setSiteId(siteId);
		input.setListCount(listCount);
		input.setNextCollectionFromDate(nextCollectionFromDate);
		input.setNextCollectionToDate(nextCollectionToDate);

		GetCollectionSlots getCollectionSlots = objectFactoryService.getGetCollectionSlots();
		getCollectionSlots.setCollectionSlotsInputInput(input);

		return getWhitespaceService(companyId).getCollectionSlots(getCollectionSlots);
	}

	public GetFullSiteCollectionsResponse getFullSiteCollections(long companyId, String uprn, String siteId) throws Exception {

		InputGetSiteServiceInput input = objectFactoryService.getInputGetSiteServiceInput();
		input.setUprn(uprn);
		input.setSiteId(siteId);
		GetFullSiteCollections getFullSiteCollections = objectFactoryService.getGetFullSiteCollections();
		getFullSiteCollections.setSiteserviceInput(input);

		return getWhitespaceService(companyId).getFullSiteCollections(getFullSiteCollections);
	}

	public GetFullWorksheetDetailsResponse getFullWorksheetDetails(long companyId, String worksheetId, String worksheetRef) throws Exception {

		InputWorksheetInput input = objectFactoryService.getInputWorksheetInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		GetFullWorksheetDetails getFullWorksheetDetails = objectFactoryService.getGetFullWorksheetDetails();
		getFullWorksheetDetails.setFullworksheetDetailsInput(input);

		return getWhitespaceService(companyId).getFullWorksheetDetails(getFullWorksheetDetails);
	}

	public GetInCabLogsResponse getInCabLogs(long companyId, String uprn, String usrn, String logFromDate, String logToDate, ArrayOfstring logTypeID, ArrayOfstring reason, ArrayOfstring worksheetType)
			throws Exception {

		InputGetInCabLogsInput input = objectFactoryService.getInputGetInCabLogsInput();
		input.setUprn(uprn);
		input.setUsrn(usrn);
		input.setLogFromDate(logFromDate);
		input.setLogToDate(logToDate);
		input.setLogTypeID(logTypeID);
		input.setReason(reason);
		input.setWorksheetType(worksheetType);
		GetInCabLogs getInCabLogs = objectFactoryService.getGetInCabLogs();
		getInCabLogs.setInCabLogInput(input);

		return getWhitespaceService(companyId).getInCabLogs(getInCabLogs);
	}

	public GetLogsSearchResponse getLogsSearch(long companyId, String accountSiteId, String uprn, String logFromDate, String logToDate, String logMessageSearchString, String logReferenceSearchString,
			String logSubjectSearchString, ArrayOfstring logTypeID) throws Exception {

		InputGetLogSearchInput input = objectFactoryService.getInputGetLogSearchInput();
		input.setAccountSiteId(accountSiteId);
		input.setUprn(uprn);
		input.setLogFromDate(logFromDate);
		input.setLogToDate(logToDate);
		input.setLogMessageSearchString(logMessageSearchString);
		input.setLogReferenceSearchString(logReferenceSearchString);
		input.setLogSubjectSearchString(logSubjectSearchString);
		input.setLogTypeID(logTypeID);
		GetLogsSearch getLogsSearch = objectFactoryService.getGetLogsSearch();
		getLogsSearch.setLogInput(input);

		return getWhitespaceService(companyId).getLogsSearch(getLogsSearch);
	}

	public GetNotificationsResponse getNotifications(long companyId, String notificationId) throws Exception {

		InputGetNotificationInput input = objectFactoryService.getInputGetNotificationInput();
		input.setNotificationId(notificationId);
		GetNotifications getNotifications = objectFactoryService.getGetNotifications();
		getNotifications.setNotificationInput(input);

		return getWhitespaceService(companyId).getNotifications(getNotifications);
	}

	public GetServiceItemsResponse getServiceItems(long companyId, String serviceId, String serviceItemId) throws Exception {
		long start = System.currentTimeMillis();
		InputGetServiceItemInput input = objectFactoryService.getInputGetServiceItemInput();
		input.setServiceId(serviceId);
		input.setServiceItemId(serviceItemId);
		GetServiceItems getServiceItems = objectFactoryService.getGetServiceItems();
		getServiceItems.setServiceItemInput(input);

		LOG.debug("Whitespace Get service items request: " + jsonFactory.serialize(getServiceItems));

		GetServiceItemsResponse response = getWhitespaceService(companyId).getServiceItems(getServiceItems);

		LOG.debug("Whitespace Get service items response: " + jsonFactory.serialize(response));

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Whitespace Get sevice items execution time: " + timeElapsed);

		return response;
	}

	public GetServicesResponse getServices(long companyId, String serviceId) throws Exception {

		long start = System.currentTimeMillis();
		InputGetServiceInput input = objectFactoryService.getInputGetServiceInput();
		input.setServiceId(serviceId);
		GetServices getServices = objectFactoryService.getGetServices();
		getServices.setServiceInput(input);

		LOG.debug("Whitespace Get services request: " + jsonFactory.serialize(getServices));

		GetServicesResponse response = getWhitespaceService(companyId).getServices(getServices);

		LOG.debug("Whitespace Get services response: " + jsonFactory.serialize(response));

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Whitespace Get services execution time: " + timeElapsed);

		return response;
	}

	public GetSiteAttachmentsResponse getSiteAttachments(long companyId, String siteId, String uprn, boolean includeData) throws Exception {

		InputGetSiteAttachmentInput input = objectFactoryService.getInputGetSiteAttachmentInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		input.setIncludeData(includeData);
		GetSiteAttachments getSiteAttachments = objectFactoryService.getGetSiteAttachments();
		getSiteAttachments.setGetSiteAttachmentsInput(input);

		return getWhitespaceService(companyId).getSiteAttachments(getSiteAttachments);
	}

	public GetSiteAvailableRoundsResponse getSiteAvailableRounds(long companyId, String siteId, String uprn) throws Exception {

		InputGetServiceScheduleInput input = objectFactoryService.getInputGetServiceScheduleInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		GetSiteAvailableRounds getSiteAvailableRounds = objectFactoryService.getGetSiteAvailableRounds();
		getSiteAvailableRounds.setServicescheduleInput(input);

		return getWhitespaceService(companyId).getSiteAvailableRounds(getSiteAvailableRounds);
	}

	public GetSiteCollectionExtraDetailsResponse getSiteCollectionExtraDetails(long companyId, String siteServiceId) throws Exception {

		InputGetSiteServiceSiteServiceItemServiceItemPropertyInput input = objectFactoryService.getInputGetSiteServiceSiteServiceItemServiceItemPropertyInput();
		input.setSiteServiceId(siteServiceId);
		GetSiteCollectionExtraDetails getSiteCollectionExtraDetails = objectFactoryService.getGetSiteCollectionExtraDetails();
		getSiteCollectionExtraDetails.setSsssipInput(input);

		return getWhitespaceService(companyId).getSiteCollectionExtraDetails(getSiteCollectionExtraDetails);
	}

	public GetSiteCollectionsResponse getSiteCollections(long companyId, String siteId, String uprn, boolean includeCommercial) throws Exception {
		long start = System.currentTimeMillis();
		InputGetSiteServiceInput input = objectFactoryService.getInputGetSiteServiceInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		input.setIncludeCommercial(includeCommercial);
		GetSiteCollections getSiteCollections = objectFactoryService.getGetSiteCollections();
		getSiteCollections.setSiteserviceInput(input);

		LOG.debug("Whitespace Get site collections request: " + jsonFactory.serialize(getSiteCollections));

		GetSiteCollectionsResponse response = getWhitespaceService(companyId).getSiteCollections(getSiteCollections);

		LOG.debug("Whitespace Get site collections response: " + jsonFactory.serialize(response));

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Whitespace Get site collections execution time: " + timeElapsed);

		return response;
	}

	public GetSiteContactsResponse getSiteContacts(long companyId, String siteId, String uprn) throws Exception {

		InputGetSiteContactInput input = objectFactoryService.getInputGetSiteContactInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		GetSiteContacts getSiteContacts = objectFactoryService.getGetSiteContacts();
		getSiteContacts.setSiteContactInput(input);

		return getWhitespaceService(companyId).getSiteContacts(getSiteContacts);
	}

	public GetSiteContractsResponse getSiteContracts(long companyId, String siteId, String uprn) throws Exception {

		InputGetSiteContractInput input = objectFactoryService.getInputGetSiteContractInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		GetSiteContracts getSiteContracts = objectFactoryService.getGetSiteContracts();
		getSiteContracts.setSitecontractInput(input);

		return getWhitespaceService(companyId).getSiteContracts(getSiteContracts);
	}

	public GetSiteFlagsResponse getSiteFlags(long companyId, String siteId, String uprn) throws Exception {

		InputGetSiteFlagInput input = objectFactoryService.getInputGetSiteFlagInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		GetSiteFlags getSiteFlags = objectFactoryService.getGetSiteFlags();
		getSiteFlags.setSiteflagInput(input);

		return getWhitespaceService(companyId).getSiteFlags(getSiteFlags);
	}

	public GetSiteIdResponse getSiteId(long companyId, String accountSiteId, String uprn) throws Exception {

		InputGetAccountSiteInput input = objectFactoryService.getInputGetAccountSiteInput();
		input.setAccountSiteId(accountSiteId);
		input.setUPRN(uprn);
		GetSiteId getSiteId = objectFactoryService.getGetSiteId();
		getSiteId.setSiteInput(input);

		return getWhitespaceService(companyId).getSiteId(getSiteId);
	}

	public GetSiteIncidentsResponse getSiteIncidents(long companyId, String siteId, String uprn) throws Exception {

		InputGetRoundIncidentInput input = objectFactoryService.getInputGetRoundIncidentInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		GetSiteIncidents getSiteIncidents = objectFactoryService.getGetSiteIncidents();
		getSiteIncidents.setRoundIncidentInput(input);

		return getWhitespaceService(companyId).getSiteIncidents(getSiteIncidents);
	}

	public GetSiteInfoResponse getSiteInfo(long companyId, String accountSiteId, String uprn) throws Exception {

		InputGetSiteInfoInput input = objectFactoryService.getInputGetSiteInfoInput();
		input.setAccountSiteId(accountSiteId);
		input.setUprn(uprn);
		GetSiteInfo getSiteInfo = objectFactoryService.getGetSiteInfo();
		getSiteInfo.setSiteInfoInput(input);

		return getWhitespaceService(companyId).getSiteInfo(getSiteInfo);
	}

	public GetSiteLogsResponse getSiteLogs(long companyId, String accountSiteId, String uprn) throws Exception {

		InputGetLogInput input = objectFactoryService.getInputGetLogInput();
		input.setAccountSiteId(accountSiteId);
		input.setUprn(uprn);
		GetSiteLogs getSiteLogs = objectFactoryService.getGetSiteLogs();
		getSiteLogs.setLogInput(input);

		return getWhitespaceService(companyId).getSiteLogs(getSiteLogs);
	}

	public GetSiteNotificationsResponse getSiteNotifications(long companyId, String siteId, String uprn) throws Exception {

		InputGetSiteServiceNotificationInput input = objectFactoryService.getInputGetSiteServiceNotificationInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		GetSiteNotifications getSiteNotifications = objectFactoryService.getGetSiteNotifications();
		getSiteNotifications.setSiteservicenotificationInput(input);

		return getWhitespaceService(companyId).getSiteNotifications(getSiteNotifications);

	}

	public GetSitesResponse getSites(long companyId, String uprn) throws Exception {

		InputGetSitesInput input = objectFactoryService.getInputGetSitesInput();
		input.setUprn(uprn);
		GetSites getSites = objectFactoryService.getGetSites();
		getSites.setSitesInput(input);

		return getWhitespaceService(companyId).getSites(getSites);
	}

	public GetSiteServiceItemRoundSchedulesResponse getSiteServiceItemRoundSchedules(long companyId, String siteServiceId) throws Exception {

		InputGetSiteServiceItemRoundScheduleInput input = objectFactoryService.getInputGetSiteServiceItemRoundScheduleInput();
		input.setSiteServiceId(siteServiceId);
		GetSiteServiceItemRoundSchedules getSiteServiceItemRoundSchedules = objectFactoryService.getGetSiteServiceItemRoundSchedules();
		getSiteServiceItemRoundSchedules.setSiteServiceItemRoundScheduleInput(input);

		return getWhitespaceService(companyId).getSiteServiceItemRoundSchedules(getSiteServiceItemRoundSchedules);
	}

	public GetSiteWorksheetsResponse getSiteWorksheets(long companyId, String uprn, String accountSiteId, String worksheetFilterString, boolean orderAscending, String worksheetFilterName,
			String worksheetSortOrderName) throws Exception {
		long start = System.currentTimeMillis();
		InputGetWorksheetInput input = objectFactoryService.getInputGetWorksheetInput();
		input.setUprn(uprn);
		input.setAccountSiteId(accountSiteId);
		input.setWorksheetFilterString(worksheetFilterString);
		input.setOrderAscending(orderAscending);

		if (!Validator.isNull(worksheetFilterName)) {
			WorksheetFilter worksheetFilter = objectFactoryService.getWorksheetFilter(worksheetFilterName);
			input.setWorksheetFilter(worksheetFilter);
		}

		if (!Validator.isNull(worksheetSortOrderName)) {
			WorksheetSortOrder worksheetSortOrder = objectFactoryService.getWorksheetSortOrder(worksheetSortOrderName);
			input.setWorksheetSortOrder(worksheetSortOrder);
		}

		GetSiteWorksheets getSiteWorksheets = objectFactoryService.getGetSiteWorksheets();
		getSiteWorksheets.setWorksheetInput(input);

		LOG.debug("Whitespace Get site worksheets request: " + jsonFactory.serialize(getSiteWorksheets));

		GetSiteWorksheetsResponse response = getWhitespaceService(companyId).getSiteWorksheets(getSiteWorksheets);

		LOG.debug("Whitespace Get site worksheets response: " + jsonFactory.serialize(response));

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Whitespace Get site worksheets execution time: " + timeElapsed);

		return response;
	}

	public GetStreetsResponse getStreets(long companyId, String postcode, String streetName, String townName) throws Exception {

		InputStreetInput input = objectFactoryService.getInputStreetInput();
		input.setPostcode(postcode);
		input.setStreetName(streetName);
		input.setTownName(townName);
		GetStreets getStreets = objectFactoryService.getGetStreets();
		getStreets.setGetStreetInput(input);

		return getWhitespaceService(companyId).getStreets(getStreets);
	}

	public GetWalkNumbersResponse getWalkNumbers(long companyId, String siteId, String uprn) throws Exception {

		InputGetWalkNumbersInput input = objectFactoryService.getInputGetWalkNumbersInput();
		input.setSiteId(siteId);
		input.setUprn(uprn);
		GetWalkNumbers getWalkNumbers = objectFactoryService.getGetWalkNumbers();
		getWalkNumbers.setGetWalkNumbersInput(input);

		return getWhitespaceService(companyId).getWalkNumbers(getWalkNumbers);
	}

	public GetWorksheetAttachmentsResponse getWorksheetAttachments(long companyId, String worksheetId, String worksheetRef, boolean inlcudeData) throws Exception {

		InputGetWorksheetAttachmentInput input = objectFactoryService.getInputGetWorksheetAttachmentInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		input.setIncludeData(inlcudeData);
		GetWorksheetAttachments getWorksheetAttachments = objectFactoryService.getGetWorksheetAttachments();
		getWorksheetAttachments.setGetWorksheetAttachmentsInput(input);

		return getWhitespaceService(companyId).getWorksheetAttachments(getWorksheetAttachments);
	}

	public GetWorksheetChargeMatrixResponse getWorksheetChargeMatrix(long companyId, String serviceId, String serviceItemId) throws Exception {

		InputServiceAndServiceItemInput input = objectFactoryService.getInputServiceAndServiceItemInput();
		input.setServiceId(serviceId);
		input.setServiceItemId(serviceItemId);
		GetWorksheetChargeMatrix getWorksheetChargeMatrix = objectFactoryService.getGetWorksheetChargeMatrix();
		getWorksheetChargeMatrix.setWorksheetChargeMatrixInput(input);

		return getWhitespaceService(companyId).getWorksheetChargeMatrix(getWorksheetChargeMatrix);
	}

	public GetWorksheetDetailEventsResponse getWorksheetDetailEvents(long companyId, String worksheetId, String worksheetRef) throws Exception {

		InputGetWorksheetDetailEventsInput input = objectFactoryService.getInputGetWorksheetDetailEventsInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		GetWorksheetDetailEvents getWorksheetDetailEvents = objectFactoryService.getGetWorksheetDetailEvents();
		getWorksheetDetailEvents.setWorksheetDetailEventsInput(input);

		return getWhitespaceService(companyId).getWorksheetDetailEvents(getWorksheetDetailEvents);
	}

	public GetWorksheetDetailExtraInfoFieldsResponse getWorksheetDetailExtraInfoFields(long companyId, String worksheetId, String worksheetRef) throws Exception {

		InputGetWorksheetDetailExtraInfoFieldsInput input = objectFactoryService.getInputGetWorksheetDetailExtraInfoFieldsInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		GetWorksheetDetailExtraInfoFields getWorksheetDetailExtraInfoFields = objectFactoryService.getGetWorksheetDetailExtraInfoFields();
		getWorksheetDetailExtraInfoFields.setWorksheetDetailExtraInfoFieldsInput(input);

		return getWhitespaceService(companyId).getWorksheetDetailExtraInfoFields(getWorksheetDetailExtraInfoFields);
	}

	public GetWorksheetDetailNotesResponse getWorksheetDetailNotes(long companyId, String worksheetId, String worksheetRef) throws Exception {

		InputGetWorksheetDetailNotesInput input = objectFactoryService.getInputGetWorksheetDetailNotesInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		GetWorksheetDetailNotes getWorksheetDetailNotes = objectFactoryService.getGetWorksheetDetailNotes();
		getWorksheetDetailNotes.setWorksheetDetailsNotesInput(input);

		return getWhitespaceService(companyId).getWorksheetDetailNotes(getWorksheetDetailNotes);
	}

	public GetWorksheetDetailsResponse getWorksheetDetails(long companyId, String worksheetId, String worksheetRef) throws Exception {

		InputGetWorksheetDetailsInput input = objectFactoryService.getInputGetWorksheetDetailsInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		GetWorksheetDetails getWorksheetDetails = objectFactoryService.getGetWorksheetDetails();
		getWorksheetDetails.setWorksheetDetailsInput(input);

		return getWhitespaceService(companyId).getWorksheetDetails(getWorksheetDetails);
	}

	public GetWorksheetDetailServiceItemsResponse getWorksheetDetailServiceItems(long companyId, String worksheetId, String worksheetRef) throws Exception {
		long start = System.currentTimeMillis();
		InputGetWorksheetDetailServiceItemsInput input = objectFactoryService.getInputGetWorksheetDetailServiceItemsInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		GetWorksheetDetailServiceItems getWorksheetDetailServiceItems = objectFactoryService.getGetWorksheetDetailServiceItems();
		getWorksheetDetailServiceItems.setWorksheetDetailServiceItemsInput(input);

		LOG.debug("Whitespace Get worksheet detail service items request: " + jsonFactory.serialize(getWorksheetDetailServiceItems));

		GetWorksheetDetailServiceItemsResponse response = getWhitespaceService(companyId).getWorksheetDetailServiceItems(getWorksheetDetailServiceItems);

		LOG.debug("Whitespace Get worksheet detail service items response: " + jsonFactory.serialize(response));

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Whitespace Get worksheet details service items execution time: " + timeElapsed);

		return response;
	}

	public GetWorksheetExtraInfoFieldsResponse getWorksheetExtraInfoFields(long companyId, String serviceId) throws Exception {

		InputGetServicePropertyInput input = objectFactoryService.getInputGetServicePropertyInput();
		input.setServiceId(serviceId);

		GetWorksheetExtraInfoFields getWorksheetExtraInfoFields = objectFactoryService.getGetWorksheetExtraInfoFields();
		getWorksheetExtraInfoFields.setServicepropertyInput(input);

		return getWhitespaceService(companyId).getWorksheetExtraInfoFields(getWorksheetExtraInfoFields);
	}

	public GetWorksheetRolesResponse getWorksheetRoles(long companyId, String serviceId) throws Exception {

		InputGetWorksheetRolesInput input = objectFactoryService.getInputGetWorksheetRolesInput();
		input.setServiceId(serviceId);
		GetWorksheetRoles getWorksheetRoles = objectFactoryService.getGetWorksheetRoles();
		getWorksheetRoles.setWorksheetRolesInput(input);

		return getWhitespaceService(companyId).getWorksheetRoles(getWorksheetRoles);
	}

	public GetWorksheetsByReferenceResponse getWorksheetsByReference(long companyId, String worksheetReference) throws Exception {

		InputGetWorksheetsByRefInput input = objectFactoryService.getInputGetWorksheetsByRefInput();
		input.setWorksheetReference(worksheetReference);
		GetWorksheetsByReference getWorksheetsByReference = objectFactoryService.getGetWorksheetsByReference();
		getWorksheetsByReference.setWorksheetsByRefInput(input);

		return getWhitespaceService(companyId).getWorksheetsByReference(getWorksheetsByReference);
	}

	public GetWorksheetServiceItemsResponse getWorksheetServiceItems(long companyId, String siteId, String serviceId, String uprn) throws Exception {

		InputGetWorksheetServiceItemInput input = objectFactoryService.getInputGetWorksheetServiceItemInput();
		input.setSiteId(siteId);
		input.setServiceId(serviceId);
		input.setUprn(uprn);
		GetWorksheetServiceItems getWorksheetServiceItems = objectFactoryService.getGetWorksheetServiceItems();
		getWorksheetServiceItems.setWorksheetServiceItemInput(input);

		return getWhitespaceService(companyId).getWorksheetServiceItems(getWorksheetServiceItems);
	}

	public ProgressWorkflowResponse progressWorkflow(long companyId, String worksheetId, String worksheetRef, String eventName) throws Exception {

		InputProgressWorkflowInput input = objectFactoryService.getInputProgressWorkflowInput();
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		input.setEventName(eventName);
		ProgressWorkflow progressWorkflow = objectFactoryService.getProgressWorkflow();
		progressWorkflow.setProgressWorkflowInput(input);

		return getWhitespaceService(companyId).progressWorkflow(progressWorkflow);
	}

	public UpdateSiteContactResponse updateSiteContact(long companyId, String contactEmail, String contactId, String contactName, String contactFaxNumber, String contactMobileNumber,
			String contactTelephoneNumber, boolean isMainContact, String siteId, String uprn) throws Exception {

		InputUpdateSiteContactInput input = objectFactoryService.getInputUpdateSiteContactInput();
		input.setContactEmail(contactEmail);
		input.setContactId(contactId);
		input.setContactName(contactName);
		input.setContactFaxNumber(contactFaxNumber);
		input.setContactMobileNumber(contactMobileNumber);
		input.setContactTelephoneNumber(contactTelephoneNumber);
		input.setIsMainContact(isMainContact);
		input.setSiteId(siteId);
		input.setUprn(uprn);
		UpdateSiteContact updateSiteContact = objectFactoryService.getUpdateSiteContact();
		updateSiteContact.setUpdateSiteContactInput(input);

		return getWhitespaceService(companyId).updateSiteContact(updateSiteContact);
	}

	public UpdateSiteServiceItemResponse updateSiteServiceItem(long companyId, String siteServiceId, String serviceItemQuantity, String serviceItemValidFrom, String serviceItemValidTo,
			String chargeTypeId, SiteCollectionExtraDetailInputExt siteCollectionExtraDetailInputExt) throws Exception {

		InputUpdateSiteServiceItemInput input = objectFactoryService.getInputUpdateSiteServiceItemInput();
		input.setSiteServiceId(siteServiceId);
		input.setServiceItemQuantity(serviceItemQuantity);
		input.setServiceItemValidFrom(serviceItemValidFrom);
		input.setServiceItemValidTo(serviceItemValidTo);
		input.setChargeTypeId(chargeTypeId);

		if (!Validator.isNull(siteCollectionExtraDetailInputExt)) {
			input.setExtraDetailInputs(siteCollectionExtraDetailInputExt.getSiteCollectionExtraDetails());
		}

		UpdateSiteServiceItem updateSiteServiceItem = objectFactoryService.getUpdateSiteServiceItem();
		updateSiteServiceItem.setUpdateSiteServiceItemInput(input);

		return getWhitespaceService(companyId).updateSiteServiceItem(updateSiteServiceItem);
	}

	public UpdateWorkflowEventDateResponse updateWorkflowEventDate(long companyId, String eventDate, String eventName, String worksheetId, String worksheetRef, String stateName) throws Exception {

		InputUpdateWorkflowEventDateInput input = objectFactoryService.getInputUpdateWorkflowEventDateInput();
		input.setEventDate(eventDate);
		input.setEventName(eventName);
		input.setWorksheetId(worksheetId);
		input.setWorksheetRef(worksheetRef);
		input.setStateName(stateName);
		UpdateWorkflowEventDate updateWorkflowEventDate = objectFactoryService.getUpdateWorkflowEventDate();
		updateWorkflowEventDate.setUpdateWorkflowEventDateInput(input);

		return getWhitespaceService(companyId).updateWorkflowEventDate(updateWorkflowEventDate);
	}

	public UpdateWorksheetResponse updateWorksheet(long companyId, String workLocationAddress, String workLocationAddressID, String workLocationEasting, String workLocationLatitude,
			String workLocationLongitude, String workLocationName, String workLocationNorthing, String workLocationText, String worksheetApprovedDate, String worksheetAssignedToID,
			String worksheetAssignedToTypeID, String worksheetCompletedDate, String worksheetDueDate, String worksheetEscallatedDate, String worksheetExpiryDate, String worksheetMessage,
			String worksheetImportanceID, String worksheetId, String worksheetPaymentDate, String worksheetReference, String worksheetSubject, ServicePropertyInputExt servicePropertyInputExt)
			throws Exception {

		InputUpdateWorksheetDetailInput input = objectFactoryService.getInputUpdateWorksheetDetailInput();

		input.setWorkLocationAddress(workLocationAddress);
		input.setWorkLocationAddressID(workLocationAddressID);
		input.setWorkLocationEasting(workLocationEasting);
		input.setWorkLocationLatitude(workLocationLatitude);
		input.setWorkLocationLongitude(workLocationLongitude);
		input.setWorkLocationName(workLocationName);
		input.setWorkLocationNorthing(workLocationNorthing);
		input.setWorkLocationText(workLocationText);
		input.setWorksheetApprovedDate(worksheetApprovedDate);
		input.setWorksheetAssignedToID(worksheetAssignedToID);
		input.setWorksheetAssignedToTypeID(worksheetAssignedToTypeID);
		input.setWorksheetCompletedDate(worksheetCompletedDate);
		input.setWorksheetDueDate(worksheetDueDate);
		input.setWorksheetEscallatedDate(worksheetEscallatedDate);
		input.setWorksheetExpiryDate(worksheetExpiryDate);
		input.setWorksheetMessage(worksheetMessage);
		input.setWorksheetImportanceID(worksheetImportanceID);
		input.setWorksheetId(worksheetId);
		input.setWorksheetPaymentDate(worksheetPaymentDate);
		input.setWorksheetReference(worksheetReference);
		input.setWorksheetSubject(worksheetSubject);
		input.setServicePropertyInputs(servicePropertyInputExt.getServicePropertyUpdateInputs());
		UpdateWorksheet updateWorksheet = objectFactoryService.getUpdateWorksheet();
		updateWorksheet.setWorksheetDetailInput(input);

		return getWhitespaceService(companyId).updateWorksheet(updateWorksheet);
	}

	private WSAPIServiceStub getWhitespaceService(long companyId) throws ConfigurationException, AxisFault {
		return whitespaceAxisService.getWhitespaceAxisService(companyId);
	}
}