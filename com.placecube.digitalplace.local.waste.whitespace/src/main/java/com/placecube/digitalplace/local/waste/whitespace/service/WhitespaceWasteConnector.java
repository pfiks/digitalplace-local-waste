package com.placecube.digitalplace.local.waste.whitespace.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.constants.WasteConstants;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.service.WasteConnector;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ApiAdHocRoundInstance;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionByUprnAndDateResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionSlotsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteService;
import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;
import com.placecube.digitalplace.local.waste.whitespace.model.WorksheetContext;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.WorksheetValidator;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceDateUtil;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

@Component(immediate = true, service = WasteConnector.class)
public class WhitespaceWasteConnector implements WasteConnector {

	private static final Log LOG = LogFactoryUtil.getLog(WhitespaceWasteConnector.class);

	@Reference
	private ResponseParserService responseParserService;

	@Reference
	private WhitespaceRequestService whitespaceRequestService;

	@Reference
	private WhitespaceConfigurationService whitespaceConfigurationService;

	@Reference
	private WhitespaceWasteService whitespaceWasteService;

	@Reference
	private WhitespaceWorksheetService whitespaceWorksheetService;

	@Reference
	private WhitespaceWasteServiceUtil whitespaceWasteServiceUtil;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private ObjectFactoryService objectFactoryService;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Override
	public String createAssistedBinCollectionJob(long companyId, AssistedBinCollectionJob assistedBinCollectionJob) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public WasteSubscriptionResponse createBinSubscription(long companyId, String uprn, String serviceType, String firstName, String lastName, String emailAddress, String telephone, int numberOfBins)
			throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, List<String> itemsToBeCollected,
			String locationOfItems, Date collectionDate) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, BulkyWasteCollectionRequest bulkyWasteCollectionRequest, Date date) throws WasteRetrievalException {
		LOG.debug("createBulkyWasteCollectionJob request: " + jsonFactory.serialize(bulkyWasteCollectionRequest) + ", companyId: " + companyId);

		try {
			String uprn = bulkyWasteCollectionRequest.getUprn();
			long formInstanceRecordId = Long.parseLong(bulkyWasteCollectionRequest.getClassPK());

			DDMFormInstanceRecord ddmFormInstanceRecord = ddmFormInstanceRecordLocalService.getDDMFormInstanceRecord(formInstanceRecordId);

			WhitespaceService whitespaceService = whitespaceConfigurationService.getConfiguredDefaultWhitespaceService(companyId, ddmFormInstanceRecord.getFormInstanceId());

			Optional<ApiAdHocRoundInstance> apiAdHocRoundInstanceForSelectedDate = getMathingAdHocRoundInstance(companyId, whitespaceService.getServiceId(),
					WasteConstants.BULKY_DFLT_NUMBER_OF_DATES_TO_RETURN, uprn, date);

			if (apiAdHocRoundInstanceForSelectedDate.isPresent() && apiAdHocRoundInstanceForSelectedDate.get().getSlotsFree() > 0) {
				int adHocRoundInstanceId = apiAdHocRoundInstanceForSelectedDate.get().getAdHocRoundInstanceID();

				final String worksheetMessage = "New bulky waste collection request";

				Map<String, Integer> itemsToQuantitiesMap = whitespaceWasteService.getItemsToQuantitiesMap(bulkyWasteCollectionRequest.getItemsForCollection(),
						bulkyWasteCollectionRequest.getQuantities());

				WorksheetContext worksheetContext = objectFactoryService.getWorksheetContextBuilder(companyId, uprn) //
						.setFormInstanceRecordId(formInstanceRecordId) //
						.setBinTypes(new ArrayList<>(itemsToQuantitiesMap.keySet())) //
						.setBinSizes(Collections.emptyList()) //
						.setFirstName(bulkyWasteCollectionRequest.getFirstName()) //
						.setLastName(bulkyWasteCollectionRequest.getLastName()) //
						.setQuantities(new ArrayList<>(itemsToQuantitiesMap.values())) //
						.setWorksheetMessage(worksheetMessage) //
						.setAdHocRoundInstanceId(adHocRoundInstanceId) //
						.build();

				String worksheetRef = whitespaceWorksheetService.createWorksheetForDefaultService(worksheetContext);

				LOG.debug("createBulkyWasteCollectionJob response: " + worksheetRef);

				return worksheetRef;

			} else {
				throw new WasteRetrievalException("The selected slot is not available anymore");
			}
		} catch (WasteRetrievalException e) {
			throw e;
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}

	}

	@Override
	public String createFlyTippingReport(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, String typeOfRubbish, String sizeOfRubbish,
			String details, String coordinates, String locationDetails, String dateOfIncident) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public WasteSubscriptionResponse createGardenWasteSubscription(long companyId, GardenWasteSubscription gardenWasteSubscription) throws WasteRetrievalException {
		LOG.debug("createGardenWasteSubscription request: " + jsonFactory.serialize(gardenWasteSubscription) + ", companyId: " + companyId);

		String worksheetMessage = "New garden waste subscription request";
		int quantity = gardenWasteSubscription.getNumberRequestedNewBins();

		WorksheetContext worksheetContext = objectFactoryService.getWorksheetContextBuilder(companyId, gardenWasteSubscription.getUprn()) //
				.setFormInstanceRecordId(Long.parseLong(gardenWasteSubscription.getClassPK())) //
				.setBinTypes(Arrays.asList(gardenWasteSubscription.getBinType())) //
				.setBinSizes(Collections.emptyList()) //
				.setFirstName(gardenWasteSubscription.getFirstName()) //
				.setLastName(gardenWasteSubscription.getLastName()) //
				.setQuantities(Arrays.asList(quantity)) //
				.setWorksheetMessage(worksheetMessage) //
				.build();

		String worksheetRef = whitespaceWorksheetService.createWorksheetForDefaultService(worksheetContext);

		WasteSubscriptionResponse response = whitespaceWasteService.createWasteSubscriptionResponse(worksheetRef, SubscriptionType.GARDEN_WASTE);

		LOG.debug("createGardenWasteSubscription response: " + jsonFactory.serialize(response));

		return response;

	}

	@Override
	public String createMissedBinCollectionJob(long companyId, MissedBinCollectionJob missedBinCollectionJob) throws WasteRetrievalException {
		LOG.debug("createMissedBinCollectionJob request: " + jsonFactory.serialize(missedBinCollectionJob) + ", companyId: " + companyId);

		String worksheetMessage = "New missed bin collection request";

		WorksheetContext worksheetContext = objectFactoryService.getWorksheetContextBuilder(companyId, missedBinCollectionJob.getUprn()) //
				.setFormInstanceRecordId(Long.parseLong(missedBinCollectionJob.getClassPK())) //
				.setBinTypes(Collections.emptyList()) //
				.setBinSizes(Collections.emptyList()) //
				.setFirstName(missedBinCollectionJob.getFirstName()) //
				.setLastName(missedBinCollectionJob.getLastName()) //
				.setQuantities(Collections.emptyList()) //
				.setWorksheetMessage(worksheetMessage) //
				.build();

		String worksheetResponseRef = whitespaceWorksheetService.createWorksheet(worksheetContext, missedBinCollectionJob.getBinType());

		LOG.debug("createMissedBinCollectionJob response: " + worksheetResponseRef);

		return worksheetResponseRef;
	}

	@Override
	public String createNewWasteContainerRequest(long companyId, NewContainerRequest newContainerRequest) throws WasteRetrievalException {
		LOG.debug("createNewWasteContainerRequest request: " + jsonFactory.serialize(newContainerRequest) + ", companyId: " + companyId);

		String worksheetMessage = "New waste container request";
		int quantity = 1;

		WorksheetContext worksheetContext = objectFactoryService.getWorksheetContextBuilder(companyId, newContainerRequest.getUprn()) //
				.setFormInstanceRecordId(Long.parseLong(newContainerRequest.getClassPK())) //
				.setBinTypes(Arrays.asList(newContainerRequest.getBinType())) //
				.setBinSizes(Arrays.asList(newContainerRequest.getBinSize())) //
				.setFirstName(newContainerRequest.getFirstName()) //
				.setLastName(newContainerRequest.getLastName()) //
				.setQuantities(Arrays.asList(quantity)) //
				.setWorksheetMessage(worksheetMessage) //
				.build();

		String worksheetResponseRef = whitespaceWorksheetService.createWorksheetForDefaultService(worksheetContext);

		LOG.debug("createNewWasteContainerRequest response: " + worksheetResponseRef);

		return worksheetResponseRef;
	}

	@Override
	public boolean enabled(long companyId) {
		try {
			return whitespaceConfigurationService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public List<WasteSubscriptionResponse> getAllWasteSubscriptions(long companyId, SubscriptionType subscriptionType) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRN(long companyId, String uprn) throws WasteRetrievalException {
		LOG.debug("getBinCollectionDatesByUPRN uprn: " + uprn + ", companyId: " + companyId);

		try {
			GetCollectionByUprnAndDateResponse response = whitespaceRequestService.getCollectionByUprnAndDate(companyId, uprn, WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now()));

			Set<BinCollection> binCollections = responseParserService.parseBinCollections(response, companyId);

			LOG.debug("getBinCollectionDatesByUPRN response: " + jsonFactory.serialize(binCollections));

			return binCollections;
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error getting bin collection dates: " + e.getMessage());
		}
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRNAndService(long companyId, String uprn, String serviceName, String formInstanceId) throws WasteRetrievalException {
		LOG.debug("getBinCollectionDatesByUPRNAndService uprn: " + uprn + ", companyId: " + companyId + ", serviceName: " + serviceName + ", formInstanceId: " + formInstanceId);

		try {
			Set<BinCollection> binCollections = getBinCollectionDatesByUPRN(companyId, uprn);

			Set<BinCollection> filteredBinCollections = whitespaceWasteService.filterBinCollectionsByService(companyId, serviceName, formInstanceId, binCollections);

			LOG.debug("getBinCollectionDatesByUPRNAndService response: " + jsonFactory.serialize(filteredBinCollections));

			return filteredBinCollections;
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error getting bin collection dates by uprn and service: " + e.getMessage());
		}

	}

	@Override
	public List<Date> getBulkyCollectionSlotsByUPRNAndService(long companyId, String uprn, String serviceId, int numberOfDatesToReturn) throws WasteRetrievalException {
		LOG.debug("getBulkyCollectionSlotsByUPRNAndService uprn: " + uprn + ", companyId: " + companyId + ", serviceId: " + serviceId + ", numberOfDatesToReturn: " + numberOfDatesToReturn);

		try {
			ApiAdHocRoundInstance[] apiAdHocRoundInstances = getAdHocRoundInstances(companyId, serviceId, numberOfDatesToReturn, uprn);

			List<Date> dates = Arrays.stream(apiAdHocRoundInstances).map(whitespaceWasteService::getWasteBulkyCollectionDateIfFreeSlots).flatMap(Optional::stream).collect(Collectors.toList());

			LOG.debug("getBulkyCollectionSlotsByUPRNAndService response: " + jsonFactory.serialize(dates));

			return dates;

		} catch (WasteRetrievalException e) {
			throw e;
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}
	}

	private ApiAdHocRoundInstance[] getAdHocRoundInstances(long companyId, String serviceId, int numberOfDatesToReturn, String uprn) throws Exception {
		GetCollectionSlotsResponse getCollectionSlotsResponse = whitespaceRequestService.getCollectionSlots(companyId, uprn, serviceId, null, String.valueOf(numberOfDatesToReturn),
				WhitespaceDateUtil.formatDate_dd_MM_yyyy(LocalDate.now()), null);

		return responseParserService.parseGetCollectionSlotsResponse(getCollectionSlotsResponse);
	}

	private Optional<ApiAdHocRoundInstance> getMathingAdHocRoundInstance(long companyId, String serviceId, int numberOfDatesToReturn, String uprn, Date date) throws Exception {
		ApiAdHocRoundInstance[] apiAdHocRoundInstances = getAdHocRoundInstances(companyId, serviceId, numberOfDatesToReturn, uprn);
		return Arrays.stream(apiAdHocRoundInstances).filter(x -> x.getAdHocRoundInstanceDate().getTime().equals(date)).findFirst();
	}

	@Override
	public Optional<BulkyCollectionDate> getBulkyCollectionDateByUPRN(long companyId, String uprn) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String getMissedBinCollectionResponse(long companyId, String uprn, String date) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String getName(long companyId) {
		return "Waste - Whitespace";
	}

	@Override
	public int howManyBinsSubscribed(long companyId, String uprn, String serviceType) throws WasteRetrievalException {
		LOG.debug("howManyBinsSubscribed uprn: " + uprn + ", companyId: " + companyId + ", serviceType: " + serviceType);

		try {
			GetSiteCollectionsResponse getSiteCollectionsResponse = whitespaceRequestService.getSiteCollections(companyId, null, uprn, true);

			SiteService[] siteServices = responseParserService.parseGetSiteCollectionsResponse(getSiteCollectionsResponse);

			String serviceId = whitespaceConfigurationService.getConfiguredWhitespaceServiceId(companyId, serviceType);

			int binsSubscribed = whitespaceWasteService.getTotalSiteServicesQuantities(siteServices, Integer.parseInt(serviceId));

			LOG.debug("howManyBinsSubscribed response: " + binsSubscribed);

			return binsSubscribed;

		} catch (Exception e) {
			throw new WasteRetrievalException("Error getting number of bins subscribed to: " + e.getMessage());
		}

	}

	@Override
	public int howManyBinsSubscribedForPeriod(long companyId, String uprn, String serviceType, String startDate, String endDate) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForBulkyWasteCollection(long companyId, String uprn, List<String> itemsForCollection, String formInstanceId) throws WasteRetrievalException {
		LOG.debug("isPropertyEligibleForBulkyWasteCollection request: " + ", companyId: " + companyId + ", formInstanceId: " + formInstanceId);

		Optional<WorksheetValidator> worksheetValidator = Optional.empty();

		WorksheetContext worksheetContext = objectFactoryService.getWorksheetContextBuilder(companyId, uprn) //
				.setBinTypes(itemsForCollection) //
				.setBinSizes(Collections.emptyList()) //
				.build();

		boolean result = whitespaceWorksheetService.isWorksheetContextValidForDefaultService(worksheetContext, Long.parseLong(formInstanceId), worksheetValidator);

		LOG.debug("isPropertyEligibleForBulkyWasteCollection response: " + result);

		return result;
	}

	@Override
	public boolean isPropertyEligibleForWasteContainerRequest(long companyId, String uprn, String binType, String binSize, String formInstanceId) throws WasteRetrievalException {
		LOG.debug("isPropertyEligibleForWasteContainerRequest request: " + ", companyId: " + companyId + ", binType: " + binType + ", binSize: " + binSize + ", formInstanceId: " + formInstanceId);

		Optional<WorksheetValidator> worksheetValidator = Optional.empty();

		WorksheetContext worksheetContext = objectFactoryService.getWorksheetContextBuilder(companyId, uprn) //
				.setBinTypes(Arrays.asList(binType)) //
				.setBinSizes(Arrays.asList(binSize)) //
				.build();

		boolean result = whitespaceWorksheetService.isWorksheetContextValidForDefaultService(worksheetContext, Long.parseLong(formInstanceId), worksheetValidator);

		LOG.debug("isPropertyEligibleForWasteContainerRequest response: " + result);

		return result;
	}

	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String binType, String formInstanceId)
			throws WasteRetrievalException {
		LOG.debug("isPropertyEligibleForGardenWasteSubscription request: " + ", companyId: " + companyId + ", uprn: " + uprn + ", formInstanceId: " + formInstanceId);

		WorksheetValidator worksheetValidator = objectFactoryService.createSameFinancialYearValidator(whitespaceWasteServiceUtil);

		WorksheetContext worksheetContext = objectFactoryService.getWorksheetContextBuilder(companyId, uprn) //
				.setBinTypes(Arrays.asList(binType)) //
				.setBinSizes(Collections.emptyList()) //
				.build();

		boolean result = whitespaceWorksheetService.isWorksheetContextValidForDefaultService(worksheetContext, Long.parseLong(formInstanceId), Optional.of(worksheetValidator));

		LOG.debug("isPropertyEligibleForGardenWasteSubscription response: " + result);

		return result;
	}

	@Override
	public boolean isPropertyEligibleForMissedBinCollectionReport(long companyId, String uprn, String binType, String formInstanceId) throws WasteRetrievalException {
		LOG.debug("isPropertyEligibleForMissedBinCollectionReport request: " + ", companyId: " + companyId + ", uprn: " + uprn + ", formInstanceId: " + formInstanceId);

		WorksheetValidator worksheetValidator = objectFactoryService.createTwoLastWeeksValidator(whitespaceWasteServiceUtil);

		WorksheetContext worksheetContext = objectFactoryService.getWorksheetContextBuilder(companyId, uprn) //
				.setBinTypes(Collections.emptyList()) //
				.setBinSizes(Collections.emptyList()) //
				.build();

		boolean result = whitespaceWorksheetService.isWorksheetContextValidForService(worksheetContext, Long.parseLong(formInstanceId), binType, Optional.of(worksheetValidator));

		LOG.debug("isPropertyEligibleForMissedBinCollectionReport response: " + result);

		return result;
	}

	@Deprecated
	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

}
