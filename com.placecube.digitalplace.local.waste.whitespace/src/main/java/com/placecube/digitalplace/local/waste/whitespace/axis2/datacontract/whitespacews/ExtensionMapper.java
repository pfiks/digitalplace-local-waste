/**
 * ExtensionMapper.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** ExtensionMapper class */
@SuppressWarnings({"unchecked", "unused"})
public class ExtensionMapper {

  public static java.lang.Object getTypeObject(
      java.lang.String namespaceURI,
      java.lang.String typeName,
      javax.xml.stream.XMLStreamReader reader)
      throws java.lang.Exception {

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfLog".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfLog.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetServiceItemResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetServiceItemResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteServiceResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteServiceResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetAttachmentInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetAttachmentInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.SiteAndServiceInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputSiteAndServiceInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.UpdateSiteContactInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputUpdateSiteContactInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "UpdateJobSoapGeoCode".equals(typeName)) {

      return axis2.apache.org.UpdateJobSoapGeoCode.Factory.parse(reader);
    }

    if ("http://schemas.microsoft.com/2003/10/Serialization/".equals(namespaceURI)
        && "duration".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .Duration.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/Contacts".equals(namespaceURI)
        && "ContactResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.whitespacews.contacts
          .ContactResponse.Factory.parse(reader);
    }

    if ("http://schemas.microsoft.com/2003/10/Serialization/Arrays".equals(namespaceURI)
        && "ArrayOfanyType".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .arrays.ArrayOfanyType.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.ProgressWorkflowInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputProgressWorkflowInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobFromTemplateSoapGeoCode".equals(typeName)) {

      return axis2.apache.org.CreateJobFromTemplateSoapGeoCode.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfSiteServiceNotification".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfSiteServiceNotification.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteServiceNotificationResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteServiceNotificationResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfCreateJobFromTemplateSoapGeoCode".equals(typeName)) {

      return axis2.apache.org.ArrayOfCreateJobFromTemplateSoapGeoCode.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "GetAddressInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetAddressInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetRoundIncidentInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetRoundIncidentInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.UpdateWorksheetDetailInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputUpdateWorksheetDetailInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceItemResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceItemResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfCreateJobFromTemplateSoapField".equals(typeName)) {

      return axis2.apache.org.ArrayOfCreateJobFromTemplateSoapField.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobFromTemplateSoapAttachment".equals(typeName)) {

      return axis2.apache.org.CreateJobFromTemplateSoapAttachment.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.WorksheetInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputWorksheetInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfUpdateJobSoapAttachment".equals(typeName)) {

      return axis2.apache.org.ArrayOfUpdateJobSoapAttachment.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.StreetInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputStreetInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "BM_ServiceItem".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .BM_ServiceItem.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ApiWorkflows".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ApiWorkflows.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfWorksheet".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheet.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfWorksheetNote".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetNote.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddLogInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddLogInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfWorkflowHistory".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorkflowHistory.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfUpdateJobSoapGeoCode".equals(typeName)) {

      return axis2.apache.org.ArrayOfUpdateJobSoapGeoCode.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "StreetResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .StreetResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "WorkLocationSoap".equals(typeName)) {

      return axis2.apache.org.WorkLocationSoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfService".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfService.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.UpdateSiteServiceItemInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputUpdateSiteServiceItemInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WalkNumberDetail".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WalkNumberDetail.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfApiSiteServiceWithExtraDetails".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfApiSiteServiceWithExtraDetails.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfAccountSite".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfAccountSite.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.SSRRASSInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputSSRRASSInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ApiAdHocRoundInstance".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ApiAdHocRoundInstance.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceItemChargeResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceItemChargeResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddWorksheetNotesInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddWorksheetNotesInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteContractResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteContractResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfJobSoapField".equals(typeName)) {

      return axis2.apache.org.ArrayOfJobSoapField.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfRoundIncidents".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfRoundIncidents.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.DeleteSiteServiceItemRoundScheduleInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputDeleteSiteServiceItemRoundScheduleInput.Factory.parse(reader);
    }

    if ("http://schemas.microsoft.com/2003/10/Serialization/".equals(namespaceURI)
        && "guid".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .Guid.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/API".equals(namespaceURI)
        && "ArrayOfServiceProperty".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .webservices.ArrayOfServiceProperty.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Inputs".equals(namespaceURI)
        && "ArrayOfInput.UpdateWorksheetDetailInput.UpdateServicePropertyInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .ArrayOfInputUpdateWorksheetDetailInputUpdateServicePropertyInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.SiteCollectionExtraDetailInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputSiteCollectionExtraDetailInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobFromTemplateSoapField".equals(typeName)) {

      return axis2.apache.org.CreateJobFromTemplateSoapField.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "NotificationResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .NotificationResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfContact".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfContact.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.NotificationInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputNotificationInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ApiSiteServiceWithExtraDetails".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ApiSiteServiceWithExtraDetails.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.SiteInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputSiteInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetServicePropertyResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetServicePropertyResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetNote".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetNote.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteServiceNotificationInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteServiceNotificationInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfJobSoapAttachment".equals(typeName)) {

      return axis2.apache.org.ArrayOfJobSoapAttachment.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfOrderByFilterSoap".equals(typeName)) {

      return axis2.apache.org.ArrayOfOrderByFilterSoap.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobFromTemplateSoap".equals(typeName)) {

      return axis2.apache.org.CreateJobFromTemplateSoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfInput.SiteCollectionExtraDetailInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfInputSiteCollectionExtraDetailInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfInCabLogs".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfInCabLogs.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfCollection".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfCollection.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.ServiceAndServiceItemInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputServiceAndServiceItemInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.WorkflowInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputWorkflowInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "FullSiteServiceResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .FullSiteServiceResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "AttachmentFileResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AttachmentFileResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/API/".equals(namespaceURI)
        && "AddSiteServiceItemResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .webservices.AddSiteServiceItemResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfServiceItem".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfServiceItem.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetServiceItem".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetServiceItem.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "AttachmentSoap".equals(typeName)) {

      return axis2.apache.org.AttachmentSoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteInput.Factory.parse(reader);
    }

    if ("http://schemas.microsoft.com/2003/10/Serialization/Arrays".equals(namespaceURI)
        && "ArrayOfstring".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          .arrays.ArrayOfstring.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CompanySoap".equals(typeName)) {

      return axis2.apache.org.CompanySoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddSiteServiceItemInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddSiteServiceItemInput.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/".equals(namespaceURI)
        && "ArrayOfServiceSchedule".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums
          .ArrayOfServiceSchedule.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.CreateWorksheetInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputCreateWorksheetInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceAssignedToResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceAssignedToResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetServiceItemInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetServiceItemInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.RaiseCancelWorksheetRequestInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputRaiseCancelWorksheetRequestInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfSiteService".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfSiteService.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobSoapField".equals(typeName)) {

      return axis2.apache.org.CreateJobSoapField.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfUpdateJobSoapField".equals(typeName)) {

      return axis2.apache.org.ArrayOfUpdateJobSoapField.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.ServiceInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputServiceInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetAccountSiteInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetAccountSiteInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetCollectionSlotsInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetCollectionSlotsInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfNotification".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfNotification.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteInfoInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteInfoInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfApiAttachment".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfApiAttachment.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WalkNumberResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WalkNumberResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "AccountSiteIDResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AccountSiteIDResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfAddress".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfAddress.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ApiAttachment".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ApiAttachment.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServicePropertyResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServicePropertyResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetDetailEventsInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetDetailEventsInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfServiceItemCharge".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfServiceItemCharge.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddSiteServiceItemRoundScheduleInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddSiteServiceItemRoundScheduleInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteServiceItemRoundScheduleInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteServiceItemRoundScheduleInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteArrayResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteArrayResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "JobSoapField".equals(typeName)) {

      return axis2.apache.org.JobSoapField.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfApiServiceItem".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfApiServiceItem.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Inputs".equals(namespaceURI)
        && "ArrayOfInput.CreateWorksheetInput.ServicePropertyInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .ArrayOfInputCreateWorksheetInputServicePropertyInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetDetailNotesInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetDetailNotesInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfCreateJobSoapAttachment".equals(typeName)) {

      return axis2.apache.org.ArrayOfCreateJobSoapAttachment.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "BM_ServiceItemCharge".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .BM_ServiceItemCharge.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Notification".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .Notification.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "RRASSContractRoundResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .RRASSContractRoundResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "GetCollectionByUprnAndDateInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetCollectionByUprnAndDateInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "GetAddressesByCoordinatesRadiusInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetAddressesByCoordinatesRadiusInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ApplicationSoap".equals(typeName)) {

      return axis2.apache.org.ApplicationSoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetsByRefInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetsByRefInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfApiAdHocRoundInstance".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfApiAdHocRoundInstance.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSitesInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSitesInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.UpdateSiteServiceNotificationInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputUpdateSiteServiceNotificationInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetNoteResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetNoteResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfWorksheetServiceItem".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetServiceItem.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceAssignedTo".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceAssignedTo.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "RRASSContractRound".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .RRASSContractRound.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "UpdateJobSoap".equals(typeName)) {

      return axis2.apache.org.UpdateJobSoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetDetailResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetDetailResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.UpdateWorkflowEventDateInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputUpdateWorkflowEventDateInput.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/".equals(namespaceURI)
        && "LogDataPG".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.LogDataPG
          .Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetInCabLogsInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetInCabLogsInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "AttachmentResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AttachmentResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetLogSearchInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetLogSearchInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfRRASSContractRound".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfRRASSContractRound.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetDetailsInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetDetailsInput.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Enums".equals(namespaceURI)
        && "WorksheetFilter".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums
          .WorksheetFilter.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetDetailServiceItemsInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetDetailServiceItemsInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorkflowHistory".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorkflowHistory.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WSResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WSResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfFlag".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfFlag.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "InCabLogResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InCabLogResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "BaseSoap".equals(typeName)) {

      return axis2.apache.org.BaseSoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/API".equals(namespaceURI)
        && "ServiceProperty".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .webservices.ServiceProperty.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfSiteContract".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfSiteContract.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddSiteContactInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddSiteContactInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "UpdateJobWorkSoapLocation".equals(typeName)) {

      return axis2.apache.org.UpdateJobWorkSoapLocation.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Collection".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .Collection.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteFlagResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteFlagResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.DeleteSiteServiceItemInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputDeleteSiteServiceItemInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "FullWorksheetDetailResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .FullWorksheetDetailResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "JobSoapGeoCode".equals(typeName)) {

      return axis2.apache.org.JobSoapGeoCode.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "OrderByFilterSoap".equals(typeName)) {

      return axis2.apache.org.OrderByFilterSoap.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "JobSoapAttachment".equals(typeName)) {

      return axis2.apache.org.JobSoapAttachment.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI) && "Flag".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Flag
          .Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobSoapWorkLocation".equals(typeName)) {

      return axis2.apache.org.CreateJobSoapWorkLocation.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Inputs".equals(namespaceURI)
        && "ArrayOfInput.CreateWorksheetInput.ServiceItemInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .ArrayOfInputCreateWorksheetInputServiceItemInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfWorksheetServiceProperty".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetServiceProperty.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobSoapAttachment".equals(typeName)) {

      return axis2.apache.org.CreateJobSoapAttachment.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteService".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteService.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "UpdateJobSoapAttachment".equals(typeName)) {

      return axis2.apache.org.UpdateJobSoapAttachment.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "JobSoap".equals(typeName)) {

      return axis2.apache.org.JobSoap.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/".equals(namespaceURI)
        && "Street".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.Street
          .Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Inputs".equals(namespaceURI)
        && "Input.UpdateWorksheetDetailInput.UpdateServicePropertyInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .InputUpdateWorksheetDetailInputUpdateServicePropertyInput.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/".equals(namespaceURI)
        && "ServiceSchedule".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums
          .ServiceSchedule.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "GeoCodeSoap".equals(typeName)) {

      return axis2.apache.org.GeoCodeSoap.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfCreateJobFromTemplateSoapAttachment".equals(typeName)) {

      return axis2.apache.org.ArrayOfCreateJobFromTemplateSoapAttachment.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.DeleteSiteContactInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputDeleteSiteContactInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddAoMRoundLogInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddAoMRoundLogInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddWorksheetAttachmentBase64Input".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddWorksheetAttachmentBase64Input.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetDetailExtraInfoFieldsInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetDetailExtraInfoFieldsInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteContractInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteContractInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddAoMWorksheetLogInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddAoMWorksheetLogInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "RoundIncidentResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .RoundIncidentResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "AdHocInstanceRRASS".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AdHocInstanceRRASS.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorkflowResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorkflowResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetNotificationInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetNotificationInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "AccountSite".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AccountSite.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ApiServiceItem".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ApiServiceItem.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteServiceInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteServiceInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfJobSoapGeoCode".equals(typeName)) {

      return axis2.apache.org.ArrayOfJobSoapGeoCode.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteFlagInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteFlagInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfWhereFilterSoap".equals(typeName)) {

      return axis2.apache.org.ArrayOfWhereFilterSoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI) && "Service".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .Service.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceScheduleResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceScheduleResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfCreateJobSoapField".equals(typeName)) {

      return axis2.apache.org.ArrayOfCreateJobSoapField.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddSiteAttachmentBase64Input".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddSiteAttachmentBase64Input.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI) && "Contact".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .Contact.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Inputs".equals(namespaceURI)
        && "Input.CreateWorksheetInput.ServiceItemInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .InputCreateWorksheetInputServiceItemInput.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Inputs".equals(namespaceURI)
        && "Input.CreateWorksheetInput.ServicePropertyInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs
          .InputCreateWorksheetInputServicePropertyInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteServiceSiteServiceItemServiceItemPropertyInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteServiceSiteServiceItemServiceItemPropertyInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "InCabLogs".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InCabLogs.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/".equals(namespaceURI)
        && "ArrayOfStreet".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums
          .ArrayOfStreet.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteAttachmentInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteAttachmentInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "AdHocRoundInstanceResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AdHocRoundInstanceResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobSoapGeoCode".equals(typeName)) {

      return axis2.apache.org.CreateJobSoapGeoCode.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "AddressResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddressResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetServiceItemInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetServiceItemInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "UpdateJobSoapField".equals(typeName)) {

      return axis2.apache.org.UpdateJobSoapField.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI) && "Address".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .Address.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorksheetRolesInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorksheetRolesInput.Factory.parse(reader);
    }

    if ("http://schemas.datacontract.org/2004/07/WSAPIAuth.Web.Enums".equals(namespaceURI)
        && "WorksheetSortOrder".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums
          .WorksheetSortOrder.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfServiceAssignedTo".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfServiceAssignedTo.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "JobSoapWorkLocation".equals(typeName)) {

      return axis2.apache.org.JobSoapWorkLocation.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ApiFullWorksheetDetails".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ApiFullWorksheetDetails.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/API/".equals(namespaceURI)
        && "CreateWorksheetResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .webservices.CreateWorksheetResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceProperty".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceProperty.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "BM_Service".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .BM_Service.Factory.parse(reader);
    }

    if ("http://schemas.microsoft.com/2003/10/Serialization/".equals(namespaceURI)
        && "char".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization
          ._char.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AccountSiteInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAccountSiteInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "RoundIncidents".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .RoundIncidents.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "GetCollectionByUprnAndDatePlusInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetCollectionByUprnAndDatePlusInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceItem".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceItem.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetServiceScheduleInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetServiceScheduleInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceItemWorksheetResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceItemWorksheetResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI) && "Log".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Log
          .Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteContract".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteContract.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetSiteContactInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetSiteContactInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteServiceNotification".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteServiceNotification.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "CollectionResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .CollectionResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "WhereFilterSoap".equals(typeName)) {

      return axis2.apache.org.WhereFilterSoap.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "ArrayOfCreateJobSoapGeoCode".equals(typeName)) {

      return axis2.apache.org.ArrayOfCreateJobSoapGeoCode.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetServiceProperty".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetServiceProperty.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteIDResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteIDResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteServiceSiteServiceItemServiceItemProperty".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteServiceSiteServiceItemServiceItemProperty.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetLogInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetLogInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "LogResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .LogResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ServiceItemCharge".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ServiceItemCharge.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfWalkNumberDetail".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWalkNumberDetail.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorkflowHistoryResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorkflowHistoryResponse.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI) && "Site".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
          .Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.AddSiteServiceNotificationInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputAddSiteServiceNotificationInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Worksheet".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .Worksheet.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "JobSoapBasic".equals(typeName)) {

      return axis2.apache.org.JobSoapBasic.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "WorksheetResult".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .WorksheetResult.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.SiteServiceInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputSiteServiceInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "SiteServiceSiteServiceItemServiceItemPropertyResponse".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .SiteServiceSiteServiceItemServiceItemPropertyResponse.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CoordinatesSoap".equals(typeName)) {

      return axis2.apache.org.CoordinatesSoap.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "UserSoap".equals(typeName)) {

      return axis2.apache.org.UserSoap.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWalkNumbersInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWalkNumbersInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetServiceInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetServiceInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "ArrayOfSiteServiceSiteServiceItemServiceItemProperty".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfSiteServiceSiteServiceItemServiceItemProperty.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetServicePropertyInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetServicePropertyInput.Factory.parse(reader);
    }

    if ("http://webservices.whitespacews.com/".equals(namespaceURI)
        && "Input.GetWorkflowStatusAndEventsInput".equals(typeName)) {

      return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .InputGetWorkflowStatusAndEventsInput.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "QuerySoap".equals(typeName)) {

      return axis2.apache.org.QuerySoap.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobFromTemplateSoapWorkLocation".equals(typeName)) {

      return axis2.apache.org.CreateJobFromTemplateSoapWorkLocation.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "SettingSoap".equals(typeName)) {

      return axis2.apache.org.SettingSoap.Factory.parse(reader);
    }

    if ("".equals(namespaceURI) && "CreateJobSoap".equals(typeName)) {

      return axis2.apache.org.CreateJobSoap.Factory.parse(reader);
    }

    throw new org.apache.axis2.databinding.ADBException(
        "Unsupported type " + namespaceURI + " " + typeName);
  }
}
