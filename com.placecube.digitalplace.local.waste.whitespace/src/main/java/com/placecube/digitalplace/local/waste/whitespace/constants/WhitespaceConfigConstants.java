package com.placecube.digitalplace.local.waste.whitespace.constants;


public final class WhitespaceConfigConstants {
	
	public static final String CONFIGURATION_PID = "com.placecube.digitalplace.local.waste.whitespace.configuration.WhitespaceCompanyConfiguration";
	
	public static final String END_POINT_SERVICE_NAME = "WSAPIService.svc";
	
	private WhitespaceConfigConstants() {
		
	}

}
