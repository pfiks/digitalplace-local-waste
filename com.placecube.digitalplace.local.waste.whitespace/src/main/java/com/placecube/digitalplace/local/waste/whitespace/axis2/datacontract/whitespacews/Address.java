/**
 * Address.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** Address bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Address implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = Address
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for SiteShortAddress */
  protected java.lang.String localSiteShortAddress;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteShortAddressTracker = false;

  public boolean isSiteShortAddressSpecified() {
    return localSiteShortAddressTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteShortAddress() {
    return localSiteShortAddress;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteShortAddress
   */
  public void setSiteShortAddress(java.lang.String param) {
    localSiteShortAddressTracker = true;

    this.localSiteShortAddress = param;
  }

  /** field for SiteName */
  protected java.lang.String localSiteName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteNameTracker = false;

  public boolean isSiteNameSpecified() {
    return localSiteNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteName() {
    return localSiteName;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteName
   */
  public void setSiteName(java.lang.String param) {
    localSiteNameTracker = true;

    this.localSiteName = param;
  }

  /** field for SiteAddressName */
  protected java.lang.String localSiteAddressName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteAddressNameTracker = false;

  public boolean isSiteAddressNameSpecified() {
    return localSiteAddressNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteAddressName() {
    return localSiteAddressName;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteAddressName
   */
  public void setSiteAddressName(java.lang.String param) {
    localSiteAddressNameTracker = true;

    this.localSiteAddressName = param;
  }

  /** field for SiteAddressPrefix */
  protected java.lang.String localSiteAddressPrefix;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteAddressPrefixTracker = false;

  public boolean isSiteAddressPrefixSpecified() {
    return localSiteAddressPrefixTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteAddressPrefix() {
    return localSiteAddressPrefix;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteAddressPrefix
   */
  public void setSiteAddressPrefix(java.lang.String param) {
    localSiteAddressPrefixTracker = true;

    this.localSiteAddressPrefix = param;
  }

  /** field for SiteAddressNumber */
  protected java.lang.String localSiteAddressNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteAddressNumberTracker = false;

  public boolean isSiteAddressNumberSpecified() {
    return localSiteAddressNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteAddressNumber() {
    return localSiteAddressNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteAddressNumber
   */
  public void setSiteAddressNumber(java.lang.String param) {
    localSiteAddressNumberTracker = true;

    this.localSiteAddressNumber = param;
  }

  /** field for SiteAddress1 */
  protected java.lang.String localSiteAddress1;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteAddress1Tracker = false;

  public boolean isSiteAddress1Specified() {
    return localSiteAddress1Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteAddress1() {
    return localSiteAddress1;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteAddress1
   */
  public void setSiteAddress1(java.lang.String param) {
    localSiteAddress1Tracker = true;

    this.localSiteAddress1 = param;
  }

  /** field for SiteAddress2 */
  protected java.lang.String localSiteAddress2;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteAddress2Tracker = false;

  public boolean isSiteAddress2Specified() {
    return localSiteAddress2Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteAddress2() {
    return localSiteAddress2;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteAddress2
   */
  public void setSiteAddress2(java.lang.String param) {
    localSiteAddress2Tracker = true;

    this.localSiteAddress2 = param;
  }

  /** field for SiteTown */
  protected java.lang.String localSiteTown;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteTownTracker = false;

  public boolean isSiteTownSpecified() {
    return localSiteTownTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteTown() {
    return localSiteTown;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteTown
   */
  public void setSiteTown(java.lang.String param) {
    localSiteTownTracker = true;

    this.localSiteTown = param;
  }

  /** field for SiteCity */
  protected java.lang.String localSiteCity;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteCityTracker = false;

  public boolean isSiteCitySpecified() {
    return localSiteCityTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteCity() {
    return localSiteCity;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteCity
   */
  public void setSiteCity(java.lang.String param) {
    localSiteCityTracker = true;

    this.localSiteCity = param;
  }

  /** field for SiteCounty */
  protected java.lang.String localSiteCounty;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteCountyTracker = false;

  public boolean isSiteCountySpecified() {
    return localSiteCountyTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteCounty() {
    return localSiteCounty;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteCounty
   */
  public void setSiteCounty(java.lang.String param) {
    localSiteCountyTracker = true;

    this.localSiteCounty = param;
  }

  /** field for SiteCountry */
  protected java.lang.String localSiteCountry;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteCountryTracker = false;

  public boolean isSiteCountrySpecified() {
    return localSiteCountryTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteCountry() {
    return localSiteCountry;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteCountry
   */
  public void setSiteCountry(java.lang.String param) {
    localSiteCountryTracker = true;

    this.localSiteCountry = param;
  }

  /** field for SitePostCode */
  protected java.lang.String localSitePostCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSitePostCodeTracker = false;

  public boolean isSitePostCodeSpecified() {
    return localSitePostCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSitePostCode() {
    return localSitePostCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param SitePostCode
   */
  public void setSitePostCode(java.lang.String param) {
    localSitePostCodeTracker = true;

    this.localSitePostCode = param;
  }

  /** field for SiteLatitude */
  protected double localSiteLatitude;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteLatitudeTracker = false;

  public boolean isSiteLatitudeSpecified() {
    return localSiteLatitudeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getSiteLatitude() {
    return localSiteLatitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteLatitude
   */
  public void setSiteLatitude(double param) {

    // setting primitive attribute tracker to true
    localSiteLatitudeTracker = !java.lang.Double.isNaN(param);

    this.localSiteLatitude = param;
  }

  /** field for SiteLongitude */
  protected double localSiteLongitude;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteLongitudeTracker = false;

  public boolean isSiteLongitudeSpecified() {
    return localSiteLongitudeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getSiteLongitude() {
    return localSiteLongitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteLongitude
   */
  public void setSiteLongitude(double param) {

    // setting primitive attribute tracker to true
    localSiteLongitudeTracker = !java.lang.Double.isNaN(param);

    this.localSiteLongitude = param;
  }

  /** field for SiteNorthing */
  protected double localSiteNorthing;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteNorthingTracker = false;

  public boolean isSiteNorthingSpecified() {
    return localSiteNorthingTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getSiteNorthing() {
    return localSiteNorthing;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteNorthing
   */
  public void setSiteNorthing(double param) {

    // setting primitive attribute tracker to true
    localSiteNorthingTracker = !java.lang.Double.isNaN(param);

    this.localSiteNorthing = param;
  }

  /** field for SiteEasting */
  protected double localSiteEasting;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteEastingTracker = false;

  public boolean isSiteEastingSpecified() {
    return localSiteEastingTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getSiteEasting() {
    return localSiteEasting;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteEasting
   */
  public void setSiteEasting(double param) {

    // setting primitive attribute tracker to true
    localSiteEastingTracker = !java.lang.Double.isNaN(param);

    this.localSiteEasting = param;
  }

  /** field for AccountSiteUprn */
  protected java.math.BigDecimal localAccountSiteUprn;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteUprnTracker = false;

  public boolean isAccountSiteUprnSpecified() {
    return localAccountSiteUprnTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getAccountSiteUprn() {
    return localAccountSiteUprn;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteUprn
   */
  public void setAccountSiteUprn(java.math.BigDecimal param) {
    localAccountSiteUprnTracker = param != null;

    this.localAccountSiteUprn = param;
  }

  /** field for SiteId */
  protected int localSiteId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteIdTracker = false;

  public boolean isSiteIdSpecified() {
    return localSiteIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteId() {
    return localSiteId;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteId
   */
  public void setSiteId(int param) {

    // setting primitive attribute tracker to true
    localSiteIdTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSiteId = param;
  }

  /** field for AccountSiteId */
  protected int localAccountSiteId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteIdTracker = false;

  public boolean isAccountSiteIdSpecified() {
    return localAccountSiteIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAccountSiteId() {
    return localAccountSiteId;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteId
   */
  public void setAccountSiteId(int param) {

    // setting primitive attribute tracker to true
    localAccountSiteIdTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAccountSiteId = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Address",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Address", xmlWriter);
      }
    }
    if (localSiteShortAddressTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteShortAddress", xmlWriter);

      if (localSiteShortAddress == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteShortAddress);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteName", xmlWriter);

      if (localSiteName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteName);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteAddressNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteAddressName", xmlWriter);

      if (localSiteAddressName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteAddressName);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteAddressPrefixTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteAddressPrefix", xmlWriter);

      if (localSiteAddressPrefix == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteAddressPrefix);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteAddressNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteAddressNumber", xmlWriter);

      if (localSiteAddressNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteAddressNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteAddress1Tracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteAddress1", xmlWriter);

      if (localSiteAddress1 == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteAddress1);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteAddress2Tracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteAddress2", xmlWriter);

      if (localSiteAddress2 == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteAddress2);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteTownTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteTown", xmlWriter);

      if (localSiteTown == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteTown);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteCityTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteCity", xmlWriter);

      if (localSiteCity == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteCity);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteCountyTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteCounty", xmlWriter);

      if (localSiteCounty == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteCounty);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteCountryTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteCountry", xmlWriter);

      if (localSiteCountry == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteCountry);
      }

      xmlWriter.writeEndElement();
    }
    if (localSitePostCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SitePostCode", xmlWriter);

      if (localSitePostCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSitePostCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteLatitudeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteLatitude", xmlWriter);

      if (java.lang.Double.isNaN(localSiteLatitude)) {

        throw new org.apache.axis2.databinding.ADBException("SiteLatitude cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteLatitude));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteLongitudeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteLongitude", xmlWriter);

      if (java.lang.Double.isNaN(localSiteLongitude)) {

        throw new org.apache.axis2.databinding.ADBException("SiteLongitude cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteLongitude));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteNorthingTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteNorthing", xmlWriter);

      if (java.lang.Double.isNaN(localSiteNorthing)) {

        throw new org.apache.axis2.databinding.ADBException("SiteNorthing cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteNorthing));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteEastingTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteEasting", xmlWriter);

      if (java.lang.Double.isNaN(localSiteEasting)) {

        throw new org.apache.axis2.databinding.ADBException("SiteEasting cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteEasting));
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteUprnTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteUprn", xmlWriter);

      if (localAccountSiteUprn == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("AccountSiteUprn cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAccountSiteUprn));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteIdTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteId", xmlWriter);

      if (localSiteId == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SiteId cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteId));
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteIdTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteId", xmlWriter);

      if (localAccountSiteId == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AccountSiteId cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAccountSiteId));
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Address parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Address object = new Address();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Address".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Address)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteShortAddress")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteShortAddress(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteAddressName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteAddressName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteAddressPrefix")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteAddressPrefix(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteAddressNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteAddressNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteAddress1")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteAddress1(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteAddress2")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteAddress2(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteTown")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteTown(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteCity")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteCity(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteCounty")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteCounty(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteCountry")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteCountry(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SitePostCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSitePostCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteLatitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteLatitude" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteLatitude(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteLatitude(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteLongitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteLongitude" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteLongitude(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteLongitude(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteNorthing")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteNorthing" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteNorthing(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteNorthing(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteEasting")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteEasting" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteEasting(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteEasting(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteUprn")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountSiteUprn" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountSiteUprn(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteId")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteId" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteId(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteId")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountSiteId" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountSiteId(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAccountSiteId(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
