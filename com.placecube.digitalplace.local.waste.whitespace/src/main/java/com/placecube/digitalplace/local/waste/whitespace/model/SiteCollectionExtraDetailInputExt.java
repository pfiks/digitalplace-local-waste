package com.placecube.digitalplace.local.waste.whitespace.model;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfInputSiteCollectionExtraDetailInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionExtraDetailsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.InputSiteCollectionExtraDetailInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteServiceSiteServiceItemServiceItemProperty;
import com.placecube.digitalplace.local.waste.whitespace.exception.WhitespaceParameterException;

public class SiteCollectionExtraDetailInputExt {

	private List<String> serviceItemPropertyIds = new ArrayList<>();

	private List<String> serviceItemPropertyNames = new ArrayList<>();

	private List<String> serviceItemPropertyValues = new ArrayList<>();

	public ArrayOfInputSiteCollectionExtraDetailInput getSiteCollectionExtraDetails() throws WhitespaceParameterException {
		if (serviceItemPropertyIds.size() != serviceItemPropertyNames.size() || serviceItemPropertyIds.size() != serviceItemPropertyValues.size()) {
			throw new WhitespaceParameterException("The InputSiteCollectionExtraDetailInput parameter counts don't match.");
		}

		ArrayOfInputSiteCollectionExtraDetailInput arrayOfInputSiteCollectionExtraDetailInput = new ArrayOfInputSiteCollectionExtraDetailInput();
		for (int i = 0; i < serviceItemPropertyIds.size(); i++) {
			InputSiteCollectionExtraDetailInput inputSiteCollectionExtraDetailInput = new InputSiteCollectionExtraDetailInput();
			inputSiteCollectionExtraDetailInput.setServiceItemPropertyId(serviceItemPropertyIds.get(i));
			inputSiteCollectionExtraDetailInput.setServiceItemPropertyName(serviceItemPropertyNames.get(i));
			inputSiteCollectionExtraDetailInput.setServiceItemPropertyValue(serviceItemPropertyValues.get(i));
			arrayOfInputSiteCollectionExtraDetailInput.addInputSiteCollectionExtraDetailInput(inputSiteCollectionExtraDetailInput);
		}

		return arrayOfInputSiteCollectionExtraDetailInput;
	}

	public static ArrayOfInputSiteCollectionExtraDetailInput getSiteCollectionExtraDetailsFromGetSiteCollectionExtraDetailsResponse(
			GetSiteCollectionExtraDetailsResponse getSiteCollectionExtraDetailsResponse) {
		ArrayOfInputSiteCollectionExtraDetailInput arrayOfInputSiteCollectionExtraDetailInput = new ArrayOfInputSiteCollectionExtraDetailInput();

		if (getSiteCollectionExtraDetailsResponse.getGetSiteCollectionExtraDetailsResult() != null
				&& getSiteCollectionExtraDetailsResponse.getGetSiteCollectionExtraDetailsResult().getSiteServiceSiteServiceItemServiceItemProperties() != null && getSiteCollectionExtraDetailsResponse
						.getGetSiteCollectionExtraDetailsResult().getSiteServiceSiteServiceItemServiceItemProperties().getSiteServiceSiteServiceItemServiceItemProperty() != null) {

			SiteServiceSiteServiceItemServiceItemProperty[] siteServiceSiteServiceItemServiceItemProperties = getSiteCollectionExtraDetailsResponse.getGetSiteCollectionExtraDetailsResult()
					.getSiteServiceSiteServiceItemServiceItemProperties().getSiteServiceSiteServiceItemServiceItemProperty();

			for (SiteServiceSiteServiceItemServiceItemProperty siteServiceSiteServiceItemServiceItemProperty : siteServiceSiteServiceItemServiceItemProperties) {
				InputSiteCollectionExtraDetailInput extraDetailInput = new InputSiteCollectionExtraDetailInput();
				extraDetailInput.setServiceItemPropertyId(String.valueOf(siteServiceSiteServiceItemServiceItemProperty.getServiceItemPropertyID()));
				extraDetailInput.setServiceItemPropertyName(siteServiceSiteServiceItemServiceItemProperty.getServiceItemPropertyName());
				extraDetailInput.setServiceItemPropertyValue(siteServiceSiteServiceItemServiceItemProperty.getSiteServiceSiteServiceItemServiceItemPropertyValue());
				arrayOfInputSiteCollectionExtraDetailInput.addInputSiteCollectionExtraDetailInput(extraDetailInput);
			}
		}

		return arrayOfInputSiteCollectionExtraDetailInput;
	}

	@FormParam("serviceItemPropertyId")
	public void setServiceItemPropertyId(List<String> serviceItemPropertyIds) {
		this.serviceItemPropertyIds = serviceItemPropertyIds;
	}

	@FormParam("serviceItemPropertyName")
	public void setServiceItemPropertyName(List<String> serviceItemPropertyNames) {
		this.serviceItemPropertyNames = serviceItemPropertyNames;
	}

	@FormParam("serviceItemPropertyValue")
	public void setServiceItemPropertyValue(List<String> serviceItemPropertyValues) {
		this.serviceItemPropertyValues = serviceItemPropertyValues;
	}

}
