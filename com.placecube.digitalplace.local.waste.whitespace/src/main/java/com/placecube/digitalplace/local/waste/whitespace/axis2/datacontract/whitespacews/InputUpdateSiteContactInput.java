/**
 * InputUpdateSiteContactInput.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** InputUpdateSiteContactInput bean class */
@SuppressWarnings({"unchecked", "unused"})
public class InputUpdateSiteContactInput
    extends com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
        .InputSiteInput
    implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = Input.UpdateSiteContactInput
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for ContactId */
  protected java.lang.String localContactId;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactIdTracker = false;

  public boolean isContactIdSpecified() {
    return localContactIdTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactId() {
    return localContactId;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactId
   */
  public void setContactId(java.lang.String param) {
    localContactIdTracker = true;

    this.localContactId = param;
  }

  /** field for ContactName */
  protected java.lang.String localContactName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactNameTracker = false;

  public boolean isContactNameSpecified() {
    return localContactNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactName() {
    return localContactName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactName
   */
  public void setContactName(java.lang.String param) {
    localContactNameTracker = true;

    this.localContactName = param;
  }

  /** field for ContactTelephoneNumber */
  protected java.lang.String localContactTelephoneNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactTelephoneNumberTracker = false;

  public boolean isContactTelephoneNumberSpecified() {
    return localContactTelephoneNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactTelephoneNumber() {
    return localContactTelephoneNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactTelephoneNumber
   */
  public void setContactTelephoneNumber(java.lang.String param) {
    localContactTelephoneNumberTracker = true;

    this.localContactTelephoneNumber = param;
  }

  /** field for ContactMobileNumber */
  protected java.lang.String localContactMobileNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactMobileNumberTracker = false;

  public boolean isContactMobileNumberSpecified() {
    return localContactMobileNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactMobileNumber() {
    return localContactMobileNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactMobileNumber
   */
  public void setContactMobileNumber(java.lang.String param) {
    localContactMobileNumberTracker = true;

    this.localContactMobileNumber = param;
  }

  /** field for ContactFaxNumber */
  protected java.lang.String localContactFaxNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactFaxNumberTracker = false;

  public boolean isContactFaxNumberSpecified() {
    return localContactFaxNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactFaxNumber() {
    return localContactFaxNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactFaxNumber
   */
  public void setContactFaxNumber(java.lang.String param) {
    localContactFaxNumberTracker = true;

    this.localContactFaxNumber = param;
  }

  /** field for ContactEmail */
  protected java.lang.String localContactEmail;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactEmailTracker = false;

  public boolean isContactEmailSpecified() {
    return localContactEmailTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactEmail() {
    return localContactEmail;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactEmail
   */
  public void setContactEmail(java.lang.String param) {
    localContactEmailTracker = true;

    this.localContactEmail = param;
  }

  /** field for IsMainContact */
  protected boolean localIsMainContact;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIsMainContactTracker = false;

  public boolean isIsMainContactSpecified() {
    return localIsMainContactTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIsMainContact() {
    return localIsMainContact;
  }

  /**
   * Auto generated setter method
   *
   * @param param IsMainContact
   */
  public void setIsMainContact(boolean param) {
    localIsMainContactTracker = true;

    this.localIsMainContact = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    java.lang.String namespacePrefix =
        registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
    if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
      writeAttribute(
          "xsi",
          "http://www.w3.org/2001/XMLSchema-instance",
          "type",
          namespacePrefix + ":Input.UpdateSiteContactInput",
          xmlWriter);
    } else {
      writeAttribute(
          "xsi",
          "http://www.w3.org/2001/XMLSchema-instance",
          "type",
          "Input.UpdateSiteContactInput",
          xmlWriter);
    }

    if (localUprnTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "Uprn", xmlWriter);

      if (localUprn == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localUprn);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteIdTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteId", xmlWriter);

      if (localSiteId == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteId);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactIdTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactId", xmlWriter);

      if (localContactId == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactId);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactName", xmlWriter);

      if (localContactName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactTelephoneNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactTelephoneNumber", xmlWriter);

      if (localContactTelephoneNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactTelephoneNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactMobileNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactMobileNumber", xmlWriter);

      if (localContactMobileNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactMobileNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactFaxNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactFaxNumber", xmlWriter);

      if (localContactFaxNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactFaxNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactEmailTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContactEmail", xmlWriter);

      if (localContactEmail == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContactEmail);
      }

      xmlWriter.writeEndElement();
    }
    if (localIsMainContactTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "IsMainContact", xmlWriter);

      if (false) {

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsMainContact));
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static InputUpdateSiteContactInput parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      InputUpdateSiteContactInput object = new InputUpdateSiteContactInput();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Input.UpdateSiteContactInput".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (InputUpdateSiteContactInput)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Uprn")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setUprn(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteId")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteId(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactId")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactId(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactTelephoneNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactTelephoneNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactMobileNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactMobileNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContactFaxNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactFaxNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContactEmail")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContactEmail(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "IsMainContact")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setIsMainContact(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
