package com.placecube.digitalplace.local.waste.whitespace.model;

import java.util.List;

public class WorksheetContext {

	long companyId;
	long formInstanceRecordId;
	String uprn;
	List<String> binTypes;
	List<String> binSizes;
	String firstName;
	String lastName;
	List<Integer> quantities;
	String worksheetMessage;
	int adHocRoundInstanceId;

	public WorksheetContext(WorksheetContextBuilder builder) {
		companyId = builder.companyId;
		formInstanceRecordId = builder.formInstanceRecordId;
		uprn = builder.uprn;
		binTypes = builder.binTypes;
		binSizes = builder.binSizes;
		firstName = builder.firstName;
		lastName = builder.lastName;
		quantities = builder.quantities;
		worksheetMessage = builder.worksheetMessage;
		adHocRoundInstanceId = builder.adHocRoundInstanceId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public long getFormInstanceRecordId() {
		return formInstanceRecordId;
	}

	public String getUprn() {
		return uprn;
	}

	public List<String> getBinTypes() {
		return binTypes;
	}

	public List<String> getBinSizes() {
		return binSizes;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public List<Integer> getQuantities() {
		return quantities;
	}

	public String getWorksheetMessage() {
		return worksheetMessage;
	}

	public int getAdHocRoundInstanceId() {
		return adHocRoundInstanceId;
	}

	public static class WorksheetContextBuilder {

		long companyId;
		long formInstanceRecordId;
		String uprn;
		List<String> binTypes;
		List<String> binSizes;
		String firstName;
		String lastName;
		List<Integer> quantities;
		String worksheetMessage;
		int adHocRoundInstanceId;

		public WorksheetContextBuilder(long companyId, String uprn) {
			this.companyId = companyId;
			this.uprn = uprn;
		}

		public WorksheetContextBuilder setFormInstanceRecordId(long formInstanceRecordId) {
			this.formInstanceRecordId = formInstanceRecordId;
			return this;
		}

		public WorksheetContextBuilder setBinTypes(List<String> binTypes) {
			this.binTypes = binTypes;
			return this;
		}

		public WorksheetContextBuilder setBinSizes(List<String> binSizes) {
			this.binSizes = binSizes;
			return this;
		}

		public WorksheetContextBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public WorksheetContextBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public WorksheetContextBuilder setQuantities(List<Integer> quantities) {
			this.quantities = quantities;
			return this;
		}

		public WorksheetContextBuilder setWorksheetMessage(String worksheetMessage) {
			this.worksheetMessage = worksheetMessage;
			return this;
		}

		public WorksheetContextBuilder setAdHocRoundInstanceId(int adHocRoundInstanceId) {
			this.adHocRoundInstanceId = adHocRoundInstanceId;
			return this;
		}

		public WorksheetContext build() {
			return new WorksheetContext(this);
		}
	}
}
