/**
 * ApiFullWorksheetDetails.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** ApiFullWorksheetDetails bean class */
@SuppressWarnings({"unchecked", "unused"})
public class ApiFullWorksheetDetails implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ApiFullWorksheetDetails
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for Worksheet */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .Worksheet
      localWorksheet;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetTracker = false;

  public boolean isWorksheetSpecified() {
    return localWorksheetTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet
      getWorksheet() {
    return localWorksheet;
  }

  /**
   * Auto generated setter method
   *
   * @param param Worksheet
   */
  public void setWorksheet(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet
          param) {
    localWorksheetTracker = true;

    this.localWorksheet = param;
  }

  /** field for WorkflowHistories */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorkflowHistory
      localWorkflowHistories;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowHistoriesTracker = false;

  public boolean isWorkflowHistoriesSpecified() {
    return localWorkflowHistoriesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfWorkflowHistory
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorkflowHistory
      getWorkflowHistories() {
    return localWorkflowHistories;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowHistories
   */
  public void setWorkflowHistories(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
              .ArrayOfWorkflowHistory
          param) {
    localWorkflowHistoriesTracker = true;

    this.localWorkflowHistories = param;
  }

  /** field for WSNotes */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetNote
      localWSNotes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWSNotesTracker = false;

  public boolean isWSNotesSpecified() {
    return localWSNotesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfWorksheetNote
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetNote
      getWSNotes() {
    return localWSNotes;
  }

  /**
   * Auto generated setter method
   *
   * @param param WSNotes
   */
  public void setWSNotes(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
              .ArrayOfWorksheetNote
          param) {
    localWSNotesTracker = true;

    this.localWSNotes = param;
  }

  /** field for WSServiceItems */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetServiceItem
      localWSServiceItems;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWSServiceItemsTracker = false;

  public boolean isWSServiceItemsSpecified() {
    return localWSServiceItemsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfWorksheetServiceItem
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetServiceItem
      getWSServiceItems() {
    return localWSServiceItems;
  }

  /**
   * Auto generated setter method
   *
   * @param param WSServiceItems
   */
  public void setWSServiceItems(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
              .ArrayOfWorksheetServiceItem
          param) {
    localWSServiceItemsTracker = true;

    this.localWSServiceItems = param;
  }

  /** field for WSServiceProperties */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetServiceProperty
      localWSServiceProperties;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWSServicePropertiesTracker = false;

  public boolean isWSServicePropertiesSpecified() {
    return localWSServicePropertiesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ArrayOfWorksheetServiceProperty
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ArrayOfWorksheetServiceProperty
      getWSServiceProperties() {
    return localWSServiceProperties;
  }

  /**
   * Auto generated setter method
   *
   * @param param WSServiceProperties
   */
  public void setWSServiceProperties(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
              .ArrayOfWorksheetServiceProperty
          param) {
    localWSServicePropertiesTracker = true;

    this.localWSServiceProperties = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ApiFullWorksheetDetails",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ApiFullWorksheetDetails",
            xmlWriter);
      }
    }
    if (localWorksheetTracker) {
      if (localWorksheet == null) {

        writeStartElement(null, "http://webservices.whitespacews.com/", "Worksheet", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localWorksheet.serialize(
            new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Worksheet"),
            xmlWriter);
      }
    }
    if (localWorkflowHistoriesTracker) {
      if (localWorkflowHistories == null) {

        writeStartElement(
            null, "http://webservices.whitespacews.com/", "WorkflowHistories", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localWorkflowHistories.serialize(
            new javax.xml.namespace.QName(
                "http://webservices.whitespacews.com/", "WorkflowHistories"),
            xmlWriter);
      }
    }
    if (localWSNotesTracker) {
      if (localWSNotes == null) {

        writeStartElement(null, "http://webservices.whitespacews.com/", "WSNotes", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localWSNotes.serialize(
            new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WSNotes"),
            xmlWriter);
      }
    }
    if (localWSServiceItemsTracker) {
      if (localWSServiceItems == null) {

        writeStartElement(
            null, "http://webservices.whitespacews.com/", "WSServiceItems", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localWSServiceItems.serialize(
            new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WSServiceItems"),
            xmlWriter);
      }
    }
    if (localWSServicePropertiesTracker) {
      if (localWSServiceProperties == null) {

        writeStartElement(
            null, "http://webservices.whitespacews.com/", "WSServiceProperties", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localWSServiceProperties.serialize(
            new javax.xml.namespace.QName(
                "http://webservices.whitespacews.com/", "WSServiceProperties"),
            xmlWriter);
      }
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static ApiFullWorksheetDetails parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      ApiFullWorksheetDetails object = new ApiFullWorksheetDetails();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ApiFullWorksheetDetails".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (ApiFullWorksheetDetails)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Worksheet")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setWorksheet(null);
            reader.next();

            reader.next();

          } else {

            object.setWorksheet(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                    .Worksheet.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowHistories")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setWorkflowHistories(null);
            reader.next();

            reader.next();

          } else {

            object.setWorkflowHistories(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                    .ArrayOfWorkflowHistory.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WSNotes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setWSNotes(null);
            reader.next();

            reader.next();

          } else {

            object.setWSNotes(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                    .ArrayOfWorksheetNote.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WSServiceItems")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setWSServiceItems(null);
            reader.next();

            reader.next();

          } else {

            object.setWSServiceItems(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                    .ArrayOfWorksheetServiceItem.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WSServiceProperties")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setWSServiceProperties(null);
            reader.next();

            reader.next();

          } else {

            object.setWSServiceProperties(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                    .ArrayOfWorksheetServiceProperty.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
