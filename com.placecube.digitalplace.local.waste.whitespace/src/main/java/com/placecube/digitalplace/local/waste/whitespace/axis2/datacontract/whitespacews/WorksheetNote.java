/**
 * WorksheetNote.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** WorksheetNote bean class */
@SuppressWarnings({"unchecked", "unused"})
public class WorksheetNote implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = WorksheetNote
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for WorksheetNoteDate */
  protected java.util.Calendar localWorksheetNoteDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetNoteDateTracker = false;

  public boolean isWorksheetNoteDateSpecified() {
    return localWorksheetNoteDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorksheetNoteDate() {
    return localWorksheetNoteDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetNoteDate
   */
  public void setWorksheetNoteDate(java.util.Calendar param) {
    localWorksheetNoteDateTracker = param != null;

    this.localWorksheetNoteDate = param;
  }

  /** field for SecurityID */
  protected int localSecurityID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSecurityIDTracker = false;

  public boolean isSecurityIDSpecified() {
    return localSecurityIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSecurityID() {
    return localSecurityID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SecurityID
   */
  public void setSecurityID(int param) {

    // setting primitive attribute tracker to true
    localSecurityIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSecurityID = param;
  }

  /** field for WorksheetNoteText */
  protected java.lang.String localWorksheetNoteText;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetNoteTextTracker = false;

  public boolean isWorksheetNoteTextSpecified() {
    return localWorksheetNoteTextTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorksheetNoteText() {
    return localWorksheetNoteText;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetNoteText
   */
  public void setWorksheetNoteText(java.lang.String param) {
    localWorksheetNoteTextTracker = true;

    this.localWorksheetNoteText = param;
  }

  /** field for SecurityName */
  protected java.lang.String localSecurityName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSecurityNameTracker = false;

  public boolean isSecurityNameSpecified() {
    return localSecurityNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSecurityName() {
    return localSecurityName;
  }

  /**
   * Auto generated setter method
   *
   * @param param SecurityName
   */
  public void setSecurityName(java.lang.String param) {
    localSecurityNameTracker = true;

    this.localSecurityName = param;
  }

  /** field for WorksheetNoteID */
  protected int localWorksheetNoteID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetNoteIDTracker = false;

  public boolean isWorksheetNoteIDSpecified() {
    return localWorksheetNoteIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetNoteID() {
    return localWorksheetNoteID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetNoteID
   */
  public void setWorksheetNoteID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetNoteIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetNoteID = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":WorksheetNote",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "WorksheetNote", xmlWriter);
      }
    }
    if (localWorksheetNoteDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetNoteDate", xmlWriter);

      if (localWorksheetNoteDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WorksheetNoteDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorksheetNoteDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localSecurityIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SecurityID", xmlWriter);

      if (localSecurityID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SecurityID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSecurityID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetNoteTextTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetNoteText", xmlWriter);

      if (localWorksheetNoteText == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorksheetNoteText);
      }

      xmlWriter.writeEndElement();
    }
    if (localSecurityNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SecurityName", xmlWriter);

      if (localSecurityName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSecurityName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetNoteIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetNoteID", xmlWriter);

      if (localWorksheetNoteID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorksheetNoteID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorksheetNoteID));
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static WorksheetNote parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      WorksheetNote object = new WorksheetNote();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"WorksheetNote".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (WorksheetNote)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetNoteDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetNoteDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetNoteDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SecurityID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SecurityID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSecurityID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSecurityID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetNoteText")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorksheetNoteText(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SecurityName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSecurityName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorksheetNoteID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetNoteID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetNoteID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetNoteID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
