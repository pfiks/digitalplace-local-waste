package com.placecube.digitalplace.local.waste.whitespace.service;

import java.time.Clock;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ApiAdHocRoundInstance;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServicesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteWorksheetsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Service;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ServiceItem;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.SiteService;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.model.ServiceItemInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.WhitespaceService;
import com.placecube.digitalplace.local.waste.whitespace.model.validator.WorksheetValidator;
import com.placecube.digitalplace.local.waste.whitespace.util.WhitespaceWasteServiceUtil;

@Component(immediate = true, service = WhitespaceWasteService.class)
public class WhitespaceWasteService {

	private static final Log LOG = LogFactoryUtil.getLog(WhitespaceWasteService.class);

	@Reference
	private WhitespaceRequestService whiteSpaceRequestService;

	@Reference
	private ResponseParserService responseParserService;

	@Reference
	private WhitespaceConfigurationService whitespaceConfigurationService;

	@Reference
	private WhitespaceWasteServiceHelper whitespaceWasteServiceHelper;

	@Reference
	private WhitespaceWasteServiceUtil whitespaceWasteServiceUtil;

	@Reference
	private JSONFactory jsonFactory;

	public Service getWhitespaceService(long companyId, WhitespaceService whitespaceService) throws Exception {
		GetServicesResponse getServicesResponse = whiteSpaceRequestService.getServices(companyId, whitespaceService.getServiceId());

		Service[] services = responseParserService.parseGetServicesResponse(getServicesResponse);

		if (services.length != 1) {
			throw new WasteRetrievalException("Unexpected number of services returned for service id " + whitespaceService.getServiceId() + ": " + services.length);
		} else {
			return services[0];
		}
	}

	public String getServiceItemId(WhitespaceService wasteService, String serviceItemMappingName, String size, long formInstanceId) throws WasteRetrievalException {
		Map<String, String> serviceItemMappings = wasteService.getServiceItemMappings(serviceItemMappingName);

		if (Validator.isNull(serviceItemMappings)) {
			Optional<String> optionReference = whitespaceWasteServiceHelper.getFormFieldValueOptionReference(formInstanceId, serviceItemMappingName);

			if (optionReference.isPresent()) {
				serviceItemMappings = wasteService.getServiceItemMappings(optionReference.get());
			}
		}

		if (Validator.isNotNull(serviceItemMappings)) {
			return getSizedServiceItemId(serviceItemMappingName, serviceItemMappings, size, formInstanceId);
		} else {
			throw new WasteRetrievalException("Service item  " + serviceItemMappingName + " not found in service configuration");
		}
	}

	private String getSizedServiceItemId(String serviceItemMappingName, Map<String, String> serviceItemMappings, String size, long formInstanceId) throws WasteRetrievalException {
		String serviceItemId = null;

		if (serviceItemMappings != null && !serviceItemMappings.isEmpty()) {

			if (Validator.isNotNull(size)) {
				serviceItemId = serviceItemMappings.get(size);

				if (serviceItemId == null) {
					Optional<String> optionReference = whitespaceWasteServiceHelper.getFormFieldValueOptionReference(formInstanceId, size);

					if (optionReference.isPresent()) {
						serviceItemId = serviceItemMappings.get(optionReference.get());
					}

				}
			} else {
				serviceItemId = serviceItemMappings.entrySet().iterator().next().getValue();
			}
		}

		if (serviceItemId != null) {
			return serviceItemId;
		} else {
			throw new WasteRetrievalException("No service item mapping available for size " + size + " and service item " + serviceItemMappingName);
		}
	}

	public ServiceItem getWhitespaceServiceItem(long companyId, WhitespaceService wasteService, String serviceItemId) throws Exception {
		GetServiceItemsResponse getServiceItemsResponse = whiteSpaceRequestService.getServiceItems(companyId, wasteService.getServiceId(), serviceItemId);

		ServiceItem[] serviceItems = responseParserService.parseGetServiceItemsResponse(getServiceItemsResponse);

		if (serviceItems.length != 1) {
			throw new WasteRetrievalException(
					"Unexpected number of service items returned for service id " + wasteService.getServiceId() + ", service item id " + serviceItemId + ": " + serviceItems.length);
		} else {
			return serviceItems[0];
		}

	}

	public boolean existsOpenWorksheetByUprnServiceAndServiceItems(long companyId, String uprn, String serviceName, List<String> serviceItemIds, Optional<WorksheetValidator> validator)
			throws Exception {
		GetSiteWorksheetsResponse response = whiteSpaceRequestService.getSiteWorksheets(companyId, uprn, null, serviceName, true, "WorksheetSubject", null);

		Worksheet[] worksheets = responseParserService.parseGetSiteWorksheetsResponse(response);

		for (Worksheet worksheet : worksheets) {
			if (isOpenWorksheet(worksheet) && validateWorksheet(validator, worksheet) && whitespaceWasteServiceHelper.anyServiceItemMatchesWorksheetItems(companyId, worksheet, serviceItemIds)) {
				LOG.debug("Open worksheet with id: " + worksheet.getWorksheetID() + " found for uprn: " + uprn + ", serviceName: " + serviceName + ", service item ids: "
						+ jsonFactory.looseSerialize(serviceItemIds));
				return true;
			}
		}

		return false;
	}

	public int getTotalSiteServicesQuantities(SiteService[] siteServices, int serviceId) {
		List<SiteService> filteredSiteServices = Stream.of(siteServices).filter(s -> s.getServiceID() == serviceId && isSiteServiceDateValid(s)).collect(Collectors.toList());
		return filteredSiteServices.stream().mapToInt(SiteService::getQuantity).sum();
	}

	public ServiceItemInputExt createServiceItemInputExt(List<ServiceItem> whitespaceServiceItems, List<Integer> quantities) {
		ServiceItemInputExt serviceItemInputExt = new ServiceItemInputExt();
		serviceItemInputExt.setServiceItemIds(whitespaceServiceItems.stream().map(x -> String.valueOf(x.getServiceItemID())).collect(Collectors.toList()));
		serviceItemInputExt.setServiceItemNames(whitespaceServiceItems.stream().map(ServiceItem::getServiceItemName).collect(Collectors.toList()));
		serviceItemInputExt.setServiceItemQuantities(quantities.stream().map(String::valueOf).collect(Collectors.toList()));

		return serviceItemInputExt;
	}

	public WasteSubscriptionResponse createWasteSubscriptionResponse(String subscriptionRef, SubscriptionType subscriptionType) {
		return new WasteSubscriptionResponse(subscriptionRef, subscriptionType);
	}

	public Optional<Date> getWasteBulkyCollectionDateIfFreeSlots(ApiAdHocRoundInstance apiAdHocRoundInstance) {
		if (apiAdHocRoundInstance.getAdHocRoundInstanceDate() != null && apiAdHocRoundInstance.getSlotsFree() > 0) {
			return Optional.of(apiAdHocRoundInstance.getAdHocRoundInstanceDate().getTime());
		} else {
			return Optional.empty();
		}

	}

	public Map<String, Integer> getItemsToQuantitiesMap(String[] items, String[] quantities) throws WasteRetrievalException {
		try {
			List<String> itemsList = Arrays.stream(items).filter(Validator::isNotNull).collect(Collectors.toList());
			List<Integer> quantitiesList = Arrays.stream(quantities).filter(Validator::isNotNull).map(Integer::parseInt).collect(Collectors.toList());

			if (itemsList.size() != quantitiesList.size()) {
				throw new WasteRetrievalException("Invalid number of items and quantities received");
			} else {
				return IntStream.range(0, itemsList.size()).boxed().collect(Collectors.toMap(itemsList::get, quantitiesList::get, (quantity1, quantity2) -> quantity1 + quantity2, LinkedHashMap::new));
			}

		} catch (NumberFormatException e) {
			throw new WasteRetrievalException(e);
		}

	}

	public Set<BinCollection> filterBinCollectionsByService(long companyId, String serviceName, String formInstanceId, Set<BinCollection> binCollections) throws WasteRetrievalException {
		try {
			Set<BinCollection> filteredBinCollections = null;

			Map<String, WhitespaceService> whitespaceServicesMap = whitespaceConfigurationService.getConfiguredWhitespaceServiceMappingsByFormInstanceId(companyId, Long.parseLong(formInstanceId));

			WhitespaceService whitespaceService = whitespaceServicesMap.get(serviceName);

			if (whitespaceService != null && Validator.isNotNull(whitespaceService.getCollectionServiceName())) {
				filteredBinCollections = binCollections.stream().filter(binCollection -> whitespaceService.getCollectionServiceName().equalsIgnoreCase(binCollection.getName()))
						.collect(Collectors.toSet());
			} else {
				LOG.warn("No service collection name configured");
				filteredBinCollections = binCollections;
			}

			return filteredBinCollections;
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error filtering bin collection dates: " + e.getMessage());
		}

	}

	private boolean validateWorksheet(Optional<WorksheetValidator> validator, Worksheet worksheet) {
		return !validator.isPresent() || !validator.get().isValid(worksheet);
	}

	private boolean isSiteServiceDateValid(SiteService siteService) {
		Calendar siteServiceValidToCalendar = siteService.getSiteServiceValidTo();
		Calendar defaultCalendar = new Calendar.Builder().setDate(1, 1, 1).setTimeOfDay(0, 0, 0).build();

		return siteServiceValidToCalendar.equals(defaultCalendar) || !whitespaceWasteServiceUtil.isBeforeClockDate(Clock.systemDefaultZone(), siteServiceValidToCalendar.getTime());
	}

	private boolean isOpenWorksheet(Worksheet worksheet) {
		return "Open".equals(worksheet.getWorksheetStatusName());
	}

}
