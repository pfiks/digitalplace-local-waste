package com.placecube.digitalplace.local.waste.whitespace.handler;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;

import com.liferay.portal.kernel.util.Validator;

public class IncomingWSSecurityHandler extends AbstractHandler {

	@Override
	public InvocationResponse invoke(MessageContext msgContext) throws AxisFault {
		SOAPEnvelope envelope = msgContext.getEnvelope();
		OMElement header = envelope.getHeader();
		if (header != null) {
			OMElement securityHeader = header.getFirstChildWithName(new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security"));
			if (Validator.isNotNull(securityHeader)) {
				securityHeader.detach();
			}
		}

		return InvocationResponse.CONTINUE;
	}
}
