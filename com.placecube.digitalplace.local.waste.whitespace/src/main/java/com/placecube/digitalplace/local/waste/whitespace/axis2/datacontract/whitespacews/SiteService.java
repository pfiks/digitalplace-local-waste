/**
 * SiteService.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** SiteService bean class */
@SuppressWarnings({"unchecked", "unused"})
public class SiteService implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = SiteService
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for SiteServiceID */
  protected int localSiteServiceID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteServiceIDTracker = false;

  public boolean isSiteServiceIDSpecified() {
    return localSiteServiceIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteServiceID() {
    return localSiteServiceID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteServiceID
   */
  public void setSiteServiceID(int param) {

    // setting primitive attribute tracker to true
    localSiteServiceIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSiteServiceID = param;
  }

  /** field for SiteID */
  protected int localSiteID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteIDTracker = false;

  public boolean isSiteIDSpecified() {
    return localSiteIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteID() {
    return localSiteID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteID
   */
  public void setSiteID(int param) {

    // setting primitive attribute tracker to true
    localSiteIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSiteID = param;
  }

  /** field for AccountSiteID */
  protected int localAccountSiteID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteIDTracker = false;

  public boolean isAccountSiteIDSpecified() {
    return localAccountSiteIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAccountSiteID() {
    return localAccountSiteID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteID
   */
  public void setAccountSiteID(int param) {

    // setting primitive attribute tracker to true
    localAccountSiteIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAccountSiteID = param;
  }

  /** field for ServiceItemID */
  protected int localServiceItemID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemIDTracker = false;

  public boolean isServiceItemIDSpecified() {
    return localServiceItemIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceItemID() {
    return localServiceItemID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemID
   */
  public void setServiceItemID(int param) {

    // setting primitive attribute tracker to true
    localServiceItemIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceItemID = param;
  }

  /** field for ServiceItemName */
  protected java.lang.String localServiceItemName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemNameTracker = false;

  public boolean isServiceItemNameSpecified() {
    return localServiceItemNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemName() {
    return localServiceItemName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemName
   */
  public void setServiceItemName(java.lang.String param) {
    localServiceItemNameTracker = true;

    this.localServiceItemName = param;
  }

  /** field for ServiceItemDescription */
  protected java.lang.String localServiceItemDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemDescriptionTracker = false;

  public boolean isServiceItemDescriptionSpecified() {
    return localServiceItemDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemDescription() {
    return localServiceItemDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemDescription
   */
  public void setServiceItemDescription(java.lang.String param) {
    localServiceItemDescriptionTracker = true;

    this.localServiceItemDescription = param;
  }

  /** field for ServiceItemSerialNumber */
  protected java.lang.String localServiceItemSerialNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemSerialNumberTracker = false;

  public boolean isServiceItemSerialNumberSpecified() {
    return localServiceItemSerialNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemSerialNumber() {
    return localServiceItemSerialNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemSerialNumber
   */
  public void setServiceItemSerialNumber(java.lang.String param) {
    localServiceItemSerialNumberTracker = true;

    this.localServiceItemSerialNumber = param;
  }

  /** field for ServiceItemChipNumber */
  protected java.lang.String localServiceItemChipNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemChipNumberTracker = false;

  public boolean isServiceItemChipNumberSpecified() {
    return localServiceItemChipNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemChipNumber() {
    return localServiceItemChipNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemChipNumber
   */
  public void setServiceItemChipNumber(java.lang.String param) {
    localServiceItemChipNumberTracker = true;

    this.localServiceItemChipNumber = param;
  }

  /** field for ServiceItemUniqueNumber */
  protected java.lang.String localServiceItemUniqueNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemUniqueNumberTracker = false;

  public boolean isServiceItemUniqueNumberSpecified() {
    return localServiceItemUniqueNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemUniqueNumber() {
    return localServiceItemUniqueNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemUniqueNumber
   */
  public void setServiceItemUniqueNumber(java.lang.String param) {
    localServiceItemUniqueNumberTracker = true;

    this.localServiceItemUniqueNumber = param;
  }

  /** field for SiteServiceValidFrom */
  protected java.util.Calendar localSiteServiceValidFrom;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteServiceValidFromTracker = false;

  public boolean isSiteServiceValidFromSpecified() {
    return localSiteServiceValidFromTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getSiteServiceValidFrom() {
    return localSiteServiceValidFrom;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteServiceValidFrom
   */
  public void setSiteServiceValidFrom(java.util.Calendar param) {
    localSiteServiceValidFromTracker = param != null;

    this.localSiteServiceValidFrom = param;
  }

  /** field for ContractValidTo */
  protected java.util.Calendar localContractValidTo;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractValidToTracker = false;

  public boolean isContractValidToSpecified() {
    return localContractValidToTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getContractValidTo() {
    return localContractValidTo;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractValidTo
   */
  public void setContractValidTo(java.util.Calendar param) {
    localContractValidToTracker = param != null;

    this.localContractValidTo = param;
  }

  /** field for SiteServiceValidTo */
  protected java.util.Calendar localSiteServiceValidTo;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteServiceValidToTracker = false;

  public boolean isSiteServiceValidToSpecified() {
    return localSiteServiceValidToTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getSiteServiceValidTo() {
    return localSiteServiceValidTo;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteServiceValidTo
   */
  public void setSiteServiceValidTo(java.util.Calendar param) {
    localSiteServiceValidToTracker = param != null;

    this.localSiteServiceValidTo = param;
  }

  /** field for SiteServiceBlockedFrom */
  protected java.util.Calendar localSiteServiceBlockedFrom;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteServiceBlockedFromTracker = false;

  public boolean isSiteServiceBlockedFromSpecified() {
    return localSiteServiceBlockedFromTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getSiteServiceBlockedFrom() {
    return localSiteServiceBlockedFrom;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteServiceBlockedFrom
   */
  public void setSiteServiceBlockedFrom(java.util.Calendar param) {
    localSiteServiceBlockedFromTracker = param != null;

    this.localSiteServiceBlockedFrom = param;
  }

  /** field for SiteServiceNotes */
  protected java.lang.String localSiteServiceNotes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteServiceNotesTracker = false;

  public boolean isSiteServiceNotesSpecified() {
    return localSiteServiceNotesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteServiceNotes() {
    return localSiteServiceNotes;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteServiceNotes
   */
  public void setSiteServiceNotes(java.lang.String param) {
    localSiteServiceNotesTracker = true;

    this.localSiteServiceNotes = param;
  }

  /** field for ServiceID */
  protected int localServiceID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIDTracker = false;

  public boolean isServiceIDSpecified() {
    return localServiceIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceID() {
    return localServiceID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceID
   */
  public void setServiceID(int param) {

    // setting primitive attribute tracker to true
    localServiceIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceID = param;
  }

  /** field for ServiceName */
  protected java.lang.String localServiceName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceNameTracker = false;

  public boolean isServiceNameSpecified() {
    return localServiceNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceName() {
    return localServiceName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceName
   */
  public void setServiceName(java.lang.String param) {
    localServiceNameTracker = true;

    this.localServiceName = param;
  }

  /** field for ServiceDescription */
  protected java.lang.String localServiceDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceDescriptionTracker = false;

  public boolean isServiceDescriptionSpecified() {
    return localServiceDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceDescription() {
    return localServiceDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceDescription
   */
  public void setServiceDescription(java.lang.String param) {
    localServiceDescriptionTracker = true;

    this.localServiceDescription = param;
  }

  /** field for ContractID */
  protected int localContractID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractIDTracker = false;

  public boolean isContractIDSpecified() {
    return localContractIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getContractID() {
    return localContractID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractID
   */
  public void setContractID(int param) {

    // setting primitive attribute tracker to true
    localContractIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localContractID = param;
  }

  /** field for ContractName */
  protected java.lang.String localContractName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractNameTracker = false;

  public boolean isContractNameSpecified() {
    return localContractNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractName() {
    return localContractName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractName
   */
  public void setContractName(java.lang.String param) {
    localContractNameTracker = true;

    this.localContractName = param;
  }

  /** field for ContractDescription */
  protected java.lang.String localContractDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractDescriptionTracker = false;

  public boolean isContractDescriptionSpecified() {
    return localContractDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractDescription() {
    return localContractDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractDescription
   */
  public void setContractDescription(java.lang.String param) {
    localContractDescriptionTracker = true;

    this.localContractDescription = param;
  }

  /** field for ContractValidFrom */
  protected java.util.Calendar localContractValidFrom;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractValidFromTracker = false;

  public boolean isContractValidFromSpecified() {
    return localContractValidFromTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getContractValidFrom() {
    return localContractValidFrom;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractValidFrom
   */
  public void setContractValidFrom(java.util.Calendar param) {
    localContractValidFromTracker = param != null;

    this.localContractValidFrom = param;
  }

  /** field for ContractTypeID */
  protected int localContractTypeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractTypeIDTracker = false;

  public boolean isContractTypeIDSpecified() {
    return localContractTypeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getContractTypeID() {
    return localContractTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractTypeID
   */
  public void setContractTypeID(int param) {

    // setting primitive attribute tracker to true
    localContractTypeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localContractTypeID = param;
  }

  /** field for ContractTypeName */
  protected java.lang.String localContractTypeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractTypeNameTracker = false;

  public boolean isContractTypeNameSpecified() {
    return localContractTypeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractTypeName() {
    return localContractTypeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractTypeName
   */
  public void setContractTypeName(java.lang.String param) {
    localContractTypeNameTracker = true;

    this.localContractTypeName = param;
  }

  /** field for ContractTypeDescription */
  protected java.lang.String localContractTypeDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractTypeDescriptionTracker = false;

  public boolean isContractTypeDescriptionSpecified() {
    return localContractTypeDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractTypeDescription() {
    return localContractTypeDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractTypeDescription
   */
  public void setContractTypeDescription(java.lang.String param) {
    localContractTypeDescriptionTracker = true;

    this.localContractTypeDescription = param;
  }

  /** field for Quantity */
  protected int localQuantity;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localQuantityTracker = false;

  public boolean isQuantitySpecified() {
    return localQuantityTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getQuantity() {
    return localQuantity;
  }

  /**
   * Auto generated setter method
   *
   * @param param Quantity
   */
  public void setQuantity(int param) {

    // setting primitive attribute tracker to true
    localQuantityTracker = param != java.lang.Integer.MIN_VALUE;

    this.localQuantity = param;
  }

  /** field for Charge */
  protected double localCharge;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localChargeTracker = false;

  public boolean isChargeSpecified() {
    return localChargeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getCharge() {
    return localCharge;
  }

  /**
   * Auto generated setter method
   *
   * @param param Charge
   */
  public void setCharge(double param) {

    // setting primitive attribute tracker to true
    localChargeTracker = !java.lang.Double.isNaN(param);

    this.localCharge = param;
  }

  /** field for Cost */
  protected double localCost;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCostTracker = false;

  public boolean isCostSpecified() {
    return localCostTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getCost() {
    return localCost;
  }

  /**
   * Auto generated setter method
   *
   * @param param Cost
   */
  public void setCost(double param) {

    // setting primitive attribute tracker to true
    localCostTracker = !java.lang.Double.isNaN(param);

    this.localCost = param;
  }

  /** field for CSSIValidFrom */
  protected java.util.Calendar localCSSIValidFrom;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCSSIValidFromTracker = false;

  public boolean isCSSIValidFromSpecified() {
    return localCSSIValidFromTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getCSSIValidFrom() {
    return localCSSIValidFrom;
  }

  /**
   * Auto generated setter method
   *
   * @param param CSSIValidFrom
   */
  public void setCSSIValidFrom(java.util.Calendar param) {
    localCSSIValidFromTracker = param != null;

    this.localCSSIValidFrom = param;
  }

  /** field for CSSIValidTo */
  protected java.util.Calendar localCSSIValidTo;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCSSIValidToTracker = false;

  public boolean isCSSIValidToSpecified() {
    return localCSSIValidToTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getCSSIValidTo() {
    return localCSSIValidTo;
  }

  /**
   * Auto generated setter method
   *
   * @param param CSSIValidTo
   */
  public void setCSSIValidTo(java.util.Calendar param) {
    localCSSIValidToTracker = param != null;

    this.localCSSIValidTo = param;
  }

  /** field for ProductCode */
  protected java.lang.String localProductCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localProductCodeTracker = false;

  public boolean isProductCodeSpecified() {
    return localProductCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getProductCode() {
    return localProductCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ProductCode
   */
  public void setProductCode(java.lang.String param) {
    localProductCodeTracker = true;

    this.localProductCode = param;
  }

  /** field for ChargeTypeID */
  protected int localChargeTypeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localChargeTypeIDTracker = false;

  public boolean isChargeTypeIDSpecified() {
    return localChargeTypeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getChargeTypeID() {
    return localChargeTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ChargeTypeID
   */
  public void setChargeTypeID(int param) {

    // setting primitive attribute tracker to true
    localChargeTypeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localChargeTypeID = param;
  }

  /** field for ChargeTypeName */
  protected java.lang.String localChargeTypeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localChargeTypeNameTracker = false;

  public boolean isChargeTypeNameSpecified() {
    return localChargeTypeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getChargeTypeName() {
    return localChargeTypeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ChargeTypeName
   */
  public void setChargeTypeName(java.lang.String param) {
    localChargeTypeNameTracker = true;

    this.localChargeTypeName = param;
  }

  /** field for PerCollection */
  protected boolean localPerCollection;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPerCollectionTracker = false;

  public boolean isPerCollectionSpecified() {
    return localPerCollectionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getPerCollection() {
    return localPerCollection;
  }

  /**
   * Auto generated setter method
   *
   * @param param PerCollection
   */
  public void setPerCollection(boolean param) {

    // setting primitive attribute tracker to true
    localPerCollectionTracker = true;

    this.localPerCollection = param;
  }

  /** field for PerLift */
  protected boolean localPerLift;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPerLiftTracker = false;

  public boolean isPerLiftSpecified() {
    return localPerLiftTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getPerLift() {
    return localPerLift;
  }

  /**
   * Auto generated setter method
   *
   * @param param PerLift
   */
  public void setPerLift(boolean param) {

    // setting primitive attribute tracker to true
    localPerLiftTracker = true;

    this.localPerLift = param;
  }

  /** field for PerKg */
  protected boolean localPerKg;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPerKgTracker = false;

  public boolean isPerKgSpecified() {
    return localPerKgTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getPerKg() {
    return localPerKg;
  }

  /**
   * Auto generated setter method
   *
   * @param param PerKg
   */
  public void setPerKg(boolean param) {

    // setting primitive attribute tracker to true
    localPerKgTracker = true;

    this.localPerKg = param;
  }

  /** field for PaymentScheduleID */
  protected int localPaymentScheduleID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentScheduleIDTracker = false;

  public boolean isPaymentScheduleIDSpecified() {
    return localPaymentScheduleIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getPaymentScheduleID() {
    return localPaymentScheduleID;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentScheduleID
   */
  public void setPaymentScheduleID(int param) {

    // setting primitive attribute tracker to true
    localPaymentScheduleIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localPaymentScheduleID = param;
  }

  /** field for PaymentScheduleName */
  protected java.lang.String localPaymentScheduleName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentScheduleNameTracker = false;

  public boolean isPaymentScheduleNameSpecified() {
    return localPaymentScheduleNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPaymentScheduleName() {
    return localPaymentScheduleName;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentScheduleName
   */
  public void setPaymentScheduleName(java.lang.String param) {
    localPaymentScheduleNameTracker = true;

    this.localPaymentScheduleName = param;
  }

  /** field for PaymentScheduleDescription */
  protected java.lang.String localPaymentScheduleDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentScheduleDescriptionTracker = false;

  public boolean isPaymentScheduleDescriptionSpecified() {
    return localPaymentScheduleDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPaymentScheduleDescription() {
    return localPaymentScheduleDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentScheduleDescription
   */
  public void setPaymentScheduleDescription(java.lang.String param) {
    localPaymentScheduleDescriptionTracker = true;

    this.localPaymentScheduleDescription = param;
  }

  /** field for PaymentMethodID */
  protected int localPaymentMethodID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentMethodIDTracker = false;

  public boolean isPaymentMethodIDSpecified() {
    return localPaymentMethodIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getPaymentMethodID() {
    return localPaymentMethodID;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentMethodID
   */
  public void setPaymentMethodID(int param) {

    // setting primitive attribute tracker to true
    localPaymentMethodIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localPaymentMethodID = param;
  }

  /** field for PaymentMethodName */
  protected java.lang.String localPaymentMethodName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentMethodNameTracker = false;

  public boolean isPaymentMethodNameSpecified() {
    return localPaymentMethodNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPaymentMethodName() {
    return localPaymentMethodName;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentMethodName
   */
  public void setPaymentMethodName(java.lang.String param) {
    localPaymentMethodNameTracker = true;

    this.localPaymentMethodName = param;
  }

  /** field for PaymentMethodDescription */
  protected java.lang.String localPaymentMethodDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentMethodDescriptionTracker = false;

  public boolean isPaymentMethodDescriptionSpecified() {
    return localPaymentMethodDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPaymentMethodDescription() {
    return localPaymentMethodDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentMethodDescription
   */
  public void setPaymentMethodDescription(java.lang.String param) {
    localPaymentMethodDescriptionTracker = true;

    this.localPaymentMethodDescription = param;
  }

  /** field for GenerateTransaction */
  protected boolean localGenerateTransaction;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localGenerateTransactionTracker = false;

  public boolean isGenerateTransactionSpecified() {
    return localGenerateTransactionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getGenerateTransaction() {
    return localGenerateTransaction;
  }

  /**
   * Auto generated setter method
   *
   * @param param GenerateTransaction
   */
  public void setGenerateTransaction(boolean param) {

    // setting primitive attribute tracker to true
    localGenerateTransactionTracker = true;

    this.localGenerateTransaction = param;
  }

  /** field for RoundSchedule */
  protected java.lang.String localRoundSchedule;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRoundScheduleTracker = false;

  public boolean isRoundScheduleSpecified() {
    return localRoundScheduleTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getRoundSchedule() {
    return localRoundSchedule;
  }

  /**
   * Auto generated setter method
   *
   * @param param RoundSchedule
   */
  public void setRoundSchedule(java.lang.String param) {
    localRoundScheduleTracker = true;

    this.localRoundSchedule = param;
  }

  /** field for SiteContractID */
  protected int localSiteContractID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteContractIDTracker = false;

  public boolean isSiteContractIDSpecified() {
    return localSiteContractIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteContractID() {
    return localSiteContractID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteContractID
   */
  public void setSiteContractID(int param) {

    // setting primitive attribute tracker to true
    localSiteContractIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSiteContractID = param;
  }

  /** field for ContractStatusCode */
  protected java.lang.String localContractStatusCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContractStatusCodeTracker = false;

  public boolean isContractStatusCodeSpecified() {
    return localContractStatusCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContractStatusCode() {
    return localContractStatusCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContractStatusCode
   */
  public void setContractStatusCode(java.lang.String param) {
    localContractStatusCodeTracker = true;

    this.localContractStatusCode = param;
  }

  /** field for EWCCode */
  protected java.lang.String localEWCCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEWCCodeTracker = false;

  public boolean isEWCCodeSpecified() {
    return localEWCCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getEWCCode() {
    return localEWCCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param EWCCode
   */
  public void setEWCCode(java.lang.String param) {
    localEWCCodeTracker = true;

    this.localEWCCode = param;
  }

  /** field for SiteServiceItemQuantity */
  protected java.math.BigDecimal localSiteServiceItemQuantity;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteServiceItemQuantityTracker = false;

  public boolean isSiteServiceItemQuantitySpecified() {
    return localSiteServiceItemQuantityTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getSiteServiceItemQuantity() {
    return localSiteServiceItemQuantity;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteServiceItemQuantity
   */
  public void setSiteServiceItemQuantity(java.math.BigDecimal param) {
    localSiteServiceItemQuantityTracker = param != null;

    this.localSiteServiceItemQuantity = param;
  }

  /** field for SiteServiceItemID */
  protected int localSiteServiceItemID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteServiceItemIDTracker = false;

  public boolean isSiteServiceItemIDSpecified() {
    return localSiteServiceItemIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteServiceItemID() {
    return localSiteServiceItemID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteServiceItemID
   */
  public void setSiteServiceItemID(int param) {

    // setting primitive attribute tracker to true
    localSiteServiceItemIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSiteServiceItemID = param;
  }

  /** field for NextCollectionDate */
  protected java.util.Calendar localNextCollectionDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localNextCollectionDateTracker = false;

  public boolean isNextCollectionDateSpecified() {
    return localNextCollectionDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getNextCollectionDate() {
    return localNextCollectionDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param NextCollectionDate
   */
  public void setNextCollectionDate(java.util.Calendar param) {
    localNextCollectionDateTracker = true;

    this.localNextCollectionDate = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":SiteService",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "SiteService", xmlWriter);
      }
    }
    if (localSiteServiceIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteServiceID", xmlWriter);

      if (localSiteServiceID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SiteServiceID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteServiceID));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteID", xmlWriter);

      if (localSiteID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SiteID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteID));
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteID", xmlWriter);

      if (localAccountSiteID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AccountSiteID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAccountSiteID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemID", xmlWriter);

      if (localServiceItemID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceItemID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceItemID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemName", xmlWriter);

      if (localServiceItemName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemDescription", xmlWriter);

      if (localServiceItemDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemSerialNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemSerialNumber", xmlWriter);

      if (localServiceItemSerialNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemSerialNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemChipNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemChipNumber", xmlWriter);

      if (localServiceItemChipNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemChipNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemUniqueNumberTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemUniqueNumber", xmlWriter);

      if (localServiceItemUniqueNumber == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemUniqueNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteServiceValidFromTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteServiceValidFrom", xmlWriter);

      if (localSiteServiceValidFrom == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "SiteServiceValidFrom cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localSiteServiceValidFrom));
      }

      xmlWriter.writeEndElement();
    }
    if (localContractValidToTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractValidTo", xmlWriter);

      if (localContractValidTo == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContractValidTo cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContractValidTo));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteServiceValidToTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteServiceValidTo", xmlWriter);

      if (localSiteServiceValidTo == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("SiteServiceValidTo cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localSiteServiceValidTo));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteServiceBlockedFromTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteServiceBlockedFrom", xmlWriter);

      if (localSiteServiceBlockedFrom == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "SiteServiceBlockedFrom cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localSiteServiceBlockedFrom));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteServiceNotesTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteServiceNotes", xmlWriter);

      if (localSiteServiceNotes == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSiteServiceNotes);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceID", xmlWriter);

      if (localServiceID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceName", xmlWriter);

      if (localServiceName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceDescription", xmlWriter);

      if (localServiceDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractID", xmlWriter);

      if (localContractID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ContractID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContractID));
      }

      xmlWriter.writeEndElement();
    }
    if (localContractNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractName", xmlWriter);

      if (localContractName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractDescription", xmlWriter);

      if (localContractDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractValidFromTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractValidFrom", xmlWriter);

      if (localContractValidFrom == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContractValidFrom cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localContractValidFrom));
      }

      xmlWriter.writeEndElement();
    }
    if (localContractTypeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractTypeID", xmlWriter);

      if (localContractTypeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ContractTypeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContractTypeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localContractTypeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractTypeName", xmlWriter);

      if (localContractTypeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractTypeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContractTypeDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractTypeDescription", xmlWriter);

      if (localContractTypeDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractTypeDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localQuantityTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "Quantity", xmlWriter);

      if (localQuantity == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("Quantity cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
      }

      xmlWriter.writeEndElement();
    }
    if (localChargeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "Charge", xmlWriter);

      if (java.lang.Double.isNaN(localCharge)) {

        throw new org.apache.axis2.databinding.ADBException("Charge cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCharge));
      }

      xmlWriter.writeEndElement();
    }
    if (localCostTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "Cost", xmlWriter);

      if (java.lang.Double.isNaN(localCost)) {

        throw new org.apache.axis2.databinding.ADBException("Cost cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCost));
      }

      xmlWriter.writeEndElement();
    }
    if (localCSSIValidFromTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "CSSIValidFrom", xmlWriter);

      if (localCSSIValidFrom == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("CSSIValidFrom cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCSSIValidFrom));
      }

      xmlWriter.writeEndElement();
    }
    if (localCSSIValidToTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "CSSIValidTo", xmlWriter);

      if (localCSSIValidTo == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("CSSIValidTo cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCSSIValidTo));
      }

      xmlWriter.writeEndElement();
    }
    if (localProductCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ProductCode", xmlWriter);

      if (localProductCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localProductCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localChargeTypeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ChargeTypeID", xmlWriter);

      if (localChargeTypeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ChargeTypeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localChargeTypeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localChargeTypeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ChargeTypeName", xmlWriter);

      if (localChargeTypeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localChargeTypeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localPerCollectionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PerCollection", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("PerCollection cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPerCollection));
      }

      xmlWriter.writeEndElement();
    }
    if (localPerLiftTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PerLift", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("PerLift cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPerLift));
      }

      xmlWriter.writeEndElement();
    }
    if (localPerKgTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PerKg", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("PerKg cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPerKg));
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentScheduleIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PaymentScheduleID", xmlWriter);

      if (localPaymentScheduleID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("PaymentScheduleID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localPaymentScheduleID));
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentScheduleNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PaymentScheduleName", xmlWriter);

      if (localPaymentScheduleName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localPaymentScheduleName);
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentScheduleDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PaymentScheduleDescription", xmlWriter);

      if (localPaymentScheduleDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localPaymentScheduleDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentMethodIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PaymentMethodID", xmlWriter);

      if (localPaymentMethodID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("PaymentMethodID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPaymentMethodID));
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentMethodNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PaymentMethodName", xmlWriter);

      if (localPaymentMethodName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localPaymentMethodName);
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentMethodDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "PaymentMethodDescription", xmlWriter);

      if (localPaymentMethodDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localPaymentMethodDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localGenerateTransactionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "GenerateTransaction", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("GenerateTransaction cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localGenerateTransaction));
      }

      xmlWriter.writeEndElement();
    }
    if (localRoundScheduleTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "RoundSchedule", xmlWriter);

      if (localRoundSchedule == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localRoundSchedule);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteContractIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteContractID", xmlWriter);

      if (localSiteContractID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SiteContractID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteContractID));
      }

      xmlWriter.writeEndElement();
    }
    if (localContractStatusCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ContractStatusCode", xmlWriter);

      if (localContractStatusCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localContractStatusCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localEWCCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "EWCCode", xmlWriter);

      if (localEWCCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localEWCCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteServiceItemQuantityTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteServiceItemQuantity", xmlWriter);

      if (localSiteServiceItemQuantity == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "SiteServiceItemQuantity cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localSiteServiceItemQuantity));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteServiceItemIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteServiceItemID", xmlWriter);

      if (localSiteServiceItemID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SiteServiceItemID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localSiteServiceItemID));
      }

      xmlWriter.writeEndElement();
    }
    if (localNextCollectionDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "NextCollectionDate", xmlWriter);

      if (localNextCollectionDate == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localNextCollectionDate));
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static SiteService parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      SiteService object = new SiteService();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"SiteService".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (SiteService)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteServiceID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteServiceID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteServiceID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteServiceID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "SiteID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountSiteID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountSiteID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAccountSiteID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceItemID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemSerialNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemSerialNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemChipNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemChipNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemUniqueNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemUniqueNumber(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteServiceValidFrom")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteServiceValidFrom" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteServiceValidFrom(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractValidTo")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContractValidTo" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContractValidTo(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteServiceValidTo")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteServiceValidTo" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteServiceValidTo(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteServiceBlockedFrom")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteServiceBlockedFrom" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteServiceBlockedFrom(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteServiceNotes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteServiceNotes(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContractID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContractID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContractID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setContractID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ContractName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractValidFrom")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContractValidFrom" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContractValidFrom(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContractTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContractTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setContractTypeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractTypeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractTypeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractTypeDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractTypeDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Quantity")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Quantity" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setQuantity(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setQuantity(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Charge")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Charge" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCharge(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setCharge(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Cost")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Cost" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCost(org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setCost(java.lang.Double.NaN);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "CSSIValidFrom")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "CSSIValidFrom" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCSSIValidFrom(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "CSSIValidTo")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "CSSIValidTo" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCSSIValidTo(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ProductCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setProductCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ChargeTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ChargeTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setChargeTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setChargeTypeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ChargeTypeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setChargeTypeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PerCollection")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PerCollection" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPerCollection(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "PerLift")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PerLift" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPerLift(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "PerKg")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PerKg" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPerKg(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PaymentScheduleID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PaymentScheduleID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPaymentScheduleID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setPaymentScheduleID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PaymentScheduleName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setPaymentScheduleName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PaymentScheduleDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setPaymentScheduleDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PaymentMethodID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PaymentMethodID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPaymentMethodID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setPaymentMethodID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PaymentMethodName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setPaymentMethodName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "PaymentMethodDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setPaymentMethodDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "GenerateTransaction")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "GenerateTransaction" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setGenerateTransaction(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "RoundSchedule")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setRoundSchedule(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteContractID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteContractID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteContractID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteContractID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ContractStatusCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setContractStatusCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "EWCCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setEWCCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteServiceItemQuantity")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteServiceItemQuantity" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteServiceItemQuantity(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteServiceItemID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteServiceItemID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteServiceItemID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteServiceItemID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "NextCollectionDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setNextCollectionDate(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
