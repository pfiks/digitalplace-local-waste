/**
 * AccountSite.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** AccountSite bean class */
@SuppressWarnings({"unchecked", "unused"})
public class AccountSite implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = AccountSite
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for AccountSiteID */
  protected int localAccountSiteID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteIDTracker = false;

  public boolean isAccountSiteIDSpecified() {
    return localAccountSiteIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAccountSiteID() {
    return localAccountSiteID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteID
   */
  public void setAccountSiteID(int param) {

    // setting primitive attribute tracker to true
    localAccountSiteIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAccountSiteID = param;
  }

  /** field for AccountSiteUPRN */
  protected java.math.BigDecimal localAccountSiteUPRN;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteUPRNTracker = false;

  public boolean isAccountSiteUPRNSpecified() {
    return localAccountSiteUPRNTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getAccountSiteUPRN() {
    return localAccountSiteUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteUPRN
   */
  public void setAccountSiteUPRN(java.math.BigDecimal param) {
    localAccountSiteUPRNTracker = param != null;

    this.localAccountSiteUPRN = param;
  }

  /** field for BusinessTypeCode */
  protected java.lang.String localBusinessTypeCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localBusinessTypeCodeTracker = false;

  public boolean isBusinessTypeCodeSpecified() {
    return localBusinessTypeCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getBusinessTypeCode() {
    return localBusinessTypeCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param BusinessTypeCode
   */
  public void setBusinessTypeCode(java.lang.String param) {
    localBusinessTypeCodeTracker = true;

    this.localBusinessTypeCode = param;
  }

  /** field for BusinessTypeName */
  protected java.lang.String localBusinessTypeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localBusinessTypeNameTracker = false;

  public boolean isBusinessTypeNameSpecified() {
    return localBusinessTypeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getBusinessTypeName() {
    return localBusinessTypeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param BusinessTypeName
   */
  public void setBusinessTypeName(java.lang.String param) {
    localBusinessTypeNameTracker = true;

    this.localBusinessTypeName = param;
  }

  /** field for SalesZoneCode */
  protected java.lang.String localSalesZoneCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSalesZoneCodeTracker = false;

  public boolean isSalesZoneCodeSpecified() {
    return localSalesZoneCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSalesZoneCode() {
    return localSalesZoneCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param SalesZoneCode
   */
  public void setSalesZoneCode(java.lang.String param) {
    localSalesZoneCodeTracker = true;

    this.localSalesZoneCode = param;
  }

  /** field for SalesZoneName */
  protected java.lang.String localSalesZoneName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSalesZoneNameTracker = false;

  public boolean isSalesZoneNameSpecified() {
    return localSalesZoneNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSalesZoneName() {
    return localSalesZoneName;
  }

  /**
   * Auto generated setter method
   *
   * @param param SalesZoneName
   */
  public void setSalesZoneName(java.lang.String param) {
    localSalesZoneNameTracker = true;

    this.localSalesZoneName = param;
  }

  /** field for AccountSiteStatusID */
  protected int localAccountSiteStatusID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteStatusIDTracker = false;

  public boolean isAccountSiteStatusIDSpecified() {
    return localAccountSiteStatusIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAccountSiteStatusID() {
    return localAccountSiteStatusID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteStatusID
   */
  public void setAccountSiteStatusID(int param) {

    // setting primitive attribute tracker to true
    localAccountSiteStatusIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAccountSiteStatusID = param;
  }

  /** field for AccountSiteStatusName */
  protected java.lang.String localAccountSiteStatusName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteStatusNameTracker = false;

  public boolean isAccountSiteStatusNameSpecified() {
    return localAccountSiteStatusNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAccountSiteStatusName() {
    return localAccountSiteStatusName;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteStatusName
   */
  public void setAccountSiteStatusName(java.lang.String param) {
    localAccountSiteStatusNameTracker = true;

    this.localAccountSiteStatusName = param;
  }

  /** field for AreaID */
  protected int localAreaID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAreaIDTracker = false;

  public boolean isAreaIDSpecified() {
    return localAreaIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAreaID() {
    return localAreaID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AreaID
   */
  public void setAreaID(int param) {

    // setting primitive attribute tracker to true
    localAreaIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAreaID = param;
  }

  /** field for AreaName */
  protected java.lang.String localAreaName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAreaNameTracker = false;

  public boolean isAreaNameSpecified() {
    return localAreaNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAreaName() {
    return localAreaName;
  }

  /**
   * Auto generated setter method
   *
   * @param param AreaName
   */
  public void setAreaName(java.lang.String param) {
    localAreaNameTracker = true;

    this.localAreaName = param;
  }

  /** field for AccountSiteTypeID */
  protected int localAccountSiteTypeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteTypeIDTracker = false;

  public boolean isAccountSiteTypeIDSpecified() {
    return localAccountSiteTypeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAccountSiteTypeID() {
    return localAccountSiteTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteTypeID
   */
  public void setAccountSiteTypeID(int param) {

    // setting primitive attribute tracker to true
    localAccountSiteTypeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAccountSiteTypeID = param;
  }

  /** field for AccountSiteTypeName */
  protected java.lang.String localAccountSiteTypeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountSiteTypeNameTracker = false;

  public boolean isAccountSiteTypeNameSpecified() {
    return localAccountSiteTypeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAccountSiteTypeName() {
    return localAccountSiteTypeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountSiteTypeName
   */
  public void setAccountSiteTypeName(java.lang.String param) {
    localAccountSiteTypeNameTracker = true;

    this.localAccountSiteTypeName = param;
  }

  /** field for ParishID */
  protected int localParishID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localParishIDTracker = false;

  public boolean isParishIDSpecified() {
    return localParishIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getParishID() {
    return localParishID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ParishID
   */
  public void setParishID(int param) {

    // setting primitive attribute tracker to true
    localParishIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localParishID = param;
  }

  /** field for ParishName */
  protected java.lang.String localParishName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localParishNameTracker = false;

  public boolean isParishNameSpecified() {
    return localParishNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getParishName() {
    return localParishName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ParishName
   */
  public void setParishName(java.lang.String param) {
    localParishNameTracker = true;

    this.localParishName = param;
  }

  /** field for WardID */
  protected int localWardID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWardIDTracker = false;

  public boolean isWardIDSpecified() {
    return localWardIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWardID() {
    return localWardID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WardID
   */
  public void setWardID(int param) {

    // setting primitive attribute tracker to true
    localWardIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWardID = param;
  }

  /** field for WardName */
  protected java.lang.String localWardName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWardNameTracker = false;

  public boolean isWardNameSpecified() {
    return localWardNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWardName() {
    return localWardName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WardName
   */
  public void setWardName(java.lang.String param) {
    localWardNameTracker = true;

    this.localWardName = param;
  }

  /** field for NumChildren */
  protected int localNumChildren;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localNumChildrenTracker = false;

  public boolean isNumChildrenSpecified() {
    return localNumChildrenTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getNumChildren() {
    return localNumChildren;
  }

  /**
   * Auto generated setter method
   *
   * @param param NumChildren
   */
  public void setNumChildren(int param) {

    // setting primitive attribute tracker to true
    localNumChildrenTracker = param != java.lang.Integer.MIN_VALUE;

    this.localNumChildren = param;
  }

  /** field for Site */
  protected com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
      localSite;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteTracker = false;

  public boolean isSiteSpecified() {
    return localSiteTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
      getSite() {
    return localSite;
  }

  /**
   * Auto generated setter method
   *
   * @param param Site
   */
  public void setSite(
      com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Site
          param) {
    localSiteTracker = true;

    this.localSite = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":AccountSite",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "AccountSite", xmlWriter);
      }
    }
    if (localAccountSiteIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteID", xmlWriter);

      if (localAccountSiteID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AccountSiteID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAccountSiteID));
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteUPRNTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteUPRN", xmlWriter);

      if (localAccountSiteUPRN == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("AccountSiteUPRN cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAccountSiteUPRN));
      }

      xmlWriter.writeEndElement();
    }
    if (localBusinessTypeCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "BusinessTypeCode", xmlWriter);

      if (localBusinessTypeCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localBusinessTypeCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localBusinessTypeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "BusinessTypeName", xmlWriter);

      if (localBusinessTypeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localBusinessTypeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localSalesZoneCodeTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SalesZoneCode", xmlWriter);

      if (localSalesZoneCode == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSalesZoneCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localSalesZoneNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SalesZoneName", xmlWriter);

      if (localSalesZoneName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localSalesZoneName);
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteStatusIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteStatusID", xmlWriter);

      if (localAccountSiteStatusID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AccountSiteStatusID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localAccountSiteStatusID));
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteStatusNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteStatusName", xmlWriter);

      if (localAccountSiteStatusName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localAccountSiteStatusName);
      }

      xmlWriter.writeEndElement();
    }
    if (localAreaIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AreaID", xmlWriter);

      if (localAreaID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AreaID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAreaID));
      }

      xmlWriter.writeEndElement();
    }
    if (localAreaNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AreaName", xmlWriter);

      if (localAreaName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localAreaName);
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteTypeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteTypeID", xmlWriter);

      if (localAccountSiteTypeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AccountSiteTypeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localAccountSiteTypeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountSiteTypeNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AccountSiteTypeName", xmlWriter);

      if (localAccountSiteTypeName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localAccountSiteTypeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localParishIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ParishID", xmlWriter);

      if (localParishID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ParishID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParishID));
      }

      xmlWriter.writeEndElement();
    }
    if (localParishNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ParishName", xmlWriter);

      if (localParishName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localParishName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWardIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WardID", xmlWriter);

      if (localWardID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WardID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWardID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWardNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WardName", xmlWriter);

      if (localWardName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWardName);
      }

      xmlWriter.writeEndElement();
    }
    if (localNumChildrenTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "NumChildren", xmlWriter);

      if (localNumChildren == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("NumChildren cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumChildren));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteTracker) {
      if (localSite == null) {

        writeStartElement(null, "http://webservices.whitespacews.com/", "Site", xmlWriter);

        // write the nil attribute
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
        xmlWriter.writeEndElement();
      } else {
        localSite.serialize(
            new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Site"),
            xmlWriter);
      }
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static AccountSite parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      AccountSite object = new AccountSite();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"AccountSite".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (AccountSite)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountSiteID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountSiteID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAccountSiteID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteUPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountSiteUPRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountSiteUPRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "BusinessTypeCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setBusinessTypeCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "BusinessTypeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setBusinessTypeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SalesZoneCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSalesZoneCode(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SalesZoneName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSalesZoneName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteStatusID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountSiteStatusID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountSiteStatusID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAccountSiteStatusID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteStatusName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setAccountSiteStatusName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "AreaID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AreaID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAreaID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAreaID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "AreaName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setAreaName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountSiteTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountSiteTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAccountSiteTypeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AccountSiteTypeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setAccountSiteTypeName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ParishID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ParishID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setParishID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setParishID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ParishName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setParishName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WardID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WardID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWardID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWardID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WardName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWardName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "NumChildren")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "NumChildren" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setNumChildren(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setNumChildren(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "Site")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            object.setSite(null);
            reader.next();

            reader.next();

          } else {

            object.setSite(
                com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                    .Site.Factory.parse(reader));

            reader.next();
          }
        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
