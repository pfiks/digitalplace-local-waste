/**
 * WorkflowHistory.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** WorkflowHistory bean class */
@SuppressWarnings({"unchecked", "unused"})
public class WorkflowHistory implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = WorkflowHistory
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for WorkflowHistoryID */
  protected int localWorkflowHistoryID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowHistoryIDTracker = false;

  public boolean isWorkflowHistoryIDSpecified() {
    return localWorkflowHistoryIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorkflowHistoryID() {
    return localWorkflowHistoryID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowHistoryID
   */
  public void setWorkflowHistoryID(int param) {

    // setting primitive attribute tracker to true
    localWorkflowHistoryIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorkflowHistoryID = param;
  }

  /** field for WorkflowGUID */
  protected java.lang.String localWorkflowGUID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowGUIDTracker = false;

  public boolean isWorkflowGUIDSpecified() {
    return localWorkflowGUIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkflowGUID() {
    return localWorkflowGUID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowGUID
   */
  public void setWorkflowGUID(java.lang.String param) {
    localWorkflowGUIDTracker = true;

    this.localWorkflowGUID = param;
  }

  /** field for WorksheetID */
  protected int localWorksheetID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorksheetIDTracker = false;

  public boolean isWorksheetIDSpecified() {
    return localWorksheetIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorksheetID() {
    return localWorksheetID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorksheetID
   */
  public void setWorksheetID(int param) {

    // setting primitive attribute tracker to true
    localWorksheetIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorksheetID = param;
  }

  /** field for SiteContractID */
  protected int localSiteContractID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteContractIDTracker = false;

  public boolean isSiteContractIDSpecified() {
    return localSiteContractIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteContractID() {
    return localSiteContractID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteContractID
   */
  public void setSiteContractID(int param) {

    // setting primitive attribute tracker to true
    localSiteContractIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localSiteContractID = param;
  }

  /** field for WorkflowTypeID */
  protected int localWorkflowTypeID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowTypeIDTracker = false;

  public boolean isWorkflowTypeIDSpecified() {
    return localWorkflowTypeIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorkflowTypeID() {
    return localWorkflowTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowTypeID
   */
  public void setWorkflowTypeID(int param) {

    // setting primitive attribute tracker to true
    localWorkflowTypeIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorkflowTypeID = param;
  }

  /** field for WorkflowID */
  protected int localWorkflowID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowIDTracker = false;

  public boolean isWorkflowIDSpecified() {
    return localWorkflowIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorkflowID() {
    return localWorkflowID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowID
   */
  public void setWorkflowID(int param) {

    // setting primitive attribute tracker to true
    localWorkflowIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorkflowID = param;
  }

  /** field for WorkflowEventName */
  protected java.lang.String localWorkflowEventName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowEventNameTracker = false;

  public boolean isWorkflowEventNameSpecified() {
    return localWorkflowEventNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkflowEventName() {
    return localWorkflowEventName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowEventName
   */
  public void setWorkflowEventName(java.lang.String param) {
    localWorkflowEventNameTracker = true;

    this.localWorkflowEventName = param;
  }

  /** field for WorkflowEventFriendlyName */
  protected java.lang.String localWorkflowEventFriendlyName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowEventFriendlyNameTracker = false;

  public boolean isWorkflowEventFriendlyNameSpecified() {
    return localWorkflowEventFriendlyNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkflowEventFriendlyName() {
    return localWorkflowEventFriendlyName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowEventFriendlyName
   */
  public void setWorkflowEventFriendlyName(java.lang.String param) {
    localWorkflowEventFriendlyNameTracker = true;

    this.localWorkflowEventFriendlyName = param;
  }

  /** field for WorkflowEventDate */
  protected java.util.Calendar localWorkflowEventDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowEventDateTracker = false;

  public boolean isWorkflowEventDateSpecified() {
    return localWorkflowEventDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getWorkflowEventDate() {
    return localWorkflowEventDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowEventDate
   */
  public void setWorkflowEventDate(java.util.Calendar param) {
    localWorkflowEventDateTracker = param != null;

    this.localWorkflowEventDate = param;
  }

  /** field for WorkflowRaisedBy */
  protected java.lang.String localWorkflowRaisedBy;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowRaisedByTracker = false;

  public boolean isWorkflowRaisedBySpecified() {
    return localWorkflowRaisedByTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkflowRaisedBy() {
    return localWorkflowRaisedBy;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowRaisedBy
   */
  public void setWorkflowRaisedBy(java.lang.String param) {
    localWorkflowRaisedByTracker = true;

    this.localWorkflowRaisedBy = param;
  }

  /** field for WorkflowStateName */
  protected java.lang.String localWorkflowStateName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowStateNameTracker = false;

  public boolean isWorkflowStateNameSpecified() {
    return localWorkflowStateNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkflowStateName() {
    return localWorkflowStateName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowStateName
   */
  public void setWorkflowStateName(java.lang.String param) {
    localWorkflowStateNameTracker = true;

    this.localWorkflowStateName = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":WorkflowHistory",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "WorkflowHistory",
            xmlWriter);
      }
    }
    if (localWorkflowHistoryIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowHistoryID", xmlWriter);

      if (localWorkflowHistoryID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorkflowHistoryID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorkflowHistoryID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowGUIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowGUID", xmlWriter);

      if (localWorkflowGUID == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkflowGUID);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorksheetIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorksheetID", xmlWriter);

      if (localWorksheetID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorksheetID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorksheetID));
      }

      xmlWriter.writeEndElement();
    }
    if (localSiteContractIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "SiteContractID", xmlWriter);

      if (localSiteContractID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("SiteContractID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteContractID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowTypeIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowTypeID", xmlWriter);

      if (localWorkflowTypeID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorkflowTypeID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorkflowTypeID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowID", xmlWriter);

      if (localWorkflowID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorkflowID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorkflowID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowEventNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowEventName", xmlWriter);

      if (localWorkflowEventName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkflowEventName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowEventFriendlyNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowEventFriendlyName", xmlWriter);

      if (localWorkflowEventFriendlyName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkflowEventFriendlyName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowEventDateTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowEventDate", xmlWriter);

      if (localWorkflowEventDate == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WorkflowEventDate cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localWorkflowEventDate));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowRaisedByTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowRaisedBy", xmlWriter);

      if (localWorkflowRaisedBy == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkflowRaisedBy);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowStateNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowStateName", xmlWriter);

      if (localWorkflowStateName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkflowStateName);
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static WorkflowHistory parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      WorkflowHistory object = new WorkflowHistory();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"WorkflowHistory".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (WorkflowHistory)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowHistoryID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkflowHistoryID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkflowHistoryID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorkflowHistoryID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorkflowGUID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkflowGUID(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorksheetID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorksheetID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorksheetID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorksheetID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "SiteContractID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteContractID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteContractID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setSiteContractID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkflowTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkflowTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorkflowTypeID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorkflowID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkflowID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkflowID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorkflowID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowEventName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkflowEventName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowEventFriendlyName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkflowEventFriendlyName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowEventDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkflowEventDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkflowEventDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowRaisedBy")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkflowRaisedBy(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "WorkflowStateName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkflowStateName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
