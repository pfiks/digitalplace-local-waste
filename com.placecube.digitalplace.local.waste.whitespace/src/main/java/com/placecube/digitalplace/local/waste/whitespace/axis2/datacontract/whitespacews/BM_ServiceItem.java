/**
 * BM_ServiceItem.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** BM_ServiceItem bean class */
@SuppressWarnings({"unchecked", "unused"})
public class BM_ServiceItem implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = BM_ServiceItem
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for ServiceItemID */
  protected int localServiceItemID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceItemID() {
    return localServiceItemID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemID
   */
  public void setServiceItemID(int param) {

    this.localServiceItemID = param;
  }

  /** field for ServiceItemName */
  protected java.lang.String localServiceItemName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemNameTracker = false;

  public boolean isServiceItemNameSpecified() {
    return localServiceItemNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemName() {
    return localServiceItemName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemName
   */
  public void setServiceItemName(java.lang.String param) {
    localServiceItemNameTracker = true;

    this.localServiceItemName = param;
  }

  /** field for ServiceItemDescription */
  protected java.lang.String localServiceItemDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemDescriptionTracker = false;

  public boolean isServiceItemDescriptionSpecified() {
    return localServiceItemDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemDescription() {
    return localServiceItemDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemDescription
   */
  public void setServiceItemDescription(java.lang.String param) {
    localServiceItemDescriptionTracker = true;

    this.localServiceItemDescription = param;
  }

  /** field for ServiceItemAllowDiscount */
  protected boolean localServiceItemAllowDiscount;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceItemAllowDiscount() {
    return localServiceItemAllowDiscount;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemAllowDiscount
   */
  public void setServiceItemAllowDiscount(boolean param) {

    this.localServiceItemAllowDiscount = param;
  }

  /** field for ServiceItemIsAdditional */
  protected boolean localServiceItemIsAdditional;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceItemIsAdditional() {
    return localServiceItemIsAdditional;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemIsAdditional
   */
  public void setServiceItemIsAdditional(boolean param) {

    this.localServiceItemIsAdditional = param;
  }

  /** field for ServiceItemRunFirstWorkflowStep */
  protected boolean localServiceItemRunFirstWorkflowStep;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceItemRunFirstWorkflowStep() {
    return localServiceItemRunFirstWorkflowStep;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemRunFirstWorkflowStep
   */
  public void setServiceItemRunFirstWorkflowStep(boolean param) {

    this.localServiceItemRunFirstWorkflowStep = param;
  }

  /** field for ServiceWorkflowID */
  protected int localServiceWorkflowID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceWorkflowID() {
    return localServiceWorkflowID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceWorkflowID
   */
  public void setServiceWorkflowID(int param) {

    this.localServiceWorkflowID = param;
  }

  /** field for ServiceWorkflowName */
  protected java.lang.String localServiceWorkflowName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceWorkflowNameTracker = false;

  public boolean isServiceWorkflowNameSpecified() {
    return localServiceWorkflowNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceWorkflowName() {
    return localServiceWorkflowName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceWorkflowName
   */
  public void setServiceWorkflowName(java.lang.String param) {
    localServiceWorkflowNameTracker = true;

    this.localServiceWorkflowName = param;
  }

  /** field for ServiceItemTemplateName */
  protected java.lang.String localServiceItemTemplateName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceItemTemplateNameTracker = false;

  public boolean isServiceItemTemplateNameSpecified() {
    return localServiceItemTemplateNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceItemTemplateName() {
    return localServiceItemTemplateName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemTemplateName
   */
  public void setServiceItemTemplateName(java.lang.String param) {
    localServiceItemTemplateNameTracker = true;

    this.localServiceItemTemplateName = param;
  }

  /** field for ServiceItemWasteSlots */
  protected int localServiceItemWasteSlots;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceItemWasteSlots() {
    return localServiceItemWasteSlots;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemWasteSlots
   */
  public void setServiceItemWasteSlots(int param) {

    this.localServiceItemWasteSlots = param;
  }

  /** field for ServiceItemPhysicallyExists */
  protected boolean localServiceItemPhysicallyExists;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceItemPhysicallyExists() {
    return localServiceItemPhysicallyExists;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemPhysicallyExists
   */
  public void setServiceItemPhysicallyExists(boolean param) {

    this.localServiceItemPhysicallyExists = param;
  }

  /** field for ServiceItemAverageWeight */
  protected double localServiceItemAverageWeight;

  /**
   * Auto generated getter method
   *
   * @return double
   */
  public double getServiceItemAverageWeight() {
    return localServiceItemAverageWeight;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceItemAverageWeight
   */
  public void setServiceItemAverageWeight(double param) {

    this.localServiceItemAverageWeight = param;
  }

  /** field for WorkflowID */
  protected int localWorkflowID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorkflowID() {
    return localWorkflowID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowID
   */
  public void setWorkflowID(int param) {

    this.localWorkflowID = param;
  }

  /** field for WorkflowName */
  protected java.lang.String localWorkflowName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowNameTracker = false;

  public boolean isWorkflowNameSpecified() {
    return localWorkflowNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkflowName() {
    return localWorkflowName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowName
   */
  public void setWorkflowName(java.lang.String param) {
    localWorkflowNameTracker = true;

    this.localWorkflowName = param;
  }

  /** field for AuthorityID */
  protected int localAuthorityID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAuthorityID() {
    return localAuthorityID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AuthorityID
   */
  public void setAuthorityID(int param) {

    this.localAuthorityID = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":BM_ServiceItem",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "BM_ServiceItem",
            xmlWriter);
      }
    }

    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "ServiceItemID", xmlWriter);

    if (localServiceItemID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ServiceItemID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceItemID));
    }

    xmlWriter.writeEndElement();
    if (localServiceItemNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemName", xmlWriter);

      if (localServiceItemName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemDescription", xmlWriter);

      if (localServiceItemDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemDescription);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "ServiceItemAllowDiscount", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException(
          "ServiceItemAllowDiscount cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localServiceItemAllowDiscount));
    }

    xmlWriter.writeEndElement();

    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "ServiceItemIsAdditional", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException(
          "ServiceItemIsAdditional cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localServiceItemIsAdditional));
    }

    xmlWriter.writeEndElement();

    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "ServiceItemRunFirstWorkflowStep", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException(
          "ServiceItemRunFirstWorkflowStep cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localServiceItemRunFirstWorkflowStep));
    }

    xmlWriter.writeEndElement();

    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "ServiceWorkflowID", xmlWriter);

    if (localServiceWorkflowID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ServiceWorkflowID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceWorkflowID));
    }

    xmlWriter.writeEndElement();
    if (localServiceWorkflowNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceWorkflowName", xmlWriter);

      if (localServiceWorkflowName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceWorkflowName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceItemTemplateNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceItemTemplateName", xmlWriter);

      if (localServiceItemTemplateName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceItemTemplateName);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "ServiceItemWasteSlots", xmlWriter);

    if (localServiceItemWasteSlots == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ServiceItemWasteSlots cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localServiceItemWasteSlots));
    }

    xmlWriter.writeEndElement();

    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "ServiceItemPhysicallyExists", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException(
          "ServiceItemPhysicallyExists cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localServiceItemPhysicallyExists));
    }

    xmlWriter.writeEndElement();

    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "ServiceItemAverageWeight", xmlWriter);

    if (java.lang.Double.isNaN(localServiceItemAverageWeight)) {

      throw new org.apache.axis2.databinding.ADBException(
          "ServiceItemAverageWeight cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localServiceItemAverageWeight));
    }

    xmlWriter.writeEndElement();

    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "WorkflowID", xmlWriter);

    if (localWorkflowID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("WorkflowID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorkflowID));
    }

    xmlWriter.writeEndElement();
    if (localWorkflowNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowName", xmlWriter);

      if (localWorkflowName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkflowName);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://webservices.whitespacews.com/";
    writeStartElement(null, namespace, "AuthorityID", xmlWriter);

    if (localAuthorityID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("AuthorityID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAuthorityID));
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static BM_ServiceItem parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      BM_ServiceItem object = new BM_ServiceItem();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"BM_ServiceItem".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (BM_ServiceItem)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemAllowDiscount")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemAllowDiscount" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemAllowDiscount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemIsAdditional")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemIsAdditional" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemIsAdditional(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemRunFirstWorkflowStep")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemRunFirstWorkflowStep" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemRunFirstWorkflowStep(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceWorkflowID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceWorkflowID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceWorkflowID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceWorkflowName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceWorkflowName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemTemplateName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceItemTemplateName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemWasteSlots")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemWasteSlots" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemWasteSlots(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemPhysicallyExists")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemPhysicallyExists" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemPhysicallyExists(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceItemAverageWeight")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceItemAverageWeight" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceItemAverageWeight(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorkflowID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkflowID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkflowID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorkflowName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkflowName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "AuthorityID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AuthorityID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAuthorityID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
