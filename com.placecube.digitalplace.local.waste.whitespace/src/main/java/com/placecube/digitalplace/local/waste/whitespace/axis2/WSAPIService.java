/**
 * WSAPIService.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:03 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2;

/*
 *  WSAPIService java interface
 */

public interface WSAPIService {

  /**
   * Auto generated method signature
   *
   * @param getSiteLogs
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteLogsResponse
      getSiteLogs(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteLogs
              getSiteLogs)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param deleteSiteServiceItemRoundSchedule
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .DeleteSiteServiceItemRoundScheduleResponse
      deleteSiteServiceItemRoundSchedule(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .DeleteSiteServiceItemRoundSchedule
              deleteSiteServiceItemRoundSchedule)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteAvailableRounds
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteAvailableRoundsResponse
      getSiteAvailableRounds(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteAvailableRounds
              getSiteAvailableRounds)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteInfo
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteInfoResponse
      getSiteInfo(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteInfo
              getSiteInfo)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param progressWorkflow
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .ProgressWorkflowResponse
      progressWorkflow(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .ProgressWorkflow
              progressWorkflow)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addSiteServiceItemRoundSchedule
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddSiteServiceItemRoundScheduleResponse
      addSiteServiceItemRoundSchedule(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddSiteServiceItemRoundSchedule
              addSiteServiceItemRoundSchedule)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getFullSiteCollections
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetFullSiteCollectionsResponse
      getFullSiteCollections(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetFullSiteCollections
              getFullSiteCollections)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWalkNumbers
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWalkNumbersResponse
      getWalkNumbers(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWalkNumbers
              getWalkNumbers)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addSiteServiceNotification
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddSiteServiceNotificationResponse
      addSiteServiceNotification(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddSiteServiceNotification
              addSiteServiceNotification)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addSiteServiceItem
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddSiteServiceItemResponse
      addSiteServiceItem(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddSiteServiceItem
              addSiteServiceItem)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetDetails
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetDetailsResponse
      getWorksheetDetails(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetDetails
              getWorksheetDetails)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param updateWorksheet
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .UpdateWorksheetResponse
      updateWorksheet(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .UpdateWorksheet
              updateWorksheet)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addSiteLog
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddSiteLogResponse
      addSiteLog(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddSiteLog
              addSiteLog)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSites
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSitesResponse
      getSites(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSites
              getSites)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param updateSiteServiceItem
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .UpdateSiteServiceItemResponse
      updateSiteServiceItem(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .UpdateSiteServiceItem
              updateSiteServiceItem)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getCollectionSlots
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetCollectionSlotsResponse
      getCollectionSlots(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetCollectionSlots
              getCollectionSlots)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetDetailNotes
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetDetailNotesResponse
      getWorksheetDetailNotes(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetDetailNotes
              getWorksheetDetailNotes)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteCollections
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteCollectionsResponse
      getSiteCollections(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteCollections
              getSiteCollections)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteNotifications
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteNotificationsResponse
      getSiteNotifications(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteNotifications
              getSiteNotifications)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param raiseCancelWorksheetRequest
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .RaiseCancelWorksheetRequestResponse
      raiseCancelWorksheetRequest(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .RaiseCancelWorksheetRequest
              raiseCancelWorksheetRequest)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addAoMWorksheetLogs
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddAoMWorksheetLogsResponse
      addAoMWorksheetLogs(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddAoMWorksheetLogs
              addAoMWorksheetLogs)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteServiceItemRoundSchedules
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteServiceItemRoundSchedulesResponse
      getSiteServiceItemRoundSchedules(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteServiceItemRoundSchedules
              getSiteServiceItemRoundSchedules)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getCollectionByUprnAndDate
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetCollectionByUprnAndDateResponse
      getCollectionByUprnAndDate(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetCollectionByUprnAndDate
              getCollectionByUprnAndDate)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetChargeMatrix
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetChargeMatrixResponse
      getWorksheetChargeMatrix(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetChargeMatrix
              getWorksheetChargeMatrix)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getServiceItems
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetServiceItemsResponse
      getServiceItems(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetServiceItems
              getServiceItems)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param deleteSiteServiceItem
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .DeleteSiteServiceItemResponse
      deleteSiteServiceItem(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .DeleteSiteServiceItem
              deleteSiteServiceItem)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteCollectionExtraDetails
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteCollectionExtraDetailsResponse
      getSiteCollectionExtraDetails(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteCollectionExtraDetails
              getSiteCollectionExtraDetails)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetServiceItems
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetServiceItemsResponse
      getWorksheetServiceItems(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetServiceItems
              getWorksheetServiceItems)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getNotifications
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetNotificationsResponse
      getNotifications(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetNotifications
              getNotifications)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetDetailServiceItems
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetDetailServiceItemsResponse
      getWorksheetDetailServiceItems(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetDetailServiceItems
              getWorksheetDetailServiceItems)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param updateSiteServiceNotification
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .UpdateSiteServiceNotificationResponse
      updateSiteServiceNotification(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .UpdateSiteServiceNotification
              updateSiteServiceNotification)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param helloWorld
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .HelloWorldResponse
      helloWorld(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .HelloWorld
              helloWorld)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addSiteAttachment
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddSiteAttachmentResponse
      addSiteAttachment(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddSiteAttachment
              addSiteAttachment)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteContracts
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteContractsResponse
      getSiteContracts(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteContracts
              getSiteContracts)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetRoles
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetRolesResponse
      getWorksheetRoles(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetRoles
              getWorksheetRoles)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteAttachments
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteAttachmentsResponse
      getSiteAttachments(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteAttachments
              getSiteAttachments)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getFullWorksheetDetails
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetFullWorksheetDetailsResponse
      getFullWorksheetDetails(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetFullWorksheetDetails
              getFullWorksheetDetails)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getAddresses
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetAddressesResponse
      getAddresses(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetAddresses
              getAddresses)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetAttachments
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetAttachmentsResponse
      getWorksheetAttachments(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetAttachments
              getWorksheetAttachments)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addAoMRoundLogs
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddAoMRoundLogsResponse
      addAoMRoundLogs(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddAoMRoundLogs
              addAoMRoundLogs)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteId
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteIdResponse
      getSiteId(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteId
              getSiteId)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getCollectionByUprnAndDatePlus
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetCollectionByUprnAndDatePlusResponse
      getCollectionByUprnAndDatePlus(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetCollectionByUprnAndDatePlus
              getCollectionByUprnAndDatePlus)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getInCabLogs
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetInCabLogsResponse
      getInCabLogs(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetInCabLogs
              getInCabLogs)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param cancelWorksheet
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .CancelWorksheetResponse
      cancelWorksheet(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .CancelWorksheet
              cancelWorksheet)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getStreets
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetStreetsResponse
      getStreets(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetStreets
              getStreets)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param updateSiteContact
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .UpdateSiteContactResponse
      updateSiteContact(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .UpdateSiteContact
              updateSiteContact)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getServices
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetServicesResponse
      getServices(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetServices
              getServices)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param deleteSiteContact
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .DeleteSiteContactResponse
      deleteSiteContact(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .DeleteSiteContact
              deleteSiteContact)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getAddressesByCoordinatesRadius
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetAddressesByCoordinatesRadiusResponse
      getAddressesByCoordinatesRadius(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetAddressesByCoordinatesRadius
              getAddressesByCoordinatesRadius)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetDetailExtraInfoFields
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetDetailExtraInfoFieldsResponse
      getWorksheetDetailExtraInfoFields(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetDetailExtraInfoFields
              getWorksheetDetailExtraInfoFields)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addWorksheetNotes
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddWorksheetNotesResponse
      addWorksheetNotes(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddWorksheetNotes
              addWorksheetNotes)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetsByReference
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetsByReferenceResponse
      getWorksheetsByReference(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetsByReference
              getWorksheetsByReference)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getAccountSiteId
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetAccountSiteIdResponse
      getAccountSiteId(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetAccountSiteId
              getAccountSiteId)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addWorksheetAttachment
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddWorksheetAttachmentResponse
      addWorksheetAttachment(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddWorksheetAttachment
              addWorksheetAttachment)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetExtraInfoFields
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetExtraInfoFieldsResponse
      getWorksheetExtraInfoFields(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetExtraInfoFields
              getWorksheetExtraInfoFields)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteContacts
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteContactsResponse
      getSiteContacts(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteContacts
              getSiteContacts)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteWorksheets
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteWorksheetsResponse
      getSiteWorksheets(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteWorksheets
              getSiteWorksheets)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param createWorksheet
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .CreateWorksheetResponse
      createWorksheet(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .CreateWorksheet
              createWorksheet)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getLogsSearch
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetLogsSearchResponse
      getLogsSearch(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetLogsSearch
              getLogsSearch)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteFlags
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteFlagsResponse
      getSiteFlags(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteFlags
              getSiteFlags)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorksheetDetailEvents
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorksheetDetailEventsResponse
      getWorksheetDetailEvents(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorksheetDetailEvents
              getWorksheetDetailEvents)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getWorkflowStatusAndEvents
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetWorkflowStatusAndEventsResponse
      getWorkflowStatusAndEvents(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetWorkflowStatusAndEvents
              getWorkflowStatusAndEvents)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getActiveAddresses
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetActiveAddressesResponse
      getActiveAddresses(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetActiveAddresses
              getActiveAddresses)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param getSiteIncidents
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .GetSiteIncidentsResponse
      getSiteIncidents(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .GetSiteIncidents
              getSiteIncidents)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param addSiteContact
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .AddSiteContactResponse
      addSiteContact(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .AddSiteContact
              addSiteContact)
          throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param updateWorkflowEventDate
   */
  public com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
          .UpdateWorkflowEventDateResponse
      updateWorkflowEventDate(
          com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                  .UpdateWorkflowEventDate
              updateWorkflowEventDate)
          throws java.rmi.RemoteException;

  //
}
