package com.placecube.digitalplace.local.waste.whitespace.model;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.WorksheetSortOrder;

public class WorksheetSortOrderWrapper extends WorksheetSortOrder {

	private static final long serialVersionUID = 1L;

	public WorksheetSortOrderWrapper(String value) {
		super(value, true);
	}

}
