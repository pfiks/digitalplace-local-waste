package com.placecube.digitalplace.local.waste.whitespace.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.WorksheetServiceItem;

@Component(immediate = true, service = WhitespaceWasteServiceHelper.class)
public class WhitespaceWasteServiceHelper {

	private static final Log LOG = LogFactoryUtil.getLog(WhitespaceWasteServiceHelper.class);

	@Reference
	private WhitespaceRequestService whiteSpaceRequestService;

	@Reference
	private ResponseParserService responseParserService;

	@Reference
	private DDMFormInstanceLocalService ddmFormInstanceLocalService;

	public boolean anyServiceItemMatchesWorksheetItems(long companyId, Worksheet worksheet, List<String> serviceItemIds) throws Exception {
		if (serviceItemIds != null && !serviceItemIds.isEmpty()) {
			GetWorksheetDetailServiceItemsResponse getWorksheetDetailServiceItemsResponse = whiteSpaceRequestService.getWorksheetDetailServiceItems(companyId,
					String.valueOf(worksheet.getWorksheetID()), null);

			WorksheetServiceItem[] worksheetServiceItems = responseParserService.parseGetWorksheetDetailServiceItemsResponse(getWorksheetDetailServiceItemsResponse);

			return Arrays.stream(worksheetServiceItems).anyMatch(x -> serviceItemIds.contains(String.valueOf(x.getServiceItemID())));
		} else {
			return true;
		}
	}

	public Optional<String> getFormFieldValueOptionReference(long formInstanceId, String value) {
		try {
			DDMFormInstance ddmFormInstance = ddmFormInstanceLocalService.getDDMFormInstance(formInstanceId);
			DDMStructure ddmStructure = ddmFormInstance.getStructure();

			return findDDMFormFieldOptionReferenceForValue(ddmStructure, value);
		} catch (PortalException e) {
			LOG.error(e);
		}
		return Optional.empty();
	}

	private Optional<String> findDDMFormFieldOptionReferenceForValue(DDMStructure ddmStructure, String value) {
		List<DDMFormField> ddmFormFields = ddmStructure.getDDMFormFields(false);

		return ddmFormFields.stream().map(ddmFormField -> getFieldOptionReferenceForValue(ddmFormField, value)).flatMap(Optional::stream).findFirst();

	}

	private Optional<String> getFieldOptionReferenceForValue(DDMFormField ddmFormField, String value) {
		DDMFormFieldOptions options = ddmFormField.getDDMFormFieldOptions();

		if (options != null) {
			Map<String, LocalizedValue> optionsMap = options.getOptions();

			Optional<String> optionKey = optionsMap.entrySet().stream().filter(entry -> value.equals(entry.getValue().getString(LocaleUtil.getDefault()))).map(Map.Entry::getKey).findFirst();

			return optionKey.map(options::getOptionReference);
		}

		return Optional.empty();

	}
}
