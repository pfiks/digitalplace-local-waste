package com.placecube.digitalplace.local.waste.whitespace.util;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.Worksheet;

@Component(immediate = true, service = WhitespaceWasteServiceUtil.class)
public class WhitespaceWasteServiceUtil {

	public boolean isCurrentFinancialYear(Clock currentClock, Worksheet worksheet) {
		return worksheet.getWorksheetCreatedDate() == null || isCalendarInCurrentFinancialYear(currentClock, worksheet.getWorksheetCreatedDate());
	}

	public boolean isWorksheetCreatedDateInTwoLastWeeks(Clock currentClock, Worksheet worksheet) {
		return worksheet.getWorksheetCreatedDate() == null || isCalendarInTwoLastWeeks(currentClock, worksheet.getWorksheetCreatedDate());
	}

	public boolean isBeforeClockDate(Clock currentClock, Date time) {
		LocalDateTime todayLocalDateTime = LocalDate.ofInstant(currentClock.instant(), ZoneId.systemDefault()).atStartOfDay();
		LocalDateTime localDateTime = LocalDate.ofInstant(time.toInstant(), ZoneId.systemDefault()).atStartOfDay();

		return localDateTime.isBefore(todayLocalDateTime);
	}

	public String formatSchedule(String schedule) {
		return schedule.replaceAll("Fort.*", "fornightly")//
				.replaceAll("^(\\w{0,10}\\s?)(Monday|Mon)(\\s?\\w{0,10}\\s?)$", "Monday $3")//
				.replaceAll("^(\\w{0,10}\\s?)(Tuesday|Tue)(\\s?\\w{0,10}\\s?)$", "Tuesday $3")//
				.replaceAll("^(\\w{0,10}\\s?)(Wednesday|Wed)(\\s?\\w{0,10}\\s?)$", "Wednesday $3")//
				.replaceAll("^(\\w{0,10}\\s?)(Thursday|Thu)(\\s?\\w{0,10}\\s?)$", "Thursday $3")//
				.replaceAll("^(\\w{0,10}\\s?)(Friday|Fri)(\\s?\\w{0,10}\\s?)$", "Friday $3")//
				.replaceAll("^(\\w{0,10}\\s?)(Saturday|Sat)(\\s?\\w{0,10}\\s?)$", "Saturday $3")//
				.replaceAll("^(\\w{0,10}\\s?)(Sunday|Sun)(\\s?\\w{0,10}\\s?)$", "Sunday $3")//
				.replaceAll("\\s+", " ");
	}

	private boolean isCalendarInCurrentFinancialYear(Clock currentClock, Calendar calendar) {
		LocalDateTime currentLocalDateTime = LocalDate.ofInstant(currentClock.instant(), ZoneId.systemDefault()).atStartOfDay();
		LocalDateTime financialYearStartLocalDateTime = currentLocalDateTime.withDayOfMonth(6).withMonth(Month.APRIL.getValue());

		if (currentLocalDateTime.isBefore(financialYearStartLocalDateTime)) {
			financialYearStartLocalDateTime = financialYearStartLocalDateTime.minusYears(1);
		}

		LocalDateTime parameterCalendarLocalDateTime = LocalDate.ofInstant(calendar.toInstant(), ZoneId.systemDefault()).atStartOfDay();

		return !parameterCalendarLocalDateTime.isBefore(financialYearStartLocalDateTime);
	}

	private boolean isCalendarInTwoLastWeeks(Clock currentClock, Calendar calendar) {
		LocalDateTime currentLocalDateTime = LocalDate.ofInstant(currentClock.instant(), ZoneId.systemDefault()).atStartOfDay();
		LocalDateTime twoLastWeeksStart = currentLocalDateTime.minusWeeks(2);

		LocalDateTime parameterCalendarLocalDateTime = LocalDate.ofInstant(calendar.toInstant(), ZoneId.systemDefault()).atStartOfDay();

		return !parameterCalendarLocalDateTime.isBefore(twoLastWeeksStart);
	}
}
