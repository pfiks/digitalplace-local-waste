package com.placecube.digitalplace.local.waste.whitespace.application;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteAttachmentResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteContactResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteLogResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteServiceItemRoundScheduleResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddSiteServiceNotificationResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddWorksheetAttachmentResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.AddWorksheetNotesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CancelWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.CreateWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.DeleteSiteContactResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.DeleteSiteServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.DeleteSiteServiceItemRoundScheduleResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetAccountSiteIdResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetActiveAddressesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetAddressesByCoordinatesRadiusResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetAddressesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionByUprnAndDateResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetCollectionSlotsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetFullSiteCollectionsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetFullWorksheetDetailsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetInCabLogsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetLogsSearchResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetNotificationsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetServicesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteAttachmentsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteAvailableRoundsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionExtraDetailsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteCollectionsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteContactsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteContractsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteFlagsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteIdResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteIncidentsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteInfoResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteLogsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteNotificationsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteServiceItemRoundSchedulesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSiteWorksheetsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetSitesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetStreetsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWalkNumbersResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetAttachmentsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetChargeMatrixResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailEventsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailExtraInfoFieldsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailNotesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetDetailsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetExtraInfoFieldsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetRolesResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetServiceItemsResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.GetWorksheetsByReferenceResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.ProgressWorkflowResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateSiteContactResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateSiteServiceItemResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateSiteServiceNotificationResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateWorkflowEventDateResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews.UpdateWorksheetResponse;
import com.placecube.digitalplace.local.waste.whitespace.axis2.microsoft.schemas.serialization.arrays.ArrayOfstring;
import com.placecube.digitalplace.local.waste.whitespace.model.ServiceItemInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.ServicePropertyInputExt;
import com.placecube.digitalplace.local.waste.whitespace.model.SiteCollectionExtraDetailInputExt;
import com.placecube.digitalplace.local.waste.whitespace.service.JsonResponseParserService;
import com.placecube.digitalplace.local.waste.whitespace.service.ObjectFactoryService;
import com.placecube.digitalplace.local.waste.whitespace.service.WhitespaceRequestService;

@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/waste/whitespace", JaxrsWhiteboardConstants.JAX_RS_NAME + "=Whitespace.Rest",
		"oauth2.scopechecker.type=none", "auth.verifier.guest.allowed=false", "liferay.access.control.disable=true" }, service = Application.class)
public class WhitespaceRESTApplication extends Application {

	@Reference
	private JsonResponseParserService jsonResponseParserService;

	@Reference
	private ObjectFactoryService objectFatoryService;

	@Reference
	private WhitespaceRequestService whiteSpaceRequestService;

	@POST
	@Path("/add-site-attachment")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addSiteAttachment(@FormParam("uprn") String uprn, @FormParam("siteId") String siteId, @FormParam("fileName") String fileName, @FormParam("fileExtension") String fileExtension,
			@FormParam("base64String") String base64String, @Context HttpServletRequest request) throws Exception {

		AddSiteAttachmentResponse response = whiteSpaceRequestService.addSiteAttachment(getCompanyId(request), uprn, siteId, fileName, fileExtension, base64String);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/add-site-contact")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addSiteContact(@FormParam("uprn") String uprn, @FormParam("siteId") String siteId, @FormParam("isMainContact") boolean isMainContact, @FormParam("contactName") String contactName,
			@FormParam("contactEmail") String contactEmail, @FormParam("contactTelephoneNumber") String contactTelephoneNumber, @FormParam("contactMobileNumber") String contactMobileNumber,
			@FormParam("contactFaxNumber") String contactFaxNumber, @Context HttpServletRequest request) throws Exception {

		AddSiteContactResponse response = whiteSpaceRequestService.addSiteContact(getCompanyId(request), uprn, siteId, isMainContact, contactName, contactEmail, contactTelephoneNumber,
				contactMobileNumber, contactFaxNumber);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/add-site-log")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addSiteLog(@FormParam("uprn") String uprn, @FormParam("logText") String logText, @FormParam("accountSiteId") String accountSiteId, @Context HttpServletRequest request)
			throws Exception {

		AddSiteLogResponse response = whiteSpaceRequestService.addSiteLog(getCompanyId(request), uprn, logText, accountSiteId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/add-site-service-item")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addSiteServiceItem(@FormParam("uprn") String uprn, @FormParam("siteId") String siteId, @FormParam("serviceId") String serviceId, @FormParam("serviceItemId") String serviceItemId,
			@FormParam("serviceItemQuantity") String serviceItemQuantity, @FormParam("serviceItemValidFrom") String serviceItemValidFrom, @FormParam("serviceItemValidTo") String serviceItemValidTo,
			@Context HttpServletRequest request) throws Exception {

		AddSiteServiceItemResponse response = whiteSpaceRequestService.addSiteServiceItem(getCompanyId(request), uprn, siteId, serviceId, serviceItemId, serviceItemQuantity, serviceItemValidFrom,
				serviceItemValidTo);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/add-site-service-item-round-schedule")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addSiteServiceItemRoundSchedule(@FormParam("siteServiceId") String siteServiceId, @FormParam("roundRoundAreaServiceScheduleId") String roundRoundAreaServiceScheduleId,
			@Context HttpServletRequest request) throws Exception {

		AddSiteServiceItemRoundScheduleResponse response = whiteSpaceRequestService.addSiteServiceItemRoundSchedule(getCompanyId(request), siteServiceId, roundRoundAreaServiceScheduleId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/add-site-service-notification")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addSiteServiceNotification(@FormParam("uprn") String uprn, @FormParam("siteId") String siteId, @FormParam("notificationId") String notificationId,
			@FormParam("serviceId") String serviceId, @FormParam("siteServiceNotificationNotes") String siteServiceNotificationNotes,
			@FormParam("serviceNotificationValidFrom") String serviceNotificationValidFrom, @FormParam("siteServiceNotificationValidTo") String siteServiceNotificationValidTo,
			@Context HttpServletRequest request) throws Exception {

		AddSiteServiceNotificationResponse response = whiteSpaceRequestService.addSiteServiceNotification(getCompanyId(request), uprn, siteId, notificationId, serviceId, siteServiceNotificationNotes,
				serviceNotificationValidFrom, siteServiceNotificationValidTo);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/add-worksheet-attachment")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addWorksheetAttachment(@FormParam("worksheetId") String worksheetId, @FormParam("fileName") String fileName, @FormParam("fileExtension") String fileExtension,
			@FormParam("base64String") String base64String, @FormParam("worksheetRef") String worksheetRef, @Context HttpServletRequest request) throws Exception {

		AddWorksheetAttachmentResponse response = whiteSpaceRequestService.addWorksheetAttachment(getCompanyId(request), worksheetId, fileName, fileExtension, base64String, worksheetRef);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/add-worksheet-notes")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addWorksheetNotes(@FormParam("worksheetId") String worksheetId, @FormParam("worksheetRef") String worksheetRef, @FormParam("noteText") String noteText,
			@Context HttpServletRequest request) throws Exception {

		AddWorksheetNotesResponse response = whiteSpaceRequestService.addWorksheetNotes(getCompanyId(request), worksheetId, worksheetRef, noteText);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/cancel-worksheet")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String cancelWorksheet(@FormParam("worksheetId") String worksheetId, @FormParam("worksheetRef") String worksheetRef, @Context HttpServletRequest request) throws Exception {

		CancelWorksheetResponse response = whiteSpaceRequestService.cancelWorksheet(getCompanyId(request), worksheetId, worksheetRef);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/create-worksheet")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createWorksheet(@BeanParam ServiceItemInputExt serviceItemInputExt, @BeanParam ServicePropertyInputExt servicePropertyInputExt, @FormParam("uprn") String uprn,
			@FormParam("usrn") String usrn, @FormParam("serviceId") String serviceId, @FormParam("accountSiteId") String accountSiteId, @FormParam("roleId") Integer roleId,
			@FormParam("roleName") String roleName, @FormParam("adHocRoundInstanceId") String adHocRoundInstanceId, @FormParam("workLocationAddress") String workLocationAddress,
			@FormParam("workLocationEasting") String workLocationEasting, @FormParam("workLocationLatitude") String workLocationLatitude,
			@FormParam("workLocationLongitude") String workLocationLongitude, @FormParam("workLocationName") String workLocationName, @FormParam("workLocationNorthing") String workLocationNorthing,
			@FormParam("worksheetMessage") String worksheetMessage, @FormParam("worksheetReference") String worksheetReference, @FormParam("worksheetDueDate") String worksheetDueDate,
			@FormParam("worksheetStartDate") String worksheetStartDate, @Context HttpServletRequest request) throws Exception {

		CreateWorksheetResponse response = whiteSpaceRequestService.createWorksheet(getCompanyId(request), servicePropertyInputExt, serviceItemInputExt, uprn, usrn, serviceId, accountSiteId, roleId,
				roleName, adHocRoundInstanceId, workLocationAddress, workLocationEasting, workLocationLatitude, workLocationLongitude, workLocationName, workLocationNorthing, worksheetMessage,
				worksheetReference, worksheetDueDate, worksheetStartDate);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/delete-site-contact")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteSiteContact(@FormParam("contactId") String contactId, @Context HttpServletRequest request) throws Exception {

		DeleteSiteContactResponse response = whiteSpaceRequestService.deleteSiteContact(getCompanyId(request), contactId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/delete-site-service-item")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteSiteServiceItem(@FormParam("siteServiceId") String siteServiceId, @Context HttpServletRequest request) throws Exception {

		DeleteSiteServiceItemResponse response = whiteSpaceRequestService.deleteSiteServiceItem(getCompanyId(request), siteServiceId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/delete-site-service-item-round-schedule")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteSiteServiceItemRoundSchedule(@FormParam("siteServiceId") String siteServiceId, @FormParam("roundRoundAreaServiceScheduleId") String roundRoundAreaServiceScheduleId,
			@Context HttpServletRequest request) throws Exception {

		DeleteSiteServiceItemRoundScheduleResponse response = whiteSpaceRequestService.deleteSiteServiceItemRoundSchedule(getCompanyId(request), siteServiceId, roundRoundAreaServiceScheduleId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-account-site-id")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAccountSiteId(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetAccountSiteIdResponse response = whiteSpaceRequestService.getAccountSiteId(getCompanyId(request), siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-active-address")
	@Produces(MediaType.APPLICATION_JSON)
	public String getActiveAddresses(@QueryParam("addressLine1") String addressLine1, @QueryParam("addressLine2") String addressLine2, @QueryParam("addressNameNumber") String addressNameNumber,
			@QueryParam("county") String county, @QueryParam("country") String country, @QueryParam("postalCode") String postalCode, @QueryParam("town") String town,
			@Context HttpServletRequest request) throws Exception {

		GetActiveAddressesResponse response = whiteSpaceRequestService.getActiveAddresses(getCompanyId(request), addressLine1, addressLine2, addressNameNumber, county, country, postalCode, town);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-address")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAddresses(@QueryParam("addressLine1") String addressLine1, @QueryParam("addressLine2") String addressLine2, @QueryParam("addressNameNumber") String addressNameNumber,
			@QueryParam("county") String county, @QueryParam("country") String country, @QueryParam("postalCode") String postalCode, @QueryParam("town") String town,
			@Context HttpServletRequest request) throws Exception {

		GetAddressesResponse response = whiteSpaceRequestService.getAddresses(getCompanyId(request), addressLine1, addressLine2, addressNameNumber, county, country, postalCode, town);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-address-by-coordinates-radius")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAddressesByCoordinatesRadius(@QueryParam("latitude") String latitude, @QueryParam("longitude") String longitude, @QueryParam("radiusMetres") String radiusMetres,
			@Context HttpServletRequest request) throws Exception {

		GetAddressesByCoordinatesRadiusResponse response = whiteSpaceRequestService.getAddressesByCoordinatesRadius(getCompanyId(request), latitude, longitude, radiusMetres);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-collection-by-uprn-and-date")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCollectionByUprnAndDate(@QueryParam("uprn") String uprn, @QueryParam("nextCollectionFromDate") String nextCollectionFromDate, @Context HttpServletRequest request)
			throws Exception {

		GetCollectionByUprnAndDateResponse response = whiteSpaceRequestService.getCollectionByUprnAndDate(getCompanyId(request), uprn, nextCollectionFromDate);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-collection-slots")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCollectionSlots(@QueryParam("uprn") String uprn, @QueryParam("serviceId") String serviceId, @QueryParam("siteId") String siteId, @QueryParam("listCount") String listCount,
			@QueryParam("nextCollectionFromDate") String nextCollectionFromDate, @QueryParam("nextCollectionToDate") String nextCollectionToDate, @Context HttpServletRequest request)
			throws Exception {

		GetCollectionSlotsResponse response = whiteSpaceRequestService.getCollectionSlots(getCompanyId(request), uprn, serviceId, siteId, listCount, nextCollectionFromDate, nextCollectionToDate);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-full-site-collections")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFullSiteCollections(@QueryParam("uprn") String uprn, @QueryParam("siteId") String siteId, @Context HttpServletRequest request) throws Exception {

		GetFullSiteCollectionsResponse response = whiteSpaceRequestService.getFullSiteCollections(getCompanyId(request), uprn, siteId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-full-worksheet-details")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFullWorksheetDetails(@QueryParam("worksheetId") String worksheetId, @QueryParam("worksheetRef") String worksheetRef, @Context HttpServletRequest request) throws Exception {

		GetFullWorksheetDetailsResponse response = whiteSpaceRequestService.getFullWorksheetDetails(getCompanyId(request), worksheetId, worksheetRef);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-in-cab-logs")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInCabLogs(@QueryParam("uprn") String uprn, @QueryParam("usrn") String usrn, @QueryParam("logFromDate") String logFromDate, @QueryParam("logToDate") String logToDate,
			@QueryParam("logTypeID") List<String> logTypeId, @QueryParam("reason") List<String> reason, @QueryParam("worksheetType") List<String> worksheetType, @Context HttpServletRequest request)
			throws Exception {

		ArrayOfstring worksheetTypeArr = objectFatoryService.getArrayOfstring(worksheetType);
		ArrayOfstring reasonArr = objectFatoryService.getArrayOfstring(reason);
		ArrayOfstring logTypeIdArr = objectFatoryService.getArrayOfstring(logTypeId);

		GetInCabLogsResponse response = whiteSpaceRequestService.getInCabLogs(getCompanyId(request), uprn, usrn, logFromDate, logToDate, logTypeIdArr, reasonArr, worksheetTypeArr);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-logs-search")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLogsSearch(@QueryParam("accountSiteId") String accountSiteId, @QueryParam("uprn") String uprn, @QueryParam("logFromDate") String logFromDate,
			@QueryParam("logToDate") String logToDate, @QueryParam("logMessageSearchString") String logMessageSearchString, @QueryParam("logReferenceSearchString") String logReferenceSearchString,
			@QueryParam("logSubjectSearchString") String logSubjectSearchString, @QueryParam("logTypeID") List<String> logTypeId, @Context HttpServletRequest request) throws Exception {

		ArrayOfstring logTypeIDArr = objectFatoryService.getArrayOfstring(logTypeId);

		GetLogsSearchResponse response = whiteSpaceRequestService.getLogsSearch(getCompanyId(request), accountSiteId, uprn, logFromDate, logToDate, logMessageSearchString, logReferenceSearchString,
				logSubjectSearchString, logTypeIDArr);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-notifications")
	@Produces(MediaType.APPLICATION_JSON)
	public String getNotifications(@QueryParam("notificationId") String notificationId, @Context HttpServletRequest request) throws Exception {

		GetNotificationsResponse response = whiteSpaceRequestService.getNotifications(getCompanyId(request), notificationId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-service-items")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceItems(@QueryParam("serviceId") String serviceId, @QueryParam("serviceItemId") String serviceItemId, @Context HttpServletRequest request) throws Exception {

		GetServiceItemsResponse response = whiteSpaceRequestService.getServiceItems(getCompanyId(request), serviceId, serviceItemId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-services")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServices(@QueryParam("serviceId") String serviceId, @Context HttpServletRequest request) throws Exception {

		GetServicesResponse response = whiteSpaceRequestService.getServices(getCompanyId(request), serviceId);
		return jsonResponseParserService.toJson(response);
	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

	@GET
	@Path("/get-site-attachments")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteAttachments(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @QueryParam("includeData") boolean includeData, @Context HttpServletRequest request)
			throws Exception {

		GetSiteAttachmentsResponse response = whiteSpaceRequestService.getSiteAttachments(getCompanyId(request), siteId, uprn, includeData);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-available-rounds")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteAvailableRounds(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteAvailableRoundsResponse response = whiteSpaceRequestService.getSiteAvailableRounds(getCompanyId(request), siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-collection-extra-details")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteCollectionExtraDetails(@QueryParam("siteServiceId") String siteServiceId, @Context HttpServletRequest request) throws Exception {

		GetSiteCollectionExtraDetailsResponse response = whiteSpaceRequestService.getSiteCollectionExtraDetails(getCompanyId(request), siteServiceId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-collections")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteCollections(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteCollectionsResponse response = whiteSpaceRequestService.getSiteCollections(getCompanyId(request), siteId, uprn, true);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-contacts")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteContacts(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteContactsResponse response = whiteSpaceRequestService.getSiteContacts(getCompanyId(request), siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-contracts")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteContracts(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteContractsResponse response = whiteSpaceRequestService.getSiteContracts(getCompanyId(request), siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-flags")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteFlags(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteFlagsResponse response = whiteSpaceRequestService.getSiteFlags(getCompanyId(request), siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-id")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteId(@QueryParam("accountSiteId") String accountSiteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteIdResponse response = whiteSpaceRequestService.getSiteId(getCompanyId(request), accountSiteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-incidents")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteIncidents(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteIncidentsResponse response = whiteSpaceRequestService.getSiteIncidents(getCompanyId(request), siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-info")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteInfo(@QueryParam("accountSiteId") String accountSiteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteInfoResponse response = whiteSpaceRequestService.getSiteInfo(getCompanyId(request), accountSiteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-logs")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteLogs(@QueryParam("accountSiteId") String accountSiteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteLogsResponse response = whiteSpaceRequestService.getSiteLogs(getCompanyId(request), accountSiteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-notifications")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteNotifications(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSiteNotificationsResponse response = whiteSpaceRequestService.getSiteNotifications(getCompanyId(request), siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-sites")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSites(@QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetSitesResponse response = whiteSpaceRequestService.getSites(getCompanyId(request), uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-service-item-round-schedules")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteServiceItemRoundSchedules(@QueryParam("siteServiceId") String siteServiceId, @Context HttpServletRequest request) throws Exception {

		GetSiteServiceItemRoundSchedulesResponse response = whiteSpaceRequestService.getSiteServiceItemRoundSchedules(getCompanyId(request), siteServiceId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-site-worksheets")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteWorksheets(@QueryParam("uprn") String uprn, @QueryParam("accountSiteId") String accountSiteId, @QueryParam("worksheetFilterString") String worksheetFilterString,
			@QueryParam("orderAscending") boolean orderAscending, @QueryParam("worksheetFilterName") String worksheetFilterName, @QueryParam("worksheetSortOrderName") String worksheetSortOrderName,
			@Context HttpServletRequest request) throws Exception {

		GetSiteWorksheetsResponse response = whiteSpaceRequestService.getSiteWorksheets(getCompanyId(request), uprn, accountSiteId, worksheetFilterString, orderAscending, worksheetFilterName,
				worksheetSortOrderName);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-streets")
	@Produces(MediaType.APPLICATION_JSON)
	public String getStreets(@QueryParam("postcode") String postcode, @QueryParam("streetName") String streetName, @QueryParam("townName") String townName, @Context HttpServletRequest request)
			throws Exception {

		GetStreetsResponse response = whiteSpaceRequestService.getStreets(getCompanyId(request), postcode, streetName, townName);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-walk-numbers")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWalkNumbers(@QueryParam("siteId") String siteId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		GetWalkNumbersResponse response = whiteSpaceRequestService.getWalkNumbers(getCompanyId(request), siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-attachments")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetAttachments(@QueryParam("worksheetId") String worksheetId, @QueryParam("worksheetRef") String worksheetRef, @QueryParam("worksheetRef") boolean inlcudeData,
			@Context HttpServletRequest request) throws Exception {

		GetWorksheetAttachmentsResponse response = whiteSpaceRequestService.getWorksheetAttachments(getCompanyId(request), worksheetId, worksheetRef, inlcudeData);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-charge-matrix")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetChargeMatrix(@QueryParam("serviceId") String serviceId, @QueryParam("serviceItemId") String serviceItemId, @Context HttpServletRequest request) throws Exception {

		GetWorksheetChargeMatrixResponse response = whiteSpaceRequestService.getWorksheetChargeMatrix(getCompanyId(request), serviceId, serviceItemId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-detail-events")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetDetailEvents(@QueryParam("worksheetId") String worksheetId, @QueryParam("worksheetRef") String worksheetRef, @Context HttpServletRequest request) throws Exception {

		GetWorksheetDetailEventsResponse response = whiteSpaceRequestService.getWorksheetDetailEvents(getCompanyId(request), worksheetId, worksheetRef);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-detail-extra-info-fields")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetDetailExtraInfoFields(@QueryParam("worksheetId") String worksheetId, @QueryParam("worksheetRef") String worksheetRef, @Context HttpServletRequest request)
			throws Exception {

		GetWorksheetDetailExtraInfoFieldsResponse response = whiteSpaceRequestService.getWorksheetDetailExtraInfoFields(getCompanyId(request), worksheetId, worksheetRef);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-detail-notes")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetDetailNotes(@QueryParam("worksheetId") String worksheetId, @QueryParam("worksheetRef") String worksheetRef, @Context HttpServletRequest request) throws Exception {

		GetWorksheetDetailNotesResponse response = whiteSpaceRequestService.getWorksheetDetailNotes(getCompanyId(request), worksheetId, worksheetRef);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-details")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetDetails(@QueryParam("worksheetId") String worksheetId, @QueryParam("worksheetRef") String worksheetRef, @Context HttpServletRequest request) throws Exception {

		GetWorksheetDetailsResponse response = whiteSpaceRequestService.getWorksheetDetails(getCompanyId(request), worksheetId, worksheetRef);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-detail-service-items")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetDetailServiceItems(@QueryParam("worksheetId") String worksheetId, @QueryParam("worksheetRef") String worksheetRef, @Context HttpServletRequest request) throws Exception {

		GetWorksheetDetailServiceItemsResponse response = whiteSpaceRequestService.getWorksheetDetailServiceItems(getCompanyId(request), worksheetId, worksheetRef);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-extra-info-fields")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetExtraInfoFields(@QueryParam("serviceId") String serviceId, @Context HttpServletRequest request) throws Exception {

		GetWorksheetExtraInfoFieldsResponse response = whiteSpaceRequestService.getWorksheetExtraInfoFields(getCompanyId(request), serviceId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-roles")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetRoles(@QueryParam("serviceId") String serviceId, @Context HttpServletRequest request) throws Exception {

		GetWorksheetRolesResponse response = whiteSpaceRequestService.getWorksheetRoles(getCompanyId(request), serviceId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-by-reference")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetsByReference(@QueryParam("worksheetReference") String worksheetReference, @Context HttpServletRequest request) throws Exception {

		GetWorksheetsByReferenceResponse response = whiteSpaceRequestService.getWorksheetsByReference(getCompanyId(request), worksheetReference);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/get-worksheet-service-items")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorksheetServiceItems(@QueryParam("siteId") String siteId, @QueryParam("serviceId") String serviceId, @QueryParam("uprn") String uprn, @Context HttpServletRequest request)
			throws Exception {

		GetWorksheetServiceItemsResponse response = whiteSpaceRequestService.getWorksheetServiceItems(getCompanyId(request), siteId, serviceId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/progress-workflow")
	@Produces(MediaType.APPLICATION_JSON)
	public String progressWorkflow(@QueryParam("worksheetId") String worksheetId, @QueryParam("worksheetRef") String worksheetRef, @QueryParam("eventName") String eventName,
			@Context HttpServletRequest request) throws Exception {

		ProgressWorkflowResponse response = whiteSpaceRequestService.progressWorkflow(getCompanyId(request), worksheetId, worksheetRef, eventName);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/update-site-contact")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateSiteContact(@FormParam("contactEmail") String contactEmail, @FormParam("contactId") String contactId, @FormParam("contactName") String contactName,
			@FormParam("contactFaxNumber") String contactFaxNumber, @FormParam("contactMobileNumber") String contactMobileNumber, @FormParam("contactTelephoneNumber") String contactTelephoneNumber,
			@FormParam("isMainContact") boolean isMainContact, @FormParam("siteId") String siteId, @FormParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		UpdateSiteContactResponse response = whiteSpaceRequestService.updateSiteContact(getCompanyId(request), contactEmail, contactId, contactName, contactFaxNumber, contactMobileNumber,
				contactTelephoneNumber, isMainContact, siteId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/update-site-service-item")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateSiteServiceItem(@FormParam("siteServiceId") String siteServiceId, @FormParam("serviceItemQuantity") String serviceItemQuantity,
			@FormParam("serviceItemValidFrom") String serviceItemValidFrom, @FormParam("serviceItemValidTo") String serviceItemValidTo, @FormParam("chargeTypeId") String chargeTypeId,
			@BeanParam SiteCollectionExtraDetailInputExt siteCollectionExtraDetailInputExt, @Context HttpServletRequest request) throws Exception {

		UpdateSiteServiceItemResponse response = whiteSpaceRequestService.updateSiteServiceItem(getCompanyId(request), siteServiceId, serviceItemQuantity, serviceItemValidFrom, serviceItemValidTo,
				chargeTypeId, siteCollectionExtraDetailInputExt);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/update-site-service-notification")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateSiteServiceNotification(@FormParam("notificationId") String notificationId, @FormParam("siteServiceNotificationNotes") String siteServiceNotificationNotes,
			@FormParam("serviceId") String serviceId, @FormParam("siteId") String siteId, @FormParam("siteServiceNotificationId") String siteServiceNotificationId,
			@FormParam("siteServiceNotificationValidFrom") String siteServiceNotificationValidFrom, @FormParam("siteServiceNotificationValidTo") String siteServiceNotificationValidTo,
			@FormParam("uprn") String uprn, @Context HttpServletRequest request) throws Exception {

		UpdateSiteServiceNotificationResponse response = whiteSpaceRequestService.updateSiteServiceNotification(getCompanyId(request), notificationId, siteServiceNotificationNotes, serviceId, siteId,
				siteServiceNotificationId, siteServiceNotificationValidFrom, siteServiceNotificationValidTo, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/update-workflow-event-date")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateWorkflowEventDate(@FormParam("eventDate") String eventDate, @FormParam("eventName") String eventName, @FormParam("worksheetId") String worksheetId,
			@FormParam("worksheetRef") String worksheetRef, @FormParam("stateName") String stateName, @Context HttpServletRequest request) throws Exception {

		UpdateWorkflowEventDateResponse response = whiteSpaceRequestService.updateWorkflowEventDate(getCompanyId(request), eventDate, eventName, worksheetId, worksheetRef, stateName);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/update-worksheet")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateWorksheet(@FormParam("workLocationAddress") String workLocationAddress, @FormParam("workLocationAddressID") String workLocationAddressID,
			@FormParam("workLocationEasting") String workLocationEasting, @FormParam("workLocationLatitude") String workLocationLatitude,
			@FormParam("workLocationLongitude") String workLocationLongitude, @FormParam("workLocationName") String workLocationName, @FormParam("workLocationNorthing") String workLocationNorthing,
			@FormParam("workLocationText") String workLocationText, @FormParam("worksheetApprovedDate") String worksheetApprovedDate, @FormParam("worksheetAssignedToID") String worksheetAssignedToID,
			@FormParam("worksheetAssignedToTypeID") String worksheetAssignedToTypeID, @FormParam("worksheetCompletedDate") String worksheetCompletedDate,
			@FormParam("worksheetDueDate") String worksheetDueDate, @FormParam("worksheetEscallatedDate") String worksheetEscallatedDate, @FormParam("worksheetExpiryDate") String worksheetExpiryDate,
			@FormParam("worksheetMessage") String worksheetMessage, @FormParam("worksheetImportanceID") String worksheetImportanceID, @FormParam("worksheetId") String worksheetId,
			@FormParam("worksheetPaymentDate") String worksheetPaymentDate, @FormParam("worksheetReference") String worksheetReference, @FormParam("worksheetSubject") String worksheetSubject,
			@BeanParam ServicePropertyInputExt servicePropertyInputExt, @Context HttpServletRequest request) throws Exception {

		UpdateWorksheetResponse response = whiteSpaceRequestService.updateWorksheet(getCompanyId(request), workLocationAddress, workLocationAddressID, workLocationEasting, workLocationLatitude,
				workLocationLongitude, workLocationName, workLocationNorthing, workLocationText, worksheetApprovedDate, worksheetAssignedToID, worksheetAssignedToTypeID, worksheetCompletedDate,
				worksheetDueDate, worksheetEscallatedDate, worksheetExpiryDate, worksheetMessage, worksheetImportanceID, worksheetId, worksheetPaymentDate, worksheetReference, worksheetSubject,
				servicePropertyInputExt);
		return jsonResponseParserService.toJson(response);
	}

	private long getCompanyId(HttpServletRequest request) {
		return (Long) request.getAttribute("COMPANY_ID");
	}

}