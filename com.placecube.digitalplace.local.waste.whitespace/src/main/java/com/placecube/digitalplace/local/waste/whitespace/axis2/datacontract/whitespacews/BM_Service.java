/**
 * BM_Service.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews;

/** BM_Service bean class */
@SuppressWarnings({"unchecked", "unused"})
public class BM_Service implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = BM_Service
  Namespace URI = http://webservices.whitespacews.com/
  Namespace Prefix = ns1
  */

  /** field for ServiceID */
  protected int localServiceID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIDTracker = false;

  public boolean isServiceIDSpecified() {
    return localServiceIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceID() {
    return localServiceID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceID
   */
  public void setServiceID(int param) {

    // setting primitive attribute tracker to true
    localServiceIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceID = param;
  }

  /** field for ServiceName */
  protected java.lang.String localServiceName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceNameTracker = false;

  public boolean isServiceNameSpecified() {
    return localServiceNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceName() {
    return localServiceName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceName
   */
  public void setServiceName(java.lang.String param) {
    localServiceNameTracker = true;

    this.localServiceName = param;
  }

  /** field for ServiceDescription */
  protected java.lang.String localServiceDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceDescriptionTracker = false;

  public boolean isServiceDescriptionSpecified() {
    return localServiceDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceDescription() {
    return localServiceDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceDescription
   */
  public void setServiceDescription(java.lang.String param) {
    localServiceDescriptionTracker = true;

    this.localServiceDescription = param;
  }

  /** field for ServiceWorkflowID */
  protected int localServiceWorkflowID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceWorkflowIDTracker = false;

  public boolean isServiceWorkflowIDSpecified() {
    return localServiceWorkflowIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceWorkflowID() {
    return localServiceWorkflowID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceWorkflowID
   */
  public void setServiceWorkflowID(int param) {

    // setting primitive attribute tracker to true
    localServiceWorkflowIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceWorkflowID = param;
  }

  /** field for ServiceWorkflowName */
  protected java.lang.String localServiceWorkflowName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceWorkflowNameTracker = false;

  public boolean isServiceWorkflowNameSpecified() {
    return localServiceWorkflowNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceWorkflowName() {
    return localServiceWorkflowName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceWorkflowName
   */
  public void setServiceWorkflowName(java.lang.String param) {
    localServiceWorkflowNameTracker = true;

    this.localServiceWorkflowName = param;
  }

  /** field for ServiceNeedsServiceItem */
  protected boolean localServiceNeedsServiceItem;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceNeedsServiceItemTracker = false;

  public boolean isServiceNeedsServiceItemSpecified() {
    return localServiceNeedsServiceItemTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceNeedsServiceItem() {
    return localServiceNeedsServiceItem;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceNeedsServiceItem
   */
  public void setServiceNeedsServiceItem(boolean param) {

    // setting primitive attribute tracker to true
    localServiceNeedsServiceItemTracker = true;

    this.localServiceNeedsServiceItem = param;
  }

  /** field for ServiceNeedsNotes */
  protected boolean localServiceNeedsNotes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceNeedsNotesTracker = false;

  public boolean isServiceNeedsNotesSpecified() {
    return localServiceNeedsNotesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceNeedsNotes() {
    return localServiceNeedsNotes;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceNeedsNotes
   */
  public void setServiceNeedsNotes(boolean param) {

    // setting primitive attribute tracker to true
    localServiceNeedsNotesTracker = true;

    this.localServiceNeedsNotes = param;
  }

  /** field for ServiceRunFirstStep */
  protected boolean localServiceRunFirstStep;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceRunFirstStepTracker = false;

  public boolean isServiceRunFirstStepSpecified() {
    return localServiceRunFirstStepTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceRunFirstStep() {
    return localServiceRunFirstStep;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceRunFirstStep
   */
  public void setServiceRunFirstStep(boolean param) {

    // setting primitive attribute tracker to true
    localServiceRunFirstStepTracker = true;

    this.localServiceRunFirstStep = param;
  }

  /** field for ServiceRuleID */
  protected int localServiceRuleID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceRuleIDTracker = false;

  public boolean isServiceRuleIDSpecified() {
    return localServiceRuleIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceRuleID() {
    return localServiceRuleID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceRuleID
   */
  public void setServiceRuleID(int param) {

    // setting primitive attribute tracker to true
    localServiceRuleIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceRuleID = param;
  }

  /** field for ServiceRuleName */
  protected java.lang.String localServiceRuleName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceRuleNameTracker = false;

  public boolean isServiceRuleNameSpecified() {
    return localServiceRuleNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceRuleName() {
    return localServiceRuleName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceRuleName
   */
  public void setServiceRuleName(java.lang.String param) {
    localServiceRuleNameTracker = true;

    this.localServiceRuleName = param;
  }

  /** field for ServiceOrder */
  protected int localServiceOrder;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceOrderTracker = false;

  public boolean isServiceOrderSpecified() {
    return localServiceOrderTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceOrder() {
    return localServiceOrder;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceOrder
   */
  public void setServiceOrder(int param) {

    // setting primitive attribute tracker to true
    localServiceOrderTracker = param != java.lang.Integer.MIN_VALUE;

    this.localServiceOrder = param;
  }

  /** field for ServiceIsCollection */
  protected boolean localServiceIsCollection;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIsCollectionTracker = false;

  public boolean isServiceIsCollectionSpecified() {
    return localServiceIsCollectionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceIsCollection() {
    return localServiceIsCollection;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceIsCollection
   */
  public void setServiceIsCollection(boolean param) {

    // setting primitive attribute tracker to true
    localServiceIsCollectionTracker = true;

    this.localServiceIsCollection = param;
  }

  /** field for ServiceIsWorksheet */
  protected boolean localServiceIsWorksheet;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIsWorksheetTracker = false;

  public boolean isServiceIsWorksheetSpecified() {
    return localServiceIsWorksheetTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceIsWorksheet() {
    return localServiceIsWorksheet;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceIsWorksheet
   */
  public void setServiceIsWorksheet(boolean param) {

    // setting primitive attribute tracker to true
    localServiceIsWorksheetTracker = true;

    this.localServiceIsWorksheet = param;
  }

  /** field for ServiceIsAdditional */
  protected boolean localServiceIsAdditional;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIsAdditionalTracker = false;

  public boolean isServiceIsAdditionalSpecified() {
    return localServiceIsAdditionalTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceIsAdditional() {
    return localServiceIsAdditional;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceIsAdditional
   */
  public void setServiceIsAdditional(boolean param) {

    // setting primitive attribute tracker to true
    localServiceIsAdditionalTracker = true;

    this.localServiceIsAdditional = param;
  }

  /** field for ServiceIsExtra */
  protected boolean localServiceIsExtra;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIsExtraTracker = false;

  public boolean isServiceIsExtraSpecified() {
    return localServiceIsExtraTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceIsExtra() {
    return localServiceIsExtra;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceIsExtra
   */
  public void setServiceIsExtra(boolean param) {

    // setting primitive attribute tracker to true
    localServiceIsExtraTracker = true;

    this.localServiceIsExtra = param;
  }

  /** field for ServiceIsChargeable */
  protected boolean localServiceIsChargeable;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIsChargeableTracker = false;

  public boolean isServiceIsChargeableSpecified() {
    return localServiceIsChargeableTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceIsChargeable() {
    return localServiceIsChargeable;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceIsChargeable
   */
  public void setServiceIsChargeable(boolean param) {

    // setting primitive attribute tracker to true
    localServiceIsChargeableTracker = true;

    this.localServiceIsChargeable = param;
  }

  /** field for ServiceIsAdHoc */
  protected boolean localServiceIsAdHoc;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIsAdHocTracker = false;

  public boolean isServiceIsAdHocSpecified() {
    return localServiceIsAdHocTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceIsAdHoc() {
    return localServiceIsAdHoc;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceIsAdHoc
   */
  public void setServiceIsAdHoc(boolean param) {

    // setting primitive attribute tracker to true
    localServiceIsAdHocTracker = true;

    this.localServiceIsAdHoc = param;
  }

  /** field for ServiceIsBulky */
  protected boolean localServiceIsBulky;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIsBulkyTracker = false;

  public boolean isServiceIsBulkySpecified() {
    return localServiceIsBulkyTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceIsBulky() {
    return localServiceIsBulky;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceIsBulky
   */
  public void setServiceIsBulky(boolean param) {

    // setting primitive attribute tracker to true
    localServiceIsBulkyTracker = true;

    this.localServiceIsBulky = param;
  }

  /** field for AuthorityID */
  protected int localAuthorityID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAuthorityIDTracker = false;

  public boolean isAuthorityIDSpecified() {
    return localAuthorityIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAuthorityID() {
    return localAuthorityID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AuthorityID
   */
  public void setAuthorityID(int param) {

    // setting primitive attribute tracker to true
    localAuthorityIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localAuthorityID = param;
  }

  /** field for AuthorityName */
  protected java.lang.String localAuthorityName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAuthorityNameTracker = false;

  public boolean isAuthorityNameSpecified() {
    return localAuthorityNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAuthorityName() {
    return localAuthorityName;
  }

  /**
   * Auto generated setter method
   *
   * @param param AuthorityName
   */
  public void setAuthorityName(java.lang.String param) {
    localAuthorityNameTracker = true;

    this.localAuthorityName = param;
  }

  /** field for WorkflowID */
  protected int localWorkflowID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowIDTracker = false;

  public boolean isWorkflowIDSpecified() {
    return localWorkflowIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorkflowID() {
    return localWorkflowID;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowID
   */
  public void setWorkflowID(int param) {

    // setting primitive attribute tracker to true
    localWorkflowIDTracker = param != java.lang.Integer.MIN_VALUE;

    this.localWorkflowID = param;
  }

  /** field for WorkflowName */
  protected java.lang.String localWorkflowName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkflowNameTracker = false;

  public boolean isWorkflowNameSpecified() {
    return localWorkflowNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkflowName() {
    return localWorkflowName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkflowName
   */
  public void setWorkflowName(java.lang.String param) {
    localWorkflowNameTracker = true;

    this.localWorkflowName = param;
  }

  /** field for ServiceIsMOP */
  protected boolean localServiceIsMOP;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceIsMOPTracker = false;

  public boolean isServiceIsMOPSpecified() {
    return localServiceIsMOPTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getServiceIsMOP() {
    return localServiceIsMOP;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceIsMOP
   */
  public void setServiceIsMOP(boolean param) {

    // setting primitive attribute tracker to true
    localServiceIsMOPTracker = true;

    this.localServiceIsMOP = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://webservices.whitespacews.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":BM_Service",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "BM_Service", xmlWriter);
      }
    }
    if (localServiceIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceID", xmlWriter);

      if (localServiceID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceName", xmlWriter);

      if (localServiceName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceDescriptionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceDescription", xmlWriter);

      if (localServiceDescription == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceWorkflowIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceWorkflowID", xmlWriter);

      if (localServiceWorkflowID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceWorkflowID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceWorkflowID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceWorkflowNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceWorkflowName", xmlWriter);

      if (localServiceWorkflowName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceWorkflowName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceNeedsServiceItemTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceNeedsServiceItem", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceNeedsServiceItem cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceNeedsServiceItem));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceNeedsNotesTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceNeedsNotes", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceNeedsNotes cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceNeedsNotes));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceRunFirstStepTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceRunFirstStep", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceRunFirstStep cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceRunFirstStep));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceRuleIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceRuleID", xmlWriter);

      if (localServiceRuleID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceRuleID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceRuleID));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceRuleNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceRuleName", xmlWriter);

      if (localServiceRuleName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localServiceRuleName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceOrderTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceOrder", xmlWriter);

      if (localServiceOrder == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("ServiceOrder cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceOrder));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIsCollectionTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceIsCollection", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceIsCollection cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceIsCollection));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIsWorksheetTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceIsWorksheet", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceIsWorksheet cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceIsWorksheet));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIsAdditionalTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceIsAdditional", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceIsAdditional cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceIsAdditional));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIsExtraTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceIsExtra", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceIsExtra cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceIsExtra));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIsChargeableTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceIsChargeable", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceIsChargeable cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
                localServiceIsChargeable));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIsAdHocTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceIsAdHoc", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceIsAdHoc cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceIsAdHoc));
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIsBulkyTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceIsBulky", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceIsBulky cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceIsBulky));
      }

      xmlWriter.writeEndElement();
    }
    if (localAuthorityIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AuthorityID", xmlWriter);

      if (localAuthorityID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("AuthorityID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAuthorityID));
      }

      xmlWriter.writeEndElement();
    }
    if (localAuthorityNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "AuthorityName", xmlWriter);

      if (localAuthorityName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localAuthorityName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowIDTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowID", xmlWriter);

      if (localWorkflowID == java.lang.Integer.MIN_VALUE) {

        throw new org.apache.axis2.databinding.ADBException("WorkflowID cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorkflowID));
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkflowNameTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "WorkflowName", xmlWriter);

      if (localWorkflowName == null) {
        // write the nil attribute

        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

      } else {

        xmlWriter.writeCharacters(localWorkflowName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceIsMOPTracker) {
      namespace = "http://webservices.whitespacews.com/";
      writeStartElement(null, namespace, "ServiceIsMOP", xmlWriter);

      if (false) {

        throw new org.apache.axis2.databinding.ADBException("ServiceIsMOP cannot be null!!");

      } else {
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceIsMOP));
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://webservices.whitespacews.com/")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static BM_Service parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      BM_Service object = new BM_Service();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"BM_Service".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (BM_Service)
                  com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.whitespacews
                      .ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceDescription(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceWorkflowID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceWorkflowID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceWorkflowID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceWorkflowID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceWorkflowName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceWorkflowName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceNeedsServiceItem")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceNeedsServiceItem" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceNeedsServiceItem(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceNeedsNotes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceNeedsNotes" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceNeedsNotes(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceRunFirstStep")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceRunFirstStep" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceRunFirstStep(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceRuleID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceRuleID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceRuleID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceRuleID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceRuleName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceRuleName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceOrder")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceOrder" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceOrder(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setServiceOrder(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceIsCollection")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceIsCollection" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceIsCollection(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceIsWorksheet")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceIsWorksheet" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceIsWorksheet(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceIsAdditional")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceIsAdditional" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceIsAdditional(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceIsExtra")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceIsExtra" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceIsExtra(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceIsChargeable")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceIsChargeable" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceIsChargeable(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceIsAdHoc")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceIsAdHoc" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceIsAdHoc(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "ServiceIsBulky")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceIsBulky" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceIsBulky(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "AuthorityID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AuthorityID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAuthorityID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setAuthorityID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://webservices.whitespacews.com/", "AuthorityName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setAuthorityName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorkflowID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkflowID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkflowID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {

          object.setWorkflowID(java.lang.Integer.MIN_VALUE);
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "WorkflowName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setWorkflowName(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://webservices.whitespacews.com/", "ServiceIsMOP")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceIsMOP" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceIsMOP(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
