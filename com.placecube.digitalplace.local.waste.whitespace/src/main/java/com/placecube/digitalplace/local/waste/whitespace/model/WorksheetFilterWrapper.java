package com.placecube.digitalplace.local.waste.whitespace.model;

import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.enums.WorksheetFilter;

public class WorksheetFilterWrapper extends WorksheetFilter {

	private static final long serialVersionUID = 1L;

	public WorksheetFilterWrapper(String value) {
		super(value, true);
	}

}
