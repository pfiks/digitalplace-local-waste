package com.placecube.digitalplace.local.waste.whitespace.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;

import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.ArrayOfInputCreateWorksheetInputServiceItemInput;
import com.placecube.digitalplace.local.waste.whitespace.axis2.datacontract.inputs.InputCreateWorksheetInputServiceItemInput;
import com.placecube.digitalplace.local.waste.whitespace.exception.WhitespaceParameterException;

public class ServiceItemInputExt {

	private List<String> serviceItemNames = new ArrayList<>();

	private List<String> serviceItemQuantities = new ArrayList<>();

	private List<String> serviceItemIds = new ArrayList<>();

	public ArrayOfInputCreateWorksheetInputServiceItemInput getServiceItemInputs() throws WhitespaceParameterException {
		if ((serviceItemIds.size() != serviceItemNames.size()) || (serviceItemIds.size() != serviceItemQuantities.size())) {
			throw new WhitespaceParameterException("The ServiceItemInput parameter counts don't match.");
		}

		ArrayOfInputCreateWorksheetInputServiceItemInput arrayOfInputCreateWorksheetInputServiceItemInput = new ArrayOfInputCreateWorksheetInputServiceItemInput();

		for (int i = 0; i < serviceItemIds.size(); i++) {
			InputCreateWorksheetInputServiceItemInput inputCreateWorksheetInputServiceItemInput = new InputCreateWorksheetInputServiceItemInput();

			inputCreateWorksheetInputServiceItemInput.setServiceItemName(serviceItemNames.get(i));
			inputCreateWorksheetInputServiceItemInput.setServiceItemQuantity(new BigDecimal(GetterUtil.get(serviceItemQuantities.get(i), 0)));
			inputCreateWorksheetInputServiceItemInput.setServiceItemId(GetterUtil.get(serviceItemIds.get(i), 0));
			arrayOfInputCreateWorksheetInputServiceItemInput.addInputCreateWorksheetInputServiceItemInput(inputCreateWorksheetInputServiceItemInput);
		}

		return arrayOfInputCreateWorksheetInputServiceItemInput;
	}

	@FormParam("serviceItemId")
	public void setServiceItemIds(List<String> serviceItemIds) {
		this.serviceItemIds = serviceItemIds;
	}

	@FormParam("serviceItemName")
	public void setServiceItemNames(List<String> serviceItemNames) {
		this.serviceItemNames = serviceItemNames;
	}

	@FormParam("serviceItemQuantity")
	public void setServiceItemQuantities(List<String> serviceItemQuantities) {
		this.serviceItemQuantities = serviceItemQuantities;
	}

}
