package com.placecube.digitalplace.local.waste.webaspx.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.configuration.WebaspxConfiguration;
import com.placecube.digitalplace.local.waste.webaspx.constants.WebaspxWasteConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@SuppressStaticInitializationFor({ "com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNResponseDocument",
		"com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument" })
public class WebaspxWasteConnectorTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String UPRN = "12345678";

	@Mock
	private WebaspxConfiguration webaspxConfiguration;

	@Mock
	private WebaspxRequestService mockWebaspxRequestService;

	@InjectMocks
	private WebaspxWasteConnector webaspxWasteConnector;

	@Mock
	private WebaspxWasteService mockWebaspxWasteService;

	@Mock
	private JsonResponseParserService mockJsonResponseParserService;

	@Mock
	private BinCollection mockBinCollection;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObjectOne;

	@Mock
	private JSONObject mockJSONObjectTwo;

	@Mock
	private JSONObject mockJSONObjectThree;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private GetRoundForUPRNResponseDocument mockGetRoundForUPRNResponseDocument;

	@Mock
	private GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockWebaspxWasteService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());
		boolean result = webaspxWasteConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {

		when(mockWebaspxWasteService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = webaspxWasteConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenGetRoundForUPRNJsonContainsErrorString_ThenReturnsEmptySet() throws Exception {
		String uprn = "12345678";
		String getRoundJson = "{getRoundJson}";
		String getRoundAndBinJson = "{getRoundAndbinJson}";

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT, Locale.ENGLISH);
		String startDate = formatter.format(LocalDateTime.now());
		String endDate = formatter.format(LocalDateTime.now().plusMonths(2));

		when(mockWebaspxRequestService.getRoundForUPRN(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, startDate)).thenReturn(mockGetRoundForUPRNResponseDocument);
		when(mockWebaspxRequestService.getRoundAndBinInfoForUPRNForNewAdjustedDates(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, null, startDate, endDate))
				.thenReturn(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument);
		when(mockJsonResponseParserService.toJson(mockGetRoundForUPRNResponseDocument)).thenReturn(getRoundJson);
		when(mockJsonResponseParserService.toJson(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument)).thenReturn(getRoundAndBinJson);

		when(mockJSONFactory.createJSONObject(getRoundJson)).thenReturn(mockJSONObjectOne);
		when(mockJSONFactory.createJSONObject(getRoundAndBinJson)).thenReturn(mockJSONObjectTwo);

		when(mockJSONObjectOne.toString()).thenReturn("{\"Status\":" + WebaspxWasteConstants.ERROR_STRING + "}");

		Set<BinCollection> result = webaspxWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, uprn);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenGetRoundAndBinForUPRNJsonContainsErrorString_ThenReturnsEmptySet() throws Exception {
		String uprn = "12345678";
		String getRoundJson = "{getRoundJson}";
		String getRoundAndBinJson = "{getRoundAndbinJson}";

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT, Locale.ENGLISH);
		String startDate = formatter.format(LocalDateTime.now());
		String endDate = formatter.format(LocalDateTime.now().plusMonths(2));

		when(mockWebaspxRequestService.getRoundForUPRN(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, startDate)).thenReturn(mockGetRoundForUPRNResponseDocument);
		when(mockWebaspxRequestService.getRoundAndBinInfoForUPRNForNewAdjustedDates(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, null, startDate, endDate))
				.thenReturn(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument);
		when(mockJsonResponseParserService.toJson(mockGetRoundForUPRNResponseDocument)).thenReturn(getRoundJson);
		when(mockJsonResponseParserService.toJson(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument)).thenReturn(getRoundAndBinJson);

		when(mockJSONFactory.createJSONObject(getRoundJson)).thenReturn(mockJSONObjectOne);
		when(mockJSONFactory.createJSONObject(getRoundAndBinJson)).thenReturn(mockJSONObjectTwo);

		when(mockJSONObjectOne.toString()).thenReturn("no error");
		when(mockJSONObjectTwo.toString()).thenReturn("{\"Status\":" + WebaspxWasteConstants.ERROR_STRING + "}");

		Set<BinCollection> result = webaspxWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, uprn);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenGetRoundForUPRNJsonDoesNotContainRoundData_ThenReturnsEmptySet() throws Exception {
		String uprn = "12345678";
		String getRoundJson = "{getRoundJson}";
		String getRoundAndBinJson = "{getRoundAndbinJson}";

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT, Locale.ENGLISH);
		String startDate = formatter.format(LocalDateTime.now());
		String endDate = formatter.format(LocalDateTime.now().plusMonths(2));

		when(mockWebaspxRequestService.getRoundForUPRN(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, startDate)).thenReturn(mockGetRoundForUPRNResponseDocument);
		when(mockWebaspxRequestService.getRoundAndBinInfoForUPRNForNewAdjustedDates(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, null, startDate, endDate))
				.thenReturn(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument);
		when(mockJsonResponseParserService.toJson(mockGetRoundForUPRNResponseDocument)).thenReturn(getRoundJson);
		when(mockJsonResponseParserService.toJson(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument)).thenReturn(getRoundAndBinJson);

		when(mockJSONFactory.createJSONObject(getRoundJson)).thenReturn(mockJSONObjectOne);
		when(mockJSONFactory.createJSONObject(getRoundAndBinJson)).thenReturn(mockJSONObjectTwo);

		when(mockJSONObjectOne.toString()).thenReturn("no error");
		when(mockJSONObjectTwo.toString()).thenReturn("no error");

		when(mockJsonResponseParserService.getRoundForUPRNRowJSONObject(mockJSONObjectOne)).thenReturn(Optional.empty());

		Set<BinCollection> result = webaspxWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, uprn);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenGetRoundAndBinForUPRNJsonDoesNotContainBinData_ThenReturnsEmptySet() throws Exception {
		String uprn = "12345678";
		String getRoundJson = "{getRoundJson}";
		String getRoundAndBinJson = "{getRoundAndbinJson}";

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT, Locale.ENGLISH);
		String startDate = formatter.format(LocalDateTime.now());
		String endDate = formatter.format(LocalDateTime.now().plusMonths(2));

		when(mockWebaspxRequestService.getRoundForUPRN(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, startDate)).thenReturn(mockGetRoundForUPRNResponseDocument);
		when(mockWebaspxRequestService.getRoundAndBinInfoForUPRNForNewAdjustedDates(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, null, startDate, endDate))
				.thenReturn(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument);
		when(mockJsonResponseParserService.toJson(mockGetRoundForUPRNResponseDocument)).thenReturn(getRoundJson);
		when(mockJsonResponseParserService.toJson(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument)).thenReturn(getRoundAndBinJson);

		when(mockJSONFactory.createJSONObject(getRoundJson)).thenReturn(mockJSONObjectOne);
		when(mockJSONFactory.createJSONObject(getRoundAndBinJson)).thenReturn(mockJSONObjectTwo);

		when(mockJSONObjectOne.toString()).thenReturn("no error");
		when(mockJSONObjectTwo.toString()).thenReturn("no error");

		when(mockJsonResponseParserService.getRoundForUPRNRowJSONObject(mockJSONObjectOne)).thenReturn(Optional.of(mockJSONObjectThree));
		when(mockJsonResponseParserService.getRoundAndBinForUPRNJSONObject(mockJSONObjectTwo)).thenReturn(Optional.empty());

		Set<BinCollection> result = webaspxWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, uprn);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenGetRoundAndBinForUPRNJsonContainsRoundAndBinData_ThenReturnsBinCollection() throws Exception {
		String uprn = "12345678";
		String binCollectionKey = "key";
		String binCollectionName = "name";
		String getRoundJson = "{getRoundJson}";
		String getRoundAndBinJson = "{getRoundAndbinJson}";

		Iterator<String> keysSet = new HashSet<>(Arrays.asList(binCollectionKey)).iterator();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT, Locale.ENGLISH);
		String startDate = formatter.format(LocalDateTime.now());
		String endDate = formatter.format(LocalDateTime.now().plusMonths(2));

		when(mockWebaspxRequestService.getRoundForUPRN(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, startDate)).thenReturn(mockGetRoundForUPRNResponseDocument);
		when(mockWebaspxRequestService.getRoundAndBinInfoForUPRNForNewAdjustedDates(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, null, startDate, endDate))
				.thenReturn(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument);
		when(mockJsonResponseParserService.toJson(mockGetRoundForUPRNResponseDocument)).thenReturn(getRoundJson);
		when(mockJsonResponseParserService.toJson(mockGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument)).thenReturn(getRoundAndBinJson);

		when(mockJSONFactory.createJSONObject(getRoundJson)).thenReturn(mockJSONObjectOne);
		when(mockJSONFactory.createJSONObject(getRoundAndBinJson)).thenReturn(mockJSONObjectTwo);

		when(mockJSONObjectOne.toString()).thenReturn("no error");
		when(mockJSONObjectTwo.toString()).thenReturn("no error");

		when(mockJsonResponseParserService.getRoundForUPRNRowJSONObject(mockJSONObjectOne)).thenReturn(Optional.of(mockJSONObjectThree));
		when(mockJsonResponseParserService.getRoundAndBinForUPRNJSONObject(mockJSONObjectTwo)).thenReturn(Optional.of(mockJSONArray));

		when(mockJSONObjectThree.keys()).thenReturn(keysSet);
		when(mockJSONObjectThree.get(binCollectionKey)).thenReturn(binCollectionName);
		when(mockJsonResponseParserService.getBinCollectionFromJSONRoundAndBin(binCollectionName, mockJSONArray)).thenReturn(Optional.of(mockBinCollection));

		Set<BinCollection> result = webaspxWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, uprn);

		assertThat(result.size(), equalTo(1));
		assertThat(result.toArray()[0], sameInstance(mockBinCollection));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRN_WhenCollectionRoundsCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws Exception {
		String uprn = "12345678";

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT, Locale.ENGLISH);
		String startDate = formatter.format(LocalDateTime.now());

		when(mockWebaspxRequestService.getRoundForUPRN(COMPANY_ID, WebaspxWasteConstants.COUNCIL, uprn, startDate)).thenThrow(new ConfigurationException());

		webaspxWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, uprn);
	}
}
