package com.placecube.digitalplace.local.waste.webaspx.application;

import static org.junit.Assert.assertEquals;

import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetUPRNForDBFieldResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.service.JsonResponseParserService;
import com.placecube.digitalplace.local.waste.webaspx.service.WebaspxRequestService;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WebaspxRESTApplicationTest extends PowerMockito {

	private static final long COMPANY_ID = 89764l;

	private static final String PARAM1 = "PARAM1";

	private static final String PARAM10 = "PARAM10";
	private static final String PARAM11 = "PARAM11";
	private static final String PARAM12 = "PARAM12";
	private static final boolean PARAM13 = false;
	private static final String PARAM2 = "PARAM2";
	private static final String PARAM3 = "PARAM3";
	private static final String PARAM4 = "PARAM4";
	private static final String PARAM5 = "PARAM5";
	private static final String PARAM6 = "PARAM6";
	private static final String PARAM7 = "PARAM7";
	private static final String PARAM8 = "PARAM8";
	private static final String PARAM9 = "PARAM9";

	private static final String RESPONSE = "[{\"answer\":true}]";

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JsonResponseParserService mockJsonResponseParserService;

	@Mock
	private WebaspxRequestService mockWebaspxRequestService;

	@InjectMocks
	private WebaspxRESTApplication webaspxRESTApplication = new WebaspxRESTApplication();

	@Before
	public void activate() {
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);

	}

	@Test
	public void getAAHelloWorldStringTest_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		AAHelloWorldStringTestResponseDocument mockResponse = mock(AAHelloWorldStringTestResponseDocument.class);
		when(mockWebaspxRequestService.getAAHelloWorldStringTest(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAAHelloWorldStringTest(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAAHelloWorldXMLTest_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		AAHelloWorldXMLTestResponseDocument mockResponse = mock(AAHelloWorldXMLTestResponseDocument.class);
		when(mockWebaspxRequestService.getAAHelloWorldXMLTest(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAAHelloWorldXMLTest(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getBinDetailsForService_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetBinDetailsForServiceResponseDocument mockResponse = mock(GetBinDetailsForServiceResponseDocument.class);
		when(mockWebaspxRequestService.getBinDetailsForService(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getBinDetailsForService(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getRoundAndBinInfoForUPRNForNewAdjustedDates_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument mockResponse = mock(GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.class);
		when(mockWebaspxRequestService.getRoundAndBinInfoForUPRNForNewAdjustedDates(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getRoundAndBinInfoForUPRNForNewAdjustedDates(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getNoCollectionDates_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetNoCollectionDatesResponseDocument mockResponse = mock(GetNoCollectionDatesResponseDocument.class);
		when(mockWebaspxRequestService.getNoCollectionDates(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getNoCollectionDates(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAddressOrUPRN_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetAddressOrUPRNResponseDocument mockResponse = mock(GetAddressOrUPRNResponseDocument.class);
		when(mockWebaspxRequestService.getAddressOrUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAddressOrUPRN(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getIssuesForUPRN_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetIssuesForUPRNResponseDocument mockResponse = mock(GetIssuesForUPRNResponseDocument.class);
		when(mockWebaspxRequestService.getIssuesForUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getIssuesForUPRN(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAllIssuesForIssueType_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetAllIssuesForIssueTypeResponseDocument mockResponse = mock(GetAllIssuesForIssueTypeResponseDocument.class);
		when(mockWebaspxRequestService.getAllIssuesForIssueType(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAllIssuesForIssueType(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getIssues_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetIssuesResponseDocument mockResponse = mock(GetIssuesResponseDocument.class);
		when(mockWebaspxRequestService.getIssues(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getIssues(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getLastTruckPosition_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetLastTruckPositionResponseDocument mockResponse = mock(GetLastTruckPositionResponseDocument.class);
		when(mockWebaspxRequestService.getLastTruckPosition(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getLastTruckPosition(PARAM1, PARAM2, PARAM3, PARAM4, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getTrucksOutToday_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetTrucksOutTodayResponseDocument mockResponse = mock(GetTrucksOutTodayResponseDocument.class);
		when(mockWebaspxRequestService.getTrucksOutToday(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getTrucksOutToday(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void countBinsForService_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		CountBinsForServiceResponseDocument mockResponse = mock(CountBinsForServiceResponseDocument.class);
		when(mockWebaspxRequestService.countBinsForService(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.countBinsForService(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getLatestTruckPositions_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetLatestTruckPositionsResponseDocument mockResponse = mock(GetLatestTruckPositionsResponseDocument.class);
		when(mockWebaspxRequestService.getLatestTruckPositions(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getLatestTruckPositions(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getRoundCalendarForUPRN_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetRoundCalendarForUPRNResponseDocument mockResponse = mock(GetRoundCalendarForUPRNResponseDocument.class);
		when(mockWebaspxRequestService.getRoundCalendarForUPRN(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getRoundCalendarForUPRN(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getRoundForUPRN_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetRoundForUPRNResponseDocument mockResponse = mock(GetRoundForUPRNResponseDocument.class);
		when(mockWebaspxRequestService.getRoundForUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getRoundForUPRN(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void readWriteMissedBin_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		ReadWriteMissedBinResponseDocument mockResponse = mock(ReadWriteMissedBinResponseDocument.class);
		when(mockWebaspxRequestService.readWriteMissedBin(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.readWriteMissedBin(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getLastCollected_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		LastCollectedResponseDocument mockResponse = mock(LastCollectedResponseDocument.class);
		when(mockWebaspxRequestService.getLastCollected(COMPANY_ID, PARAM1, PARAM2, PARAM13)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getLastCollected(PARAM1, PARAM2, PARAM13, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getRoundNameForUPRNService_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetRoundNameForUPRNServiceResponseDocument mockResponse = mock(GetRoundNameForUPRNServiceResponseDocument.class);
		when(mockWebaspxRequestService.getRoundNameForUPRNService(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getRoundNameForUPRNService(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void insertBinDetails_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		BinInsertResponseDocument mockResponse = mock(BinInsertResponseDocument.class);
		when(mockWebaspxRequestService.insertBinDetails(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.insertBinDetails(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void deleteBin_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		BinDeleteResponseDocument mockResponse = mock(BinDeleteResponseDocument.class);
		when(mockWebaspxRequestService.deleteBin(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.deleteBin(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void updateBin_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		BinUpdateResponseDocument mockResponse = mock(BinUpdateResponseDocument.class);
		when(mockWebaspxRequestService.updateBin(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.updateBin(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getCachedCalendar_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		CachedCalendarResponseDocument mockResponse = mock(CachedCalendarResponseDocument.class);
		when(mockWebaspxRequestService.getCachedCalendar(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getCachedCalendar(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getCheckAssisted_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		CheckAssistedResponseDocument mockResponse = mock(CheckAssistedResponseDocument.class);
		when(mockWebaspxRequestService.getCheckAssisted(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getCheckAssisted(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void updateAssisted_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		UpdateAssistedResponseDocument mockResponse = mock(UpdateAssistedResponseDocument.class);
		when(mockWebaspxRequestService.updateAssisted(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.updateAssisted(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void addOrUpdateGardenSubscription_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GardenSubscriptionResponseDocument mockResponse = mock(GardenSubscriptionResponseDocument.class);
		when(mockWebaspxRequestService.addOrUpdateGardenSubscription(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.addOrUpdateGardenSubscription(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void removeGardenSubscription_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GardenRemoveSubscriptionResponseDocument mockResponse = mock(GardenRemoveSubscriptionResponseDocument.class);
		when(mockWebaspxRequestService.removeGardenSubscription(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.removeGardenSubscription(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void changeGardenAddress_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GardenChangeAddressResponseDocument mockResponse = mock(GardenChangeAddressResponseDocument.class);
		when(mockWebaspxRequestService.changeGardenAddress(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.changeGardenAddress(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void gardenSubscribersDeliveriesCollections_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GardenSubscribersDeliveriesCollectionsResponseDocument mockResponse = mock(GardenSubscribersDeliveriesCollectionsResponseDocument.class);
		when(mockWebaspxRequestService.getGardenSubscribersDeliveriesCollections(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getGardenSubscribersDeliveriesCollections(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getPotentialCollectionDatesForNewGardenSubscriber_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument mockResponse = mock(GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.class);
		when(mockWebaspxRequestService.getPotentialCollectionDatesForNewGardenSubscriber(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getPotentialCollectionDatesForNewGardenSubscriber(PARAM1, PARAM2, PARAM3, PARAM4, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAllRoundDetails_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetAllRoundDetailsResponseDocument mockResponse = mock(GetAllRoundDetailsResponseDocument.class);
		when(mockWebaspxRequestService.getAllRoundDetails(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAllRoundDetails(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAllUPRNsForDate_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetAllUPRNsForDateResponseDocument mockResponse = mock(GetAllUPRNsForDateResponseDocument.class);
		when(mockWebaspxRequestService.getAllUPRNsForDate(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAllUPRNsForDate(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAvailableContainers_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetAvailableContainersResponseDocument mockResponse = mock(GetAvailableContainersResponseDocument.class);
		when(mockWebaspxRequestService.getAvailableContainers(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAvailableContainers(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAvailableContainersForClientOrProperty_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetAvailableContainersForClientOrPropertyResponseDocument mockResponse = mock(GetAvailableContainersForClientOrPropertyResponseDocument.class);
		when(mockWebaspxRequestService.getAvailableContainersForClientOrProperty(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAvailableContainersForClientOrProperty(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAvailableRounds_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetAvailableRoundsResponseDocument mockResponse = mock(GetAvailableRoundsResponseDocument.class);
		when(mockWebaspxRequestService.getAvailableRounds(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAvailableRounds(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getInCabIssueTextAndIssueCode_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetInCabIssueTextAndIssueCodeResponseDocument mockResponse = mock(GetInCabIssueTextAndIssueCodeResponseDocument.class);
		when(mockWebaspxRequestService.getInCabIssueTextAndIssueCode(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getInCabIssueTextAndIssueCode(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getLLPGXtraUpdateAvailableFieldDetails_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument mockResponse = mock(LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.class);
		when(mockWebaspxRequestService.getLLPGXtraUpdateAvailableFieldDetails(COMPANY_ID, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getLLPGXtraUpdateAvailableFieldDetails(PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getLLPGXtraUPRNCurrentValuesForDBField_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument mockResponse = mock(LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.class);
		when(mockWebaspxRequestService.getLLPGXtraUPRNCurrentValuesForDBField(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getLLPGXtraUPRNCurrentValuesForDBField(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getLLPGXtraUpdateUPRNForDBField_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		LLPGXtraUpdateGetUPRNForDBFieldResponseDocument mockResponse = mock(LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.class);
		when(mockWebaspxRequestService.getLLPGXtraUpdateUPRNForDBField(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getLLPGXtraUpdateUPRNForDBField(PARAM1, PARAM2, PARAM3, PARAM4, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getLargeHouseholds_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		LargeHouseholdsResponseDocument mockResponse = mock(LargeHouseholdsResponseDocument.class);
		when(mockWebaspxRequestService.getLargeHouseholds(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getLargeHouseholds(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getQueryBinEndDates_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		QueryBinEndDatesResponseDocument mockResponse = mock(QueryBinEndDatesResponseDocument.class);
		when(mockWebaspxRequestService.getQueryBinEndDates(COMPANY_ID, PARAM1, PARAM2, PARAM3)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getQueryBinEndDates(PARAM1, PARAM2, PARAM3, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getQueryBinOnType_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		QueryBinOnTypeResponseDocument mockResponse = mock(QueryBinOnTypeResponseDocument.class);
		when(mockWebaspxRequestService.getQueryBinOnType(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getQueryBinOnType(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void countSubscriptions_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		CountSubscriptionsResponseDocument mockResponse = mock(CountSubscriptionsResponseDocument.class);
		when(mockWebaspxRequestService.countSubscriptions(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.countSubscriptions(PARAM1, PARAM2, PARAM3, PARAM4, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getAddressFromPostcode_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetAddressFromPostcodeResponseDocument mockResponse = mock(GetAddressFromPostcodeResponseDocument.class);
		when(mockWebaspxRequestService.getAddressFromPostcode(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getAddressFromPostcode(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getIssuesAndCollectionStatusForUPRN_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetIssuesAndCollectionStatusForUPRNResponseDocument mockResponse = mock(GetIssuesAndCollectionStatusForUPRNResponseDocument.class);
		when(mockWebaspxRequestService.getIssuesAndCollectionStatusForUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getIssuesAndCollectionStatusForUPRN(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void readWriteMissedBinByContainerType_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		ReadWriteMissedBinByContainerTypeResponseDocument mockResponse = mock(ReadWriteMissedBinByContainerTypeResponseDocument.class);
		when(mockWebaspxRequestService.readWriteMissedBinByContainerType(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.readWriteMissedBinByContainerType(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getExistingCommentsForIncab_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		CommentsForIncabGetExistingResponseDocument mockResponse = mock(CommentsForIncabGetExistingResponseDocument.class);
		when(mockWebaspxRequestService.getExistingCommentsForIncab(COMPANY_ID, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getExistingCommentsForIncab(PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void commentsForIncabUpdate_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		CommentsForIncabUpdateResponseDocument mockResponse = mock(CommentsForIncabUpdateResponseDocument.class);
		when(mockWebaspxRequestService.updateCommentsForIncab(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.updateCommentsForIncab(PARAM1, PARAM2, PARAM3, PARAM4, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void showAdditionalBins_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		ShowAdditionalBinsResponseDocument mockResponse = mock(ShowAdditionalBinsResponseDocument.class);
		when(mockWebaspxRequestService.showAdditionalBins(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.showAdditionalBins(PARAM1, PARAM2, PARAM3, PARAM4, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

	@Test
	public void getRoundChangesInDateRangeByRoundOrUPRN_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {

		GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument mockResponse = mock(GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.class);
		when(mockWebaspxRequestService.getRoundChangesInDateRangeByRoundOrUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = webaspxRESTApplication.getRoundChangesInDateRangeByRoundOrUPRN(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);

	}

}
