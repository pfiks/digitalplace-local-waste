package com.placecube.digitalplace.local.waste.webaspx.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.local.waste.model.BinCollection;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class JsonResponseParserServiceTest {

	@InjectMocks
	private JsonResponseParserService jsonResponseParserService;

	@Mock
	private WebaspxWasteService mockWebaspxWasteService;

	@Mock
	private BinCollection mockBinCollection;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJSONObjectOne;

	@Mock
	private JSONObject mockJSONObjectTwo;

	@Mock
	private JSONObject mockJSONObjectThree;

	@Mock
	private JSONObject mockJSONObjectFour;

	@Test
	public void getRoundForUPRNRowJSONObject_WhenRoundForUPRNResultObjectIsNull_ThenReturnsEmptyOptional() {
		when(mockJSONObjectOne.getJSONObject("getRoundForUPRNResult")).thenReturn(null);

		Optional<JSONObject> result = jsonResponseParserService.getRoundForUPRNRowJSONObject(mockJSONObjectOne);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getRoundForUPRNRowJSONObject_WhenRoundForUPRNResultObjectHasNoRows_ThenReturnsEmptyOptional() {
		when(mockJSONObjectOne.getJSONObject("getRoundForUPRNResult")).thenReturn(mockJSONObjectTwo);
		when(mockJSONObjectTwo.getJSONObject("Rows")).thenReturn(null);

		Optional<JSONObject> result = jsonResponseParserService.getRoundForUPRNRowJSONObject(mockJSONObjectOne);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getRoundForUPRNRowJSONObject_WhenRoundForUPRNResultObjectHasRows_ThenReturnsRowJSONObject() {
		when(mockJSONObjectOne.getJSONObject("getRoundForUPRNResult")).thenReturn(mockJSONObjectTwo);
		when(mockJSONObjectTwo.getJSONObject("Rows")).thenReturn(mockJSONObjectThree);
		when(mockJSONObjectThree.getJSONObject("Row")).thenReturn(mockJSONObjectFour);

		Optional<JSONObject> result = jsonResponseParserService.getRoundForUPRNRowJSONObject(mockJSONObjectOne);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockJSONObjectFour));
	}

	@Test
	public void getRoundAndBinForUPRNJSONObject_WhenRoundAndBinForUPRNResultObjectIsNull_ThenReturnsEmptyOptional() {
		when(mockJSONObjectOne.getJSONObject("getRoundAndBinInfoForUPRNForNewAdjustedDatesResult")).thenReturn(null);

		Optional<JSONArray> result = jsonResponseParserService.getRoundAndBinForUPRNJSONObject(mockJSONObjectOne);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getRoundAndBinForUPRNJSONObject_WhenRoundAndBinForUPRNResultObjectHasNoRows_ThenReturnsEmptyOptional() {
		when(mockJSONObjectOne.getJSONObject("getRoundAndBinInfoForUPRNForNewAdjustedDatesResult")).thenReturn(mockJSONObjectTwo);
		when(mockJSONObjectTwo.getJSONObject("Rows")).thenReturn(null);

		Optional<JSONArray> result = jsonResponseParserService.getRoundAndBinForUPRNJSONObject(mockJSONObjectOne);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getRoundAndBinForUPRNJSONObject_WhenRoundAndBinForUPRNResultObjectHasRows_ThenReturnsBinJSONObject() {
		when(mockJSONObjectOne.getJSONObject("getRoundAndBinInfoForUPRNForNewAdjustedDatesResult")).thenReturn(mockJSONObjectTwo);
		when(mockJSONObjectTwo.getJSONObject("Rows")).thenReturn(mockJSONObjectThree);
		when(mockJSONObjectThree.getJSONArray("Bin")).thenReturn(mockJSONArray);

		Optional<JSONArray> result = jsonResponseParserService.getRoundAndBinForUPRNJSONObject(mockJSONObjectOne);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockJSONArray));
	}

	@Test
	@Parameters({ "refuse,rubbish", "garden,garden", "recycling,recycling" })
	public void getBinCollectionFromJSONRoundAndBin_WhenNameIsRefuse_ThenReturnsRubbishBinCollection(String serviceName, String webAspxsServiceName) {
		String name = serviceName + ":123";
		when(mockWebaspxWasteService.createBinCollection(name, mockJSONArray, webAspxsServiceName)).thenReturn(Optional.of(mockBinCollection));

		Optional<BinCollection> result = jsonResponseParserService.getBinCollectionFromJSONRoundAndBin(name, mockJSONArray);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockBinCollection));
	}

	@Test
	public void getBinCollectionFromJSONRoundAndBin_WhenNameIsUnknown_ThenReturnsEmptyOptional() {
		String name = "unknown:123";

		Optional<BinCollection> result = jsonResponseParserService.getBinCollectionFromJSONRoundAndBin(name, mockJSONArray);

		assertThat(result.isPresent(), equalTo(false));
	}
}
