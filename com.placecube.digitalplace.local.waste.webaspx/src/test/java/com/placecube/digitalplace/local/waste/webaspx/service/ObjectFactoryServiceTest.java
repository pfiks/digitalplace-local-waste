package com.placecube.digitalplace.local.waste.webaspx.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.placecube.digitalplace.local.waste.model.BinCollection.BinCollectionBuilder;

public class ObjectFactoryServiceTest {

	private ObjectFactoryService objectFactoryService;

	@Before
	public void activateSetup() {
		objectFactoryService = new ObjectFactoryService();
	}

	@Test
	public void createBinCollectionBuilder_WhenNoErrors_ThenCreatesNotNullBuilderWithProperties() {
		final String name = "name";
		final LocalDate localDate = LocalDate.now();
		Map<Locale, String> labelMap = new HashMap<>();
		labelMap.put(Locale.UK, "label");

		BinCollectionBuilder builder = objectFactoryService.createBinCollectionBuilder(name, localDate, labelMap);

		assertThat(builder.getName(), equalTo(name));
		assertThat(builder.getCollectionDate(), equalTo(localDate));
		assertThat(builder.getLabelMap().size(), equalTo(1));
		assertThat(builder.getLabelMap().get(Locale.UK), equalTo("label"));
	}
}
