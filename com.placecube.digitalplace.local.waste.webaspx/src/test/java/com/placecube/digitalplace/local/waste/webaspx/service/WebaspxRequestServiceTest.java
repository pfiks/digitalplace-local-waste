package com.placecube.digitalplace.local.waste.webaspx.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONFactory;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestDocument.AAHelloWorldStringTest;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteDocument.BinDelete;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertDocument.BinInsert;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateDocument.BinUpdate;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarDocument.CachedCalendar;
import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedDocument.CheckAssisted;
import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingDocument.CommentsForIncabGetExisting;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateDocument.CommentsForIncabUpdate;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceDocument.CountBinsForService;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsDocument.CountSubscriptions;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressDocument.GardenChangeAddress;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionDocument.GardenRemoveSubscription;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionDocument.GardenSubscription;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeDocument.GetAddressFromPostcode;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNDocument.GetAddressOrUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsDocument.GetAllRoundDetails;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateDocument.GetAllUPRNsForDate;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersDocument.GetAvailableContainers;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyDocument.GetAvailableContainersForClientOrProperty;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsDocument.GetAvailableRounds;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceDocument.GetBinDetailsForService;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeDocument.GetInCabIssueTextAndIssueCode;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesDocument.GetIssues;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNDocument.GetIssuesForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionDocument.GetLastTruckPosition;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsDocument.GetLatestTruckPositions;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesDocument.GetNoCollectionDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNDocument.GetRoundCalendarForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNDocument.GetRoundChangesInDateRangeByRoundOrUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNDocument.GetRoundForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceDocument.GetRoundNameForUPRNService;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayDocument.GetTrucksOutToday;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsDocument.LLPGXtraUpdateGetAvailableFieldDetails;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsDocument.LargeHouseholds;
import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedDocument.LastCollected;
import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesDocument.QueryBinEndDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeDocument.QueryBinOnType;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeDocument.ReadWriteMissedBinByContainerType;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinDocument.ReadWriteMissedBin;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsDocument.ShowAdditionalBins;
import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedDocument.UpdateAssisted;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.WSCollExternalStub;
import com.placecube.digitalplace.local.waste.webaspx.configuration.WebaspxConfiguration;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WebaspxRequestServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 0;

	private static final String PARAM1 = "PARAM1";
	private static final String PARAM10 = "PARAM10";
	private static final String PARAM11 = null;
	private static final String PARAM12 = null;
	private static final String PARAM2 = "PARAM2";
	private static final String PARAM3 = "PARAM3";
	private static final String PARAM4 = "PARAM4";
	private static final String PARAM5 = "PARAM5";
	private static final String PARAM6 = "PARAM6";
	private static final String PARAM7 = "PARAM7";

	private static final String PARAM8 = "PARAM8";

	private static final String PARAM9 = "PARAM9";

	private static final String password = null;

	private static final String userName = null;

	private static final String userNamePassword = null;

	@Mock
	private WebaspxConfiguration mockConfiguration;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JsonResponseParserService mockJsonResponseParserService;

	@Mock
	private ObjectFactoryService mockObjectFactoryService;

	@Mock
	private WSCollExternalStub mockWSCollExternalStub;

	@Mock
	private WebaspxAxisService webAspxAxisService;

	@InjectMocks
	private WebaspxRequestService webaspxRequestService;

	@Mock
	private WebaspxWasteService webAspxWasteService;

	@Test
	public void aaHelloWorldXMLTest_WhenNoError_ThenReturn_AAHelloWorldXMLTestResponseDocument() throws Exception {

		AAHelloWorldXMLTestDocument mockDocumentInput = mock(AAHelloWorldXMLTestDocument.class);

		AAHelloWorldXMLTest mockInput = mock(AAHelloWorldXMLTest.class);

		AAHelloWorldXMLTestResponseDocument mockResponse = mock(AAHelloWorldXMLTestResponseDocument.class);

		when(mockObjectFactoryService.aaHelloWorldXMLTestDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.aaHelloWorldXMLTestInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.aA_HelloWorld_XML_Test(mockDocumentInput)).thenReturn(mockResponse);

		AAHelloWorldXMLTestResponseDocument response = webaspxRequestService.getAAHelloWorldXMLTest(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setTestName(PARAM1);

		verify(mockDocumentInput, times(1)).setAAHelloWorldXMLTest(mockInput);

		assertEquals(response, mockResponse);

	}

	@Before
	public void activate() throws Exception {

		when(webAspxAxisService.getWebaspxAxisService(COMPANY_ID)).thenReturn(mockWSCollExternalStub);

		when(webAspxWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockConfiguration);

	}

	@Test
	public void addOrUpdateGardenSubscription_WhenNoError_ThenReturn_GardenSubscriptionResponseDocument() throws Exception {

		GardenSubscriptionDocument mockDocumentInput = mock(GardenSubscriptionDocument.class);

		GardenSubscription mockInput = mock(GardenSubscription.class);

		GardenSubscriptionResponseDocument mockResponse = mock(GardenSubscriptionResponseDocument.class);

		when(mockObjectFactoryService.getGardenSubscriptionDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getGardenSubscriptionInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.gardenSubscription(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_addOrUpdateGardenSubscription()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GardenSubscriptionResponseDocument response = webaspxRequestService.addOrUpdateGardenSubscription(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setBinsAtProperty(PARAM3);
		verify(mockInput, times(1)).setTotalNoSubsRequired(PARAM4);
		verify(mockInput, times(1)).setSubscriptionStartDate(PARAM5);
		verify(mockInput, times(1)).setSubscriptionEndDate(PARAM6);
		verify(mockInput, times(1)).setNewPayRef(PARAM7);
		verify(mockInput, times(1)).setBinType(PARAM8);
		verify(mockInput, times(1)).setGardenContainerDeliveryComments(PARAM9);
		verify(mockInput, times(1)).setCrmGardenRef(PARAM10);
		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockDocumentInput, times(1)).setGardenSubscription(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void changeGardenAddress_WhenNoError_ThenReturn_GardenChangeAddressResponseDocument() throws Exception {

		GardenChangeAddressDocument mockDocumentInput = mock(GardenChangeAddressDocument.class);

		GardenChangeAddress mockInput = mock(GardenChangeAddress.class);

		GardenChangeAddressResponseDocument mockResponse = mock(GardenChangeAddressResponseDocument.class);

		when(mockObjectFactoryService.getGardenChangeAddressDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getGardenChangeAddressInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.garden_ChangeAddress(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_changeGardenAddress()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GardenChangeAddressResponseDocument response = webaspxRequestService.changeGardenAddress(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setFromUPRN(PARAM2);
		verify(mockInput, times(1)).setToUPRN(PARAM3);
		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockDocumentInput, times(1)).setGardenChangeAddress(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void countBinsForService_WhenNoError_ThenReturn_CountBinsForServiceResponseDocument() throws Exception {

		CountBinsForServiceDocument mockDocumentInput = mock(CountBinsForServiceDocument.class);

		CountBinsForService mockInput = mock(CountBinsForService.class);

		CountBinsForServiceResponseDocument mockResponse = mock(CountBinsForServiceResponseDocument.class);

		when(mockObjectFactoryService.countBinsForServiceDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.countBinsForServiceInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.countBinsForService(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_countBinsForService()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		CountBinsForServiceResponseDocument response = webaspxRequestService.countBinsForService(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setBinService(PARAM3);
		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockDocumentInput, times(1)).setCountBinsForService(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void countSubscriptions_WhenNoError_ThenReturn_CountSubscriptionsResponseDocument() throws Exception {

		CountSubscriptionsDocument mockDocumentInput = mock(CountSubscriptionsDocument.class);

		CountSubscriptions mockInput = mock(CountSubscriptions.class);

		CountSubscriptionsResponseDocument mockResponse = mock(CountSubscriptionsResponseDocument.class);

		when(mockObjectFactoryService.getCountSubscriptionsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getCountSubscriptionsInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.countSubscriptions(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_countSubscriptions()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		CountSubscriptionsResponseDocument response = webaspxRequestService.countSubscriptions(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setStartDateDDsMMsYYYY(PARAM2);
		verify(mockInput, times(1)).setEndDateDDsMMsYYYY(PARAM3);
		verify(mockInput, times(1)).setReturnUPRNs(PARAM4);

		verify(mockDocumentInput, times(1)).setCountSubscriptions(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void deleteBin_WhenNoError_ThenReturn_BinDeleteResponseDocument() throws Exception {

		BinDeleteDocument mockDocumentInput = mock(BinDeleteDocument.class);

		BinDelete mockInput = mock(BinDelete.class);

		BinDeleteResponseDocument mockResponse = mock(BinDeleteResponseDocument.class);

		when(mockObjectFactoryService.getBinDeleteDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getBinDeleteInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.binDelete(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_deleteBin()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		BinDeleteResponseDocument response = webaspxRequestService.deleteBin(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setBinID(PARAM3);

		verify(mockDocumentInput, times(1)).setBinDelete(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAAHelloWorldString_WhenNoError_ThenReturn_AAHelloWorldStringTestResponseDocument() throws Exception {

		AAHelloWorldStringTestDocument mockDocumentInput = mock(AAHelloWorldStringTestDocument.class);

		AAHelloWorldStringTest mockInput = mock(AAHelloWorldStringTest.class);

		AAHelloWorldStringTestResponseDocument mockResponse = mock(AAHelloWorldStringTestResponseDocument.class);

		when(mockObjectFactoryService.aaHelloWorldStringTestDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.aaHelloWorldStringTestInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.aA_HelloWorld_String_Test(mockDocumentInput)).thenReturn(mockResponse);

		AAHelloWorldStringTestResponseDocument response = webaspxRequestService.getAAHelloWorldStringTest(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setTestName(PARAM1);

		verify(mockDocumentInput, times(1)).setAAHelloWorldStringTest(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAddressFromPostcode_WhenNoError_ThenReturn_GetAddressFromPostcodeResponseDocument() throws Exception {

		GetAddressFromPostcodeDocument mockDocumentInput = mock(GetAddressFromPostcodeDocument.class);

		GetAddressFromPostcode mockInput = mock(GetAddressFromPostcode.class);

		GetAddressFromPostcodeResponseDocument mockResponse = mock(GetAddressFromPostcodeResponseDocument.class);

		when(mockObjectFactoryService.getGetAddressFromPostcodeDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getGetAddressFromPostcodeInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getAddressFromPostcode(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getAddressFromPostcode()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetAddressFromPostcodeResponseDocument response = webaspxRequestService.getAddressFromPostcode(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setSearchString(PARAM2);

		verify(mockDocumentInput, times(1)).setGetAddressFromPostcode(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAddressFromPostcode_WhenNoError_ThenReturn_GetAddressOrUPRNResponseDocument() throws Exception {

		GetAddressOrUPRNDocument mockDocumentInput = mock(GetAddressOrUPRNDocument.class);

		GetAddressOrUPRN mockInput = mock(GetAddressOrUPRN.class);

		GetAddressOrUPRNResponseDocument mockResponse = mock(GetAddressOrUPRNResponseDocument.class);

		when(mockObjectFactoryService.getAddressOrUPRNDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getAddressOrUPRNInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getAddressOrUPRN(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getAddressFromPostcode()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetAddressOrUPRNResponseDocument response = webaspxRequestService.getAddressOrUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setAddress(PARAM3);

		verify(mockDocumentInput, times(1)).setGetAddressOrUPRN(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAllIssuesForIssueType_WhenNoError_ThenReturn_GetAddressOrUPRNResponseDocument() throws Exception {

		GetAllIssuesForIssueTypeDocument mockDocumentInput = mock(GetAllIssuesForIssueTypeDocument.class);

		GetAllIssuesForIssueType mockInput = mock(GetAllIssuesForIssueType.class);

		GetAllIssuesForIssueTypeResponseDocument mockResponse = mock(GetAllIssuesForIssueTypeResponseDocument.class);

		when(mockObjectFactoryService.getAllIssuesForIssueTypeDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getAllIssuesForIssueTypeInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getAllIssuesForIssueType(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getAllIssuesForIssueType()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		GetAllIssuesForIssueTypeResponseDocument response = webaspxRequestService.getAllIssuesForIssueType(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockInput, times(1)).setIssueType(PARAM2);
		verify(mockInput, times(1)).setStartDateDDMMYYYY(PARAM3);
		verify(mockInput, times(1)).setEndDateDDMMYYYY(PARAM4);
		verify(mockInput, times(1)).setDigTime(PARAM5);
		verify(mockInput, times(1)).setService(PARAM6);

		verify(mockDocumentInput, times(1)).setGetAllIssuesForIssueType(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAllRoundDetails_WhenNoError_ThenReturn_GetAddressOrUPRNResponseDocument() throws Exception {

		GetAllRoundDetailsDocument mockDocumentInput = mock(GetAllRoundDetailsDocument.class);

		GetAllRoundDetails mockInput = mock(GetAllRoundDetails.class);

		GetAllRoundDetailsResponseDocument mockResponse = mock(GetAllRoundDetailsResponseDocument.class);

		when(mockObjectFactoryService.getGetAllRoundDetailsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getAllRoundDetailsResponseDocumentInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getAllRoundDetails(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getAllRoundDetails()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetAllRoundDetailsResponseDocument response = webaspxRequestService.getAllRoundDetails(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockDocumentInput, times(1)).setGetAllRoundDetails(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAllUPRNsForDate_WhenNoError_ThenReturn_GetAddressOrUPRNResponseDocument() throws Exception {

		GetAllUPRNsForDateDocument mockDocumentInput = mock(GetAllUPRNsForDateDocument.class);

		GetAllUPRNsForDate mockInput = mock(GetAllUPRNsForDate.class);

		GetAllUPRNsForDateResponseDocument mockResponse = mock(GetAllUPRNsForDateResponseDocument.class);

		when(mockObjectFactoryService.getGetAllUPRNsForDateDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getAllUPRNsForDateInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getAllUPRNsForDate(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getAllUPRNsForDate()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetAllUPRNsForDateResponseDocument response = webaspxRequestService.getAllUPRNsForDate(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setDateRequiredDDsMMsYYYY(PARAM2);

		verify(mockDocumentInput, times(1)).setGetAllUPRNsForDate(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAvailableContainers_WhenNoError_ThenReturn_GetAddressOrUPRNResponseDocument() throws Exception {

		GetAvailableContainersDocument mockDocumentInput = mock(GetAvailableContainersDocument.class);

		GetAvailableContainers mockInput = mock(GetAvailableContainers.class);

		GetAvailableContainersResponseDocument mockResponse = mock(GetAvailableContainersResponseDocument.class);

		when(mockObjectFactoryService.getAvailableContainersDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getAvailableContainersInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getAvailableContainers(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getAvailableContainers()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetAvailableContainersResponseDocument response = webaspxRequestService.getAvailableContainers(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockDocumentInput, times(1)).setGetAvailableContainers(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAvailableContainersForClientOrProperty_WhenNoError_ThenReturn_GetAddressOrUPRNResponseDocument() throws Exception {

		GetAvailableContainersForClientOrPropertyDocument mockDocumentInput = mock(GetAvailableContainersForClientOrPropertyDocument.class);

		GetAvailableContainersForClientOrProperty mockInput = mock(GetAvailableContainersForClientOrProperty.class);

		GetAvailableContainersForClientOrPropertyResponseDocument mockResponse = mock(GetAvailableContainersForClientOrPropertyResponseDocument.class);

		when(mockObjectFactoryService.getAvailableContainersForClientOrPropertyDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getAvailableContainersForClientOrPropertyInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getAvailableContainersForClientOrProperty(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getAvailableContainersForClientOrProperty()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetAvailableContainersForClientOrPropertyResponseDocument response = webaspxRequestService.getAvailableContainersForClientOrProperty(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);

		verify(mockDocumentInput, times(1)).setGetAvailableContainersForClientOrProperty(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getAvailableRounds_WhenNoError_ThenReturn_GetAvailableRoundsResponseDocument() throws Exception {

		GetAvailableRoundsDocument mockDocumentInput = mock(GetAvailableRoundsDocument.class);

		GetAvailableRounds mockInput = mock(GetAvailableRounds.class);

		GetAvailableRoundsResponseDocument mockResponse = mock(GetAvailableRoundsResponseDocument.class);

		when(mockObjectFactoryService.getAvailableRoundsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getAvailableRoundsInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getAvailableRounds(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getAvailableRounds()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetAvailableRoundsResponseDocument response = webaspxRequestService.getAvailableRounds(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setService3L(PARAM2);

		verify(mockDocumentInput, times(1)).setGetAvailableRounds(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getBinDetailsForService_WhenNoError_ThenReturn_GetAvailableRoundsResponseDocument() throws Exception {

		GetBinDetailsForServiceDocument mockDocumentInput = mock(GetBinDetailsForServiceDocument.class);

		GetBinDetailsForService mockInput = mock(GetBinDetailsForService.class);

		GetBinDetailsForServiceResponseDocument mockResponse = mock(GetBinDetailsForServiceResponseDocument.class);

		when(mockObjectFactoryService.getBinDetailsForServiceDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getBinDetailsForServiceInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getBinDetailsForService(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getBinDetailsForService()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetBinDetailsForServiceResponseDocument response = webaspxRequestService.getBinDetailsForService(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setBinService(PARAM3);

		verify(mockDocumentInput, times(1)).setGetBinDetailsForService(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getCachedCalendar_WhenNoError_ThenReturn_CachedCalendarResponseDocument() throws Exception {

		CachedCalendarDocument mockDocumentInput = mock(CachedCalendarDocument.class);

		CachedCalendar mockInput = mock(CachedCalendar.class);

		CachedCalendarResponseDocument mockResponse = mock(CachedCalendarResponseDocument.class);

		when(mockObjectFactoryService.getCachedCalendarDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getCachedCalendarInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.cachedCalendar(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getCachedCalendar()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		CachedCalendarResponseDocument response = webaspxRequestService.getCachedCalendar(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setSplitOutRounds(PARAM2);

		verify(mockDocumentInput, times(1)).setCachedCalendar(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getCheckAssisted_WhenNoError_ThenReturn_CachedCalendarResponseDocument() throws Exception {

		CheckAssistedDocument mockDocumentInput = mock(CheckAssistedDocument.class);

		CheckAssisted mockInput = mock(CheckAssisted.class);

		CheckAssistedResponseDocument mockResponse = mock(CheckAssistedResponseDocument.class);

		when(mockObjectFactoryService.getCheckAssistedDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getCheckAssistedInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.checkAssisted(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getCheckAssisted()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		CheckAssistedResponseDocument response = webaspxRequestService.getCheckAssisted(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);

		verify(mockDocumentInput, times(1)).setCheckAssisted(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getExistingCommentsForIncab_WhenNoError_ThenReturn_CachedCalendarResponseDocument() throws Exception {

		CommentsForIncabGetExistingDocument mockDocumentInput = mock(CommentsForIncabGetExistingDocument.class);

		CommentsForIncabGetExisting mockInput = mock(CommentsForIncabGetExisting.class);

		CommentsForIncabGetExistingResponseDocument mockResponse = mock(CommentsForIncabGetExistingResponseDocument.class);

		when(mockObjectFactoryService.getCommentsForIncabGetExistingDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getCommentsForIncabGetExistingInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.commentsForIncabGetExisting(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getExistingCommentsForIncab()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		CommentsForIncabGetExistingResponseDocument response = webaspxRequestService.getExistingCommentsForIncab(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);

		verify(mockDocumentInput, times(1)).setCommentsForIncabGetExisting(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getGardenSubscribersDeliveriesCollections_WhenNoError_ThenReturn_CachedCalendarResponseDocument() throws Exception {

		GardenSubscribersDeliveriesCollectionsDocument mockDocumentInput = mock(GardenSubscribersDeliveriesCollectionsDocument.class);

		GardenSubscribersDeliveriesCollections mockInput = mock(GardenSubscribersDeliveriesCollections.class);

		GardenSubscribersDeliveriesCollectionsResponseDocument mockResponse = mock(GardenSubscribersDeliveriesCollectionsResponseDocument.class);

		when(mockObjectFactoryService.getGardenSubscribersDeliveriesCollectionsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getGardenSubscribersDeliveriesCollectionsInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.garden_Subscribers_Deliveries_Collections(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getGardenSubscribersDeliveriesCollections()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GardenSubscribersDeliveriesCollectionsResponseDocument response = webaspxRequestService.getGardenSubscribersDeliveriesCollections(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setSubscribersDeliveriesOrCollections(PARAM2);
		verify(mockInput, times(1)).setDateToCheckOnYyyyMMdd(PARAM3);

		verify(mockDocumentInput, times(1)).setGardenSubscribersDeliveriesCollections(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getInCabIssueTextAndIssueCode_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		GetInCabIssueTextAndIssueCodeDocument mockDocumentInput = mock(GetInCabIssueTextAndIssueCodeDocument.class);

		GetInCabIssueTextAndIssueCode mockInput = mock(GetInCabIssueTextAndIssueCode.class);

		GetInCabIssueTextAndIssueCodeResponseDocument mockResponse = mock(GetInCabIssueTextAndIssueCodeResponseDocument.class);

		when(mockObjectFactoryService.getGetInCabIssueTextAndIssueCodeDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getInCabIssueTextAndIssueCodeInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getInCabIssueTextAndIssueCode(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getInCabIssueTextAndIssueCode()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		GetInCabIssueTextAndIssueCodeResponseDocument response = webaspxRequestService.getInCabIssueTextAndIssueCode(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockDocumentInput, times(1)).setGetInCabIssueTextAndIssueCode(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getIssues_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		GetIssuesDocument mockDocumentInput = mock(GetIssuesDocument.class);

		GetIssues mockInput = mock(GetIssues.class);

		GetIssuesResponseDocument mockResponse = mock(GetIssuesResponseDocument.class);

		when(mockObjectFactoryService.getIssuesDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getIssuesInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getIssues(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getIssues()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		GetIssuesResponseDocument response = webaspxRequestService.getIssues(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setDateReq(PARAM2);
		verify(mockInput, times(1)).setIssueStartTime(PARAM3);
		verify(mockInput, times(1)).setIssueEndTime(PARAM4);
		verify(mockInput, times(1)).setIssueSortField(PARAM5);
		verify(mockInput, times(1)).setIssueSortOrder(PARAM6);
		verify(mockInput, times(1)).setIssueFilterName(PARAM7);
		verify(mockInput, times(1)).setIssueFilterType(PARAM8);
		verify(mockInput, times(1)).setSearchAllDates(PARAM9);
		verify(mockInput, times(1)).setPropNoName(PARAM10);
		verify(mockInput, times(1)).setPropStreet(PARAM11);
		verify(mockInput, times(1)).setUPRN(PARAM12);

		verify(mockDocumentInput, times(1)).setGetIssues(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getIssuesAndCollectionStatusForUPRN_WhenNoError_ThenReturn_GetIssuesAndCollectionStatusForUPRNResponseDocument() throws Exception {

		GetIssuesAndCollectionStatusForUPRNDocument mockDocumentInput = mock(GetIssuesAndCollectionStatusForUPRNDocument.class);

		GetIssuesAndCollectionStatusForUPRN mockInput = mock(GetIssuesAndCollectionStatusForUPRN.class);

		GetIssuesAndCollectionStatusForUPRNResponseDocument mockResponse = mock(GetIssuesAndCollectionStatusForUPRNResponseDocument.class);

		when(mockObjectFactoryService.getIssuesAndCollectionStatusForUPRNDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getIssuesAndCollectionStatusForUPRNInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getIssuesAndCollectionStatusForUPRN(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getIssuesAndCollectionStatusForUPRN()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		GetIssuesAndCollectionStatusForUPRNResponseDocument response = webaspxRequestService.getIssuesAndCollectionStatusForUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setLastTimeOnly(PARAM3);
		verify(mockInput, times(1)).setStartDateyyyyMMdd(PARAM4);
		verify(mockInput, times(1)).setEndDateyyyyMMdd(PARAM5);
		verify(mockInput, times(1)).setServiceFilter(PARAM6);

		verify(mockDocumentInput, times(1)).setGetIssuesAndCollectionStatusForUPRN(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getIssuesForUPRN_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		GetIssuesForUPRNDocument mockDocumentInput = mock(GetIssuesForUPRNDocument.class);

		GetIssuesForUPRN mockInput = mock(GetIssuesForUPRN.class);

		GetIssuesForUPRNResponseDocument mockResponse = mock(GetIssuesForUPRNResponseDocument.class);

		when(mockObjectFactoryService.getIssuesForUPRNDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getIssuesForUPRNInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getIssuesForUPRN(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getIssuesForUPRN()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		GetIssuesForUPRNResponseDocument response = webaspxRequestService.getIssuesForUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setDateReq(PARAM2);
		verify(mockInput, times(1)).setUPRN(PARAM3);

		verify(mockDocumentInput, times(1)).setGetIssuesForUPRN(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLargeHouseholds_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		LargeHouseholdsDocument mockDocumentInput = mock(LargeHouseholdsDocument.class);

		LargeHouseholds mockInput = mock(LargeHouseholds.class);

		LargeHouseholdsResponseDocument mockResponse = mock(LargeHouseholdsResponseDocument.class);

		when(mockObjectFactoryService.getLargeHouseholdsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getLargeHouseholdsInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.largeHouseholds(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getLargeHouseholds()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		LargeHouseholdsResponseDocument response = webaspxRequestService.getLargeHouseholds(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setBinType(PARAM2);

		verify(mockDocumentInput, times(1)).setLargeHouseholds(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLastCollected_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		LastCollectedDocument mockDocumentInput = mock(LastCollectedDocument.class);

		LastCollected mockInput = mock(LastCollected.class);

		LastCollectedResponseDocument mockResponse = mock(LastCollectedResponseDocument.class);

		when(mockObjectFactoryService.lastCollectedDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.lastCollectedInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.lastCollected(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getLastCollected()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		LastCollectedResponseDocument response = webaspxRequestService.getLastCollected(COMPANY_ID, PARAM1, PARAM2, false);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setReturnGPS(false);

		verify(mockDocumentInput, times(1)).setLastCollected(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLastTruckPosition_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		GetLastTruckPositionDocument mockDocumentInput = mock(GetLastTruckPositionDocument.class);

		GetLastTruckPosition mockInput = mock(GetLastTruckPosition.class);

		GetLastTruckPositionResponseDocument mockResponse = mock(GetLastTruckPositionResponseDocument.class);

		when(mockObjectFactoryService.getLastTruckPositionDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getLastTruckPositionInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getLastTruckPosition(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getLastTruckPosition()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		GetLastTruckPositionResponseDocument response = webaspxRequestService.getLastTruckPosition(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setDateReq(PARAM2);
		verify(mockInput, times(1)).setTimeReq(PARAM3);
		verify(mockInput, times(1)).setRegReq(PARAM4);

		verify(mockDocumentInput, times(1)).setGetLastTruckPosition(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLatestTruckPositions_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		GetLatestTruckPositionsDocument mockDocumentInput = mock(GetLatestTruckPositionsDocument.class);

		GetLatestTruckPositions mockInput = mock(GetLatestTruckPositions.class);

		GetLatestTruckPositionsResponseDocument mockResponse = mock(GetLatestTruckPositionsResponseDocument.class);

		when(mockObjectFactoryService.getLatestTruckPositionsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getLatestTruckPositionsInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getLatestTruckPositions(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getLatestTruckPositions()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		GetLatestTruckPositionsResponseDocument response = webaspxRequestService.getLatestTruckPositions(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);

		verify(mockDocumentInput, times(1)).setGetLatestTruckPositions(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLLPGXtraUpdateAvailableFieldDetails_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		LLPGXtraUpdateGetAvailableFieldDetailsDocument mockDocumentInput = mock(LLPGXtraUpdateGetAvailableFieldDetailsDocument.class);

		LLPGXtraUpdateGetAvailableFieldDetails mockInput = mock(LLPGXtraUpdateGetAvailableFieldDetails.class);

		LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument mockResponse = mock(LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.class);

		when(mockObjectFactoryService.getLLPGXtraUpdateGetAvailableFieldDetailsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getLLPGXtraUpdateGetAvailableFieldDetailsInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.lLPGXtraUpdateGetAvailableFieldDetails(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getLLPGXtraUpdateAvailableFieldDetails()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument response = webaspxRequestService.getLLPGXtraUpdateAvailableFieldDetails(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockDocumentInput, times(1)).setLLPGXtraUpdateGetAvailableFieldDetails(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLLPGXtraUpdateUPRNForDBField_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		LLPGXtraUpdateGetAvailableFieldDetailsDocument mockDocumentInput = mock(LLPGXtraUpdateGetAvailableFieldDetailsDocument.class);

		LLPGXtraUpdateGetAvailableFieldDetails mockInput = mock(LLPGXtraUpdateGetAvailableFieldDetails.class);

		LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument mockResponse = mock(LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.class);

		when(mockObjectFactoryService.getLLPGXtraUpdateGetAvailableFieldDetailsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getLLPGXtraUpdateGetAvailableFieldDetailsInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.lLPGXtraUpdateGetAvailableFieldDetails(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getLLPGXtraUpdateUPRNForDBField()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument response = webaspxRequestService.getLLPGXtraUpdateAvailableFieldDetails(COMPANY_ID, PARAM1);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockDocumentInput, times(1)).setLLPGXtraUpdateGetAvailableFieldDetails(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLLPGXtraUPRNCurrentValuesForDBField_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		LLPGXtraGetUPRNCurrentValuesForDBFieldDocument mockDocumentInput = mock(LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.class);

		LLPGXtraGetUPRNCurrentValuesForDBField mockInput = mock(LLPGXtraGetUPRNCurrentValuesForDBField.class);

		LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument mockResponse = mock(LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.class);

		when(mockObjectFactoryService.getLLPGXtraGetUPRNCurrentValuesForDBFieldDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getLLPGXtraGetUPRNCurrentValuesForDBFieldInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.lLPGXtraGetUPRNCurrentValuesForDBField(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getLLPGXtraUPRNCurrentValuesForDBField()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument response = webaspxRequestService.getLLPGXtraUPRNCurrentValuesForDBField(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRNs(PARAM2);
		verify(mockInput, times(1)).setDatabaseFieldName(PARAM3);

		verify(mockDocumentInput, times(1)).setLLPGXtraGetUPRNCurrentValuesForDBField(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getNoCollectionDates_WhenNoError_ThenReturn_GetInCabIssueTextAndIssueCodeResponseDocument() throws Exception {

		GetNoCollectionDatesDocument mockDocumentInput = mock(GetNoCollectionDatesDocument.class);

		GetNoCollectionDates mockInput = mock(GetNoCollectionDates.class);

		GetNoCollectionDatesResponseDocument mockResponse = mock(GetNoCollectionDatesResponseDocument.class);

		when(mockObjectFactoryService.getNoCollectionDatesDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getNoCollectionDatesServiceInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getNoCollectionDates(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		// when(mockConfiguration.password()).thenReturn(password); //TODO
		// missing password

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetNoCollectionDatesResponseDocument response = webaspxRequestService.getNoCollectionDates(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		// verify(mockInput, times(1)).setWebServicePassword(password); //TODO
		// missing password

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setAllService3LRoundnameUPRN(PARAM2);
		verify(mockInput, times(1)).setAllService3LRoundnameUPRNValue(PARAM3);
		verify(mockInput, times(1)).setStartDateDDsMMsYYYY(PARAM4);
		verify(mockInput, times(1)).setEndDateDDsMMsYYYY(PARAM5);

		verify(mockDocumentInput, times(1)).setGetNoCollectionDates(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPotentialCollectionDatesForNewGardenSubscriber_WhenNoError_ThenReturn_GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument() throws Exception {

		GetPotentialCollectionDatesForNewGardenSubscriberDocument mockDocumentInput = mock(GetPotentialCollectionDatesForNewGardenSubscriberDocument.class);

		GetPotentialCollectionDatesForNewGardenSubscriber mockInput = mock(GetPotentialCollectionDatesForNewGardenSubscriber.class);

		GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument mockResponse = mock(GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.class);

		when(mockObjectFactoryService.getPotentialCollectionDatesForNewGardenSubscriberDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getGetPotentialCollectionDatesForNewGardenSubscriberInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getPotentialCollectionDatesForNewGardenSubscriber(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getPotentialCollectionDatesForNewGardenSubscriber()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument response = webaspxRequestService.getPotentialCollectionDatesForNewGardenSubscriber(COMPANY_ID, PARAM1, PARAM2, PARAM3,
				PARAM4);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setStartDateDdsMMsyyyy(PARAM3);
		verify(mockInput, times(1)).setEndDateDdsMMsyyyy(PARAM4);

		verify(mockDocumentInput, times(1)).setGetPotentialCollectionDatesForNewGardenSubscriber(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getQueryBinEndDates_WhenNoError_ThenReturn_GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument() throws Exception {

		QueryBinEndDatesDocument mockDocumentInput = mock(QueryBinEndDatesDocument.class);

		QueryBinEndDates mockInput = mock(QueryBinEndDates.class);

		QueryBinEndDatesResponseDocument mockResponse = mock(QueryBinEndDatesResponseDocument.class);

		when(mockObjectFactoryService.getQueryBinEndDatesDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getQueryBinEndDatesInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.queryBinEndDates(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getQueryBinEndDates()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		QueryBinEndDatesResponseDocument response = webaspxRequestService.getQueryBinEndDates(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setStartDateddsMMsyyyy(PARAM2);
		verify(mockInput, times(1)).setEndDateddsMMsyyyy(PARAM3);

		verify(mockDocumentInput, times(1)).setQueryBinEndDates(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getQueryBinOnType_WhenNoError_ThenReturn_GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument() throws Exception {

		QueryBinOnTypeDocument mockDocumentInput = mock(QueryBinOnTypeDocument.class);

		QueryBinOnType mockInput = mock(QueryBinOnType.class);

		QueryBinOnTypeResponseDocument mockResponse = mock(QueryBinOnTypeResponseDocument.class);

		when(mockObjectFactoryService.getQueryBinOnTypeDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getQueryBinOnTypeInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.queryBinOnType(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getQueryBinOnType()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		QueryBinOnTypeResponseDocument response = webaspxRequestService.getQueryBinOnType(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setBinType(PARAM2);

		verify(mockDocumentInput, times(1)).setQueryBinOnType(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getRoundAndBinInfoForUPRNForNewAdjustedDates_WhenNoError_ThenReturn_GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument() throws Exception {

		GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument mockDocumentInput = mock(GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument.class);

		GetRoundAndBinInfoForUPRNForNewAdjustedDates mockInput = mock(GetRoundAndBinInfoForUPRNForNewAdjustedDates.class);

		GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument mockResponse = mock(GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.class);

		when(mockObjectFactoryService.getRoundAndBinInfoForUPRNForNewAdjustedDatesDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getRoundAndBinInfoForUPRNForNewAdjustedDatesServiceInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getRoundAndBinInfoForUPRNForNewAdjustedDates(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getRoundAndBinInfoForUPRNForNewAdjustedDates()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument response = webaspxRequestService.getRoundAndBinInfoForUPRNForNewAdjustedDates(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setBinID(PARAM3);
		verify(mockInput, times(1)).setStartDateDDsMMsYYYY(PARAM4);
		verify(mockInput, times(1)).setEndDateDDsMMsYYYY(PARAM5);
		verify(mockDocumentInput, times(1)).setGetRoundAndBinInfoForUPRNForNewAdjustedDates(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getRoundCalendarForUPRN_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		GetRoundCalendarForUPRNDocument mockDocumentInput = mock(GetRoundCalendarForUPRNDocument.class);

		GetRoundCalendarForUPRN mockInput = mock(GetRoundCalendarForUPRN.class);

		GetRoundCalendarForUPRNResponseDocument mockResponse = mock(GetRoundCalendarForUPRNResponseDocument.class);

		when(mockObjectFactoryService.getRoundCalendarForUPRNDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getRoundCalendarForUPRNInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getRoundCalendarForUPRN(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getRoundCalendarForUPRN()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetRoundCalendarForUPRNResponseDocument response = webaspxRequestService.getRoundCalendarForUPRN(COMPANY_ID, PARAM1, PARAM2);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockDocumentInput, times(1)).setGetRoundCalendarForUPRN(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getRoundChangesInDateRangeByRoundOrUPRN_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		GetRoundChangesInDateRangeByRoundOrUPRNDocument mockDocumentInput = mock(GetRoundChangesInDateRangeByRoundOrUPRNDocument.class);

		GetRoundChangesInDateRangeByRoundOrUPRN mockInput = mock(GetRoundChangesInDateRangeByRoundOrUPRN.class);

		GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument mockResponse = mock(GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.class);

		when(mockObjectFactoryService.getGetRoundChangesInDateRangeByRoundOrUPRNDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getRoundChangesInDateRangeByRoundOrUPRNInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getRoundChangesInDateRangeByRoundOrUPRN(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getRoundChangesInDateRangeByRoundOrUPRN()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument response = webaspxRequestService.getRoundChangesInDateRangeByRoundOrUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setRoundNameCrXsDaysWkNsSrv(PARAM3);
		verify(mockInput, times(1)).setStartDateDdsMMsyyyy(PARAM4);
		verify(mockInput, times(1)).setEndDateDdsMMsYYYY(PARAM5);

		verify(mockDocumentInput, times(1)).setGetRoundChangesInDateRangeByRoundOrUPRN(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getRoundForUPRN_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		GetRoundForUPRNDocument mockDocumentInput = mock(GetRoundForUPRNDocument.class);

		GetRoundForUPRN mockInput = mock(GetRoundForUPRN.class);

		GetRoundForUPRNResponseDocument mockResponse = mock(GetRoundForUPRNResponseDocument.class);

		when(mockObjectFactoryService.getRoundForUPRNDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getRoundForUPRNInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getRoundForUPRN(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getRoundForUPRN()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetRoundForUPRNResponseDocument response = webaspxRequestService.getRoundForUPRN(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setStartDateDDsMMsYYYY(PARAM3);

		verify(mockDocumentInput, times(1)).setGetRoundForUPRN(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getRoundNameForUPRNService_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		GetRoundNameForUPRNServiceDocument mockDocumentInput = mock(GetRoundNameForUPRNServiceDocument.class);

		GetRoundNameForUPRNService mockInput = mock(GetRoundNameForUPRNService.class);

		GetRoundNameForUPRNServiceResponseDocument mockResponse = mock(GetRoundNameForUPRNServiceResponseDocument.class);

		when(mockObjectFactoryService.getRoundNameForUPRNServiceDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getRoundNameForUPRNServiceInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getRoundNameForUPRNService(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_getRoundNameForUPRNService()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GetRoundNameForUPRNServiceResponseDocument response = webaspxRequestService.getRoundNameForUPRNService(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setService3L(PARAM3);
		verify(mockDocumentInput, times(1)).setGetRoundNameForUPRNService(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getTrucksOutToday_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		GetTrucksOutTodayDocument mockDocumentInput = mock(GetTrucksOutTodayDocument.class);

		GetTrucksOutToday mockInput = mock(GetTrucksOutToday.class);

		GetTrucksOutTodayResponseDocument mockResponse = mock(GetTrucksOutTodayResponseDocument.class);

		when(mockObjectFactoryService.getTrucksOutTodayDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getTrucksOutTodayInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.getTrucksOutToday(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userNameIncab()).thenReturn(userName);

		when(mockConfiguration.password_getTrucksOutToday()).thenReturn(password);

		when(mockConfiguration.userNamePasswordIncab()).thenReturn(userNamePassword);

		GetTrucksOutTodayResponseDocument response = webaspxRequestService.getTrucksOutToday(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);

		verify(mockInput, times(1)).setDateReq(PARAM2);
		verify(mockInput, times(1)).setTimeReq(PARAM3);

		verify(mockDocumentInput, times(1)).setGetTrucksOutToday(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void insertBinDetails_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		BinInsertDocument mockDocumentInput = mock(BinInsertDocument.class);

		BinInsert mockInput = mock(BinInsert.class);

		BinInsertResponseDocument mockResponse = mock(BinInsertResponseDocument.class);

		when(mockObjectFactoryService.getBinInsertDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getBinInsertInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.binInsert(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_insertBinDetails()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		BinInsertResponseDocument response = webaspxRequestService.insertBinDetails(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setBinType(PARAM3);
		verify(mockInput, times(1)).setPayRef(PARAM4);
		verify(mockInput, times(1)).setDeliverYN(PARAM5);
		verify(mockInput, times(1)).setCollectYN(PARAM6);
		verify(mockInput, times(1)).setStartDateDDsMMsYYYY(PARAM7);
		verify(mockInput, times(1)).setEndDateDDsMMsYYYY(PARAM8);
		verify(mockInput, times(1)).setReportedDateDDsMMsYYYY(PARAM9);
		verify(mockInput, times(1)).setCompletedDateDDsMMsYYYY(PARAM10);
		verify(mockInput, times(1)).setNoOfNewContainersRequired(PARAM11);

		verify(mockDocumentInput, times(1)).setBinInsert(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void readWriteMissedBin_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		ReadWriteMissedBinDocument mockDocumentInput = mock(ReadWriteMissedBinDocument.class);

		ReadWriteMissedBin mockInput = mock(ReadWriteMissedBin.class);

		ReadWriteMissedBinResponseDocument mockResponse = mock(ReadWriteMissedBinResponseDocument.class);

		when(mockObjectFactoryService.getReadWriteMissedBinDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.readWriteMissedBinInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.readWriteMissedBin(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_readWriteMissedBin()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		ReadWriteMissedBinResponseDocument response = webaspxRequestService.readWriteMissedBin(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setService3L(PARAM3);
		verify(mockInput, times(1)).setMissedYN(PARAM4);
		verify(mockInput, times(1)).setMissedDateDDsMMsYY(PARAM5);
		verify(mockDocumentInput, times(1)).setReadWriteMissedBin(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void readWriteMissedBinByContainerType_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		ReadWriteMissedBinByContainerTypeDocument mockDocumentInput = mock(ReadWriteMissedBinByContainerTypeDocument.class);

		ReadWriteMissedBinByContainerType mockInput = mock(ReadWriteMissedBinByContainerType.class);

		ReadWriteMissedBinByContainerTypeResponseDocument mockResponse = mock(ReadWriteMissedBinByContainerTypeResponseDocument.class);

		when(mockObjectFactoryService.getReadWriteMissedBinByContainerTypeDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getReadWriteMissedBinByContainerTypeInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.readWriteMissedBinByContainerType(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_readWriteMissedBinByContainerType()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		ReadWriteMissedBinByContainerTypeResponseDocument response = webaspxRequestService.readWriteMissedBinByContainerType(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);

		verify(mockInput, times(1)).setContainerType(PARAM3);

		verify(mockInput, times(1)).setMissedYN(PARAM4);
		verify(mockInput, times(1)).setMissedDateDDsMMsYY(PARAM5);
		verify(mockDocumentInput, times(1)).setReadWriteMissedBinByContainerType(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void removeGardenSubscription_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		GardenRemoveSubscriptionDocument mockDocumentInput = mock(GardenRemoveSubscriptionDocument.class);

		GardenRemoveSubscription mockInput = mock(GardenRemoveSubscription.class);

		GardenRemoveSubscriptionResponseDocument mockResponse = mock(GardenRemoveSubscriptionResponseDocument.class);

		when(mockObjectFactoryService.getGardenRemoveSubscriptionDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getGardenRemoveSubscriptionInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.garden_RemoveSubscription(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_removeGardenSubscription()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		GardenRemoveSubscriptionResponseDocument response = webaspxRequestService.removeGardenSubscription(COMPANY_ID, PARAM1, PARAM2, PARAM3);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);

		verify(mockInput, times(1)).setEndDateDdsMMsyyyy(PARAM3);
		verify(mockDocumentInput, times(1)).setGardenRemoveSubscription(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void showAdditionalBins_WhenNoError_ThenReturn_GetRoundCalendarForUPRNResponseDocumentt() throws Exception {

		ShowAdditionalBinsDocument mockDocumentInput = mock(ShowAdditionalBinsDocument.class);

		ShowAdditionalBins mockInput = mock(ShowAdditionalBins.class);

		ShowAdditionalBinsResponseDocument mockResponse = mock(ShowAdditionalBinsResponseDocument.class);

		when(mockObjectFactoryService.getShowAdditionalBinsDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getShowAdditionalBinsInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.showAdditionalBins(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_showAdditionalBins()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		ShowAdditionalBinsResponseDocument response = webaspxRequestService.showAdditionalBins(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setService(PARAM2);
		verify(mockInput, times(1)).setStartDateDDsMMsYYYY(PARAM3);
		verify(mockInput, times(1)).setEndDateDDsMMsYYYY(PARAM4);

		verify(mockDocumentInput, times(1)).setShowAdditionalBins(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void updateAssisted_WhenNoError_ThenReturn_UpdateAssistedResponseDocument() throws Exception {

		UpdateAssistedDocument mockDocumentInput = mock(UpdateAssistedDocument.class);

		UpdateAssisted mockInput = mock(UpdateAssisted.class);

		UpdateAssistedResponseDocument mockResponse = mock(UpdateAssistedResponseDocument.class);

		when(mockObjectFactoryService.getUpdateAssistedDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getUpdateAssistedInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.updateAssisted(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_updateAssisted()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		UpdateAssistedResponseDocument response = webaspxRequestService.updateAssisted(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setAssistedYN01234(PARAM3);
		verify(mockInput, times(1)).setAssistedStartDateDDsMMsYYYY(PARAM4);
		verify(mockInput, times(1)).setAssistedEndDateDDsMMsYYYY(PARAM5);
		verify(mockInput, times(1)).setAssistedLocation(PARAM6);

		verify(mockDocumentInput, times(1)).setUpdateAssisted(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void updateBin_WhenNoError_ThenReturn_UpdateAssistedResponseDocument() throws Exception {

		BinUpdateDocument mockDocumentInput = mock(BinUpdateDocument.class);

		BinUpdate mockInput = mock(BinUpdate.class);

		BinUpdateResponseDocument mockResponse = mock(BinUpdateResponseDocument.class);

		when(mockObjectFactoryService.getBinUpdateDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getBinUpdateInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.binUpdate(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_updateBin()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		BinUpdateResponseDocument response = webaspxRequestService.updateBin(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM5, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);

		verify(mockInput, times(1)).setBinID(PARAM3);
		verify(mockInput, times(1)).setBinType(PARAM4);
		verify(mockInput, times(1)).setPayRef(PARAM5);
		verify(mockInput, times(1)).setDeliverYN(PARAM6);
		verify(mockInput, times(1)).setCollectYN(PARAM7);
		verify(mockInput, times(1)).setStartDateDDsMMsYYYY(PARAM8);
		verify(mockInput, times(1)).setEndDateDDsMMsYYYY(PARAM9);
		verify(mockInput, times(1)).setReportedDateDDsMMsYYYY(PARAM10);
		verify(mockInput, times(1)).setCompletedDateDDsMMsYYYY(PARAM11);
		verify(mockInput, times(1)).setLeaveBlanksAsIsYN(PARAM12);

		verify(mockDocumentInput, times(1)).setBinUpdate(mockInput);

		assertEquals(response, mockResponse);

	}

	@Test
	public void updateCommentsForIncab_WhenNoError_ThenReturn_UpdateAssistedResponseDocument() throws Exception {

		CommentsForIncabUpdateDocument mockDocumentInput = mock(CommentsForIncabUpdateDocument.class);

		CommentsForIncabUpdate mockInput = mock(CommentsForIncabUpdate.class);

		CommentsForIncabUpdateResponseDocument mockResponse = mock(CommentsForIncabUpdateResponseDocument.class);

		when(mockObjectFactoryService.getCommentsForIncabUpdateDocumentInput()).thenReturn(mockDocumentInput);

		when(mockObjectFactoryService.getCommentsForIncabUpdateInput()).thenReturn(mockInput);

		when(mockWSCollExternalStub.commentsForIncabUpdate(mockDocumentInput)).thenReturn(mockResponse);

		when(mockConfiguration.userName()).thenReturn(userName);

		when(mockConfiguration.password_updateCommentsForIncab()).thenReturn(password);

		when(mockConfiguration.userNamePassword()).thenReturn(userNamePassword);

		CommentsForIncabUpdateResponseDocument response = webaspxRequestService.updateCommentsForIncab(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4);

		verify(mockInput, times(1)).setUsername(userName);
		verify(mockInput, times(1)).setUsernamePassword(userNamePassword);
		verify(mockInput, times(1)).setWebServicePassword(password);

		verify(mockInput, times(1)).setCouncil(PARAM1);
		verify(mockInput, times(1)).setUPRN(PARAM2);
		verify(mockInput, times(1)).setNewComment(PARAM3);
		verify(mockInput, times(1)).setAppendOrReplace(PARAM4);

		verify(mockDocumentInput, times(1)).setCommentsForIncabUpdate(mockInput);

		assertEquals(response, mockResponse);

	}

}
