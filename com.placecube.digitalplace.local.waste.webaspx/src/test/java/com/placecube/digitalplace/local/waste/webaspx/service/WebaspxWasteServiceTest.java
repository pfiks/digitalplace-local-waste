package com.placecube.digitalplace.local.waste.webaspx.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BinCollection.BinCollectionBuilder;
import com.placecube.digitalplace.local.waste.webaspx.configuration.WebaspxConfiguration;
import com.placecube.digitalplace.local.waste.webaspx.constants.WebaspxWasteConstants;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EntityUtils.class, JSONFactoryUtil.class, LocaleUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WebaspxWasteServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String WEB_SERVICE_URL = "SERVICE_NAME";
	private static final String USERNAME = "USERNAME";
	private static final String USER_NAME_PASSWORD = "USER_NAME_PASSWORD";

	@InjectMocks
	private WebaspxWasteService webaspxWasteService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private ObjectFactoryService mockObjectFactoryService;

	@Mock
	private BinCollection mockBinCollection;

	@Mock
	private BinCollectionBuilder mockBinCollectionBuilder;

	@Mock
	private WebaspxConfiguration webaspxConfiguration;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONArray mockJSONDatesArray;

	@Mock
	private JSONObject mockJSONObject;

	@Captor
	private ArgumentCaptor<Map<Locale, String>> mapCaptor;

	@Before
	public void activateSetup() {
		mockStatic(LocaleUtil.class);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenErrorGettingConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WebaspxConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		webaspxWasteService.getConfiguration(COMPANY_ID);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsConfiguration() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WebaspxConfiguration.class, COMPANY_ID)).thenReturn(webaspxConfiguration);

		WebaspxConfiguration result = webaspxWasteService.getConfiguration(COMPANY_ID);

		assertEquals(webaspxConfiguration, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getWebServiceUrl_WhenWebServiceUrlIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(webaspxConfiguration.webServiceUrl()).thenReturn(StringPool.BLANK);

		webaspxWasteService.getWebServiceUrl(webaspxConfiguration);
	}

	@Test
	public void getWebServiceUrl_WhenWebServiceUrlIsNotBlank_ThenReturnsWebServiceUrl() throws Exception {
		when(webaspxConfiguration.webServiceUrl()).thenReturn(WEB_SERVICE_URL);

		String result = webaspxWasteService.getWebServiceUrl(webaspxConfiguration);

		assertEquals(WEB_SERVICE_URL, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getUserNamePassword_WhenUserNamePasswordIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(webaspxConfiguration.userNamePassword()).thenReturn(StringPool.BLANK);

		webaspxWasteService.getUserNamePassword(webaspxConfiguration);
	}

	@Test
	public void getUserNamePassword_WhenUserNamePasswordIsNotBlank_ThenReturnsUserNamePassword() throws Exception {
		when(webaspxConfiguration.userNamePassword()).thenReturn(USER_NAME_PASSWORD);

		String result = webaspxWasteService.getUserNamePassword(webaspxConfiguration);

		assertEquals(USER_NAME_PASSWORD, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getUsername_WhenUsernameIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(webaspxConfiguration.userName()).thenReturn(StringPool.BLANK);

		webaspxWasteService.getUserName(webaspxConfiguration);
	}

	@Test
	public void getUsername_WhenUsernameIsNotBlank_ThenReturnsUsername() throws Exception {
		when(webaspxConfiguration.userName()).thenReturn(USERNAME);

		String result = webaspxWasteService.getUserName(webaspxConfiguration);

		assertEquals(USERNAME, result);
	}

	@Test
	public void createBinCollection_WhenNameContainsType_ThenReturnsBinCollection() {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT);
		String name = "Bin: General Waste then Weekly";
		String type = "general";
		LocalDate collectionDate = LocalDate.now();
		LocalDate followingCollectionDate = collectionDate.plusWeeks(1);

		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject);
		when(mockJSONObject.getString("Bin_number")).thenReturn(type);
		when(mockJSONObject.getJSONArray("CollectionDate")).thenReturn(mockJSONDatesArray);
		when(LocaleUtil.getDefault()).thenReturn(Locale.UK);

		when(mockJSONDatesArray.get(0)).thenReturn(dateTimeFormatter.format(collectionDate));
		when(mockJSONDatesArray.get(1)).thenReturn(dateTimeFormatter.format(followingCollectionDate));

		when(mockObjectFactoryService.createBinCollectionBuilder(eq("Bin"), eq(collectionDate), any(Map.class))).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.frequency("Weekly")).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.followingCollectionDate(followingCollectionDate)).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.formattedCollectionDate(dateTimeFormatter.format(followingCollectionDate))).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.build()).thenReturn(mockBinCollection);

		Optional<BinCollection> result = webaspxWasteService.createBinCollection(name, mockJSONArray, type);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockBinCollection));
	}

	@Test
	public void createBinCollection_WhenNameContainsType_ThenCreatesBinCollectionWithFrequencyAndCollectionDates() {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT);
		String name = "Bin: General Waste then Weekly";
		String type = "general";
		LocalDate collectionDate = LocalDate.now();
		LocalDate followingCollectionDate = collectionDate.plusWeeks(1);

		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject);
		when(mockJSONObject.getString("Bin_number")).thenReturn(type);
		when(mockJSONObject.getJSONArray("CollectionDate")).thenReturn(mockJSONDatesArray);
		when(LocaleUtil.getDefault()).thenReturn(Locale.UK);

		when(mockJSONDatesArray.get(0)).thenReturn(dateTimeFormatter.format(collectionDate));
		when(mockJSONDatesArray.get(1)).thenReturn(dateTimeFormatter.format(followingCollectionDate));

		when(mockObjectFactoryService.createBinCollectionBuilder(eq("Bin"), eq(collectionDate), any(Map.class))).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.frequency("Weekly")).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.followingCollectionDate(followingCollectionDate)).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.formattedCollectionDate(dateTimeFormatter.format(followingCollectionDate))).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.build()).thenReturn(mockBinCollection);

		webaspxWasteService.createBinCollection(name, mockJSONArray, type);

		InOrder inOrder = inOrder(mockBinCollectionBuilder);
		inOrder.verify(mockBinCollectionBuilder, times(1)).frequency("Weekly");
		inOrder.verify(mockBinCollectionBuilder, times(1)).followingCollectionDate(followingCollectionDate);
		inOrder.verify(mockBinCollectionBuilder, times(1)).formattedCollectionDate(dateTimeFormatter.format(followingCollectionDate));
		inOrder.verify(mockBinCollectionBuilder, times(1)).build();
	}

	@Test
	public void createBinCollection_WhenNameContainsType_ThenBinCollectionBuilderIsCreatedWithLocaleMap() {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT);
		String name = "Bin: General Waste then Weekly";
		String type = "general";
		LocalDate collectionDate = LocalDate.now();
		LocalDate followingCollectionDate = collectionDate.plusWeeks(1);

		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject);
		when(mockJSONObject.getString("Bin_number")).thenReturn(type);
		when(mockJSONObject.getJSONArray("CollectionDate")).thenReturn(mockJSONDatesArray);
		when(LocaleUtil.getDefault()).thenReturn(Locale.UK);

		when(mockJSONDatesArray.get(0)).thenReturn(dateTimeFormatter.format(collectionDate));
		when(mockJSONDatesArray.get(1)).thenReturn(dateTimeFormatter.format(followingCollectionDate));

		when(mockObjectFactoryService.createBinCollectionBuilder(eq("Bin"), eq(collectionDate), mapCaptor.capture())).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.frequency("Weekly")).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.followingCollectionDate(followingCollectionDate)).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.formattedCollectionDate(dateTimeFormatter.format(followingCollectionDate))).thenReturn(mockBinCollectionBuilder);
		when(mockBinCollectionBuilder.build()).thenReturn(mockBinCollection);

		webaspxWasteService.createBinCollection(name, mockJSONArray, type);

		Map<Locale, String> capturedMap = mapCaptor.getValue();

		assertThat(capturedMap.get(Locale.UK), equalTo(type));
	}

	@Test
	public void createBinCollection_WhenNameDoesNotContainsType_ThenReturnsEmptyOptional() {
		String name = "Bin: General Waste then Weekly";
		String type = "general";

		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject);
		when(mockJSONObject.getString("Bin_number")).thenReturn("another type");

		Optional<BinCollection> result = webaspxWasteService.createBinCollection(name, mockJSONArray, type);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void createBinCollection_WhenJSONArrayIsEmpty_ThenReturnsEmptyOptional() {
		String name = "Bin: General Waste then Weekly";
		String type = "general";

		when(mockJSONArray.length()).thenReturn(0);

		Optional<BinCollection> result = webaspxWasteService.createBinCollection(name, mockJSONArray, type);

		assertThat(result.isPresent(), equalTo(false));
	}
}
