package com.placecube.digitalplace.local.waste.webaspx.service;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.local.waste.model.BinCollection;

@Component(immediate = true, service = JsonResponseParserService.class)
public class JsonResponseParserService {

	private static final Set<String> EXCLUDE_FIELDS = new HashSet<>();
	private static Gson gson = getGson();

	private static ObjectMapper jsonMapper = new ObjectMapper();

	@Reference
	private WebaspxWasteService webaspxWasteService;

	public Optional<JSONObject> getRoundForUPRNRowJSONObject(JSONObject json) {
		JSONObject jsonRoundForUPRNResult = json.getJSONObject("getRoundForUPRNResult");
		Optional<JSONObject> jsonRoundForUPRNResultRows = getRowsJSONObject(jsonRoundForUPRNResult);

		return Optional.ofNullable(jsonRoundForUPRNResultRows.isPresent() ? jsonRoundForUPRNResultRows.get().getJSONObject("Row") : null);
	}

	public Optional<JSONArray> getRoundAndBinForUPRNJSONObject(JSONObject json) {
		JSONObject jsonRoundAndBinDatesResult = json.getJSONObject("getRoundAndBinInfoForUPRNForNewAdjustedDatesResult");
		Optional<JSONObject> jsonRoundAndBinDatesResultRows = getRowsJSONObject(jsonRoundAndBinDatesResult);

		return Optional.ofNullable(jsonRoundAndBinDatesResultRows.isPresent() ? jsonRoundAndBinDatesResultRows.get().getJSONArray("Bin") : null);
	}

	public Optional<BinCollection> getBinCollectionFromJSONRoundAndBin(String name, JSONArray jsonRoundAndBin) {
		Optional<BinCollection> binCollection = Optional.empty();

		if (name.split(":")[0].equalsIgnoreCase("refuse")) {
			binCollection = webaspxWasteService.createBinCollection(name, jsonRoundAndBin, "rubbish");
		} else if (name.split(":")[0].equalsIgnoreCase("garden")) {
			binCollection = webaspxWasteService.createBinCollection(name, jsonRoundAndBin, "garden");
		} else if (name.split(":")[0].equalsIgnoreCase("recycling")) {
			binCollection = webaspxWasteService.createBinCollection(name, jsonRoundAndBin, "recycling");
		}

		return binCollection;
	}

	public String toJson(Object object) {
		try {
			XmlMapper xmlMapper = new XmlMapper();
			JsonNode node = xmlMapper.readTree(object.toString().getBytes());
			return jsonMapper.writeValueAsString(node);
		} catch (IOException e) {
			return toJson2(object);
		}
	}

	public String toJson2(Object object) {
		return gson.toJson(object);
	}

	private Optional<JSONObject> getRowsJSONObject(JSONObject json) {
		return Optional.ofNullable(json != null ? json.getJSONObject("Rows") : null);

	}

	private static ExclusionStrategy getExclusionStrategy() {
		return new ExclusionStrategy() {

			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				return false;
			}

			@Override
			public boolean shouldSkipField(FieldAttributes field) {
				if (EXCLUDE_FIELDS.contains(field.getName())) {
					return true;
				}
				return false;
			}
		};
	}

	private static Gson getGson() {
		EXCLUDE_FIELDS.add("__hashCodeCalc");
		EXCLUDE_FIELDS.add("__equalsCalc");
		EXCLUDE_FIELDS.add("parallelLockMap");
		GsonBuilder gsonBuilder = new GsonBuilder().setExclusionStrategies(getExclusionStrategy());
		return gsonBuilder.create();
	}

}
