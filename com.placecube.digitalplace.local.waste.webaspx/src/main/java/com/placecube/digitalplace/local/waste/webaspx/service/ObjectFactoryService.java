package com.placecube.digitalplace.local.waste.webaspx.service;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.local.waste.model.BinCollection.BinCollectionBuilder;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestDocument.AAHelloWorldStringTest;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteDocument.BinDelete;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertDocument.BinInsert;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateDocument.BinUpdate;
import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarDocument.CachedCalendar;
import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedDocument.CheckAssisted;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingDocument.CommentsForIncabGetExisting;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateDocument.CommentsForIncabUpdate;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceDocument.CountBinsForService;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsDocument.CountSubscriptions;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressDocument.GardenChangeAddress;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionDocument.GardenRemoveSubscription;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionDocument.GardenSubscription;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeDocument.GetAddressFromPostcode;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNDocument.GetAddressOrUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsDocument.GetAllRoundDetails;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateDocument.GetAllUPRNsForDate;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersDocument.GetAvailableContainers;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyDocument.GetAvailableContainersForClientOrProperty;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsDocument.GetAvailableRounds;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceDocument.GetBinDetailsForService;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeDocument.GetInCabIssueTextAndIssueCode;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesDocument.GetIssues;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNDocument.GetIssuesForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionDocument.GetLastTruckPosition;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsDocument.GetLatestTruckPositions;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesDocument.GetNoCollectionDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNDocument.GetRoundCalendarForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNDocument.GetRoundChangesInDateRangeByRoundOrUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNDocument.GetRoundForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceDocument.GetRoundNameForUPRNService;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayDocument.GetTrucksOutToday;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsDocument.LLPGXtraUpdateGetAvailableFieldDetails;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetUPRNForDBFieldDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField;
import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsDocument.LargeHouseholds;
import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedDocument.LastCollected;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesDocument.QueryBinEndDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeDocument.QueryBinOnType;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeDocument.ReadWriteMissedBinByContainerType;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinDocument.ReadWriteMissedBin;
import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsDocument.ShowAdditionalBins;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedDocument.UpdateAssisted;

@Component(immediate = true, service = ObjectFactoryService.class)
public class ObjectFactoryService {

	public AAHelloWorldStringTestDocument aaHelloWorldStringTestDocumentInput() {

		return AAHelloWorldStringTestDocument.Factory.newInstance();
	}

	public AAHelloWorldStringTest aaHelloWorldStringTestInput() {

		return AAHelloWorldStringTest.Factory.newInstance();
	}

	public AAHelloWorldXMLTestDocument aaHelloWorldXMLTestDocumentInput() {

		return AAHelloWorldXMLTestDocument.Factory.newInstance();
	}

	public AAHelloWorldXMLTest aaHelloWorldXMLTestInput() {

		return AAHelloWorldXMLTest.Factory.newInstance();
	}

	public CountBinsForServiceDocument countBinsForServiceDocumentInput() {

		return CountBinsForServiceDocument.Factory.newInstance();
	}

	public CountBinsForService countBinsForServiceInput() {

		return CountBinsForService.Factory.newInstance();
	}

	public GetAddressOrUPRNDocument getAddressOrUPRNDocumentInput() {

		return GetAddressOrUPRNDocument.Factory.newInstance();
	}

	public GetAddressOrUPRN getAddressOrUPRNInput() {

		return GetAddressOrUPRN.Factory.newInstance();
	}

	public GetAllIssuesForIssueTypeDocument getAllIssuesForIssueTypeDocumentInput() {
		return GetAllIssuesForIssueTypeDocument.Factory.newInstance();
	}

	public GetAllIssuesForIssueType getAllIssuesForIssueTypeInput() {

		return GetAllIssuesForIssueType.Factory.newInstance();
	}

	public GetAllRoundDetails getAllRoundDetailsResponseDocumentInput() {

		return GetAllRoundDetails.Factory.newInstance();
	}

	public GetAllUPRNsForDate getAllUPRNsForDateInput() {

		return GetAllUPRNsForDate.Factory.newInstance();
	}

	public GetAvailableContainersDocument getAvailableContainersDocumentInput() {

		return GetAvailableContainersDocument.Factory.newInstance();
	}

	public GetAvailableContainersForClientOrPropertyDocument getAvailableContainersForClientOrPropertyDocumentInput() {

		return GetAvailableContainersForClientOrPropertyDocument.Factory.newInstance();
	}

	public GetAvailableContainersForClientOrProperty getAvailableContainersForClientOrPropertyInput() {

		return GetAvailableContainersForClientOrProperty.Factory.newInstance();
	}

	public GetAvailableContainers getAvailableContainersInput() {

		return GetAvailableContainers.Factory.newInstance();
	}

	public GetAvailableRoundsDocument getAvailableRoundsDocumentInput() {

		return GetAvailableRoundsDocument.Factory.newInstance();
	}

	public GetAvailableRounds getAvailableRoundsInput() {

		return GetAvailableRounds.Factory.newInstance();
	}

	public BinDeleteDocument getBinDeleteDocumentInput() {

		return BinDeleteDocument.Factory.newInstance();
	}

	public BinDelete getBinDeleteInput() {

		return BinDelete.Factory.newInstance();
	}

	public GetBinDetailsForServiceDocument getBinDetailsForServiceDocumentInput() {

		return GetBinDetailsForServiceDocument.Factory.newInstance();
	}

	public GetBinDetailsForService getBinDetailsForServiceInput() {

		return GetBinDetailsForService.Factory.newInstance();
	}

	public BinInsertDocument getBinInsertDocumentInput() {

		return BinInsertDocument.Factory.newInstance();
	}

	public BinInsert getBinInsertInput() {

		return BinInsert.Factory.newInstance();
	}

	public BinUpdateDocument getBinUpdateDocumentInput() {

		return BinUpdateDocument.Factory.newInstance();
	}

	public BinUpdate getBinUpdateInput() {

		return BinUpdate.Factory.newInstance();
	}

	public CachedCalendarDocument getCachedCalendarDocumentInput() {

		return CachedCalendarDocument.Factory.newInstance();
	}

	public CachedCalendar getCachedCalendarInput() {

		return CachedCalendar.Factory.newInstance();
	}

	public CheckAssistedDocument getCheckAssistedDocumentInput() {

		return CheckAssistedDocument.Factory.newInstance();
	}

	public CheckAssisted getCheckAssistedInput() {

		return CheckAssisted.Factory.newInstance();
	}

	public CommentsForIncabGetExistingDocument getCommentsForIncabGetExistingDocumentInput() {

		return CommentsForIncabGetExistingDocument.Factory.newInstance();
	}

	public CommentsForIncabGetExisting getCommentsForIncabGetExistingInput() {

		return CommentsForIncabGetExisting.Factory.newInstance();
	}

	public CommentsForIncabUpdateDocument getCommentsForIncabUpdateDocumentInput() {

		return CommentsForIncabUpdateDocument.Factory.newInstance();
	}

	public CommentsForIncabUpdate getCommentsForIncabUpdateInput() {

		return CommentsForIncabUpdate.Factory.newInstance();
	}

	public CountSubscriptionsDocument getCountSubscriptionsDocumentInput() {

		return CountSubscriptionsDocument.Factory.newInstance();
	}

	public CountSubscriptions getCountSubscriptionsInput() {

		return CountSubscriptions.Factory.newInstance();
	}

	public GardenChangeAddressDocument getGardenChangeAddressDocumentInput() {

		return GardenChangeAddressDocument.Factory.newInstance();
	}

	public GardenChangeAddress getGardenChangeAddressInput() {

		return GardenChangeAddress.Factory.newInstance();
	}

	public GardenRemoveSubscriptionDocument getGardenRemoveSubscriptionDocumentInput() {

		return GardenRemoveSubscriptionDocument.Factory.newInstance();
	}

	public GardenRemoveSubscription getGardenRemoveSubscriptionInput() {

		return GardenRemoveSubscription.Factory.newInstance();
	}

	public GardenSubscribersDeliveriesCollectionsDocument getGardenSubscribersDeliveriesCollectionsDocumentInput() {

		return GardenSubscribersDeliveriesCollectionsDocument.Factory.newInstance();
	}

	public GardenSubscribersDeliveriesCollections getGardenSubscribersDeliveriesCollectionsInput() {

		return GardenSubscribersDeliveriesCollections.Factory.newInstance();
	}

	public GardenSubscriptionDocument getGardenSubscriptionDocumentInput() {

		return GardenSubscriptionDocument.Factory.newInstance();
	}

	public GardenSubscription getGardenSubscriptionInput() {

		return GardenSubscription.Factory.newInstance();
	}

	public GetAddressFromPostcodeDocument getGetAddressFromPostcodeDocumentInput() {

		return GetAddressFromPostcodeDocument.Factory.newInstance();
	}

	public GetAddressFromPostcode getGetAddressFromPostcodeInput() {

		return GetAddressFromPostcode.Factory.newInstance();
	}

	public GetAllRoundDetailsDocument getGetAllRoundDetailsDocumentInput() {

		return GetAllRoundDetailsDocument.Factory.newInstance();
	}

	public GetAllUPRNsForDateDocument getGetAllUPRNsForDateDocumentInput() {

		return GetAllUPRNsForDateDocument.Factory.newInstance();
	}

	public GetInCabIssueTextAndIssueCodeDocument getGetInCabIssueTextAndIssueCodeDocumentInput() {

		return GetInCabIssueTextAndIssueCodeDocument.Factory.newInstance();
	}

	public GetPotentialCollectionDatesForNewGardenSubscriber getGetPotentialCollectionDatesForNewGardenSubscriberInput() {

		return GetPotentialCollectionDatesForNewGardenSubscriber.Factory.newInstance();
	}

	public GetRoundChangesInDateRangeByRoundOrUPRNDocument getGetRoundChangesInDateRangeByRoundOrUPRNDocumentInput() {

		return GetRoundChangesInDateRangeByRoundOrUPRNDocument.Factory.newInstance();
	}

	public GetInCabIssueTextAndIssueCode getInCabIssueTextAndIssueCodeInput() {

		return GetInCabIssueTextAndIssueCode.Factory.newInstance();
	}

	public GetIssuesAndCollectionStatusForUPRNDocument getIssuesAndCollectionStatusForUPRNDocumentInput() {

		return GetIssuesAndCollectionStatusForUPRNDocument.Factory.newInstance();
	}

	public GetIssuesAndCollectionStatusForUPRN getIssuesAndCollectionStatusForUPRNInput() {

		return GetIssuesAndCollectionStatusForUPRN.Factory.newInstance();
	}

	public GetIssuesDocument getIssuesDocumentInput() {

		return GetIssuesDocument.Factory.newInstance();
	}

	public GetIssuesForUPRNDocument getIssuesForUPRNDocumentInput() {

		return GetIssuesForUPRNDocument.Factory.newInstance();
	}

	public GetIssuesForUPRN getIssuesForUPRNInput() {

		return GetIssuesForUPRN.Factory.newInstance();
	}

	public GetIssues getIssuesInput() {

		return GetIssues.Factory.newInstance();
	}

	public LargeHouseholdsDocument getLargeHouseholdsDocumentInput() {

		return LargeHouseholdsDocument.Factory.newInstance();
	}

	public LargeHouseholds getLargeHouseholdsInput() {

		return LargeHouseholds.Factory.newInstance();
	}

	public GetLastTruckPositionDocument getLastTruckPositionDocumentInput() {

		return GetLastTruckPositionDocument.Factory.newInstance();
	}

	public GetLastTruckPosition getLastTruckPositionInput() {

		return GetLastTruckPosition.Factory.newInstance();
	}

	public GetLatestTruckPositionsDocument getLatestTruckPositionsDocumentInput() {

		return GetLatestTruckPositionsDocument.Factory.newInstance();
	}

	public GetLatestTruckPositions getLatestTruckPositionsInput() {

		return GetLatestTruckPositions.Factory.newInstance();
	}

	public LLPGXtraGetUPRNCurrentValuesForDBFieldDocument getLLPGXtraGetUPRNCurrentValuesForDBFieldDocumentInput() {

		return LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.Factory.newInstance();
	}

	public LLPGXtraGetUPRNCurrentValuesForDBField getLLPGXtraGetUPRNCurrentValuesForDBFieldInput() {

		return LLPGXtraGetUPRNCurrentValuesForDBField.Factory.newInstance();
	}

	public LLPGXtraUpdateGetAvailableFieldDetailsDocument getLLPGXtraUpdateGetAvailableFieldDetailsDocumentInput() {

		return LLPGXtraUpdateGetAvailableFieldDetailsDocument.Factory.newInstance();
	}

	public LLPGXtraUpdateGetAvailableFieldDetails getLLPGXtraUpdateGetAvailableFieldDetailsInput() {

		return LLPGXtraUpdateGetAvailableFieldDetails.Factory.newInstance();
	}

	public LLPGXtraUpdateGetUPRNForDBFieldDocument getLLPGXtraUpdateGetUPRNForDBFieldDocumentInput() {

		return LLPGXtraUpdateGetUPRNForDBFieldDocument.Factory.newInstance();
	}

	public LLPGXtraUpdateGetUPRNForDBField getLLPGXtraUpdateGetUPRNForDBFieldInput() {

		return LLPGXtraUpdateGetUPRNForDBField.Factory.newInstance();
	}

	public GetNoCollectionDatesDocument getNoCollectionDatesDocumentInput() {

		return GetNoCollectionDatesDocument.Factory.newInstance();
	}

	public GetNoCollectionDates getNoCollectionDatesServiceInput() {

		return GetNoCollectionDates.Factory.newInstance();
	}

	public GetPotentialCollectionDatesForNewGardenSubscriberDocument getPotentialCollectionDatesForNewGardenSubscriberDocumentInput() {

		return GetPotentialCollectionDatesForNewGardenSubscriberDocument.Factory.newInstance();
	}

	public QueryBinEndDatesDocument getQueryBinEndDatesDocumentInput() {

		return QueryBinEndDatesDocument.Factory.newInstance();
	}

	public QueryBinEndDates getQueryBinEndDatesInput() {

		return QueryBinEndDates.Factory.newInstance();
	}

	public QueryBinOnTypeDocument getQueryBinOnTypeDocumentInput() {

		return QueryBinOnTypeDocument.Factory.newInstance();
	}

	public QueryBinOnType getQueryBinOnTypeInput() {

		return QueryBinOnType.Factory.newInstance();
	}

	public ReadWriteMissedBinByContainerTypeDocument getReadWriteMissedBinByContainerTypeDocumentInput() {

		return ReadWriteMissedBinByContainerTypeDocument.Factory.newInstance();
	}

	public ReadWriteMissedBinByContainerType getReadWriteMissedBinByContainerTypeInput() {

		return ReadWriteMissedBinByContainerType.Factory.newInstance();
	}

	public ReadWriteMissedBinDocument getReadWriteMissedBinDocumentInput() {

		return ReadWriteMissedBinDocument.Factory.newInstance();
	}

	public GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument getRoundAndBinInfoForUPRNForNewAdjustedDatesDocumentInput() {

		return GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument.Factory.newInstance();
	}

	public GetRoundAndBinInfoForUPRNForNewAdjustedDates getRoundAndBinInfoForUPRNForNewAdjustedDatesServiceInput() {

		return GetRoundAndBinInfoForUPRNForNewAdjustedDates.Factory.newInstance();
	}

	public GetRoundCalendarForUPRNDocument getRoundCalendarForUPRNDocumentInput() {

		return GetRoundCalendarForUPRNDocument.Factory.newInstance();
	}

	public GetRoundCalendarForUPRN getRoundCalendarForUPRNInput() {

		return GetRoundCalendarForUPRN.Factory.newInstance();
	}

	public GetRoundChangesInDateRangeByRoundOrUPRN getRoundChangesInDateRangeByRoundOrUPRNInput() {

		return GetRoundChangesInDateRangeByRoundOrUPRN.Factory.newInstance();
	}

	public GetRoundForUPRNDocument getRoundForUPRNDocumentInput() {

		return GetRoundForUPRNDocument.Factory.newInstance();
	}

	public GetRoundForUPRN getRoundForUPRNInput() {

		return GetRoundForUPRN.Factory.newInstance();
	}

	public GetRoundNameForUPRNServiceDocument getRoundNameForUPRNServiceDocumentInput() {

		return GetRoundNameForUPRNServiceDocument.Factory.newInstance();
	}

	public GetRoundNameForUPRNService getRoundNameForUPRNServiceInput() {

		return GetRoundNameForUPRNService.Factory.newInstance();
	}

	public ShowAdditionalBinsDocument getShowAdditionalBinsDocumentInput() {

		return ShowAdditionalBinsDocument.Factory.newInstance();
	}

	public ShowAdditionalBins getShowAdditionalBinsInput() {

		return ShowAdditionalBins.Factory.newInstance();
	}

	public GetTrucksOutTodayDocument getTrucksOutTodayDocumentInput() {

		return GetTrucksOutTodayDocument.Factory.newInstance();
	}

	public GetTrucksOutToday getTrucksOutTodayInput() {

		return GetTrucksOutToday.Factory.newInstance();
	}

	public UpdateAssistedDocument getUpdateAssistedDocumentInput() {

		return UpdateAssistedDocument.Factory.newInstance();
	}

	public UpdateAssisted getUpdateAssistedInput() {

		return UpdateAssisted.Factory.newInstance();
	}

	public LastCollectedDocument lastCollectedDocumentInput() {

		return LastCollectedDocument.Factory.newInstance();
	}

	public LastCollected lastCollectedInput() {

		return LastCollected.Factory.newInstance();
	}

	public ReadWriteMissedBin readWriteMissedBinInput() {
		return ReadWriteMissedBin.Factory.newInstance();
	}

	public BinCollectionBuilder createBinCollectionBuilder(String name, LocalDate collectionDate, Map<Locale, String> labelMap) {
		return new BinCollectionBuilder(name, collectionDate, labelMap);
	}
}