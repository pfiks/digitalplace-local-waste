package com.placecube.digitalplace.local.waste.webaspx.service;

import java.rmi.RemoteException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.waste.webaspx.axis.WSCollExternalStub;
import com.placecube.digitalplace.local.waste.webaspx.configuration.WebaspxConfiguration;

@Component(immediate = true, service = WebaspxAxisService.class)
public class WebaspxAxisService {

	@Reference
	private WebaspxWasteService webaspxWasteService;

	public WSCollExternalStub getWebaspxAxisService(long companyId) throws ConfigurationException, RemoteException {
		WebaspxConfiguration configuration = webaspxWasteService.getConfiguration(companyId);
		return new WSCollExternalStub(configuration.webServiceUrl());

	}
}
