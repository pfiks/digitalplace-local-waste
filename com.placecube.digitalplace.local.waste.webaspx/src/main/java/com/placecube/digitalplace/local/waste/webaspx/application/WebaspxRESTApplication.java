package com.placecube.digitalplace.local.waste.webaspx.application;

import java.util.Collections;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetUPRNForDBFieldResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.service.JsonResponseParserService;
import com.placecube.digitalplace.local.waste.webaspx.service.WebaspxRequestService;

@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/waste/webaspx", JaxrsWhiteboardConstants.JAX_RS_NAME + "=webaspx.Rest", "oauth2.scopechecker.type=none",
		"auth.verifier.guest.allowed=false", "liferay.access.control.disable=true" }, service = Application.class)
public class WebaspxRESTApplication extends Application {

	private static final Log LOG = LogFactoryUtil.getLog(com.placecube.digitalplace.local.waste.webaspx.application.WebaspxRESTApplication.class);

	@Reference
	private JsonResponseParserService jsonResponseParserService;

	@Reference
	private WebaspxRequestService webaspxRequestService;

	@Activate
	public void activate() {
		LOG.info("Webaspx Rest service activated");
	}

	@POST
	@Path("/GardenSubscription")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String addOrUpdateGardenSubscription(@QueryParam("council") String council, @QueryParam("UPRN ") String uprn, @QueryParam("binsAtProperty") String binsAtProperty,
			@QueryParam("totalNoSubsRequired") String totalNoSubsRequired, @QueryParam("subscriptionStartDate") String subscriptionStartDate,
			@QueryParam("subscriptionEndDate") String subscriptionEndDate, @QueryParam("newPayRef") String newPayRef, @QueryParam("binType") String binType,
			@QueryParam("gardenContainerDeliveryComments") String gardenContainerDeliveryComments, @QueryParam("crmGardenRef") String crmGardenRef, @Context HttpServletRequest request)
			throws Exception {

		GardenSubscriptionResponseDocument response = webaspxRequestService.addOrUpdateGardenSubscription(getCompanyId(request), council, uprn, binsAtProperty, totalNoSubsRequired,
				subscriptionStartDate, subscriptionEndDate, newPayRef, binType, gardenContainerDeliveryComments, crmGardenRef);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/Garden_ChangeAddress")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String changeGardenAddress(@QueryParam("council") String council, @QueryParam("fromUPRN") String fromUPRN, @QueryParam("toUPRN") String toUPRN, @Context HttpServletRequest request)
			throws Exception {

		GardenChangeAddressResponseDocument response = webaspxRequestService.changeGardenAddress(getCompanyId(request), council, fromUPRN, toUPRN);
		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/countBinsForService")
	@Produces(MediaType.APPLICATION_JSON)
	public String countBinsForService(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("BinService") String binService, @Context HttpServletRequest request)
			throws Exception {

		CountBinsForServiceResponseDocument response = webaspxRequestService.countBinsForService(getCompanyId(request), council, uprn, binService);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/countSubscriptions")
	@Produces(MediaType.APPLICATION_JSON)
	public String countSubscriptions(@QueryParam("council") String council, @QueryParam("startDateDDsMMsYYYY") String startDateddsMMsyyyy, @QueryParam("endDateddsMMsyyyy") String endDateddsMMsyyyy,
			@QueryParam("returnUPRNs") String returnUPRNs, @Context HttpServletRequest request) throws Exception {

		CountSubscriptionsResponseDocument response = webaspxRequestService.countSubscriptions(getCompanyId(request), council, startDateddsMMsyyyy, endDateddsMMsyyyy, returnUPRNs);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/BinDelete")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteBin(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("binID") String binID, @Context HttpServletRequest request) throws Exception {

		BinDeleteResponseDocument response = webaspxRequestService.deleteBin(getCompanyId(request), council, uprn, binID);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/AA_HelloWorld_String_Test")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAAHelloWorldStringTest(@QueryParam("testName") String testName, @Context HttpServletRequest request) throws Exception {

		AAHelloWorldStringTestResponseDocument response = webaspxRequestService.getAAHelloWorldStringTest(getCompanyId(request), testName);
		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/AA_HelloWorld_XML_Test")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAAHelloWorldXMLTest(@QueryParam("testName") String testName, @Context HttpServletRequest request) throws Exception {

		AAHelloWorldXMLTestResponseDocument response = webaspxRequestService.getAAHelloWorldXMLTest(getCompanyId(request), testName);
		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getAddressFromPostcode")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAddressFromPostcode(@QueryParam("council") String council, @QueryParam("searchString") String searchString, @Context HttpServletRequest request) throws Exception {

		GetAddressFromPostcodeResponseDocument response = webaspxRequestService.getAddressFromPostcode(getCompanyId(request), council, searchString);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetAddressOrUPRN")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAddressOrUPRN(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("Address") String address, @Context HttpServletRequest request)
			throws Exception {

		GetAddressOrUPRNResponseDocument response = webaspxRequestService.getAddressOrUPRN(getCompanyId(request), council, uprn, address);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getAllIssuesForIssueType")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllIssuesForIssueType(@QueryParam("council") String council, @QueryParam("issueType") String issueType, @QueryParam("startDateDDMMYYYY") String startDateDDMMYYYY,
			@QueryParam("endDateDDMMYYYY") String endDateDDMMYYYY, @QueryParam("digTime") String digTime, @QueryParam("service") String service, @Context HttpServletRequest request) throws Exception {

		GetAllIssuesForIssueTypeResponseDocument response = webaspxRequestService.getAllIssuesForIssueType(getCompanyId(request), council, issueType, startDateDDMMYYYY, endDateDDMMYYYY, digTime,
				service);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetAllRoundDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllRoundDetails(@QueryParam("council") String council, @Context HttpServletRequest request) throws Exception {

		GetAllRoundDetailsResponseDocument response = webaspxRequestService.getAllRoundDetails(getCompanyId(request), council);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetAllUPRNsForDate")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllUPRNsForDate(@QueryParam("council") String council, @QueryParam("dateRequiredDDsMMsYYYY") String dateRequiredDDsMMsYYYY, @Context HttpServletRequest request) throws Exception {

		GetAllUPRNsForDateResponseDocument response = webaspxRequestService.getAllUPRNsForDate(getCompanyId(request), council, dateRequiredDDsMMsYYYY);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetAvailableContainers")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAvailableContainers(@QueryParam("council") String council, @Context HttpServletRequest request) throws Exception {

		GetAvailableContainersResponseDocument response = webaspxRequestService.getAvailableContainers(getCompanyId(request), council);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetAvailableContainersForClientOrProperty")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAvailableContainersForClientOrProperty(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @Context HttpServletRequest request) throws Exception {

		GetAvailableContainersForClientOrPropertyResponseDocument response = webaspxRequestService.getAvailableContainersForClientOrProperty(getCompanyId(request), council, uprn);

		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/GetAvailableRounds")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAvailableRounds(@QueryParam("council") String council, @QueryParam("service3L") String service3L, @Context HttpServletRequest request) throws Exception {

		GetAvailableRoundsResponseDocument response = webaspxRequestService.getAvailableRounds(getCompanyId(request), council, service3L);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetBinDetailsForService")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBinDetailsForService(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("BinService") String binService, @Context HttpServletRequest request)
			throws Exception {

		GetBinDetailsForServiceResponseDocument response = webaspxRequestService.getBinDetailsForService(getCompanyId(request), council, uprn, binService);
		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/CachedCalendar")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCachedCalendar(@QueryParam("council") String council, @QueryParam("splitOutRounds") String splitOutRounds, @Context HttpServletRequest request) throws Exception {

		CachedCalendarResponseDocument response = webaspxRequestService.getCachedCalendar(getCompanyId(request), council, splitOutRounds);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/CheckAssisted")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCheckAssisted(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @Context HttpServletRequest request) throws Exception {

		CheckAssistedResponseDocument response = webaspxRequestService.getCheckAssisted(getCompanyId(request), council, uprn);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/CommentsForIncabGetExisting")
	@Produces(MediaType.APPLICATION_JSON)
	public String getExistingCommentsForIncab(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @Context HttpServletRequest request) throws Exception {

		CommentsForIncabGetExistingResponseDocument response = webaspxRequestService.getExistingCommentsForIncab(getCompanyId(request), council, uprn);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/Garden_Subscribers_Deliveries_Collections")
	@Produces(MediaType.APPLICATION_JSON)
	public String getGardenSubscribersDeliveriesCollections(@QueryParam("council") String council, @QueryParam("subscribersDeliveriesOrCollections ") String subscribersDeliveriesOrCollections,
			@QueryParam("dateToCheckOn_yyyyMMdd") String dateToCheckOn_yyyyMMdd, @Context HttpServletRequest request) throws Exception {

		GardenSubscribersDeliveriesCollectionsResponseDocument response = webaspxRequestService.getGardenSubscribersDeliveriesCollections(getCompanyId(request), council,
				subscribersDeliveriesOrCollections, dateToCheckOn_yyyyMMdd);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetInCabIssueTextAndIssueCode")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInCabIssueTextAndIssueCode(@QueryParam("council") String council, @Context HttpServletRequest request) throws Exception {

		GetInCabIssueTextAndIssueCodeResponseDocument response = webaspxRequestService.getInCabIssueTextAndIssueCode(getCompanyId(request), council);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetIssues")
	@Produces(MediaType.APPLICATION_JSON)
	public String getIssues(@QueryParam("council") String council, @QueryParam("dateReq") String dateReq, @QueryParam("issueStartTime") String issueStartTime,
			@QueryParam("issueEndTime") String issueEndTime, @QueryParam("issueSortField") String issueSortField, @QueryParam("issueSortOrder") String issueSortOrder,
			@QueryParam("issueFilterName") String issueFilterName, @QueryParam("issueFilterType") String issueFilterType, @QueryParam("searchAllDates") String searchAllDates,
			@QueryParam("propNoName") String propNoName, @QueryParam("propStreet") String propStreet, @QueryParam("UPRN") String uprn, @Context HttpServletRequest request) throws Exception {

		GetIssuesResponseDocument response = webaspxRequestService.getIssues(getCompanyId(request), council, dateReq, issueStartTime, issueEndTime, issueSortField, issueSortOrder, issueFilterName,
				issueFilterType, searchAllDates, propNoName, propStreet, uprn);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getIssuesAndCollectionStatusForUPRN")
	@Produces(MediaType.APPLICATION_JSON)
	public String getIssuesAndCollectionStatusForUPRN(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("lastTimeOnly") String lastTimeOnly,
			@QueryParam("startDateyyyyMMdd") String startDateyyyyMMdd, @QueryParam("endDateyyyyMMdd") String endDateyyyyMMdd, @QueryParam("serviceFilter") String serviceFilter,
			@Context HttpServletRequest request) throws Exception {

		GetIssuesAndCollectionStatusForUPRNResponseDocument response = webaspxRequestService.getIssuesAndCollectionStatusForUPRN(getCompanyId(request), council, uprn, lastTimeOnly, startDateyyyyMMdd,
				endDateyyyyMMdd, serviceFilter);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetIssuesForUPRN")
	@Produces(MediaType.APPLICATION_JSON)
	public String getIssuesForUPRN(@QueryParam("council") String council, @QueryParam("dateReq") String dateReq, @QueryParam("UPRN") String uprn, @Context HttpServletRequest request)
			throws Exception {

		GetIssuesForUPRNResponseDocument response = webaspxRequestService.getIssuesForUPRN(getCompanyId(request), council, dateReq, uprn);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/LargeHouseholds")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLargeHouseholds(@QueryParam("council") String council, @QueryParam("binType") String binType, @Context HttpServletRequest request) throws Exception {

		LargeHouseholdsResponseDocument response = webaspxRequestService.getLargeHouseholds(getCompanyId(request), council, binType);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/LastCollected")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLastCollected(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("returnGPS") boolean returnGPS, @Context HttpServletRequest request)
			throws Exception {

		LastCollectedResponseDocument response = webaspxRequestService.getLastCollected(getCompanyId(request), council, uprn, returnGPS);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetLastTruckPosition")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLastTruckPosition(@QueryParam("council") String council, @QueryParam("dateReq") String dateReq, @QueryParam("timeReq") String timeReq, @QueryParam("regReq") String regReq,
			@Context HttpServletRequest request) throws Exception {

		GetLastTruckPositionResponseDocument response = webaspxRequestService.getLastTruckPosition(getCompanyId(request), council, dateReq, timeReq, regReq);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getLatestTruckPositions")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLatestTruckPositions(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @Context HttpServletRequest request) throws Exception {

		GetLatestTruckPositionsResponseDocument response = webaspxRequestService.getLatestTruckPositions(getCompanyId(request), council, uprn);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/LLPGXtraUpdateGetAvailableFieldDetails")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String getLLPGXtraUpdateAvailableFieldDetails(@QueryParam("council") String council, @Context HttpServletRequest request) throws Exception {

		LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument response = webaspxRequestService.getLLPGXtraUpdateAvailableFieldDetails(getCompanyId(request), council);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/LLPGXtraUpdateGetUPRNForDBField")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String getLLPGXtraUpdateUPRNForDBField(@QueryParam("council") String council, @QueryParam("UPRNs") String uprns, @QueryParam("databaseFieldName") String databaseFieldName,
			@QueryParam("newValue") String newValue, @Context HttpServletRequest request) throws Exception {

		LLPGXtraUpdateGetUPRNForDBFieldResponseDocument response = webaspxRequestService.getLLPGXtraUpdateUPRNForDBField(getCompanyId(request), council, uprns, databaseFieldName, newValue);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/LLPGXtraGetUPRNCurrentValuesForDBField")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLLPGXtraUPRNCurrentValuesForDBField(@QueryParam("council") String council, @QueryParam("UPRNs") String uprns, @QueryParam("databaseFieldName") String databaseFieldName,
			@Context HttpServletRequest request) throws Exception {

		LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument response = webaspxRequestService.getLLPGXtraUPRNCurrentValuesForDBField(getCompanyId(request), council, uprns, databaseFieldName);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getNoCollectionDates")
	@Produces(MediaType.APPLICATION_JSON)
	public String getNoCollectionDates(@QueryParam("council") String council, @QueryParam("AllService3LRoundnameUPRN") String allService3LRoundnameuprn,
			@QueryParam("AllService3LRoundnameUPRNValue") String allService3LRoundnameUPRNValue, @QueryParam("startDateDDsMMsYYYY") String startDateDDsMMsYYYY,
			@QueryParam("endDateDDsMMsYYYY") String endDateDDsMMsYYYY, @Context HttpServletRequest request) throws Exception {

		GetNoCollectionDatesResponseDocument response = webaspxRequestService.getNoCollectionDates(getCompanyId(request), council, allService3LRoundnameuprn, allService3LRoundnameUPRNValue,
				startDateDDsMMsYYYY, endDateDDsMMsYYYY);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getPotentialCollectionDatesForNewGardenSubscriber")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPotentialCollectionDatesForNewGardenSubscriber(@QueryParam("council") String council, @QueryParam("fromUPRN") String fromuprn,
			@QueryParam("startDate_ddsMMsyyyy") String startDate_ddsMMsyyyy, @QueryParam("endDate_ddsMMsyyyy") String endDate_ddsMMsyyyy, @Context HttpServletRequest request) throws Exception {

		GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument response = webaspxRequestService.getPotentialCollectionDatesForNewGardenSubscriber(getCompanyId(request), council, fromuprn,
				startDate_ddsMMsyyyy, endDate_ddsMMsyyyy);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/QueryBinEndDates")
	@Produces(MediaType.APPLICATION_JSON)
	public String getQueryBinEndDates(@QueryParam("council") String council, @QueryParam("startDateddsMMsyyyy") String startDateddsMMsyyyy, @QueryParam("endDateddsMMsyyyy") String endDateddsMMsyyyy,
			@Context HttpServletRequest request) throws Exception {

		QueryBinEndDatesResponseDocument response = webaspxRequestService.getQueryBinEndDates(getCompanyId(request), council, startDateddsMMsyyyy, endDateddsMMsyyyy);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/QueryBinOnType")
	@Produces(MediaType.APPLICATION_JSON)
	public String getQueryBinOnType(@QueryParam("council") String council, @QueryParam("binType") String binType, @Context HttpServletRequest request) throws Exception {

		QueryBinOnTypeResponseDocument response = webaspxRequestService.getQueryBinOnType(getCompanyId(request), council, binType);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getRoundAndBinInfoForUPRNForNewAdjustedDates")
	@Produces(MediaType.APPLICATION_JSON)
	public String getRoundAndBinInfoForUPRNForNewAdjustedDates(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("binID") String binID,
			@QueryParam("startDateDDsMMsYYYY") String startDateDDsMMsYYYY, @QueryParam("endDateDDsMMsYYYY") String endDateDDsMMsYYYY, @Context HttpServletRequest request) throws Exception {

		GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument response = webaspxRequestService.getRoundAndBinInfoForUPRNForNewAdjustedDates(getCompanyId(request), council, uprn, binID,
				startDateDDsMMsYYYY, endDateDDsMMsYYYY);
		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getRoundCalendarForUPRN")
	@Produces(MediaType.APPLICATION_JSON)
	public String getRoundCalendarForUPRN(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @Context HttpServletRequest request) throws Exception {

		GetRoundCalendarForUPRNResponseDocument response = webaspxRequestService.getRoundCalendarForUPRN(getCompanyId(request), council, uprn);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/GetRoundChangesInDateRangeByRoundOrUPRN")
	@Produces(MediaType.APPLICATION_JSON)
	public String getRoundChangesInDateRangeByRoundOrUPRN(@QueryParam("council") String council, @QueryParam("UPRN") String uprn,
			@QueryParam("roundName_CrXsDaysWkNsSrv") String roundName_CrXsDaysWkNsSrv, @QueryParam("startDate_ddsMMsyyyy") String startDate_ddsMMsyyyy,
			@QueryParam("endDate_ddsMMsYYYY") String endDate_ddsMMsYYYY, @Context HttpServletRequest request) throws Exception {

		GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument response = webaspxRequestService.getRoundChangesInDateRangeByRoundOrUPRN(getCompanyId(request), council, uprn,
				roundName_CrXsDaysWkNsSrv, startDate_ddsMMsyyyy, endDate_ddsMMsYYYY);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getRoundForUPRN")
	@Produces(MediaType.APPLICATION_JSON)
	public String getRoundForUPRN(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("startDateDDsMMsYYYY") String startDateDDsMMsYYYY,
			@Context HttpServletRequest request) throws Exception {

		GetRoundForUPRNResponseDocument response = webaspxRequestService.getRoundForUPRN(getCompanyId(request), council, uprn, startDateDDsMMsYYYY);
		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/getRoundNameForUPRNService")
	@Produces(MediaType.APPLICATION_JSON)
	public String getRoundNameForUPRNService(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("service3L") String service3L, @Context HttpServletRequest request)
			throws Exception {

		GetRoundNameForUPRNServiceResponseDocument response = webaspxRequestService.getRoundNameForUPRNService(getCompanyId(request), council, uprn, service3L);

		return jsonResponseParserService.toJson(response);

	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton(this);
	}

	@GET
	@Path("/GetTrucksOutToday")
	@Produces(MediaType.APPLICATION_JSON)
	public String getTrucksOutToday(@QueryParam("council") String council, @QueryParam("dateReq") String dateReq, @QueryParam("timeReq") String timeReq, @Context HttpServletRequest request)
			throws Exception {

		GetTrucksOutTodayResponseDocument response = webaspxRequestService.getTrucksOutToday(getCompanyId(request), council, dateReq, timeReq);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/BinInsert")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String insertBinDetails(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("binType") String binType, @QueryParam("payRef") String payRef,
			@QueryParam("DeliverYN") String deliverYN, @QueryParam("CollectYN") String collectYN, @QueryParam("StartDateDDsMMsYYYY ") String startDateDDsMMsYYYY,
			@QueryParam("EndDateDDsMMsYYYY ") String endDateDDsMMsYYYY, @QueryParam("ReportedDateDDsMMsYYYY") String reportedDateDDsMMsYYYY,
			@QueryParam("CompletedDateDDsMMsYYYY") String completedDateDDsMMsYYYY, @QueryParam("noOfNewContainersRequired") String noOfNewContainersRequired, @Context HttpServletRequest request)
			throws Exception {

		BinInsertResponseDocument response = webaspxRequestService.insertBinDetails(getCompanyId(request), council, uprn, binType, payRef, deliverYN, collectYN, startDateDDsMMsYYYY, endDateDDsMMsYYYY,
				reportedDateDDsMMsYYYY, completedDateDDsMMsYYYY, noOfNewContainersRequired);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/readWriteMissedBin")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String readWriteMissedBin(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("Service3L") String service3L, @QueryParam("missedYN") String missedYN,
			@QueryParam("missedDateDDsMMsYY") String missedDateDDsMMsYY, @Context HttpServletRequest request) throws Exception {

		ReadWriteMissedBinResponseDocument response = webaspxRequestService.readWriteMissedBin(getCompanyId(request), council, uprn, service3L, missedYN, missedDateDDsMMsYY);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/readWriteMissedBinByContainerType")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String readWriteMissedBinByContainerType(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("containerType") String containerType,
			@QueryParam("missedYN") String missedYN, @QueryParam("missedDateDDsMMsYY") String missedDateDDsMMsYY, @Context HttpServletRequest request) throws Exception {

		ReadWriteMissedBinByContainerTypeResponseDocument response = webaspxRequestService.readWriteMissedBinByContainerType(getCompanyId(request), council, uprn, containerType, missedYN,
				missedDateDDsMMsYY);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/Garden_RemoveSubscription")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String removeGardenSubscription(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("endDate_ddsMMsyyyy") String endDate_ddsMMsyyyy,
			@Context HttpServletRequest request) throws Exception {

		GardenRemoveSubscriptionResponseDocument response = webaspxRequestService.removeGardenSubscription(getCompanyId(request), council, uprn, endDate_ddsMMsyyyy);

		return jsonResponseParserService.toJson(response);

	}

	@GET
	@Path("/ShowAdditionalBins")
	@Produces(MediaType.APPLICATION_JSON)
	public String showAdditionalBins(@QueryParam("council") String council, @QueryParam("Service") String service, @QueryParam("startDateDDsMMsYYYY") String startDateDDsMMsYYYY,
			@QueryParam("endDateDDsMMsYYYY") String endDateDDsMMsYYYY, @Context HttpServletRequest request) throws Exception {

		ShowAdditionalBinsResponseDocument response = webaspxRequestService.showAdditionalBins(getCompanyId(request), council, service, startDateDDsMMsYYYY, endDateDDsMMsYYYY);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/UpdateAssisted")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateAssisted(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("AssistedYN01234") String assistedYN01234,
			@QueryParam("AssistedStartDateDDsMMsYYYY") String assistedStartDateDDsMMsYYYY, @QueryParam("AssistedEndDateDDsMMsYYYY") String assistedEndDateDDsMMsYYYY,
			@QueryParam("AssistedLocation") String assistedLocation, @Context HttpServletRequest request) throws Exception {

		UpdateAssistedResponseDocument response = webaspxRequestService.updateAssisted(getCompanyId(request), council, uprn, assistedYN01234, assistedStartDateDDsMMsYYYY, assistedEndDateDDsMMsYYYY,
				assistedLocation);

		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/BinUpdate")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateBin(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("binID") String binID, @QueryParam("binType") String binType,
			@QueryParam("payRef") String payRef, @QueryParam("DeliverYN") String deliverYN, @QueryParam("CollectYN") String collectYN, @QueryParam("StartDateDDsMMsYYYY") String startDateDDsMMsYYYY,
			@QueryParam("EndDateDDsMMsYYYY") String endDateDDsMMsYYYY, @QueryParam("ReportedDateDDsMMsYYYY") String reportedDateDDsMMsYYYY,
			@QueryParam("CompletedDateDDsMMsYYYY") String completedDateDDsMMsYYYY, @QueryParam("LeaveBlanksAsIsYN") String leaveBlanksAsIsYN, @Context HttpServletRequest request) throws Exception {

		BinUpdateResponseDocument response = webaspxRequestService.updateBin(getCompanyId(request), council, uprn, binID, binType, payRef, deliverYN, collectYN, startDateDDsMMsYYYY, endDateDDsMMsYYYY,
				reportedDateDDsMMsYYYY, completedDateDDsMMsYYYY, leaveBlanksAsIsYN);

		return jsonResponseParserService.toJson(response);

	}

	@POST
	@Path("/CommentsForIncabUpdate")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateCommentsForIncab(@QueryParam("council") String council, @QueryParam("UPRN") String uprn, @QueryParam("newComment") String newComment,
			@QueryParam("AppendOrReplace") String appendOrReplace, @Context HttpServletRequest request) throws Exception {

		CommentsForIncabUpdateResponseDocument response = webaspxRequestService.updateCommentsForIncab(getCompanyId(request), council, uprn, newComment, appendOrReplace);

		return jsonResponseParserService.toJson(response);
	}

	private long getCompanyId(HttpServletRequest request) {
		return (Long) request.getAttribute("COMPANY_ID");
	}

}