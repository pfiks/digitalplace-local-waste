package com.placecube.digitalplace.local.waste.webaspx.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;

@Component(immediate = true, service = RequestDetailsParsingService.class)
public class RequestDetailsParsingService {

	private static final String EXPECTED_DATE_PATTERN = "dd/MM/yyyy";

	public String getBinServiceForBinType(String binType) throws WasteRetrievalException {
		if (Validator.isNull(binType)) {
			throw new WasteRetrievalException("BinType is null");
		}

		binType = binType.toLowerCase();

		if (binType.contains("garden")) {
			return "GARDEN";
		}

		if (binType.contains("plastic")) {
			return "PLASTIC";
		}

		if (binType.contains("recycling")) {
			return "RECYCLING";
		}

		if (binType.contains("refuse") || binType.contains("rubbish")) {
			return "REFUSE";
		}

		throw new WasteRetrievalException("BinType is not managed  - " + binType);
	}

	public String getDateInCorrectFormat(String dateToParse) {
		if (Validator.isNull(dateToParse)) {
			return StringPool.BLANK;
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(EXPECTED_DATE_PATTERN);
		try {
			LocalDate.parse(dateToParse, dtf);
			return dateToParse;
		} catch (DateTimeParseException e) {
			LocalDate date = LocalDate.parse(dateToParse, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			return date.format(dtf);
		}
	}

}
