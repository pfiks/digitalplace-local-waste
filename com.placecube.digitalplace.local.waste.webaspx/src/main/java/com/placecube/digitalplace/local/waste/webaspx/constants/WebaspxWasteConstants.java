package com.placecube.digitalplace.local.waste.webaspx.constants;

public class WebaspxWasteConstants {

	public static final String COUNCIL = "Rugby";
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String ERROR_STRING = "\"Error\"";
	public static final String SUBSCRIBERS = "Subscribers";

	private WebaspxWasteConstants() {

	}
}
