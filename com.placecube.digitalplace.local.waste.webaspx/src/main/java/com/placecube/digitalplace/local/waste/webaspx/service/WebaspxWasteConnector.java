package com.placecube.digitalplace.local.waste.webaspx.service;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.Bin;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.service.WasteConnector;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedResponseDocument;
import com.placecube.digitalplace.local.waste.webaspx.constants.WebaspxWasteConstants;

@Component(immediate = true, service = WasteConnector.class)
public class WebaspxWasteConnector implements WasteConnector {

	private static final Log LOG = LogFactoryUtil.getLog(WebaspxWasteConnector.class);

	@Reference
	private JsonResponseParserService jsonResponseParserService;

	@Reference
	private RequestDetailsParsingService requestDetailsParsingService;

	@Reference
	private WebaspxRequestService webaspxRequestService;

	@Reference
	private WebaspxWasteService webaspxWasteService;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public String createAssistedBinCollectionJob(long companyId, AssistedBinCollectionJob assistedBinCollectionJob) throws WasteRetrievalException {
		try {

			LOG.debug("createAssistedBinCollectionJob: " + jsonFactory.serialize(assistedBinCollectionJob));

			String assistedYN01234 = "Y";

			String startDateFormatted = requestDetailsParsingService.getDateInCorrectFormat(assistedBinCollectionJob.getStartDate());
			String endDateFormatted = requestDetailsParsingService.getDateInCorrectFormat(assistedBinCollectionJob.getEndDate());

			UpdateAssistedResponseDocument response = webaspxRequestService.updateAssisted(companyId, WebaspxWasteConstants.COUNCIL, assistedBinCollectionJob.getUprn(), assistedYN01234,
					startDateFormatted, endDateFormatted, assistedBinCollectionJob.getBinLocation());

			LOG.debug("Response: " + response.toString());

			LOG.info("Created assisted bin collection job - classPK: " + assistedBinCollectionJob.getClassPK());

			return response.toString();
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error creating assisted bin collection job: " + e.getMessage());
		}
	}

	@Override
	public WasteSubscriptionResponse createBinSubscription(long companyId, String uprn, String serviceType, String firstName, String lastName, String emailAddress, String telephone, int numberOfBins)
			throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, List<String> itemsToBeCollected,
			String locationOfItems, Date collectionDate) throws WasteRetrievalException {

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createFlyTippingReport(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, String typeOfRubbish, String sizeOfRubbish,
			String details, String coordinates, String locationDetails, String dateOfIncident) throws WasteRetrievalException {

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public WasteSubscriptionResponse createGardenWasteSubscription(long companyId, GardenWasteSubscription gardenWasteSubscription) throws WasteRetrievalException {

		try {

			LOG.debug("createGardenWasteSubscription:" + jsonFactory.serialize(gardenWasteSubscription));

			String binService = "Garden";

			int numberRequestedNewBins = gardenWasteSubscription.getNumberRequestedNewBins();
			if (numberRequestedNewBins < 0) {
				throw new WasteRetrievalException("Incorrect value for 'numberRequestedNewBins'");
			}

			String uprn = gardenWasteSubscription.getUprn();
			List<Bin> binList = getBinDetailsForService(companyId, WebaspxWasteConstants.COUNCIL, uprn, binService);

			String startDateFormatted = requestDetailsParsingService.getDateInCorrectFormat(gardenWasteSubscription.getStartDate());
			String endDateFormatted = requestDetailsParsingService.getDateInCorrectFormat(gardenWasteSubscription.getEndDate());

			int numberSubscribedBins = howManyBinsSubscribedForPeriod(companyId, uprn, binService, startDateFormatted, endDateFormatted);

			String newPayRef = gardenWasteSubscription.getClassPK();
			String crmGardenRef = gardenWasteSubscription.getClassPK();

			GardenSubscriptionResponseDocument gardenSubscriptionResponseDocument = webaspxRequestService.addOrUpdateGardenSubscription(companyId, WebaspxWasteConstants.COUNCIL, uprn,
					String.valueOf(numberSubscribedBins), String.valueOf(numberSubscribedBins + numberRequestedNewBins), startDateFormatted, endDateFormatted, newPayRef,
					gardenWasteSubscription.getBinType(), gardenWasteSubscription.getGardenContainerDeliveryComments(), crmGardenRef);
			String response = jsonResponseParserService.toJson(gardenSubscriptionResponseDocument);

			LOG.debug("Response: " + response);

			LOG.info("Created Garden Waste Subscription - classPK: " + gardenWasteSubscription.getClassPK());

			String binId = getBinIdFromJsonResponse(companyId, uprn, binService, binList, response);
			return new WasteSubscriptionResponse(binId, SubscriptionType.GARDEN_WASTE);

		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error creating garden waste subscription: " + e.getMessage());
		}
	}

	@Override
	public String createMissedBinCollectionJob(long companyId, MissedBinCollectionJob missedBinCollectionJob) throws WasteRetrievalException {

		try {

			LOG.debug("createMissedBinCollectionJob: " + jsonFactory.serialize(missedBinCollectionJob));

			String missedYN = "Y";
			String missedDateFormatted = requestDetailsParsingService.getDateInCorrectFormat(missedBinCollectionJob.getMissedDate());
			String binTypeFormatted = formatOptionReference(missedBinCollectionJob.getBinType());

			String response = webaspxRequestService.readWriteMissedBin(companyId, WebaspxWasteConstants.COUNCIL, missedBinCollectionJob.getUprn(), binTypeFormatted, missedYN, missedDateFormatted)
					.toString();

			LOG.debug("Response: " + response);

			LOG.info("Create Missed Bin Collection Job - classPK: " + missedBinCollectionJob.getClassPK());

			return response;
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error creating missed bin collection job: " + e.getMessage());
		}
	}

	@Override
	public String createNewWasteContainerRequest(long companyId, NewContainerRequest newContainerRequest) throws WasteRetrievalException {
		try {

			LOG.debug("createNewWasteContainerRequest: " + jsonFactory.serialize(newContainerRequest));

			String binType = formatOptionReference(newContainerRequest.getBinType());
			String binService = requestDetailsParsingService.getBinServiceForBinType(binType);

			String uprn = newContainerRequest.getUprn();
			List<Bin> allBinsForService = getBinDetailsForService(companyId, WebaspxWasteConstants.COUNCIL, uprn, binService);

			// ASSUMPTION - There is only 1 bin for each type
			Optional<Bin> currentBin = allBinsForService.isEmpty() ? Optional.empty() : Optional.ofNullable(allBinsForService.get(0));

			String payRef = StringPool.BLANK;
			String deliveryYN = "Y";
			String collectYN = newContainerRequest.isCollectOldBin() ? "Y" : "N";
			String startDate = StringPool.BLANK;
			String endDate = StringPool.BLANK;
			String reportedDate = StringPool.BLANK;
			String completedDate = StringPool.BLANK;
			String noOfNewContainersRequired = "1";

			// If it's a replacement bin then delete the current bin first
			if (currentBin.isPresent()) {
				Bin bin = currentBin.get();
				startDate = bin.getStartDate();
				endDate = bin.getEndDate();

				if (newContainerRequest.isCollectOldBin()) {
					BinDeleteResponseDocument deleteBinResponse = webaspxRequestService.deleteBin(companyId, WebaspxWasteConstants.COUNCIL, uprn, bin.getBinID());
					LOG.debug(deleteBinResponse);
				}
			}

			BinInsertResponseDocument insertBinResponse = webaspxRequestService.insertBinDetails(companyId, WebaspxWasteConstants.COUNCIL, uprn, binType, payRef, deliveryYN, collectYN, startDate,
					endDate, reportedDate, completedDate, noOfNewContainersRequired);

			String response = jsonResponseParserService.toJson(insertBinResponse);

			LOG.debug("Response: " + response);

			LOG.info("Created New Waste Container Request - classPK: " + newContainerRequest.getClassPK());

			return getBinIdFromJsonResponse(companyId, uprn, binService, allBinsForService, response);
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error requesting a new waste container: " + e.getMessage(), e);
		}
	}

	@Override
	public boolean enabled(long companyId) {
		try {
			return webaspxWasteService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public List<WasteSubscriptionResponse> getAllWasteSubscriptions(long companyId, SubscriptionType subscriptionType) throws WasteRetrievalException {

		List<WasteSubscriptionResponse> wasteSubscriptionResponses = new ArrayList<>();

		try {
			GardenSubscribersDeliveriesCollectionsResponseDocument gardenSubscribersDeliveriesCollectionsResponseDocument = webaspxRequestService.getGardenSubscribersDeliveriesCollections(companyId,
					WebaspxWasteConstants.COUNCIL, WebaspxWasteConstants.SUBSCRIBERS, StringPool.BLANK);
			GardenSubscribersDeliveriesCollectionsResponse collectionResponse = gardenSubscribersDeliveriesCollectionsResponseDocument.getGardenSubscribersDeliveriesCollectionsResponse();

			GardenSubscribersDeliveriesCollectionsResult collectionResult = collectionResponse.getGardenSubscribersDeliveriesCollectionsResult();

			JSONObject response = jsonFactory.createJSONObject(jsonResponseParserService.toJson(collectionResult));

			if (!response.toString().contains(WebaspxWasteConstants.ERROR_STRING)) {

				JSONArray subscriberResponse = response.getJSONArray("Subscriber");

				for (Object subscriberObject : subscriberResponse) {
					JSONObject subscriber = (JSONObject) subscriberObject;
					if (Validator.isNotNull(subscriber)) {
						String crmRef = subscriber.getString("CRM_Ref");
						String uprn = subscriber.getString("UPRN");

						WasteSubscriptionResponse wasteSubscriptionResponse = new WasteSubscriptionResponse(crmRef, subscriptionType);
						wasteSubscriptionResponse.setUprn(uprn);

						JSONObject containers = subscriber.getJSONObject("Containers");

						addStartAndEndDateToWasteSubscriptionResponse(containers, wasteSubscriptionResponse);

						wasteSubscriptionResponses.add(wasteSubscriptionResponse);
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error getting all waste subscriptions: " + e.getMessage(), e);
		}

		return wasteSubscriptionResponses;
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRN(long companyId, String uprn) throws WasteRetrievalException {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT, Locale.ENGLISH);
			String startDate = formatter.format(LocalDateTime.now());
			String endDate = formatter.format(LocalDateTime.now().plusMonths(2));

			GetRoundForUPRNResponseDocument getRoundForUPRNResponseDocument = webaspxRequestService.getRoundForUPRN(companyId, WebaspxWasteConstants.COUNCIL, uprn, startDate);

			GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument getRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument = webaspxRequestService
					.getRoundAndBinInfoForUPRNForNewAdjustedDates(companyId, WebaspxWasteConstants.COUNCIL, uprn, null, startDate, endDate);

			JSONObject json = jsonFactory.createJSONObject(jsonResponseParserService.toJson(getRoundForUPRNResponseDocument));
			JSONObject jsonExtra = jsonFactory.createJSONObject(jsonResponseParserService.toJson(getRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument));

			LOG.debug("getBinCollectionDatesByUPRN: " + jsonFactory.serialize(json));
			LOG.debug("getBinCollectionDatesByUPRN: extra: " + jsonFactory.serialize(jsonExtra));

			Set<BinCollection> binCollections = new HashSet<>();

			if (!json.toString().contains(WebaspxWasteConstants.ERROR_STRING) && !jsonExtra.toString().contains(WebaspxWasteConstants.ERROR_STRING)) {

				Optional<JSONObject> optionalJsonRoundForUPRN = jsonResponseParserService.getRoundForUPRNRowJSONObject(json);
				Optional<JSONArray> optionalJsonRoundAndBin = jsonResponseParserService.getRoundAndBinForUPRNJSONObject(jsonExtra);

				if (optionalJsonRoundForUPRN.isPresent() && optionalJsonRoundAndBin.isPresent()) {
					JSONObject jsonRoundForUPRN = optionalJsonRoundForUPRN.get();
					JSONArray jsonRoundAndBin = optionalJsonRoundAndBin.get();

					for (Iterator<String> key = jsonRoundForUPRN.keys(); key.hasNext();) {
						String name = (String) jsonRoundForUPRN.get(key.next());
						jsonResponseParserService.getBinCollectionFromJSONRoundAndBin(name, jsonRoundAndBin).ifPresent(binCollections::add);
					}
				}
			}

			return binCollections;

		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error getting bin collection dates by UPRN: " + e.getMessage());
		}
	}

	@Override
	public Optional<BulkyCollectionDate> getBulkyCollectionDateByUPRN(long companyId, String uprn) throws WasteRetrievalException {

		int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		String dayNameOfWeek = StringUtil.upperCaseFirstLetter(DayOfWeek.of(dayOfWeek).name().toLowerCase());

		return Optional.of(new BulkyCollectionDate(dayOfWeek, dayNameOfWeek));
	}

	@Override
	public String getMissedBinCollectionResponse(long companyId, String uprn, String date) throws WasteRetrievalException {

		try {

			GetIssuesForUPRNResponseDocument getIssuesForUPRNResponseDocument = webaspxRequestService.getIssuesForUPRN(companyId, WebaspxWasteConstants.COUNCIL, date, uprn);
			String json = jsonResponseParserService.toJson(getIssuesForUPRNResponseDocument.getGetIssuesForUPRNResponse().getGetIssuesForUPRNResult());

			LOG.debug("getMissedBinCollectionResponse: " + json);

			if (json.equalsIgnoreCase("\"\"")) {
				return json;
			}
			JSONObject jsonObject = jsonFactory.createJSONObject(json);
			if (jsonObject.has("Status") && jsonObject.getString("Status").equalsIgnoreCase("Error")) {
				throw new WasteRetrievalException(json);
			}

			LOG.debug("getMissedBinCollectionResponse: " + json);

			return jsonObject.getJSONObject("Item").length() > 0 ? jsonObject.getJSONObject("Item").getString("Issue") : "";
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error getting missed bin collection response: " + e.getMessage());
		}

	}

	@Override
	public String getName(long companyId) {
		return "Waste - Webaspx";
	}

	@Override
	public int howManyBinsSubscribed(long companyId, String uprn, String serviceType) throws WasteRetrievalException {
		try {

			LOG.debug("howManyBinsSubscribed: " + serviceType);

			List<Bin> binList = getBinDetailsForService(companyId, WebaspxWasteConstants.COUNCIL, uprn, serviceType);

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT);

			return binList.stream().filter(bin -> {
				LocalDate binStartDate = LocalDate.parse(bin.getEndDate(), dtf);
				LocalDate binEndDate = LocalDate.parse(bin.getEndDate(), dtf);
				return binEndDate.isAfter(LocalDate.now()) && binStartDate.isBefore(LocalDate.now());
			}).collect(Collectors.toList()).size();

		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error checking how many bins a property is subscribed to: " + e.getMessage());
		}
	}

	@Override
	public int howManyBinsSubscribedForPeriod(long companyId, String uprn, String serviceType, String startDate, String endDate) throws WasteRetrievalException {
		try {

			LOG.debug("howManyBinsSubscribedForPeriod: " + serviceType + " - startDate: " + startDate + " - endDate: " + endDate);

			List<Bin> binList = getBinDetailsForService(companyId, WebaspxWasteConstants.COUNCIL, uprn, serviceType);

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT);

			return binList.stream().filter(bin -> {
				LocalDate periodStartDate = LocalDate.parse(startDate, dtf);
				LocalDate periodEndDate = LocalDate.parse(endDate, dtf);
				LocalDate binStartDate = LocalDate.parse(bin.getStartDate(), dtf);
				LocalDate binEndDate = LocalDate.parse(bin.getEndDate(), dtf);

				return binEndDate.isAfter(periodStartDate) && binStartDate.isBefore(periodEndDate);
			}).collect(Collectors.toList()).size();

		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error checking how many bins a property is subscribed to: " + e.getMessage());
		}
	}

	@Deprecated
	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws WasteRetrievalException {

		try {
			String binService = "Garden";

			GetBinDetailsForServiceResponseDocument response = webaspxRequestService.getBinDetailsForService(companyId, WebaspxWasteConstants.COUNCIL, uprn, binService);
			JSONObject json = jsonFactory.createJSONObject(jsonResponseParserService.toJson(response));

			JSONObject bins = json.getJSONObject("GetBinDetailsForServiceResult").getJSONObject("Bins");
			JSONObject binObj = bins.getJSONObject("Bin");
			if (binObj != null) {
				return isPropertyEligibleForGardenWasteSubscription(binObj, startDateDDsMMsYYYY, endDateDDsMMsYYYY);
			}

			JSONArray binArr = bins.getJSONArray("Bin");
			if (binArr != null) {
				for (int i = 0; i < binArr.length(); i++) {
					if (isPropertyEligibleForGardenWasteSubscription(binArr.getJSONObject(i), startDateDDsMMsYYYY, endDateDDsMMsYYYY)) {
						return true;
					}
				}
			}

			return false;
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error checking if property is eligible for garden waste subscription: " + e.getMessage());
		}
	}

	private void addStartAndEndDateToWasteSubscriptionResponse(JSONObject containers, WasteSubscriptionResponse wasteSubscriptionResponse) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT);
		for (Iterator<String> containerKey = containers.keys(); containerKey.hasNext();) {
			JSONObject container = containers.getJSONObject(containerKey.next());

			if (Validator.isNotNull(container)) {
				String startDate = container.getString("StartDate");
				String endDate = container.getString("EndDate");
				if (Validator.isNotNull(startDate)) {
					wasteSubscriptionResponse.setStartDate(LocalDate.parse(startDate, formatter));
				}
				if (Validator.isNotNull(endDate)) {
					wasteSubscriptionResponse.setEndDate(LocalDate.parse(endDate, formatter));
				}
			}
		}
	}

	private Bin createBin(JSONObject binJSON) {
		return new Bin(binJSON.getString("binID"), binJSON.getString("UPRN"), binJSON.getString("Bin_Number"), binJSON.getString("Start_date"), binJSON.getString("End_date"),
				binJSON.getString("Reported_date"), binJSON.getString("Completed_date"), binJSON.getString("Deliver"), binJSON.getString("Collect"), binJSON.getString("Payment_Ref"),
				binJSON.getString("binMissed"), binJSON.getString("binMissed_date"));
	}

	private String formatOptionReference(String optionReference) {
		return optionReference.replace("_", " ").trim();
	}

	private List<Bin> getBinDetailsForService(long companyId, String council, String uprn, String binService) throws ConfigurationException, RemoteException, JSONException {
		GetBinDetailsForServiceResponseDocument response = webaspxRequestService.getBinDetailsForService(companyId, WebaspxWasteConstants.COUNCIL, uprn, binService);
		JSONObject json = jsonFactory.createJSONObject(jsonResponseParserService.toJson(response));

		JSONObject bins = json.getJSONObject("GetBinDetailsForServiceResult").getJSONObject("Bins");

		if (Validator.isNotNull(bins)) {
			JSONObject binObj = bins.getJSONObject("Bin");
			if (binObj != null) {
				return Collections.singletonList(createBin(binObj));
			}

			JSONArray binArr = bins.getJSONArray("Bin");
			if (binArr != null) {
				List<Bin> binList = new ArrayList<>();
				for (int i = 0; i < binArr.length(); i++) {
					binList.add(createBin(binArr.getJSONObject(i)));
				}
				return binList;
			}
		}
		return Collections.emptyList();
	}

	private String getBinIdFromJsonResponse(long companyId, String uprn, String binService, List<Bin> binList, String responseJson) throws JSONException, ConfigurationException, RemoteException {
		Map<String, Bin> binMap = binList.stream().collect(Collectors.toMap(Bin::getBinID, e -> e));
		JSONObject json = jsonFactory.createJSONObject(responseJson);

		if (!json.toString().contains(WebaspxWasteConstants.ERROR_STRING)) {
			JSONArray binJSONArr = jsonFactory.createJSONArray();

			binList = getBinDetailsForService(companyId, WebaspxWasteConstants.COUNCIL, uprn, binService);
			binList.stream().filter(b -> !binMap.containsKey(b.getBinID())).forEach(b -> binJSONArr.put(b.getBinID()));
			return JSONUtil.put("binId", binJSONArr).toString();
		}
		return JSONUtil.put("binId", "Error").toString();
	}

	private boolean isPropertyEligibleForGardenWasteSubscription(JSONObject binJSON, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws ParseException {
		SimpleDateFormat parser = new SimpleDateFormat(WebaspxWasteConstants.DATE_FORMAT);
		Date startDate = parser.parse(binJSON.getString("Start_date"));
		Date endDate = parser.parse(binJSON.getString("End_date"));
		Date startDateQuery = parser.parse(startDateDDsMMsYYYY);
		Date endDateQuery = parser.parse(endDateDDsMMsYYYY);

		return !startDate.after(startDateQuery) && !endDate.before(endDateQuery);
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, BulkyWasteCollectionRequest bulkyWasteCollectionRequest, Date date) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public List<Date> getBulkyCollectionSlotsByUPRNAndService(long companyId, String uprn, String serviceId, int numberOfDatesToReturn) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForWasteContainerRequest(long companyId, String uprn, String binType, String binSize, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForBulkyWasteCollection(long companyId, String uprn, List<String> itemsForCollection, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForMissedBinCollectionReport(long companyId, String uprn, String binType, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRNAndService(long companyId, String uprn, String serviceName, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String binType, String formInstanceId)
			throws WasteRetrievalException {
		return isPropertyEligibleForGardenWasteSubscription(companyId, uprn, startDateDDsMMsYYYY, endDateDDsMMsYYYY);
	}

}
