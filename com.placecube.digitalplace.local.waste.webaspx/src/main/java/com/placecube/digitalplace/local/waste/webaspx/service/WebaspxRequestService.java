package com.placecube.digitalplace.local.waste.webaspx.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.rmi.RemoteException;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.waste.webaspx.axis.*;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestDocument.AAHelloWorldStringTest;
import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteDocument.BinDelete;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertDocument.BinInsert;
import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateDocument.BinUpdate;
import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarDocument.CachedCalendar;
import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedDocument.CheckAssisted;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingDocument.CommentsForIncabGetExisting;
import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateDocument.CommentsForIncabUpdate;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceDocument.CountBinsForService;
import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsDocument.CountSubscriptions;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressDocument.GardenChangeAddress;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionDocument.GardenRemoveSubscription;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections;
import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionDocument.GardenSubscription;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeDocument.GetAddressFromPostcode;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNDocument.GetAddressOrUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsDocument.GetAllRoundDetails;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateDocument.GetAllUPRNsForDate;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersDocument.GetAvailableContainers;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyDocument.GetAvailableContainersForClientOrProperty;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsDocument.GetAvailableRounds;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceDocument.GetBinDetailsForService;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeDocument.GetInCabIssueTextAndIssueCode;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesDocument.GetIssues;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNDocument.GetIssuesForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionDocument.GetLastTruckPosition;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsDocument.GetLatestTruckPositions;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesDocument.GetNoCollectionDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNDocument.GetRoundCalendarForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNDocument.GetRoundChangesInDateRangeByRoundOrUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNDocument.GetRoundForUPRN;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceDocument.GetRoundNameForUPRNService;
import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayDocument.GetTrucksOutToday;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsDocument.LLPGXtraUpdateGetAvailableFieldDetails;
import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField;
import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsDocument.LargeHouseholds;
import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedDocument.LastCollected;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesDocument.QueryBinEndDates;
import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeDocument.QueryBinOnType;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeDocument.ReadWriteMissedBinByContainerType;
import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinDocument.ReadWriteMissedBin;
import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsDocument.ShowAdditionalBins;
import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedDocument.UpdateAssisted;
import com.placecube.digitalplace.local.waste.webaspx.configuration.WebaspxConfiguration;

@Component(immediate = true, service = WebaspxRequestService.class)
public class WebaspxRequestService {

	@Reference
	private ObjectFactoryService objectFactoryService;

	@Reference
	private WebaspxAxisService webaspxAxisService;

	@Reference
	private WebaspxWasteService webaspxWasteService;


	public GardenSubscriptionResponseDocument addOrUpdateGardenSubscription(long companyId, String council, String uprn, String binsAtProperty, String totalNoSubsRequired,
			String subscriptionStartDate, String subscriptionEndDate, String newPayRef, String binType, String gardenContainerDeliveryComments, String crmGardenRef) throws ConfigurationException, RemoteException {

		GardenSubscriptionDocument gardenSubscriptionDocument = objectFactoryService.getGardenSubscriptionDocumentInput();
		GardenSubscription input = objectFactoryService.getGardenSubscriptionInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setBinsAtProperty(binsAtProperty);
		input.setTotalNoSubsRequired(totalNoSubsRequired);
		input.setSubscriptionStartDate(subscriptionStartDate);
		input.setSubscriptionEndDate(subscriptionEndDate);
		input.setNewPayRef(newPayRef);
		input.setBinType(binType);
		input.setGardenContainerDeliveryComments(gardenContainerDeliveryComments);
		input.setCrmGardenRef(crmGardenRef);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_addOrUpdateGardenSubscription());
		gardenSubscriptionDocument.setGardenSubscription(input);

		return getWebaspxService(companyId).gardenSubscription(gardenSubscriptionDocument);
	}

	public GardenChangeAddressResponseDocument changeGardenAddress(long companyId, String council, String fromUPRN, String toUPRN) throws ConfigurationException, RemoteException {

		GardenChangeAddressDocument gardenChangeAddressDocument = objectFactoryService.getGardenChangeAddressDocumentInput();
		GardenChangeAddress input = objectFactoryService.getGardenChangeAddressInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setFromUPRN(fromUPRN);
		input.setToUPRN(toUPRN);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_changeGardenAddress());

		gardenChangeAddressDocument.setGardenChangeAddress(input);

		return getWebaspxService(companyId).garden_ChangeAddress(gardenChangeAddressDocument);
	}

	public CountBinsForServiceResponseDocument countBinsForService(long companyId, String council, String uprn, String binService) throws ConfigurationException, RemoteException {

		CountBinsForServiceDocument countBinsForServiceDocument = objectFactoryService.countBinsForServiceDocumentInput();
		CountBinsForService input = objectFactoryService.countBinsForServiceInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setBinService(binService);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_countBinsForService());

		countBinsForServiceDocument.setCountBinsForService(input);

		return getWebaspxService(companyId).countBinsForService(countBinsForServiceDocument);
	}

	public CountSubscriptionsResponseDocument countSubscriptions(long companyId, String council, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String returnUPRNs) throws ConfigurationException, RemoteException {

		CountSubscriptionsDocument countSubscriptionsDocument = objectFactoryService.getCountSubscriptionsDocumentInput();
		CountSubscriptions input = objectFactoryService.getCountSubscriptionsInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setStartDateDDsMMsYYYY(startDateDDsMMsYYYY);
		input.setEndDateDDsMMsYYYY(endDateDDsMMsYYYY);
		input.setReturnUPRNs(returnUPRNs);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_countSubscriptions());

		countSubscriptionsDocument.setCountSubscriptions(input);

		return getWebaspxService(companyId).countSubscriptions(countSubscriptionsDocument);
	}

	public BinDeleteResponseDocument deleteBin(long companyId, String council, String uprn, String binID) throws ConfigurationException, RemoteException {

		BinDeleteDocument binDeleteDocument = objectFactoryService.getBinDeleteDocumentInput();
		BinDelete input = objectFactoryService.getBinDeleteInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setBinID(binID);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_deleteBin());

		binDeleteDocument.setBinDelete(input);

		return getWebaspxService(companyId).binDelete(binDeleteDocument);
	}

	public AAHelloWorldStringTestResponseDocument getAAHelloWorldStringTest(long companyId, String testName) throws ConfigurationException, RemoteException {
		AAHelloWorldStringTestDocument aaHelloWorldStringTestDocument = objectFactoryService.aaHelloWorldStringTestDocumentInput();
		AAHelloWorldStringTest input = objectFactoryService.aaHelloWorldStringTestInput();
		input.setTestName(testName);
		aaHelloWorldStringTestDocument.setAAHelloWorldStringTest(input);

		return getWebaspxService(companyId).aA_HelloWorld_String_Test(aaHelloWorldStringTestDocument);
	}

	public AAHelloWorldXMLTestResponseDocument getAAHelloWorldXMLTest(long companyId, String testName) throws ConfigurationException, RemoteException {

		AAHelloWorldXMLTestDocument aaHelloWorldXMLTestDocument = objectFactoryService.aaHelloWorldXMLTestDocumentInput();
		AAHelloWorldXMLTest input = objectFactoryService.aaHelloWorldXMLTestInput();
		input.setTestName(testName);
		aaHelloWorldXMLTestDocument.setAAHelloWorldXMLTest(input);

		return getWebaspxService(companyId).aA_HelloWorld_XML_Test(aaHelloWorldXMLTestDocument);
	}

	public GetAddressFromPostcodeResponseDocument getAddressFromPostcode(long companyId, String council, String searchString) throws ConfigurationException, RemoteException {

		GetAddressFromPostcodeDocument getAddressFromPostcodeDocument = objectFactoryService.getGetAddressFromPostcodeDocumentInput();
		GetAddressFromPostcode input = objectFactoryService.getGetAddressFromPostcodeInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setSearchString(searchString);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getAddressFromPostcode());

		getAddressFromPostcodeDocument.setGetAddressFromPostcode(input);

		return getWebaspxService(companyId).getAddressFromPostcode(getAddressFromPostcodeDocument);
	}

	public GetAddressOrUPRNResponseDocument getAddressOrUPRN(long companyId, String council, String uprn, String address) throws ConfigurationException, RemoteException {

		GetAddressOrUPRNDocument getAddressOrUPRNDocument = objectFactoryService.getAddressOrUPRNDocumentInput();
		GetAddressOrUPRN input = objectFactoryService.getAddressOrUPRNInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setAddress(address);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getAddressOrUPRN());
		getAddressOrUPRNDocument.setGetAddressOrUPRN(input);

		return getWebaspxService(companyId).getAddressOrUPRN(getAddressOrUPRNDocument);
	}

	public GetAllIssuesForIssueTypeResponseDocument getAllIssuesForIssueType(long companyId, String council, String issueType, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String digTime,
			String service) throws ConfigurationException, RemoteException {

		GetAllIssuesForIssueTypeDocument getIssuesForUPRNDocument = objectFactoryService.getAllIssuesForIssueTypeDocumentInput();
		GetAllIssuesForIssueType input = objectFactoryService.getAllIssuesForIssueTypeInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setIssueType(issueType);
		input.setStartDateDDMMYYYY(startDateDDsMMsYYYY);
		input.setEndDateDDMMYYYY(endDateDDsMMsYYYY);
		input.setDigTime(digTime);
		input.setService(service);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getAllIssuesForIssueType());
		getIssuesForUPRNDocument.setGetAllIssuesForIssueType(input);

		return getWebaspxService(companyId).getAllIssuesForIssueType(getIssuesForUPRNDocument);
	}

	public GetAllRoundDetailsResponseDocument getAllRoundDetails(long companyId, String council) throws ConfigurationException, RemoteException {

		GetAllRoundDetailsDocument getAllRoundDetailsDocument = objectFactoryService.getGetAllRoundDetailsDocumentInput();
		GetAllRoundDetails input = objectFactoryService.getAllRoundDetailsResponseDocumentInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getAllRoundDetails());
		getAllRoundDetailsDocument.setGetAllRoundDetails(input);

		return getWebaspxService(companyId).getAllRoundDetails(getAllRoundDetailsDocument);
	}

	public GetAllUPRNsForDateResponseDocument getAllUPRNsForDate(long companyId, String council, String dateRequiredDDsMMsYYYY) throws ConfigurationException, RemoteException {

		GetAllUPRNsForDateDocument getAllUPRNsForDateDocument = objectFactoryService.getGetAllUPRNsForDateDocumentInput();
		GetAllUPRNsForDate input = objectFactoryService.getAllUPRNsForDateInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setDateRequiredDDsMMsYYYY(dateRequiredDDsMMsYYYY);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getAllUPRNsForDate());
		getAllUPRNsForDateDocument.setGetAllUPRNsForDate(input);

		return getWebaspxService(companyId).getAllUPRNsForDate(getAllUPRNsForDateDocument);
	}

	public GetAvailableContainersResponseDocument getAvailableContainers(long companyId, String council) throws ConfigurationException, RemoteException {

		GetAvailableContainersDocument getAvailableContainersDocument = objectFactoryService.getAvailableContainersDocumentInput();
		GetAvailableContainers input = objectFactoryService.getAvailableContainersInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getAvailableContainers());
		getAvailableContainersDocument.setGetAvailableContainers(input);

		return getWebaspxService(companyId).getAvailableContainers(getAvailableContainersDocument);
	}

	public GetAvailableContainersForClientOrPropertyResponseDocument getAvailableContainersForClientOrProperty(long companyId, String council, String uprn) throws ConfigurationException, RemoteException {

		GetAvailableContainersForClientOrPropertyDocument availableContainersForClientOrProperty = objectFactoryService.getAvailableContainersForClientOrPropertyDocumentInput();
		GetAvailableContainersForClientOrProperty input = objectFactoryService.getAvailableContainersForClientOrPropertyInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getAvailableContainersForClientOrProperty());
		availableContainersForClientOrProperty.setGetAvailableContainersForClientOrProperty(input);

		return getWebaspxService(companyId).getAvailableContainersForClientOrProperty(availableContainersForClientOrProperty);
	}

	public GetAvailableRoundsResponseDocument getAvailableRounds(long companyId, String council, String service3L) throws ConfigurationException, RemoteException {

		GetAvailableRoundsDocument getAvailableRoundsDocument = objectFactoryService.getAvailableRoundsDocumentInput();
		GetAvailableRounds input = objectFactoryService.getAvailableRoundsInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setService3L(service3L);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getAvailableRounds());

		getAvailableRoundsDocument.setGetAvailableRounds(input);

		return getWebaspxService(companyId).getAvailableRounds(getAvailableRoundsDocument);
	}

	public GetBinDetailsForServiceResponseDocument getBinDetailsForService(long companyId, String council, String uprn, String binService) throws ConfigurationException, RemoteException {

		GetBinDetailsForServiceDocument getBinDetailsForServiceDocument = objectFactoryService.getBinDetailsForServiceDocumentInput();
		GetBinDetailsForService input = objectFactoryService.getBinDetailsForServiceInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setUPRN(uprn);
		input.setCouncil(council);
		input.setBinService(binService);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getBinDetailsForService());
		getBinDetailsForServiceDocument.setGetBinDetailsForService(input);

		return getWebaspxService(companyId).getBinDetailsForService(getBinDetailsForServiceDocument);
	}

	public CachedCalendarResponseDocument getCachedCalendar(long companyId, String council, String splitOutRounds) throws ConfigurationException, RemoteException {

		CachedCalendarDocument cachedCalendarDocument = objectFactoryService.getCachedCalendarDocumentInput();
		CachedCalendar input = objectFactoryService.getCachedCalendarInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setSplitOutRounds(splitOutRounds);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getCachedCalendar());
		cachedCalendarDocument.setCachedCalendar(input);

		return getWebaspxService(companyId).cachedCalendar(cachedCalendarDocument);
	}

	public CheckAssistedResponseDocument getCheckAssisted(long companyId, String council, String uprn) throws ConfigurationException, RemoteException {

		CheckAssistedDocument checkAssistedDocument = objectFactoryService.getCheckAssistedDocumentInput();
		CheckAssisted input = objectFactoryService.getCheckAssistedInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getCheckAssisted());
		checkAssistedDocument.setCheckAssisted(input);

		return getWebaspxService(companyId).checkAssisted(checkAssistedDocument);
	}

	public WebaspxConfiguration getConfigurationData(long companyId) throws ConfigurationException {

		return webaspxWasteService.getConfiguration(companyId);
	}

	public CommentsForIncabGetExistingResponseDocument getExistingCommentsForIncab(long companyId, String council, String uprn) throws ConfigurationException, RemoteException {

		CommentsForIncabGetExistingDocument commentsForIncabGetExistingDocument = objectFactoryService.getCommentsForIncabGetExistingDocumentInput();
		CommentsForIncabGetExisting input = objectFactoryService.getCommentsForIncabGetExistingInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getExistingCommentsForIncab());

		commentsForIncabGetExistingDocument.setCommentsForIncabGetExisting(input);

		return getWebaspxService(companyId).commentsForIncabGetExisting(commentsForIncabGetExistingDocument);
	}

	public GardenSubscribersDeliveriesCollectionsResponseDocument getGardenSubscribersDeliveriesCollections(long companyId, String council, String subscribersDeliveriesOrCollections,
			String dateToCheckOnYyyyMMdd) throws ConfigurationException, RemoteException {

		GardenSubscribersDeliveriesCollectionsDocument gardenSubscribersDeliveriesCollectionsDocument = objectFactoryService.getGardenSubscribersDeliveriesCollectionsDocumentInput();
		GardenSubscribersDeliveriesCollections input = objectFactoryService.getGardenSubscribersDeliveriesCollectionsInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setSubscribersDeliveriesOrCollections(subscribersDeliveriesOrCollections);
		input.setDateToCheckOnYyyyMMdd(dateToCheckOnYyyyMMdd);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getGardenSubscribersDeliveriesCollections());

		gardenSubscribersDeliveriesCollectionsDocument.setGardenSubscribersDeliveriesCollections(input);

		return getWebaspxService(companyId).garden_Subscribers_Deliveries_Collections(gardenSubscribersDeliveriesCollectionsDocument);
	}

	public GetInCabIssueTextAndIssueCodeResponseDocument getInCabIssueTextAndIssueCode(long companyId, String council) throws ConfigurationException, RemoteException {

		GetInCabIssueTextAndIssueCodeDocument getInCabIssueTextAndIssueCodeDocument = objectFactoryService.getGetInCabIssueTextAndIssueCodeDocumentInput();
		GetInCabIssueTextAndIssueCode input = objectFactoryService.getInCabIssueTextAndIssueCodeInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getInCabIssueTextAndIssueCode());
		getInCabIssueTextAndIssueCodeDocument.setGetInCabIssueTextAndIssueCode(input);

		return getWebaspxService(companyId).getInCabIssueTextAndIssueCode(getInCabIssueTextAndIssueCodeDocument);
	}

	public GetIssuesResponseDocument getIssues(long companyId, String council, String dateReq, String issueStartTime, String issueEndTime, String issueSortField, String issueSortOrder,
			String issueFilterName, String issueFilterType, String searchAllDates, String propNoName, String propStreet, String uprn) throws ConfigurationException, RemoteException {

		GetIssuesDocument getIssuesDocument = objectFactoryService.getIssuesDocumentInput();
		GetIssues input = objectFactoryService.getIssuesInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setDateReq(dateReq);
		input.setIssueStartTime(issueStartTime);
		input.setIssueEndTime(issueEndTime);
		input.setIssueSortField(issueSortField);
		input.setIssueSortOrder(issueSortOrder);
		input.setIssueFilterName(issueFilterName);
		input.setIssueFilterType(issueFilterType);
		input.setSearchAllDates(searchAllDates);
		input.setPropNoName(propNoName);
		input.setPropStreet(propStreet);
		input.setUPRN(uprn);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getIssues());
		getIssuesDocument.setGetIssues(input);

		return getWebaspxService(companyId).getIssues(getIssuesDocument);
	}

	public GetIssuesAndCollectionStatusForUPRNResponseDocument getIssuesAndCollectionStatusForUPRN(long companyId, String council, String uprn, String lastTimeOnly, String startDateyyyyMMdd,
			String endDateyyyyMMdd, String serviceFilter) throws ConfigurationException, RemoteException {

		GetIssuesAndCollectionStatusForUPRNDocument getIssuesAndCollectionStatusForUPRNDocument = objectFactoryService.getIssuesAndCollectionStatusForUPRNDocumentInput();
		GetIssuesAndCollectionStatusForUPRN input = objectFactoryService.getIssuesAndCollectionStatusForUPRNInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setLastTimeOnly(lastTimeOnly);
		input.setStartDateyyyyMMdd(startDateyyyyMMdd);
		input.setEndDateyyyyMMdd(endDateyyyyMMdd);
		input.setServiceFilter(serviceFilter);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getIssuesAndCollectionStatusForUPRN());

		getIssuesAndCollectionStatusForUPRNDocument.setGetIssuesAndCollectionStatusForUPRN(input);

		return getWebaspxService(companyId).getIssuesAndCollectionStatusForUPRN(getIssuesAndCollectionStatusForUPRNDocument);
	}

	public GetIssuesForUPRNResponseDocument getIssuesForUPRN(long companyId, String council, String dateReq, String uprn) throws ConfigurationException, RemoteException {

		GetIssuesForUPRNDocument getIssuesForUPRNDocument = objectFactoryService.getIssuesForUPRNDocumentInput();
		GetIssuesForUPRN input = objectFactoryService.getIssuesForUPRNInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setDateReq(dateReq);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getIssuesForUPRN());
		getIssuesForUPRNDocument.setGetIssuesForUPRN(input);

		return getWebaspxService(companyId).getIssuesForUPRN(getIssuesForUPRNDocument);
	}

	public LargeHouseholdsResponseDocument getLargeHouseholds(long companyId, String council, String binType) throws ConfigurationException, RemoteException {

		LargeHouseholdsDocument largeHouseholdsDocument = objectFactoryService.getLargeHouseholdsDocumentInput();
		LargeHouseholds input = objectFactoryService.getLargeHouseholdsInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setBinType(binType);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getLargeHouseholds());
		largeHouseholdsDocument.setLargeHouseholds(input);

		return getWebaspxService(companyId).largeHouseholds(largeHouseholdsDocument);
	}

	public LastCollectedResponseDocument getLastCollected(long companyId, String council, String uprn, boolean returnGPS) throws ConfigurationException, RemoteException {

		LastCollectedDocument lastCollectedDocument = objectFactoryService.lastCollectedDocumentInput();
		LastCollected input = objectFactoryService.lastCollectedInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setReturnGPS(returnGPS);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getLastCollected());
		lastCollectedDocument.setLastCollected(input);
		return getWebaspxService(companyId).lastCollected(lastCollectedDocument);
	}

	public GetLastTruckPositionResponseDocument getLastTruckPosition(long companyId, String council, String dateReq, String timeReq, String regReq) throws ConfigurationException, RemoteException {

		GetLastTruckPositionDocument lastTruckPositionDocument = objectFactoryService.getLastTruckPositionDocumentInput();
		GetLastTruckPosition input = objectFactoryService.getLastTruckPositionInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setTimeReq(timeReq);
		input.setRegReq(regReq);
		input.setDateReq(dateReq);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getLastTruckPosition());
		lastTruckPositionDocument.setGetLastTruckPosition(input);

		return getWebaspxService(companyId).getLastTruckPosition(lastTruckPositionDocument);
	}

	public GetLatestTruckPositionsResponseDocument getLatestTruckPositions(long companyId, String council, String uprn) throws ConfigurationException, RemoteException {

		GetLatestTruckPositionsDocument latestTruckPositionsDocument = objectFactoryService.getLatestTruckPositionsDocumentInput();
		GetLatestTruckPositions input = objectFactoryService.getLatestTruckPositionsInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getLatestTruckPositions());
		latestTruckPositionsDocument.setGetLatestTruckPositions(input);

		return getWebaspxService(companyId).getLatestTruckPositions(latestTruckPositionsDocument);
	}

	public LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument getLLPGXtraUpdateAvailableFieldDetails(long companyId, String council) throws ConfigurationException, RemoteException {

		LLPGXtraUpdateGetAvailableFieldDetailsDocument llpgXtraUpdateGetAvailableFieldDetailsDocument = objectFactoryService.getLLPGXtraUpdateGetAvailableFieldDetailsDocumentInput();
		LLPGXtraUpdateGetAvailableFieldDetails input = objectFactoryService.getLLPGXtraUpdateGetAvailableFieldDetailsInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getLLPGXtraUpdateAvailableFieldDetails());
		llpgXtraUpdateGetAvailableFieldDetailsDocument.setLLPGXtraUpdateGetAvailableFieldDetails(input);
		return getWebaspxService(companyId).lLPGXtraUpdateGetAvailableFieldDetails(llpgXtraUpdateGetAvailableFieldDetailsDocument);
	}

	public LLPGXtraUpdateGetUPRNForDBFieldResponseDocument getLLPGXtraUpdateUPRNForDBField(long companyId, String council, String uprNs, String databaseFieldName, String newValue) throws ConfigurationException, RemoteException {

		LLPGXtraUpdateGetUPRNForDBFieldDocument llpgXtraUpdateGetUPRNForDBFieldDocument = objectFactoryService.getLLPGXtraUpdateGetUPRNForDBFieldDocumentInput();
		LLPGXtraUpdateGetUPRNForDBField input = objectFactoryService.getLLPGXtraUpdateGetUPRNForDBFieldInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRNs(uprNs);
		input.setDatabaseFieldName(databaseFieldName);
		input.setNewValue(newValue);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getLLPGXtraUpdateUPRNForDBField());
		llpgXtraUpdateGetUPRNForDBFieldDocument.setLLPGXtraUpdateGetUPRNForDBField(input);

		return getWebaspxService(companyId).lLPGXtraUpdateGetUPRNForDBField(llpgXtraUpdateGetUPRNForDBFieldDocument);
	}

	public LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument getLLPGXtraUPRNCurrentValuesForDBField(long companyId, String council, String uprNs, String databaseFieldName) throws ConfigurationException, RemoteException {

		LLPGXtraGetUPRNCurrentValuesForDBFieldDocument llpgXtraGetUPRNCurrentValuesForDBFieldDocument = objectFactoryService.getLLPGXtraGetUPRNCurrentValuesForDBFieldDocumentInput();
		LLPGXtraGetUPRNCurrentValuesForDBField input = objectFactoryService.getLLPGXtraGetUPRNCurrentValuesForDBFieldInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRNs(uprNs);
		input.setDatabaseFieldName(databaseFieldName);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getLLPGXtraUPRNCurrentValuesForDBField());

		llpgXtraGetUPRNCurrentValuesForDBFieldDocument.setLLPGXtraGetUPRNCurrentValuesForDBField(input);

		return getWebaspxService(companyId).lLPGXtraGetUPRNCurrentValuesForDBField(llpgXtraGetUPRNCurrentValuesForDBFieldDocument);
	}

	public GetNoCollectionDatesResponseDocument getNoCollectionDates(long companyId, String council, String allService3LRoundnameUPRN, String allService3LRoundnameUPRNValue,
			String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws ConfigurationException, RemoteException {

		GetNoCollectionDatesDocument getNoCollectionDatesDocument = objectFactoryService.getNoCollectionDatesDocumentInput();
		GetNoCollectionDates input = objectFactoryService.getNoCollectionDatesServiceInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setStartDateDDsMMsYYYY(startDateDDsMMsYYYY);
		input.setEndDateDDsMMsYYYY(endDateDDsMMsYYYY);
		input.setAllService3LRoundnameUPRN(allService3LRoundnameUPRN);
		input.setAllService3LRoundnameUPRNValue(allService3LRoundnameUPRNValue);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		//input.setWebServicePassword(configuration.password()); //TODO missing password

		getNoCollectionDatesDocument.setGetNoCollectionDates(input);

		return getWebaspxService(companyId).getNoCollectionDates(getNoCollectionDatesDocument);
	}

	public GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument getPotentialCollectionDatesForNewGardenSubscriber(long companyId, String council, String uprn, String startDateDdsMMsyyyy,
			String endDateDdsMMsyyyy) throws ConfigurationException, RemoteException {

		GetPotentialCollectionDatesForNewGardenSubscriberDocument potentialCollectionDatesForNewGardenSubscriberDocument = objectFactoryService
				.getPotentialCollectionDatesForNewGardenSubscriberDocumentInput();
		GetPotentialCollectionDatesForNewGardenSubscriber input = objectFactoryService.getGetPotentialCollectionDatesForNewGardenSubscriberInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setStartDateDdsMMsyyyy(startDateDdsMMsyyyy);
		input.setEndDateDdsMMsyyyy(endDateDdsMMsyyyy);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getPotentialCollectionDatesForNewGardenSubscriber());

		potentialCollectionDatesForNewGardenSubscriberDocument.setGetPotentialCollectionDatesForNewGardenSubscriber(input);
		return getWebaspxService(companyId).getPotentialCollectionDatesForNewGardenSubscriber(potentialCollectionDatesForNewGardenSubscriberDocument);
	}

	public QueryBinEndDatesResponseDocument getQueryBinEndDates(long companyId, String council, String startDateddsMMsyyyy, String endDateddsMMsyyyy) throws ConfigurationException, RemoteException {

		QueryBinEndDatesDocument queryBinEndDatesDocument = objectFactoryService.getQueryBinEndDatesDocumentInput();
		QueryBinEndDates input = objectFactoryService.getQueryBinEndDatesInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setStartDateddsMMsyyyy(startDateddsMMsyyyy);
		input.setEndDateddsMMsyyyy(endDateddsMMsyyyy);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getQueryBinEndDates());
		queryBinEndDatesDocument.setQueryBinEndDates(input);

		return getWebaspxService(companyId).queryBinEndDates(queryBinEndDatesDocument);
	}

	public QueryBinOnTypeResponseDocument getQueryBinOnType(long companyId, String council, String binType) throws ConfigurationException, RemoteException {

		QueryBinOnTypeDocument queryBinOnTypeDocument = objectFactoryService.getQueryBinOnTypeDocumentInput();
		QueryBinOnType input = objectFactoryService.getQueryBinOnTypeInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setBinType(binType);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getQueryBinOnType());
		queryBinOnTypeDocument.setQueryBinOnType(input);

		return getWebaspxService(companyId).queryBinOnType(queryBinOnTypeDocument);
	}

	public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument getRoundAndBinInfoForUPRNForNewAdjustedDates(long companyId, String council, String uprn, String binID,
			String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws ConfigurationException, RemoteException {

		GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument roundAndBinInfoForUPRNForNewAdjustedDatesDocument = objectFactoryService.getRoundAndBinInfoForUPRNForNewAdjustedDatesDocumentInput();
		GetRoundAndBinInfoForUPRNForNewAdjustedDates input = objectFactoryService.getRoundAndBinInfoForUPRNForNewAdjustedDatesServiceInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setUPRN(uprn);
		input.setCouncil(council);
		input.setBinID(binID);
		input.setStartDateDDsMMsYYYY(startDateDDsMMsYYYY);
		input.setEndDateDDsMMsYYYY(endDateDDsMMsYYYY);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getRoundAndBinInfoForUPRNForNewAdjustedDates());
		roundAndBinInfoForUPRNForNewAdjustedDatesDocument.setGetRoundAndBinInfoForUPRNForNewAdjustedDates(input);
		return getWebaspxService(companyId).getRoundAndBinInfoForUPRNForNewAdjustedDates(roundAndBinInfoForUPRNForNewAdjustedDatesDocument);
	}

	public GetRoundCalendarForUPRNResponseDocument getRoundCalendarForUPRN(long companyId, String council, String uprn) throws ConfigurationException, RemoteException {

		GetRoundCalendarForUPRNDocument getRoundCalendarForUPRNDocument = objectFactoryService.getRoundCalendarForUPRNDocumentInput();
		GetRoundCalendarForUPRN input = objectFactoryService.getRoundCalendarForUPRNInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getRoundCalendarForUPRN());
		getRoundCalendarForUPRNDocument.setGetRoundCalendarForUPRN(input);
		return getWebaspxService(companyId).getRoundCalendarForUPRN(getRoundCalendarForUPRNDocument);
	}

	public GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument getRoundChangesInDateRangeByRoundOrUPRN(long companyId, String council, String uprn, String roundNameCrXsDaysWkNsSrv,
			String startDateDdsMMsyyyy, String endDateDdsMMsYYYY) throws ConfigurationException, RemoteException {

		GetRoundChangesInDateRangeByRoundOrUPRNDocument getRoundChangesInDateRangeByRoundOrUPRNDocument = objectFactoryService.getGetRoundChangesInDateRangeByRoundOrUPRNDocumentInput();
		GetRoundChangesInDateRangeByRoundOrUPRN input = objectFactoryService.getRoundChangesInDateRangeByRoundOrUPRNInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setRoundNameCrXsDaysWkNsSrv(roundNameCrXsDaysWkNsSrv);
		input.setStartDateDdsMMsyyyy(startDateDdsMMsyyyy);
		input.setEndDateDdsMMsYYYY(endDateDdsMMsYYYY);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getRoundChangesInDateRangeByRoundOrUPRN());

		getRoundChangesInDateRangeByRoundOrUPRNDocument.setGetRoundChangesInDateRangeByRoundOrUPRN(input);

		return getWebaspxService(companyId).getRoundChangesInDateRangeByRoundOrUPRN(getRoundChangesInDateRangeByRoundOrUPRNDocument);
	}

	public GetRoundForUPRNResponseDocument getRoundForUPRN(long companyId, String council, String uprn, String startDateDDsMMsYYYY) throws ConfigurationException, RemoteException {

		GetRoundForUPRNDocument roundForUPRNDocument = objectFactoryService.getRoundForUPRNDocumentInput();
		GetRoundForUPRN input = objectFactoryService.getRoundForUPRNInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setStartDateDDsMMsYYYY(startDateDDsMMsYYYY);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getRoundForUPRN());
		roundForUPRNDocument.setGetRoundForUPRN(input);

		return getWebaspxService(companyId).getRoundForUPRN(roundForUPRNDocument);
	}

	public GetRoundNameForUPRNServiceResponseDocument getRoundNameForUPRNService(long companyId, String council, String uprn, String service3L) throws ConfigurationException, RemoteException {

		GetRoundNameForUPRNServiceDocument getRoundNameForUPRNServiceDocument = objectFactoryService.getRoundNameForUPRNServiceDocumentInput();
		GetRoundNameForUPRNService input = objectFactoryService.getRoundNameForUPRNServiceInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setService3L(service3L);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_getRoundNameForUPRNService());
		getRoundNameForUPRNServiceDocument.setGetRoundNameForUPRNService(input);

		return getWebaspxService(companyId).getRoundNameForUPRNService(getRoundNameForUPRNServiceDocument);
	}

	public GetTrucksOutTodayResponseDocument getTrucksOutToday(long companyId, String council, String dateReq, String timeReq) throws ConfigurationException, RemoteException {

		GetTrucksOutTodayDocument trucksOutTodayDocument = objectFactoryService.getTrucksOutTodayDocumentInput();
		GetTrucksOutToday input = objectFactoryService.getTrucksOutTodayInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setTimeReq(timeReq);
		input.setDateReq(dateReq);
		input.setUsername(configuration.userNameIncab());
		input.setUsernamePassword(configuration.userNamePasswordIncab());
		input.setWebServicePassword(configuration.password_getTrucksOutToday());
		trucksOutTodayDocument.setGetTrucksOutToday(input);

		return getWebaspxService(companyId).getTrucksOutToday(trucksOutTodayDocument);
	}

	private WSCollExternalStub getWebaspxService(long companyId) throws ConfigurationException, RemoteException {

		return webaspxAxisService.getWebaspxAxisService(companyId);
	}

	public BinInsertResponseDocument insertBinDetails(long companyId, String council, String uprn, String binType, String payRef, String deliverYN, String collectYN, String startDateDDsMMsYYYY,
			String endDateDDsMMsYYYY, String reportedDateDDsMMsYYYY, String completedDateDDsMMsYYYY, String noOfNewContainersRequired) throws ConfigurationException, RemoteException {

		BinInsertDocument binInsertDocument = objectFactoryService.getBinInsertDocumentInput();
		BinInsert input = objectFactoryService.getBinInsertInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setPayRef(payRef);
		input.setDeliverYN(deliverYN);
		input.setCollectYN(collectYN);
		input.setStartDateDDsMMsYYYY(startDateDDsMMsYYYY);
		input.setEndDateDDsMMsYYYY(endDateDDsMMsYYYY);
		input.setBinType(binType);
		input.setReportedDateDDsMMsYYYY(reportedDateDDsMMsYYYY);
		input.setCompletedDateDDsMMsYYYY(completedDateDDsMMsYYYY);
		input.setNoOfNewContainersRequired(noOfNewContainersRequired);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_insertBinDetails());
		binInsertDocument.setBinInsert(input);

		return getWebaspxService(companyId).binInsert(binInsertDocument);
	}

	public ReadWriteMissedBinResponseDocument readWriteMissedBin(long companyId, String council, String uprn, String service3L, String missedYN, String missedDateDDsMMsYY) throws ConfigurationException, RemoteException {

		ReadWriteMissedBinDocument roundForUPRNDocument = objectFactoryService.getReadWriteMissedBinDocumentInput();
		ReadWriteMissedBin input = objectFactoryService.readWriteMissedBinInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setService3L(service3L);
		input.setMissedYN(missedYN);
		input.setMissedDateDDsMMsYY(missedDateDDsMMsYY);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_readWriteMissedBin());
		roundForUPRNDocument.setReadWriteMissedBin(input);

		return getWebaspxService(companyId).readWriteMissedBin(roundForUPRNDocument);
	}

	public ReadWriteMissedBinByContainerTypeResponseDocument readWriteMissedBinByContainerType(long companyId, String council, String uprn, String containerType, String missedYN,
			String missedDateDDsMMsYY) throws ConfigurationException, RemoteException {

		ReadWriteMissedBinByContainerTypeDocument readWriteMissedBinByContainerTypeDocument = objectFactoryService.getReadWriteMissedBinByContainerTypeDocumentInput();
		ReadWriteMissedBinByContainerType input = objectFactoryService.getReadWriteMissedBinByContainerTypeInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setContainerType(containerType);
		input.setMissedYN(missedYN);
		input.setMissedDateDDsMMsYY(missedDateDDsMMsYY);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_readWriteMissedBinByContainerType());
		readWriteMissedBinByContainerTypeDocument.setReadWriteMissedBinByContainerType(input);

		return getWebaspxService(companyId).readWriteMissedBinByContainerType(readWriteMissedBinByContainerTypeDocument);
	}

	public GardenRemoveSubscriptionResponseDocument removeGardenSubscription(long companyId, String council, String uprn, String endDateDdsMMsyyyy) throws ConfigurationException, RemoteException {

		GardenRemoveSubscriptionDocument gardenRemoveSubscriptionDocument = objectFactoryService.getGardenRemoveSubscriptionDocumentInput();
		GardenRemoveSubscription input = objectFactoryService.getGardenRemoveSubscriptionInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setEndDateDdsMMsyyyy(endDateDdsMMsyyyy);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_removeGardenSubscription());
		gardenRemoveSubscriptionDocument.setGardenRemoveSubscription(input);

		return getWebaspxService(companyId).garden_RemoveSubscription(gardenRemoveSubscriptionDocument);
	}

	public ShowAdditionalBinsResponseDocument showAdditionalBins(long companyId, String council, String service, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws ConfigurationException, RemoteException {

		ShowAdditionalBinsDocument showAdditionalBinsDocument = objectFactoryService.getShowAdditionalBinsDocumentInput();
		ShowAdditionalBins input = objectFactoryService.getShowAdditionalBinsInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setService(service);
		input.setStartDateDDsMMsYYYY(startDateDDsMMsYYYY);
		input.setEndDateDDsMMsYYYY(endDateDDsMMsYYYY);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_showAdditionalBins());

		showAdditionalBinsDocument.setShowAdditionalBins(input);

		return getWebaspxService(companyId).showAdditionalBins(showAdditionalBinsDocument);
	}

	public UpdateAssistedResponseDocument updateAssisted(long companyId, String council, String uprn, String assistedYN01234, String assistedStartDateDDsMMsYYYY, String assistedEndDateDDsMMsYYYY,
			String assistedLocation) throws ConfigurationException, RemoteException {

		UpdateAssistedDocument updateAssistedDocument = objectFactoryService.getUpdateAssistedDocumentInput();
		UpdateAssisted input = objectFactoryService.getUpdateAssistedInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setAssistedYN01234(assistedYN01234);
		input.setAssistedStartDateDDsMMsYYYY(assistedStartDateDDsMMsYYYY);
		input.setAssistedEndDateDDsMMsYYYY(assistedEndDateDDsMMsYYYY);
		input.setAssistedLocation(assistedLocation);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_updateAssisted());
		updateAssistedDocument.setUpdateAssisted(input);

		return getWebaspxService(companyId).updateAssisted(updateAssistedDocument);
	}

	public BinUpdateResponseDocument updateBin(long companyId, String council, String uprn, String binID, String binType, String payRef, String deliverYN, String collectYN, String startDateDDsMMsYYYY,
			String endDateDDsMMsYYYY, String reportedDateDDsMMsYYYY, String completedDateDDsMMsYYYY, String leaveBlanksAsIsYN) throws ConfigurationException, RemoteException {

		BinUpdateDocument binUpdateDocument = objectFactoryService.getBinUpdateDocumentInput();
		BinUpdate input = objectFactoryService.getBinUpdateInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setBinID(binID);
		input.setBinType(binType);
		input.setPayRef(payRef);
		input.setDeliverYN(deliverYN);
		input.setCollectYN(collectYN);
		input.setStartDateDDsMMsYYYY(startDateDDsMMsYYYY);
		input.setEndDateDDsMMsYYYY(endDateDDsMMsYYYY);
		input.setReportedDateDDsMMsYYYY(reportedDateDDsMMsYYYY);
		input.setCompletedDateDDsMMsYYYY(completedDateDDsMMsYYYY);
		input.setLeaveBlanksAsIsYN(leaveBlanksAsIsYN);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_updateBin());
		binUpdateDocument.setBinUpdate(input);

		return getWebaspxService(companyId).binUpdate(binUpdateDocument);
	}

	public CommentsForIncabUpdateResponseDocument updateCommentsForIncab(long companyId, String council, String uprn, String newComment, String appendOrReplace) throws ConfigurationException, RemoteException {

		CommentsForIncabUpdateDocument commentsForIncabUpdateDocument = objectFactoryService.getCommentsForIncabUpdateDocumentInput();
		CommentsForIncabUpdate input = objectFactoryService.getCommentsForIncabUpdateInput();

		WebaspxConfiguration configuration = getConfigurationData(companyId);

		input.setCouncil(council);
		input.setUPRN(uprn);
		input.setNewComment(newComment);
		input.setAppendOrReplace(appendOrReplace);
		input.setUsername(configuration.userName());
		input.setUsernamePassword(configuration.userNamePassword());
		input.setWebServicePassword(configuration.password_updateCommentsForIncab());

		commentsForIncabUpdateDocument.setCommentsForIncabUpdate(input);

		return getWebaspxService(companyId).commentsForIncabUpdate(commentsForIncabUpdateDocument);
	}


}
