package com.placecube.digitalplace.local.waste.webaspx.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.webaspx.configuration.WebaspxConfiguration;
import com.placecube.digitalplace.local.waste.webaspx.constants.WebaspxWasteConstants;

@Component(immediate = true, service = WebaspxWasteService.class)
public class WebaspxWasteService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private ObjectFactoryService objectFactoryService;

	public WebaspxConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(WebaspxConfiguration.class, companyId);
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		WebaspxConfiguration configuration = configurationProvider.getCompanyConfiguration(WebaspxConfiguration.class, companyId);
		return configuration.enabled();
	}

	public String getWebServiceUrl(WebaspxConfiguration configuration) throws ConfigurationException {
		String webServiceUrl = configuration.webServiceUrl();

		if (Validator.isNull(webServiceUrl)) {
			throw new ConfigurationException("Web service URL configuration cannot be empty");
		}

		return webServiceUrl;
	}

	public String getUserName(WebaspxConfiguration configuration) throws ConfigurationException {
		String userName = configuration.userName();

		if (Validator.isNull(userName)) {
			throw new ConfigurationException("User name configuration cannot be empty");
		}

		return userName;
	}

	public String getUserNamePassword(WebaspxConfiguration configuration) throws ConfigurationException {
		String userNamePassword = configuration.userNamePassword();

		if (Validator.isNull(userNamePassword)) {
			throw new ConfigurationException("User Name password configuration cannot be empty");
		}

		return userNamePassword;
	}

	public Optional<BinCollection> createBinCollection(String name, JSONArray jsonRoundAndBin, String type) {
		for (int i = 0; i < jsonRoundAndBin.length(); i++) {
			JSONObject object = jsonRoundAndBin.getJSONObject(i);

			if (object.getString("Bin_number").toLowerCase().contains(type)) {
				JSONArray dates = object.getJSONArray("CollectionDate");
				Map<Locale, String> labelMap = new HashMap<>();
				labelMap.put(LocaleUtil.getDefault(), object.getString("Bin_number"));

				return Optional.of(objectFactoryService
						.createBinCollectionBuilder(name.split(":")[0], LocalDate.parse(dates.get(0).toString(), DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT)), labelMap) //
						.frequency(StringUtil.upperCaseFirstLetter(name.split("then ")[1])) //
						.followingCollectionDate(LocalDate.parse(dates.get(1).toString(), DateTimeFormatter.ofPattern(WebaspxWasteConstants.DATE_FORMAT))) //
						.formattedCollectionDate(dates.get(1).toString()) //
						.build());
			}
		}

		return Optional.empty();
	}
}
