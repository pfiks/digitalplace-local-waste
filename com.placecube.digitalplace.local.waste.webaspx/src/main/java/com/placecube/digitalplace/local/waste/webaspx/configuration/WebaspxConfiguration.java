package com.placecube.digitalplace.local.waste.webaspx.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.waste.webaspx.configuration.WebaspxConfiguration", localization = "content/Language", name = "Waste - Webaspx")
public interface WebaspxConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "", name = "web-service-url")
	String webServiceUrl();

	@Meta.AD(required = false, deflt = "", name = "user-name")
	String userName();

	@Meta.AD(required = false, deflt = "", name = "username-password", type = Meta.Type.Password)
	String userNamePassword();

	@Meta.AD(required = false, deflt = "", name = "user-name-incab")
	String userNameIncab();

	@Meta.AD(required = false, deflt = "", name = "username-password-incab", type = Meta.Type.Password)
	String userNamePasswordIncab();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-BinDelete", type = Meta.Type.Password)
	String password_deleteBin();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-BinInsert", type = Meta.Type.Password)
	String password_insertBinDetails();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-BinUpdate", type = Meta.Type.Password)
	String password_updateBin();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-CachedCalendar", type = Meta.Type.Password)
	String password_getCachedCalendar();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-CheckAssisted", type = Meta.Type.Password)
	String password_getCheckAssisted();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-CommentsForIncabGetExisting", type = Meta.Type.Password)
	String password_getExistingCommentsForIncab();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-CommentsForIncabUpdate", type = Meta.Type.Password)
	String password_updateCommentsForIncab();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GardenSubscription", type = Meta.Type.Password)
	String password_addOrUpdateGardenSubscription();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-Garden_ChangeAddress", type = Meta.Type.Password)
	String password_changeGardenAddress();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-Garden_RemoveSubscription", type = Meta.Type.Password)
	String password_removeGardenSubscription();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-Garden_Subscribers_Deliveries_Collections", type = Meta.Type.Password)
	String password_getGardenSubscribersDeliveriesCollections();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getPotentialCollectionDatesForNewGardenSubscriber", type = Meta.Type.Password)
	String password_getPotentialCollectionDatesForNewGardenSubscriber();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetAddressOrUPRN", type = Meta.Type.Password)
	String password_getAddressOrUPRN();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetAllRoundDetails", type = Meta.Type.Password)
	String password_getAllRoundDetails();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetAllUPRNsForDate", type = Meta.Type.Password)
	String password_getAllUPRNsForDate();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetAvailableContainers", type = Meta.Type.Password)
	String password_getAvailableContainers();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetAvailableContainersForClientOrProperty", type = Meta.Type.Password)
	String password_getAvailableContainersForClientOrProperty();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetAvailableRounds", type = Meta.Type.Password)
	String password_getAvailableRounds();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetBinDetailsForService", type = Meta.Type.Password)
	String password_getBinDetailsForService();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetInCabIssueTextAndIssueCode", type = Meta.Type.Password)
	String password_getInCabIssueTextAndIssueCode();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetIssues", type = Meta.Type.Password)
	String password_getIssues();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetIssuesForUPRN", type = Meta.Type.Password)
	String password_getIssuesForUPRN();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetLastTruckPosition", type = Meta.Type.Password)
	String password_getLastTruckPosition();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetTrucksOutToday", type = Meta.Type.Password)
	String password_getTrucksOutToday();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-LLPGXtraGetUPRNCurrentValuesForDBField", type = Meta.Type.Password)
	String password_getLLPGXtraUPRNCurrentValuesForDBField();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-LLPGXtraUpdateGetAvailableFieldDetails", type = Meta.Type.Password)
	String password_getLLPGXtraUpdateAvailableFieldDetails();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-LLPGXtraUpdateGetUPRNForDBField", type = Meta.Type.Password)
	String password_getLLPGXtraUpdateUPRNForDBField();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-LargeHouseholds", type = Meta.Type.Password)
	String password_getLargeHouseholds();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-LastCollected", type = Meta.Type.Password)
	String password_getLastCollected();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-QueryBinEndDates", type = Meta.Type.Password)
	String password_getQueryBinEndDates();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-QueryBinOnType", type = Meta.Type.Password)
	String password_getQueryBinOnType();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-ShowAdditionalBins", type = Meta.Type.Password)
	String password_showAdditionalBins();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-UpdateAssisted", type = Meta.Type.Password)
	String password_updateAssisted();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-countBinsForService", type = Meta.Type.Password)
	String password_countBinsForService();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-countSubscriptions", type = Meta.Type.Password)
	String password_countSubscriptions();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getAddressFromPostcode", type = Meta.Type.Password)
	String password_getAddressFromPostcode();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getAllIssuesForIssueType", type = Meta.Type.Password)
	String password_getAllIssuesForIssueType();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getIssuesAndCollectionStatusForUPRN", type = Meta.Type.Password)
	String password_getIssuesAndCollectionStatusForUPRN();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getLatestTruckPositions", type = Meta.Type.Password)
	String password_getLatestTruckPositions();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getRoundAndBinInfoForUPRNForNewAdjustedDates", type = Meta.Type.Password)
	String password_getRoundAndBinInfoForUPRNForNewAdjustedDates();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getRoundCalendarForUPRN", type = Meta.Type.Password)
	String password_getRoundCalendarForUPRN();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getRoundForUPRN", type = Meta.Type.Password)
	String password_getRoundForUPRN();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-getRoundNameForUPRNService", type = Meta.Type.Password)
	String password_getRoundNameForUPRNService();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-readWriteMissedBin", type = Meta.Type.Password)
	String password_readWriteMissedBin();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-readWriteMissedBinByContainerType", type = Meta.Type.Password)
	String password_readWriteMissedBinByContainerType();

	@Meta.AD(required = false, deflt = "", name = "web-service-password-GetRoundChangesInDateRangeByRoundOrUPRN", type = Meta.Type.Password)
	String password_getRoundChangesInDateRangeByRoundOrUPRN();
}