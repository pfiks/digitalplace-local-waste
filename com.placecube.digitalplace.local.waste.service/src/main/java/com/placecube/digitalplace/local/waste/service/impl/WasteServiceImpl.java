package com.placecube.digitalplace.local.waste.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.overrideaction.internal.WasteServiceOverrideActionExecutor;
import com.placecube.digitalplace.local.waste.service.WasteConnector;
import com.placecube.digitalplace.local.waste.service.WasteService;

@Component(immediate = true, service = WasteService.class)
public class WasteServiceImpl implements WasteService {

	@Reference
	WasteServiceOverrideActionExecutor wasteServiceOverrideActionExecutor;

	private Set<WasteConnector> wasteConnectors = new LinkedHashSet<>();

	@Override
	public String createAssistedBinCollectionJob(long companyId, AssistedBinCollectionJob assistedBinCollectionJob) throws WasteRetrievalException {

		validateUPRN(assistedBinCollectionJob.getUprn());

		return getWasteConnector(companyId).createAssistedBinCollectionJob(companyId, assistedBinCollectionJob);
	}

	@Override
	public WasteSubscriptionResponse createBinSubscription(long companyId, String uprn, String serviceType, String firstName, String lastName, String emailAddress, String telephone, int numberOfBins)
			throws WasteRetrievalException {

		validateUPRN(uprn);

		return getWasteConnector(companyId).createBinSubscription(companyId, uprn, serviceType, firstName, lastName, emailAddress, telephone, numberOfBins);
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, List<String> itemsToBeCollected,
			String locationOfItems, Date collectionDate) throws WasteRetrievalException {
		validateUPRN(uprn);

		return getWasteConnector(companyId).createBulkyWasteCollectionJob(companyId, uprn, firstName, lastName, emailAddress, telephone, itemsToBeCollected, locationOfItems, collectionDate);
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, BulkyWasteCollectionRequest bulkyWasteCollectionRequest, Date date) throws WasteRetrievalException {
		validateUPRN(bulkyWasteCollectionRequest.getUprn());

		return getWasteConnector(companyId).createBulkyWasteCollectionJob(companyId, bulkyWasteCollectionRequest, date);
	}

	@Override
	public String createFlyTippingReport(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, String typeOfRubbish, String sizeOfRubbish,
			String details, String coordinates, String locationDetails, String dateOfIncident) throws WasteRetrievalException {

		validateUPRN(uprn);

		return getWasteConnector(companyId).createFlyTippingReport(companyId, uprn, firstName, lastName, emailAddress, telephone, typeOfRubbish, sizeOfRubbish, details, coordinates, locationDetails,
				dateOfIncident);
	}

	@Override
	public WasteSubscriptionResponse createGardenWasteSubscription(long companyId, GardenWasteSubscription gardenWasteSubscription) throws WasteRetrievalException {

		validateUPRN(gardenWasteSubscription.getUprn());

		WasteSubscriptionResponse wasteSubscriptionResponse = getWasteConnector(companyId).createGardenWasteSubscription(companyId, gardenWasteSubscription);

		return wasteServiceOverrideActionExecutor.executeActionsAfterWasteSubscriptionResponseCreation(companyId, wasteSubscriptionResponse);
	}

	@Override
	public String createMissedBinCollectionJob(long companyId, MissedBinCollectionJob missedBinCollectionJob) throws WasteRetrievalException {

		validateUPRN(missedBinCollectionJob.getUprn());

		return getWasteConnector(companyId).createMissedBinCollectionJob(companyId, missedBinCollectionJob);
	}

	@Override
	public String createNewWasteContainerRequest(long companyId, NewContainerRequest newContainerRequest) throws WasteRetrievalException {
		validateUPRN(newContainerRequest.getUprn());

		return getWasteConnector(companyId).createNewWasteContainerRequest(companyId, newContainerRequest);
	}

	@Override
	public List<WasteSubscriptionResponse> getAllWasteSubscriptions(long companyId, SubscriptionType subscriptionType) throws WasteRetrievalException {
		return getWasteConnector(companyId).getAllWasteSubscriptions(companyId, subscriptionType);
	}

	@Override
	public Optional<BinCollection> getBinCollectionByUPRNAndName(long companyId, String uprn, String name) throws WasteRetrievalException {

		validateUPRN(uprn);

		Set<BinCollection> binCollections = getWasteConnector(companyId).getBinCollectionDatesByUPRN(companyId, uprn);

		return binCollections.stream().filter(bc -> name.equals(bc.getName())).findFirst();
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRN(long companyId, String uprn) throws WasteRetrievalException {
		validateUPRN(uprn);

		return getWasteConnector(companyId).getBinCollectionDatesByUPRN(companyId, uprn);
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRNAndService(long companyId, String uprn, String serviceName, String formInstanceId) throws WasteRetrievalException {
		validateUPRN(uprn);
		return getWasteConnector(companyId).getBinCollectionDatesByUPRNAndService(companyId, uprn, serviceName, formInstanceId);
	}

	@Override
	public List<Date> getBulkyCollectionSlotsByUPRNAndService(long companyId, String uprn, String serviceId, int numberOfDatesToReturn) throws WasteRetrievalException {
		validateUPRN(uprn);

		return getWasteConnector(companyId).getBulkyCollectionSlotsByUPRNAndService(companyId, uprn, serviceId, numberOfDatesToReturn);
	}

	@Override
	public Optional<BulkyCollectionDate> getBulkyCollectionDateByUPRN(long companyId, String uprn) throws WasteRetrievalException {

		validateUPRN(uprn);

		return getWasteConnector(companyId).getBulkyCollectionDateByUPRN(companyId, uprn);

	}

	@Override
	public String getMissedBinCollectionResponse(long companyId, String uprn, String date) throws WasteRetrievalException {

		validateUPRN(uprn);

		return getWasteConnector(companyId).getMissedBinCollectionResponse(companyId, uprn, date);
	}

	public WasteConnector getWasteConnector(long companyId) throws WasteRetrievalException {
		Optional<WasteConnector> wasteConnector = wasteConnectors.stream().filter(entry -> entry.enabled(companyId)).findFirst();
		if (wasteConnector.isPresent()) {
			return wasteConnector.get();
		}
		throw new WasteRetrievalException("No waste connector configured");
	}

	@Override
	public List<String> getWasteConnectorList(long companyId) throws WasteRetrievalException {
		List<String> wasteConnectorList = new ArrayList<>();
		if (wasteConnectors.isEmpty()) {
			throw new WasteRetrievalException("No waste connector found");
		}
		for (WasteConnector wasteConnector : wasteConnectors) {
			wasteConnectorList.add(wasteConnector.getName(companyId));
		}
		return wasteConnectorList;
	}

	@Override
	public int howManyBinsSubscribed(long companyId, String uprn, String serviceType) throws WasteRetrievalException {

		validateUPRN(uprn);

		return getWasteConnector(companyId).howManyBinsSubscribed(companyId, uprn, serviceType);
	}

	@Override
	public int howManyBinsSubscribedForPeriod(long companyId, String uprn, String serviceType, String startDate, String endDate) throws WasteRetrievalException {

		validateUPRN(uprn);

		return getWasteConnector(companyId).howManyBinsSubscribedForPeriod(companyId, uprn, serviceType, startDate, endDate);
	}

	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String binType, String formInstanceId)
			throws WasteRetrievalException {

		validateUPRN(uprn);

		return getWasteConnector(companyId).isPropertyEligibleForGardenWasteSubscription(companyId, uprn, startDateDDsMMsYYYY, endDateDDsMMsYYYY, binType, formInstanceId);
	}

	@Override
	public boolean isPropertyEligibleForBulkyWasteCollection(long companyId, String uprn, List<String> itemsForCollection, String formInstanceId) throws WasteRetrievalException {
		validateUPRN(uprn);

		return getWasteConnector(companyId).isPropertyEligibleForBulkyWasteCollection(companyId, uprn, itemsForCollection, formInstanceId);
	}

	@Override
	public boolean isPropertyEligibleForMissedBinCollectionReport(long companyId, String uprn, String binType, String formInstanceId) throws WasteRetrievalException {
		validateUPRN(uprn);

		return getWasteConnector(companyId).isPropertyEligibleForMissedBinCollectionReport(companyId, uprn, binType, formInstanceId);
	}

	@Override
	public boolean isPropertyEligibleForWasteContainerRequest(long companyId, String uprn, String binType, String binSize, String formInstanceId) throws WasteRetrievalException {
		validateUPRN(uprn);

		return getWasteConnector(companyId).isPropertyEligibleForWasteContainerRequest(companyId, uprn, binType, binSize, formInstanceId);
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setWasteConnector(WasteConnector wasteConnector) {
		wasteConnectors.add(wasteConnector);
	}

	protected void unsetWasteConnector(WasteConnector wasteConnector) {
		wasteConnectors.remove(wasteConnector);
	}

	private void validateUPRN(String uprn) throws WasteRetrievalException {
		if (Validator.isNull(uprn)) {
			throw new WasteRetrievalException("UPRN is empty or null");
		}
	}

	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDate, String endDate) throws WasteRetrievalException {
		validateUPRN(uprn);

		return getWasteConnector(companyId).isPropertyEligibleForGardenWasteSubscription(companyId, uprn, startDate, endDate);
	}
}
