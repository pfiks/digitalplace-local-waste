package com.placecube.digitalplace.local.waste.overrideaction.internal;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.overrideaction.WasteServiceOverrideAction;

@Component(immediate = true, service = WasteServiceOverrideActionExecutor.class)
public class WasteServiceOverrideActionExecutor {

	@Reference
	private WasteServiceOverrideActionRegistry wasteServiceOverrideActionRegistry;

	public WasteSubscriptionResponse executeActionsAfterWasteSubscriptionResponseCreation(long companyId, WasteSubscriptionResponse wasteSubscriptionResponse) {
		List<WasteServiceOverrideAction> wasteServiceActions = wasteServiceOverrideActionRegistry.getWasteServiceOverrideActions();

		for (WasteServiceOverrideAction wasteServiceOverrideAction : wasteServiceActions) {
			wasteSubscriptionResponse = wasteServiceOverrideAction.executeOnAfterWasteSubscriptionResponseCreation(companyId, wasteSubscriptionResponse);
		}

		return wasteSubscriptionResponse;
	}

}
