package com.placecube.digitalplace.local.waste.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.waste.configuration.WasteConfiguration", localization = "content/Language", name = "Waste - Default")
public interface WasteConfiguration {

	@Meta.AD(name = "default", required = false)
	public String defaultWasteConnector();
}