package com.placecube.digitalplace.local.waste.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.configuration.admin.definition.ConfigurationFieldOptionsProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.placecube.digitalplace.local.waste.service.WasteService;

@Component(property = { "configuration.field.name=defaultWasteConnector",
		"configuration.pid=com.placecube.digitalplace.local.waste.configuration.WasteConfiguration" }, service = ConfigurationFieldOptionsProvider.class)
public class WasteConfigurationFieldIOptionProvider implements ConfigurationFieldOptionsProvider {

	private static final Log LOG = LogFactoryUtil.getLog(WasteConfigurationFieldIOptionProvider.class);

	@Reference
	private WasteService wasteService;

	@Override
	public List<Option> getOptions() {
		List<String> wasteConnector = new ArrayList<>();

		try {
			wasteConnector = wasteService.getWasteConnectorList(CompanyThreadLocal.getCompanyId());
		} catch (Exception e) {
			LOG.error("There was an error retrieving the waste connector list - " + e.getMessage(), e);
		}
		Stream<String> stream = wasteConnector.stream();

		return stream.map(wasteConnectors -> new Option() {

			@Override
			public String getLabel(Locale locale) {
				return wasteConnectors;
			}

			@Override
			public String getValue() {
				return wasteConnectors;
			}

		}).collect(Collectors.toList());

	}

}
