package com.placecube.digitalplace.local.waste.application;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.TimeZoneUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.local.waste.constants.WasteConstants;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.helper.AppointmentBookingEntryHelper;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.service.WasteService;

@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/waste", JaxrsWhiteboardConstants.JAX_RS_NAME + "=Waste.Rest", "oauth2.scopechecker.type=none",
		"auth.verifier.guest.allowed=true", "liferay.access.control.disable=true" }, service = Application.class)
public class WasteRESTApplication extends Application {

	protected static final String APPOINMENT_DATE_PATTERN = "EEE d MMM yyyy";

	protected static final int DFLT_ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS = 10;

	private static final Log LOG = LogFactoryUtil.getLog(WasteRESTApplication.class);

	@Reference
	private AppointmentBookingEntryHelper appointmentBookingEntryHelper;

	@Reference
	private AppointmentBookingService appointmentBookingService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private WasteService wasteService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create-assisted-collection-job")
	public Response createAssistedBinCollectionJob(AssistedBinCollectionJob assistedBinCollectionJob, @Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			String reference = wasteService.createAssistedBinCollectionJob(companyId, assistedBinCollectionJob);
			responseBuilder.entity(reference);
		} catch (Exception e) {
			LOG.error("Assisted bin collection creation job failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create-bin-subscription/{uprn}/{serviceType}/{firstName}/{lastName}/{emailAddress}/{telephone}/{numberOfBins}")
	public Response createBinSubscription(@PathParam("uprn") String uprn, @PathParam("serviceType") String serviceType, @PathParam("firstName") String firstName,
			@PathParam("lastName") String lastName, @PathParam("emailAddress") String emailAddress, @PathParam("telephone") String telephone, @PathParam("numberOfBins") int numberOfBins,
			@Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			WasteSubscriptionResponse wasteSubscriptionResponse = wasteService.createBinSubscription(companyId, uprn, serviceType, firstName, lastName, emailAddress, telephone, numberOfBins);
			responseBuilder.entity(wasteSubscriptionResponse.getSubscriptionRef());
		} catch (Exception e) {
			LOG.error("Create bin subscription failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create-bulky-waste-collection-job/{uprn}/{firstName}/{lastName}/{emailAddress}/{telephone}/{itemsToBeCollected}/{locationOfItems}/{collectionDate}")
	public Response createBulkyWasteCollectionJob(@PathParam("uprn") String uprn, @PathParam("firstName") String firstName, @PathParam("lastName") String lastName,
			@PathParam("emailAddress") String emailAddress, @PathParam("telephone") String telephone, @PathParam("itemsToBeCollected") List<String> itemsToBeCollected,
			@PathParam("locationOfItems") String locationOfItems, @PathParam("collectionDate") Date collectionDate, @Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			String reference = wasteService.createBulkyWasteCollectionJob(companyId, uprn, firstName, lastName, emailAddress, telephone, itemsToBeCollected, locationOfItems, collectionDate);
			responseBuilder.entity(reference);
		} catch (Exception e) {
			LOG.error("Create bulky waste collection job failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create-bulky-waste-collection-with-quantities")
	public Response createBulkyWasteCollection(BulkyWasteCollectionRequest bulkyWasteCollectionRequest, @Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			ThemeDisplay themeDisplay = getThemeDisplay(request);
			Locale locale = themeDisplay != null ? themeDisplay.getLocale() : LocaleUtil.getDefault();

			Date date = DateUtil.parseDate(APPOINMENT_DATE_PATTERN, bulkyWasteCollectionRequest.getDate(), locale);

			String reference = wasteService.createBulkyWasteCollectionJob(companyId, bulkyWasteCollectionRequest, date);
			responseBuilder.entity(reference);
		} catch (Exception e) {
			LOG.error("Create bulky waste collection failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Create bulky waste collection with quantities execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create-fly-tipping-report/{uprn}/{firstName}/{lastName}/{emailAddress}/{telephone}/{typeOfRubbish}/{sizeOfRubbish}/{details}/{coordinates}/{locationDetails}/{dateOfIncident}")
	public Response createFlyTippingReport(@PathParam("uprn") String uprn, @PathParam("firstName") String firstName, @PathParam("lastName") String lastName,
			@PathParam("emailAddress") String emailAddress, @PathParam("telephone") String telephone, @PathParam("typeOfRubbish") String typeOfRubbish,
			@PathParam("sizeOfRubbish") String sizeOfRubbish, @PathParam("details") String details, @PathParam("coordinates") String coordinates, @PathParam("locationDetails") String locationDetails,
			@PathParam("dateOfIncident") String dateOfIncident, @Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			String reference = wasteService.createFlyTippingReport(companyId, uprn, firstName, lastName, emailAddress, telephone, typeOfRubbish, sizeOfRubbish, details, coordinates, locationDetails,
					dateOfIncident);
			responseBuilder.entity(reference);
		} catch (Exception e) {
			LOG.error("Create fly tipping report failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create-garden-waste-subscription")
	public Response createGardenWasteSubscription(GardenWasteSubscription gardenWasteSubscription, @Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			WasteSubscriptionResponse wasteSubscriptionResponse = wasteService.createGardenWasteSubscription(companyId, gardenWasteSubscription);
			responseBuilder.entity(wasteSubscriptionResponse.getSubscriptionRef());
		} catch (Exception e) {
			LOG.error("Create garden waste subscription failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Create garden waste subscription execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create-missed-collection-job")
	public Response createMissedBinCollectionJob(MissedBinCollectionJob missedBinCollectionJob, @Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			String reference = wasteService.createMissedBinCollectionJob(companyId, missedBinCollectionJob);
			responseBuilder.entity(reference);
		} catch (Exception e) {
			LOG.error("Create missed bin collection job failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Create missed bin collection job execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create-new-waste-container-request")
	public Response createNewWasteContainerRequest(NewContainerRequest newContainerRequest, @Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			String reference = wasteService.createNewWasteContainerRequest(companyId, newContainerRequest);
			responseBuilder.entity(reference);
		} catch (Exception e) {
			LOG.error("Create new waste container request failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Create new waste container request execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@GET
	@Path("/collection-dates-by-uprn/{uprn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBinCollectionDatesByUPRN(@PathParam("uprn") String uprn, @Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		long companyId = getCompanyId(request);
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		Locale locale = LocaleUtil.getDefault();
		if (themeDisplay != null) {
			locale = themeDisplay.getLocale();
		}
		try {
			final Locale binCollectionLocale = locale;

			Set<BinCollection> binCollections = wasteService.getBinCollectionDatesByUPRN(companyId, uprn);

			JSONArray array = jsonFactory.createJSONArray();

			binCollections.stream().filter(Objects::nonNull).map(x -> binCollectionToJSONObject(x, binCollectionLocale)).forEach(array::put);

			addMissingBinCollections(array, 3);

			LOG.debug("binCollections: " + array.toJSONString());

			responseBuilder.entity(array.toJSONString());
		} catch (Exception e) {
			LOG.error("Get bin collection dates by UPRN failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	@GET
	@Path("/collection-dates-by-uprn-and-service/{uprn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBinCollectionDatesByUPRNAndService(@PathParam("uprn") String uprn, @QueryParam("serviceName") String serviceName, @QueryParam("formInstanceId") String formInstanceId,
			@Context HttpServletRequest request) {

		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		long companyId = getCompanyId(request);
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		Locale locale = LocaleUtil.getDefault();
		if (themeDisplay != null) {
			locale = themeDisplay.getLocale();
		}

		try {
			final Locale binCollectionLocale = locale;

			Set<BinCollection> binCollections = wasteService.getBinCollectionDatesByUPRNAndService(companyId, uprn, serviceName, formInstanceId);

			JSONArray array = jsonFactory.createJSONArray();

			binCollections.stream().filter(Objects::nonNull).map(x -> binCollectionToJSONObject(x, binCollectionLocale)).forEach(array::put);

			addMissingBinCollections(array, 3);

			LOG.debug("binCollections: " + array.toJSONString());

			responseBuilder.entity(array.toJSONString());
		} catch (Exception e) {
			LOG.error("Get bin collection dates by UPRN and service failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Get bin collection dates by UPRN and service execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@GET
	@Path("/bin-collection-by-uprn-and-name/{uprn}/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBinCollectionByUPRNAndName(@PathParam("uprn") String uprn, @PathParam("name") String name, @Context HttpServletRequest request) {

		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		long companyId = getCompanyId(request);
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		Locale locale = LocaleUtil.getDefault();

		if (themeDisplay != null) {
			locale = themeDisplay.getLocale();
		}

		try {
			Optional<BinCollection> binCollectionOpt = wasteService.getBinCollectionByUPRNAndName(companyId, uprn, name);

			JSONObject jsonCollection = null;

			if (binCollectionOpt.isPresent()) {
				BinCollection binCollection = binCollectionOpt.get();
				jsonCollection = binCollectionToJSONObject(binCollection, locale);
			} else {
				jsonCollection = getMissingBinCollection();
				jsonCollection.put("dayOfWeek", 0);
			}

			LOG.debug("Get bin collection by UPRN and name for UPRN " + uprn + ", name: " + name + " returned: " + jsonCollection.toJSONString());

			responseBuilder.entity(jsonCollection.toJSONString());
		} catch (Exception e) {
			LOG.error("Get bin collection by UPRN and name failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		return responseBuilder.build();
	}

	@GET
	@Path("/waste-bulky-collection-slots-by-uprn/{uprn : [0-9]*}/{serviceId : [a-zA-Z_0-9]*}/{numberOfDatesToReturn : [0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWasteBulkyCollectionSlotsByUPRNAndService(@PathParam("uprn") String uprn, @PathParam("serviceId") String serviceId,
			@PathParam("numberOfDatesToReturn") String numberOfDatesToReturn, @Context HttpServletRequest request) throws WasteRetrievalException {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);

		try {
			Long companyId = getCompanyId(request);
			ThemeDisplay themeDisplay = getThemeDisplay(request);
			Locale locale = themeDisplay != null ? themeDisplay.getLocale() : LocaleUtil.getDefault();
			TimeZone timeZone = themeDisplay != null ? themeDisplay.getTimeZone() : TimeZoneUtil.getDefault();

			JSONArray bulkyCollectionDates = jsonFactory.createJSONArray();
			wasteService.getBulkyCollectionSlotsByUPRNAndService(companyId, uprn, serviceId, GetterUtil.getInteger(numberOfDatesToReturn, WasteConstants.BULKY_DFLT_NUMBER_OF_DATES_TO_RETURN))
					.forEach(date -> bulkyCollectionDates.put(jsonFactory.createJSONObject().put("date", DateUtil.getDate(date, APPOINMENT_DATE_PATTERN, locale, timeZone))));

			LOG.debug("Bulky collection dates:" + bulkyCollectionDates.toJSONString());

			responseBuilder.entity(bulkyCollectionDates.toJSONString());

		} catch (Exception e) {
			LOG.error("getWasteBulkyCollectionSlotsByUPRNAndService failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Get waste bulky collection slots by uprn and service execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@GET
	@Path("/bulky-collection-dates-by-uprn/{uprn : [0-9]*}/{serviceId : [a-zA-Z_0-9]*}/{numberOfDatesToReturn : [0-9]*}/{onlyDatesWithLessThanXappoinments : [0-9]*}{p:/?}{dayOfWeek : [0-9]?}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBulkyCollectionDatesByUPRN(@PathParam("uprn") String uprn, @PathParam("serviceId") String serviceId, @PathParam("numberOfDatesToReturn") String numberOfDatesToReturn,
			@PathParam("onlyDatesWithLessThanXappoinments") String onlyDatesWithLessThanXappoinments, @PathParam("dayOfWeek") String dayOfWeekParam, @Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		int dayOfWeek = GetterUtil.getInteger(dayOfWeekParam, -1);

		LOG.debug("getBulkyCollectionDatesByUPRN called for serviceId: " + serviceId + ", uprn: " + uprn + ", dayOfWeek: " + dayOfWeek + ", numberOfDatesToReturn: " + numberOfDatesToReturn
				+ ", onlyDatesWithLessThanXappoinments: " + onlyDatesWithLessThanXappoinments);
		try {
			if (dayOfWeek <= 0) {
				Optional<BulkyCollectionDate> bulkyCollectionDateOpt = wasteService.getBulkyCollectionDateByUPRN(companyId, uprn);
				if (bulkyCollectionDateOpt.isPresent()) {
					dayOfWeek = bulkyCollectionDateOpt.get().getDayOfWeek();
				}
			}

			JSONArray bulkyCollectionDates = jsonFactory.createJSONArray();

			if (dayOfWeek > 0) {
				ThemeDisplay themeDisplay = getThemeDisplay(request);
				Locale locale = themeDisplay != null ? themeDisplay.getLocale() : LocaleUtil.getDefault();
				TimeZone timeZone = themeDisplay != null ? themeDisplay.getTimeZone() : TimeZoneUtil.getDefault();

				LOG.debug("Retrieving collection date for serviceId: " + serviceId + " for dayOfWeek: " + dayOfWeek + ", with numberOfDatesToReturn: " + numberOfDatesToReturn
						+ ", onlyDatesWithLessThanXappoinments: " + onlyDatesWithLessThanXappoinments);

				appointmentBookingService
						.getDatesWithAvailableAppoinmentsForDayOfWeek(companyId, serviceId, GetterUtil.getInteger(numberOfDatesToReturn, WasteConstants.BULKY_DFLT_NUMBER_OF_DATES_TO_RETURN),
								dayOfWeek, timeZone, GetterUtil.getInteger(onlyDatesWithLessThanXappoinments, DFLT_ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS))
						.forEach(date -> bulkyCollectionDates.put(jsonFactory.createJSONObject().put("date", DateUtil.getDate(date, APPOINMENT_DATE_PATTERN, locale, timeZone))));
			}

			LOG.debug("Bulky collection dates:" + bulkyCollectionDates.toJSONString());

			responseBuilder.entity(bulkyCollectionDates.toJSONString());
		} catch (Exception e) {
			LOG.error("Get bulky collection dates by UPRN failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/get-missed-bin-collection-response/{uprn}/{dateReq}")
	public Response getMissedBinCollectionResponse(@PathParam("uprn") String uprn, @PathParam("dateReq") String date, @Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			String response = wasteService.getMissedBinCollectionResponse(companyId, uprn, date);
			responseBuilder.entity(response);
		} catch (Exception e) {
			LOG.error("Get missed bin collection response failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

	@GET
	@Path("/how-many-bins-subscribed/{uprn}/{serviceType}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response howManyBinsSubscribed(@PathParam("uprn") String uprn, @PathParam("serviceType") String serviceType, @Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			int howManyBins = wasteService.howManyBinsSubscribed(companyId, uprn, serviceType);

			JSONObject jsonObject = jsonFactory.createJSONObject();
			jsonObject.put("uprn", uprn);
			jsonObject.put("serviceType", serviceType);
			jsonObject.put("numberOfBins", howManyBins);

			LOG.debug("How many bins subscribed: " + jsonObject.toJSONString());

			responseBuilder.entity(jsonObject.toJSONString());
		} catch (Exception e) {
			LOG.error("How many bins subscribed failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("How many bins subscribed execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@GET
	@Path("/how-many-bins-subscribed-for-period/{uprn}/{serviceType}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response howManyBinsSubscribedForPeriod(@PathParam("uprn") String uprn, @PathParam("serviceType") String serviceType, @QueryParam("startDate") String startDate,
			@QueryParam("endDate") String endDate, @Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);

		try {
			int howManyBins = wasteService.howManyBinsSubscribedForPeriod(companyId, uprn, serviceType, startDate, endDate);

			JSONObject jsonObject = jsonFactory.createJSONObject();
			jsonObject.put("uprn", uprn);
			jsonObject.put("serviceType", serviceType);
			jsonObject.put("numberOfBins", howManyBins);

			LOG.debug("How many bins subscribed: " + jsonObject.toJSONString());

			responseBuilder.entity(jsonObject.toJSONString());
		} catch (Exception e) {
			LOG.error("How many bins subscribed for period failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	@GET
	@Path("/is-property-eligible-for-bulky-waste-collection/{uprn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isPropertyEligibleForBulkyWasteCollection(@PathParam("uprn") String uprn, @QueryParam("items") final List<String> items, @QueryParam("formInstanceId") String formInstanceId,
			@Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			boolean canRequestBulkyWasteCollection = wasteService.isPropertyEligibleForBulkyWasteCollection(companyId, uprn, items, formInstanceId);

			JSONObject jsonObject = jsonFactory.createJSONObject();
			jsonObject.put("canRequestBulkyWasteCollection", canRequestBulkyWasteCollection);

			LOG.debug("Is property eligible for bulky waste collection: " + jsonObject.toJSONString());

			responseBuilder.entity(jsonObject.toJSONString());
		} catch (Exception e) {
			LOG.error("Is property eligible for bulky waste collection: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Is property eligible for bulky waste collection execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@GET
	@Path("/is-property-eligible-for-waste-container-request/{uprn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isPropertyEligibleForWasteContainerRequest(@PathParam("uprn") String uprn, @QueryParam("binType") String binType, @QueryParam("binSize") String binSize,
			@QueryParam("formInstanceId") String formInstanceId, @Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			boolean canRequestWasteContainer = wasteService.isPropertyEligibleForWasteContainerRequest(companyId, uprn, binType, binSize, formInstanceId);

			JSONObject jsonObject = jsonFactory.createJSONObject();
			jsonObject.put("canRequestWasteContainer", canRequestWasteContainer);

			LOG.debug("Can request waste container: " + jsonObject.toJSONString());

			responseBuilder.entity(jsonObject.toJSONString());
		} catch (Exception e) {
			LOG.error("Is property eligible for waste container request: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}

		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Is property eligible for waste container request execution time: " + timeElapsed);

		return responseBuilder.build();
	}

	@GET
	@Path("/is-property-eligible-for-garden-waste-subscription/{uprn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isPropertyEligibleForGardenWasteSubscription(@PathParam("uprn") String uprn, @QueryParam("startDateDDsMMsYYYY") String startDateDDsMMsYYYY,
			@QueryParam("endDateDDsMMsYYYY") String endDateDDsMMsYYYY, @Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			boolean isPropertyEligible = wasteService.isPropertyEligibleForGardenWasteSubscription(companyId, uprn, startDateDDsMMsYYYY, endDateDDsMMsYYYY);

			JSONObject jsonObject = jsonFactory.createJSONObject();
			jsonObject.put("uprn", uprn);
			jsonObject.put("isPropertyEligible", isPropertyEligible);

			LOG.debug("Is property eligible: " + jsonObject.toJSONString());

			responseBuilder.entity(jsonObject.toJSONString());
		} catch (Exception e) {
			LOG.error("Is property eligible for garden waste subscription failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Is property eligible for garden waste subscription execution time: " + timeElapsed);
		return responseBuilder.build();
	}

	@GET
	@Path("/is-property-eligible-for-garden-waste-subscription-by-bin/{uprn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isPropertyEligibleForGardenWasteSubscriptionByBin(@PathParam("uprn") String uprn, @QueryParam("startDateDDsMMsYYYY") String startDateDDsMMsYYYY,
			@QueryParam("endDateDDsMMsYYYY") String endDateDDsMMsYYYY, @QueryParam("formInstanceId") String formInstanceId, @QueryParam("binType") String binType,
			@Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			boolean isPropertyEligible = wasteService.isPropertyEligibleForGardenWasteSubscription(companyId, uprn, startDateDDsMMsYYYY, endDateDDsMMsYYYY, binType, formInstanceId);

			JSONObject jsonObject = jsonFactory.createJSONObject();
			jsonObject.put("isPropertyEligible", isPropertyEligible);

			LOG.debug("Is property eligible: " + jsonObject.toJSONString());

			responseBuilder.entity(jsonObject.toJSONString());
		} catch (Exception e) {
			LOG.error("Is property eligible for garden waste subscription by bin failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Is property eligible for garden waste subscription  by bin execution time: " + timeElapsed);
		return responseBuilder.build();
	}

	@GET
	@Path("/is-property-eligible-for-missed-bin-collection-report/{uprn}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isPropertyEligibleForMissedBinCollectionReport(@PathParam("uprn") String uprn, @QueryParam("formInstanceId") String formInstanceId, @QueryParam("binType") String binType,
			@Context HttpServletRequest request) {
		long start = System.currentTimeMillis();
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			boolean isPropertyEligible = wasteService.isPropertyEligibleForMissedBinCollectionReport(companyId, uprn, binType, formInstanceId);

			JSONObject jsonObject = jsonFactory.createJSONObject();
			jsonObject.put("uprn", uprn);
			jsonObject.put("isPropertyEligible", isPropertyEligible);

			LOG.debug("Is property eligible: " + jsonObject.toJSONString());

			responseBuilder.entity(jsonObject.toJSONString());
		} catch (Exception e) {
			LOG.error("Is property eligible for missed bin collection failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		LOG.debug("Is property eligible for missed bin collection execution time: " + timeElapsed);
		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/book-collection-date/{serviceId}")
	public Response postBookBulkyCollectionDate(String bodyRequest, @PathParam("serviceId") String serviceId, @Context HttpServletRequest request) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
		Long companyId = getCompanyId(request);
		try {
			JSONObject jsonObject = jsonFactory.createJSONObject(bodyRequest);

			LOG.debug("Booking collection date for serviceId: " + serviceId + " with details: " + jsonObject.toJSONString());

			long classPK = appointmentBookingEntryHelper.getClassPK(jsonObject);
			long classNameId = appointmentBookingEntryHelper.getClassNameId(jsonObject);
			String title = appointmentBookingEntryHelper.getTitle(jsonObject);
			String description = appointmentBookingEntryHelper.buildDescription(jsonObject, classPK);

			ThemeDisplay themeDisplay = getThemeDisplay(request);
			Locale locale = themeDisplay != null ? themeDisplay.getLocale() : LocaleUtil.getDefault();
			TimeZone timeZone = themeDisplay != null ? themeDisplay.getTimeZone() : TimeZoneUtil.getDefault();

			Date date = DateUtil.parseDate(APPOINMENT_DATE_PATTERN, appointmentBookingEntryHelper.getDate(jsonObject), locale);

			AppointmentBookingEntry appointmentBookingEntry = appointmentBookingEntryHelper.create(classPK, classNameId, title, description, date, timeZone);

			LOG.debug("appointmentBookingEntry: " + JSONFactoryUtil.looseSerialize(appointmentBookingEntry));

			appointmentBookingService.bookAppointment(companyId, serviceId, appointmentBookingEntry);

			LOG.info("Appointment created - appointmentBookingEntry: " + JSONFactoryUtil.looseSerialize(appointmentBookingEntry));

		} catch (Exception e) {
			LOG.error("Appointment booking failed: " + e);
			responseBuilder = getErrorResponseBuilder(e);
		}
		return responseBuilder.build();
	}

	private JSONObject binCollectionToJSONObject(BinCollection binCollection, Locale locale) {
		JSONObject jsonObject = jsonFactory.createJSONObject();
		jsonObject.put("name", binCollection.getName());
		jsonObject.put("label", binCollection.getLabel(locale));
		jsonObject.put("collectionDate", formatDateForOutput(binCollection.getCollectionDate()));
		jsonObject.put("followingCollectionDate", formatDateForOutput(binCollection.getFollowingCollectionDate()));
		jsonObject.put("formattedCollectionDate", binCollection.getFormattedCollectionDate());
		jsonObject.put("frequency", binCollection.getFrequency());
		jsonObject.put("dayOfWeek", binCollection.getDayOfWeek().getValue());
		return jsonObject;
	}

	private void addMissingBinCollections(JSONArray array, int requiredBinCollections) {
		for (int i = 0; i < requiredBinCollections - array.length(); i++) {
			array.put(getMissingBinCollection());
		}
	}

	private JSONObject getMissingBinCollection() {
		JSONObject jsonBinCollection = jsonFactory.createJSONObject();
		jsonBinCollection.put("name", StringPool.BLANK);
		jsonBinCollection.put("label", StringPool.BLANK);
		jsonBinCollection.put("collectionDate", StringPool.BLANK);
		jsonBinCollection.put("followingCollectionDate", StringPool.BLANK);
		jsonBinCollection.put("frequency", StringPool.BLANK);
		return jsonBinCollection;
	}

	private String formatDateForOutput(LocalDate date) {
		if (Validator.isNotNull(date)) {
			return date.format(DateTimeFormatter.ofPattern("EEE dd MMM yyyy"));
		} else {
			return StringPool.BLANK;
		}
	}

	private long getCompanyId(HttpServletRequest request) {
		return (Long) request.getAttribute("COMPANY_ID");
	}

	private ResponseBuilder getErrorResponseBuilder(Exception exception) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.INTERNAL_SERVER_ERROR);
		return responseBuilder.entity(exception.getMessage());
	}

	private ThemeDisplay getThemeDisplay(HttpServletRequest request) {
		return (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

}