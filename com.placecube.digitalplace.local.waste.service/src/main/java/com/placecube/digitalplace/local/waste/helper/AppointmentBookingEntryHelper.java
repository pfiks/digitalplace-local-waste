package com.placecube.digitalplace.local.waste.helper;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;

@Component(immediate = true, service = AppointmentBookingEntryHelper.class)
public class AppointmentBookingEntryHelper {

	protected static final String APP_DESCRIPTION = "Ref: [$FORM_REFERENCE$]".concat(StringPool.COMMA + StringPool.SPACE).concat("Address: [$ADDRESS$]").concat(StringPool.COMMA + StringPool.SPACE)
			.concat("Items: [$ITEMS$]");

	@Reference
	private JSONFactory jsonFactory;

	public String buildDescription(JSONObject jsonObject, long classPK) {

		String address = getAddress(jsonObject.getString("address", jsonObject.getString("fullAddress", StringPool.BLANK)));

		List<String> values = new LinkedList<>();

		jsonObject.keys().forEachRemaining(key -> {
			if (isValidForDescription(key)) {
				String value = jsonObject.getString(key, StringPool.BLANK);
				if (Validator.isNotNull(value) && !(value.startsWith("$") && value.endsWith("$"))) {
					values.add(replaceBrackets(value));
				}
			}
		});

		return StringUtil.replace(APP_DESCRIPTION, new String[] { "[$FORM_REFERENCE$]", "[$ADDRESS$]", "[$ITEMS$]" },
				new String[] { String.valueOf(classPK), address, values.stream().filter(Validator::isNotNull).collect(Collectors.joining(StringPool.COMMA_AND_SPACE)) });

	}

	public AppointmentBookingEntry create(long classPK, long classNameId, String title, String description, Date date, TimeZone timeZone) {

		AppointmentBookingEntry appointmentBookingEntry = new AppointmentBookingEntry(classPK, classNameId);
		appointmentBookingEntry.setStartDate(date);
		appointmentBookingEntry.setEndDate(date);
		appointmentBookingEntry.setTimeZone(timeZone);
		appointmentBookingEntry.setTitle(title);
		appointmentBookingEntry.setDescription(description);

		return appointmentBookingEntry;
	}

	public long getClassNameId(JSONObject jsonObject) {
		return jsonObject.getLong("classNameId");
	}

	public long getClassPK(JSONObject jsonObject) {
		return jsonObject.getLong("classPK");
	}

	public String getDate(JSONObject jsonObject) {
		return replaceBrackets(jsonObject.getString("date", StringPool.BLANK));
	}

	public String getTitle(JSONObject jsonObject) {
		return jsonObject.getString("title", "Bulky Waste Collection");
	}

	private String getAddress(final String addressObject) {
		try {
			return jsonFactory.createJSONObject(addressObject).getString("fullAddress");
		} catch (JSONException e) {
			return addressObject;
		}
	}

	private boolean isValidForDescription(String key) {
		return key.toLowerCase().contains("item");
	}

	private String replaceBrackets(String value) {
		return StringUtil.replace(value, new String[] { "[\"", "\"]", "[", "]" }, new String[] { StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK });
	}
}
