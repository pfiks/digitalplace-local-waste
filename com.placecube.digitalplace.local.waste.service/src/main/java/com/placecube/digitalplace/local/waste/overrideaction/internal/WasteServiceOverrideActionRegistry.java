package com.placecube.digitalplace.local.waste.overrideaction.internal;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.placecube.digitalplace.local.waste.overrideaction.WasteServiceOverrideAction;

@Component(immediate = true, service = WasteServiceOverrideActionRegistry.class)
public class WasteServiceOverrideActionRegistry {

	private static final class ExecutionOrderComparator implements Comparator<ServiceWrapper<WasteServiceOverrideAction>> {

		@Override
		public int compare(ServiceWrapper<WasteServiceOverrideAction> serviceWrapper1, ServiceWrapper<WasteServiceOverrideAction> serviceWrapper2) {
			Long executionOrder1 = MapUtil.getLong(serviceWrapper1.getProperties(), WasteServiceOverrideAction.SERVICE_EXECUTION_PRIORITY);
			Long executionOrder2 = MapUtil.getLong(serviceWrapper2.getProperties(), WasteServiceOverrideAction.SERVICE_EXECUTION_PRIORITY);

			return executionOrder1.compareTo(executionOrder2);
		}
	}

	@SuppressWarnings("unused")
	private BundleContext bundleContext;

	private ServiceTrackerMap<String, ServiceWrapper<WasteServiceOverrideAction>> wasteListenerActionMap;

	public List<WasteServiceOverrideAction> getWasteServiceOverrideActions() {
		List<ServiceWrapper<WasteServiceOverrideAction>> allServices = ListUtil.fromCollection(wasteListenerActionMap.values());

		allServices.sort(new ExecutionOrderComparator());

		return allServices.stream().map(ServiceWrapper::getService).collect(Collectors.toUnmodifiableList());
	}

	@Activate
	protected void activate(BundleContext bundleContext) {
		this.bundleContext = bundleContext;

		wasteListenerActionMap = ServiceTrackerMapFactory.openSingleValueMap(bundleContext, WasteServiceOverrideAction.class, WasteServiceOverrideAction.SERVICE_ID,
				ServiceTrackerCustomizerFactory.serviceWrapper(bundleContext));
	}

	@Deactivate
	protected void deactivate() {
		wasteListenerActionMap.close();
	}

}
