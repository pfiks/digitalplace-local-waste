package com.placecube.digitalplace.local.waste.overrideaction.internal;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.reflect.internal.WhiteboxImpl;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.placecube.digitalplace.local.waste.overrideaction.WasteServiceOverrideAction;

public class WasteServiceOverrideActionRegistryTest extends PowerMockito {

	@Mock
	private ServiceWrapper<WasteServiceOverrideAction> mockServiceWrapper1;

	@Mock
	private ServiceWrapper<WasteServiceOverrideAction> mockServiceWrapper2;

	@Mock
	private ServiceWrapper<WasteServiceOverrideAction> mockServiceWrapper3;

	@Mock
	private WasteServiceOverrideAction mockWasteServiceOverrideAction1;

	@Mock
	private WasteServiceOverrideAction mockWasteServiceOverrideAction2;

	@Mock
	private WasteServiceOverrideAction mockWasteServiceOverrideAction3;

	@InjectMocks
	private WasteServiceOverrideActionRegistry wasteServiceOverrideActionRegistry;

	@Before
	public void activeSetup() {
		initMocks(this);
		wasteServiceOverrideActionRegistry = spy(new WasteServiceOverrideActionRegistry());
	}

	@Test
	public void executeActionsAfterWasteSubscriptionResponseCreation_WhenNoError_ThenExecutesAllOverrideActionsFromTheRegistry() {
		configureInternalMap();

		when(mockServiceWrapper1.getService()).thenReturn(mockWasteServiceOverrideAction1);
		when(mockServiceWrapper2.getService()).thenReturn(mockWasteServiceOverrideAction2);
		when(mockServiceWrapper3.getService()).thenReturn(mockWasteServiceOverrideAction3);

		when(mockServiceWrapper1.getProperties()).thenReturn(Collections.singletonMap(WasteServiceOverrideAction.SERVICE_EXECUTION_PRIORITY, 2));
		when(mockServiceWrapper2.getProperties()).thenReturn(Collections.singletonMap(WasteServiceOverrideAction.SERVICE_EXECUTION_PRIORITY, 1));
		when(mockServiceWrapper3.getProperties()).thenReturn(Collections.singletonMap(WasteServiceOverrideAction.SERVICE_EXECUTION_PRIORITY, 3));

		List<WasteServiceOverrideAction> results = wasteServiceOverrideActionRegistry.getWasteServiceOverrideActions();

		assertThat(results, contains(mockWasteServiceOverrideAction2, mockWasteServiceOverrideAction1, mockWasteServiceOverrideAction3));
	}

	private void configureInternalMap() {
		ServiceTrackerMap<String, ServiceWrapper<WasteServiceOverrideAction>> wasteListenerActionMap = new ServiceTrackerMap<String, ServiceTrackerCustomizerFactory.ServiceWrapper<WasteServiceOverrideAction>>() {

			@Override
			public void close() {
			}

			@Override
			public boolean containsKey(String key) {
				return false;
			}

			@Override
			public ServiceWrapper<WasteServiceOverrideAction> getService(String key) {
				return null;
			}

			@Override
			public Set<String> keySet() {
				return null;
			}

			@Override
			public Collection<ServiceWrapper<WasteServiceOverrideAction>> values() {
				Collection<ServiceWrapper<WasteServiceOverrideAction>> actions = new ArrayList<>();
				actions.add(mockServiceWrapper1);
				actions.add(mockServiceWrapper2);
				actions.add(mockServiceWrapper3);
				return actions;
			}
		};
		WhiteboxImpl.setInternalState(wasteServiceOverrideActionRegistry, "wasteListenerActionMap", wasteListenerActionMap);
	}

}
