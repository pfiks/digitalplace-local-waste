package com.placecube.digitalplace.local.waste.configuration;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.configuration.admin.definition.ConfigurationFieldOptionsProvider.Option;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.placecube.digitalplace.local.waste.service.impl.WasteServiceImpl;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ CompanyThreadLocal.class })
public class WasteConfigurationFieldIOptionProviderTest extends PowerMockito {

	private static final Long COMPANY_ID = 11l;

	@Mock
	private WasteServiceImpl mockWasteServiceImpl;

	@Mock
	private List<Option> optionList;

	@InjectMocks
	private WasteConfigurationFieldIOptionProvider wasteConfigurationFieldOptionProvider;

	@Before
	public void setUp() {
		mockStatic(CompanyThreadLocal.class);
	}

	@Test(expected = Exception.class)
	public void getWasteConnectorList_WhenNotGettingList_ThenThrowWasteRetrievalException() throws Exception {
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWasteServiceImpl.getWasteConnectorList(COMPANY_ID)).thenReturn(null);
		wasteConfigurationFieldOptionProvider.getOptions();

	}

	@Test
	public void getWasteConnectorList_WhenGettingList_ThenReturnListOfOption() throws Exception {
		String name1 = "waste-mock";
		List<String> mockWasteConnectors = new ArrayList<String>();
		mockWasteConnectors.add(name1);
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWasteServiceImpl.getWasteConnectorList(COMPANY_ID)).thenReturn(mockWasteConnectors);
		List<Option> wasteConnectorList = wasteConfigurationFieldOptionProvider.getOptions();

		assertEquals(mockWasteConnectors.get(0), wasteConnectorList.get(0).getValue());
	}

}
