package com.placecube.digitalplace.local.waste.helper;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.TimeZoneUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;

public class AppointmentBookingEntryHelperTest extends PowerMockito {

	@InjectMocks
	private AppointmentBookingEntryHelper appointmentBookingEntryHelper;

	@Mock
	private JSONObject mockAddressJsonObject;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void buildDescription_WhenAddressIsJson_ThenReturnDescriptionWithAddressAddedFromJSON() throws JSONException {

		long classPK = 1;
		String fullAddress = "Rugby Borough Council, Town Hall, Evreux Way, Rugby, CV21 2RR";
		String addressJson = "{\"fullAddress\" : \"" + fullAddress + "\"}";
		StringBundler items = new StringBundler();
		when(mockJsonObject.getString("address", null)).thenReturn(addressJson);
		when(mockJsonObject.getString("fullAddress", StringPool.BLANK)).thenReturn(null);
		when(mockJsonFactory.createJSONObject(addressJson)).thenReturn(mockAddressJsonObject);
		when(mockAddressJsonObject.getString("fullAddress")).thenReturn(fullAddress);

		when(mockJsonObject.getLong("classPK")).thenReturn(classPK);
		when(mockJsonObject.keys()).thenReturn(Collections.emptyIterator());

		String expected = StringUtil.replace(AppointmentBookingEntryHelper.APP_DESCRIPTION, new String[] { "[$FORM_REFERENCE$]", "[$ADDRESS$]", "[$ITEMS$]" },
				new String[] { String.valueOf(classPK), fullAddress, items.toString() });

		String result = appointmentBookingEntryHelper.buildDescription(mockJsonObject, classPK);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void buildDescription_WhenAddressIsNotJson_ThenReturnDescriptionWithAddressAdded() throws JSONException {

		long classPK = 1;
		String address = "Rugby Borough Council, Town Hall, Evreux Way, Rugby, CV21 2RR";
		StringBundler items = new StringBundler();
		when(mockJsonObject.getString("fullAddress", StringPool.BLANK)).thenReturn(address);
		when(mockJsonObject.getString("address", address)).thenReturn(address);
		when(mockJsonFactory.createJSONObject(address)).thenThrow(new JSONException());
		when(mockJsonObject.getLong("classPK")).thenReturn(classPK);
		when(mockJsonObject.keys()).thenReturn(Collections.emptyIterator());

		String expected = StringUtil.replace(AppointmentBookingEntryHelper.APP_DESCRIPTION, new String[] { "[$FORM_REFERENCE$]", "[$ADDRESS$]", "[$ITEMS$]" },
				new String[] { String.valueOf(classPK), address, items.toString() });

		String result = appointmentBookingEntryHelper.buildDescription(mockJsonObject, classPK);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void buildDescription_WhenFullAddressIsJson_ThenReturnDescriptionWithFullAddressAddedFromJSON() throws JSONException {

		long classPK = 1;
		String fullAddress = "Rugby Borough Council, Town Hall, Evreux Way, Rugby, CV21 2RR";
		String addressJson = "{\"fullAddress\" : \"" + fullAddress + "\"}";
		StringBundler items = new StringBundler();
		when(mockJsonObject.getString("fullAddress", StringPool.BLANK)).thenReturn(addressJson);
		when(mockJsonObject.getString("address", addressJson)).thenReturn(addressJson);
		when(mockJsonFactory.createJSONObject(addressJson)).thenReturn(mockAddressJsonObject);
		when(mockAddressJsonObject.getString("fullAddress")).thenReturn(fullAddress);

		when(mockJsonObject.getLong("classPK")).thenReturn(classPK);
		when(mockJsonObject.keys()).thenReturn(Collections.emptyIterator());

		String expected = StringUtil.replace(AppointmentBookingEntryHelper.APP_DESCRIPTION, new String[] { "[$FORM_REFERENCE$]", "[$ADDRESS$]", "[$ITEMS$]" },
				new String[] { String.valueOf(classPK), fullAddress, items.toString() });

		String result = appointmentBookingEntryHelper.buildDescription(mockJsonObject, classPK);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void buildDescription_WhenFullAddressIsNotJson_ThenReturnDescriptionWithFullAddressAddressAdded() throws JSONException {

		long classPK = 1;
		String address = "Rugby Borough Council, Town Hall, Evreux Way, Rugby, CV21 2RR";
		StringBundler items = new StringBundler();
		when(mockJsonObject.getString("address", null)).thenReturn(address);
		when(mockJsonObject.getString("fullAddress", StringPool.BLANK)).thenReturn(null);
		when(mockJsonFactory.createJSONObject(address)).thenThrow(new JSONException());
		when(mockJsonObject.getLong("classPK")).thenReturn(classPK);
		when(mockJsonObject.keys()).thenReturn(Collections.emptyIterator());

		String expected = StringUtil.replace(AppointmentBookingEntryHelper.APP_DESCRIPTION, new String[] { "[$FORM_REFERENCE$]", "[$ADDRESS$]", "[$ITEMS$]" },
				new String[] { String.valueOf(classPK), address, items.toString() });

		String result = appointmentBookingEntryHelper.buildDescription(mockJsonObject, classPK);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void buildDescription_WhenThereIsItemForDescription_ThenReturnDescriptionWithItemAdded() throws JSONException {

		long classPK = 1;
		String address = StringPool.BLANK;
		when(mockJsonObject.getString("fullAddress", StringPool.BLANK)).thenReturn(StringPool.BLANK);
		when(mockJsonObject.getString("address", StringPool.BLANK)).thenReturn(address);
		when(mockJsonFactory.createJSONObject(address)).thenThrow(new JSONException());
		when(mockJsonObject.getLong("classPK")).thenReturn(classPK);
		List<String> keys = Arrays.asList("classPK", "item1", "item2", "item3", "item4", "item5");
		when(mockJsonObject.keys()).thenReturn(keys.iterator());
		when(mockJsonObject.getString("item1", StringPool.BLANK)).thenReturn("[\"fridge\"]");
		when(mockJsonObject.getString("item2", StringPool.BLANK)).thenReturn("");
		when(mockJsonObject.getString("item4", StringPool.BLANK)).thenReturn("oven");
		when(mockJsonObject.getString("item5", StringPool.BLANK)).thenReturn("[]");

		String expected = StringUtil.replace(AppointmentBookingEntryHelper.APP_DESCRIPTION, new String[] { "[$FORM_REFERENCE$]", "[$ADDRESS$]", "[$ITEMS$]" },
				new String[] { String.valueOf(classPK), StringPool.BLANK, "fridge, oven" });

		String result = appointmentBookingEntryHelper.buildDescription(mockJsonObject, classPK);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void buildDescription_WhenThereIsNoAddress_ThenReturnDescriptionWithEmptyAddressAdded() throws JSONException {

		long classPK = 1;
		String address = StringPool.BLANK;
		StringBundler items = new StringBundler();
		when(mockJsonObject.getString("fullAddress", StringPool.BLANK)).thenReturn(null);
		when(mockJsonObject.getString("address", null)).thenReturn(address);
		when(mockJsonFactory.createJSONObject(address)).thenThrow(new JSONException());
		when(mockJsonObject.getLong("classPK")).thenReturn(classPK);
		when(mockJsonObject.keys()).thenReturn(Collections.emptyIterator());

		String expected = StringUtil.replace(AppointmentBookingEntryHelper.APP_DESCRIPTION, new String[] { "[$FORM_REFERENCE$]", "[$ADDRESS$]", "[$ITEMS$]" },
				new String[] { String.valueOf(classPK), StringPool.BLANK, items.toString() });

		String result = appointmentBookingEntryHelper.buildDescription(mockJsonObject, classPK);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void buildDescription_WhenThereIsNotItemForDescription_ThenReturnDescriptionWithoutItemAdded() throws JSONException {

		long classPK = 1;
		String address = StringPool.BLANK;
		StringBundler items = new StringBundler();
		when(mockJsonObject.getString("fullAddress", StringPool.BLANK)).thenReturn(null);
		when(mockJsonObject.getString("address", null)).thenReturn(address);
		when(mockJsonFactory.createJSONObject(address)).thenThrow(new JSONException());
		when(mockJsonObject.getLong("classPK")).thenReturn(classPK);
		List<String> keys = Arrays.asList("classPK", "other");
		when(mockJsonObject.keys()).thenReturn(keys.iterator());

		String expected = StringUtil.replace(AppointmentBookingEntryHelper.APP_DESCRIPTION, new String[] { "[$FORM_REFERENCE$]", "[$ADDRESS$]", "[$ITEMS$]" },
				new String[] { String.valueOf(classPK), StringPool.BLANK, items.toString() });

		String result = appointmentBookingEntryHelper.buildDescription(mockJsonObject, classPK);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void create_WhenNoError_ThenReturnAppoinmentCreated() {

		long classPK = 1;
		long classNameId = 2;
		String title = "title";
		String description = "description";
		Date now = new Date();

		AppointmentBookingEntry result = appointmentBookingEntryHelper.create(classPK, classNameId, title, description, now, TimeZoneUtil.GMT);

		assertThat(result.getClassNameId(), equalTo(classNameId));
		assertThat(result.getClassPK(), equalTo(classPK));
		assertThat(result.getDescription(), equalTo(description));
		assertThat(result.getStartDate(), equalTo(now));
		assertThat(result.getEndDate(), equalTo(now));
		assertThat(result.getTimeZone(), equalTo(TimeZoneUtil.GMT));
		assertThat(result.getTitle(), equalTo(title));
	}

	@Test
	public void getClassNameId_WhenNoError_ThenReturnClassNameId() {

		long classNameId = 2;
		when(mockJsonObject.getLong("classNameId")).thenReturn(classNameId);

		assertThat(appointmentBookingEntryHelper.getClassNameId(mockJsonObject), equalTo(classNameId));

	}

	@Test
	public void getClassPK_WhenNoError_ThenReturnClassPK() {

		long classPK = 2;
		when(mockJsonObject.getLong("classPK")).thenReturn(classPK);

		assertThat(appointmentBookingEntryHelper.getClassPK(mockJsonObject), equalTo(classPK));

	}

	@Test
	public void getDate_WhenNoError_ThenReturnDate() {
		String date = "[\"Mon 4 May\"]";
		String expected = "Mon 4 May";

		when(mockJsonObject.getString("date", StringPool.BLANK)).thenReturn(date);

		assertThat(appointmentBookingEntryHelper.getDate(mockJsonObject), equalTo(expected));

	}

	@Test
	public void getTitle_WhenNoError_ThenReturnTitle() {

		String title = "title";
		String defaultTitle = "Bulky Waste Collection";

		when(mockJsonObject.getString(title, defaultTitle)).thenReturn(title);

		assertThat(appointmentBookingEntryHelper.getTitle(mockJsonObject), equalTo(title));

	}
}
