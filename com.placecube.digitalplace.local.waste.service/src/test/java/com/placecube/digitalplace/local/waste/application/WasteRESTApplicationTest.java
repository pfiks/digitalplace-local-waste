package com.placecube.digitalplace.local.waste.application;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.service.component.annotations.Reference;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.TimeZoneUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.local.waste.constants.WasteConstants;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.helper.AppointmentBookingEntryHelper;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.service.WasteService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class, LocaleUtil.class, TimeZoneUtil.class, JSONFactoryUtil.class })
public class WasteRESTApplicationTest extends PowerMockito {

	private static final String BIN_FREQUENCY = "Collection is weekly on a Wednesday";

	private static final String BIN_LABEL = "Black bin collection";

	private static final String BIN_NAME = "refuse";

	private static final String BIN_SIZE = "size";

	private static final Date COLLECTION_DATE = new Date();

	private static final long COMPANY_ID = 1l;

	private static final String COORDINATES = "11.00";

	private static final String DATE_OF_INCIDENT = "01/01/2022";

	private static final String DETAILS = "details";

	private static final String EMAIL_ADDRESS = "abc@gmail.com";

	private static final String END_DATE = "01/01/2023";

	private static final String FIRST_NAME = "abc";

	private static final String FORM_INSTANCE_ID = "3222";

	private static final String JSON = "{json:test}";

	private static final String LAST_NAME = "xyz";

	private static final String LOCATION_DETAILS = "location";

	private static final String LOCATION_OF_ITEMS = "pqr";

	private static final int NUMBER_OF_BINS = 3;

	private static final int NUMBER_OF_DATES_TO_RETURN = 10;

	private static final int ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS = 8;

	private static final String SERVICE_ID = "2";

	private static final String SERVICE_TYPE = "serviceType";

	private static final String SIZE_OF_RUBBISH = "20";

	private static final String START_DATE = "01/01/2022";

	private static final String TELEPHONE = "989898989";

	private static final String TYPE_OF_RUBBISH = "type";

	private static final String UPRN = "12345678";

	private static final String VALUE = "abcd";

	@Reference
	private JSONFactory jsonFactory;

	@Mock
	private AppointmentEntry mockAppointment;

	@Mock
	private AppointmentBookingEntryHelper mockAppointmentBookingEntryHelper;

	@Mock
	private AppointmentBookingService mockAppointmentBookingService;

	@Mock
	private AppointmentBookingEntry mockAppointmentEntry;

	@Mock
	private JSONArray mockArray;

	@Mock
	private AssistedBinCollectionJob mockAssistedBinCollectionJob;

	@Mock
	private String mockBodyRequest;

	@Mock
	private BinCollection mockBinCollection;

	@Mock
	private BulkyCollectionDate mockBulkyCollectionDate;

	@Mock
	private BulkyWasteCollectionRequest mockBulkyWasteCollectionRequest;

	@Mock
	private Date mockDate;

	@Mock
	private GardenWasteSubscription mockGardenWasteSubscription;

	@Mock
	private List<String> mockItemsToBeCollectedList;

	@Mock
	private JSONObject mockJsonAppointment;

	@Mock
	private JSONObject mockJsonCollection;

	@Mock
	private JSONObject mockJsonObject1;

	@Mock
	private JSONObject mockJsonObject2;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private MissedBinCollectionJob mockMissedBinCollectionJob;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private Date mockStartDate;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private WasteService mockWasteService;

	@Mock
	private WasteSubscriptionResponse mockWasteSubscriptionResponse;

	@InjectMocks
	private WasteRESTApplication wasteRESTApplication;

	@Before
	public void activeSetup() {

		mockStatic(DateUtil.class, LocaleUtil.class, TimeZoneUtil.class, JSONFactoryUtil.class);
	}

	@Test
	public void createAssistedBinCollectionJob_WhenErrorGettingBinCollectionDaysFromWasteAPI_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.createAssistedBinCollectionJob(mockAssistedBinCollectionJob, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void createAssistedBinCollectionJob_WhenNoErrorGettingBinCollectionDaysFromWasteAPI_ThenReturnsBinCollectionDatesJSON() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockWasteService.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob)).thenReturn(VALUE);

		Response result = wasteRESTApplication.createAssistedBinCollectionJob(mockAssistedBinCollectionJob, mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void createBinSubscription_WhenErrorCreatingBinSubscription_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createBinSubscription(COMPANY_ID, UPRN, SERVICE_TYPE, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, NUMBER_OF_BINS)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.createBinSubscription(UPRN, SERVICE_TYPE, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, NUMBER_OF_BINS, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void createBinSubscription_WhenNoError_ThenReturnsValue() throws WasteRetrievalException {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createBinSubscription(COMPANY_ID, UPRN, SERVICE_TYPE, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, NUMBER_OF_BINS)).thenReturn(mockWasteSubscriptionResponse);
		when(mockWasteSubscriptionResponse.getSubscriptionRef()).thenReturn(VALUE);

		Response result = wasteRESTApplication.createBinSubscription(UPRN, SERVICE_TYPE, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, NUMBER_OF_BINS, mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void createBulkyWasteCollectionJob_WhenErrorGettingBulkyWasteCollectionDaysFromWasteAPI_ThenThrowsWasteRetrievalException() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createBulkyWasteCollectionJob(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE))
				.thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.createBulkyWasteCollectionJob(UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE,
				mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void createBulkyWasteCollectionJob_WhenNoErrorGettingBulkyWasteCollectionDaysFromWasteAPI_ThenReturnsBulkyWasteCollectionDatesJSON() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockWasteService.createBulkyWasteCollectionJob(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE))
				.thenReturn(VALUE);

		Response result = wasteRESTApplication.createBulkyWasteCollectionJob(UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE,
				mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void createBulkyWasteCollection_WhenErrorCreatingBulkyWasteCollectionJob_ThenThrowsWasteRetrievalException() throws Exception {
		final String dateString = COLLECTION_DATE.toString();

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockBulkyWasteCollectionRequest.getDate()).thenReturn(dateString);
		when(DateUtil.parseDate(WasteRESTApplication.APPOINMENT_DATE_PATTERN, dateString, Locale.UK)).thenReturn(COLLECTION_DATE);

		when(mockWasteService.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.createBulkyWasteCollection(mockBulkyWasteCollectionRequest, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void createBulkyWasteCollection_WhenNoErrorCreatingBulkyWasteCollectionJob_ThenReturnsBulkyWasteCollectionDatesJSON() throws Exception {
		final String dateString = COLLECTION_DATE.toString();

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockBulkyWasteCollectionRequest.getDate()).thenReturn(dateString);
		when(DateUtil.parseDate(WasteRESTApplication.APPOINMENT_DATE_PATTERN, dateString, Locale.UK)).thenReturn(COLLECTION_DATE);

		when(mockWasteService.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE)).thenReturn(VALUE);

		Response result = wasteRESTApplication.createBulkyWasteCollection(mockBulkyWasteCollectionRequest, mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void createBulkyWasteCollection_WhenThemeDisplayIsNotInRequest_ThenCreatesCollectionDateWithDefaultLocale() throws Exception {
		final String dateString = COLLECTION_DATE.toString();

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);
		when(LocaleUtil.getDefault()).thenReturn(Locale.UK);
		when(mockBulkyWasteCollectionRequest.getDate()).thenReturn(dateString);
		when(DateUtil.parseDate(WasteRESTApplication.APPOINMENT_DATE_PATTERN, dateString, Locale.UK)).thenReturn(COLLECTION_DATE);

		when(mockWasteService.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE)).thenReturn(VALUE);

		Response result = wasteRESTApplication.createBulkyWasteCollection(mockBulkyWasteCollectionRequest, mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void createFlyTippingReport_WhenError_ThenThrowsWasteRetrievalException() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createFlyTippingReport(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, TYPE_OF_RUBBISH, SIZE_OF_RUBBISH, DETAILS, COORDINATES, LOCATION_DETAILS,
				DATE_OF_INCIDENT)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.createFlyTippingReport(UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, TYPE_OF_RUBBISH, SIZE_OF_RUBBISH, DETAILS, COORDINATES, LOCATION_DETAILS,
				DATE_OF_INCIDENT, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void createFlyTippingReport_WhenNoError_ThenReturnsReference() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createFlyTippingReport(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, TYPE_OF_RUBBISH, SIZE_OF_RUBBISH, DETAILS, COORDINATES, LOCATION_DETAILS,
				DATE_OF_INCIDENT)).thenReturn(VALUE);

		Response result = wasteRESTApplication.createFlyTippingReport(UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, TYPE_OF_RUBBISH, SIZE_OF_RUBBISH, DETAILS, COORDINATES, LOCATION_DETAILS,
				DATE_OF_INCIDENT, mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void createGardenWasteSubscription_WhenError_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.createGardenWasteSubscription(mockGardenWasteSubscription, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void createGardenWasteSubscription_WhenNoError_ThenReturnsReference() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription)).thenReturn(mockWasteSubscriptionResponse);
		when(mockWasteSubscriptionResponse.getSubscriptionRef()).thenReturn(VALUE);

		Response result = wasteRESTApplication.createGardenWasteSubscription(mockGardenWasteSubscription, mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void createMissedBinCollectionJob_WhenErrorGettingBinCollectionDaysFromWasteAPI_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.createMissedBinCollectionJob(mockMissedBinCollectionJob, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void createMissedBinCollectionJob_WhenNoErrorGettingBinCollectionDaysFromWasteAPI_ThenReturnsBinCollectionDatesJSON() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockWasteService.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob)).thenReturn(VALUE);

		Response result = wasteRESTApplication.createMissedBinCollectionJob(mockMissedBinCollectionJob, mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBinCollectionByUPRNAndName_WhenErrorGettingBinCollectionFromWasteAPI_ThenThrowsWasteRetrievalException() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.getBinCollectionByUPRNAndName(COMPANY_ID, UPRN, BIN_NAME)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.getBinCollectionByUPRNAndName(UPRN, BIN_NAME, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBinCollectionByUPRNAndName_WhenNoErrorGettingBinCollectionFromWasteAPIAndBinCollectionMatchingNameIsFound_ThenReturnsBinCollectionDatesJSON() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		Map<Locale, String> labelMap = new HashMap<>() {

			{
				put(LocaleUtil.getDefault(), BIN_LABEL);
			}
		};
		BinCollection binCollectionRefuse = new BinCollection.BinCollectionBuilder(BIN_NAME, LocalDate.now(), labelMap).frequency(BIN_FREQUENCY).followingCollectionDate(LocalDate.now().plusWeeks(1))
				.build();

		when(mockWasteService.getBinCollectionByUPRNAndName(COMPANY_ID, UPRN, BIN_NAME)).thenReturn(Optional.of(binCollectionRefuse));

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);
		when(mockJsonCollection.toJSONString()).thenReturn(JSON);

		Response result = wasteRESTApplication.getBinCollectionByUPRNAndName(UPRN, BIN_NAME, mockRequest);

		assertEquals(JSON, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());

		verify(mockJsonCollection, times(1)).put("name", BIN_NAME);
		verify(mockJsonCollection, times(1)).put("label", BIN_LABEL);
		verify(mockJsonCollection, times(1)).put("collectionDate", LocalDate.now().format(DateTimeFormatter.ofPattern("EEE dd MMM yyyy")));
	}

	@Test
	public void getBinCollectionByUPRNAndName_WhenNoErrorGettingBinCollectionFromWasteAPIAndBinCollectionMatchingNameIsNotFound_ThenReturnsBinCollectionDatesJSON() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockWasteService.getBinCollectionByUPRNAndName(COMPANY_ID, UPRN, BIN_NAME)).thenReturn(Optional.empty());
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);
		when(mockJsonCollection.toJSONString()).thenReturn(JSON);

		Response result = wasteRESTApplication.getBinCollectionByUPRNAndName(UPRN, BIN_NAME, mockRequest);

		assertEquals(JSON, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());

		verify(mockJsonCollection, times(1)).put("name", StringPool.BLANK);
		verify(mockJsonCollection, times(1)).put("label", StringPool.BLANK);
		verify(mockJsonCollection, times(1)).put("collectionDate", StringPool.BLANK);
		verify(mockJsonCollection, times(1)).put("followingCollectionDate", StringPool.BLANK);
		verify(mockJsonCollection, times(1)).put("frequency", StringPool.BLANK);
		verify(mockJsonCollection, times(1)).put("dayOfWeek", 0);
	}

	@Test
	public void getBinCollectionDatesByUPRNAndService_WhenErrorGettingBinCollectionDaysFromWasteAPI_ThenThrowsWasteRetrievalException() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.getBinCollectionDatesByUPRNAndService(UPRN, SERVICE_TYPE, FORM_INSTANCE_ID, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBinCollectionDatesByUPRNAndService_WhenNoErrorGettingBinCollectionDaysFromWasteAPI_ThenReturnsBinCollectionDatesJSON() throws Exception {
		LocalDate localDate = LocalDate.now();

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);

		Set<BinCollection> binCollections = new HashSet<>();
		binCollections.add(mockBinCollection);

		when(mockBinCollection.getName()).thenReturn(BIN_NAME);
		when(mockBinCollection.getLabel(Locale.UK)).thenReturn(BIN_LABEL);
		when(mockBinCollection.getCollectionDate()).thenReturn(localDate);
		when(mockBinCollection.getFollowingCollectionDate()).thenReturn(localDate.plusWeeks(1));
		when(mockBinCollection.getFormattedCollectionDate()).thenReturn(localDate.toString());
		when(mockBinCollection.getDayOfWeek()).thenReturn(DayOfWeek.MONDAY);
		when(mockBinCollection.getFrequency()).thenReturn(BIN_FREQUENCY);

		when(mockWasteService.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID)).thenReturn(binCollections);

		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		when(mockArray.toJSONString()).thenReturn(JSON);

		Response result = wasteRESTApplication.getBinCollectionDatesByUPRNAndService(UPRN, SERVICE_TYPE, FORM_INSTANCE_ID, mockRequest);

		assertEquals(JSON, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());

		verify(mockJsonCollection, times(1)).put("name", BIN_NAME);
		verify(mockJsonCollection, times(1)).put("label", BIN_LABEL);
		verify(mockJsonCollection, times(1)).put("collectionDate", localDate.format(DateTimeFormatter.ofPattern("EEE dd MMM yyyy")));
		verify(mockJsonCollection, times(1)).put("followingCollectionDate", localDate.plusWeeks(1).format(DateTimeFormatter.ofPattern("EEE dd MMM yyyy")));
		verify(mockJsonCollection, times(1)).put("formattedCollectionDate", localDate.toString());
		verify(mockJsonCollection, times(1)).put("frequency", BIN_FREQUENCY);
		verify(mockJsonCollection, times(1)).put("dayOfWeek", DayOfWeek.MONDAY.getValue());
	}

	@Test
	public void getBinCollectionDatesByUPRNAndService_WhenThemeDisplayIsNotPresent_ThenReturnsBinCollectionLabelForDefaultLocale() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);
		when(LocaleUtil.getDefault()).thenReturn(Locale.UK);

		Set<BinCollection> binCollections = new HashSet<>();
		binCollections.add(mockBinCollection);

		when(mockBinCollection.getLabel(Locale.UK)).thenReturn(BIN_LABEL);

		when(mockWasteService.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID)).thenReturn(binCollections);

		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		when(mockArray.toJSONString()).thenReturn(JSON);

		wasteRESTApplication.getBinCollectionDatesByUPRNAndService(UPRN, SERVICE_TYPE, FORM_INSTANCE_ID, mockRequest);

		verify(mockJsonCollection, times(1)).put("label", BIN_LABEL);
	}

	@Test
	public void getBinCollectionDatesByUPRNAndService_WhenCollectionDatesAreNull_ThenSetsEmptyDateStringsInJSON() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(LocaleUtil.getDefault()).thenReturn(Locale.UK);

		Set<BinCollection> binCollections = new HashSet<>();
		binCollections.add(mockBinCollection);

		when(mockBinCollection.getLabel(Locale.UK)).thenReturn(BIN_LABEL);

		when(mockWasteService.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID)).thenReturn(binCollections);

		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		when(mockArray.toJSONString()).thenReturn(JSON);

		wasteRESTApplication.getBinCollectionDatesByUPRNAndService(UPRN, SERVICE_TYPE, FORM_INSTANCE_ID, mockRequest);

		verify(mockJsonCollection, times(1)).put("collectionDate", StringPool.BLANK);
		verify(mockJsonCollection, times(1)).put("followingCollectionDate", StringPool.BLANK);

	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenErrorGettingBinCollectionDaysFromWasteAPI_ThenThrowsWasteRetrievalException() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.getBinCollectionDatesByUPRN(UPRN, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenNoErrorGettingBinCollectionDaysFromWasteAPI_ThenReturnsBinCollectionDatesJSON() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		Set<BinCollection> binCollections = new HashSet<>();
		Map<Locale, String> labelMap = new HashMap<>() {

			{
				put(LocaleUtil.getDefault(), BIN_LABEL);
			}
		};
		BinCollection binCollectionRefuse = new BinCollection.BinCollectionBuilder(BIN_NAME, LocalDate.now(), labelMap).frequency(BIN_FREQUENCY).followingCollectionDate(LocalDate.now().plusWeeks(1))
				.formattedCollectionDate("day" + StringPool.SPACE + "schedule")

				.build();
		binCollections.add(binCollectionRefuse);

		when(mockWasteService.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenReturn(binCollections);

		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		when(mockArray.toJSONString()).thenReturn(JSON);

		Response result = wasteRESTApplication.getBinCollectionDatesByUPRN(UPRN, mockRequest);

		assertEquals(JSON, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());

		verify(mockJsonCollection, times(1)).put("name", BIN_NAME);
		verify(mockJsonCollection, times(1)).put("label", BIN_LABEL);
		verify(mockJsonCollection, times(1)).put("collectionDate", LocalDate.now().format(DateTimeFormatter.ofPattern("EEE dd MMM yyyy")));
		verify(mockJsonCollection, times(1)).put("followingCollectionDate", LocalDate.now().plusWeeks(1).format(DateTimeFormatter.ofPattern("EEE dd MMM yyyy")));
		verify(mockJsonCollection, times(1)).put("formattedCollectionDate", "day" + StringPool.SPACE + "schedule");
		verify(mockJsonCollection, times(1)).put("frequency", BIN_FREQUENCY);
		verify(mockJsonCollection, times(1)).put("dayOfWeek", LocalDate.now().getDayOfWeek().getValue());
	}

	@Test
	public void getBulkyCollectionDatesByUPRN_WhenDayOfWeekIsBlank_AndThereIsBulkyCollectionDateForUPRNAndThemeDisplayIsNotNull_ThenReturnJSONArraysWithAppoinments() throws WasteRetrievalException {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenReturn(Optional.of(mockBulkyCollectionDate));
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.CANADA);
		when(mockThemeDisplay.getTimeZone()).thenReturn(TimeZoneUtil.GMT);
		when(mockBulkyCollectionDate.getDayOfWeek()).thenReturn(DayOfWeek.MONDAY.getValue());

		List<Date> dates = new ArrayList<>();
		dates.add(mockDate);

		String formattedStartDate = "Mon 3 May";
		when(DateUtil.getDate(mockDate, WasteRESTApplication.APPOINMENT_DATE_PATTERN, Locale.CANADA, TimeZoneUtil.GMT)).thenReturn(formattedStartDate);

		when(mockAppointmentBookingService.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN, DayOfWeek.MONDAY.getValue(), TimeZoneUtil.GMT,
				ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS)).thenReturn(dates);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonAppointment);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);

		String expectedAppointmentJson = "{\"date\":\"Mon 3 May\"}";
		when(mockArray.toJSONString()).thenReturn("[" + expectedAppointmentJson + "]");

		Response result = wasteRESTApplication.getBulkyCollectionDatesByUPRN(UPRN, SERVICE_ID, String.valueOf(NUMBER_OF_DATES_TO_RETURN), String.valueOf(ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS),
				StringPool.BLANK, mockRequest);

		assertThat(result.getEntity(), equalTo("[" + expectedAppointmentJson + "]"));
		verify(mockJsonAppointment, times(1)).put("date", formattedStartDate);
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBulkyCollectionDatesByUPRN_WhenDayOfWeekIsZero_AndThereIsBulkyCollectionDateForUPRNAndThemeDisplayIsNotNull_ThenReturnJSONArraysWithAppoinments() throws WasteRetrievalException {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenReturn(Optional.of(mockBulkyCollectionDate));
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.CANADA);
		when(mockThemeDisplay.getTimeZone()).thenReturn(TimeZoneUtil.GMT);
		when(mockBulkyCollectionDate.getDayOfWeek()).thenReturn(DayOfWeek.MONDAY.getValue());

		List<Date> dates = new ArrayList<>();
		dates.add(mockDate);

		String formattedStartDate = "Mon 3 May";
		when(DateUtil.getDate(mockDate, WasteRESTApplication.APPOINMENT_DATE_PATTERN, Locale.CANADA, TimeZoneUtil.GMT)).thenReturn(formattedStartDate);

		when(mockAppointmentBookingService.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN, DayOfWeek.MONDAY.getValue(), TimeZoneUtil.GMT,
				ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS)).thenReturn(dates);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonAppointment);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);

		String expectedAppointmentJson = "{\"date\":\"Mon 3 May\"}";
		when(mockArray.toJSONString()).thenReturn("[" + expectedAppointmentJson + "]");

		Response result = wasteRESTApplication.getBulkyCollectionDatesByUPRN(UPRN, SERVICE_ID, String.valueOf(NUMBER_OF_DATES_TO_RETURN), String.valueOf(ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS), "0",
				mockRequest);

		assertThat(result.getEntity(), equalTo("[" + expectedAppointmentJson + "]"));
		verify(mockJsonAppointment, times(1)).put("date", formattedStartDate);
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBulkyCollectionDatesByUPRN_WhenDayOfWeekIsBlank_AndThereIsNotABulkyCollectionDateForUPRNAndThemeDisplayIsNotNull_ThenReturnEmptyJSONArray() throws WasteRetrievalException {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenReturn(Optional.empty());

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonAppointment);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);

		when(mockArray.toJSONString()).thenReturn("[]");

		Response result = wasteRESTApplication.getBulkyCollectionDatesByUPRN(UPRN, SERVICE_ID, String.valueOf(NUMBER_OF_DATES_TO_RETURN), String.valueOf(ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS),
				StringPool.BLANK, mockRequest);

		assertThat(result.getEntity(), equalTo("[]"));
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBulkyCollectionDatesByUPRN_WhenDayOfWeekIsZero_AndThereIsNotABulkyCollectionDateForUPRNAndThemeDisplayIsNotNull_ThenReturnEmptyJSONArray() throws WasteRetrievalException {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenReturn(Optional.empty());

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonAppointment);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);

		when(mockArray.toJSONString()).thenReturn("[]");

		Response result = wasteRESTApplication.getBulkyCollectionDatesByUPRN(UPRN, SERVICE_ID, String.valueOf(NUMBER_OF_DATES_TO_RETURN), String.valueOf(ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS), "0",
				mockRequest);

		assertThat(result.getEntity(), equalTo("[]"));
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBulkyCollectionDatesByUPRN_WhenDayOfWeekIsNotBlank_AndThemeDisplayIsNotNull_ThenReturnJSONArraysWithAppoinments() throws WasteRetrievalException {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.CANADA);
		when(mockThemeDisplay.getTimeZone()).thenReturn(TimeZoneUtil.GMT);

		List<Date> dates = new ArrayList<>();
		dates.add(mockDate);

		String formattedStartDate = "Mon 3 May";
		when(DateUtil.getDate(mockDate, WasteRESTApplication.APPOINMENT_DATE_PATTERN, Locale.CANADA, TimeZoneUtil.GMT)).thenReturn(formattedStartDate);

		when(mockAppointmentBookingService.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN, DayOfWeek.MONDAY.getValue(), TimeZoneUtil.GMT,
				ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS)).thenReturn(dates);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonAppointment);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);

		String expectedAppointmentJson = "{\"date\":\"Mon 3 May\"}";
		when(mockArray.toJSONString()).thenReturn("[" + expectedAppointmentJson + "]");

		Response result = wasteRESTApplication.getBulkyCollectionDatesByUPRN(UPRN, SERVICE_ID, String.valueOf(NUMBER_OF_DATES_TO_RETURN), String.valueOf(ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS),
				String.valueOf(DayOfWeek.MONDAY.getValue()), mockRequest);

		assertThat(result.getEntity(), equalTo("[" + expectedAppointmentJson + "]"));
		verify(mockJsonAppointment, times(1)).put("date", formattedStartDate);
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void getBulkyCollectionDatesByUPRN_WhenDayOfWeekIsNotBlank_AndThemeDisplayIsNull_ThenReturnJSONArraysWithAppoinments() throws WasteRetrievalException {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);
		when(LocaleUtil.getDefault()).thenReturn(Locale.CANADA);
		when(TimeZoneUtil.getDefault()).thenReturn(TimeZoneUtil.GMT);
		when(DateUtil.newDate()).thenReturn(mockDate);

		List<Date> dates = new ArrayList<>();
		dates.add(mockDate);

		String formattedStartDate = "Mon 3 May";
		when(DateUtil.getDate(mockDate, WasteRESTApplication.APPOINMENT_DATE_PATTERN, Locale.CANADA, TimeZoneUtil.GMT)).thenReturn(formattedStartDate);

		when(mockAppointmentBookingService.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN, DayOfWeek.MONDAY.getValue(), TimeZoneUtil.GMT,
				ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS)).thenReturn(dates);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonAppointment);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);

		String expectedAppointmentJson = "{\"date\":\"Mon 3 May\"}";
		when(mockArray.toJSONString()).thenReturn("[" + expectedAppointmentJson + "]");

		Response result = wasteRESTApplication.getBulkyCollectionDatesByUPRN(UPRN, SERVICE_ID, String.valueOf(NUMBER_OF_DATES_TO_RETURN), String.valueOf(ONLY_DATES_WITH_LESS_THAN_X_APPOINMENTS),
				String.valueOf(DayOfWeek.MONDAY.getValue()), mockRequest);

		assertThat(result.getEntity(), equalTo("[" + expectedAppointmentJson + "]"));
		verify(mockJsonAppointment, times(1)).put("date", formattedStartDate);
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void getMissedBinCollectionResponse_WhenError_ThenThrowsWasteRetrievalException() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.getMissedBinCollectionResponse(COMPANY_ID, UPRN, START_DATE)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.getMissedBinCollectionResponse(UPRN, START_DATE, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void getMissedBinCollectionResponse_WhenNoError_ThenReturnsBinCollectionDatesJSON() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockWasteService.getMissedBinCollectionResponse(COMPANY_ID, UPRN, START_DATE)).thenReturn(VALUE);

		Response result = wasteRESTApplication.getMissedBinCollectionResponse(UPRN, START_DATE, mockRequest);

		assertEquals(VALUE, result.getEntity());
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
	}

	@Test
	public void howManyBinsSubscribed_WhenErrorGettingValueFromAPI_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.howManyBinsSubscribed(COMPANY_ID, UPRN, SERVICE_TYPE)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.howManyBinsSubscribed(UPRN, SERVICE_TYPE, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void howManyBinsSubscribed_WhenNoError_ThenReturnsValue() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.howManyBinsSubscribed(COMPANY_ID, UPRN, SERVICE_TYPE)).thenReturn(0);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		wasteRESTApplication.howManyBinsSubscribed(UPRN, SERVICE_TYPE, mockRequest);

		verify(mockJsonCollection, times(1)).put("numberOfBins", 0);
	}

	@Test
	public void isPropertyEligibleForGardenWasteSubscription_WhenErrorGettingValueFromAPI_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, START_DATE, END_DATE)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.isPropertyEligibleForGardenWasteSubscription(UPRN, START_DATE, END_DATE, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void isPropertyEligibleForGardenWasteSubscription_WhenNoError_ThenReturnsValue() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, START_DATE, END_DATE)).thenReturn(true);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		wasteRESTApplication.isPropertyEligibleForGardenWasteSubscription(UPRN, START_DATE, END_DATE, mockRequest);

		verify(mockJsonCollection, times(1)).put("isPropertyEligible", true);
	}

	@Test
	public void isPropertyEligibleForGardenWasteSubscriptionByBin_WhenErrorGettingValueFromAPI_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, START_DATE, END_DATE, BIN_NAME, FORM_INSTANCE_ID)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.isPropertyEligibleForGardenWasteSubscriptionByBin(UPRN, START_DATE, END_DATE, FORM_INSTANCE_ID, BIN_NAME, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void isPropertyEligibleForGardenWasteSubscriptionByBin_WhenNoError_ThenReturnsValue() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, START_DATE, END_DATE, BIN_NAME, FORM_INSTANCE_ID)).thenReturn(true);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		wasteRESTApplication.isPropertyEligibleForGardenWasteSubscriptionByBin(UPRN, START_DATE, END_DATE, FORM_INSTANCE_ID, BIN_NAME, mockRequest);

		verify(mockJsonCollection, times(1)).put("isPropertyEligible", true);
	}

	@Test
	public void isPropertyEligibleForBulkyWasteCollection_WhenErrorGettingValueFromAPI_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForBulkyWasteCollection(COMPANY_ID, UPRN, mockItemsToBeCollectedList, FORM_INSTANCE_ID)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.isPropertyEligibleForBulkyWasteCollection(UPRN, mockItemsToBeCollectedList, FORM_INSTANCE_ID, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void isPropertyEligibleForBulkyWasteCollection_WhenNoError_ThenReturnsValue() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForBulkyWasteCollection(COMPANY_ID, UPRN, mockItemsToBeCollectedList, FORM_INSTANCE_ID)).thenReturn(true);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		wasteRESTApplication.isPropertyEligibleForBulkyWasteCollection(UPRN, mockItemsToBeCollectedList, FORM_INSTANCE_ID, mockRequest);

		verify(mockJsonCollection, times(1)).put("canRequestBulkyWasteCollection", true);
	}

	@Test
	public void isPropertyEligibleForWasteContainerRequest_WhenErrorGettingValueFromAPI_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForWasteContainerRequest(COMPANY_ID, UPRN, BIN_NAME, BIN_SIZE, FORM_INSTANCE_ID)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.isPropertyEligibleForWasteContainerRequest(UPRN, BIN_NAME, BIN_SIZE, FORM_INSTANCE_ID, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void isPropertyEligibleForWasteContainerRequest_WhenNoError_ThenReturnsValue() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForWasteContainerRequest(COMPANY_ID, UPRN, BIN_NAME, BIN_SIZE, FORM_INSTANCE_ID)).thenReturn(true);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		wasteRESTApplication.isPropertyEligibleForWasteContainerRequest(UPRN, BIN_NAME, BIN_SIZE, FORM_INSTANCE_ID, mockRequest);

		verify(mockJsonCollection, times(1)).put("canRequestWasteContainer", true);
	}

	@Test
	public void isPropertyEligibleForMissedBinCollectionReport_WhenErrorGettingValueFromAPI_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForMissedBinCollectionReport(COMPANY_ID, UPRN, BIN_NAME, FORM_INSTANCE_ID)).thenThrow(new WasteRetrievalException());

		Response result = wasteRESTApplication.isPropertyEligibleForMissedBinCollectionReport(UPRN, FORM_INSTANCE_ID, BIN_NAME, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), result.getStatus());
	}

	@Test
	public void isPropertyEligibleForMissedBinCollectionReport_WhenNoError_ThenReturnsValue() throws Exception {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockWasteService.isPropertyEligibleForMissedBinCollectionReport(COMPANY_ID, UPRN, BIN_NAME, FORM_INSTANCE_ID)).thenReturn(true);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonCollection);

		wasteRESTApplication.isPropertyEligibleForMissedBinCollectionReport(UPRN, FORM_INSTANCE_ID, BIN_NAME, mockRequest);

		verify(mockJsonCollection, times(1)).put("isPropertyEligible", true);
	}

	@Test
	public void postBookBulkyCollectionDate_WhenErrorCreatingJSONObject_ThenWasteRetrievalExceptionIsThrown() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockJsonFactory.createJSONObject(mockBodyRequest)).thenThrow(new JSONException());

		Response response = wasteRESTApplication.postBookBulkyCollectionDate(mockBodyRequest, SERVICE_ID, mockRequest);
		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
	}

	@Test
	public void postBookBulkyCollectionDate_WhenThemeIsNotNull_ThenBookAppoinmentUsingLocaleAndTimeZoneFromThemeDisplay() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.CANADA);
		when(mockThemeDisplay.getTimeZone()).thenReturn(TimeZoneUtil.GMT);

		when(mockJsonFactory.createJSONObject(mockBodyRequest)).thenReturn(mockJsonAppointment);
		long classPK = 1;
		long classNameId = 2;
		String title = "title";
		String description = "description";
		String date = "Mon 3 May";

		when(mockAppointmentBookingEntryHelper.getClassPK(mockJsonAppointment)).thenReturn(classPK);
		when(mockAppointmentBookingEntryHelper.getClassNameId(mockJsonAppointment)).thenReturn(classNameId);
		when(mockAppointmentBookingEntryHelper.getTitle(mockJsonAppointment)).thenReturn(title);
		when(mockAppointmentBookingEntryHelper.buildDescription(mockJsonAppointment, classPK)).thenReturn(description);
		when(mockAppointmentBookingEntryHelper.getDate(mockJsonAppointment)).thenReturn(date);
		when(DateUtil.parseDate(WasteRESTApplication.APPOINMENT_DATE_PATTERN, date, Locale.CANADA)).thenReturn(mockDate);

		when(mockAppointmentBookingEntryHelper.create(classPK, classNameId, title, description, mockDate, TimeZoneUtil.GMT)).thenReturn(mockAppointmentEntry);

		when(JSONFactoryUtil.looseSerialize(mockAppointmentEntry)).thenReturn("{}");

		Response response = wasteRESTApplication.postBookBulkyCollectionDate(mockBodyRequest, SERVICE_ID, mockRequest);

		verify(mockAppointmentBookingService, times(1)).bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry);
		assertEquals(Status.OK.getStatusCode(), response.getStatus());
	}

	@Test
	public void postBookBulkyCollectionDate_WhenThemeIsNull_ThenBookAppoinmentUsingDefaultLocaleAndTimeZone() throws Exception {

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);
		when(LocaleUtil.getDefault()).thenReturn(Locale.CANADA);
		when(TimeZoneUtil.getDefault()).thenReturn(TimeZoneUtil.GMT);

		when(mockJsonFactory.createJSONObject(mockBodyRequest)).thenReturn(mockJsonAppointment);
		long classPK = 1;
		long classNameId = 2;
		String title = "title";
		String description = "description";
		String date = "Mon 3 May";

		when(mockAppointmentBookingEntryHelper.getClassPK(mockJsonAppointment)).thenReturn(classPK);
		when(mockAppointmentBookingEntryHelper.getClassNameId(mockJsonAppointment)).thenReturn(classNameId);
		when(mockAppointmentBookingEntryHelper.getTitle(mockJsonAppointment)).thenReturn(title);
		when(mockAppointmentBookingEntryHelper.buildDescription(mockJsonAppointment, classPK)).thenReturn(description);
		when(mockAppointmentBookingEntryHelper.getDate(mockJsonAppointment)).thenReturn(date);
		when(DateUtil.parseDate(WasteRESTApplication.APPOINMENT_DATE_PATTERN, date, Locale.CANADA)).thenReturn(mockDate);

		when(mockAppointmentBookingEntryHelper.create(classPK, classNameId, title, description, mockDate, TimeZoneUtil.GMT)).thenReturn(mockAppointmentEntry);

		when(JSONFactoryUtil.looseSerialize(mockAppointmentEntry)).thenReturn("{}");

		wasteRESTApplication.postBookBulkyCollectionDate(mockBodyRequest, SERVICE_ID, mockRequest);

		verify(mockAppointmentBookingService, times(1)).bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry);

	}

	@Test
	public void getWasteBulkyCollectionSlotsByUPRNAndService_WhenNoErrors_ThenReturnsOkResponseWithCollectionDatesAsJson() throws WasteRetrievalException {
		String date = "Mon 3 May";

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.CANADA);
		when(mockThemeDisplay.getTimeZone()).thenReturn(TimeZoneUtil.GMT);

		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject1);
		when(DateUtil.getDate(COLLECTION_DATE, WasteRESTApplication.APPOINMENT_DATE_PATTERN, Locale.CANADA, TimeZoneUtil.GMT)).thenReturn(date);

		when(mockWasteService.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN)).thenReturn(Arrays.asList(COLLECTION_DATE));
		when(mockArray.toJSONString()).thenReturn(JSON);
		when(mockJsonObject1.put("date", date)).thenReturn(mockJsonObject2);

		Response response = wasteRESTApplication.getWasteBulkyCollectionSlotsByUPRNAndService(UPRN, SERVICE_ID, String.valueOf(NUMBER_OF_DATES_TO_RETURN), mockRequest);

		verify(mockArray, times(1)).put(mockJsonObject2);

		assertThat(response.getEntity(), equalTo(JSON));
		assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
	}

	@Test
	public void getWasteBulkyCollectionSlotsByUPRNAndService_WhenThemeDisplayIsNull_ThenFormatsDateWithDefaultLocaleAndTimezone() throws WasteRetrievalException {
		String date = "Mon 3 May";

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);
		when(LocaleUtil.getDefault()).thenReturn(Locale.CANADA);
		when(TimeZoneUtil.getDefault()).thenReturn(TimeZoneUtil.GMT);

		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject1);
		when(DateUtil.getDate(COLLECTION_DATE, WasteRESTApplication.APPOINMENT_DATE_PATTERN, Locale.CANADA, TimeZoneUtil.GMT)).thenReturn(date);

		when(mockWasteService.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN)).thenReturn(Arrays.asList(COLLECTION_DATE));
		when(mockArray.toJSONString()).thenReturn(JSON);

		wasteRESTApplication.getWasteBulkyCollectionSlotsByUPRNAndService(UPRN, SERVICE_ID, String.valueOf(NUMBER_OF_DATES_TO_RETURN), mockRequest);

		verify(mockJsonObject1, times(1)).put("date", date);
	}

	@Test
	public void getWasteBulkyCollectionSlotsByUPRNAndService_WhenNumberOfDatesToReturnStringIsNotAvalidInt_ThenGetsDefaultNumberOfDates() throws WasteRetrievalException {
		String date = "Mon 3 May";

		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);
		when(LocaleUtil.getDefault()).thenReturn(Locale.CANADA);
		when(TimeZoneUtil.getDefault()).thenReturn(TimeZoneUtil.GMT);

		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject1);
		when(DateUtil.getDate(COLLECTION_DATE, WasteRESTApplication.APPOINMENT_DATE_PATTERN, Locale.CANADA, TimeZoneUtil.GMT)).thenReturn(date);

		when(mockArray.toJSONString()).thenReturn(JSON);

		wasteRESTApplication.getWasteBulkyCollectionSlotsByUPRNAndService(UPRN, SERVICE_ID, null, mockRequest);

		verify(mockWasteService, times(1)).getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, WasteConstants.BULKY_DFLT_NUMBER_OF_DATES_TO_RETURN);
	}

	@Test
	public void getWasteBulkyCollectionSlotsByUPRNAndService_WhenBulkyCollectionSlotsRetrievalFails_ThenReturnsInternalErrorResponse() throws WasteRetrievalException {
		when(mockRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);
		when(LocaleUtil.getDefault()).thenReturn(Locale.CANADA);
		when(TimeZoneUtil.getDefault()).thenReturn(TimeZoneUtil.GMT);

		when(mockJsonFactory.createJSONArray()).thenReturn(mockArray);
		when(mockWasteService.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, WasteConstants.BULKY_DFLT_NUMBER_OF_DATES_TO_RETURN)).thenThrow(new WasteRetrievalException());

		Response response = wasteRESTApplication.getWasteBulkyCollectionSlotsByUPRNAndService(UPRN, SERVICE_ID, null, mockRequest);

		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
	}

}