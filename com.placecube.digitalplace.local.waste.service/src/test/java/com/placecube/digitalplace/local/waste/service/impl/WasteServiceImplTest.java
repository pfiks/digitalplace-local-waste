package com.placecube.digitalplace.local.waste.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BinCollection.BinCollectionBuilder;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.overrideaction.internal.WasteServiceOverrideActionExecutor;
import com.placecube.digitalplace.local.waste.service.WasteConnector;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class WasteServiceImplTest extends PowerMockito {

	private static final Date COLLECTION_DATE = new Date();
	private static final Long COMPANY_ID = 11l;
	private static final String BIN_SIZE = "size";
	private static final String BIN_TYPE = "type";
	private static final String COORDINATES = "11.00";
	private static final String DATE_OF_INCIDENT = "01/01/2022";
	private static final String DETAILS = "details";
	private static final String EMAIL_ADDRESS = "abc@gmail.com";
	private static final String END_DATE = "01/01/2023";
	private static final String FIRST_NAME = "abc";
	private static final String FORM_INSTANCE_ID = "3245";
	private static final String LAST_NAME = "xyz";
	private static final String LOCATION_DETAILS = "location";
	private static final String LOCATION_OF_ITEMS = "pqr";
	private static final String NAME = "recycling";
	private static final int NUMBER_OF_BINS = 3;
	private static final int NUMBER_OF_DATES_TO_RETURN = 10;
	private static final String SERVICE_ID = "234";
	private static final String SERVICE_TYPE = "serviceType";
	private static final String SIZE_OF_RUBBISH = "20";
	private static final String START_DATE = "01/01/2022";
	private static final String TELEPHONE = "989898989";
	private static final String TYPE_OF_RUBBISH = "type";
	private static final String UPRN = "12345678";
	private static final String VALUE = "abcd";

	@Mock
	private AssistedBinCollectionJob mockAssistedBinCollectionJob;

	@Mock
	private BinCollection mockBinCollection;

	@Mock
	private Set<BinCollection> mockBinCollectionSet;

	@Mock
	private BulkyCollectionDate mockBulkyCollectionDate;

	@Mock
	private BulkyWasteCollectionRequest mockBulkyWasteCollectionRequest;

	@Mock
	private GardenWasteSubscription mockGardenWasteSubscription;

	@Mock
	private List<String> mockItemsToBeCollectedList;

	@Mock
	private MissedBinCollectionJob mockMissedBinCollectionJob;

	@Mock
	private WasteConnector mockWasteConnector1;

	@Mock
	private WasteConnector mockWasteConnector2;

	@Mock
	private WasteServiceOverrideActionExecutor mockWasteServiceOverrideActionExecutor;

	@Mock
	private WasteSubscriptionResponse mockWasteSubscriptionResponse;

	@Mock
	private List<WasteSubscriptionResponse> mockWasteSubscriptionResponseList;

	@Mock
	private List<Date> mockDatesList;

	@InjectMocks
	private WasteServiceImpl wasteServiceImpl;

	@Test(expected = WasteRetrievalException.class)
	public void createAssistedBinCollectionJob_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		when(mockAssistedBinCollectionJob.getUprn()).thenReturn(" ");
		wasteServiceImpl.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob);
	}

	@Test
	public void createAssistedBinCollectionJob_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsBinCollectionDates() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockAssistedBinCollectionJob.getUprn()).thenReturn(UPRN);
		when(mockWasteConnector2.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob)).thenReturn(VALUE);

		String result = wasteServiceImpl.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob);

		assertThat(result, sameInstance(VALUE));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createAssistedBinCollectionJob_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionDatesCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockAssistedBinCollectionJob.getUprn()).thenReturn(UPRN);
		when(mockWasteConnector2.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createAssistedBinCollectionJob_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		when(mockAssistedBinCollectionJob.getUprn()).thenReturn(UPRN);

		wasteServiceImpl.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createAssistedBinCollectionJob_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockAssistedBinCollectionJob.getUprn()).thenReturn(null);
		wasteServiceImpl.createAssistedBinCollectionJob(COMPANY_ID, mockAssistedBinCollectionJob);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBinSubscription_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.createBinSubscription(COMPANY_ID, " ", SERVICE_TYPE, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, NUMBER_OF_BINS);
	}

	@Test
	public void createBinSubscription_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsIsPropertyEligible() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.createBinSubscription(COMPANY_ID, UPRN, SERVICE_TYPE, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, NUMBER_OF_BINS)).thenReturn(mockWasteSubscriptionResponse);

		WasteSubscriptionResponse result = wasteServiceImpl.createBinSubscription(COMPANY_ID, UPRN, SERVICE_TYPE, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, NUMBER_OF_BINS);

		assertThat(result, sameInstance(mockWasteSubscriptionResponse));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBinSubscription_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.createBinSubscription(COMPANY_ID, null, SERVICE_TYPE, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, NUMBER_OF_BINS);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, " ", "", "", "", "", mockItemsToBeCollectedList, "", COLLECTION_DATE);
	}

	@Test
	public void createBulkyWasteCollectionJob_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsBulkyWatseCollectionDates() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.createBulkyWasteCollectionJob(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE))
				.thenReturn(VALUE);

		String result = wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS,
				COLLECTION_DATE);

		assertThat(result, sameInstance(VALUE));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionDatesCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.createBulkyWasteCollectionJob(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE))
				.thenThrow(new WasteRetrievalException());

		wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);

		wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, null, null, null, null, null, mockItemsToBeCollectedList, LOCATION_OF_ITEMS, COLLECTION_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenCalledWithBulkyWasteCollectionRequestAndUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(StringPool.BLANK);
		wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE);
	}

	@Test
	public void createBulkyWasteCollectionJob_WhenCalledWithBulkyWasteCollectionRequestAndUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsBulkyWatseCollectionDates() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE)).thenReturn(VALUE);

		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(UPRN);

		String result = wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE);

		assertThat(result, sameInstance(VALUE));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenCalledWithBulkyWasteCollectionRequestAndUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionDatesCannotBeRetrieved_ThenThrowsWasteRetrievalException()
			throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE)).thenThrow(new WasteRetrievalException());

		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(UPRN);

		wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenCalledWithBulkyWasteCollectionRequestAndUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		when(mockBulkyWasteCollectionRequest.getUprn()).thenReturn(UPRN);

		wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createBulkyWasteCollectionJob_WhenCalledWithBulkyWasteCollectionRequestAndUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.createBulkyWasteCollectionJob(COMPANY_ID, mockBulkyWasteCollectionRequest, COLLECTION_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createFlyTippingReport_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.createFlyTippingReport(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, TYPE_OF_RUBBISH, SIZE_OF_RUBBISH, DETAILS, COORDINATES, LOCATION_DETAILS,
				DATE_OF_INCIDENT)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.createFlyTippingReport(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, TYPE_OF_RUBBISH, SIZE_OF_RUBBISH, DETAILS, COORDINATES, LOCATION_DETAILS,
				DATE_OF_INCIDENT);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createFlyTippingReport_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {

		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);

		wasteServiceImpl.createFlyTippingReport(COMPANY_ID, UPRN, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, TELEPHONE, TYPE_OF_RUBBISH, SIZE_OF_RUBBISH, DETAILS, COORDINATES, LOCATION_DETAILS,
				DATE_OF_INCIDENT);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createFlyTippingReport_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.createFlyTippingReport(COMPANY_ID, null, null, null, null, null, null, null, null, null, null, null);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createGardenWasteSubscription_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		when(mockGardenWasteSubscription.getUprn()).thenReturn(" ");
		wasteServiceImpl.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription);
	}

	@Test
	public void createGardenWasteSubscription_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsReference() throws PortalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockGardenWasteSubscription.getUprn()).thenReturn("uprnValue");
		when(mockWasteConnector2.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription)).thenReturn(mockWasteSubscriptionResponse);
		when(mockWasteServiceOverrideActionExecutor.executeActionsAfterWasteSubscriptionResponseCreation(COMPANY_ID, mockWasteSubscriptionResponse)).thenReturn(mockWasteSubscriptionResponse);

		WasteSubscriptionResponse result = wasteServiceImpl.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription);

		verify(mockWasteServiceOverrideActionExecutor, times(1)).executeActionsAfterWasteSubscriptionResponseCreation(COMPANY_ID, mockWasteSubscriptionResponse);
		assertThat(result, sameInstance(mockWasteSubscriptionResponse));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createGardenWasteSubscription_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndReferenceCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockGardenWasteSubscription.getUprn()).thenReturn("uprnValue");
		when(mockWasteConnector2.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createGardenWasteSubscription_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {

		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		when(mockGardenWasteSubscription.getUprn()).thenReturn("uprnValue");

		wasteServiceImpl.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createGardenWasteSubscription_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		when(mockGardenWasteSubscription.getUprn()).thenReturn(null);
		wasteServiceImpl.createGardenWasteSubscription(COMPANY_ID, mockGardenWasteSubscription);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createMissedBinCollectionJob_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(" ");
		wasteServiceImpl.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob);
	}

	@Test
	public void createMissedBinCollectionJob_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsMissedCollectionJobResult() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(UPRN);
		when(mockWasteConnector2.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob)).thenReturn(VALUE);

		String result = wasteServiceImpl.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob);

		assertThat(result, sameInstance(VALUE));
	}

	@Test(expected = WasteRetrievalException.class)
	public void createMissedBinCollectionJob_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionDatesCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(UPRN);
		when(mockWasteConnector2.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createMissedBinCollectionJob_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(UPRN);

		wasteServiceImpl.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob);
	}

	@Test(expected = WasteRetrievalException.class)
	public void createMissedBinCollectionJob_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(null);
		wasteServiceImpl.createMissedBinCollectionJob(COMPANY_ID, mockMissedBinCollectionJob);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRNAndService_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(" ");
		wasteServiceImpl.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID);
	}

	@Test
	public void getBinCollectionDatesByUPRNAndService_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsBinCollectionDates() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID)).thenReturn(mockBinCollectionSet);

		Set<BinCollection> result = wasteServiceImpl.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID);

		assertThat(result, sameInstance(mockBinCollectionSet));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRNAndService_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionDatesCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(UPRN);
		when(mockWasteConnector2.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRNAndService_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(UPRN);

		wasteServiceImpl.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRNAndService_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		when(mockMissedBinCollectionJob.getUprn()).thenReturn(null);
		wasteServiceImpl.getBinCollectionDatesByUPRNAndService(COMPANY_ID, UPRN, SERVICE_TYPE, FORM_INSTANCE_ID);
	}

	@Test
	public void getAllWasteSubscriptions_WhenEnabledWasteConnectorFound_ThenReturnsListWithTheWasteSubscriptionResponses() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getAllWasteSubscriptions(COMPANY_ID, SubscriptionType.GARDEN_WASTE)).thenReturn(mockWasteSubscriptionResponseList);

		List<WasteSubscriptionResponse> result = wasteServiceImpl.getAllWasteSubscriptions(COMPANY_ID, SubscriptionType.GARDEN_WASTE);

		assertThat(result, sameInstance(mockWasteSubscriptionResponseList));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getAllWasteSubscriptions_WhenEnabledWasteConnectorFoundAndExceptionRetrievingSubscriptions_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getAllWasteSubscriptions(COMPANY_ID, SubscriptionType.GARDEN_WASTE)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.getAllWasteSubscriptions(COMPANY_ID, SubscriptionType.GARDEN_WASTE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getAllWasteSubscriptions_WhenNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);

		wasteServiceImpl.getAllWasteSubscriptions(COMPANY_ID, SubscriptionType.GARDEN_WASTE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionByUPRNAndName_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getBinCollectionByUPRNAndName(COMPANY_ID, " ", NAME);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionByUPRNAndName_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionDatesCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.getBinCollectionByUPRNAndName(COMPANY_ID, UPRN, NAME);
	}

	@Test
	public void getBinCollectionByUPRNAndName_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionMatchingNameIsFound_ThenReturnsBinCollection() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);

		Set<BinCollection> binCollections = new HashSet<>();
		Map<Locale, String> labelMap = new HashMap<>();
		labelMap.put(LocaleUtil.getDefault(), NAME);
		BinCollection binCollection = new BinCollectionBuilder(NAME, LocalDate.now(), labelMap).build();
		binCollections.add(binCollection);

		when(mockWasteConnector2.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenReturn(binCollections);

		Optional<BinCollection> result = wasteServiceImpl.getBinCollectionByUPRNAndName(COMPANY_ID, UPRN, NAME);

		assertThat(result.get(), sameInstance(binCollection));
	}

	@Test
	public void getBinCollectionByUPRNAndName_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionMatchingNameIsNotFound_ThenReturnsEmptyBinCollectionOptional()
			throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);

		Set<BinCollection> binCollections = new HashSet<>();
		when(mockWasteConnector2.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenReturn(binCollections);

		Optional<BinCollection> result = wasteServiceImpl.getBinCollectionByUPRNAndName(COMPANY_ID, UPRN, NAME);

		assertTrue(result.isEmpty());
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionByUPRNAndName_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);

		wasteServiceImpl.getBinCollectionByUPRNAndName(COMPANY_ID, UPRN, NAME);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionByUPRNAndName_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getBinCollectionByUPRNAndName(COMPANY_ID, null, NAME);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRN_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getBinCollectionDatesByUPRN(COMPANY_ID, " ");
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsBinCollectionDates() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenReturn(mockBinCollectionSet);

		Set<BinCollection> result = wasteServiceImpl.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);

		assertThat(result, sameInstance(mockBinCollectionSet));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRN_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBinCollectionDatesCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRN_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);

		wasteServiceImpl.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBinCollectionDatesByUPRN_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getBinCollectionDatesByUPRN(COMPANY_ID, null);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionDateByUPRN_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getBulkyCollectionDateByUPRN(COMPANY_ID, " ");
	}

	@Test
	public void getBulkyCollectionDateByUPRN_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsBulkyCollectionDate() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenReturn(Optional.of(mockBulkyCollectionDate));

		Optional<BulkyCollectionDate> result = wasteServiceImpl.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN);

		assertThat(result.get(), sameInstance(mockBulkyCollectionDate));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionDateByUPRN_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBulkyCollectionDateCannotBeRetrieved_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionDateByUPRN_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);

		wasteServiceImpl.getBulkyCollectionDateByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionDateByUPRN_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getBulkyCollectionDateByUPRN(COMPANY_ID, null);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionSlotsByUPRNAndService_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);
	}

	@Test
	public void getBulkyCollectionSlotsByUPRNAndService_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsBulkyCollectionSlots() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN)).thenReturn(mockDatesList);

		List<Date> result = wasteServiceImpl.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);

		assertThat(result, sameInstance(mockDatesList));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionSlotsByUPRNAndService_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndBulkyCollectionSlotsCannotBeRetrieved_ThenThrowsWasteRetrievalException()
			throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionSlotsByUPRNAndService_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);

		wasteServiceImpl.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getBulkyCollectionSlotsByUPRNAndService_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getBulkyCollectionSlotsByUPRNAndService(COMPANY_ID, UPRN, SERVICE_ID, NUMBER_OF_DATES_TO_RETURN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getMissedBinCollectionResponse_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getMissedBinCollectionResponse(COMPANY_ID, " ", START_DATE);
	}

	@Test
	public void getMissedBinCollectionResponse_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsMissedBinCollectionResponse() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getMissedBinCollectionResponse(COMPANY_ID, UPRN, START_DATE)).thenReturn(VALUE);

		String result = wasteServiceImpl.getMissedBinCollectionResponse(COMPANY_ID, UPRN, START_DATE);

		assertThat(result, sameInstance(VALUE));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getMissedBinCollectionResponse_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFoundAndGetMissedBinCollectionResponseCannotBeRetrieved_ThenThrowsWasteRetrievalException()
			throws WasteRetrievalException {

		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.getMissedBinCollectionResponse(COMPANY_ID, UPRN, START_DATE)).thenThrow(new WasteRetrievalException());

		wasteServiceImpl.getMissedBinCollectionResponse(COMPANY_ID, UPRN, START_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getMissedBinCollectionResponse_WhenUPRNIsNotEmptyAndNoEnabledWasteConnectorFound_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);

		wasteServiceImpl.getMissedBinCollectionResponse(COMPANY_ID, UPRN, START_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getMissedBinCollectionResponse_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws WasteRetrievalException {
		wasteServiceImpl.getMissedBinCollectionResponse(COMPANY_ID, null, null);
	}

	@Test
	public void getWasteConnector_twoConnectorsAvailable_returnEnabledOne() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);

		WasteConnector result = wasteServiceImpl.getWasteConnector(COMPANY_ID);

		assertEquals(mockWasteConnector2, result);
	}

	@Test
	public void getWasteConnector_twoEnabledConnectors_returnFirstOne() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, true);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);

		WasteConnector result = wasteServiceImpl.getWasteConnector(COMPANY_ID);

		assertEquals(mockWasteConnector1, result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getWasteConnectorConnector_noEnabledConnectorsForCompany_Exception() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(1l, mockWasteConnector2, true);

		wasteServiceImpl.getWasteConnector(COMPANY_ID);
	}

	@Test
	public void getWasteConnectorList_WhenWasteConnectorListHasValue_ThenReturnsWasteConnectorNameList() throws Exception {
		String name1 = "waste-mock";
		String name2 = "waste-echo";
		List<String> mockWasteConnectors = new ArrayList<>();
		mockWasteConnectors.add(name1);
		mockWasteConnectors.add(name2);
		addWasteConnector(COMPANY_ID, mockWasteConnector1, true);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector1.getName(COMPANY_ID)).thenReturn(name1);
		when(mockWasteConnector2.getName(COMPANY_ID)).thenReturn(name2);
		List<String> result = wasteServiceImpl.getWasteConnectorList(COMPANY_ID);

		assertEquals(mockWasteConnectors, result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getWasteConnectorList_WhenWasteConnectorListIsEmpty_ThenThrowWasteRetrievalException() throws Exception {

		wasteServiceImpl.getWasteConnectorList(COMPANY_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void howManyBinsSubscribed_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.howManyBinsSubscribed(COMPANY_ID, " ", SERVICE_TYPE);
	}

	@Test
	public void howManyBinsSubscribed_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsIsPropertyEligible() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.howManyBinsSubscribed(COMPANY_ID, UPRN, SERVICE_TYPE)).thenReturn(0);

		int result = wasteServiceImpl.howManyBinsSubscribed(COMPANY_ID, UPRN, SERVICE_TYPE);

		assertEquals(0, result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void howManyBinsSubscribed_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.howManyBinsSubscribed(COMPANY_ID, null, SERVICE_TYPE);
	}

	@Test
	public void isPropertyEligibleForGardenWasteSubscription_WhenCalledWithBinTypeAndUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsIsPropertyEligible() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, START_DATE, END_DATE, BIN_TYPE, FORM_INSTANCE_ID)).thenReturn(true);

		boolean result = wasteServiceImpl.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, START_DATE, END_DATE, BIN_TYPE, FORM_INSTANCE_ID);

		assertTrue(result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForGardenWasteSubscription_WhenCalledWithBinTypeAndUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, " ", START_DATE, END_DATE, BIN_TYPE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForGardenWasteSubscription_WhenCalledWithBinTypeAndUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, null, START_DATE, END_DATE, BIN_TYPE, FORM_INSTANCE_ID);
	}

	@Test
	public void isPropertyEligibleForGardenWasteSubscription_WhenCalledWithoutBinTypeAndUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsIsPropertyEligible() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, START_DATE, END_DATE)).thenReturn(true);

		boolean result = wasteServiceImpl.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, UPRN, START_DATE, END_DATE);

		assertTrue(result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForGardenWasteSubscription_WhenCalledWithoutBinTypeAndUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, " ", START_DATE, END_DATE);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForGardenWasteSubscription_WhenCalledWithoutBinTypeAndUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForGardenWasteSubscription(COMPANY_ID, null, START_DATE, END_DATE);
	}

	@Test
	public void isPropertyEligibleForBulkyWasteCollection_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsIsPropertyEligible() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.isPropertyEligibleForBulkyWasteCollection(COMPANY_ID, UPRN, mockItemsToBeCollectedList, FORM_INSTANCE_ID)).thenReturn(true);

		boolean result = wasteServiceImpl.isPropertyEligibleForBulkyWasteCollection(COMPANY_ID, UPRN, mockItemsToBeCollectedList, FORM_INSTANCE_ID);

		assertTrue(result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForBulkyWasteCollection_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForBulkyWasteCollection(COMPANY_ID, UPRN, mockItemsToBeCollectedList, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForBulkyWasteCollection_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForBulkyWasteCollection(COMPANY_ID, UPRN, mockItemsToBeCollectedList, FORM_INSTANCE_ID);
	}

	@Test
	public void isPropertyEligibleForMissedBinCollectionReport_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsIsPropertyEligible() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.isPropertyEligibleForMissedBinCollectionReport(COMPANY_ID, UPRN, BIN_TYPE, FORM_INSTANCE_ID)).thenReturn(true);

		boolean result = wasteServiceImpl.isPropertyEligibleForMissedBinCollectionReport(COMPANY_ID, UPRN, BIN_TYPE, FORM_INSTANCE_ID);

		assertTrue(result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForMissedBinCollectionReport_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForMissedBinCollectionReport(COMPANY_ID, UPRN, BIN_TYPE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForMissedBinCollectionReport_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForMissedBinCollectionReport(COMPANY_ID, UPRN, BIN_TYPE, FORM_INSTANCE_ID);
	}

	@Test
	public void isPropertyEligibleForWasteContainerRequest_WhenUPRNIsNotEmptyAndEnabledWasteConnectorFound_ThenReturnsIsPropertyEligible() throws Exception {
		addWasteConnector(COMPANY_ID, mockWasteConnector1, false);
		addWasteConnector(COMPANY_ID, mockWasteConnector2, true);
		when(mockWasteConnector2.isPropertyEligibleForWasteContainerRequest(COMPANY_ID, UPRN, BIN_TYPE, BIN_SIZE, FORM_INSTANCE_ID)).thenReturn(true);

		boolean result = wasteServiceImpl.isPropertyEligibleForWasteContainerRequest(COMPANY_ID, UPRN, BIN_TYPE, BIN_SIZE, FORM_INSTANCE_ID);

		assertTrue(result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForWasteContainerRequest_WhenUPRNIsEmpty_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForWasteContainerRequest(COMPANY_ID, UPRN, BIN_TYPE, BIN_SIZE, FORM_INSTANCE_ID);
	}

	@Test(expected = WasteRetrievalException.class)
	public void isPropertyEligibleForWasteContainerRequest_WhenUPRNIsNull_ThenThrowsWasteRetrievalException() throws Exception {
		wasteServiceImpl.isPropertyEligibleForWasteContainerRequest(COMPANY_ID, UPRN, BIN_TYPE, BIN_SIZE, FORM_INSTANCE_ID);
	}

	@Before
	public void setUp() {
		initMocks(this);
		wasteServiceImpl = new WasteServiceImpl();
		wasteServiceImpl.wasteServiceOverrideActionExecutor = mockWasteServiceOverrideActionExecutor;
	}

	private void addWasteConnector(long companyId, WasteConnector wasteConnector, boolean enabled) {
		when(wasteConnector.enabled(companyId)).thenReturn(enabled);
		wasteServiceImpl.setWasteConnector(wasteConnector);
	}
}
