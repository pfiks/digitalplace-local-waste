package com.placecube.digitalplace.local.waste.overrideaction.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.overrideaction.WasteServiceOverrideAction;

public class WasteServiceOverrideActionExecutorTest extends PowerMockito {

	@Mock
	private WasteServiceOverrideAction mockWasteServiceOverrideAction1;

	@Mock
	private WasteServiceOverrideAction mockWasteServiceOverrideAction2;

	@Mock
	private WasteServiceOverrideActionRegistry mockWasteServiceOverrideActionRegistry;

	@Mock
	private WasteSubscriptionResponse mockWasteSubscriptionResponse;

	@InjectMocks
	private WasteServiceOverrideActionExecutor wasteServiceOverrideActionExecutor;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void executeActionsAfterWasteSubscriptionResponseCreation_WhenNoError_ThenExecutesAllOverrideActionsFromTheRegistry() {
		long companyId = 123;

		when(mockWasteServiceOverrideActionRegistry.getWasteServiceOverrideActions()).thenReturn(List.of(mockWasteServiceOverrideAction1, mockWasteServiceOverrideAction2));
		when(mockWasteServiceOverrideAction1.executeOnAfterWasteSubscriptionResponseCreation(companyId, mockWasteSubscriptionResponse)).thenReturn(mockWasteSubscriptionResponse);
		when(mockWasteServiceOverrideAction2.executeOnAfterWasteSubscriptionResponseCreation(companyId, mockWasteSubscriptionResponse)).thenReturn(mockWasteSubscriptionResponse);

		WasteSubscriptionResponse result = wasteServiceOverrideActionExecutor.executeActionsAfterWasteSubscriptionResponseCreation(companyId, mockWasteSubscriptionResponse);

		assertThat(result, equalTo(mockWasteSubscriptionResponse));
		verify(mockWasteServiceOverrideAction1, times(1)).executeOnAfterWasteSubscriptionResponseCreation(companyId, mockWasteSubscriptionResponse);
		verify(mockWasteServiceOverrideAction2, times(1)).executeOnAfterWasteSubscriptionResponseCreation(companyId, mockWasteSubscriptionResponse);
	}

}
