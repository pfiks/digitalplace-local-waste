package com.placecube.digitalplace.local.waste.bartec.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class BartecWasteServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String PASSWORD = "PASSWORD";
	private static final String USERNAME = "USERNAME";

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private BartecConfiguration mockBartecCompanyConfiguration;

	@InjectMocks
	private BartecWasteService bartecWasteService;

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenErrorGettingConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(BartecConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		bartecWasteService.getConfiguration(COMPANY_ID);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsConfiguration() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(BartecConfiguration.class, COMPANY_ID)).thenReturn(mockBartecCompanyConfiguration);

		BartecConfiguration result = bartecWasteService.getConfiguration(COMPANY_ID);

		assertEquals(mockBartecCompanyConfiguration, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getPassword_WhenPasswordIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockBartecCompanyConfiguration.password()).thenReturn(StringPool.BLANK);

		bartecWasteService.getPassword(mockBartecCompanyConfiguration);
	}

	@Test
	public void getPassword_WhenPasswordIsNotBlank_ThenReturnsPassword() throws Exception {
		when(mockBartecCompanyConfiguration.password()).thenReturn(PASSWORD);

		String result = bartecWasteService.getPassword(mockBartecCompanyConfiguration);

		assertEquals(PASSWORD, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getUsername_WhenUsernameIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockBartecCompanyConfiguration.username()).thenReturn(StringPool.BLANK);

		bartecWasteService.getUsername(mockBartecCompanyConfiguration);
	}

	@Test
	public void getUsername_WhenUsernameIsNotBlank_ThenReturnsUsername() throws Exception {
		when(mockBartecCompanyConfiguration.username()).thenReturn(USERNAME);

		String result = bartecWasteService.getUsername(mockBartecCompanyConfiguration);

		assertEquals(USERNAME, result);
	}

	@Test(expected = ConfigurationException.class)
	public void isEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(BartecConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		bartecWasteService.isEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(BartecConfiguration.class, COMPANY_ID)).thenReturn(mockBartecCompanyConfiguration);
		when(mockBartecCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = bartecWasteService.isEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}
}
