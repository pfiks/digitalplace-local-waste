package com.placecube.digitalplace.local.waste.bartec.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_GetResponse;
import com.placecube.digitalplace.local.waste.model.BinCollection;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class BartecWasteConnectorTest extends PowerMockito {

	private static final Set<BinCollection> BIN_COLLECTIONS = new HashSet<>();
	private static final long COMPANY_ID = 10;
	private static final String UPRN = "12345678";

	@InjectMocks
	private BartecWasteConnector bartecWasteConnector;

	@Mock
	private BartecRequestService mockBartecRequestService;

	@Mock
	private BartecWasteService mockBartecWasteService;

	@Mock
	private ResponseParserService mockResponseParserService;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockBartecWasteService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = bartecWasteConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockBartecWasteService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = bartecWasteConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getBinCollectionDatesByUPRN_WhenNoErrors_ThenReturnBinCollections() throws Exception {

		BigDecimal UPRN1 = BigDecimal.valueOf(Double.valueOf(UPRN));

		Jobs_GetResponse mockResponse = mock(Jobs_GetResponse.class);

		when(mockBartecRequestService.getJobs(eq(COMPANY_ID), eq(UPRN1), any(BigDecimal.class), anyInt(), any(), anyInt(), anyInt(), anyInt(), any(), anyBoolean())).thenReturn(mockResponse);
		when(mockResponseParserService.getBinCollections(mockResponse)).thenReturn(BIN_COLLECTIONS);

		Set<BinCollection> result = bartecWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);

		assertEquals(BIN_COLLECTIONS, result);
	}

}
