package com.placecube.digitalplace.local.waste.bartec.application;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.local.waste.bartec.axis.*;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtAttachedDocument;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtPointMetric;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterBusiness;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact;
import com.placecube.digitalplace.local.waste.bartec.service.BartecRequestService;
import com.placecube.digitalplace.local.waste.bartec.service.JsonResponseParserService;

@RunWith(PowerMockRunner.class)
public class BartecRESTApplicationTest extends PowerMockito {

	private static final long COMPANY_ID = 89764l;
	private static final String PARAM1 = "PARAM1";
	private static final String PARAM10 = "PARAM10";
	private static final BigDecimal PARAM11 = BigDecimal.TEN;
	private static final ArrayOfInt PARAM12 = new ArrayOfInt();
	private static final ArrayOfInt PARAM13 = new ArrayOfInt();
	private static final ArrayOfInt PARAM14 = new ArrayOfInt();
	private static final ArrayOfInt PARAM15 = new ArrayOfInt();
	private static final ArrayOfInt PARAM16 = new ArrayOfInt();
	private static final ArrayOfInt PARAM17 = new ArrayOfInt();
	private static final ArrayOfString PARAM18 = null;
	private static final ArrayOfInt PARAM19 = new ArrayOfInt();
	private static final String PARAM2 = "PARAM2";
	private static final int PARAM20 = 0;
	private static final String PARAM21 = "PARAM21";
	private static final ArrayOfInt PARAM22 = new ArrayOfInt();
	private static final Calendar PARAM23 = Calendar.getInstance();
	private static final CtDateQuery PARAM24 = null;
	private static final CtMapBox PARAM25 = null;
	private static final boolean PARAM26 = false;
	private static final CtDateQuery PARAM27 = null;
	private static final ArrayOfDecimal PARAM28 = null;
	private static final CtAttachedDocument PARAM29 = null;
	private static final String PARAM3 = "PARAM3";
	private static final CtPointMetric PARAM30 = null;
	private static final ArrayOfServiceRequest_CreateServiceRequest_CreateFields PARAM31 = null;
	private static final ArrayOfServiceRequest_CreateServiceRequest_CreateLocation PARAM32 = null;
	private static final ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest PARAM33 = null;
	private static final ServiceRequest_CreateServiceRequest_CreateReporterBusiness PARAM34 = null;
	private static final ServiceRequest_CreateServiceRequest_CreateReporterContact PARAM35 = null;
	private static final CtMapPoint_Set PARAM36 = null;
	private static final String PARAM37 = "PARAM37";
	private static final String PARAM4 = "PARAM4";
	private static final int PARAM5 = 2;
	private static final String PARAM6 = "PARAM6";
	private static final String PARAM7 = "PARAM7";
	private static final String PARAM8 = "PARAM8";
	private static final String PARAM9 = "PARAM9";

	private static final String RESPONSE = "[{\"answer\":true}]";

	@InjectMocks
	private BartecRESTApplication bartecRESTApplication = new BartecRESTApplication();

	@Mock
	private BartecRequestService mockBartecRequestService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JsonResponseParserService mockJsonResponseParserService;

	@Before
	public void activate() {
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(COMPANY_ID);

	}

	@Test
	public void cancelServiceRequest_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_CancelResponse mockResponse = mock(ServiceRequest_CancelResponse.class);
		when(mockBartecRequestService.cancelServiceRequest(COMPANY_ID, PARAM20, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.cancelServiceRequest(PARAM20, PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void cancelServiceRequestAppointment_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_Appointment_Reservation_CancelResponse mockResponse = mock(ServiceRequest_Appointment_Reservation_CancelResponse.class);
		when(mockBartecRequestService.cancelServiceRequestAppointment(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.cancelServiceRequestAppointment(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void closeJobs_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Jobs_CloseResponse mockResponse = mock(Jobs_CloseResponse.class);
		when(mockBartecRequestService.closeJobs(COMPANY_ID, PARAM5, PARAM26, PARAM26, PARAM26, PARAM1, PARAM3, PARAM4, PARAM23)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.closeJobs(PARAM5, PARAM26, PARAM26, PARAM26, PARAM1, PARAM3, PARAM4, PARAM23, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createBusinessDocument_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Business_Document_CreateResponse mockResponse = mock(Business_Document_CreateResponse.class);
		when(mockBartecRequestService.createBusinessDocument(COMPANY_ID, PARAM20, PARAM29, PARAM23, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createBusinessDocument(PARAM20, PARAM29, PARAM23, PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createPremisesAttribute_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Attribute_CreateResponse mockResponse = mock(Premises_Attribute_CreateResponse.class);
		when(mockBartecRequestService.createPremisesAttribute(COMPANY_ID, PARAM5, PARAM10, PARAM1, PARAM20, PARAM23, PARAM23, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createPremisesAttribute(PARAM5, PARAM10, PARAM1, PARAM20, PARAM23, PARAM23, PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createPremisesDocument_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Document_CreateResponse mockResponse = mock(Premises_Document_CreateResponse.class);
		when(mockBartecRequestService.createPremisesDocument(COMPANY_ID, PARAM29, PARAM1, PARAM23, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createPremisesDocument(PARAM29, PARAM1, PARAM23, PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createPremisesEvent_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Event_CreateResponse mockResponse = mock(Premises_Event_CreateResponse.class);
		when(mockBartecRequestService.createPremisesEvent(COMPANY_ID, PARAM11, PARAM1, PARAM20, PARAM20, PARAM23, PARAM30, PARAM20, PARAM18, PARAM12, PARAM20, PARAM18)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createPremisesEvent(PARAM11, PARAM1, PARAM20, PARAM20, PARAM23, PARAM30, PARAM20, PARAM18, PARAM12, PARAM20, PARAM18, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createPremisesEventsDocument_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Events_Document_CreateResponse mockResponse = mock(Premises_Events_Document_CreateResponse.class);
		when(mockBartecRequestService.createPremisesEventsDocument(COMPANY_ID, PARAM29, PARAM1, PARAM23, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createPremisesEventsDocument(PARAM29, PARAM1, PARAM23, PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createServiceRequest_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_CreateResponse mockResponse = mock(ServiceRequest_CreateResponse.class);
		when(mockBartecRequestService.createServiceRequest(COMPANY_ID, PARAM20, PARAM31, PARAM20, PARAM23, PARAM32, PARAM20, PARAM33, PARAM34, PARAM10, PARAM35, PARAM36, PARAM20, PARAM5, PARAM20,
				PARAM10, PARAM11, PARAM37)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createServiceRequest(PARAM20, PARAM31, PARAM20, PARAM23, PARAM32, PARAM20, PARAM33, PARAM34, PARAM10, PARAM35, PARAM36, PARAM20, PARAM5, PARAM20,
				PARAM10, PARAM11, PARAM37, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createServiceRequestAppointmentReservation_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_Appointment_Reservation_CreateResponse mockResponse = mock(ServiceRequest_Appointment_Reservation_CreateResponse.class);
		when(mockBartecRequestService.createServiceRequestAppointmentReservation(COMPANY_ID, PARAM20, PARAM20, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createServiceRequestAppointmentReservation(PARAM20, PARAM20, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createServiceRequestDocument_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_Document_CreateResponse mockResponse = mock(ServiceRequest_Document_CreateResponse.class);
		when(mockBartecRequestService.createServiceRequestDocument(COMPANY_ID, PARAM29, PARAM1, PARAM23, PARAM26, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createServiceRequestDocument(PARAM29, PARAM1, PARAM23, PARAM26, PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createServiceRequestNote_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_Note_CreateResponse mockResponse = mock(ServiceRequest_Note_CreateResponse.class);
		when(mockBartecRequestService.createServiceRequestNote(COMPANY_ID, PARAM20, PARAM2, PARAM10, PARAM1, PARAM5, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createServiceRequestNote(PARAM20, PARAM2, PARAM10, PARAM1, PARAM5, PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createSiteDocument_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Site_Document_CreateResponse mockResponse = mock(Site_Document_CreateResponse.class);
		when(mockBartecRequestService.createSiteDocument(COMPANY_ID, PARAM29, PARAM1, PARAM23, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createSiteDocument(PARAM29, PARAM1, PARAM23, PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createVehicleMessage_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Vehicle_Message_CreateResponse mockResponse = mock(Vehicle_Message_CreateResponse.class);
		when(mockBartecRequestService.createVehicleMessage(COMPANY_ID, PARAM20, PARAM10, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createVehicleMessage(PARAM20, PARAM10, PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void createWorkPackNote_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Workpack_Note_CreateResponse mockResponse = mock(Workpack_Note_CreateResponse.class);
		when(mockBartecRequestService.createWorkPackNote(COMPANY_ID, PARAM2, PARAM10, PARAM1, PARAM5, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.createWorkPackNote(PARAM2, PARAM10, PARAM1, PARAM5, PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void detetePremisesAttributes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Attributes_DeleteResponse mockResponse = mock(Premises_Attributes_DeleteResponse.class);
		when(mockBartecRequestService.detetePremisesAttributes(COMPANY_ID, PARAM20, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.detetePremisesAttributes(PARAM20, PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void extendServiceRequestAppointmentReservation_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_Appointment_Reservation_ExtendResponse mockResponse = mock(ServiceRequest_Appointment_Reservation_ExtendResponse.class);
		when(mockBartecRequestService.extendServiceRequestAppointmentReservation(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.extendServiceRequestAppointmentReservation(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getBusiness_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Businesses_GetResponse mockResponse = mock(Businesses_GetResponse.class);
		when(mockBartecRequestService.getBusiness(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM22, PARAM22, PARAM22, PARAM22, PARAM23)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getBusiness(PARAM1, PARAM2, PARAM3, PARAM4, PARAM22, PARAM22, PARAM22, PARAM22, PARAM23, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getBusinessClasses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Businesses_Classes_GetResponse mockResponse = mock(Businesses_Classes_GetResponse.class);
		when(mockBartecRequestService.getBusinessClasses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getBusinessClasses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getBusinessDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Businesses_Documents_GetResponse mockResponse = mock(Businesses_Documents_GetResponse.class);
		when(mockBartecRequestService.getBusinessDocuments(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getBusinessDocuments(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getBusinessStatuses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Businesses_Statuses_GetResponse mockResponse = mock(Businesses_Statuses_GetResponse.class);
		when(mockBartecRequestService.getBusinessStatuses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getBusinessStatuses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getBusinessSubStatuses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Businesses_SubStatuses_GetResponse mockResponse = mock(Businesses_SubStatuses_GetResponse.class);
		when(mockBartecRequestService.getBusinessSubStatuses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getBusinessSubStatuses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getBusinessTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Businesses_Types_GetResponse mockResponse = mock(Businesses_Types_GetResponse.class);
		when(mockBartecRequestService.getBusinessTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getBusinessTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getCaseDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Cases_Documents_GetResponse mockResponse = mock(Cases_Documents_GetResponse.class);
		when(mockBartecRequestService.getCaseDocuments(COMPANY_ID, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getCaseDocuments(PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getCrews_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Crews_GetResponse mockResponse = mock(Crews_GetResponse.class);
		when(mockBartecRequestService.getCrews(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getCrews(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatureCategories_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Categories_GetResponse mockResponse = mock(Features_Categories_GetResponse.class);
		when(mockBartecRequestService.getFeatureCategories(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatureCategories(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatureClasses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Classes_GetResponse mockResponse = mock(Features_Classes_GetResponse.class);
		when(mockBartecRequestService.getFeatureClasses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatureClasses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatureColours_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Colours_GetResponse mockResponse = mock(Features_Colours_GetResponse.class);
		when(mockBartecRequestService.getFeatureColours(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatureColours(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatureConditions_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Conditions_GetResponse mockResponse = mock(Features_Conditions_GetResponse.class);
		when(mockBartecRequestService.getFeatureConditions(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatureConditions(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatureManufacturers_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Manufacturers_GetResponse mockResponse = mock(Features_Manufacturers_GetResponse.class);
		when(mockBartecRequestService.getFeatureManufacturers(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatureManufacturers(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatureNotes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Notes_GetResponse mockResponse = mock(Features_Notes_GetResponse.class);
		when(mockBartecRequestService.getFeatureNotes(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatureNotes(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatures_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_GetResponse mockResponse = mock(Features_GetResponse.class);
		when(mockBartecRequestService.getFeatures(COMPANY_ID, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12, PARAM13, PARAM14, PARAM15, PARAM16, PARAM17)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatures(PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12, PARAM13, PARAM14, PARAM15, PARAM16, PARAM17, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeaturesSchedules_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Schedules_GetResponse mockResponse = mock(Features_Schedules_GetResponse.class);
		when(mockBartecRequestService.getFeaturesSchedules(COMPANY_ID, PARAM11, PARAM19)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeaturesSchedules(PARAM11, PARAM19, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatureStatuses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Statuses_GetResponse mockResponse = mock(Features_Statuses_GetResponse.class);
		when(mockBartecRequestService.getFeatureStatuses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatureStatuses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getFeatureTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Features_Types_GetResponse mockResponse = mock(Features_Types_GetResponse.class);
		when(mockBartecRequestService.getFeatureTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getFeatureTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getInspection_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Inspections_GetResponse mockResponse = mock(Inspections_GetResponse.class);
		when(mockBartecRequestService.getInspection(COMPANY_ID, PARAM20, PARAM21, PARAM11, PARAM22, PARAM22, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getInspection(PARAM20, PARAM21, PARAM11, PARAM22, PARAM22, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getInspectionClasses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Inspections_Classes_GetResponse mockResponse = mock(Inspections_Classes_GetResponse.class);
		when(mockBartecRequestService.getInspectionClasses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getInspectionClasses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getInspectionDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Inspections_Documents_GetResponse mockResponse = mock(Inspections_Documents_GetResponse.class);
		when(mockBartecRequestService.getInspectionDocuments(COMPANY_ID, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getInspectionDocuments(PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getInspectionStatuses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Inspections_Statuses_GetResponse mockResponse = mock(Inspections_Statuses_GetResponse.class);
		when(mockBartecRequestService.getInspectionStatuses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getInspectionStatuses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getInspectionTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Inspections_Types_GetResponse mockResponse = mock(Inspections_Types_GetResponse.class);
		when(mockBartecRequestService.getInspectionTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getInspectionTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getJobDetail_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Jobs_Detail_GetResponse mockResponse = mock(Jobs_Detail_GetResponse.class);
		when(mockBartecRequestService.getJobDetail(COMPANY_ID, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getJobDetail(PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getJobDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Jobs_Documents_GetResponse mockResponse = mock(Jobs_Documents_GetResponse.class);
		when(mockBartecRequestService.getJobDocuments(COMPANY_ID, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getJobDocuments(PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getJobFeatureScheduleDates_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Jobs_FeatureScheduleDates_GetResponse mockResponse = mock(Jobs_FeatureScheduleDates_GetResponse.class);
		when(mockBartecRequestService.getJobFeatureScheduleDates(COMPANY_ID, PARAM11, PARAM24, PARAM25)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getJobFeatureScheduleDates(PARAM11, PARAM24, PARAM25, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getJobs_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Jobs_GetResponse mockResponse = mock(Jobs_GetResponse.class);
		when(mockBartecRequestService.getJobs(COMPANY_ID, PARAM11, PARAM11, PARAM5, PARAM27, PARAM5, PARAM5, PARAM5, PARAM25, PARAM26)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getJobs(PARAM11, PARAM11, PARAM5, PARAM27, PARAM5, PARAM5, PARAM5, PARAM25, PARAM26, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getJobsWorkScheduleDates_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Jobs_WorkScheduleDates_GetResponse mockResponse = mock(Jobs_WorkScheduleDates_GetResponse.class);
		when(mockBartecRequestService.getJobsWorkScheduleDates(COMPANY_ID, PARAM11, PARAM27, PARAM25)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getJobsWorkScheduleDates(PARAM11, PARAM27, PARAM25, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getLicences_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Licences_GetResponse mockResponse = mock(Licences_GetResponse.class);
		when(mockBartecRequestService.getLicences(COMPANY_ID, PARAM5, PARAM3, PARAM4, PARAM28, PARAM22, PARAM22, PARAM5, PARAM24, PARAM24, PARAM24)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getLicences(PARAM5, PARAM3, PARAM4, PARAM28, PARAM22, PARAM22, PARAM5, PARAM24, PARAM24, PARAM24, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getLicencesClasses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Licences_Classes_GetResponse mockResponse = mock(Licences_Classes_GetResponse.class);
		when(mockBartecRequestService.getLicencesClasses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getLicencesClasses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getLicencesDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Licences_Documents_GetResponse mockResponse = mock(Licences_Documents_GetResponse.class);
		when(mockBartecRequestService.getLicencesDocuments(COMPANY_ID, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getLicencesDocuments(PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getLicencesStatuses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Licences_Statuses_GetResponse mockResponse = mock(Licences_Statuses_GetResponse.class);
		when(mockBartecRequestService.getLicencesStatuses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getLicencesStatuses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getLicencesTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Licences_Types_GetResponse mockResponse = mock(Licences_Types_GetResponse.class);
		when(mockBartecRequestService.getLicencesTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getLicencesTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremises_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_GetResponse mockResponse = mock(Premises_GetResponse.class);
		when(mockBartecRequestService.getPremises(COMPANY_ID, PARAM11, PARAM1, PARAM2, PARAM3, PARAM4, PARAM1, PARAM6, PARAM7, PARAM8, PARAM9, PARAM11, PARAM25, PARAM1, PARAM18, PARAM11))
				.thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremises(PARAM11, PARAM1, PARAM2, PARAM3, PARAM4, PARAM1, PARAM6, PARAM7, PARAM8, PARAM9, PARAM11, PARAM25, PARAM1, PARAM18, PARAM11,
				mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesAttributeDefinitions_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_AttributeDefinitions_GetResponse mockResponse = mock(Premises_AttributeDefinitions_GetResponse.class);
		when(mockBartecRequestService.getPremisesAttributeDefinitions(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesAttributeDefinitions(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesAttributes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Attributes_GetResponse mockResponse = mock(Premises_Attributes_GetResponse.class);
		when(mockBartecRequestService.getPremisesAttributes(COMPANY_ID, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesAttributes(PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesDetails_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Detail_GetResponse mockResponse = mock(Premises_Detail_GetResponse.class);
		when(mockBartecRequestService.getPremisesDetails(COMPANY_ID, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesDetails(PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Documents_GetResponse mockResponse = mock(Premises_Documents_GetResponse.class);
		when(mockBartecRequestService.getPremisesDocuments(COMPANY_ID, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesDocuments(PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesEvents_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Events_GetResponse mockResponse = mock(Premises_Events_GetResponse.class);
		when(mockBartecRequestService.getPremisesEvents(COMPANY_ID, PARAM11, PARAM27, PARAM4, PARAM4, PARAM4, PARAM25, PARAM25, PARAM4, PARAM7, PARAM11, PARAM11, PARAM8, PARAM9, PARAM11, PARAM1,
				PARAM26)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesEvents(PARAM11, PARAM27, PARAM4, PARAM4, PARAM4, PARAM25, PARAM25, PARAM4, PARAM7, PARAM11, PARAM11, PARAM8, PARAM9, PARAM11, PARAM1,
				PARAM26, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesEventsClasses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Events_Classes_GetResponse mockResponse = mock(Premises_Events_Classes_GetResponse.class);
		when(mockBartecRequestService.getPremisesEventsClasses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesEventsClasses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesEventsDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Events_Documents_GetResponse mockResponse = mock(Premises_Events_Documents_GetResponse.class);
		when(mockBartecRequestService.getPremisesEventsDocuments(COMPANY_ID, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesEventsDocuments(PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesEventsTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Events_Types_GetResponse mockResponse = mock(Premises_Events_Types_GetResponse.class);
		when(mockBartecRequestService.getPremisesEventsTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesEventsTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getPremisesFutureWorkpacks_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_FutureWorkpacks_GetResponse mockResponse = mock(Premises_FutureWorkpacks_GetResponse.class);
		when(mockBartecRequestService.getPremisesFutureWorkpacks(COMPANY_ID, PARAM11, PARAM20, PARAM20, PARAM20, PARAM23, PARAM23)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getPremisesFutureWorkpacks(PARAM11, PARAM20, PARAM20, PARAM20, PARAM23, PARAM23, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getResources_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Resources_GetResponse mockResponse = mock(Resources_GetResponse.class);
		when(mockBartecRequestService.getResources(COMPANY_ID, PARAM5, PARAM1, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getResources(PARAM5, PARAM1, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getResourcesDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Resources_Documents_GetResponse mockResponse = mock(Resources_Documents_GetResponse.class);
		when(mockBartecRequestService.getResourcesDocuments(COMPANY_ID, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getResourcesDocuments(PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getResourcesType_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Resources_Types_GetResponse mockResponse = mock(Resources_Types_GetResponse.class);
		when(mockBartecRequestService.getResourcesType(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getResourcesType(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequests_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_GetResponse mockResponse = mock(ServiceRequests_GetResponse.class);
		when(mockBartecRequestService.getServiceRequests(COMPANY_ID, PARAM1, PARAM1, PARAM10, PARAM28, PARAM2, PARAM21, PARAM25, PARAM25, PARAM24, PARAM12, PARAM12, PARAM12, PARAM12, PARAM3, PARAM24))
				.thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequests(PARAM1, PARAM1, PARAM10, PARAM28, PARAM2, PARAM21, PARAM25, PARAM25, PARAM24, PARAM12, PARAM12, PARAM12, PARAM12, PARAM3, PARAM24,
				mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsAppoitmentsAvailability_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Appointments_Availability_GetResponse mockResponse = mock(ServiceRequests_Appointments_Availability_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsAppoitmentsAvailability(COMPANY_ID, PARAM20, PARAM20, PARAM23)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsAppoitmentsAvailability(PARAM20, PARAM20, PARAM23, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsClasses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Classes_GetResponse mockResponse = mock(ServiceRequests_Classes_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsClasses(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsClasses(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsDetails_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Detail_GetResponse mockResponse = mock(ServiceRequests_Detail_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsDetails(COMPANY_ID, PARAM12, PARAM18)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsDetails(PARAM12, PARAM18, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Documents_GetResponse mockResponse = mock(ServiceRequests_Documents_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsDocuments(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsDocuments(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsHistory_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_History_GetResponse mockResponse = mock(ServiceRequests_History_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsHistory(COMPANY_ID, PARAM20, PARAM20, PARAM23)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsHistory(PARAM20, PARAM20, PARAM23, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsNotes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Notes_GetResponse mockResponse = mock(ServiceRequests_Notes_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsNotes(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsNotes(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsNotesTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Notes_Types_GetResponse mockResponse = mock(ServiceRequests_Notes_Types_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsNotesTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsNotesTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsSLAs_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_SLAs_GetResponse mockResponse = mock(ServiceRequests_SLAs_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsSLAs(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsSLAs(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsStatuses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Statuses_GetResponse mockResponse = mock(ServiceRequests_Statuses_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsStatuses(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsStatuses(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Types_GetResponse mockResponse = mock(ServiceRequests_Types_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsTypes(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsTypes(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getServiceRequestsUpdates_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequests_Updates_GetResponse mockResponse = mock(ServiceRequests_Updates_GetResponse.class);
		when(mockBartecRequestService.getServiceRequestsUpdates(COMPANY_ID, PARAM1, PARAM23)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getServiceRequestsUpdates(PARAM1, PARAM23, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteDocuments_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Sites_Documents_GetResponse mockResponse = mock(Sites_Documents_GetResponse.class);
		when(mockBartecRequestService.getSiteDocuments(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getSiteDocuments(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSiteDocumentsAll_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Sites_Documents_GetAllResponse mockResponse = mock(Sites_Documents_GetAllResponse.class);
		when(mockBartecRequestService.getSiteDocumentsAll(COMPANY_ID, PARAM24, PARAM1, PARAM12)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getSiteDocumentsAll(PARAM24, PARAM1, PARAM12, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSites_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Sites_GetResponse mockResponse = mock(Sites_GetResponse.class);
		when(mockBartecRequestService.getSites(COMPANY_ID, PARAM1, PARAM23, PARAM23, PARAM20, PARAM10, PARAM23, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getSites(PARAM1, PARAM23, PARAM23, PARAM20, PARAM10, PARAM23, PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getStreets_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Streets_GetResponse mockResponse = mock(Streets_GetResponse.class);
		when(mockBartecRequestService.getStreets(COMPANY_ID, PARAM11, PARAM1, PARAM10, PARAM2)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getStreets(PARAM11, PARAM1, PARAM10, PARAM2, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getStreetsAttributes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Streets_Attributes_GetResponse mockResponse = mock(Streets_Attributes_GetResponse.class);
		when(mockBartecRequestService.getStreetsAttributes(COMPANY_ID, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getStreetsAttributes(PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getStreetsDetail_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Streets_Detail_GetResponse mockResponse = mock(Streets_Detail_GetResponse.class);
		when(mockBartecRequestService.getStreetsDetail(COMPANY_ID, PARAM11)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getStreetsDetail(PARAM11, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getStreetsEvents_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Streets_Events_GetResponse mockResponse = mock(Streets_Events_GetResponse.class);
		when(mockBartecRequestService.getStreetsEvents(COMPANY_ID, PARAM11, PARAM1, PARAM10, PARAM23, PARAM25, PARAM2, PARAM23)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getStreetsEvents(PARAM11, PARAM1, PARAM10, PARAM23, PARAM25, PARAM2, PARAM23, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getStreetsEventsType_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Streets_Events_Types_GetResponse mockResponse = mock(Streets_Events_Types_GetResponse.class);
		when(mockBartecRequestService.getStreetsEventsType(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getStreetsEventsType(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSystemExtendedDateDefinitions_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		System_ExtendedDataDefinitions_GetResponse mockResponse = mock(System_ExtendedDataDefinitions_GetResponse.class);
		when(mockBartecRequestService.getSystemExtendedDateDefinitions(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getSystemExtendedDateDefinitions(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSystemLandTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		System_LandTypes_GetResponse mockResponse = mock(System_LandTypes_GetResponse.class);
		when(mockBartecRequestService.getSystemLandTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getSystemLandTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getSystemWasteTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		System_WasteTypes_GetResponse mockResponse = mock(System_WasteTypes_GetResponse.class);
		when(mockBartecRequestService.getSystemWasteTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getSystemWasteTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getVehicleInspectionForms_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Vehicle_InspectionForms_GetResponse mockResponse = mock(Vehicle_InspectionForms_GetResponse.class);
		when(mockBartecRequestService.getVehicleInspectionForms(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getVehicleInspectionForms(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getVehicleInspectionResponses_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Vehicle_InspectionResults_GetResponse mockResponse = mock(Vehicle_InspectionResults_GetResponse.class);
		when(mockBartecRequestService.getVehicleInspectionResult(COMPANY_ID, PARAM24, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getVehicleInspectionResult(PARAM24, PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getVehicles_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Vehicles_GetResponse mockResponse = mock(Vehicles_GetResponse.class);
		when(mockBartecRequestService.getVehicles(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getVehicles(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorkGroups_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		WorkGroups_GetResponse mockResponse = mock(WorkGroups_GetResponse.class);
		when(mockBartecRequestService.getWorkGroups(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getWorkGroups(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorkPacks_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		WorkPacks_GetResponse mockResponse = mock(WorkPacks_GetResponse.class);
		when(mockBartecRequestService.getWorkPacks(COMPANY_ID, PARAM20, PARAM24, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getWorkPacks(PARAM20, PARAM24, PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorkPacksMetrics_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Workpacks_Metrics_GetResponse mockResponse = mock(Workpacks_Metrics_GetResponse.class);
		when(mockBartecRequestService.getWorkPacksMetrics(COMPANY_ID, PARAM12)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getWorkPacksMetrics(PARAM12, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorkPacksNotes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Workpacks_Notes_GetResponse mockResponse = mock(Workpacks_Notes_GetResponse.class);
		when(mockBartecRequestService.getWorkPacksNotes(COMPANY_ID, PARAM20)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getWorkPacksNotes(PARAM20, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void getWorkPacksNotesTypes_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Workpacks_Notes_Types_GetResponse mockResponse = mock(Workpacks_Notes_Types_GetResponse.class);
		when(mockBartecRequestService.getWorkPacksNotesTypes(COMPANY_ID)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.getWorkPacksNotesTypes(mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void reserveServiceRequestAppointmentSlot_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_Appointment_Slot_ReserveResponse mockResponse = mock(ServiceRequest_Appointment_Slot_ReserveResponse.class);
		when(mockBartecRequestService.reserveServiceRequestAppointmentSlot(COMPANY_ID, PARAM20, PARAM20, PARAM5, PARAM5)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.reserveServiceRequestAppointmentSlot(PARAM20, PARAM20, PARAM5, PARAM5, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void setServiceRequestStatus_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_Status_SetResponse mockResponse = mock(ServiceRequest_Status_SetResponse.class);
		when(mockBartecRequestService.setServiceRequestStatus(COMPANY_ID, PARAM5, PARAM10, PARAM20, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.setServiceRequestStatus(PARAM5, PARAM10, PARAM20, PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void updatePremisesAttribute_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		Premises_Attribute_UpdateResponse mockResponse = mock(Premises_Attribute_UpdateResponse.class);
		when(mockBartecRequestService.updatePremisesAttribute(COMPANY_ID, PARAM20, PARAM1, PARAM10, PARAM23, PARAM23)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.updatePremisesAttribute(PARAM20, PARAM1, PARAM10, PARAM23, PARAM23, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

	@Test
	public void updateServiceRequest_WhenNoError_ThenCorrectJsonResponseIsReturned() throws Exception {
		ServiceRequest_UpdateResponse mockResponse = mock(ServiceRequest_UpdateResponse.class);
		when(mockBartecRequestService.updateServiceRequest(COMPANY_ID, PARAM20, PARAM1)).thenReturn(mockResponse);
		when(mockJsonResponseParserService.toJson(mockResponse)).thenReturn(RESPONSE);

		String response = bartecRESTApplication.updateServiceRequest(PARAM20, PARAM1, mockHttpServletRequest);

		assertEquals(RESPONSE, response);
	}

}
