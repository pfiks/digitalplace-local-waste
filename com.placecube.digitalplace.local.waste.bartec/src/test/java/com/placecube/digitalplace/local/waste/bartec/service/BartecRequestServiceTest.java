package com.placecube.digitalplace.local.waste.bartec.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.local.waste.bartec.axis.*;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtAttachedDocument;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtPointMetric;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterBusiness;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact;
import com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class BartecRequestServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 89764l;
	private static final String PARAM1 = "PARAM1";
	private static final String PARAM10 = "PARAM10";
	private static final BigDecimal PARAM11 = BigDecimal.TEN;
	private static final ArrayOfInt PARAM12 = new ArrayOfInt();
	private static final ArrayOfInt PARAM13 = new ArrayOfInt();
	private static final ArrayOfInt PARAM14 = new ArrayOfInt();
	private static final ArrayOfInt PARAM15 = new ArrayOfInt();
	private static final ArrayOfInt PARAM16 = new ArrayOfInt();
	private static final ArrayOfInt PARAM17 = new ArrayOfInt();
	private static final ArrayOfString PARAM18 = null;
	private static final ArrayOfInt PARAM19 = new ArrayOfInt();
	private static final String PARAM2 = "PARAM2";
	private static final int PARAM20 = 0;
	private static final String PARAM21 = "PARAM21";
	private static final ArrayOfInt PARAM22 = new ArrayOfInt();
	private static final Calendar PARAM23 = Calendar.getInstance();
	private static final CtDateQuery PARAM24 = null;
	private static final CtMapBox PARAM25 = null;
	private static final boolean PARAM26 = false;
	private static final CtDateQuery PARAM27 = null;
	private static final ArrayOfDecimal PARAM28 = null;
	private static final CtAttachedDocument PARAM29 = null;
	private static final String PARAM3 = "PARAM3";
	private static final CtPointMetric PARAM30 = null;
	private static final ArrayOfServiceRequest_CreateServiceRequest_CreateFields PARAM31 = null;
	private static final ArrayOfServiceRequest_CreateServiceRequest_CreateLocation PARAM32 = null;
	private static final ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest PARAM33 = null;
	private static final ServiceRequest_CreateServiceRequest_CreateReporterBusiness PARAM34 = null;
	private static final ServiceRequest_CreateServiceRequest_CreateReporterContact PARAM35 = null;
	private static final CtMapPoint_Set PARAM36 = null;
	private static final String PARAM37 = "PARAM37";
	private static final String PARAM4 = "PARAM4";
	private static final int PARAM5 = 2;
	private static final String PARAM6 = "PARAM6";
	private static final String PARAM7 = "PARAM7";
	private static final String PARAM8 = "PARAM8";
	private static final String PARAM9 = "PARAM9";
	private static final String PASSWORD = "PASSWORD";
	private static final String TOKEN = "TOKEN";
	private static final String USERNAME = "USERNAME";
	@Mock
	private BartecAxisService bartecAxisService;

	@InjectMocks
	private BartecRequestService bartecRequestService;

	@Mock
	private BartecWasteService bartecWasteService;

	@Mock
	private CollectiveAPIWebService mockCollectiveAPIWebService;

	@Mock
	private BartecConfiguration mockConfiguration;

	@Mock
	private ObjectFactoryService mockObjectFactoryService;

	@Before
	public void activate() throws Exception {

		when(bartecAxisService.getBartecAxisService(COMPANY_ID)).thenReturn(mockCollectiveAPIWebService);
		when(bartecWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockConfiguration);
		when(mockConfiguration.username()).thenReturn(USERNAME);
		when(mockConfiguration.password()).thenReturn(PASSWORD);
		when(bartecAxisService.authenticate(mockConfiguration.username(), mockConfiguration.password())).thenReturn(TOKEN);

	}

	@Test
	public void cancelServiceRequest_WhenNoError_ThenReturnServiceRequest_Appointment_Slot_ReserveResponse() throws Exception {

		ServiceRequest_Cancel mockInput = mock(ServiceRequest_Cancel.class);
		ServiceRequest_CancelResponse mockResponse = mock(ServiceRequest_CancelResponse.class);

		when(mockObjectFactoryService.cancelServiceRequestInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Cancel(mockInput)).thenReturn(mockResponse);

		ServiceRequest_CancelResponse response = bartecRequestService.cancelServiceRequest(COMPANY_ID, PARAM20, PARAM1);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setServiceCode(PARAM1);
		verify(mockInput, times(1)).setServiceRequestID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void cancelServiceRequestAppointment_WhenNoError_ThenReturnServiceRequest_Appointment_Reservation_CancelResponse() throws Exception {

		ServiceRequest_Appointment_Reservation_Cancel mockInput = mock(ServiceRequest_Appointment_Reservation_Cancel.class);
		ServiceRequest_Appointment_Reservation_CancelResponse mockResponse = mock(ServiceRequest_Appointment_Reservation_CancelResponse.class);

		when(mockObjectFactoryService.cancelServiceRequestAppointmentInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Appointment_Reservation_Cancel(mockInput)).thenReturn(mockResponse);

		ServiceRequest_Appointment_Reservation_CancelResponse response = bartecRequestService.cancelServiceRequestAppointment(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setReservationID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void closeJobs_WhenNoError_ThenReturnJobs_CloseResponse() throws Exception {

		Jobs_Close mockInput = mock(Jobs_Close.class);
		Jobs_CloseResponse mockResponse = mock(Jobs_CloseResponse.class);

		when(mockObjectFactoryService.getJobCloseInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.jobs_Close(mockInput)).thenReturn(mockResponse);

		Jobs_CloseResponse response = bartecRequestService.closeJobs(COMPANY_ID, PARAM5, PARAM26, PARAM26, PARAM26, PARAM1, PARAM3, PARAM4, PARAM23);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setJobID(PARAM5);
		verify(mockInput, times(1)).setComplete(PARAM26);
		verify(mockInput, times(1)).setPartDone(PARAM26);
		verify(mockInput, times(1)).setNoFurtherAction(PARAM26);
		verify(mockInput, times(1)).setReasonText(PARAM1);
		verify(mockInput, times(1)).setClosingComments(PARAM3);
		verify(mockInput, times(1)).setOutcome(PARAM4);
		verify(mockInput, times(1)).setDateClosed(PARAM23);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createBusinessDocument_WhenNoError_ThenReturnBusiness_Document_CreateResponse() throws Exception {

		Business_Document_Create mockInput = mock(Business_Document_Create.class);
		Business_Document_CreateResponse mockResponse = mock(Business_Document_CreateResponse.class);

		when(mockObjectFactoryService.createBusinessDocumentInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.business_Document_Create(mockInput)).thenReturn(mockResponse);

		Business_Document_CreateResponse response = bartecRequestService.createBusinessDocument(COMPANY_ID, PARAM20, PARAM29, PARAM23, PARAM1);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAccountID(PARAM20);
		verify(mockInput, times(1)).setAttachedDocument(PARAM29);
		verify(mockInput, times(1)).setComment(PARAM1);
		verify(mockInput, times(1)).setDateTaken(PARAM23);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createPremisesAttribute_WhenNoError_ThenReturnPremises_Attribute_CreateResponse() throws Exception {

		Premises_Attribute_Create mockInput = mock(Premises_Attribute_Create.class);
		Premises_Attribute_CreateResponse mockResponse = mock(Premises_Attribute_CreateResponse.class);

		when(mockObjectFactoryService.createPremisesAttributeInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Attribute_Create(mockInput)).thenReturn(mockResponse);

		Premises_Attribute_CreateResponse response = bartecRequestService.createPremisesAttribute(COMPANY_ID, PARAM5, PARAM10, PARAM1, PARAM20, PARAM23, PARAM23, PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAttributeID(PARAM5);
		verify(mockInput, times(1)).setAttributeValue(PARAM10);
		verify(mockInput, times(1)).setComments(PARAM1);
		verify(mockInput, times(1)).setConfirmation(PARAM20);
		verify(mockInput, times(1)).setExpiryDate(PARAM23);
		verify(mockInput, times(1)).setReviewDate(PARAM23);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		assertEquals(response, mockResponse);

	}

	@Test
	public void createPremisesDocument_WhenNoError_ThenReturnPremises_Document_CreateResponse() throws Exception {

		Premises_Document_Create mockInput = mock(Premises_Document_Create.class);
		Premises_Document_CreateResponse mockResponse = mock(Premises_Document_CreateResponse.class);

		when(mockObjectFactoryService.createPremisesDocumentInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Document_Create(mockInput)).thenReturn(mockResponse);

		Premises_Document_CreateResponse response = bartecRequestService.createPremisesDocument(COMPANY_ID, PARAM29, PARAM1, PARAM23, PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAttachedDocument(PARAM29);
		verify(mockInput, times(1)).setComment(PARAM1);
		verify(mockInput, times(1)).setDateTaken(PARAM23);
		verify(mockInput, times(1)).setUPRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createPremisesEvent_WhenNoError_ThenReturnPremises_Document_CreateResponse() throws Exception {

		Premises_Event_Create mockInput = mock(Premises_Event_Create.class);
		Premises_Event_CreateResponse mockResponse = mock(Premises_Event_CreateResponse.class);

		when(mockObjectFactoryService.createPremisesEventInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Event_Create(mockInput)).thenReturn(mockResponse);

		Premises_Event_CreateResponse response = bartecRequestService.createPremisesEvent(COMPANY_ID, PARAM11, PARAM1, PARAM20, PARAM20, PARAM23, PARAM30, PARAM20, PARAM18, PARAM12, PARAM20, PARAM18);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setComments(PARAM1);
		verify(mockInput, times(1)).setCrewID(PARAM20);
		verify(mockInput, times(1)).setDeviceID(PARAM20);
		verify(mockInput, times(1)).setEventDate(PARAM23);
		verify(mockInput, times(1)).setEventLocation(PARAM30);
		verify(mockInput, times(1)).setEventTypeID(PARAM20);
		verify(mockInput, times(1)).setFeatureComments(PARAM18);
		verify(mockInput, times(1)).setFeatureIDs(PARAM12);
		verify(mockInput, times(1)).setJobID(PARAM20);
		verify(mockInput, times(1)).setSubEventComments(PARAM18);
		verify(mockInput, times(1)).setUPRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createPremisesEventsDocument_WhenNoError_ThenReturnPremises_Events_Document_CreateResponse() throws Exception {

		Premises_Events_Document_Create mockInput = mock(Premises_Events_Document_Create.class);
		Premises_Events_Document_CreateResponse mockResponse = mock(Premises_Events_Document_CreateResponse.class);

		when(mockObjectFactoryService.createPremisesEventsDocumentInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Events_Document_Create(mockInput)).thenReturn(mockResponse);

		Premises_Events_Document_CreateResponse response = bartecRequestService.createPremisesEventsDocument(COMPANY_ID, PARAM29, PARAM1, PARAM23, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAttachedDocument(PARAM29);
		verify(mockInput, times(1)).setComment(PARAM1);
		verify(mockInput, times(1)).setDateTaken(PARAM23);
		verify(mockInput, times(1)).setEventID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createServiceRequest_WhenNoError_ThenReturnServiceRequest_Appointment_Slot_ReserveResponse() throws Exception {

		ServiceRequest_Create mockInput = mock(ServiceRequest_Create.class);
		ServiceRequest_CreateResponse mockResponse = mock(ServiceRequest_CreateResponse.class);

		when(mockObjectFactoryService.createServiceRequestInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Create(mockInput)).thenReturn(mockResponse);

		ServiceRequest_CreateResponse response = bartecRequestService.createServiceRequest(COMPANY_ID, PARAM20, PARAM31, PARAM20, PARAM23, PARAM32, PARAM20, PARAM33, PARAM34, PARAM10, PARAM35,
				PARAM36, PARAM20, PARAM5, PARAM20, PARAM10, PARAM11, PARAM37);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAppointmentReservationID(PARAM20);
		verify(mockInput, times(1)).setExtendedData(PARAM31);
		verify(mockInput, times(1)).setExternalReference(PARAM37);
		verify(mockInput, times(1)).setCrewID(PARAM20);
		verify(mockInput, times(1)).setDateRequested(PARAM23);
		verify(mockInput, times(1)).setLandTypeID(PARAM20);
		verify(mockInput, times(1)).setRelatedPremises(PARAM32);
		verify(mockInput, times(1)).setRelatedServiceRequests(PARAM33);
		verify(mockInput, times(1)).setReporterBusiness(PARAM34);
		verify(mockInput, times(1)).setReporterContact(PARAM35);
		verify(mockInput, times(1)).setServiceLocationDescription(PARAM10);
		verify(mockInput, times(1)).setServiceRequest_Location(PARAM36);
		verify(mockInput, times(1)).setServiceStatusID(PARAM5);
		verify(mockInput, times(1)).setServiceTypeID(PARAM20);
		verify(mockInput, times(1)).setSLAID(PARAM20);
		verify(mockInput, times(1)).setSource(PARAM10);
		verify(mockInput, times(1)).setUPRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createServiceRequestAppointmentReservation_WhenNoError_ThenReturnServiceRequest_Appointment_Reservation_CreateResponse() throws Exception {

		ServiceRequest_Appointment_Reservation_Create mockInput = mock(ServiceRequest_Appointment_Reservation_Create.class);
		ServiceRequest_Appointment_Reservation_CreateResponse mockResponse = mock(ServiceRequest_Appointment_Reservation_CreateResponse.class);

		when(mockObjectFactoryService.createServiceRequestAppointmentReservationInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Appointment_Reservation_Create(mockInput)).thenReturn(mockResponse);

		ServiceRequest_Appointment_Reservation_CreateResponse response = bartecRequestService.createServiceRequestAppointmentReservation(COMPANY_ID, PARAM20, PARAM20, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setServiceTypeID(PARAM20);
		verify(mockInput, times(1)).setSlotID(PARAM20);
		verify(mockInput, times(1)).setWorkPackID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createServiceRequestDocument_WhenNoError_ThenReturnPremises_Events_Document_CreateResponse() throws Exception {

		ServiceRequest_Document_Create mockInput = mock(ServiceRequest_Document_Create.class);
		ServiceRequest_Document_CreateResponse mockResponse = mock(ServiceRequest_Document_CreateResponse.class);

		when(mockObjectFactoryService.createServiceRequestDocumentInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Document_Create(mockInput)).thenReturn(mockResponse);

		ServiceRequest_Document_CreateResponse response = bartecRequestService.createServiceRequestDocument(COMPANY_ID, PARAM29, PARAM1, PARAM23, PARAM26, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAttachedDocument(PARAM29);
		verify(mockInput, times(1)).setComment(PARAM1);
		verify(mockInput, times(1)).setDateTaken(PARAM23);
		verify(mockInput, times(1)).setPublic(PARAM26);
		verify(mockInput, times(1)).setServiceRequestID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createServiceRequestNote_WhenNoError_ThenReturnServiceRequest_Note_CreateResponse() throws Exception {

		ServiceRequest_Note_Create mockInput = mock(ServiceRequest_Note_Create.class);
		ServiceRequest_Note_CreateResponse mockResponse = mock(ServiceRequest_Note_CreateResponse.class);

		when(mockObjectFactoryService.createServiceRequestNoteInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Note_Create(mockInput)).thenReturn(mockResponse);

		ServiceRequest_Note_CreateResponse response = bartecRequestService.createServiceRequestNote(COMPANY_ID, PARAM20, PARAM2, PARAM10, PARAM1, PARAM5, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setComment(PARAM1);
		verify(mockInput, times(1)).setNote(PARAM10);
		verify(mockInput, times(1)).setNoteTypeID(PARAM5);
		verify(mockInput, times(1)).setSequenceNumber(PARAM20);
		verify(mockInput, times(1)).setServiceCode(PARAM2);
		verify(mockInput, times(1)).setServiceRequestID(PARAM20);
		assertEquals(response, mockResponse);

	}

	@Test
	public void createSiteDocument_WhenNoError_ThenReturnSSite_Document_CreateResponse() throws Exception {

		Site_Document_Create mockInput = mock(Site_Document_Create.class);
		Site_Document_CreateResponse mockResponse = mock(Site_Document_CreateResponse.class);

		when(mockObjectFactoryService.createSiteDocumentInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.site_Document_Create(mockInput)).thenReturn(mockResponse);

		Site_Document_CreateResponse response = bartecRequestService.createSiteDocument(COMPANY_ID, PARAM29, PARAM1, PARAM23, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAttachedDocument(PARAM29);
		verify(mockInput, times(1)).setComment(PARAM1);
		verify(mockInput, times(1)).setDateTaken(PARAM23);
		verify(mockInput, times(1)).setSiteID(PARAM20);
		assertEquals(response, mockResponse);

	}

	@Test
	public void createVehicleMessage_WhenNoError_ThenReturnVehicle_Message_CreateResponse() throws Exception {

		Vehicle_Message_Create mockInput = mock(Vehicle_Message_Create.class);
		Vehicle_Message_CreateResponse mockResponse = mock(Vehicle_Message_CreateResponse.class);

		when(mockObjectFactoryService.createVehicleMessageInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.vehicle_Message_Create(mockInput)).thenReturn(mockResponse);

		Vehicle_Message_CreateResponse response = bartecRequestService.createVehicleMessage(COMPANY_ID, PARAM20, PARAM10, PARAM1);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDeviceID(PARAM20);
		verify(mockInput, times(1)).setMessageText(PARAM10);
		verify(mockInput, times(1)).setSubject(PARAM1);

		assertEquals(response, mockResponse);

	}

	@Test
	public void createWorkPackNote_WhenNoError_ThenReturnServiceRequest_UpdateResponse() throws Exception {

		Workpack_Note_Create mockInput = mock(Workpack_Note_Create.class);
		Workpack_Note_CreateResponse mockResponse = mock(Workpack_Note_CreateResponse.class);

		when(mockObjectFactoryService.createWorkPackNoteInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.workpack_Note_Create(mockInput)).thenReturn(mockResponse);

		Workpack_Note_CreateResponse response = bartecRequestService.createWorkPackNote(COMPANY_ID, PARAM2, PARAM10, PARAM1, PARAM5, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setComment(PARAM2);
		verify(mockInput, times(1)).setNote(PARAM10);
		verify(mockInput, times(1)).setSubject(PARAM1);
		verify(mockInput, times(1)).setNoteTypeID(PARAM5);
		verify(mockInput, times(1)).setWorkpackID(PARAM20);
		assertEquals(response, mockResponse);

	}

	@Test
	public void detetePremisesAttributes_WhenNoError_ThenReturnPremises_Attributes_DeleteResponse() throws Exception {

		Premises_Attributes_Delete mockInput = mock(Premises_Attributes_Delete.class);
		Premises_Attributes_DeleteResponse mockResponse = mock(Premises_Attributes_DeleteResponse.class);

		when(mockObjectFactoryService.detetePremisesAttributesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Attributes_Delete(mockInput)).thenReturn(mockResponse);

		Premises_Attributes_DeleteResponse response = bartecRequestService.detetePremisesAttributes(COMPANY_ID, PARAM20, PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAttributeID(PARAM20);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		assertEquals(response, mockResponse);

	}

	@Test
	public void extendServiceRequestAppointmentReservation_WhenNoError_ThenReturnServiceRequest_Appointment_Reservation_ExtendResponse() throws Exception {

		ServiceRequest_Appointment_Reservation_Extend mockInput = mock(ServiceRequest_Appointment_Reservation_Extend.class);
		ServiceRequest_Appointment_Reservation_ExtendResponse mockResponse = mock(ServiceRequest_Appointment_Reservation_ExtendResponse.class);

		when(mockObjectFactoryService.extendServiceRequestAppointmentReservationInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Appointment_Reservation_Extend(mockInput)).thenReturn(mockResponse);

		ServiceRequest_Appointment_Reservation_ExtendResponse response = bartecRequestService.extendServiceRequestAppointmentReservation(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setReservationID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getBusiness_WhenNoError_ThenReturnBusinesses_GetResponse() throws Exception {

		Businesses_Get mockInput = mock(Businesses_Get.class);
		Businesses_GetResponse mockResponse = mock(Businesses_GetResponse.class);

		when(mockObjectFactoryService.getBusinessInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.businesses_Get(mockInput)).thenReturn(mockResponse);

		Businesses_GetResponse response = bartecRequestService.getBusiness(COMPANY_ID, PARAM1, PARAM2, PARAM3, PARAM4, PARAM22, PARAM22, PARAM22, PARAM22, PARAM23);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAccountName(PARAM2);
		verify(mockInput, times(1)).setAccountNumber(PARAM1);
		verify(mockInput, times(1)).setBusinessName(PARAM3);
		verify(mockInput, times(1)).setBusinessDescription(PARAM4);
		verify(mockInput, times(1)).setStatuses(PARAM22);
		verify(mockInput, times(1)).setSubStatuses(PARAM22);
		verify(mockInput, times(1)).setBusinessTypes(PARAM22);
		verify(mockInput, times(1)).setBusinessClasses(PARAM22);
		verify(mockInput, times(1)).setDateCreated(PARAM23);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getBusinessClasses_WhenNoError_ThenReturnBusinesses_Classes_GetResponse() throws Exception {

		Businesses_Classes_Get mockInput = mock(Businesses_Classes_Get.class);
		Businesses_Classes_GetResponse mockResponse = mock(Businesses_Classes_GetResponse.class);

		when(mockObjectFactoryService.getBusinessClassesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.businesses_Classes_Get(mockInput)).thenReturn(mockResponse);

		Businesses_Classes_GetResponse response = bartecRequestService.getBusinessClasses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getBusinessDocuments_WhenNoError_ThenReturnBusinesses_Documents_GetResponse() throws Exception {

		Businesses_Documents_Get mockInput = mock(Businesses_Documents_Get.class);
		Businesses_Documents_GetResponse mockResponse = mock(Businesses_Documents_GetResponse.class);

		when(mockObjectFactoryService.getBusinessDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.businesses_Documents_Get(mockInput)).thenReturn(mockResponse);

		Businesses_Documents_GetResponse response = bartecRequestService.getBusinessDocuments(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getBusinessStatuses_WhenNoError_ThenReturnBusinesses_Statuses_GetResponse() throws Exception {

		Businesses_Statuses_Get mockInput = mock(Businesses_Statuses_Get.class);
		Businesses_Statuses_GetResponse mockResponse = mock(Businesses_Statuses_GetResponse.class);

		when(mockObjectFactoryService.getBusinessStatusesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.businesses_Statuses_Get(mockInput)).thenReturn(mockResponse);

		Businesses_Statuses_GetResponse response = bartecRequestService.getBusinessStatuses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getBusinessStatuses_WhenNoError_ThenReturnBusinesses_SubStatuses_GetResponse() throws Exception {

		Businesses_SubStatuses_Get mockInput = mock(Businesses_SubStatuses_Get.class);
		Businesses_SubStatuses_GetResponse mockResponse = mock(Businesses_SubStatuses_GetResponse.class);

		when(mockObjectFactoryService.getBusinessSubStatusesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.businesses_SubStatuses_Get(mockInput)).thenReturn(mockResponse);

		Businesses_SubStatuses_GetResponse response = bartecRequestService.getBusinessSubStatuses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getBusinessTypes_WhenNoError_ThenReturnBusinesses_Types_GetResponse() throws Exception {

		Businesses_Types_Get mockInput = mock(Businesses_Types_Get.class);
		Businesses_Types_GetResponse mockResponse = mock(Businesses_Types_GetResponse.class);

		when(mockObjectFactoryService.getBusinessesTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.businesses_Types_Get(mockInput)).thenReturn(mockResponse);

		Businesses_Types_GetResponse response = bartecRequestService.getBusinessTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getCaseDocuments_WhenNoError_ThenReturnCases_Documents_GetResponse() throws Exception {

		Cases_Documents_Get mockInput = mock(Cases_Documents_Get.class);
		Cases_Documents_GetResponse mockResponse = mock(Cases_Documents_GetResponse.class);

		when(mockObjectFactoryService.getCaseDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.cases_Documents_Get(mockInput)).thenReturn(mockResponse);

		Cases_Documents_GetResponse response = bartecRequestService.getCaseDocuments(COMPANY_ID, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getCrews_WhenNoError_ThenReturnCrews_GetResponse() throws Exception {

		Crews_Get mockInput = mock(Crews_Get.class);
		Crews_GetResponse mockResponse = mock(Crews_GetResponse.class);

		when(mockObjectFactoryService.getCrewsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.crews_Get(mockInput)).thenReturn(mockResponse);

		Crews_GetResponse response = bartecRequestService.getCrews(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeatureCategories_WhenNoError_ThenReturnCrews_GetResponse() throws Exception {

		Features_Categories_Get mockInput = mock(Features_Categories_Get.class);
		Features_Categories_GetResponse mockResponse = mock(Features_Categories_GetResponse.class);

		when(mockObjectFactoryService.getFeaturesCategoryInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Categories_Get(mockInput)).thenReturn(mockResponse);

		Features_Categories_GetResponse response = bartecRequestService.getFeatureCategories(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeatureClasses_WhenNoError_ThenReturnFeatures_Classes_GetResponse() throws Exception {

		Features_Classes_Get mockInput = mock(Features_Classes_Get.class);
		Features_Classes_GetResponse mockResponse = mock(Features_Classes_GetResponse.class);

		when(mockObjectFactoryService.getFeaturesClassesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Classes_Get(mockInput)).thenReturn(mockResponse);

		Features_Classes_GetResponse response = bartecRequestService.getFeatureClasses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeatureColours_WhenNoError_ThenReturnFeatures_Classes_GetResponse() throws Exception {

		Features_Colours_Get mockInput = mock(Features_Colours_Get.class);
		Features_Colours_GetResponse mockResponse = mock(Features_Colours_GetResponse.class);

		when(mockObjectFactoryService.getFeatureColoursInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Colours_Get(mockInput)).thenReturn(mockResponse);

		Features_Colours_GetResponse response = bartecRequestService.getFeatureColours(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeatureConditions_WhenNoError_ThenReturnFeatures_Conditions_GetResponse() throws Exception {

		Features_Conditions_Get mockInput = mock(Features_Conditions_Get.class);
		Features_Conditions_GetResponse mockResponse = mock(Features_Conditions_GetResponse.class);

		when(mockObjectFactoryService.getFeatureConditionsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Conditions_Get(mockInput)).thenReturn(mockResponse);

		Features_Conditions_GetResponse response = bartecRequestService.getFeatureConditions(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeatureManufacturers_WhenNoError_ThenReturnFeatures_Manufacturers_GetResponse() throws Exception {

		Features_Manufacturers_Get mockInput = mock(Features_Manufacturers_Get.class);
		Features_Manufacturers_GetResponse mockResponse = mock(Features_Manufacturers_GetResponse.class);

		when(mockObjectFactoryService.getFeatureManufacturersInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Manufacturers_Get(mockInput)).thenReturn(mockResponse);

		Features_Manufacturers_GetResponse response = bartecRequestService.getFeatureManufacturers(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeatures_WhenNoError_ThenReturnFeatures_GetResponse() throws Exception {

		Features_Get mockInput = mock(Features_Get.class);
		Features_GetResponse mockResponse = mock(Features_GetResponse.class);

		when(mockObjectFactoryService.getFeatuersInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Get(mockInput)).thenReturn(mockResponse);

		Features_GetResponse response = bartecRequestService.getFeatures(COMPANY_ID, PARAM6, PARAM7, PARAM8, PARAM9, PARAM10, PARAM11, PARAM12, PARAM13, PARAM14, PARAM15, PARAM16, PARAM17);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setName(PARAM6);
		verify(mockInput, times(1)).setDescription(PARAM7);
		verify(mockInput, times(1)).setSerialNumber(PARAM8);
		verify(mockInput, times(1)).setAssetTag(PARAM9);
		verify(mockInput, times(1)).setRFIDTag(PARAM10);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setTypes(PARAM12);
		verify(mockInput, times(1)).setStatuses(PARAM13);
		verify(mockInput, times(1)).setManufacturers(PARAM14);
		verify(mockInput, times(1)).setColours(PARAM15);
		verify(mockInput, times(1)).setConditions(PARAM16);
		verify(mockInput, times(1)).setWasteTypes(PARAM17);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeaturesSchedules_WhenNoError_ThenReturnFeatures_Schedules_GetResponse() throws Exception {

		Features_Schedules_Get mockInput = mock(Features_Schedules_Get.class);
		Features_Schedules_GetResponse mockResponse = mock(Features_Schedules_GetResponse.class);

		when(mockObjectFactoryService.getFeaturesSchedulesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Schedules_Get(mockInput)).thenReturn(mockResponse);

		Features_Schedules_GetResponse response = bartecRequestService.getFeaturesSchedules(COMPANY_ID, PARAM11, PARAM19);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setTypes(PARAM19);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeatureStatuses_WhenNoError_ThenReturnFeatures_Statuses_GetResponse() throws Exception {

		Features_Statuses_Get mockInput = mock(Features_Statuses_Get.class);
		Features_Statuses_GetResponse mockResponse = mock(Features_Statuses_GetResponse.class);

		when(mockObjectFactoryService.getFeatureStatusesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Statuses_Get(mockInput)).thenReturn(mockResponse);

		Features_Statuses_GetResponse response = bartecRequestService.getFeatureStatuses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getFeatureTypes_WhenNoError_ThenReturnFeatures_Types_GetResponse() throws Exception {

		Features_Types_Get mockInput = mock(Features_Types_Get.class);
		Features_Types_GetResponse mockResponse = mock(Features_Types_GetResponse.class);

		when(mockObjectFactoryService.getFeatureTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Types_Get(mockInput)).thenReturn(mockResponse);

		Features_Types_GetResponse response = bartecRequestService.getFeatureTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getInspection_WhenNoError_ThenReturnInspections_GetResponse() throws Exception {

		Inspections_Get mockInput = mock(Inspections_Get.class);
		Inspections_GetResponse mockResponse = mock(Inspections_GetResponse.class);

		when(mockObjectFactoryService.getInspectionsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.inspections_Get(mockInput)).thenReturn(mockResponse);

		Inspections_GetResponse response = bartecRequestService.getInspection(COMPANY_ID, PARAM20, PARAM21, PARAM11, PARAM22, PARAM22, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setSiteID(PARAM20);
		verify(mockInput, times(1)).setSiteName(PARAM21);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setInspectionTypes(PARAM22);
		verify(mockInput, times(1)).setInspectionSatuses(PARAM22);
		verify(mockInput, times(1)).setFeatureID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getInspectionClasses_WhenNoError_ThenReturnInspections_Classes_GetResponse() throws Exception {

		Inspections_Classes_Get mockInput = mock(Inspections_Classes_Get.class);
		Inspections_Classes_GetResponse mockResponse = mock(Inspections_Classes_GetResponse.class);

		when(mockObjectFactoryService.getInspectionClassesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.inspections_Classes_Get(mockInput)).thenReturn(mockResponse);

		Inspections_Classes_GetResponse response = bartecRequestService.getInspectionClasses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getInspectionDocuments_WhenNoError_ThenReturnInspections_Documents_GetResponse() throws Exception {

		Inspections_Documents_Get mockInput = mock(Inspections_Documents_Get.class);
		Inspections_Documents_GetResponse mockResponse = mock(Inspections_Documents_GetResponse.class);

		when(mockObjectFactoryService.getInspectionDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.inspections_Documents_Get(mockInput)).thenReturn(mockResponse);

		Inspections_Documents_GetResponse response = bartecRequestService.getInspectionDocuments(COMPANY_ID, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getInspectionStatuses_WhenNoError_ThenReturnInspections_Statuses_GetResponse() throws Exception {

		Inspections_Statuses_Get mockInput = mock(Inspections_Statuses_Get.class);
		Inspections_Statuses_GetResponse mockResponse = mock(Inspections_Statuses_GetResponse.class);

		when(mockObjectFactoryService.getInspectionsStatusesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.inspections_Statuses_Get(mockInput)).thenReturn(mockResponse);

		Inspections_Statuses_GetResponse response = bartecRequestService.getInspectionStatuses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getInspectionTypes_WhenNoError_ThenReturnInspections_Types_GetResponse() throws Exception {

		Inspections_Types_Get mockInput = mock(Inspections_Types_Get.class);
		Inspections_Types_GetResponse mockResponse = mock(Inspections_Types_GetResponse.class);

		when(mockObjectFactoryService.getInspectionsTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.inspections_Types_Get(mockInput)).thenReturn(mockResponse);

		Inspections_Types_GetResponse response = bartecRequestService.getInspectionTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getJobDetail_WhenNoError_ThenReturnJobs_Detail_GetResponse() throws Exception {

		Jobs_Detail_Get mockInput = mock(Jobs_Detail_Get.class);
		Jobs_Detail_GetResponse mockResponse = mock(Jobs_Detail_GetResponse.class);

		when(mockObjectFactoryService.getJobDetailInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.jobs_Detail_Get(mockInput)).thenReturn(mockResponse);

		Jobs_Detail_GetResponse response = bartecRequestService.getJobDetail(COMPANY_ID, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setJobID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getJobDetail_WhenNoError_ThenReturnJobs_Documents_GetResponse() throws Exception {

		Jobs_Documents_Get mockInput = mock(Jobs_Documents_Get.class);
		Jobs_Documents_GetResponse mockResponse = mock(Jobs_Documents_GetResponse.class);

		when(mockObjectFactoryService.getJobDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.jobs_Documents_Get(mockInput)).thenReturn(mockResponse);

		Jobs_Documents_GetResponse response = bartecRequestService.getJobDocuments(COMPANY_ID, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getJobFeatureScheduleDates_WhenNoError_ThenReturnJobs_FeatureScheduleDates_GetResponse() throws Exception {

		Jobs_FeatureScheduleDates_Get mockInput = mock(Jobs_FeatureScheduleDates_Get.class);
		Jobs_FeatureScheduleDates_GetResponse mockResponse = mock(Jobs_FeatureScheduleDates_GetResponse.class);

		when(mockObjectFactoryService.getJobFeatureScheduleDatesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.jobs_FeatureScheduleDates_Get(mockInput)).thenReturn(mockResponse);

		Jobs_FeatureScheduleDates_GetResponse response = bartecRequestService.getJobFeatureScheduleDates(COMPANY_ID, PARAM11, PARAM24, PARAM25);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setJobs_Bounds(PARAM25);
		verify(mockInput, times(1)).setDateRange(PARAM24);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getJobs_WhenNoError_ThenReturnJobs_GetResponse() throws Exception {

		Jobs_Get mockInput = mock(Jobs_Get.class);
		Jobs_GetResponse mockResponse = mock(Jobs_GetResponse.class);

		when(mockObjectFactoryService.getJobsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.jobs_Get(mockInput)).thenReturn(mockResponse);

		Jobs_GetResponse response = bartecRequestService.getJobs(COMPANY_ID, PARAM11, PARAM11, PARAM5, PARAM27, PARAM5, PARAM5, PARAM5, PARAM25, PARAM26);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setUSRN(PARAM11);
		verify(mockInput, times(1)).setWorkPackID(PARAM5);
		verify(mockInput, times(1)).setParishID(PARAM5);
		verify(mockInput, times(1)).setScheduleStart(PARAM27);
		verify(mockInput, times(1)).setServiceRequestID(PARAM5);
		verify(mockInput, times(1)).setWardID(PARAM5);
		verify(mockInput, times(1)).setJobs_Bounds(PARAM25);
		verify(mockInput, times(1)).setIncludeRelated(PARAM26);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getJobsWorkScheduleDates_WhenNoError_ThenReturnJobs_WorkScheduleDates_GetResponse() throws Exception {

		Jobs_WorkScheduleDates_Get mockInput = mock(Jobs_WorkScheduleDates_Get.class);
		Jobs_WorkScheduleDates_GetResponse mockResponse = mock(Jobs_WorkScheduleDates_GetResponse.class);

		when(mockObjectFactoryService.getJobsWorkScheduleDatesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.jobs_WorkScheduleDates_Get(mockInput)).thenReturn(mockResponse);

		Jobs_WorkScheduleDates_GetResponse response = bartecRequestService.getJobsWorkScheduleDates(COMPANY_ID, PARAM11, PARAM27, PARAM25);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setDateRange(PARAM27);
		verify(mockInput, times(1)).setJobs_Bounds(PARAM25);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLicences_WhenNoError_ThenReturnLicences_GetResponse() throws Exception {

		Licences_Get mockInput = mock(Licences_Get.class);
		Licences_GetResponse mockResponse = mock(Licences_GetResponse.class);

		when(mockObjectFactoryService.getLicencesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.licences_Get(mockInput)).thenReturn(mockResponse);

		Licences_GetResponse response = bartecRequestService.getLicences(COMPANY_ID, PARAM5, PARAM3, PARAM4, PARAM28, PARAM22, PARAM22, PARAM5, PARAM24, PARAM24, PARAM24);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setSiteID(PARAM5);
		verify(mockInput, times(1)).setSiteName(PARAM3);
		verify(mockInput, times(1)).setAccountNumber(PARAM4);
		verify(mockInput, times(1)).setUPRNs(PARAM28);
		verify(mockInput, times(1)).setLicenceTypes(PARAM22);
		verify(mockInput, times(1)).setLicenceStatuses(PARAM22);
		verify(mockInput, times(1)).setServiceRequestID(PARAM5);
		verify(mockInput, times(1)).setStartDate(PARAM24);
		verify(mockInput, times(1)).setIssueDate(PARAM24);
		verify(mockInput, times(1)).setDateCreated(PARAM24);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLicencesClasses_WhenNoError_ThenReturnLicences_Classes_GetResponse() throws Exception {

		Licences_Classes_Get mockInput = mock(Licences_Classes_Get.class);
		Licences_Classes_GetResponse mockResponse = mock(Licences_Classes_GetResponse.class);

		when(mockObjectFactoryService.getLicencesClassesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.licences_Classes_Get(mockInput)).thenReturn(mockResponse);

		Licences_Classes_GetResponse response = bartecRequestService.getLicencesClasses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLicencesDocuments_WhenNoError_ThenReturnLicences_Documents_GetResponse() throws Exception {

		Licences_Documents_Get mockInput = mock(Licences_Documents_Get.class);
		Licences_Documents_GetResponse mockResponse = mock(Licences_Documents_GetResponse.class);

		when(mockObjectFactoryService.getLicencesDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.licences_Documents_Get(mockInput)).thenReturn(mockResponse);

		Licences_Documents_GetResponse response = bartecRequestService.getLicencesDocuments(COMPANY_ID, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLicencesStatuses_WhenNoError_ThenReturnLicences_Statuses_GetResponse() throws Exception {

		Licences_Statuses_Get mockInput = mock(Licences_Statuses_Get.class);
		Licences_Statuses_GetResponse mockResponse = mock(Licences_Statuses_GetResponse.class);

		when(mockObjectFactoryService.getLicencesStatusesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.licences_Statuses_Get(mockInput)).thenReturn(mockResponse);

		Licences_Statuses_GetResponse response = bartecRequestService.getLicencesStatuses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getLicencesTypes_WhenNoError_ThenReturnLicences_Types_GetResponse() throws Exception {

		Licences_Types_Get mockInput = mock(Licences_Types_Get.class);
		Licences_Types_GetResponse mockResponse = mock(Licences_Types_GetResponse.class);

		when(mockObjectFactoryService.getLicencesTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.licences_Types_Get(mockInput)).thenReturn(mockResponse);

		Licences_Types_GetResponse response = bartecRequestService.getLicencesTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremises_WhenNoError_ThenReturnPremises_FutureWorkpacks_GetResponse() throws Exception {

		Premises_Get mockInput = mock(Premises_Get.class);
		Premises_GetResponse mockResponse = mock(Premises_GetResponse.class);

		when(mockObjectFactoryService.getPremisesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Get(mockInput)).thenReturn(mockResponse);

		Premises_GetResponse response = bartecRequestService.getPremises(COMPANY_ID, PARAM11, PARAM1, PARAM2, PARAM3, PARAM4, PARAM1, PARAM6, PARAM7, PARAM8, PARAM9, PARAM11, PARAM25, PARAM1, PARAM18,
				PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setUSRN(PARAM11);
		verify(mockInput, times(1)).setAddress1(PARAM1);
		verify(mockInput, times(1)).setAddress2(PARAM2);
		verify(mockInput, times(1)).setWardName(PARAM7);
		verify(mockInput, times(1)).setParishName(PARAM8);
		verify(mockInput, times(1)).setStreet(PARAM3);
		verify(mockInput, times(1)).setLocality(PARAM4);
		verify(mockInput, times(1)).setTown(PARAM1);
		verify(mockInput, times(1)).setPostcode(PARAM6);
		verify(mockInput, times(1)).setUserLabel(PARAM9);
		verify(mockInput, times(1)).setParentUPRN(PARAM11);
		verify(mockInput, times(1)).setBounds(PARAM25);
		verify(mockInput, times(1)).setBLPUClass(PARAM1);
		verify(mockInput, times(1)).setBLPULStat(PARAM18);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesAttributeDefinitions_WhenNoError_ThenReturnPremises_AttributeDefinitions_GetResponse() throws Exception {

		Premises_AttributeDefinitions_Get mockInput = mock(Premises_AttributeDefinitions_Get.class);
		Premises_AttributeDefinitions_GetResponse mockResponse = mock(Premises_AttributeDefinitions_GetResponse.class);

		when(mockObjectFactoryService.getPremisesAttributeDefinitions()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_AttributeDefinitions_Get(mockInput)).thenReturn(mockResponse);

		Premises_AttributeDefinitions_GetResponse response = bartecRequestService.getPremisesAttributeDefinitions(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesAttributes_WhenNoError_ThenReturnPremises_Attributes_GetResponse() throws Exception {

		Premises_Attributes_Get mockInput = mock(Premises_Attributes_Get.class);
		Premises_Attributes_GetResponse mockResponse = mock(Premises_Attributes_GetResponse.class);

		when(mockObjectFactoryService.getPremisesAttributesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Attributes_Get(mockInput)).thenReturn(mockResponse);

		Premises_Attributes_GetResponse response = bartecRequestService.getPremisesAttributes(COMPANY_ID, PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesDetails_WhenNoError_ThenReturnPremises_Detail_GetResponse() throws Exception {

		Premises_Detail_Get mockInput = mock(Premises_Detail_Get.class);
		Premises_Detail_GetResponse mockResponse = mock(Premises_Detail_GetResponse.class);

		when(mockObjectFactoryService.getPremisesDetailsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Detail_Get(mockInput)).thenReturn(mockResponse);

		Premises_Detail_GetResponse response = bartecRequestService.getPremisesDetails(COMPANY_ID, PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesDocuments_WhenNoError_ThenReturnPremises_Documents_GetResponse() throws Exception {

		Premises_Documents_Get mockInput = mock(Premises_Documents_Get.class);
		Premises_Documents_GetResponse mockResponse = mock(Premises_Documents_GetResponse.class);

		when(mockObjectFactoryService.getPremisesDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Documents_Get(mockInput)).thenReturn(mockResponse);

		Premises_Documents_GetResponse response = bartecRequestService.getPremisesDocuments(COMPANY_ID, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesEvents_WhenNoError_ThenReturnPremises_Events_Classes_GetResponse() throws Exception {

		Premises_Events_Classes_Get mockInput = mock(Premises_Events_Classes_Get.class);
		Premises_Events_Classes_GetResponse mockResponse = mock(Premises_Events_Classes_GetResponse.class);

		when(mockObjectFactoryService.getPremisesEventsClassesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Events_Classes_Get(mockInput)).thenReturn(mockResponse);

		Premises_Events_Classes_GetResponse response = bartecRequestService.getPremisesEventsClasses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesEvents_WhenNoError_ThenReturnPremises_Events_GetResponse() throws Exception {

		Premises_Events_Get mockInput = mock(Premises_Events_Get.class);
		Premises_Events_GetResponse mockResponse = mock(Premises_Events_GetResponse.class);

		when(mockObjectFactoryService.getPremisesEventsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Events_Get(mockInput)).thenReturn(mockResponse);

		Premises_Events_GetResponse response = bartecRequestService.getPremisesEvents(COMPANY_ID, PARAM11, PARAM27, PARAM4, PARAM4, PARAM4, PARAM25, PARAM25, PARAM4, PARAM7, PARAM11, PARAM11, PARAM8,
				PARAM9, PARAM11, PARAM1, PARAM26);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setUSRN(PARAM11);
		verify(mockInput, times(1)).setDateRange(PARAM27);
		verify(mockInput, times(1)).setPostcode(PARAM4);
		verify(mockInput, times(1)).setWardName(PARAM4);
		verify(mockInput, times(1)).setParishName(PARAM4);
		verify(mockInput, times(1)).setPremises_Bounds(PARAM25);
		verify(mockInput, times(1)).setEvents_Bounds(PARAM25);
		verify(mockInput, times(1)).setEventType(PARAM4);
		verify(mockInput, times(1)).setEventSubType(PARAM7);
		verify(mockInput, times(1)).setMinDistance(PARAM11);
		verify(mockInput, times(1)).setMaxDistance(PARAM11);
		verify(mockInput, times(1)).setDeviceType(PARAM8);
		verify(mockInput, times(1)).setDeviceName(PARAM9);
		verify(mockInput, times(1)).setWorkPack(PARAM1);
		verify(mockInput, times(1)).setIncludeRelated(PARAM26);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesEventsDocuments_WhenNoError_ThenReturnPremises_Events_Documents_GetResponse() throws Exception {

		Premises_Events_Documents_Get mockInput = mock(Premises_Events_Documents_Get.class);
		Premises_Events_Documents_GetResponse mockResponse = mock(Premises_Events_Documents_GetResponse.class);

		when(mockObjectFactoryService.getPremisesEventsDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Events_Documents_Get(mockInput)).thenReturn(mockResponse);

		Premises_Events_Documents_GetResponse response = bartecRequestService.getPremisesEventsDocuments(COMPANY_ID, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesEventsTypes_WhenNoError_ThenReturnPremises_Events_Types_GetResponse() throws Exception {

		Premises_Events_Types_Get mockInput = mock(Premises_Events_Types_Get.class);
		Premises_Events_Types_GetResponse mockResponse = mock(Premises_Events_Types_GetResponse.class);

		when(mockObjectFactoryService.getPremisesEventsTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Events_Types_Get(mockInput)).thenReturn(mockResponse);

		Premises_Events_Types_GetResponse response = bartecRequestService.getPremisesEventsTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getPremisesFutureWorkpacks_WhenNoError_ThenReturnPremises_FutureWorkpacks_GetResponse() throws Exception {

		Premises_FutureWorkpacks_Get mockInput = mock(Premises_FutureWorkpacks_Get.class);
		Premises_FutureWorkpacks_GetResponse mockResponse = mock(Premises_FutureWorkpacks_GetResponse.class);

		when(mockObjectFactoryService.getPremisesFutureWorkpacksInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_FutureWorkpacks_Get(mockInput)).thenReturn(mockResponse);

		Premises_FutureWorkpacks_GetResponse response = bartecRequestService.getPremisesFutureWorkpacks(COMPANY_ID, PARAM11, PARAM20, PARAM20, PARAM20, PARAM23, PARAM23);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUPRN(PARAM11);
		verify(mockInput, times(1)).setFeatureTypeID(PARAM20);
		verify(mockInput, times(1)).setActionTypeID(PARAM20);
		verify(mockInput, times(1)).setFeatureStatusID(PARAM20);
		verify(mockInput, times(1)).setDateFrom(PARAM23);
		verify(mockInput, times(1)).setDateTo(PARAM23);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getResources_WhenNoError_ThenReturnResources_GetResponse() throws Exception {

		Resources_Get mockInput = mock(Resources_Get.class);
		Resources_GetResponse mockResponse = mock(Resources_GetResponse.class);

		when(mockObjectFactoryService.getResourcesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.resources_Get(mockInput)).thenReturn(mockResponse);

		Resources_GetResponse response = bartecRequestService.getResources(COMPANY_ID, PARAM5, PARAM1, PARAM2);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setResourceType(PARAM5);
		verify(mockInput, times(1)).setAssetTag(PARAM1);
		verify(mockInput, times(1)).setSerialNumber(PARAM2);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getResourcesDocuments_WhenNoError_ThenReturnResources_Documents_GetResponse() throws Exception {

		Resources_Documents_Get mockInput = mock(Resources_Documents_Get.class);
		Resources_Documents_GetResponse mockResponse = mock(Resources_Documents_GetResponse.class);

		when(mockObjectFactoryService.getResourcesDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.resources_Documents_Get(mockInput)).thenReturn(mockResponse);

		Resources_Documents_GetResponse response = bartecRequestService.getResourcesDocuments(COMPANY_ID, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM5);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getResourcesType_WhenNoError_ThenReturnResources_Types_GetResponse() throws Exception {

		Resources_Types_Get mockInput = mock(Resources_Types_Get.class);
		Resources_Types_GetResponse mockResponse = mock(Resources_Types_GetResponse.class);

		when(mockObjectFactoryService.getResourcesTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.resources_Types_Get(mockInput)).thenReturn(mockResponse);

		Resources_Types_GetResponse response = bartecRequestService.getResourcesType(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getResourcesType_WhenNoError_ThenReturnServiceRequests_Appointments_Availability_GetResponse() throws Exception {

		ServiceRequests_Appointments_Availability_Get mockInput = mock(ServiceRequests_Appointments_Availability_Get.class);
		ServiceRequests_Appointments_Availability_GetResponse mockResponse = mock(ServiceRequests_Appointments_Availability_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsAppoitmentsAvailabilityInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Appointments_Availability_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Appointments_Availability_GetResponse response = bartecRequestService.getServiceRequestsAppoitmentsAvailability(COMPANY_ID, PARAM20, PARAM20, PARAM23);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAppointmentIndex(PARAM20);
		verify(mockInput, times(1)).setServiceTypeID(PARAM20);
		verify(mockInput, times(1)).setStartDate(PARAM23);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequests_WhenNoError_ThenReturnServiceRequests_GetResponse() throws Exception {

		ServiceRequests_Get mockInput = mock(ServiceRequests_Get.class);
		ServiceRequests_GetResponse mockResponse = mock(ServiceRequests_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_GetResponse response = bartecRequestService.getServiceRequests(COMPANY_ID, PARAM1, PARAM1, PARAM10, PARAM28, PARAM2, PARAM21, PARAM25, PARAM25, PARAM24, PARAM12, PARAM12,
				PARAM12, PARAM12, PARAM3, PARAM24);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setContactEmail(PARAM1);
		verify(mockInput, times(1)).setServiceCode(PARAM1);
		verify(mockInput, times(1)).setExternalRef(PARAM10);
		verify(mockInput, times(1)).setUPRNs(PARAM28);
		verify(mockInput, times(1)).setContactName(PARAM2);
		verify(mockInput, times(1)).setContactPhoneNo(PARAM21);
		verify(mockInput, times(1)).setPremisesBounds(PARAM25);
		verify(mockInput, times(1)).setRequestBounds(PARAM25);
		verify(mockInput, times(1)).setRequestDate(PARAM24);
		verify(mockInput, times(1)).setServiceTypes(PARAM12);
		verify(mockInput, times(1)).setLandTypes(PARAM12);
		verify(mockInput, times(1)).setServiceStatuses(PARAM12);
		verify(mockInput, times(1)).setCrews(PARAM12);
		verify(mockInput, times(1)).setSLAStatus(PARAM3);
		verify(mockInput, times(1)).setSLABreachTime(PARAM24);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsClasses_WhenNoError_ThenReturnServiceRequests_Classes_GetResponse() throws Exception {

		ServiceRequests_Classes_Get mockInput = mock(ServiceRequests_Classes_Get.class);
		ServiceRequests_Classes_GetResponse mockResponse = mock(ServiceRequests_Classes_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsClassesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Classes_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Classes_GetResponse response = bartecRequestService.getServiceRequestsClasses(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsDetails_WhenNoError_ThenReturnServiceRequests_Classes_GetResponse() throws Exception {

		ServiceRequests_Detail_Get mockInput = mock(ServiceRequests_Detail_Get.class);
		ServiceRequests_Detail_GetResponse mockResponse = mock(ServiceRequests_Detail_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsDetailsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Detail_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Detail_GetResponse response = bartecRequestService.getServiceRequestsDetails(COMPANY_ID, PARAM12, PARAM18);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setServiceRequestIDs(PARAM12);
		verify(mockInput, times(1)).setServiceCodes(PARAM18);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsHistory_GetResponse_WhenNoError_ThenReturnServiceRequests_History_GetResponse() throws Exception {

		ServiceRequests_History_Get mockInput = mock(ServiceRequests_History_Get.class);
		ServiceRequests_History_GetResponse mockResponse = mock(ServiceRequests_History_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsHistoryInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_History_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_History_GetResponse response = bartecRequestService.getServiceRequestsHistory(COMPANY_ID, PARAM20, PARAM20, PARAM23);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDate(PARAM23);
		verify(mockInput, times(1)).setID(PARAM20);
		verify(mockInput, times(1)).setServiceRequestID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsNotes_GetResponse_WhenNoError_ThenReturnServiceRequests_History_GetResponse() throws Exception {

		ServiceRequests_Notes_Get mockInput = mock(ServiceRequests_Notes_Get.class);
		ServiceRequests_Notes_GetResponse mockResponse = mock(ServiceRequests_Notes_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsNotesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Notes_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Notes_GetResponse response = bartecRequestService.getServiceRequestsNotes(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setServiceRequestID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsNotesTypes_GetResponse_WhenNoError_ThenReturnServiceRequests_Notes_Types_GetResponse() throws Exception {

		ServiceRequests_Notes_Types_Get mockInput = mock(ServiceRequests_Notes_Types_Get.class);
		ServiceRequests_Notes_Types_GetResponse mockResponse = mock(ServiceRequests_Notes_Types_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsNotesTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Notes_Types_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Notes_Types_GetResponse response = bartecRequestService.getServiceRequestsNotesTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsSLAs_GetResponse_WhenNoError_ThenReturnServiceRequests_Notes_Types_GetResponse() throws Exception {

		ServiceRequests_SLAs_Get mockInput = mock(ServiceRequests_SLAs_Get.class);
		ServiceRequests_SLAs_GetResponse mockResponse = mock(ServiceRequests_SLAs_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsSLAsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_SLAs_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_SLAs_GetResponse response = bartecRequestService.getServiceRequestsSLAs(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsStatuses_GetResponse_WhenNoError_ThenReturnServiceRequests_Statuses_GetResponse() throws Exception {

		ServiceRequests_Statuses_Get mockInput = mock(ServiceRequests_Statuses_Get.class);
		ServiceRequests_Statuses_GetResponse mockResponse = mock(ServiceRequests_Statuses_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsStatusesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Statuses_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Statuses_GetResponse response = bartecRequestService.getServiceRequestsStatuses(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setServiceTypeID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsTypes_GetResponse_WhenNoError_ThenReturnServiceRequests_Types_GetResponse() throws Exception {

		ServiceRequests_Types_Get mockInput = mock(ServiceRequests_Types_Get.class);
		ServiceRequests_Types_GetResponse mockResponse = mock(ServiceRequests_Types_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Types_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Types_GetResponse response = bartecRequestService.getServiceRequestsTypes(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setServiceClassID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsTypes_GetResponse_WhenNoError_ThenReturnServiceRequests_Updates_GetResponse() throws Exception {

		ServiceRequests_Updates_Get mockInput = mock(ServiceRequests_Updates_Get.class);
		ServiceRequests_Updates_GetResponse mockResponse = mock(ServiceRequests_Updates_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsUpdatesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Updates_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Updates_GetResponse response = bartecRequestService.getServiceRequestsUpdates(COMPANY_ID, PARAM1, PARAM23);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setServiceCode(PARAM1);
		verify(mockInput, times(1)).setLastUpdated(PARAM23);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getServiceRequestsTypes_GetResponse_WhenNoError_ThenReturnSites_Documents_GetResponse() throws Exception {

		Sites_Documents_Get mockInput = mock(Sites_Documents_Get.class);
		Sites_Documents_GetResponse mockResponse = mock(Sites_Documents_GetResponse.class);

		when(mockObjectFactoryService.getSiteDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.sites_Documents_Get(mockInput)).thenReturn(mockResponse);

		Sites_Documents_GetResponse response = bartecRequestService.getSiteDocuments(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getSiteDocumentsAll_GetResponse_WhenNoError_ThenReturnSites_Documents_GetAllResponse() throws Exception {

		Sites_Documents_GetAll mockInput = mock(Sites_Documents_GetAll.class);
		Sites_Documents_GetAllResponse mockResponse = mock(Sites_Documents_GetAllResponse.class);

		when(mockObjectFactoryService.getSiteDocumentsAllInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.sites_Documents_GetAll(mockInput)).thenReturn(mockResponse);

		Sites_Documents_GetAllResponse response = bartecRequestService.getSiteDocumentsAll(COMPANY_ID, PARAM24, PARAM1, PARAM12);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDateRange(PARAM24);
		verify(mockInput, times(1)).setDocumentType(PARAM1);
		verify(mockInput, times(1)).setSites(PARAM12);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getSites_WhenNoError_ThenReturnSites_GetResponse() throws Exception {

		Sites_Get mockInput = mock(Sites_Get.class);
		Sites_GetResponse mockResponse = mock(Sites_GetResponse.class);

		when(mockObjectFactoryService.getSitesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.sites_Get(mockInput)).thenReturn(mockResponse);

		Sites_GetResponse response = bartecRequestService.getSites(COMPANY_ID, PARAM1, PARAM23, PARAM23, PARAM20, PARAM10, PARAM23, PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAccountNumber(PARAM1);
		verify(mockInput, times(1)).setDateCreated(PARAM23);
		verify(mockInput, times(1)).setEndDate(PARAM23);
		verify(mockInput, times(1)).setSiteID(PARAM20);
		verify(mockInput, times(1)).setSiteName(PARAM10);
		verify(mockInput, times(1)).setStartDate(PARAM23);
		verify(mockInput, times(1)).setUPRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getStreets_GetResponse_WhenNoError_ThenReturnStreets_Events_Types_GetResponse() throws Exception {

		Streets_Get mockInput = mock(Streets_Get.class);
		Streets_GetResponse mockResponse = mock(Streets_GetResponse.class);

		when(mockObjectFactoryService.getStreetsGetInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.streets_Get(mockInput)).thenReturn(mockResponse);

		Streets_GetResponse response = bartecRequestService.getStreets(COMPANY_ID, PARAM11, PARAM1, PARAM10, PARAM2);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setLocality(PARAM1);
		verify(mockInput, times(1)).setStreet(PARAM10);
		verify(mockInput, times(1)).setTown(PARAM2);
		verify(mockInput, times(1)).setUSRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getStreetsAttributes_GetResponse_WhenNoError_ThenReturnStreets_Attributes_GetResponse() throws Exception {

		Streets_Attributes_Get mockInput = mock(Streets_Attributes_Get.class);
		Streets_Attributes_GetResponse mockResponse = mock(Streets_Attributes_GetResponse.class);

		when(mockObjectFactoryService.getStreetsAttributesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.streets_Attributes_Get(mockInput)).thenReturn(mockResponse);

		Streets_Attributes_GetResponse response = bartecRequestService.getStreetsAttributes(COMPANY_ID, PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUSRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getStreetsDetail_GetResponse_WhenNoError_ThenReturnStreets_Detail_GetResponse() throws Exception {

		Streets_Detail_Get mockInput = mock(Streets_Detail_Get.class);
		Streets_Detail_GetResponse mockResponse = mock(Streets_Detail_GetResponse.class);

		when(mockObjectFactoryService.getStreetsDetailInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.streets_Detail_Get(mockInput)).thenReturn(mockResponse);

		Streets_Detail_GetResponse response = bartecRequestService.getStreetsDetail(COMPANY_ID, PARAM11);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUSRN(PARAM11);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getStreetsEvents_GetResponse_WhenNoError_ThenReturnStreets_Events_GetResponse() throws Exception {

		Streets_Events_Get mockInput = mock(Streets_Events_Get.class);
		Streets_Events_GetResponse mockResponse = mock(Streets_Events_GetResponse.class);

		when(mockObjectFactoryService.getStreetsEventsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.streets_Events_Get(mockInput)).thenReturn(mockResponse);

		Streets_Events_GetResponse response = bartecRequestService.getStreetsEvents(COMPANY_ID, PARAM11, PARAM1, PARAM10, PARAM23, PARAM25, PARAM2, PARAM23);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setUSRN(PARAM11);
		verify(mockInput, times(1)).setDeviceName(PARAM1);
		verify(mockInput, times(1)).setDeviceType(PARAM10);
		verify(mockInput, times(1)).setEndDate(PARAM23);
		verify(mockInput, times(1)).setEvent_Bounds(PARAM25);
		verify(mockInput, times(1)).setEventType(PARAM2);
		verify(mockInput, times(1)).setStartDate(PARAM23);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getStreetsEventsType_GetResponse_WhenNoError_ThenReturnStreets_Events_GetResponse() throws Exception {

		Streets_Events_Types_Get mockInput = mock(Streets_Events_Types_Get.class);
		Streets_Events_Types_GetResponse mockResponse = mock(Streets_Events_Types_GetResponse.class);

		when(mockObjectFactoryService.getStreetsEventsTypeInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.streets_Events_Types_Get(mockInput)).thenReturn(mockResponse);

		Streets_Events_Types_GetResponse response = bartecRequestService.getStreetsEventsType(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getSystemExtendedDateDefinitions_GetResponse_WhenNoError_ThenReturnSystem_ExtendedDataDefinitions_GetResponse() throws Exception {

		System_ExtendedDataDefinitions_Get mockInput = mock(System_ExtendedDataDefinitions_Get.class);
		System_ExtendedDataDefinitions_GetResponse mockResponse = mock(System_ExtendedDataDefinitions_GetResponse.class);

		when(mockObjectFactoryService.getSystemExtendedDateDefinitionsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.system_ExtendedDataDefinitions_Get(mockInput)).thenReturn(mockResponse);

		System_ExtendedDataDefinitions_GetResponse response = bartecRequestService.getSystemExtendedDateDefinitions(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getSystemLandTypes_GetResponse_WhenNoError_ThenReturnSystem_ExtendedDataDefinitions_GetResponse() throws Exception {

		System_LandTypes_Get mockInput = mock(System_LandTypes_Get.class);
		System_LandTypes_GetResponse mockResponse = mock(System_LandTypes_GetResponse.class);

		when(mockObjectFactoryService.getSystemLandTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.system_LandTypes_Get(mockInput)).thenReturn(mockResponse);

		System_LandTypes_GetResponse response = bartecRequestService.getSystemLandTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getSystemWasteTypes_GetResponse_WhenNoError_ThenReturnSystem_ExtendedDataDefinitions_GetResponse() throws Exception {

		System_WasteTypes_Get mockInput = mock(System_WasteTypes_Get.class);
		System_WasteTypes_GetResponse mockResponse = mock(System_WasteTypes_GetResponse.class);

		when(mockObjectFactoryService.getSystemWasteTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.system_WasteTypes_Get(mockInput)).thenReturn(mockResponse);

		System_WasteTypes_GetResponse response = bartecRequestService.getSystemWasteTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getVehicleInspectionForms_GetResponse_WhenNoError_ThenReturnVehicle_InspectionForms_GetResponse() throws Exception {

		Vehicle_InspectionForms_Get mockInput = mock(Vehicle_InspectionForms_Get.class);
		Vehicle_InspectionForms_GetResponse mockResponse = mock(Vehicle_InspectionForms_GetResponse.class);

		when(mockObjectFactoryService.getVehicleInspectionFormsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.vehicle_InspectionForms_Get(mockInput)).thenReturn(mockResponse);

		Vehicle_InspectionForms_GetResponse response = bartecRequestService.getVehicleInspectionForms(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getVehicleInspectionResponses_GetResponse_WhenNoError_ThenReturnVehicle_InspectionResults_GetResponse() throws Exception {

		Vehicle_InspectionResults_Get mockInput = mock(Vehicle_InspectionResults_Get.class);
		Vehicle_InspectionResults_GetResponse mockResponse = mock(Vehicle_InspectionResults_GetResponse.class);

		when(mockObjectFactoryService.getVehicleInspectionResponsesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.vehicle_InspectionResults_Get(mockInput)).thenReturn(mockResponse);

		Vehicle_InspectionResults_GetResponse response = bartecRequestService.getVehicleInspectionResult(COMPANY_ID, PARAM24, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDateRange(PARAM24);
		verify(mockInput, times(1)).setDeviceID(PARAM20);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getVehicles_WhenNoError_ThenReturnVehicles_GetResponse() throws Exception {

		Vehicles_Get mockInput = mock(Vehicles_Get.class);
		Vehicles_GetResponse mockResponse = mock(Vehicles_GetResponse.class);

		when(mockObjectFactoryService.getVehiclesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.vehicles_Get(mockInput)).thenReturn(mockResponse);

		Vehicles_GetResponse response = bartecRequestService.getVehicles(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getWorkGroups_WhenNoError_ThenReturnVehicles_GetResponse() throws Exception {

		WorkGroups_Get mockInput = mock(WorkGroups_Get.class);
		WorkGroups_GetResponse mockResponse = mock(WorkGroups_GetResponse.class);

		when(mockObjectFactoryService.getWorkGroups()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.workGroups_Get(mockInput)).thenReturn(mockResponse);

		WorkGroups_GetResponse response = bartecRequestService.getWorkGroups(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);
		assertEquals(response, mockResponse);

	}

	@Test
	public void getWorkPacks_WhenNoError_ThenReturnWorkPacks_GetResponse() throws Exception {

		WorkPacks_Get mockInput = mock(WorkPacks_Get.class);
		WorkPacks_GetResponse mockResponse = mock(WorkPacks_GetResponse.class);

		when(mockObjectFactoryService.getWorkPacksInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.workPacks_Get(mockInput)).thenReturn(mockResponse);

		WorkPacks_GetResponse response = bartecRequestService.getWorkPacks(COMPANY_ID, PARAM20, PARAM24, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setCrewID(PARAM20);
		verify(mockInput, times(1)).setDate(PARAM24);
		verify(mockInput, times(1)).setWorkGroupID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getWorkPacksMetrics_WhenNoError_ThenReturnWorkpacks_Metrics_GetResponse() throws Exception {

		Workpacks_Metrics_Get mockInput = mock(Workpacks_Metrics_Get.class);
		Workpacks_Metrics_GetResponse mockResponse = mock(Workpacks_Metrics_GetResponse.class);

		when(mockObjectFactoryService.getWorkPacksMetricsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.workpacks_Metrics_Get(mockInput)).thenReturn(mockResponse);

		Workpacks_Metrics_GetResponse response = bartecRequestService.getWorkPacksMetrics(COMPANY_ID, PARAM12);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setWorkpacks(PARAM12);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getWorkPacksNotes_WhenNoError_ThenReturnWorkpacks_Notes_GetResponse() throws Exception {

		Workpacks_Notes_Get mockInput = mock(Workpacks_Notes_Get.class);
		Workpacks_Notes_GetResponse mockResponse = mock(Workpacks_Notes_GetResponse.class);

		when(mockObjectFactoryService.getWorkPacksNotesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.workpacks_Notes_Get(mockInput)).thenReturn(mockResponse);

		Workpacks_Notes_GetResponse response = bartecRequestService.getWorkPacksNotes(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setWorkpackID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void getWorkPacksNotesTypes_WhenNoError_ThenReturnWorkpacks_Notes_Types_GetResponse() throws Exception {

		Workpacks_Notes_Types_Get mockInput = mock(Workpacks_Notes_Types_Get.class);
		Workpacks_Notes_Types_GetResponse mockResponse = mock(Workpacks_Notes_Types_GetResponse.class);

		when(mockObjectFactoryService.getWorkPacksNotesTypesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.workpacks_Notes_Types_Get(mockInput)).thenReturn(mockResponse);

		Workpacks_Notes_Types_GetResponse response = bartecRequestService.getWorkPacksNotesTypes(COMPANY_ID);

		verify(mockInput, times(1)).setToken(TOKEN);

		assertEquals(response, mockResponse);

	}

	@Test
	public void ggetFeatureNotes_WhenNoError_ThenReturnFeatures_Manufacturers_GetResponse() throws Exception {

		Features_Notes_Get mockInput = mock(Features_Notes_Get.class);
		Features_Notes_GetResponse mockResponse = mock(Features_Notes_GetResponse.class);

		when(mockObjectFactoryService.getFeatureNotesInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.features_Notes_Get(mockInput)).thenReturn(mockResponse);

		Features_Notes_GetResponse response = bartecRequestService.getFeatureNotes(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setFeatureID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void reserveServiceRequestAppointmentSlot_WhenNoError_ThenReturnServiceRequest_Appointment_Slot_ReserveResponse() throws Exception {

		ServiceRequest_Appointment_Slot_Reserve mockInput = mock(ServiceRequest_Appointment_Slot_Reserve.class);
		ServiceRequest_Appointment_Slot_ReserveResponse mockResponse = mock(ServiceRequest_Appointment_Slot_ReserveResponse.class);

		when(mockObjectFactoryService.reserveServiceRequestAppointmentSlotInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Appointment_Slot_Reserve(mockInput)).thenReturn(mockResponse);

		ServiceRequest_Appointment_Slot_ReserveResponse response = bartecRequestService.reserveServiceRequestAppointmentSlot(COMPANY_ID, PARAM20, PARAM20, PARAM5, PARAM5);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setReservationID(PARAM20);
		verify(mockInput, times(1)).setSlotID(PARAM5);
		verify(mockInput, times(1)).setWorkPackID(PARAM5);
		verify(mockInput, times(1)).setAppointmentIndex(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void ServiceRequests_Documents_GetResponse_WhenNoError_ThenReturnServiceRequests_Documents_GetResponse() throws Exception {

		ServiceRequests_Documents_Get mockInput = mock(ServiceRequests_Documents_Get.class);
		ServiceRequests_Documents_GetResponse mockResponse = mock(ServiceRequests_Documents_GetResponse.class);

		when(mockObjectFactoryService.getServiceRequestsDocumentsInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequests_Documents_Get(mockInput)).thenReturn(mockResponse);

		ServiceRequests_Documents_GetResponse response = bartecRequestService.getServiceRequestsDocuments(COMPANY_ID, PARAM20);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setDocumentID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void setServiceRequestStatus_WhenNoError_ThenReturnServiceRequest_Status_SetResponse() throws Exception {

		ServiceRequest_Status_Set mockInput = mock(ServiceRequest_Status_Set.class);
		ServiceRequest_Status_SetResponse mockResponse = mock(ServiceRequest_Status_SetResponse.class);

		when(mockObjectFactoryService.setServiceRequestStatusInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Status_Set(mockInput)).thenReturn(mockResponse);

		ServiceRequest_Status_SetResponse response = bartecRequestService.setServiceRequestStatus(COMPANY_ID, PARAM5, PARAM10, PARAM20, PARAM1);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setComments(PARAM1);
		verify(mockInput, times(1)).setServiceCode(PARAM10);
		verify(mockInput, times(1)).setServiceRequestID(PARAM5);
		verify(mockInput, times(1)).setStatusID(PARAM20);

		assertEquals(response, mockResponse);

	}

	@Test
	public void updatePremisesAttribute_WhenNoError_ThenReturnPremises_Attributes_DeleteResponse() throws Exception {

		Premises_Attribute_Update mockInput = mock(Premises_Attribute_Update.class);
		Premises_Attribute_UpdateResponse mockResponse = mock(Premises_Attribute_UpdateResponse.class);

		when(mockObjectFactoryService.updatePremisesAttributeInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.premises_Attribute_Update(mockInput)).thenReturn(mockResponse);

		Premises_Attribute_UpdateResponse response = bartecRequestService.updatePremisesAttribute(COMPANY_ID, PARAM20, PARAM1, PARAM10, PARAM23, PARAM23);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setAttributeID(PARAM20);
		verify(mockInput, times(1)).setAttributeValue(PARAM1);
		verify(mockInput, times(1)).setComments(PARAM10);
		verify(mockInput, times(1)).setExpiryDate(PARAM23);
		verify(mockInput, times(1)).setReviewDate(PARAM23);

		assertEquals(response, mockResponse);

	}

	@Test
	public void updateServiceRequest_WhenNoError_ThenReturnServiceRequest_UpdateResponse() throws Exception {

		ServiceRequest_Update mockInput = mock(ServiceRequest_Update.class);
		ServiceRequest_UpdateResponse mockResponse = mock(ServiceRequest_UpdateResponse.class);

		when(mockObjectFactoryService.updateServiceRequestInput()).thenReturn(mockInput);
		when(mockCollectiveAPIWebService.serviceRequest_Update(mockInput)).thenReturn(mockResponse);

		ServiceRequest_UpdateResponse response = bartecRequestService.updateServiceRequest(COMPANY_ID, PARAM20, PARAM1);

		verify(mockInput, times(1)).setToken(TOKEN);
		verify(mockInput, times(1)).setSR_ID(PARAM20);
		verify(mockInput, times(1)).setServiceCode(PARAM1);
		assertEquals(response, mockResponse);

	}
}
