package com.placecube.digitalplace.local.waste.bartec.service;

import static org.junit.Assert.assertEquals;

import com.placecube.digitalplace.local.waste.bartec.axis.Authenticate;
import com.placecube.digitalplace.local.waste.bartec.axis.AuthenticateResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.AuthenticationAPIWebService;
import com.placecube.digitalplace.local.waste.bartec.axis.AuthenticationAPIWebServiceStub;
import com.placecube.digitalplace.local.waste.bartec.axis.www.AuthenticateResult_type0;
import com.placecube.digitalplace.local.waste.bartec.axis.www.Token_type2;
import com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BartecAxisService.class })
public class BartecAxisServiceTest extends PowerMockito {

	private static final String PASSWORD = "PASSWORD";
	private static final String USERNAME = "USERNAME";
	private static final String OBJTOKEN = "XYZ";

	@Mock
	private AuthenticationAPIWebServiceStub mockAuthenticationAPIWebServiceStub;

	@Mock
	private AuthenticationAPIWebService mockAuthenticationAPIWebService;

	@Mock
	private Authenticate mockAuthenticate;

	@Mock
	private AuthenticateResponse mockAuthenticateResponse;

	@Mock
	private BartecConfiguration mockBartecCompanyConfiguration;

	@Mock
	private Token_type2 mockToken_type2;

	@Mock
	private AuthenticateResult_type0 mockAuthenticateResult_type0;

	@InjectMocks
	private BartecAxisService mockBartecAxisService;

	@Test
	public void authenticate_WhenNoErrors_ThenReturnsStringToken() throws Exception {

		whenNew(Authenticate.class).withNoArguments().thenReturn(mockAuthenticate);
		mockAuthenticate.setPassword(PASSWORD);
		mockAuthenticate.setUser(USERNAME);

		whenNew(AuthenticationAPIWebServiceStub.class).withNoArguments().thenReturn(mockAuthenticationAPIWebServiceStub);
		Mockito.doReturn(mockAuthenticateResponse).when(mockAuthenticationAPIWebServiceStub).authenticate(mockAuthenticate);

		when(mockAuthenticateResponse.getAuthenticateResult()).thenReturn(mockAuthenticateResult_type0);
		when(mockAuthenticateResult_type0.getToken()).thenReturn(mockToken_type2);

		when(mockToken_type2.getTokenString()).thenReturn(OBJTOKEN);

		String result = mockBartecAxisService.authenticate(USERNAME, PASSWORD);

		assertEquals(OBJTOKEN, result);

	}

}
