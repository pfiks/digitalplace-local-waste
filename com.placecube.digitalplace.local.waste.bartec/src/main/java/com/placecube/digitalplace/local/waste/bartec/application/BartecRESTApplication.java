package com.placecube.digitalplace.local.waste.bartec.application;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.placecube.digitalplace.local.waste.bartec.axis.*;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtAttachedDocument;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtPointMetric;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterBusiness;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact;
import com.placecube.digitalplace.local.waste.bartec.service.BartecRequestService;
import com.placecube.digitalplace.local.waste.bartec.service.JsonResponseParserService;

@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/waste/bartec", JaxrsWhiteboardConstants.JAX_RS_NAME + "=Bartec.Rest", "oauth2.scopechecker.type=none",
		"auth.verifier.guest.allowed=false", "liferay.access.control.disable=true" }, service = Application.class)
public class BartecRESTApplication extends Application {

	@Reference
	private BartecRequestService bartecRequestService;

	@Reference
	private JsonResponseParserService jsonResponseParserService;

	@POST
	@Path("/ServiceRequest_Cancel")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String cancelServiceRequest(@FormParam("serviceRequestId") Integer serviceRequestId, @FormParam("serviceCode") String serviceCode, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_CancelResponse response = bartecRequestService.cancelServiceRequest(getCompanyId(request), serviceRequestId, serviceCode);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Appointment_Reservation_Cancel")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String cancelServiceRequestAppointment(@FormParam("reservationId") Integer reservationId, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_Appointment_Reservation_CancelResponse response = bartecRequestService.cancelServiceRequestAppointment(getCompanyId(request), reservationId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Jobs_Close")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String closeJobs(@FormParam("jobId") int jobId, @FormParam("complete") boolean complete, @FormParam("jobId") boolean partDone, @FormParam("noFurtherAction") boolean noFurtherAction,
			@FormParam("reasonText") String reasonText, @FormParam("closingComments") String closingComments, @FormParam("outcome") String outcome, @FormParam("dateClosed") Calendar dateClosed,
			@Context HttpServletRequest request) throws Exception {

		Jobs_CloseResponse response = bartecRequestService.closeJobs(getCompanyId(request), jobId, complete, partDone, noFurtherAction, reasonText, closingComments, outcome, dateClosed);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Business_Document_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createBusinessDocument(@FormParam("accountId") int accountId, @FormParam("attachedDocuemnt") CtAttachedDocument attachedDocuemnt, @FormParam("dateTaken") Calendar dateTaken,
			@FormParam("comment") String comment, @Context HttpServletRequest request) throws Exception {

		Business_Document_CreateResponse response = bartecRequestService.createBusinessDocument(getCompanyId(request), accountId, attachedDocuemnt, dateTaken, comment);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Premises_Attribute_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createPremisesAttribute(@FormParam("attributeId") int attributeId, @FormParam("attribuetValue") String attribuetValue, @FormParam("comments") String comments,
			@FormParam("confirmation") int confirmation, @FormParam("expiryDate") Calendar expiryDate, @FormParam("reviewDate") Calendar reviewDate, @FormParam("uprn") BigDecimal uprn,
			@Context HttpServletRequest request) throws Exception {

		Premises_Attribute_CreateResponse response = bartecRequestService.createPremisesAttribute(getCompanyId(request), attributeId, attribuetValue, comments, confirmation, expiryDate, reviewDate,
				uprn);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Premises_Document_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createPremisesDocument(@FormParam("attachedDocument") CtAttachedDocument attachedDocument, @FormParam("comment") String comment, @FormParam("dateTaken") Calendar dateTaken,
			@FormParam("uprn") BigDecimal uprn, @Context HttpServletRequest request) throws Exception {

		Premises_Document_CreateResponse response = bartecRequestService.createPremisesDocument(getCompanyId(request), attachedDocument, comment, dateTaken, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Premises_Event_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createPremisesEvent(@FormParam("uprn") BigDecimal uprn, @FormParam("comments") String comments, @FormParam("crewId") int crewId, @FormParam("deviceId") int deviceId,
			@FormParam("eventDate") Calendar eventDate, @FormParam("eventLocation") CtPointMetric eventLocation, @FormParam("eventType") int eventType,
			@FormParam("featureComments") ArrayOfString featureComments, @FormParam("featureIds") ArrayOfInt featureIds, @FormParam("jobId") int jobId,
			@FormParam("subEventComments") ArrayOfString subEventComments, @Context HttpServletRequest request) throws Exception {

		Premises_Event_CreateResponse response = bartecRequestService.createPremisesEvent(getCompanyId(request), uprn, comments, crewId, deviceId, eventDate, eventLocation, eventType, featureComments,
				featureIds, jobId, subEventComments);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Premises_Event_Document_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createPremisesEventsDocument(@FormParam("attachedDocument") CtAttachedDocument attachedDocument, @FormParam("comment") String comment, @FormParam("dateTaken") Calendar dateTaken,
			@FormParam("eventId") int eventId, @Context HttpServletRequest request) throws Exception {

		Premises_Events_Document_CreateResponse response = bartecRequestService.createPremisesEventsDocument(getCompanyId(request), attachedDocument, comment, dateTaken, eventId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createServiceRequest(@FormParam("appointementReservationId") int appointementReservationId,
			@FormParam("extendedData") ArrayOfServiceRequest_CreateServiceRequest_CreateFields extendedData, @FormParam("crewId") int crewId, @FormParam("dateRequested") Calendar dateRequested,
			@FormParam("relatedPremises") ArrayOfServiceRequest_CreateServiceRequest_CreateLocation relatedPremises, @FormParam("landTypeId") int landTypeId,
			@FormParam("relatedServiceRequests") ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest relatedServiceRequests,
			@FormParam("reporterBusiness") ServiceRequest_CreateServiceRequest_CreateReporterBusiness reporterBusiness, @FormParam("serviceLocationDescription") String serviceLocationDescription,
			@FormParam("reporterContact") ServiceRequest_CreateServiceRequest_CreateReporterContact reporterContact, @FormParam("serviceRequestLocation") CtMapPoint_Set serviceRequestLocation,
			@FormParam("attachedDocument") int serviceTypeId, @FormParam("serviceStatusId") int serviceStatusId, @FormParam("slaId") int slaId, @FormParam("source") String source,
			@FormParam("uprn") BigDecimal uprn, @FormParam("externalRef") String externalRef, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_CreateResponse response = bartecRequestService.createServiceRequest(getCompanyId(request), appointementReservationId, extendedData, crewId, dateRequested, relatedPremises,
				landTypeId, relatedServiceRequests, reporterBusiness, serviceLocationDescription, reporterContact, serviceRequestLocation, serviceTypeId, serviceStatusId, slaId, source, uprn, externalRef);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Appointment_Reservation_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createServiceRequestAppointmentReservation(@FormParam("serviceTypeId") int serviceTypeId, @FormParam("slotId") int slotId, @FormParam("workPackId") int workPackId,
			@Context HttpServletRequest request) throws Exception {

		ServiceRequest_Appointment_Reservation_CreateResponse response = bartecRequestService.createServiceRequestAppointmentReservation(getCompanyId(request), serviceTypeId, slotId, workPackId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Document_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createServiceRequestDocument(@FormParam("attachedDocument") CtAttachedDocument attachedDocument, @FormParam("comment") String comment, @FormParam("dateTaken") Calendar dateTaken,
			@FormParam("srPublic") boolean srPublic, @FormParam("serviceRequestId") int serviceRequestId, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_Document_CreateResponse response = bartecRequestService.createServiceRequestDocument(getCompanyId(request), attachedDocument, comment, dateTaken, srPublic, serviceRequestId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Note_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createServiceRequestNote(@FormParam("serviceRequestId") int serviceRequestId, @FormParam("serviceCode") String serviceCode, @FormParam("note") String note,
			@FormParam("comment") String comment, @FormParam("noteType") int noteType, @FormParam("sequenceNumber") int sequenceNumber, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_Note_CreateResponse response = bartecRequestService.createServiceRequestNote(getCompanyId(request), serviceRequestId, serviceCode, note, comment, noteType, sequenceNumber);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Site_Document_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createSiteDocument(@FormParam("attachedDocument") CtAttachedDocument attachedDocument, @FormParam("comment") String comment, @FormParam("dateTaken") Calendar dateTaken,
			@FormParam("siteId") int siteId, @Context HttpServletRequest request) throws Exception {

		Site_Document_CreateResponse response = bartecRequestService.createSiteDocument(getCompanyId(request), attachedDocument, comment, dateTaken, siteId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Vehicle_Message_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createVehicleMessage(@FormParam("deviceId") int deviceId, @FormParam("messageTax") String messageTax, @FormParam("subject") String subject, @Context HttpServletRequest request)
			throws Exception {

		Vehicle_Message_CreateResponse response = bartecRequestService.createVehicleMessage(getCompanyId(request), deviceId, messageTax, subject);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Workpack_Note_Create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String createWorkPackNote(@FormParam("comment") String comment, @FormParam("note") String note, @FormParam("subject") String subject, @FormParam("noteTypeId") int noteTypeId,
			@FormParam("workPackId") int workPackId, @Context HttpServletRequest request) throws Exception {

		Workpack_Note_CreateResponse response = bartecRequestService.createWorkPackNote(getCompanyId(request), comment, note, subject, noteTypeId, workPackId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Premises_Attributes_Delete")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String detetePremisesAttributes(@FormParam("attributeId") int attributeId, @FormParam("uprn") BigDecimal uprn, @Context HttpServletRequest request) throws Exception {

		Premises_Attributes_DeleteResponse response = bartecRequestService.detetePremisesAttributes(getCompanyId(request), attributeId, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Appointment_Reservation_Extend")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String extendServiceRequestAppointmentReservation(@FormParam("reservationId") int reservationId, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_Appointment_Reservation_ExtendResponse response = bartecRequestService.extendServiceRequestAppointmentReservation(getCompanyId(request), reservationId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Businesses_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBusiness(@QueryParam("accountNumber") String accountNumber, @QueryParam("accountName") String accountName, @QueryParam("businessName") String businessName,
			@QueryParam("businessDescription") String businessDescription, @QueryParam("statuses") ArrayOfInt statuses, @QueryParam("subStatuses") ArrayOfInt subStatuses,
			@QueryParam("businessTypes") ArrayOfInt businessTypes, @QueryParam("businessClasses") ArrayOfInt businessClasses, @QueryParam("dateCreated") Calendar dateCreated,
			@Context HttpServletRequest request) throws Exception {

		Businesses_GetResponse response = bartecRequestService.getBusiness(getCompanyId(request), accountNumber, accountName, businessName, businessDescription, statuses, subStatuses, businessTypes,
				businessClasses, dateCreated);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Business_Classes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBusinessClasses(@Context HttpServletRequest request) throws Exception {

		Businesses_Classes_GetResponse response = bartecRequestService.getBusinessClasses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Businesses_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBusinessDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Businesses_Documents_GetResponse response = bartecRequestService.getBusinessDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Business_Statuses_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBusinessStatuses(@Context HttpServletRequest request) throws Exception {

		Businesses_Statuses_GetResponse response = bartecRequestService.getBusinessStatuses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Business_SubStatuses_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBusinessSubStatuses(@Context HttpServletRequest request) throws Exception {

		Businesses_SubStatuses_GetResponse response = bartecRequestService.getBusinessSubStatuses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Business_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBusinessTypes(@Context HttpServletRequest request) throws Exception {

		Businesses_Types_GetResponse response = bartecRequestService.getBusinessTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Cases_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCaseDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Cases_Documents_GetResponse response = bartecRequestService.getCaseDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	private long getCompanyId(HttpServletRequest request) {
		return (Long) request.getAttribute("COMPANY_ID");
	}

	@GET
	@Path("/Crews_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCrews(@Context HttpServletRequest request) throws Exception {

		Crews_GetResponse response = bartecRequestService.getCrews(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Categories_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatureCategories(@Context HttpServletRequest request) throws Exception {

		Features_Categories_GetResponse response = bartecRequestService.getFeatureCategories(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Classes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatureClasses(@Context HttpServletRequest request) throws Exception {

		Features_Classes_GetResponse response = bartecRequestService.getFeatureClasses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Colours_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatureColours(@Context HttpServletRequest request) throws Exception {

		Features_Colours_GetResponse response = bartecRequestService.getFeatureColours(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Conditions_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatureConditions(@Context HttpServletRequest request) throws Exception {

		Features_Conditions_GetResponse response = bartecRequestService.getFeatureConditions(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Manufacturers_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatureManufacturers(@Context HttpServletRequest request) throws Exception {

		Features_Manufacturers_GetResponse response = bartecRequestService.getFeatureManufacturers(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Notes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatureNotes(@QueryParam("featureId") int featureId, @Context HttpServletRequest request) throws Exception {

		Features_Notes_GetResponse response = bartecRequestService.getFeatureNotes(getCompanyId(request), featureId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatures(@QueryParam("name") String name, @QueryParam("name") String description, @QueryParam("name") String serialNumber, @QueryParam("name") String assetTag,
			@QueryParam("name") String rfidTag, @QueryParam("name") BigDecimal uprn, @QueryParam("name") ArrayOfInt types, @QueryParam("name") ArrayOfInt statuses,
			@QueryParam("name") ArrayOfInt manufacturers, @QueryParam("name") ArrayOfInt colours, @QueryParam("name") ArrayOfInt conditions, @QueryParam("name") ArrayOfInt wasteType,
			@Context HttpServletRequest request) throws Exception {

		Features_GetResponse response = bartecRequestService.getFeatures(getCompanyId(request), name, description, serialNumber, assetTag, rfidTag, uprn, types, statuses, manufacturers, colours,
				conditions, wasteType);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Schedules_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeaturesSchedules(@QueryParam("uprn") BigDecimal uprn, @QueryParam("types") ArrayOfInt types, @Context HttpServletRequest request) throws Exception {

		Features_Schedules_GetResponse response = bartecRequestService.getFeaturesSchedules(getCompanyId(request), uprn, types);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Statuses_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatureStatuses(@Context HttpServletRequest request) throws Exception {

		Features_Statuses_GetResponse response = bartecRequestService.getFeatureStatuses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Features_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getFeatureTypes(@Context HttpServletRequest request) throws Exception {

		Features_Types_GetResponse response = bartecRequestService.getFeatureTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Inspections_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInspection(@QueryParam("siteID") int siteID, @QueryParam("siteName") String siteName, @QueryParam("uprn") BigDecimal uprn,
			@QueryParam("inspectionTypes") ArrayOfInt inspectionTypes, @QueryParam("inspectionStatuses") ArrayOfInt inspectionStatuses, @QueryParam("featureId") int featureId,
			@Context HttpServletRequest request) throws Exception {

		Inspections_GetResponse response = bartecRequestService.getInspection(getCompanyId(request), siteID, siteName, uprn, inspectionTypes, inspectionStatuses, featureId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Inspections_Classes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInspectionClasses(@Context HttpServletRequest request) throws Exception {

		Inspections_Classes_GetResponse response = bartecRequestService.getInspectionClasses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Inspections_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInspectionDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Inspections_Documents_GetResponse response = bartecRequestService.getInspectionDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Inspections_Statuses_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInspectionStatuses(@Context HttpServletRequest request) throws Exception {

		Inspections_Statuses_GetResponse response = bartecRequestService.getInspectionStatuses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Inspections_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInspectionTypes(@Context HttpServletRequest request) throws Exception {

		Inspections_Types_GetResponse response = bartecRequestService.getInspectionTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Jobs_Detail_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getJobDetail(@QueryParam("jobId") int jobId, @Context HttpServletRequest request) throws Exception {

		Jobs_Detail_GetResponse response = bartecRequestService.getJobDetail(getCompanyId(request), jobId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Jobs_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getJobDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Jobs_Documents_GetResponse response = bartecRequestService.getJobDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Jobs_FeatureScheduleDates_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getJobFeatureScheduleDates(@QueryParam("uprn") BigDecimal uprn, @QueryParam("dateRange") CtDateQuery dateRange, @QueryParam("jobBounds") CtMapBox jobBounds,
			@Context HttpServletRequest request) throws Exception {

		Jobs_FeatureScheduleDates_GetResponse response = bartecRequestService.getJobFeatureScheduleDates(getCompanyId(request), uprn, dateRange, jobBounds);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Jobs_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getJobs(@QueryParam("uprn") BigDecimal uprn, @QueryParam("usrn") BigDecimal usrn, @QueryParam("workPackId") int workPackId, @QueryParam("scheduledStart") CtDateQuery scheduledStart,
			@QueryParam("serviceRequestID") int serviceRequestID, @QueryParam("wardId") int wardId, @QueryParam("parishId") int parishId, @QueryParam("jobsBound") CtMapBox jobsBound,
			@QueryParam("includeRelated") boolean includeRelated, @Context HttpServletRequest request) throws Exception {

		Jobs_GetResponse response = bartecRequestService.getJobs(getCompanyId(request), uprn, usrn, workPackId, scheduledStart, serviceRequestID, wardId, parishId, jobsBound, includeRelated);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Jobs_WorkScheduleDates_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getJobsWorkScheduleDates(@QueryParam("uprn") BigDecimal uprn, @QueryParam("dateRange") CtDateQuery dateRange, @QueryParam("jobBounds") CtMapBox jobsBound,
			@Context HttpServletRequest request) throws Exception {

		Jobs_WorkScheduleDates_GetResponse response = bartecRequestService.getJobsWorkScheduleDates(getCompanyId(request), uprn, dateRange, jobsBound);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Licences_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLicences(@QueryParam("siteId") int siteId, @QueryParam("siteName") String siteName, @QueryParam("accountNumber") String accountNumber, @QueryParam("uprns") ArrayOfDecimal uprns,
			@QueryParam("licenceTypes") ArrayOfInt licenceTypes, @QueryParam("licenceStatuses") ArrayOfInt licenceStatuses, @QueryParam("serviceRequestId") int serviceRequestId,
			@QueryParam("startDate") CtDateQuery startDate, @QueryParam("issueDate") CtDateQuery issueDate, @QueryParam("dateCreated") CtDateQuery dateCreated, @Context HttpServletRequest request)
			throws Exception {

		Licences_GetResponse response = bartecRequestService.getLicences(getCompanyId(request), siteId, siteName, accountNumber, uprns, licenceTypes, licenceStatuses, serviceRequestId, startDate,
				issueDate, dateCreated);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Licences_Classes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLicencesClasses(@Context HttpServletRequest request) throws Exception {

		Licences_Classes_GetResponse response = bartecRequestService.getLicencesClasses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Licences_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLicencesDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Licences_Documents_GetResponse response = bartecRequestService.getLicencesDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Licences_Statuses_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLicencesStatuses(@Context HttpServletRequest request) throws Exception {

		Licences_Statuses_GetResponse response = bartecRequestService.getLicencesStatuses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Licences_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLicencesTypes(@Context HttpServletRequest request) throws Exception {

		Licences_Types_GetResponse response = bartecRequestService.getLicencesTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremises(@QueryParam("uprn") BigDecimal uprn, @QueryParam("address1") String address1, @QueryParam("address2") String address2, @QueryParam("street") String street,
			@QueryParam("locality") String locality, @QueryParam("town") String town, @QueryParam("postCode") String postCode, @QueryParam("wardName") String wardName,
			@QueryParam("parishName") String parishName, @QueryParam("userLabel") String userLabel, @QueryParam("parentUprn") BigDecimal parentUprn, @QueryParam("bounds") CtMapBox bounds,
			@QueryParam("blpuClass") String blpuClass, @QueryParam("blpulStat") ArrayOfString blpulStat, @QueryParam("usrn") BigDecimal usrn, @Context HttpServletRequest request) throws Exception {

		Premises_GetResponse response = bartecRequestService.getPremises(getCompanyId(request), uprn, address1, address2, street, locality, town, postCode, wardName, parishName, userLabel, parentUprn,
				bounds, blpuClass, blpulStat, usrn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_AttributeDefinitions_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesAttributeDefinitions(@Context HttpServletRequest request) throws Exception {

		Premises_AttributeDefinitions_GetResponse response = bartecRequestService.getPremisesAttributeDefinitions(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_Attributes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesAttributes(@QueryParam("uprn") BigDecimal uprn, @Context HttpServletRequest request) throws Exception {

		Premises_Attributes_GetResponse response = bartecRequestService.getPremisesAttributes(getCompanyId(request), uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_Detail_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesDetails(@QueryParam("uprn") BigDecimal uprn, @Context HttpServletRequest request) throws Exception {

		Premises_Detail_GetResponse response = bartecRequestService.getPremisesDetails(getCompanyId(request), uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Premises_Documents_GetResponse response = bartecRequestService.getPremisesDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_Events_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesEvents(@QueryParam("uprn") BigDecimal uprn, @QueryParam("dateRange") CtDateQuery dateRange, @QueryParam("postCode") String postCode,
			@QueryParam("wardName") String wardName, @QueryParam("parishName") String parishName, @QueryParam("premisesBound") CtMapBox premisesBound, @QueryParam("eventBounds") CtMapBox eventBounds,
			@QueryParam("eventType") String eventType, @QueryParam("eventSubType") String eventSubType, @QueryParam("minDistace") BigDecimal minDistace,
			@QueryParam("maxDistance") BigDecimal maxDistance, @QueryParam("deviceType") String deviceType, @QueryParam("deviceName") String deviceName, @QueryParam("usrn") BigDecimal usrn,
			@QueryParam("workPack") String workPack, @QueryParam("includeRelated") boolean includeRelated, @Context HttpServletRequest request) throws Exception {

		Premises_Events_GetResponse response = bartecRequestService.getPremisesEvents(getCompanyId(request), uprn, dateRange, postCode, wardName, parishName, premisesBound, eventBounds, eventType,
				eventSubType, minDistace, maxDistance, deviceType, deviceName, usrn, workPack, includeRelated);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_Events_Classes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesEventsClasses(@Context HttpServletRequest request) throws Exception {

		Premises_Events_Classes_GetResponse response = bartecRequestService.getPremisesEventsClasses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_Event_Document_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesEventsDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Premises_Events_Documents_GetResponse response = bartecRequestService.getPremisesEventsDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_Events_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesEventsTypes(@Context HttpServletRequest request) throws Exception {

		Premises_Events_Types_GetResponse response = bartecRequestService.getPremisesEventsTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Premises_FutureWorkpacks_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPremisesFutureWorkpacks(@QueryParam("uprn") BigDecimal uprn, @QueryParam("featureTypeId") int featureTypeId, @QueryParam("actionTypeId") int actionTypeId,
			@QueryParam("featureStatusId") int featureStatusId, @QueryParam("dateFrom") Calendar dateFrom, @QueryParam("dateTo") Calendar dateTo, @Context HttpServletRequest request)
			throws Exception {

		Premises_FutureWorkpacks_GetResponse response = bartecRequestService.getPremisesFutureWorkpacks(getCompanyId(request), uprn, featureTypeId, actionTypeId, featureStatusId, dateFrom, dateTo);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Resources_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getResources(@QueryParam("resourceType") int resourceType, @QueryParam("assetTag") String assetTag, @QueryParam("serialNumber") String serialNumber,
			@Context HttpServletRequest request) throws Exception {

		Resources_GetResponse response = bartecRequestService.getResources(getCompanyId(request), resourceType, assetTag, serialNumber);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Resources_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getResourcesDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Resources_Documents_GetResponse response = bartecRequestService.getResourcesDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Resources_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getResourcesType(@Context HttpServletRequest request) throws Exception {

		Resources_Types_GetResponse response = bartecRequestService.getResourcesType(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequests(@QueryParam("contactEmail") String contactEmail, @QueryParam("serviceCode") String serviceCode, @QueryParam("externalRef") String externalRef,
			@QueryParam("uprns") ArrayOfDecimal uprns, @QueryParam("contactName") String contactName, @QueryParam("contactPhoneNo") String contactPhoneNo,
			@QueryParam("premisesBounds") CtMapBox premisesBounds, @QueryParam("requestBounds") CtMapBox requestBounds, @QueryParam("requestDate") CtDateQuery requestDate,
			@QueryParam("serviceTypes") ArrayOfInt serviceTypes, @QueryParam("landTypes") ArrayOfInt landTypes, @QueryParam("serviceStatuses") ArrayOfInt serviceStatuses,
			@QueryParam("crews") ArrayOfInt crews, @QueryParam("slaStatus") String slaStatus, @QueryParam("slaBreachTime") CtDateQuery slaBreachTime, @Context HttpServletRequest request)
			throws Exception {

		ServiceRequests_GetResponse response = bartecRequestService.getServiceRequests(getCompanyId(request), contactEmail, serviceCode, externalRef, uprns, contactName, contactPhoneNo,
				premisesBounds, requestBounds, requestDate, serviceTypes, landTypes, serviceStatuses, crews, slaStatus, slaBreachTime);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Appointments_Availability_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsAppoitmentsAvailability(@QueryParam("serviceTypeId") int serviceTypeId, @QueryParam("appointmentIndex") int appointmentIndex,
			@QueryParam("startDate") Calendar startDate, @Context HttpServletRequest request) throws Exception {

		ServiceRequests_Appointments_Availability_GetResponse response = bartecRequestService.getServiceRequestsAppoitmentsAvailability(getCompanyId(request), serviceTypeId, appointmentIndex,
				startDate);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Classes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsClasses(@Context HttpServletRequest request) throws Exception {

		ServiceRequests_Classes_GetResponse response = bartecRequestService.getServiceRequestsClasses(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Detail_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsDetails(@QueryParam("serviceRequestIds") ArrayOfInt serviceRequestIds, @QueryParam("serviceCodes") ArrayOfString serviceCodes, @Context HttpServletRequest request)
			throws Exception {

		ServiceRequests_Detail_GetResponse response = bartecRequestService.getServiceRequestsDetails(getCompanyId(request), serviceRequestIds, serviceCodes);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		ServiceRequests_Documents_GetResponse response = bartecRequestService.getServiceRequestsDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_History_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsHistory(@QueryParam("serviceRequestId") int serviceRequestId, @QueryParam("id") int id, @QueryParam("date") Calendar date, @Context HttpServletRequest request)
			throws Exception {

		ServiceRequests_History_GetResponse response = bartecRequestService.getServiceRequestsHistory(getCompanyId(request), serviceRequestId, id, date);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Notes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsNotes(@QueryParam("serviceRequestId") int serviceRequestId, @Context HttpServletRequest request) throws Exception {

		ServiceRequests_Notes_GetResponse response = bartecRequestService.getServiceRequestsNotes(getCompanyId(request), serviceRequestId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Notes_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsNotesTypes(@Context HttpServletRequest request) throws Exception {

		ServiceRequests_Notes_Types_GetResponse response = bartecRequestService.getServiceRequestsNotesTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_SLAs_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsSLAs(@Context HttpServletRequest request) throws Exception {

		ServiceRequests_SLAs_GetResponse response = bartecRequestService.getServiceRequestsSLAs(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Statuses_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsStatuses(@QueryParam("serviceTypeId") int serviceTypeId, @Context HttpServletRequest request) throws Exception {

		ServiceRequests_Statuses_GetResponse response = bartecRequestService.getServiceRequestsStatuses(getCompanyId(request), serviceTypeId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequests_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsTypes(@QueryParam("serviceclassId") int serviceclassId, @Context HttpServletRequest request) throws Exception {

		ServiceRequests_Types_GetResponse response = bartecRequestService.getServiceRequestsTypes(getCompanyId(request), serviceclassId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/ServiceRequest_Updates_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServiceRequestsUpdates(@QueryParam("serviceCode") String serviceCode, @QueryParam("lastUpdated") Calendar lastUpdated, @Context HttpServletRequest request) throws Exception {

		ServiceRequests_Updates_GetResponse response = bartecRequestService.getServiceRequestsUpdates(getCompanyId(request), serviceCode, lastUpdated);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Sites_Documents_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteDocuments(@QueryParam("documentId") int documentId, @Context HttpServletRequest request) throws Exception {

		Sites_Documents_GetResponse response = bartecRequestService.getSiteDocuments(getCompanyId(request), documentId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Sites_Documents_GetAll")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSiteDocumentsAll(@QueryParam("dateRange") CtDateQuery dateRange, @QueryParam("documentType") String documentType, @QueryParam("sites") ArrayOfInt sites,
			@Context HttpServletRequest request) throws Exception {

		Sites_Documents_GetAllResponse response = bartecRequestService.getSiteDocumentsAll(getCompanyId(request), dateRange, documentType, sites);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Sites_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSites(@QueryParam("accountNumber") String accountNumber, @QueryParam("dateCreated") Calendar dateCreated, @QueryParam("endDate") Calendar endDate,
			@QueryParam("siteId") int siteId, @QueryParam("siteName") String siteName, @QueryParam("startDate") Calendar startDate, @QueryParam("uprn") BigDecimal uprn,
			@Context HttpServletRequest request) throws Exception {

		Sites_GetResponse response = bartecRequestService.getSites(getCompanyId(request), accountNumber, dateCreated, endDate, siteId, siteName, startDate, uprn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Streets_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getStreets(@QueryParam("usrn") BigDecimal usrn, @QueryParam("locality") String locality, @QueryParam("street") String street, @QueryParam("town") String town,
			@Context HttpServletRequest request) throws Exception {

		Streets_GetResponse response = bartecRequestService.getStreets(getCompanyId(request), usrn, locality, street, town);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Streets_Attributes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getStreetsAttributes(@QueryParam("usrn") BigDecimal usrn, @Context HttpServletRequest request) throws Exception {

		Streets_Attributes_GetResponse response = bartecRequestService.getStreetsAttributes(getCompanyId(request), usrn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Streets_Detail_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getStreetsDetail(@QueryParam("usrn") BigDecimal usrn, @Context HttpServletRequest request) throws Exception {

		Streets_Detail_GetResponse response = bartecRequestService.getStreetsDetail(getCompanyId(request), usrn);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Streets_Events_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getStreetsEvents(@QueryParam("usrn") BigDecimal usrn, @QueryParam("deviceName") String deviceName, @QueryParam("deviceType") String deviceType,
			@QueryParam("endDate") Calendar endDate, @QueryParam("eventBounds") CtMapBox eventBounds, @QueryParam("eventType") String eventType, @QueryParam("startDate") Calendar startDate,
			@Context HttpServletRequest request) throws Exception {

		Streets_Events_GetResponse response = bartecRequestService.getStreetsEvents(getCompanyId(request), usrn, deviceName, deviceType, endDate, eventBounds, eventType, startDate);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Streets_Events_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getStreetsEventsType(@Context HttpServletRequest request) throws Exception {

		Streets_Events_Types_GetResponse response = bartecRequestService.getStreetsEventsType(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/System_ExtendedDataDefinitions_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSystemExtendedDateDefinitions(@Context HttpServletRequest request) throws Exception {

		System_ExtendedDataDefinitions_GetResponse response = bartecRequestService.getSystemExtendedDateDefinitions(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/System_LandTypes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSystemLandTypes(@Context HttpServletRequest request) throws Exception {

		System_LandTypes_GetResponse response = bartecRequestService.getSystemLandTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/System_WasteTypes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSystemWasteTypes(@Context HttpServletRequest request) throws Exception {

		System_WasteTypes_GetResponse response = bartecRequestService.getSystemWasteTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Vehicle_InspectionForms_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getVehicleInspectionForms(@Context HttpServletRequest request) throws Exception {

		Vehicle_InspectionForms_GetResponse response = bartecRequestService.getVehicleInspectionForms(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Vehicle_InspectionResults_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getVehicleInspectionResult(@QueryParam("dateRange") CtDateQuery dateRange, @QueryParam("deviceId") int deviceId, @Context HttpServletRequest request) throws Exception {

		Vehicle_InspectionResults_GetResponse response = bartecRequestService.getVehicleInspectionResult(getCompanyId(request), dateRange, deviceId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Vehicles_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getVehicles(@Context HttpServletRequest request) throws Exception {

		Vehicles_GetResponse response = bartecRequestService.getVehicles(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/WorkGroups_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorkGroups(@Context HttpServletRequest request) throws Exception {

		WorkGroups_GetResponse response = bartecRequestService.getWorkGroups(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/WorkPacks_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorkPacks(@QueryParam("crewId") int crewId, @QueryParam("date") CtDateQuery date, @QueryParam("workGroupId") int workGroupId, @Context HttpServletRequest request)
			throws Exception {

		WorkPacks_GetResponse response = bartecRequestService.getWorkPacks(getCompanyId(request), crewId, date, workGroupId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/WorkPacks_Metrics_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorkPacksMetrics(@QueryParam("workPacks") ArrayOfInt workPacks, @Context HttpServletRequest request) throws Exception {

		Workpacks_Metrics_GetResponse response = bartecRequestService.getWorkPacksMetrics(getCompanyId(request), workPacks);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Workpacks_Notes_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorkPacksNotes(@QueryParam("workpackId") int workpackId, @Context HttpServletRequest request) throws Exception {

		Workpacks_Notes_GetResponse response = bartecRequestService.getWorkPacksNotes(getCompanyId(request), workpackId);
		return jsonResponseParserService.toJson(response);
	}

	@GET
	@Path("/Workpacks_Notes_Types_Get")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWorkPacksNotesTypes(@Context HttpServletRequest request) throws Exception {

		Workpacks_Notes_Types_GetResponse response = bartecRequestService.getWorkPacksNotesTypes(getCompanyId(request));
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Appointment_Slot_Reserve")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String reserveServiceRequestAppointmentSlot(@FormParam("reservationId") int reservationId, @FormParam("appointmentIndex") int appointmentIndex, @FormParam("slotId") int slotId,
			@FormParam("workpackId") int workpackId, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_Appointment_Slot_ReserveResponse response = bartecRequestService.reserveServiceRequestAppointmentSlot(getCompanyId(request), reservationId, appointmentIndex, slotId,
				workpackId);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Status_Set")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String setServiceRequestStatus(@FormParam("serviceRequestId") int serviceRequestId, @FormParam("serviceCode") String serviceCode, @FormParam("statusId") int statusId,
			@FormParam("comments") String comments, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_Status_SetResponse response = bartecRequestService.setServiceRequestStatus(getCompanyId(request), serviceRequestId, serviceCode, statusId, comments);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/Premises_Attribute_Update")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updatePremisesAttribute(@FormParam("attributeId") int attributeId, @FormParam("attributeValue") String attributeValue, @FormParam("comments") String comments,
			@FormParam("expiryDate") Calendar expiryDate, @FormParam("reviewDate") Calendar reviewDate, @Context HttpServletRequest request) throws Exception {

		Premises_Attribute_UpdateResponse response = bartecRequestService.updatePremisesAttribute(getCompanyId(request), attributeId, attributeValue, comments, expiryDate, reviewDate);
		return jsonResponseParserService.toJson(response);
	}

	@POST
	@Path("/ServiceRequest_Update")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateServiceRequest(@FormParam("srId") int srId, @FormParam("serviceCode") String serviceCode, @Context HttpServletRequest request) throws Exception {

		ServiceRequest_UpdateResponse response = bartecRequestService.updateServiceRequest(getCompanyId(request), srId, serviceCode);
		return jsonResponseParserService.toJson(response);
	}

}
