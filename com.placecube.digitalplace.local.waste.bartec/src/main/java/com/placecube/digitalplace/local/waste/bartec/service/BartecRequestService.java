package com.placecube.digitalplace.local.waste.bartec.service;

import java.math.BigDecimal;
import java.util.Calendar;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.local.waste.bartec.axis.*;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtAttachedDocument;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtPointMetric;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterBusiness;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact;
import com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration;

@Component(immediate = true, service = BartecRequestService.class)
public class BartecRequestService {

	@Reference
	private BartecAxisService bartecAxisService;

	@Reference
	private BartecWasteService bartecWasteService;

	@Reference
	private ObjectFactoryService objectFactoryService;

	public ServiceRequest_CancelResponse cancelServiceRequest(long companyId, int serviceRequestId, String serviceCode) throws Exception {

		ServiceRequest_Cancel input = objectFactoryService.cancelServiceRequestInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceCode(serviceCode);
		input.setServiceRequestID(serviceRequestId);

		return getBartecService(companyId).serviceRequest_Cancel(input);
	}

	public ServiceRequest_Appointment_Reservation_CancelResponse cancelServiceRequestAppointment(long companyId, int reservationId) throws Exception {

		ServiceRequest_Appointment_Reservation_Cancel input = objectFactoryService.cancelServiceRequestAppointmentInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setReservationID(reservationId);

		return getBartecService(companyId).serviceRequest_Appointment_Reservation_Cancel(input);
	}

	public Jobs_CloseResponse closeJobs(long companyId, int jobId, boolean complete, boolean partDone, boolean noFurtherAction, String reasonText, String closingComments, String outcome,
			Calendar dateClosed) throws Exception {

		Jobs_Close input = objectFactoryService.getJobCloseInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setJobID(jobId);
		input.setComplete(complete);
		input.setPartDone(partDone);
		input.setNoFurtherAction(noFurtherAction);
		input.setReasonText(reasonText);
		input.setClosingComments(closingComments);
		input.setOutcome(outcome);
		input.setDateClosed(dateClosed);

		return getBartecService(companyId).jobs_Close(input);
	}

	public Business_Document_CreateResponse createBusinessDocument(long companyId, int accountId, CtAttachedDocument attachedDocuemnt, Calendar dateTaken, String comment) throws Exception {

		Business_Document_Create input = objectFactoryService.createBusinessDocumentInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAccountID(accountId);
		input.setAttachedDocument(attachedDocuemnt);
		input.setComment(comment);
		input.setDateTaken(dateTaken);

		return getBartecService(companyId).business_Document_Create(input);
	}

	public Premises_Attribute_CreateResponse createPremisesAttribute(long companyId, int attributeId, String attribuetValue, String comments, int confirmation, Calendar expiryDate,
			Calendar reviewDate, BigDecimal uprn) throws Exception {

		Premises_Attribute_Create input = objectFactoryService.createPremisesAttributeInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAttributeID(attributeId);
		input.setAttributeValue(attribuetValue);
		input.setComments(comments);
		input.setConfirmation(confirmation);
		input.setExpiryDate(expiryDate);
		input.setReviewDate(reviewDate);
		input.setUPRN(uprn);

		return getBartecService(companyId).premises_Attribute_Create(input);
	}

	public Premises_Document_CreateResponse createPremisesDocument(long companyId, CtAttachedDocument attachedDocument, String comment, Calendar dateTaken, BigDecimal uprn) throws Exception {

		Premises_Document_Create input = objectFactoryService.createPremisesDocumentInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAttachedDocument(attachedDocument);
		input.setComment(comment);
		input.setDateTaken(dateTaken);
		input.setUPRN(uprn);

		return getBartecService(companyId).premises_Document_Create(input);
	}

	public Premises_Event_CreateResponse createPremisesEvent(long companyId, BigDecimal uprn, String comments, int crewId, int deviceId, Calendar eventDate, CtPointMetric eventLocation, int eventType,
			ArrayOfString featureComments, ArrayOfInt featureIds, int jobId, ArrayOfString subEventComments) throws Exception {

		Premises_Event_Create input = objectFactoryService.createPremisesEventInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setComments(comments);
		input.setCrewID(crewId);
		input.setDeviceID(deviceId);
		input.setEventDate(eventDate);
		input.setEventLocation(eventLocation);
		input.setEventTypeID(eventType);
		input.setFeatureComments(featureComments);
		input.setFeatureIDs(featureIds);
		input.setJobID(jobId);
		input.setSubEventComments(subEventComments);
		input.setUPRN(uprn);

		return getBartecService(companyId).premises_Event_Create(input);
	}

	public Premises_Events_Document_CreateResponse createPremisesEventsDocument(long companyId, CtAttachedDocument attachedDocument, String comment, Calendar dateTaken, int eventId) throws Exception {

		Premises_Events_Document_Create input = objectFactoryService.createPremisesEventsDocumentInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAttachedDocument(attachedDocument);
		input.setComment(comment);
		input.setDateTaken(dateTaken);
		input.setEventID(eventId);

		return getBartecService(companyId).premises_Events_Document_Create(input);
	}

	public ServiceRequest_CreateResponse createServiceRequest(long companyId, int appointementReservationId, ArrayOfServiceRequest_CreateServiceRequest_CreateFields extendedData, int crewId,
			Calendar dateRequested, ArrayOfServiceRequest_CreateServiceRequest_CreateLocation relatedPremises, int landTypeId,
			ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest relatedServiceRequests, ServiceRequest_CreateServiceRequest_CreateReporterBusiness reporterBusiness,
			String serviceLocationDescription, ServiceRequest_CreateServiceRequest_CreateReporterContact reporterContact, CtMapPoint_Set serviceRequestLocation, int serviceTypeId, int serviceStatusId,
			int slaId, String source, BigDecimal uprn, String externalRef) throws Exception {

		ServiceRequest_Create input = objectFactoryService.createServiceRequestInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAppointmentReservationID(appointementReservationId);
		input.setExternalReference(externalRef);
		input.setExtendedData(extendedData);
		input.setCrewID(crewId);
		input.setDateRequested(dateRequested);
		input.setLandTypeID(landTypeId);
		input.setRelatedPremises(relatedPremises);
		input.setRelatedServiceRequests(relatedServiceRequests);
		input.setReporterBusiness(reporterBusiness);
		input.setReporterContact(reporterContact);
		input.setServiceLocationDescription(serviceLocationDescription);
		input.setServiceRequest_Location(serviceRequestLocation);
		input.setServiceStatusID(serviceStatusId);
		input.setServiceTypeID(serviceTypeId);
		input.setSLAID(slaId);
		input.setSource(source);
		input.setUPRN(uprn);

		return getBartecService(companyId).serviceRequest_Create(input);
	}

	public ServiceRequest_Appointment_Reservation_CreateResponse createServiceRequestAppointmentReservation(long companyId, int serviceTypeId, int slotId, int workPackId) throws Exception {

		ServiceRequest_Appointment_Reservation_Create input = objectFactoryService.createServiceRequestAppointmentReservationInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceTypeID(serviceTypeId);
		input.setSlotID(slotId);
		input.setWorkPackID(workPackId);
		return getBartecService(companyId).serviceRequest_Appointment_Reservation_Create(input);
	}

	public ServiceRequest_Document_CreateResponse createServiceRequestDocument(long companyId, CtAttachedDocument attachedDocument, String comment, Calendar dateTaken, boolean srPublic,
			int serviceRequestId) throws Exception {

		ServiceRequest_Document_Create input = objectFactoryService.createServiceRequestDocumentInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAttachedDocument(attachedDocument);
		input.setComment(comment);
		input.setDateTaken(dateTaken);
		input.setPublic(srPublic);
		input.setServiceRequestID(serviceRequestId);

		return getBartecService(companyId).serviceRequest_Document_Create(input);
	}

	public ServiceRequest_Note_CreateResponse createServiceRequestNote(long companyId, int serviceRequestId, String serviceCode, String note, String comment, int noteType, int sequenceNumber)
			throws Exception {

		ServiceRequest_Note_Create input = objectFactoryService.createServiceRequestNoteInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceCode(serviceCode);
		input.setNote(note);
		input.setComment(comment);
		input.setNoteTypeID(noteType);
		input.setSequenceNumber(sequenceNumber);
		input.setServiceRequestID(serviceRequestId);

		return getBartecService(companyId).serviceRequest_Note_Create(input);
	}

	public Site_Document_CreateResponse createSiteDocument(long companyId, CtAttachedDocument attachedDocument, String comment, Calendar dateTaken, int siteId) throws Exception {

		Site_Document_Create input = objectFactoryService.createSiteDocumentInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAttachedDocument(attachedDocument);
		input.setComment(comment);
		input.setDateTaken(dateTaken);
		input.setSiteID(siteId);

		return getBartecService(companyId).site_Document_Create(input);
	}

	public Vehicle_Message_CreateResponse createVehicleMessage(long companyId, int deviceId, String messageTax, String subject) throws Exception {

		Vehicle_Message_Create input = objectFactoryService.createVehicleMessageInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDeviceID(deviceId);
		input.setMessageText(messageTax);
		input.setSubject(subject);

		return getBartecService(companyId).vehicle_Message_Create(input);
	}

	public Workpack_Note_CreateResponse createWorkPackNote(long companyId, String comment, String note, String subject, int noteTypeId, int workPackId) throws Exception {

		Workpack_Note_Create input = objectFactoryService.createWorkPackNoteInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setComment(comment);
		input.setNote(note);
		input.setSubject(subject);
		input.setNoteTypeID(noteTypeId);
		input.setWorkpackID(workPackId);

		return getBartecService(companyId).workpack_Note_Create(input);
	}

	public Premises_Attributes_DeleteResponse detetePremisesAttributes(long companyId, int attributeId, BigDecimal uprn) throws Exception {

		Premises_Attributes_Delete input = objectFactoryService.detetePremisesAttributesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAttributeID(attributeId);
		input.setUPRN(uprn);

		return getBartecService(companyId).premises_Attributes_Delete(input);
	}

	public ServiceRequest_Appointment_Reservation_ExtendResponse extendServiceRequestAppointmentReservation(long companyId, int reservationId) throws Exception {

		ServiceRequest_Appointment_Reservation_Extend input = objectFactoryService.extendServiceRequestAppointmentReservationInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setReservationID(reservationId);
		return getBartecService(companyId).serviceRequest_Appointment_Reservation_Extend(input);
	}

	public String getAuthToken(long companyId) throws Exception {

		BartecConfiguration configuration = bartecWasteService.getConfiguration(companyId);

		return bartecAxisService.authenticate(configuration.username(), configuration.password());
	}

	private CollectiveAPIWebService getBartecService(long companyId) throws Exception {

		return bartecAxisService.getBartecAxisService(companyId);
	}

	public Businesses_GetResponse getBusiness(long companyId, String accountNumber, String accountName, String businessName, String businessDescription, ArrayOfInt statuses, ArrayOfInt subStatuses,
			ArrayOfInt businessTypes, ArrayOfInt businessClasses, Calendar dateCreated) throws Exception {

		Businesses_Get input = objectFactoryService.getBusinessInput();

		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAccountName(accountName);
		input.setAccountNumber(accountNumber);
		input.setBusinessName(businessName);
		input.setBusinessDescription(businessDescription);
		input.setStatuses(statuses);
		input.setSubStatuses(subStatuses);
		input.setBusinessTypes(businessTypes);
		input.setBusinessClasses(businessClasses);
		input.setDateCreated(dateCreated);

		return getBartecService(companyId).businesses_Get(input);
	}

	public Businesses_Classes_GetResponse getBusinessClasses(long companyId) throws Exception {

		Businesses_Classes_Get input = objectFactoryService.getBusinessClassesInput();

		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).businesses_Classes_Get(input);
	}

	public Businesses_Documents_GetResponse getBusinessDocuments(long companyId, int documentId) throws Exception {

		Businesses_Documents_Get input = objectFactoryService.getBusinessDocumentsInput();

		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).businesses_Documents_Get(input);
	}

	public Businesses_Statuses_GetResponse getBusinessStatuses(long companyId) throws Exception {

		Businesses_Statuses_Get input = objectFactoryService.getBusinessStatusesInput();

		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).businesses_Statuses_Get(input);
	}

	public Businesses_SubStatuses_GetResponse getBusinessSubStatuses(long companyId) throws Exception {

		Businesses_SubStatuses_Get input = objectFactoryService.getBusinessSubStatusesInput();

		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).businesses_SubStatuses_Get(input);
	}

	public Businesses_Types_GetResponse getBusinessTypes(long companyId) throws Exception {

		Businesses_Types_Get input = objectFactoryService.getBusinessesTypesInput();

		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).businesses_Types_Get(input);
	}

	public Cases_Documents_GetResponse getCaseDocuments(long companyId, int documentId) throws Exception {

		Cases_Documents_Get input = objectFactoryService.getCaseDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).cases_Documents_Get(input);
	}

	public Crews_GetResponse getCrews(long companyId) throws Exception {

		Crews_Get input = objectFactoryService.getCrewsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).crews_Get(input);
	}

	public Features_Categories_GetResponse getFeatureCategories(long companyId) throws Exception {

		Features_Categories_Get input = objectFactoryService.getFeaturesCategoryInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).features_Categories_Get(input);
	}

	public Features_Classes_GetResponse getFeatureClasses(long companyId) throws Exception {

		Features_Classes_Get input = objectFactoryService.getFeaturesClassesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).features_Classes_Get(input);
	}

	public Features_Colours_GetResponse getFeatureColours(long companyId) throws Exception {

		Features_Colours_Get input = objectFactoryService.getFeatureColoursInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).features_Colours_Get(input);
	}

	public Features_Conditions_GetResponse getFeatureConditions(long companyId) throws Exception {

		Features_Conditions_Get input = objectFactoryService.getFeatureConditionsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).features_Conditions_Get(input);
	}

	public Features_Manufacturers_GetResponse getFeatureManufacturers(long companyId) throws Exception {

		Features_Manufacturers_Get input = objectFactoryService.getFeatureManufacturersInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).features_Manufacturers_Get(input);
	}

	public Features_Notes_GetResponse getFeatureNotes(long companyId, int featureId) throws Exception {

		Features_Notes_Get input = objectFactoryService.getFeatureNotesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setFeatureID(featureId);

		return getBartecService(companyId).features_Notes_Get(input);
	}

	public Features_GetResponse getFeatures(long companyId, String name, String description, String serialNumber, String assetTag, String rfidTag, BigDecimal uprn, ArrayOfInt types,
			ArrayOfInt statuses, ArrayOfInt manufacturers, ArrayOfInt colours, ArrayOfInt conditions, ArrayOfInt wasteType) throws Exception {

		Features_Get input = objectFactoryService.getFeatuersInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setName(name);
		input.setDescription(description);
		input.setSerialNumber(serialNumber);
		input.setAssetTag(assetTag);
		input.setRFIDTag(rfidTag);
		input.setUPRN(uprn);
		input.setTypes(types);
		input.setStatuses(statuses);
		input.setManufacturers(manufacturers);
		input.setColours(colours);
		input.setConditions(conditions);
		input.setWasteTypes(wasteType);

		return getBartecService(companyId).features_Get(input);
	}

	public Features_Schedules_GetResponse getFeaturesSchedules(long companyId, BigDecimal uprn, ArrayOfInt types) throws Exception {

		Features_Schedules_Get input = objectFactoryService.getFeaturesSchedulesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);
		input.setTypes(types);
		input.setFeatureId(Integer.MIN_VALUE);

		return getBartecService(companyId).features_Schedules_Get(input);
	}

	public Features_Statuses_GetResponse getFeatureStatuses(long companyId) throws Exception {

		Features_Statuses_Get input = objectFactoryService.getFeatureStatusesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).features_Statuses_Get(input);
	}

	public Features_Types_GetResponse getFeatureTypes(long companyId) throws Exception {

		Features_Types_Get input = objectFactoryService.getFeatureTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).features_Types_Get(input);
	}

	public Inspections_GetResponse getInspection(long companyId, int siteID, String siteName, BigDecimal uprn, ArrayOfInt inspectionTypes, ArrayOfInt inspectionStatuses, int featureId)
			throws Exception {

		Inspections_Get input = objectFactoryService.getInspectionsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setSiteID(siteID);
		input.setSiteName(siteName);
		input.setUPRN(uprn);
		input.setInspectionTypes(inspectionTypes);
		input.setInspectionSatuses(inspectionStatuses);
		input.setFeatureID(featureId);

		return getBartecService(companyId).inspections_Get(input);
	}

	public Inspections_Classes_GetResponse getInspectionClasses(long companyId) throws Exception {

		Inspections_Classes_Get input = objectFactoryService.getInspectionClassesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).inspections_Classes_Get(input);
	}

	public Inspections_Documents_GetResponse getInspectionDocuments(long companyId, int documentId) throws Exception {

		Inspections_Documents_Get input = objectFactoryService.getInspectionDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).inspections_Documents_Get(input);
	}

	public Inspections_Statuses_GetResponse getInspectionStatuses(long companyId) throws Exception {

		Inspections_Statuses_Get input = objectFactoryService.getInspectionsStatusesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).inspections_Statuses_Get(input);
	}

	public Inspections_Types_GetResponse getInspectionTypes(long companyId) throws Exception {

		Inspections_Types_Get input = objectFactoryService.getInspectionsTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).inspections_Types_Get(input);
	}

	public Jobs_Detail_GetResponse getJobDetail(long companyId, int jobId) throws Exception {

		Jobs_Detail_Get input = objectFactoryService.getJobDetailInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setJobID(jobId);

		return getBartecService(companyId).jobs_Detail_Get(input);
	}

	public Jobs_Documents_GetResponse getJobDocuments(long companyId, int documentId) throws Exception {

		Jobs_Documents_Get input = objectFactoryService.getJobDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).jobs_Documents_Get(input);
	}

	public Jobs_FeatureScheduleDates_GetResponse getJobFeatureScheduleDates(long companyId, BigDecimal uprn, CtDateQuery dateRange, CtMapBox jobBounds) throws Exception {

		Jobs_FeatureScheduleDates_Get input = objectFactoryService.getJobFeatureScheduleDatesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);
		input.setJobs_Bounds(jobBounds);
		input.setDateRange(dateRange);

		return getBartecService(companyId).jobs_FeatureScheduleDates_Get(input);
	}

	public Jobs_GetResponse getJobs(long companyId, BigDecimal uprn, BigDecimal usrn, int workPackId, CtDateQuery scheduledStart, int serviceRequestID, int wardId, int parishId, CtMapBox jobsBound,
			boolean includeRelated) throws Exception {

		Jobs_Get input = objectFactoryService.getJobsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);
		input.setUSRN(usrn);
		input.setWorkPackID(workPackId);
		input.setParishID(parishId);
		input.setScheduleStart(scheduledStart);
		input.setServiceRequestID(serviceRequestID);
		input.setWardID(wardId);
		input.setJobs_Bounds(jobsBound);
		input.setIncludeRelated(includeRelated);

		return getBartecService(companyId).jobs_Get(input);
	}

	public Jobs_WorkScheduleDates_GetResponse getJobsWorkScheduleDates(long companyId, BigDecimal uprn, CtDateQuery dateRange, CtMapBox jobsBound) throws Exception {

		Jobs_WorkScheduleDates_Get input = objectFactoryService.getJobsWorkScheduleDatesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);
		input.setDateRange(dateRange);
		input.setJobs_Bounds(jobsBound);

		return getBartecService(companyId).jobs_WorkScheduleDates_Get(input);
	}

	public Licences_GetResponse getLicences(long companyId, int siteId, String siteName, String accountNumber, ArrayOfDecimal uprns, ArrayOfInt licenceTypes, ArrayOfInt licenceStatuses,
			int serviceRequestId, CtDateQuery startDate, CtDateQuery issueDate, CtDateQuery dateCreated) throws Exception {

		Licences_Get input = objectFactoryService.getLicencesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setSiteID(siteId);
		input.setSiteName(siteName);
		input.setAccountNumber(accountNumber);
		input.setUPRNs(uprns);
		input.setLicenceTypes(licenceTypes);
		input.setLicenceStatuses(licenceStatuses);
		input.setServiceRequestID(serviceRequestId);
		input.setStartDate(startDate);
		input.setIssueDate(issueDate);
		input.setDateCreated(dateCreated);

		return getBartecService(companyId).licences_Get(input);
	}

	public Licences_Classes_GetResponse getLicencesClasses(long companyId) throws Exception {

		Licences_Classes_Get input = objectFactoryService.getLicencesClassesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).licences_Classes_Get(input);
	}

	public Licences_Documents_GetResponse getLicencesDocuments(long companyId, int documentId) throws Exception {

		Licences_Documents_Get input = objectFactoryService.getLicencesDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).licences_Documents_Get(input);
	}

	public Licences_Statuses_GetResponse getLicencesStatuses(long companyId) throws Exception {

		Licences_Statuses_Get input = objectFactoryService.getLicencesStatusesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).licences_Statuses_Get(input);
	}

	public Licences_Types_GetResponse getLicencesTypes(long companyId) throws Exception {

		Licences_Types_Get input = objectFactoryService.getLicencesTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).licences_Types_Get(input);
	}

	public Premises_GetResponse getPremises(long companyId, BigDecimal uprn, String address1, String address2, String street, String locality, String town, String postCode, String wardName,
			String parishName, String userLabel, BigDecimal parentUprn, CtMapBox bounds, String blpuClass, ArrayOfString blpulStat, BigDecimal usrn) throws Exception {

		Premises_Get input = objectFactoryService.getPremisesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);
		input.setUSRN(usrn);
		input.setAddress1(address1);
		input.setAddress2(address2);
		input.setWardName(wardName);
		input.setParishName(parishName);
		input.setStreet(street);
		input.setLocality(locality);
		input.setTown(town);
		input.setPostcode(postCode);
		input.setUserLabel(userLabel);
		input.setParentUPRN(parentUprn);
		input.setBounds(bounds);
		input.setBLPUClass(blpuClass);
		input.setBLPULStat(blpulStat);

		return getBartecService(companyId).premises_Get(input);
	}

	public Premises_AttributeDefinitions_GetResponse getPremisesAttributeDefinitions(long companyId) throws Exception {

		Premises_AttributeDefinitions_Get input = objectFactoryService.getPremisesAttributeDefinitions();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).premises_AttributeDefinitions_Get(input);
	}

	public Premises_Attributes_GetResponse getPremisesAttributes(long companyId, BigDecimal uprn) throws Exception {

		Premises_Attributes_Get input = objectFactoryService.getPremisesAttributesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);

		return getBartecService(companyId).premises_Attributes_Get(input);
	}

	public Premises_Detail_GetResponse getPremisesDetails(long companyId, BigDecimal uprn) throws Exception {

		Premises_Detail_Get input = objectFactoryService.getPremisesDetailsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);

		return getBartecService(companyId).premises_Detail_Get(input);
	}

	public Premises_Documents_GetResponse getPremisesDocuments(long companyId, int documentId) throws Exception {

		Premises_Documents_Get input = objectFactoryService.getPremisesDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).premises_Documents_Get(input);
	}

	public Premises_Events_GetResponse getPremisesEvents(long companyId, BigDecimal uprn, CtDateQuery dateRange, String postCode, String wardName, String parishName, CtMapBox premisesBound,
			CtMapBox eventBounds, String eventType, String eventSubType, BigDecimal minDistace, BigDecimal maxDistance, String deviceType, String deviceName, BigDecimal usrn, String workPack,
			boolean includeRelated) throws Exception {

		Premises_Events_Get input = objectFactoryService.getPremisesEventsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);
		input.setUSRN(usrn);
		input.setDateRange(dateRange);
		input.setPostcode(postCode);
		input.setWardName(wardName);
		input.setParishName(parishName);
		input.setPremises_Bounds(premisesBound);
		input.setEvents_Bounds(eventBounds);
		input.setEventType(eventType);
		input.setEventSubType(eventSubType);
		input.setMinDistance(minDistace);
		input.setMaxDistance(maxDistance);
		input.setDeviceType(deviceType);
		input.setDeviceName(deviceName);
		input.setWorkPack(workPack);
		input.setIncludeRelated(includeRelated);

		return getBartecService(companyId).premises_Events_Get(input);
	}

	public Premises_Events_Classes_GetResponse getPremisesEventsClasses(long companyId) throws Exception {

		Premises_Events_Classes_Get input = objectFactoryService.getPremisesEventsClassesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).premises_Events_Classes_Get(input);
	}

	public Premises_Events_Documents_GetResponse getPremisesEventsDocuments(long companyId, int documentId) throws Exception {

		Premises_Events_Documents_Get input = objectFactoryService.getPremisesEventsDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).premises_Events_Documents_Get(input);
	}

	public Premises_Events_Types_GetResponse getPremisesEventsTypes(long companyId) throws Exception {

		Premises_Events_Types_Get input = objectFactoryService.getPremisesEventsTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).premises_Events_Types_Get(input);
	}

	public Premises_FutureWorkpacks_GetResponse getPremisesFutureWorkpacks(long companyId, BigDecimal uprn, int featureTypeId, int actionTypeId, int featureStatusId, Calendar dateFrom,
			Calendar dateTo) throws Exception {

		Premises_FutureWorkpacks_Get input = objectFactoryService.getPremisesFutureWorkpacksInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUPRN(uprn);
		input.setFeatureTypeID(featureTypeId);
		input.setActionTypeID(actionTypeId);
		input.setFeatureStatusID(featureStatusId);
		input.setDateFrom(dateFrom);
		input.setDateTo(dateTo);

		return getBartecService(companyId).premises_FutureWorkpacks_Get(input);
	}

	public Resources_GetResponse getResources(long companyId, int resourceType, String assetTag, String serialNumber) throws Exception {

		Resources_Get input = objectFactoryService.getResourcesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setResourceType(resourceType);
		input.setAssetTag(assetTag);
		input.setSerialNumber(serialNumber);

		return getBartecService(companyId).resources_Get(input);
	}

	public Resources_Documents_GetResponse getResourcesDocuments(long companyId, int documentId) throws Exception {

		Resources_Documents_Get input = objectFactoryService.getResourcesDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).resources_Documents_Get(input);
	}

	public Resources_Types_GetResponse getResourcesType(long companyId) throws Exception {

		Resources_Types_Get input = objectFactoryService.getResourcesTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).resources_Types_Get(input);
	}

	public ServiceRequests_GetResponse getServiceRequests(long companyId, String contactEmail, String serviceCode, String externalRef, ArrayOfDecimal uprns, String contactName, String contactPhoneNo,
			CtMapBox premisesBounds, CtMapBox requestBounds, CtDateQuery requestDate, ArrayOfInt serviceTypes, ArrayOfInt landTypes, ArrayOfInt serviceStatuses, ArrayOfInt crews, String slaStatus,
			CtDateQuery slaBreachTime) throws Exception {

		ServiceRequests_Get input = objectFactoryService.getServiceRequestsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setContactEmail(contactEmail);
		input.setServiceCode(serviceCode);
		input.setExternalRef(externalRef);
		input.setUPRNs(uprns);
		input.setContactName(contactName);
		input.setContactPhoneNo(contactPhoneNo);
		input.setPremisesBounds(premisesBounds);
		input.setRequestBounds(requestBounds);
		input.setRequestDate(requestDate);
		input.setServiceTypes(serviceTypes);
		input.setLandTypes(landTypes);
		input.setServiceStatuses(serviceStatuses);
		input.setCrews(crews);
		input.setSLAStatus(slaStatus);
		input.setSLABreachTime(slaBreachTime);

		return getBartecService(companyId).serviceRequests_Get(input);
	}

	public ServiceRequests_Appointments_Availability_GetResponse getServiceRequestsAppoitmentsAvailability(long companyId, int serviceTypeId, int appointmentIndex, Calendar startDate)
			throws Exception {

		ServiceRequests_Appointments_Availability_Get input = objectFactoryService.getServiceRequestsAppoitmentsAvailabilityInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAppointmentIndex(appointmentIndex);
		input.setServiceTypeID(serviceTypeId);
		input.setStartDate(startDate);

		return getBartecService(companyId).serviceRequests_Appointments_Availability_Get(input);
	}

	public ServiceRequests_Classes_GetResponse getServiceRequestsClasses(long companyId) throws Exception {

		ServiceRequests_Classes_Get input = objectFactoryService.getServiceRequestsClassesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).serviceRequests_Classes_Get(input);
	}

	public ServiceRequests_Detail_GetResponse getServiceRequestsDetails(long companyId, ArrayOfInt serviceRequestIds, ArrayOfString serviceCodes) throws Exception {

		ServiceRequests_Detail_Get input = objectFactoryService.getServiceRequestsDetailsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceRequestIDs(serviceRequestIds);
		input.setServiceCodes(serviceCodes);

		return getBartecService(companyId).serviceRequests_Detail_Get(input);
	}

	public ServiceRequests_Documents_GetResponse getServiceRequestsDocuments(long companyId, int documentId) throws Exception {

		ServiceRequests_Documents_Get input = objectFactoryService.getServiceRequestsDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).serviceRequests_Documents_Get(input);
	}

	public ServiceRequests_History_GetResponse getServiceRequestsHistory(long companyId, int serviceRequestId, int id, Calendar date) throws Exception {

		ServiceRequests_History_Get input = objectFactoryService.getServiceRequestsHistoryInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceRequestID(serviceRequestId);
		input.setID(id);
		input.setDate(date);

		return getBartecService(companyId).serviceRequests_History_Get(input);
	}

	public ServiceRequests_Notes_GetResponse getServiceRequestsNotes(long companyId, int serviceRequestId) throws Exception {

		ServiceRequests_Notes_Get input = objectFactoryService.getServiceRequestsNotesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceRequestID(serviceRequestId);

		return getBartecService(companyId).serviceRequests_Notes_Get(input);
	}

	public ServiceRequests_Notes_Types_GetResponse getServiceRequestsNotesTypes(long companyId) throws Exception {

		ServiceRequests_Notes_Types_Get input = objectFactoryService.getServiceRequestsNotesTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).serviceRequests_Notes_Types_Get(input);
	}

	public ServiceRequests_SLAs_GetResponse getServiceRequestsSLAs(long companyId) throws Exception {

		ServiceRequests_SLAs_Get input = objectFactoryService.getServiceRequestsSLAsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).serviceRequests_SLAs_Get(input);
	}

	public ServiceRequests_Statuses_GetResponse getServiceRequestsStatuses(long companyId, int serviceTypeId) throws Exception {

		ServiceRequests_Statuses_Get input = objectFactoryService.getServiceRequestsStatusesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceTypeID(serviceTypeId);

		return getBartecService(companyId).serviceRequests_Statuses_Get(input);

	}

	public ServiceRequests_Types_GetResponse getServiceRequestsTypes(long companyId, int serviceclassId) throws Exception {

		ServiceRequests_Types_Get input = objectFactoryService.getServiceRequestsTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceClassID(serviceclassId);

		return getBartecService(companyId).serviceRequests_Types_Get(input);

	}

	public ServiceRequests_Updates_GetResponse getServiceRequestsUpdates(long companyId, String serviceCode, Calendar lastUpdated) throws Exception {

		ServiceRequests_Updates_Get input = objectFactoryService.getServiceRequestsUpdatesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceCode(serviceCode);
		input.setLastUpdated(lastUpdated);

		return getBartecService(companyId).serviceRequests_Updates_Get(input);

	}

	public Sites_Documents_GetResponse getSiteDocuments(long companyId, int documentId) throws Exception {

		Sites_Documents_Get input = objectFactoryService.getSiteDocumentsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDocumentID(documentId);

		return getBartecService(companyId).sites_Documents_Get(input);

	}

	public Sites_Documents_GetAllResponse getSiteDocumentsAll(long companyId, CtDateQuery dateRange, String documentType, ArrayOfInt sites) throws Exception {

		Sites_Documents_GetAll input = objectFactoryService.getSiteDocumentsAllInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDateRange(dateRange);
		input.setDocumentType(documentType);
		input.setSites(sites);

		return getBartecService(companyId).sites_Documents_GetAll(input);
	}

	public Sites_GetResponse getSites(long companyId, String accountNumber, Calendar dateCreated, Calendar endDate, int siteId, String siteName, Calendar startDate, BigDecimal uprn) throws Exception {

		Sites_Get input = objectFactoryService.getSitesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAccountNumber(accountNumber);
		input.setDateCreated(dateCreated);
		input.setEndDate(endDate);
		input.setSiteID(siteId);
		input.setSiteName(siteName);
		input.setStartDate(startDate);
		input.setUPRN(uprn);

		return getBartecService(companyId).sites_Get(input);
	}

	public Streets_GetResponse getStreets(long companyId, BigDecimal usrn, String locality, String street, String town) throws Exception {

		Streets_Get input = objectFactoryService.getStreetsGetInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setLocality(locality);
		input.setStreet(street);
		input.setTown(town);
		input.setUSRN(usrn);

		return getBartecService(companyId).streets_Get(input);
	}

	public Streets_Attributes_GetResponse getStreetsAttributes(long companyId, BigDecimal usrn) throws Exception {

		Streets_Attributes_Get input = objectFactoryService.getStreetsAttributesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUSRN(usrn);

		return getBartecService(companyId).streets_Attributes_Get(input);
	}

	public Streets_Detail_GetResponse getStreetsDetail(long companyId, BigDecimal usrn) throws Exception {

		Streets_Detail_Get input = objectFactoryService.getStreetsDetailInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUSRN(usrn);

		return getBartecService(companyId).streets_Detail_Get(input);
	}

	public Streets_Events_GetResponse getStreetsEvents(long companyId, BigDecimal usrn, String deviceName, String deviceType, Calendar endDate, CtMapBox eventBounds, String eventType,
			Calendar startDate) throws Exception {

		Streets_Events_Get input = objectFactoryService.getStreetsEventsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setUSRN(usrn);
		input.setDeviceName(deviceName);
		input.setDeviceType(deviceType);
		input.setEndDate(endDate);
		input.setEvent_Bounds(eventBounds);
		input.setEventType(eventType);
		input.setStartDate(startDate);

		return getBartecService(companyId).streets_Events_Get(input);
	}

	public Streets_Events_Types_GetResponse getStreetsEventsType(long companyId) throws Exception {

		Streets_Events_Types_Get input = objectFactoryService.getStreetsEventsTypeInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).streets_Events_Types_Get(input);
	}

	public System_ExtendedDataDefinitions_GetResponse getSystemExtendedDateDefinitions(long companyId) throws Exception {

		System_ExtendedDataDefinitions_Get input = objectFactoryService.getSystemExtendedDateDefinitionsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).system_ExtendedDataDefinitions_Get(input);
	}

	public System_LandTypes_GetResponse getSystemLandTypes(long companyId) throws Exception {

		System_LandTypes_Get input = objectFactoryService.getSystemLandTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).system_LandTypes_Get(input);
	}

	public System_WasteTypes_GetResponse getSystemWasteTypes(long companyId) throws Exception {

		System_WasteTypes_Get input = objectFactoryService.getSystemWasteTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).system_WasteTypes_Get(input);
	}

	public Vehicle_InspectionForms_GetResponse getVehicleInspectionForms(long companyId) throws Exception {

		Vehicle_InspectionForms_Get input = objectFactoryService.getVehicleInspectionFormsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).vehicle_InspectionForms_Get(input);
	}

	public Vehicle_InspectionResults_GetResponse getVehicleInspectionResult(long companyId, CtDateQuery dateRange, int deviceId) throws Exception {

		Vehicle_InspectionResults_Get input = objectFactoryService.getVehicleInspectionResponsesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setDateRange(dateRange);
		input.setDeviceID(deviceId);

		return getBartecService(companyId).vehicle_InspectionResults_Get(input);
	}

	public Vehicles_GetResponse getVehicles(long companyId) throws Exception {

		Vehicles_Get input = objectFactoryService.getVehiclesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).vehicles_Get(input);
	}

	public WorkGroups_GetResponse getWorkGroups(long companyId) throws Exception {

		WorkGroups_Get input = objectFactoryService.getWorkGroups();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).workGroups_Get(input);
	}

	public WorkPacks_GetResponse getWorkPacks(long companyId, int crewId, CtDateQuery date, int workGroupId) throws Exception {

		WorkPacks_Get input = objectFactoryService.getWorkPacksInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setCrewID(crewId);
		input.setDate(date);
		input.setWorkGroupID(workGroupId);

		return getBartecService(companyId).workPacks_Get(input);
	}

	public Workpacks_Metrics_GetResponse getWorkPacksMetrics(long companyId, ArrayOfInt workPacks) throws Exception {

		Workpacks_Metrics_Get input = objectFactoryService.getWorkPacksMetricsInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setWorkpacks(workPacks);

		return getBartecService(companyId).workpacks_Metrics_Get(input);
	}

	public Workpacks_Notes_GetResponse getWorkPacksNotes(long companyId, int workpackId) throws Exception {

		Workpacks_Notes_Get input = objectFactoryService.getWorkPacksNotesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setWorkpackID(workpackId);

		return getBartecService(companyId).workpacks_Notes_Get(input);
	}

	public Workpacks_Notes_Types_GetResponse getWorkPacksNotesTypes(long companyId) throws Exception {

		Workpacks_Notes_Types_Get input = objectFactoryService.getWorkPacksNotesTypesInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);

		return getBartecService(companyId).workpacks_Notes_Types_Get(input);
	}

	public ServiceRequest_Appointment_Slot_ReserveResponse reserveServiceRequestAppointmentSlot(long companyId, int reservationId, int appointmentIndex, int slotId, int workpackId) throws Exception {

		ServiceRequest_Appointment_Slot_Reserve input = objectFactoryService.reserveServiceRequestAppointmentSlotInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setReservationID(reservationId);
		input.setAppointmentIndex(appointmentIndex);
		input.setSlotID(slotId);
		input.setWorkPackID(workpackId);

		return getBartecService(companyId).serviceRequest_Appointment_Slot_Reserve(input);
	}

	public ServiceRequest_Status_SetResponse setServiceRequestStatus(long companyId, int serviceRequestId, String serviceCode, int statusId, String comments) throws Exception {

		ServiceRequest_Status_Set input = objectFactoryService.setServiceRequestStatusInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceCode(serviceCode);
		input.setStatusID(statusId);
		input.setComments(comments);
		input.setServiceRequestID(serviceRequestId);

		return getBartecService(companyId).serviceRequest_Status_Set(input);
	}

	public Premises_Attribute_UpdateResponse updatePremisesAttribute(long companyId, int attributeId, String attributeValue, String comments, Calendar expiryDate, Calendar reviewDate)
			throws Exception {

		Premises_Attribute_Update input = objectFactoryService.updatePremisesAttributeInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setAttributeID(attributeId);
		input.setAttributeValue(attributeValue);
		input.setComments(comments);
		input.setExpiryDate(expiryDate);
		input.setReviewDate(reviewDate);

		return getBartecService(companyId).premises_Attribute_Update(input);
	}

	public ServiceRequest_UpdateResponse updateServiceRequest(long companyId, int srId, String serviceCode) throws Exception {

		ServiceRequest_Update input = objectFactoryService.updateServiceRequestInput();
		String authToken = getAuthToken(companyId);
		input.setToken(authToken);
		input.setServiceCode(serviceCode);
		input.setSR_ID(srId);

		return getBartecService(companyId).serviceRequest_Update(input);
	}

}
