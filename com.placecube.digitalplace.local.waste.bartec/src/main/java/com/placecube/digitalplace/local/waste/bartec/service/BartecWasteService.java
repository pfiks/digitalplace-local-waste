package com.placecube.digitalplace.local.waste.bartec.service;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = BartecWasteService.class)
public class BartecWasteService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public BartecConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(BartecConfiguration.class, companyId);
	}

	public String getbartecEndpointUrl(BartecConfiguration configuration) throws ConfigurationException {
		String url = configuration.bartecEndpointUrl();

		if (Validator.isNull(url)) {
			throw new ConfigurationException("ServiceUnitsForObject Endpoint UR configuration cannot be empty");
		}

		return url;
	}

	public String getUsername(BartecConfiguration configuration) throws ConfigurationException {
		String username = configuration.username();

		if (Validator.isNull(username)) {
			throw new ConfigurationException("Username configuration cannot be empty");
		}

		return username;
	}

	public String getPassword(BartecConfiguration configuration) throws ConfigurationException {
		String password = configuration.password();

		if (Validator.isNull(password)) {
			throw new ConfigurationException("Password configuration cannot be empty");
		}

		return password;
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		BartecConfiguration configuration = configurationProvider.getCompanyConfiguration(BartecConfiguration.class, companyId);
		return configuration.enabled();
	}

}
