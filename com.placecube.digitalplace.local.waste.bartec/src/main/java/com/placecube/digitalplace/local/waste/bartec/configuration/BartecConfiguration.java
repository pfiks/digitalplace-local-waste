package com.placecube.digitalplace.local.waste.bartec.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration", localization = "content/Language", name = "waste-bartec")
public interface BartecConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "https://collapi.bartec-systems.com/CollAuth/Authenticate.asmx", name = "bartec-endpoint-url")
	String bartecEndpointUrl();

	@Meta.AD(required = false, deflt = "", name = "username")
	String username();

	@Meta.AD(required = false, deflt = "", name = "password", type = Meta.Type.Password)
	String password();

	@Meta.AD(required = false, deflt = "234", name = "createGardenWasteSubscriptionServiceTypeId")
	int createGardenWasteSubscriptionServiceTypeId();

	@Meta.AD(required = false, deflt = "2364", name = "createGardenWasteSubscriptionServiceStatusId")
	int createGardenWasteSubscriptionServiceStatusId();

	@Meta.AD(required = false, deflt = "279", name = "createAssistedBinCollectionServiceTypeId")
	int createAssistedBinCollectionServiceTypeId();

	@Meta.AD(required = false, deflt = "2823", name = "createAssistedBinCollectionServiceStatusId")
	int createAssistedBinCollectionServiceStatusId();

	@Meta.AD(required = false, deflt = "PUBLIC", name = "reporterType")
	String reporterType();

	@Meta.AD(required = false, deflt = "COLLECTIVE API", name = "source")
	String source();

	@Meta.AD(required = false, deflt = "19", name = "landTypeId")
	int landTypeId();

	@Meta.AD(required = false, deflt = "35", name = "SLAID")
	int SLAID();

	@Meta.AD(required = false, deflt = "3284", name = "crewId")
	int crewId();

	@Meta.AD(required = false, deflt = "", name = "event-id-missed-bin-collection-reason-mapping", description = "event-id-missed-bin-collection-reason-mapping-description")
	String[] eventIdMissedBinCollectionReasonMapping();

}
