package com.placecube.digitalplace.local.waste.bartec.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt;
import com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields;
import com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation;
import com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Schedules_GetResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_GetResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Detail_GetResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Events_GetResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_CreateResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtBLPUClassification;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_SetChoice_type0;
import com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.Premises_type1;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterBusiness;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact;
import com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration;
import com.placecube.digitalplace.local.waste.bartec.util.BartecDateUtil;
import com.placecube.digitalplace.local.waste.bartec.util.BartecParameterUtil;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.service.WasteConnector;

@Component(immediate = true, service = WasteConnector.class)
public class BartecWasteConnector implements WasteConnector {

	private static final Log LOG = LogFactoryUtil.getLog(BartecWasteConnector.class);

	@Reference
	private AddressLookupService addressLookupService;

	@Reference
	private BartecRequestService bartecRequestService;

	@Reference
	private BartecWasteService bartecWasteService;

	@Reference
	private ObjectFactoryService objectFactoryService;

	@Reference
	private PayloadParserService payloadParserService;

	@Reference
	private ResponseParserService responseParserService;

	@Override
	public String createAssistedBinCollectionJob(long companyId, AssistedBinCollectionJob assistedBinCollectionJob) throws WasteRetrievalException {
		ArrayOfServiceRequest_CreateServiceRequest_CreateFields extendedDataArray = payloadParserService.getExtendedData(assistedBinCollectionJob);
		ArrayOfServiceRequest_CreateServiceRequest_CreateLocation relatedPremises = objectFactoryService.createLocationArrayInput();
		ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest relatedServiceRequest = objectFactoryService.createRelatedServiceRequestArrayInput();
		ServiceRequest_CreateServiceRequest_CreateReporterBusiness reporterBusiness = objectFactoryService.createReporterBusinessInput();
		CtMapPoint_Set serviceRequestLocation = new CtMapPoint_Set();
		serviceRequestLocation.setCtMapPoint_SetChoice_type0(new CtMapPoint_SetChoice_type0());

		try {
			String externalReference = StringPool.BLANK;
			Optional<AddressContext> addressContextOptional = addressLookupService.getByUprn(companyId, assistedBinCollectionJob.getUprn());
			String oneLineAddress = StringPool.BLANK;
			if (addressContextOptional.isPresent()) {
				oneLineAddress = addressContextOptional.get().getFullAddress();
			}
			oneLineAddress = BartecParameterUtil.getUppercaseStringNullSafe(oneLineAddress);
			BartecConfiguration bartecConfiguration = bartecWasteService.getConfiguration(companyId);
			ServiceRequest_CreateServiceRequest_CreateReporterContact reporterContact = payloadParserService.getCreateReporterContact(assistedBinCollectionJob, bartecConfiguration.reporterType());
			int appointementReservationId = Integer.MIN_VALUE;
			int crewId = bartecConfiguration.crewId();
			Calendar dateRequested = Calendar.getInstance();
			int landTypeId = bartecConfiguration.landTypeId();
			String serviceLocationDescription = oneLineAddress;
			int serviceTypeId = bartecConfiguration.createAssistedBinCollectionServiceTypeId();
			int serviceStatusId = bartecConfiguration.createAssistedBinCollectionServiceStatusId();
			int slaId = bartecConfiguration.SLAID();
			String source = BartecParameterUtil.getUppercaseStringNullSafe(bartecConfiguration.source());
			BigDecimal uprn = new BigDecimal(assistedBinCollectionJob.getUprn());

			ServiceRequest_CreateResponse response = bartecRequestService.createServiceRequest(companyId, appointementReservationId, extendedDataArray, crewId, dateRequested, relatedPremises,
					landTypeId, relatedServiceRequest, reporterBusiness, serviceLocationDescription, reporterContact, serviceRequestLocation, serviceTypeId, serviceStatusId, slaId, source, uprn,
					externalReference);

			if (Validator.isNotNull(response) && Validator.isNotNull(response.getServiceRequest_CreateResult())) {
				return response.getServiceRequest_CreateResult().getServiceCode();
			}
			throw new WasteRetrievalException("Empty response message from creating service request");
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}
	}

	@Override
	public WasteSubscriptionResponse createBinSubscription(long companyId, String uprn, String serviceType, String firstName, String lastName, String emailAddress, String telephone, int numberOfBins)
			throws WasteRetrievalException {

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, List<String> itemsToBeCollected,
			String locationOfItems, Date collectionDate) throws WasteRetrievalException {

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createFlyTippingReport(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, String typeOfRubbish, String sizeOfRubbish,
			String details, String coordinates, String locationDetails, String dateOfIncident) throws WasteRetrievalException {

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public WasteSubscriptionResponse createGardenWasteSubscription(long companyId, GardenWasteSubscription gardenWasteSubscription) throws WasteRetrievalException {
		ArrayOfServiceRequest_CreateServiceRequest_CreateLocation relatedPremises = objectFactoryService.createLocationArrayInput();
		ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest relatedServiceRequest = objectFactoryService.createRelatedServiceRequestArrayInput();
		ServiceRequest_CreateServiceRequest_CreateReporterBusiness reporterBusiness = objectFactoryService.createReporterBusinessInput();
		CtMapPoint_Set serviceRequestLocation = new CtMapPoint_Set();
		serviceRequestLocation.setCtMapPoint_SetChoice_type0(new CtMapPoint_SetChoice_type0());

		try {
			String externalReference = BartecParameterUtil.getUppercaseStringNullSafe(gardenWasteSubscription.getCrmGardenRef());
			BigDecimal uprn = new BigDecimal(gardenWasteSubscription.getUprn());
			BartecConfiguration bartecConfiguration = bartecWasteService.getConfiguration(companyId);
			Optional<AddressContext> addressContextOptional = addressLookupService.getByUprn(companyId, gardenWasteSubscription.getUprn());
			String oneLineAddress = StringPool.BLANK;
			if (addressContextOptional.isPresent()) {
				oneLineAddress = addressContextOptional.get().getFullAddress();
			}
			oneLineAddress = BartecParameterUtil.getUppercaseStringNullSafe(oneLineAddress);
			ArrayOfServiceRequest_CreateServiceRequest_CreateFields extendedDataArray = payloadParserService.getExtendedData(companyId, gardenWasteSubscription, oneLineAddress, uprn);
			ServiceRequest_CreateServiceRequest_CreateReporterContact reporterContact = payloadParserService.getCreateReporterContact(gardenWasteSubscription, bartecConfiguration.reporterType());
			int appointementReservationId = Integer.MIN_VALUE;
			int crewId = bartecConfiguration.crewId();
			Calendar dateRequested = Calendar.getInstance();
			int landTypeId = bartecConfiguration.landTypeId();
			String serviceLocationDescription = oneLineAddress;
			int serviceTypeId = bartecConfiguration.createGardenWasteSubscriptionServiceTypeId();
			int serviceStatusId = bartecConfiguration.createGardenWasteSubscriptionServiceStatusId();
			int slaId = bartecConfiguration.SLAID();
			String source = BartecParameterUtil.getUppercaseStringNullSafe(bartecConfiguration.source());

			ServiceRequest_CreateResponse response = bartecRequestService.createServiceRequest(companyId, appointementReservationId, extendedDataArray, crewId, dateRequested, relatedPremises,
					landTypeId, relatedServiceRequest, reporterBusiness, serviceLocationDescription, reporterContact, serviceRequestLocation, serviceTypeId, serviceStatusId, slaId, source, uprn,
					externalReference);

			return responseParserService.getWasteSubscriptionResponseFromCreateServiceRequestResponse(response, gardenWasteSubscription);
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}
	}

	@Override
	public String createMissedBinCollectionJob(long companyId, MissedBinCollectionJob missedBinCollectionJob) throws WasteRetrievalException {

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createNewWasteContainerRequest(long companyId, NewContainerRequest newContainerRequest) throws WasteRetrievalException {

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean enabled(long companyId) {
		try {
			return bartecWasteService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public List<WasteSubscriptionResponse> getAllWasteSubscriptions(long companyId, SubscriptionType subscriptionType) throws WasteRetrievalException {

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRN(long companyId, String uprn) throws WasteRetrievalException {

		BigDecimal uprn1 = BigDecimal.valueOf(Double.parseDouble(uprn));
		BigDecimal usrn = null;
		int workPackId = java.lang.Integer.MIN_VALUE;
		CtDateQuery scheduledStart = new CtDateQuery();
		Calendar calendarWithDate2MonthsInTheFuture = Calendar.getInstance();
		calendarWithDate2MonthsInTheFuture.add(Calendar.DAY_OF_YEAR, 60);
		scheduledStart.setMinimumDate(Calendar.getInstance());
		scheduledStart.setMaximumDate(calendarWithDate2MonthsInTheFuture);
		int serviceRequestID = java.lang.Integer.MIN_VALUE;
		int wardId = java.lang.Integer.MIN_VALUE;
		int parishId = java.lang.Integer.MIN_VALUE;
		CtMapBox jobsBound = null;
		boolean includeRelated = true;

		try {
			Jobs_GetResponse jobsResponse = bartecRequestService.getJobs(companyId, uprn1, usrn, workPackId, scheduledStart, serviceRequestID, wardId, parishId, jobsBound, includeRelated);

			return responseParserService.getBinCollections(jobsResponse);
		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error getting bin collection dates: " + e.getMessage());
		}

	}

	@Override
	public Optional<BulkyCollectionDate> getBulkyCollectionDateByUPRN(long companyId, String uprn) throws WasteRetrievalException {
		LOG.warn("Not implemented yet. Returning empty bulk collection date");

		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String getMissedBinCollectionResponse(long companyId, String uprn, String date) throws WasteRetrievalException {
		try {
			BartecConfiguration bartecConfiguration = bartecWasteService.getConfiguration(companyId);
			Date dateObj = BartecDateUtil.parseDate_dd_MM_yyyy(date);
			Calendar givenDay = Calendar.getInstance();
			givenDay.setTime(dateObj);
			Calendar followingDay = Calendar.getInstance();
			followingDay.setTime(dateObj);
			followingDay.add(Calendar.DAY_OF_YEAR, 1);

			CtDateQuery dateRange = new CtDateQuery();
			dateRange.setMinimumDate(givenDay);
			dateRange.setMaximumDate(followingDay);
			String postCode = null;
			String wardName = null;
			String parishName = null;
			CtMapBox premisesBound = null;
			CtMapBox eventBounds = null;
			String eventType = null;
			String eventSubType = null;
			BigDecimal minDistace = null;
			BigDecimal maxDistance = null;
			String deviceType = null;
			String deviceName = null;
			BigDecimal usrn = null;
			String workPack = null;
			boolean includeRelated = true;
			BigDecimal bigDecimalUPRN = new BigDecimal(uprn);

			Premises_Events_GetResponse response = bartecRequestService.getPremisesEvents(companyId, bigDecimalUPRN, dateRange, postCode, wardName, parishName, premisesBound, eventBounds, eventType,
					eventSubType, minDistace, maxDistance, deviceType, deviceName, usrn, workPack, includeRelated);

			return responseParserService.getReasonTheBinWasMissedFromGetPremisesEventsResponse(response, bartecConfiguration);
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}
	}

	@Override
	public String getName(long companyId) {

		return StringPool.BLANK;
	}

	@Override
	public int howManyBinsSubscribed(long companyId, String uprn, String serviceType) throws WasteRetrievalException {
		BigDecimal uprnBigDecimal = new BigDecimal(uprn);
		ArrayOfInt types = new ArrayOfInt();

		try {
			if (serviceType.contains(StringPool.COMMA)) {
				String[] serviceTypes = serviceType.split(StringPool.COMMA);
				int[] serviceTypeInts = Arrays.stream(serviceTypes).mapToInt(Integer::parseInt).toArray();
				types.set_int(serviceTypeInts);
			} else {
				types.set_int(new int[] { Integer.parseInt(serviceType) });
			}

			Features_Schedules_GetResponse response = bartecRequestService.getFeaturesSchedules(companyId, uprnBigDecimal, types);

			if (Validator.isNotNull(response) && Validator.isNotNull(response.getFeatures_Schedules_GetResult())
					&& Validator.isNotNull(response.getFeatures_Schedules_GetResult().getFeatureSchedule())) {
				return response.getFeatures_Schedules_GetResult().getFeatureSchedule().length;
			}
			return 0;
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}
	}

	@Override
	public int howManyBinsSubscribedForPeriod(long companyId, String uprn, String serviceType, String startDate, String endDate) throws WasteRetrievalException {
		return howManyBinsSubscribed(companyId, uprn, serviceType);
	}

	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String binType, String formInstanceId)
			throws WasteRetrievalException {
		return isPropertyEligibleForGardenWasteSubscription(companyId, uprn, startDateDDsMMsYYYY, endDateDDsMMsYYYY);
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, BulkyWasteCollectionRequest bulkyWasteCollectionRequest, Date date) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public List<Date> getBulkyCollectionSlotsByUPRNAndService(long companyId, String uprn, String serviceId, int numberOfDatesToReturn) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForWasteContainerRequest(long companyId, String uprn, String binType, String binSize, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForBulkyWasteCollection(long companyId, String uprn, List<String> itemsForCollection, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForMissedBinCollectionReport(long companyId, String uprn, String binType, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRNAndService(long companyId, String uprn, String serviceName, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Deprecated
	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws WasteRetrievalException {
		boolean isEligible = false;
		try {
			Premises_Detail_GetResponse premisesDetailGetResponse = bartecRequestService.getPremisesDetails(companyId, BigDecimal.valueOf(Long.valueOf(uprn)));

			if (Validator.isNotNull(premisesDetailGetResponse) && Validator.isNotNull(premisesDetailGetResponse.getPremises_Detail_GetResult())) {

				Premises_type1 premisesType1 = premisesDetailGetResponse.getPremises_Detail_GetResult().getPremises();

				if (Validator.isNotNull(premisesType1)) {
					CtBLPUClassification blpuLogicalClassification = premisesType1.getBLPUClassification();

					if (Validator.isNotNull(blpuLogicalClassification) && "residential".equalsIgnoreCase(blpuLogicalClassification.getPrimaryDescription())) {
						isEligible = true;
					}
				}
			}
		} catch (Exception e) {
			throw new WasteRetrievalException(e);
		}
		return isEligible;
	}

}
