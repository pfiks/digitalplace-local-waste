package com.placecube.digitalplace.local.waste.bartec.service;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields;
import com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation;
import com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest;
import com.placecube.digitalplace.local.waste.bartec.axis.Business_Document_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.Businesses_Classes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Businesses_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Businesses_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Businesses_Statuses_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Businesses_SubStatuses_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Businesses_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Cases_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Crews_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Categories_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Classes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Colours_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Conditions_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Manufacturers_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Notes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Schedules_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Statuses_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Features_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Inspections_Classes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Inspections_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Inspections_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Inspections_Statuses_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Inspections_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_Close;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_Detail_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_FeatureScheduleDates_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_WorkScheduleDates_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Licences_Classes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Licences_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Licences_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Licences_Statuses_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Licences_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_AttributeDefinitions_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Attribute_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Attribute_Update;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Attributes_Delete;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Attributes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Detail_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Document_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Event_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Events_Classes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Events_Document_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Events_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Events_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Events_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_FutureWorkpacks_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Resources_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Resources_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Resources_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Appointment_Reservation_Cancel;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Appointment_Reservation_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Appointment_Reservation_Extend;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Appointment_Slot_Reserve;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Cancel;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Document_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Note_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Status_Set;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_Update;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Appointments_Availability_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Classes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Detail_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_History_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Notes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Notes_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_SLAs_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Statuses_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequests_Updates_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Site_Document_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.Sites_Documents_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Sites_Documents_GetAll;
import com.placecube.digitalplace.local.waste.bartec.axis.Sites_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Streets_Attributes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Streets_Detail_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Streets_Events_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Streets_Events_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Streets_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.System_ExtendedDataDefinitions_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.System_LandTypes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.System_WasteTypes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Vehicle_InspectionForms_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Vehicle_InspectionResults_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Vehicle_Message_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.Vehicles_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.WorkGroups_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.WorkPacks_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Workpack_Note_Create;
import com.placecube.digitalplace.local.waste.bartec.axis.Workpacks_Metrics_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Workpacks_Notes_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.Workpacks_Notes_Types_Get;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateFields;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterBusiness;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact;

@Component(immediate = true, service = ObjectFactoryService.class)
public class ObjectFactoryService {

	public ServiceRequest_Appointment_Reservation_Cancel cancelServiceRequestAppointmentInput() {

		return new ServiceRequest_Appointment_Reservation_Cancel();
	}

	public ServiceRequest_Cancel cancelServiceRequestInput() {

		return new ServiceRequest_Cancel();
	}

	public Business_Document_Create createBusinessDocumentInput() {

		return new Business_Document_Create();
	}

	public ArrayOfServiceRequest_CreateServiceRequest_CreateFields createFieldsArrayInput() {

		return new ArrayOfServiceRequest_CreateServiceRequest_CreateFields();
	}

	public ServiceRequest_CreateServiceRequest_CreateFields createFieldsInput() {

		return new ServiceRequest_CreateServiceRequest_CreateFields();
	}

	public ArrayOfServiceRequest_CreateServiceRequest_CreateLocation createLocationArrayInput() {

		return new ArrayOfServiceRequest_CreateServiceRequest_CreateLocation();
	}

	public Premises_Attribute_Create createPremisesAttributeInput() {

		return new Premises_Attribute_Create();
	}

	public Premises_Document_Create createPremisesDocumentInput() {

		return new Premises_Document_Create();
	}

	public Premises_Event_Create createPremisesEventInput() {

		return new Premises_Event_Create();
	}

	public Premises_Events_Document_Create createPremisesEventsDocumentInput() {

		return new Premises_Events_Document_Create();
	}

	public ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest createRelatedServiceRequestArrayInput() {

		return new ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest();
	}

	public ServiceRequest_CreateServiceRequest_CreateReporterBusiness createReporterBusinessInput() {

		return new ServiceRequest_CreateServiceRequest_CreateReporterBusiness();
	}

	public ServiceRequest_CreateServiceRequest_CreateReporterContact createReporterContactInput() {

		return new ServiceRequest_CreateServiceRequest_CreateReporterContact();
	}

	public ServiceRequest_Appointment_Reservation_Create createServiceRequestAppointmentReservationInput() {

		return new ServiceRequest_Appointment_Reservation_Create();
	}

	public ServiceRequest_Document_Create createServiceRequestDocumentInput() {

		return new ServiceRequest_Document_Create();
	}

	public ServiceRequest_Create createServiceRequestInput() {

		return new ServiceRequest_Create();
	}

	public ServiceRequest_Note_Create createServiceRequestNoteInput() {

		return new ServiceRequest_Note_Create();
	}

	public Site_Document_Create createSiteDocumentInput() {

		return new Site_Document_Create();
	}

	public Vehicle_Message_Create createVehicleMessageInput() {

		return new Vehicle_Message_Create();
	}

	public Workpack_Note_Create createWorkPackNoteInput() {

		return new Workpack_Note_Create();
	}

	public Premises_Attributes_Delete detetePremisesAttributesInput() {

		return new Premises_Attributes_Delete();
	}

	public ServiceRequest_Appointment_Reservation_Extend extendServiceRequestAppointmentReservationInput() {

		return new ServiceRequest_Appointment_Reservation_Extend();
	}

	public Businesses_Classes_Get getBusinessClassesInput() {

		return new Businesses_Classes_Get();
	}

	public Businesses_Documents_Get getBusinessDocumentsInput() {

		return new Businesses_Documents_Get();
	}

	public Businesses_Types_Get getBusinessesTypesInput() {

		return new Businesses_Types_Get();
	}

	public Businesses_Get getBusinessInput() {

		return new Businesses_Get();
	}

	public Businesses_Statuses_Get getBusinessStatusesInput() {

		return new Businesses_Statuses_Get();
	}

	public Businesses_SubStatuses_Get getBusinessSubStatusesInput() {

		return new Businesses_SubStatuses_Get();
	}

	public Cases_Documents_Get getCaseDocumentsInput() {

		return new Cases_Documents_Get();
	}

	public Crews_Get getCrewsInput() {

		return new Crews_Get();
	}

	public Features_Get getFeatuersInput() {

		return new Features_Get();
	}

	public Features_Colours_Get getFeatureColoursInput() {

		return new Features_Colours_Get();
	}

	public Features_Conditions_Get getFeatureConditionsInput() {

		return new Features_Conditions_Get();
	}

	public Features_Manufacturers_Get getFeatureManufacturersInput() {

		return new Features_Manufacturers_Get();
	}

	public Features_Notes_Get getFeatureNotesInput() {

		return new Features_Notes_Get();
	}

	public Features_Categories_Get getFeaturesCategoryInput() {

		return new Features_Categories_Get();
	}

	public Features_Classes_Get getFeaturesClassesInput() {

		return new Features_Classes_Get();
	}

	public Features_Schedules_Get getFeaturesSchedulesInput() {

		return new Features_Schedules_Get();
	}

	public Features_Statuses_Get getFeatureStatusesInput() {

		return new Features_Statuses_Get();
	}

	public Features_Types_Get getFeatureTypesInput() {

		return new Features_Types_Get();
	}

	public Inspections_Classes_Get getInspectionClassesInput() {

		return new Inspections_Classes_Get();
	}

	public Inspections_Documents_Get getInspectionDocumentsInput() {

		return new Inspections_Documents_Get();
	}

	public Inspections_Get getInspectionsInput() {

		return new Inspections_Get();
	}

	public Inspections_Statuses_Get getInspectionsStatusesInput() {

		return new Inspections_Statuses_Get();
	}

	public Inspections_Types_Get getInspectionsTypesInput() {

		return new Inspections_Types_Get();
	}

	public Jobs_Close getJobCloseInput() {

		return new Jobs_Close();
	}

	public Jobs_Detail_Get getJobDetailInput() {

		return new Jobs_Detail_Get();
	}

	public Jobs_Documents_Get getJobDocumentsInput() {

		return new Jobs_Documents_Get();
	}

	public Jobs_FeatureScheduleDates_Get getJobFeatureScheduleDatesInput() {

		return new Jobs_FeatureScheduleDates_Get();
	}

	public Jobs_Get getJobsInput() {

		return new Jobs_Get();
	}

	public Jobs_WorkScheduleDates_Get getJobsWorkScheduleDatesInput() {

		return new Jobs_WorkScheduleDates_Get();
	}

	public Licences_Classes_Get getLicencesClassesInput() {

		return new Licences_Classes_Get();
	}

	public Licences_Documents_Get getLicencesDocumentsInput() {

		return new Licences_Documents_Get();
	}

	public Licences_Get getLicencesInput() {

		return new Licences_Get();
	}

	public Licences_Statuses_Get getLicencesStatusesInput() {

		return new Licences_Statuses_Get();
	}

	public Licences_Types_Get getLicencesTypesInput() {

		return new Licences_Types_Get();
	}

	public Premises_AttributeDefinitions_Get getPremisesAttributeDefinitions() {

		return new Premises_AttributeDefinitions_Get();
	}

	public Premises_Attributes_Get getPremisesAttributesInput() {

		return new Premises_Attributes_Get();
	}

	public Premises_Detail_Get getPremisesDetailsInput() {

		return new Premises_Detail_Get();
	}

	public Premises_Documents_Get getPremisesDocumentsInput() {

		return new Premises_Documents_Get();
	}

	public Premises_Events_Classes_Get getPremisesEventsClassesInput() {

		return new Premises_Events_Classes_Get();
	}

	public Premises_Events_Documents_Get getPremisesEventsDocumentsInput() {

		return new Premises_Events_Documents_Get();
	}

	public Premises_Events_Get getPremisesEventsInput() {

		return new Premises_Events_Get();
	}

	public Premises_Events_Types_Get getPremisesEventsTypesInput() {

		return new Premises_Events_Types_Get();
	}

	public Premises_FutureWorkpacks_Get getPremisesFutureWorkpacksInput() {

		return new Premises_FutureWorkpacks_Get();
	}

	public Premises_Get getPremisesInput() {

		return new Premises_Get();
	}

	public Resources_Documents_Get getResourcesDocumentsInput() {

		return new Resources_Documents_Get();
	}

	public Resources_Get getResourcesInput() {

		return new Resources_Get();
	}

	public Resources_Types_Get getResourcesTypesInput() {

		return new Resources_Types_Get();
	}

	public ServiceRequests_Appointments_Availability_Get getServiceRequestsAppoitmentsAvailabilityInput() {

		return new ServiceRequests_Appointments_Availability_Get();
	}

	public ServiceRequests_Classes_Get getServiceRequestsClassesInput() {

		return new ServiceRequests_Classes_Get();
	}

	public ServiceRequests_Detail_Get getServiceRequestsDetailsInput() {

		return new ServiceRequests_Detail_Get();
	}

	public ServiceRequests_Documents_Get getServiceRequestsDocumentsInput() {

		return new ServiceRequests_Documents_Get();
	}

	public ServiceRequests_History_Get getServiceRequestsHistoryInput() {

		return new ServiceRequests_History_Get();
	}

	public ServiceRequests_Get getServiceRequestsInput() {

		return new ServiceRequests_Get();
	}

	public ServiceRequests_Notes_Get getServiceRequestsNotesInput() {

		return new ServiceRequests_Notes_Get();
	}

	public ServiceRequests_Notes_Types_Get getServiceRequestsNotesTypesInput() {

		return new ServiceRequests_Notes_Types_Get();
	}

	public ServiceRequests_SLAs_Get getServiceRequestsSLAsInput() {

		return new ServiceRequests_SLAs_Get();
	}

	public ServiceRequests_Statuses_Get getServiceRequestsStatusesInput() {

		return new ServiceRequests_Statuses_Get();
	}

	public ServiceRequests_Types_Get getServiceRequestsTypesInput() {
		return new ServiceRequests_Types_Get();
	}

	public ServiceRequests_Updates_Get getServiceRequestsUpdatesInput() {
		return new ServiceRequests_Updates_Get();
	}

	public Sites_Documents_GetAll getSiteDocumentsAllInput() {
		return new Sites_Documents_GetAll();
	}

	public Sites_Documents_Get getSiteDocumentsInput() {
		return new Sites_Documents_Get();
	}

	public Sites_Get getSitesInput() {

		return new Sites_Get();
	}

	public Streets_Attributes_Get getStreetsAttributesInput() {

		return new Streets_Attributes_Get();
	}

	public Streets_Detail_Get getStreetsDetailInput() {

		return new Streets_Detail_Get();
	}

	public Streets_Events_Get getStreetsEventsInput() {

		return new Streets_Events_Get();
	}

	public Streets_Events_Types_Get getStreetsEventsTypeInput() {

		return new Streets_Events_Types_Get();
	}

	public Streets_Get getStreetsGetInput() {

		return new Streets_Get();
	}

	public System_ExtendedDataDefinitions_Get getSystemExtendedDateDefinitionsInput() {

		return new System_ExtendedDataDefinitions_Get();
	}

	public System_LandTypes_Get getSystemLandTypesInput() {

		return new System_LandTypes_Get();
	}

	public System_WasteTypes_Get getSystemWasteTypesInput() {

		return new System_WasteTypes_Get();
	}

	public Vehicle_InspectionForms_Get getVehicleInspectionFormsInput() {

		return new Vehicle_InspectionForms_Get();
	}

	public Vehicle_InspectionResults_Get getVehicleInspectionResponsesInput() {

		return new Vehicle_InspectionResults_Get();
	}

	public Vehicle_InspectionResults_Get getVehicleInspectionResultsInput() {

		return new Vehicle_InspectionResults_Get();
	}

	public Vehicles_Get getVehiclesInput() {

		return new Vehicles_Get();
	}

	public WorkGroups_Get getWorkGroups() {

		return new WorkGroups_Get();
	}

	public WorkPacks_Get getWorkPacksInput() {

		return new WorkPacks_Get();
	}

	public Workpacks_Metrics_Get getWorkPacksMetricsInput() {

		return new Workpacks_Metrics_Get();
	}

	public Workpacks_Notes_Get getWorkPacksNotesInput() {

		return new Workpacks_Notes_Get();
	}

	public Workpacks_Notes_Types_Get getWorkPacksNotesTypesInput() {

		return new Workpacks_Notes_Types_Get();
	}

	public ServiceRequest_Appointment_Slot_Reserve reserveServiceRequestAppointmentSlotInput() {

		return new ServiceRequest_Appointment_Slot_Reserve();
	}

	public ServiceRequest_Status_Set setServiceRequestStatusInput() {

		return new ServiceRequest_Status_Set();
	}

	public Premises_Attribute_Update updatePremisesAttributeInput() {

		return new Premises_Attribute_Update();
	}

	public ServiceRequest_Update updateServiceRequestInput() {

		return new ServiceRequest_Update();
	}
}