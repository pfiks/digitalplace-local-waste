package com.placecube.digitalplace.local.waste.bartec.service;

import java.util.HashSet;
import java.util.Set;

import org.osgi.service.component.annotations.Component;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component(immediate = true, service = JsonResponseParserService.class)
public class JsonResponseParserService {

	private static final Set<String> EXCLUDE_FIELDS = new HashSet<>();
	private static Gson gson = getGson();

	private static ExclusionStrategy getExclusionStrategy() {
		return new ExclusionStrategy() {

			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				return false;
			}

			@Override
			public boolean shouldSkipField(FieldAttributes field) {
				if (EXCLUDE_FIELDS.contains(field.getName())) {
					return true;
				}
				return false;
			}
		};
	}

	private static Gson getGson() {
		EXCLUDE_FIELDS.add("__hashCodeCalc");
		EXCLUDE_FIELDS.add("__equalsCalc");
		GsonBuilder gsonBuilder = new GsonBuilder().setExclusionStrategies(getExclusionStrategy());
		return gsonBuilder.create();
	}

	public String toJson(Object object) {
		return gson.toJson(object);
	}
}
