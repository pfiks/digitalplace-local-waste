package com.placecube.digitalplace.local.waste.bartec.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Attributes_GetResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributes_get_xsd.Attribute_type7;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateFields;
import com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact;
import com.placecube.digitalplace.local.waste.bartec.util.BartecDateUtil;
import com.placecube.digitalplace.local.waste.bartec.util.BartecParameterUtil;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;

@Component(immediate = true, service = PayloadParserService.class)
public class PayloadParserService {

	@Reference
	private ObjectFactoryService objectFactoryService;

	@Reference
	private BartecRequestService bartecRequestService;

	public ServiceRequest_CreateServiceRequest_CreateReporterContact getCreateReporterContact(GardenWasteSubscription gardenWasteSubscription, String reporterType) {
		ServiceRequest_CreateServiceRequest_CreateReporterContact createReporterContact = objectFactoryService.createReporterContactInput();
		createReporterContact.setEmail(BartecParameterUtil.getUppercaseStringNullSafe(gardenWasteSubscription.getEmailAddress()));
		createReporterContact.setForename(BartecParameterUtil.getUppercaseStringNullSafe(gardenWasteSubscription.getFirstName()));
		createReporterContact.setSurname(BartecParameterUtil.getUppercaseStringNullSafe(gardenWasteSubscription.getLastName()));
		createReporterContact.setTelephone(gardenWasteSubscription.getTelephone());
		createReporterContact.setReporterType(BartecParameterUtil.getUppercaseStringNullSafe(reporterType));

		return createReporterContact;
	}

	public ServiceRequest_CreateServiceRequest_CreateReporterContact getCreateReporterContact(AssistedBinCollectionJob assistedBinCollectionJob, String reporterType) {
		ServiceRequest_CreateServiceRequest_CreateReporterContact createReporterContact = objectFactoryService.createReporterContactInput();
		createReporterContact.setEmail(BartecParameterUtil.getUppercaseStringNullSafe(assistedBinCollectionJob.getEmailAddress()));
		createReporterContact.setForename(BartecParameterUtil.getUppercaseStringNullSafe(assistedBinCollectionJob.getFirstName()));
		createReporterContact.setSurname(BartecParameterUtil.getUppercaseStringNullSafe(assistedBinCollectionJob.getLastName()));
		createReporterContact.setTelephone(assistedBinCollectionJob.getTelephone());
		createReporterContact.setReporterType(BartecParameterUtil.getUppercaseStringNullSafe(reporterType));

		return createReporterContact;
	}

	public ArrayOfServiceRequest_CreateServiceRequest_CreateFields getExtendedData(long companyId, GardenWasteSubscription gardenWasteSubscription, String oneLineAddress, BigDecimal uprn)
			throws Exception {
		LocalDateTime nextYearEndOfMarch = LocalDateTime.of(LocalDate.now().getYear() + 1, Month.MARCH, 31, 0, 0);
		String startDate = BartecDateUtil.formatDate_yyyy_MM_dd_HH_mm_ss(LocalDateTime.now());
		String endDate = BartecDateUtil.formatDate_yyyy_MM_dd_HH_mm_ss(nextYearEndOfMarch);

		boolean sacks = getSacksInformationForUPRN(companyId, uprn);

		ArrayOfServiceRequest_CreateServiceRequest_CreateFields extendedDataArray = objectFactoryService.createFieldsArrayInput();

		ServiceRequest_CreateServiceRequest_CreateFields extendedDataNumberOfSubscriptions = objectFactoryService.createFieldsInput();
		extendedDataNumberOfSubscriptions.setFieldName("NUMBER_OF_SUBSCRIPTIONS");
		extendedDataNumberOfSubscriptions.setFieldValue(String.valueOf(gardenWasteSubscription.getNumberSubscribedBins()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataSacks = objectFactoryService.createFieldsInput();
		extendedDataSacks.setFieldName("SACKS?");
		extendedDataSacks.setFieldValue(sacks ? "TRUE" : "FALSE");
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataBinsToDeliver = objectFactoryService.createFieldsInput();
		extendedDataBinsToDeliver.setFieldName("BINS_TO_DELIVER");
		extendedDataBinsToDeliver.setFieldValue(String.valueOf(gardenWasteSubscription.getNumberRequestedNewBins()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataDeliveryInformation = objectFactoryService.createFieldsInput();
		extendedDataDeliveryInformation.setFieldName("DELIVERY_INFORMATION");
		extendedDataDeliveryInformation.setFieldValue(BartecParameterUtil.getUppercaseStringNullSafe(gardenWasteSubscription.getGardenContainerDeliveryComments()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataStartDate = objectFactoryService.createFieldsInput();
		extendedDataStartDate.setFieldName("START_DATE");
		extendedDataStartDate.setFieldValue(startDate);
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataEndDate = objectFactoryService.createFieldsInput();
		extendedDataEndDate.setFieldName("END_DATE");
		extendedDataEndDate.setFieldValue(endDate);
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataGWDirectDebit = objectFactoryService.createFieldsInput();
		extendedDataGWDirectDebit.setFieldName("GW_DIRECT_DEBIT");
		extendedDataGWDirectDebit.setFieldValue("FALSE");
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataPaymentReference = objectFactoryService.createFieldsInput();
		extendedDataPaymentReference.setFieldName("PAYMENT_REFERENCE");
		extendedDataPaymentReference.setFieldValue(BartecParameterUtil.getUppercaseStringNullSafe(gardenWasteSubscription.getNewPayRef()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataAlternativeAddress = objectFactoryService.createFieldsInput();
		extendedDataAlternativeAddress.setFieldName("ALTERNATIVE_ADDRESS");
		extendedDataAlternativeAddress.setFieldValue(oneLineAddress);
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataContactName = objectFactoryService.createFieldsInput();
		extendedDataContactName.setFieldName("GW_CONTACT_NAME");
		extendedDataContactName.setFieldValue(BartecParameterUtil.getUppercaseStringNullSafe(gardenWasteSubscription.getFirstName() + " " + gardenWasteSubscription.getLastName()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataContactPhone = objectFactoryService.createFieldsInput();
		extendedDataContactPhone.setFieldName("GW_CONTACT_PHONE");
		extendedDataContactPhone.setFieldValue(gardenWasteSubscription.getTelephone());
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataContactEmail = objectFactoryService.createFieldsInput();
		extendedDataContactEmail.setFieldName("GW_CONTACT_EMAIL");
		extendedDataContactEmail.setFieldValue(BartecParameterUtil.getUppercaseStringNullSafe(gardenWasteSubscription.getEmailAddress()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedDataRelationship = objectFactoryService.createFieldsInput();
		extendedDataRelationship.setFieldName("RELATIONSHIP");
		extendedDataRelationship.setFieldValue("");


		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataNumberOfSubscriptions);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataSacks);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataBinsToDeliver);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataDeliveryInformation);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataStartDate);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataEndDate);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataGWDirectDebit);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataPaymentReference);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataAlternativeAddress);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataContactName);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataContactPhone);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataContactEmail);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedDataRelationship);

		return extendedDataArray;
	}

	public ArrayOfServiceRequest_CreateServiceRequest_CreateFields getExtendedData(AssistedBinCollectionJob assistedBinCollectionJob) {
		ArrayOfServiceRequest_CreateServiceRequest_CreateFields extendedDataArray = objectFactoryService.createFieldsArrayInput();

		LocalDate now = LocalDate.now();
		LocalDateTime nextYear = LocalDateTime.of(now.getYear() + 1, now.getMonth(), now.getDayOfMonth(), 0, 0);
		String reviewDate = BartecDateUtil.formatDate_yyyy_MM_dd_HH_mm_ss(nextYear);

		ServiceRequest_CreateServiceRequest_CreateFields extendedResidentName = objectFactoryService.createFieldsInput();
		extendedResidentName.setFieldName("RESIDENT_NAME");
		extendedResidentName.setFieldValue(BartecParameterUtil.getUppercaseStringNullSafe((assistedBinCollectionJob.getFirstName() + " " + assistedBinCollectionJob.getLastName())));
		ServiceRequest_CreateServiceRequest_CreateFields extendedResidentNumber = objectFactoryService.createFieldsInput();
		extendedResidentNumber.setFieldName("RESIDENT_NUMBER");
		extendedResidentNumber.setFieldValue(assistedBinCollectionJob.getTelephone());
		ServiceRequest_CreateServiceRequest_CreateFields extendedResidentEmail = objectFactoryService.createFieldsInput();
		extendedResidentEmail.setFieldName("RESIDENT_EMAIL");
		extendedResidentEmail.setFieldValue(BartecParameterUtil.getUppercaseStringNullSafe(assistedBinCollectionJob.getEmailAddress()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedNumberOfResidents = objectFactoryService.createFieldsInput();
		extendedNumberOfResidents.setFieldName("NUMBER_OF_RESIDENTS");
		extendedNumberOfResidents.setFieldValue(String.valueOf(assistedBinCollectionJob.getNumberOfResidents()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedReviewDate = objectFactoryService.createFieldsInput();
		extendedReviewDate.setFieldName("REVIEW_DATE");
		extendedReviewDate.setFieldValue(reviewDate);
		ServiceRequest_CreateServiceRequest_CreateFields extendedExpiryDate = objectFactoryService.createFieldsInput();
		extendedExpiryDate.setFieldName("EXPIRY_DATE");
		extendedExpiryDate.setFieldValue(assistedBinCollectionJob.getEndDate());
		ServiceRequest_CreateServiceRequest_CreateFields extendedLocation = objectFactoryService.createFieldsInput();
		extendedLocation.setFieldName("LOCATION");
		extendedLocation.setFieldValue(BartecParameterUtil.getUppercaseStringNullSafe(assistedBinCollectionJob.getBinLocation()));
		ServiceRequest_CreateServiceRequest_CreateFields extendedRiskAssesmentRequired = objectFactoryService.createFieldsInput();
		extendedRiskAssesmentRequired.setFieldName("RISK_ASSESMENT_REQUIRED?");
		extendedRiskAssesmentRequired.setFieldValue(assistedBinCollectionJob.isRiskAssessmentRequired() ? "TRUE" : "FALSE");
		ServiceRequest_CreateServiceRequest_CreateFields extendedRiskAssessmentReason = objectFactoryService.createFieldsInput();
		extendedRiskAssessmentReason.setFieldName("RISK_ASSESSMENT_REASON");
		extendedRiskAssessmentReason.setFieldValue(BartecParameterUtil.getUppercaseStringNullSafe(assistedBinCollectionJob.getRiskAssessmentReason()));

		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedResidentName);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedResidentNumber);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedResidentEmail);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedNumberOfResidents);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedReviewDate);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedExpiryDate);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedLocation);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedRiskAssesmentRequired);
		extendedDataArray.addServiceRequest_CreateServiceRequest_CreateFields(extendedRiskAssessmentReason);

		return extendedDataArray;
	}

	private boolean getSacksInformationForUPRN(long companyId, BigDecimal uprn) throws Exception {
		Premises_Attributes_GetResponse response = bartecRequestService.getPremisesAttributes(companyId, uprn);
		Attribute_type7[] attributes = response.getPremises_Attributes_GetResult().getAttribute();

		if (Validator.isNotNull(attributes)) {
			for (Attribute_type7 attribute : attributes) {
				if ("BAG & BOX".equals(attribute.getAttributeDefinition().getName())) {
					return true;
				}
			}
		}

		return false;
	}
}
