package com.placecube.digitalplace.local.waste.bartec.util;

import com.liferay.portal.kernel.util.Validator;

public class BartecParameterUtil {

	private BartecParameterUtil() {}

	public static String getUppercaseStringNullSafe(String sourceString) {
		return Validator.isNotNull(sourceString) ? sourceString.toUpperCase() : sourceString;
	}

}
