package com.placecube.digitalplace.local.waste.bartec.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class BartecDateUtil {

	private BartecDateUtil() {}

	private static DateTimeFormatter dd_MM_yyyy_formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	private static DateTimeFormatter yyyy_MM_dd_HH_mm_ss = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

	public static String formatDate_dd_MM_yyyy(LocalDate sourceDate) {
		return sourceDate.format(dd_MM_yyyy_formatter);
	}

	public static LocalDate parseDate_dd_MM_yyyy(Calendar calendar) {
		return LocalDate.parse((CharSequence) calendar, dd_MM_yyyy_formatter);
	}

	public static Date parseDate_dd_MM_yyyy(String dateString) {
		return Date.from(LocalDate.parse(dateString, dd_MM_yyyy_formatter).atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public static LocalDate getLocalDateFromCalendar(Calendar calendar) {
		return LocalDate.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId());
	}

	public static String formatDate_yyyy_MM_dd_HH_mm_ss(LocalDateTime sourceDateTime) {
		return sourceDateTime.format(yyyy_MM_dd_HH_mm_ss);
	}

}
