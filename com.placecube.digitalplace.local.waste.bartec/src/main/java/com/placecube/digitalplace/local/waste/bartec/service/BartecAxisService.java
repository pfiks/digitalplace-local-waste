package com.placecube.digitalplace.local.waste.bartec.service;

import com.placecube.digitalplace.local.waste.bartec.axis.Authenticate;
import com.placecube.digitalplace.local.waste.bartec.axis.AuthenticateResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.AuthenticationAPIWebServiceStub;
import com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService;
import com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebServiceStub;
import com.placecube.digitalplace.local.waste.bartec.axis.www.Token_type2;
import com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = BartecAxisService.class)
public class BartecAxisService {

	@Reference
	private BartecWasteService bartecWasteService;

	public String authenticate(String username, String password) {
		String objToken = null;
		try {

			Authenticate authenticate = new Authenticate();
			authenticate.setPassword(password);
			authenticate.setUser(username);

			AuthenticationAPIWebServiceStub apiWebServiceStub = new AuthenticationAPIWebServiceStub();

			AuthenticateResponse result = apiWebServiceStub.authenticate(authenticate);

			Token_type2 authenticateResult = result.getAuthenticateResult().getToken();
			objToken = authenticateResult.getTokenString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return objToken;

	}

	public CollectiveAPIWebService getBartecAxisService(long companyId) throws Exception {

		BartecConfiguration configuration = bartecWasteService.getConfiguration(companyId);

		CollectiveAPIWebServiceStub collectiveAPIWebServiceStub = new CollectiveAPIWebServiceStub(configuration.bartecEndpointUrl());

		return collectiveAPIWebServiceStub;

	}

}
