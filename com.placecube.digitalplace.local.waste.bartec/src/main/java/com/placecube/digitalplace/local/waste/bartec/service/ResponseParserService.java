package com.placecube.digitalplace.local.waste.bartec.service;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.bartec.axis.Jobs_GetResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.Premises_Events_GetResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.ServiceRequest_CreateResponse;
import com.placecube.digitalplace.local.waste.bartec.axis.www.CtEvent;
import com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_get_xsd.Jobs_GetResult_type0;
import com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_get_xsd.Jobs_type1;
import com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_get_xsd.Premises_Events_GetResult_type0;
import com.placecube.digitalplace.local.waste.bartec.configuration.BartecConfiguration;
import com.placecube.digitalplace.local.waste.bartec.util.BartecDateUtil;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BinCollection.BinCollectionBuilder;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;

@Component(immediate = true, service = ResponseParserService.class)
public class ResponseParserService {

	public Set<BinCollection> getBinCollections(Jobs_GetResponse jobs_getResponse) {

		Set<BinCollection> binCollections = new LinkedHashSet<>();

		Jobs_GetResult_type0 collections = jobs_getResponse.getJobs_GetResult();

		Locale defaultLocale = LocaleUtil.getDefault();
		ResourceBundle defaultBundle = ResourceBundle.getBundle("content/Language", defaultLocale);

		if (Validator.isNotNull(collections) && Validator.isNotNull(collections.getJobs())) {

			Map<String, List<Jobs_type1>> nameToBinCollectionsListMap = new HashMap<>();

			for (Jobs_type1 job : collections.getJobs()) {
				String name = job.getJob().getName();

				if (nameToBinCollectionsListMap.containsKey(name)) {
					nameToBinCollectionsListMap.get(name).add(job);
				} else {
					List<Jobs_type1> jobs = new ArrayList<>();
					jobs.add(job);
					nameToBinCollectionsListMap.put(name, jobs);
				}
			}

			for (Map.Entry<String, List<Jobs_type1>> mapEntry : nameToBinCollectionsListMap.entrySet()) {
				Map<Locale, String> labelMap = new HashMap<>();
				List<Jobs_type1> binCollectionJobs = mapEntry.getValue();

				String label = getLabel(mapEntry.getKey(), defaultBundle);
				labelMap.put(defaultLocale, label);
				LocalDate collectionDate = BartecDateUtil.getLocalDateFromCalendar(binCollectionJobs.get(0).getJob().getScheduledStart());
				BinCollectionBuilder binCollectionBuilder = new BinCollectionBuilder(mapEntry.getKey(), collectionDate, labelMap);
				if (binCollectionJobs.size() > 1) {
					long daysBetween = ChronoUnit.DAYS.between(collectionDate, BartecDateUtil.getLocalDateFromCalendar(binCollectionJobs.get(1).getJob().getScheduledStart()));
					String frequency = getFrequency((int) daysBetween, defaultBundle);

					binCollectionBuilder = binCollectionBuilder.followingCollectionDate(BartecDateUtil.getLocalDateFromCalendar(binCollectionJobs.get(1).getJob().getScheduledStart()));
					binCollectionBuilder = binCollectionBuilder.frequency(frequency);
				}

				binCollectionBuilder = binCollectionBuilder.formattedCollectionDate(BartecDateUtil.formatDate_dd_MM_yyyy(collectionDate));

				binCollections.add(binCollectionBuilder.build());
			}
		}

		return binCollections;
	}

	public String getReasonTheBinWasMissedFromGetPremisesEventsResponse(Premises_Events_GetResponse response, BartecConfiguration bartecConfiguration) throws JSONException {
		StringBuilder sb = new StringBuilder();

		Premises_Events_GetResult_type0 eventsResult = response.getPremises_Events_GetResult();
		if (Validator.isNotNull(eventsResult)) {
			CtEvent[] events = eventsResult.getEvent();
			if (Validator.isNotNull(events)) {
				String[] eventIdReasonMapping = bartecConfiguration.eventIdMissedBinCollectionReasonMapping();
				Map<Integer, JSONObject> eventIdReasonMappingMap = getEventIdReasonMappingMap(eventIdReasonMapping);
				Locale defaultLocale = LocaleUtil.getDefault();
				ResourceBundle defaultBundle = ResourceBundle.getBundle("content/Language", defaultLocale);
				for (CtEvent event : events) {
					int eventType = Validator.isNotNull(event.getEventType()) ? event.getEventType().getID() : -1;
					JSONObject mapping = eventIdReasonMappingMap.get(eventType);

					if (Validator.isNotNull(mapping)) {
						if (mapping.getBoolean("useCompoundMessage")) {
							String parameterToUseForMessage = "No reason available";
							if (Validator.isNotNull(event.getEventSubType()) && event.getEventSubType().length > 0) {
								parameterToUseForMessage = event.getEventSubType()[0].getDescription();
							}
							sb.append(LanguageUtil.format(defaultBundle, mapping.getString("messageLanguageKey"), parameterToUseForMessage)).append(StringPool.NEW_LINE);
						} else {
							sb.append(LanguageUtil.get(defaultBundle, mapping.getString("messageLanguageKey"))).append(StringPool.NEW_LINE);
						}
					} else {
						if (Validator.isNotNull(event.getEventType()) && Validator.isNotNull(event.getEventType().getDescription())) {
							sb.append(event.getEventType().getDescription()).append(StringPool.NEW_LINE);
						}
					}
				}
			}
		}

		return sb.toString();
	}

	public WasteSubscriptionResponse getWasteSubscriptionResponseFromCreateServiceRequestResponse(ServiceRequest_CreateResponse response, GardenWasteSubscription gardenWasteSubscription) {

		if (Validator.isNotNull(response) && Validator.isNotNull(response.getServiceRequest_CreateResult())) {
			WasteSubscriptionResponse result = new WasteSubscriptionResponse(response.getServiceRequest_CreateResult().getServiceCode(), SubscriptionType.GARDEN_WASTE);

			result.setUprn(gardenWasteSubscription.getUprn());
			result.setFirstName(gardenWasteSubscription.getFirstName());
			result.setLastName(gardenWasteSubscription.getLastName());
			result.setEmailAddress(gardenWasteSubscription.getEmailAddress());
			result.setTelephone(gardenWasteSubscription.getTelephone());
			result.setStartDate(LocalDate.now());
			LocalDate nextYearEndOfMarch = LocalDate.of(LocalDate.now().getYear() + 1, Month.MARCH, 31);
			result.setEndDate(nextYearEndOfMarch);

			return result;
		} else {
			return new WasteSubscriptionResponse("-1", SubscriptionType.GARDEN_WASTE);
		}
	}

	private Map<Integer, JSONObject> getEventIdReasonMappingMap(String[] valuesFromConfiguration) throws JSONException {
		Map<Integer, JSONObject> eventIdReasonMappingMap = new HashMap<>();

		for (String valueFromConfiguration : valuesFromConfiguration) {
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject(valueFromConfiguration);
			eventIdReasonMappingMap.put(jsonObject.getInt("eventId"), jsonObject);
		}

		return eventIdReasonMappingMap;
	}

	private String getFrequency(int daysBetweenCollections, ResourceBundle resourceBundle) {
		switch (daysBetweenCollections) {
		case 7:
			return LanguageUtil.get(resourceBundle, "weekly");
		case 14:
			return LanguageUtil.get(resourceBundle, "fortnightly");
		case 28:
			return LanguageUtil.get(resourceBundle, "monthly");
		default:
			return LanguageUtil.get(resourceBundle, "other-frequency");
		}
	}

	private String getLabel(String key, ResourceBundle resourceBundle) {
		key = key.toLowerCase();
		if (key.contains("garden") || key.contains("compostable")) {
			return LanguageUtil.get(resourceBundle, "garden-waste");
		} else if (key.contains("rec")) {
			return LanguageUtil.get(resourceBundle, "recycling-waste");
		} else if (key.contains("rubbish")) {
			return LanguageUtil.get(resourceBundle, "rubbish-waste");
		} else if (key.contains("food")) {
			return LanguageUtil.get(resourceBundle, "food-waste");
		} else if (key.contains("glass")) {
			return LanguageUtil.get(resourceBundle, "glass-waste");
		} else {
			return LanguageUtil.get(resourceBundle, "other-waste");
		}
	}
}
