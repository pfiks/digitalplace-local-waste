package com.placecube.digitalplace.local.waste.echo.service;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;

import org.apache.axis.message.SOAPHeaderElement;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.util.ArrayUtil;
import com.placecube.digitalplace.local.waste.echo.axis.ObjectRef;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceHttpsStub;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitQuery;
import com.placecube.digitalplace.local.waste.echo.axis.Service_Service;

public class EchoAxisServiceTest extends PowerMockito {

	private static final String ENDPOINT_ADDRESS_PROPERTY = "ENDPOINT_ADDRESS_PROPERTY";
	private static final String NAMESPACE_URI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final String PASSWORD = "PASSWORD";
	private static final String UPRN = "UPRN";
	private static final String USERNAME = "USERNAME";

	@InjectMocks
	private EchoAxisService echoAxisService;

	@Mock
	private ServiceHttpsStub mockStub;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getObjectRef_WhenNoErrors_ThenReturnsObjectRefWithUPRNValue() {

		ObjectRef result = echoAxisService.getObjectRef(UPRN);

		assertEquals(UPRN, result.getValue()[0]);
	}

	@Test
	public void getServiceLocator_WhenNoErrors_ThenReturnsServiceLocator() {
		Service_Service result = echoAxisService.getServiceLocator();

		assertThat(result, instanceOf(Service_Service.class));
	}

	@Test
	public void getServiceUnitQuery_WhenServiceIdsIsEmpty_ThenReturnsServiceUnitQueryWithNoServiceIds() {

		ServiceUnitQuery result = echoAxisService.getServiceUnitQuery(new int[0]);

		assertTrue(ArrayUtil.isEmpty(result.getServiceIds()));
	}

	@Test
	public void getServiceUnitQuery_WhenServiceIdsIsNotEmpty_ThenReturnsServiceUnitQueryWithServiceIds() {

		ServiceUnitQuery result = echoAxisService.getServiceUnitQuery(new int[] { 1 });

		assertEquals(1, result.getServiceIds()[0]);
	}

	@Test
	public void setActionHeader_WhenNoErrors_ThenAddsActionHeaderToStub() {

		echoAxisService.setActionHeader(mockStub);

		ArgumentCaptor<SOAPHeaderElement> headerCaptor = ArgumentCaptor.forClass(SOAPHeaderElement.class);
		verify(mockStub).setHeader(headerCaptor.capture());
		assertEquals("http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetServiceUnitsForObject", headerCaptor.getValue().getValue());
	}

	@Test
	public void setSecurityHeader_WhenNoErrors_ThenAddsSecurityeaderToStub() throws SOAPException {

		QName usernameTokenQName = new QName(NAMESPACE_URI, "UsernameToken", "ns1");
		QName usernameQName = new QName(NAMESPACE_URI, "Username", "ns1");
		QName passwordQName = new QName(NAMESPACE_URI, "Password", "ns1");

		echoAxisService.setSecurityHeader(mockStub, USERNAME, PASSWORD);

		ArgumentCaptor<SOAPHeaderElement> headerCaptor = ArgumentCaptor.forClass(SOAPHeaderElement.class);
		verify(mockStub).setHeader(headerCaptor.capture());
		assertEquals(USERNAME, headerCaptor.getValue().getChildElement(usernameTokenQName).getChildElement(usernameQName).getValue());
		assertEquals(PASSWORD, headerCaptor.getValue().getChildElement(usernameTokenQName).getChildElement(passwordQName).getValue());
	}

	@Test
	public void setToHeader_WhenNoErrors_ThenAddsToHeaderToStub() {

		when(mockStub._getProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY)).thenReturn(ENDPOINT_ADDRESS_PROPERTY);

		echoAxisService.setToHeader(mockStub);

		ArgumentCaptor<SOAPHeaderElement> headerCaptor = ArgumentCaptor.forClass(SOAPHeaderElement.class);
		verify(mockStub).setHeader(headerCaptor.capture());
		assertEquals(ENDPOINT_ADDRESS_PROPERTY, headerCaptor.getValue().getValue());
	}
}