package com.placecube.digitalplace.local.waste.echo.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.waste.echo.axis.ObjectRef;
import com.placecube.digitalplace.local.waste.echo.axis.ScheduledTaskInfo;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceHttpsStub;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceTask;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskSchedule;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitQuery;
import com.placecube.digitalplace.local.waste.echo.axis.Service_Service;
import com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset;
import com.placecube.digitalplace.local.waste.echo.configuration.EchoCompanyConfiguration;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EntityUtils.class, JSONFactoryUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class EchoWasteServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String ID = "ID";
	private static final String INVALID_JSON = "INVALID{{";
	private static final String PASSWORD = "PASSWORD";
	private static final String SERVICE_NAME = "SERVICE_NAME";
	private static final String SERVICE_NAME_LABEL = "SERVICE_NAME_LABEL";
	private static final String SERVICE_UNITS_FOR_OBJECT_ENDPOINT_URL = "SERVICE_UNITS_FOR_OBJECT_ENDPOINT_URL";
	private static final String UPRN = "UPRN";
	private static final String USERNAME = "USERNAME";
	private static final String VALID_JSON = "{'1':'one','2':'two'}";

	@InjectMocks
	private EchoWasteService echoWasteService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private EchoCompanyConfiguration mockEchoCompanyConfiguration;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private ServiceUnit mockServiceUnit;

	@Mock
	private ServiceTask mockServiceTask;

	@Mock
	private ServiceTaskSchedule mockServiceTaskSchedule;

	@Mock
	private ScheduledTaskInfo mockScheduledTaskInfo;

	@Mock
	private EchoAxisService mockEchoAxisService;

	@Mock
	Service_Service mockService_Service;

	@Mock
	ServiceHttpsStub mockStub;

	@Mock
	ObjectRef mockObjectRef;

	@Mock
	ServiceUnitQuery mockServiceUnitQuery;

	@Before
	public void setUp() {
		mockStatic(EntityUtils.class, JSONFactoryUtil.class);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenErrorGettingConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(EchoCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		echoWasteService.getConfiguration(COMPANY_ID);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsConfiguration() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(EchoCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockEchoCompanyConfiguration);

		echoWasteService.getConfiguration(COMPANY_ID);
	}

	@Test
	public void getNextDateForServiceUnit_WhenNoServiceTasks_ThenReturnEmptyOptional() {

		ServiceTask[] serviceTasks = new ServiceTask[0];

		Optional<LocalDate> result = echoWasteService.getNextDateForServiceUnit(serviceTasks);

		assertFalse(result.isPresent());
	}

	@Test
	public void getNextDateForServiceUnit_WhenServiceTaskScheduleIsNull_ThenReturnEmptyOptional() {

		ServiceTask[] serviceTasks = new ServiceTask[1];
		serviceTasks[0] = mockServiceTask;

		when(mockServiceTask.getServiceTaskSchedules()).thenReturn(null);

		Optional<LocalDate> result = echoWasteService.getNextDateForServiceUnit(serviceTasks);

		assertFalse(result.isPresent());
	}

	@Test
	public void getNextDateForServiceUnit_WhenNoServiceTaskSchedules_ThenReturnEmptyOptional() {

		ServiceTask[] serviceTasks = new ServiceTask[1];
		serviceTasks[0] = mockServiceTask;

		ServiceTaskSchedule[] serviceTaskSchedules = new ServiceTaskSchedule[0];

		when(mockServiceTask.getServiceTaskSchedules()).thenReturn(serviceTaskSchedules);

		Optional<LocalDate> result = echoWasteService.getNextDateForServiceUnit(serviceTasks);

		assertFalse(result.isPresent());
	}

	@Test
	public void getNextDateForServiceUnit_WhenServiceTaskSchedulesNextInstanceIsNull_ThenReturnEmptyOptional() {

		ServiceTask[] serviceTasks = new ServiceTask[1];
		serviceTasks[0] = mockServiceTask;

		ServiceTaskSchedule[] serviceTaskSchedules = new ServiceTaskSchedule[1];
		serviceTaskSchedules[0] = mockServiceTaskSchedule;

		when(mockServiceTask.getServiceTaskSchedules()).thenReturn(serviceTaskSchedules);
		when(mockServiceTaskSchedule.getNextInstance()).thenReturn(null);

		Optional<LocalDate> result = echoWasteService.getNextDateForServiceUnit(serviceTasks);

		assertFalse(result.isPresent());
	}

	@Test
	public void getNextDateForServiceUnit_WhenServiceTaskSchedulesHasNextInstance_ThenReturnNextDate() {

		ServiceTask[] serviceTasks = new ServiceTask[1];
		serviceTasks[0] = mockServiceTask;

		ServiceTaskSchedule[] serviceTaskSchedules = new ServiceTaskSchedule[1];
		serviceTaskSchedules[0] = mockServiceTaskSchedule;

		when(mockServiceTask.getServiceTaskSchedules()).thenReturn(serviceTaskSchedules);
		when(mockServiceTaskSchedule.getNextInstance()).thenReturn(mockScheduledTaskInfo);

		Calendar now = Calendar.getInstance();
		int offsetMinutes = 60;

		DateTimeOffset dateTimeOffset = new DateTimeOffset();
		dateTimeOffset.setDateTime(now);
		dateTimeOffset.setOffsetMinutes((short) offsetMinutes);
		when(mockScheduledTaskInfo.getCurrentScheduledDate()).thenReturn(dateTimeOffset);

		Optional<LocalDate> result = echoWasteService.getNextDateForServiceUnit(serviceTasks);

		assertTrue(result.isPresent());
		assertEquals(now.toInstant().atZone(ZoneOffset.ofHoursMinutes(offsetMinutes / 60, offsetMinutes % 60)).toLocalDate(), result.get());
	}

	@Test(expected = ConfigurationException.class)
	public void getPassword_WhenPasswordIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockEchoCompanyConfiguration.password()).thenReturn(StringPool.BLANK);

		echoWasteService.getPassword(mockEchoCompanyConfiguration);
	}

	@Test
	public void getPassword_WhenPasswordIsNotBlank_ThenReturnsPassword() throws Exception {
		when(mockEchoCompanyConfiguration.password()).thenReturn(PASSWORD);

		String result = echoWasteService.getPassword(mockEchoCompanyConfiguration);

		assertEquals(PASSWORD, result);
	}

	@Test
	public void getServiceUnitLabel_WhenServiceUnitMapIsNotPresent_ThenReturnServiceName() {
		when(mockServiceUnit.getServiceName()).thenReturn(SERVICE_NAME);

		String result = echoWasteService.getServiceUnitLabel(ID, mockServiceUnit, Optional.empty());

		assertEquals(SERVICE_NAME, result);
	}

	@Test
	public void getServiceUnitLabel_WhenServiceUnitMapIsPresentAndServiceUnitMapDoesNotContainId_ThenReturnServiceName() {
		when(mockServiceUnit.getServiceName()).thenReturn(SERVICE_NAME);
		when(mockJSONObject.has(ID)).thenReturn(false);

		String result = echoWasteService.getServiceUnitLabel(ID, mockServiceUnit, Optional.of(mockJSONObject));

		assertEquals(SERVICE_NAME, result);
	}

	@Test
	public void getServiceUnitLabel_WhenServiceUnitMapIsPresentAndServiceUnitMapDoesContainId_ThenReturnServiceName() {
		when(mockServiceUnit.getServiceName()).thenReturn(SERVICE_NAME);
		when(mockJSONObject.has(ID)).thenReturn(true);
		when(mockJSONObject.getString(ID)).thenReturn(SERVICE_NAME_LABEL);

		String result = echoWasteService.getServiceUnitLabel(ID, mockServiceUnit, Optional.of(mockJSONObject));

		assertEquals(SERVICE_NAME_LABEL, result);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorGettingStub_ThenThrowsWasteRetrievalException() throws Exception {

		int[] serviceIds = { 1, 2 };

		when(mockEchoAxisService.getServiceLocator()).thenReturn(mockService_Service);
		when(mockService_Service.getServiceHttps()).thenThrow(new ServiceException());

		echoWasteService.getServiceUnitsForObject(UPRN, serviceIds, SERVICE_UNITS_FOR_OBJECT_ENDPOINT_URL, USERNAME, PASSWORD);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorSettingSecurityHeader_ThenThrowsWasteRetrievalException() throws Exception {

		int[] serviceIds = { 1, 2 };

		when(mockEchoAxisService.getServiceLocator()).thenReturn(mockService_Service);
		when(mockService_Service.getServiceHttps()).thenReturn(mockStub);

		doThrow(new SOAPException()).when(mockEchoAxisService).setSecurityHeader(mockStub, USERNAME, PASSWORD);

		echoWasteService.getServiceUnitsForObject(UPRN, serviceIds, SERVICE_UNITS_FOR_OBJECT_ENDPOINT_URL, USERNAME, PASSWORD);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorCallingWebService_ThenThrowsWasteRetrievalException() throws Exception {

		int[] serviceIds = { 1, 2 };

		when(mockEchoAxisService.getServiceLocator()).thenReturn(mockService_Service);
		when(mockService_Service.getServiceHttps()).thenReturn(mockStub);

		when(mockEchoAxisService.getObjectRef(UPRN)).thenReturn(mockObjectRef);
		when(mockEchoAxisService.getServiceUnitQuery(serviceIds)).thenReturn(mockServiceUnitQuery);

		when(mockStub.getServiceUnitsForObject(mockObjectRef, mockServiceUnitQuery)).thenThrow(new RemoteException());

		echoWasteService.getServiceUnitsForObject(UPRN, serviceIds, SERVICE_UNITS_FOR_OBJECT_ENDPOINT_URL, USERNAME, PASSWORD);
	}

	@Test
	public void getServiceUnitsForObject_WhenNoErrors_ThenReturnsServiceUnits() throws Exception {

		int[] serviceIds = { 1, 2 };

		ServiceUnit[] serviceUnits = new ServiceUnit[1];
		serviceUnits[0] = new ServiceUnit();

		when(mockEchoAxisService.getServiceLocator()).thenReturn(mockService_Service);
		when(mockService_Service.getServiceHttps()).thenReturn(mockStub);

		when(mockEchoAxisService.getObjectRef(UPRN)).thenReturn(mockObjectRef);
		when(mockEchoAxisService.getServiceUnitQuery(serviceIds)).thenReturn(mockServiceUnitQuery);

		when(mockStub.getServiceUnitsForObject(mockObjectRef, mockServiceUnitQuery)).thenReturn(serviceUnits);

		ServiceUnit[] result = echoWasteService.getServiceUnitsForObject(UPRN, serviceIds, SERVICE_UNITS_FOR_OBJECT_ENDPOINT_URL, USERNAME, PASSWORD);

		assertSame(serviceUnits, result);
	}

	@Test(expected = ConfigurationException.class)
	public void getServiceUnitsForObjectEndpointURL_WhenServiceUnitsForObjectEndpointURLIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockEchoCompanyConfiguration.getServiceUnitsForObjectEndpointURL()).thenReturn(StringPool.BLANK);

		echoWasteService.getServiceUnitsForObjectEndpointURL(mockEchoCompanyConfiguration);
	}

	@Test
	public void getServiceUnitsForObjectEndpointURL_WhenServiceUnitsForObjectEndpointURLIsNotBlank_ThenReturnsServiceUnitsForObjectEndpointURL() throws Exception {
		when(mockEchoCompanyConfiguration.getServiceUnitsForObjectEndpointURL()).thenReturn(SERVICE_UNITS_FOR_OBJECT_ENDPOINT_URL);

		String result = echoWasteService.getServiceUnitsForObjectEndpointURL(mockEchoCompanyConfiguration);

		assertEquals(SERVICE_UNITS_FOR_OBJECT_ENDPOINT_URL, result);
	}

	@Test
	public void getServiceUnitMap_WhenServiceUnitMapIsBlank_ThenReturnsEmptyOptional() throws Exception {
		when(mockEchoCompanyConfiguration.serviceUnitMap()).thenReturn(StringPool.BLANK);

		Optional<JSONObject> result = echoWasteService.getServiceUnitMap(mockEchoCompanyConfiguration);

		assertFalse(result.isPresent());
	}

	@Test(expected = JSONException.class)
	public void getServiceUnitMap_WhenServiceUnitMapContainsInvalidJson_ThenThrowsJSONException() throws Exception {
		when(mockEchoCompanyConfiguration.serviceUnitMap()).thenReturn(INVALID_JSON);

		when(JSONFactoryUtil.createJSONObject(INVALID_JSON)).thenThrow(new JSONException());

		echoWasteService.getServiceUnitMap(mockEchoCompanyConfiguration);
	}

	@Test
	public void getServiceUnitMap_WhenServiceUnitMapIsNotBlankAndIsValidJson_ThenReturnServiceMapOptional() throws Exception {
		when(mockEchoCompanyConfiguration.serviceUnitMap()).thenReturn(VALID_JSON);

		when(JSONFactoryUtil.createJSONObject(VALID_JSON)).thenReturn(mockJSONObject);

		Optional<JSONObject> result = echoWasteService.getServiceUnitMap(mockEchoCompanyConfiguration);

		assertTrue(result.isPresent());
		assertEquals(mockJSONObject, result.get());
	}

	@Test
	public void getServiceUnitIds_WhenServiceUnitMapNotPresent_ThenReturnsEmptyArray() {
		int[] result = echoWasteService.getServiceUnitIds(Optional.empty());

		assertTrue(result.length == 0);
	}

	@Test
	public void getServiceUnitIds_WhenServiceUnitMapIsPresent_ThenReturnsServiceUnitIds() throws JSONException {

		Set<String> keySet = Collections.singleton("1");
		when(mockJSONObject.keySet()).thenReturn(keySet);

		int[] result = echoWasteService.getServiceUnitIds(Optional.of(mockJSONObject));

		assertTrue(result.length == 1);
		assertEquals(1, result[0]);
	}

	@Test(expected = ConfigurationException.class)
	public void getUsername_WhenUsernameIsBlank_ThenThrowsConfigurationException() throws Exception {
		when(mockEchoCompanyConfiguration.username()).thenReturn(StringPool.BLANK);

		echoWasteService.getUsername(mockEchoCompanyConfiguration);
	}

	@Test
	public void getUsername_WhenUsernameIsNotBlank_ThenReturnsUsername() throws Exception {
		when(mockEchoCompanyConfiguration.username()).thenReturn(USERNAME);

		String result = echoWasteService.getUsername(mockEchoCompanyConfiguration);

		assertEquals(USERNAME, result);
	}

	@Test(expected = ConfigurationException.class)
	public void isEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(EchoCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		echoWasteService.isEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(EchoCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockEchoCompanyConfiguration);
		when(mockEchoCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = echoWasteService.isEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

}
