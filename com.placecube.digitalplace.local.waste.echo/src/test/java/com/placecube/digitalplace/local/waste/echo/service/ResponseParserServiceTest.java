package com.placecube.digitalplace.local.waste.echo.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceTask;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit;
import com.placecube.digitalplace.local.waste.model.BinCollection;

@RunWith(PowerMockRunner.class)
public class ResponseParserServiceTest extends PowerMockito {

	private static final int SERVICE_ID = 0;
	private static final String SERVICE_NAME_LABEL = "Service Name";

	@InjectMocks
	ResponseParserService responseParserService;

	@Mock
	private Optional<JSONObject> mockJSONObject;

	@Mock
	private EchoWasteService mockEchoWasteService;

	@Mock
	private ServiceUnit mockServiceUnit;

	@Test
	public void getBinCollections_WhenNoServiceUnits_ThenReturnsEmptySet() {

		ServiceUnit[] serviceUnits = new ServiceUnit[0];

		Set<BinCollection> result = responseParserService.getBinCollections(mockJSONObject, serviceUnits);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getBinCollections_WhenServiceUnitDoesNotHaveNextCollectionDate_ThenDoesNotReturnBinCollection() {

		ServiceUnit[] serviceUnits = new ServiceUnit[1];
		serviceUnits[0] = mockServiceUnit;

		ServiceTask[] serviceTasks = new ServiceTask[0];

		when(mockServiceUnit.getServiceTasks()).thenReturn(serviceTasks);
		when(mockEchoWasteService.getNextDateForServiceUnit(serviceTasks)).thenReturn(Optional.empty());

		Set<BinCollection> result = responseParserService.getBinCollections(mockJSONObject, serviceUnits);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getBinCollections_WhenServiceUnitDoesHaveNextCollectionDate_ThenReturnsBinCollection() {

		ServiceUnit[] serviceUnits = new ServiceUnit[1];
		serviceUnits[0] = mockServiceUnit;

		ServiceTask[] serviceTasks = new ServiceTask[0];
		LocalDate LOCAL_DATE = LocalDate.now();

		when(mockServiceUnit.getServiceTasks()).thenReturn(serviceTasks);
		when(mockEchoWasteService.getNextDateForServiceUnit(serviceTasks)).thenReturn(Optional.of(LOCAL_DATE));

		when(mockServiceUnit.getServiceId()).thenReturn(SERVICE_ID);
		when(mockEchoWasteService.getServiceUnitLabel(Integer.toString(SERVICE_ID), mockServiceUnit, mockJSONObject)).thenReturn(SERVICE_NAME_LABEL);

		Set<BinCollection> result = responseParserService.getBinCollections(mockJSONObject, serviceUnits);

		assertFalse(result.isEmpty());
		BinCollection binCollection = result.stream().findFirst().get();
		assertEquals(binCollection.getName(), Integer.toString(SERVICE_ID));
		assertEquals(binCollection.getCollectionDate(), LOCAL_DATE);
		assertEquals(binCollection.getLabel(LocaleUtil.getDefault()), SERVICE_NAME_LABEL);
	}
}
