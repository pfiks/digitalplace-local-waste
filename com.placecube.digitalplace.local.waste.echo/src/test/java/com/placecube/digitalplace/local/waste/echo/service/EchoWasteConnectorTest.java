package com.placecube.digitalplace.local.waste.echo.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit;
import com.placecube.digitalplace.local.waste.echo.configuration.EchoCompanyConfiguration;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class EchoWasteConnectorTest extends PowerMockito {

	private static final Set<BinCollection> BIN_COLLECTIONS = new HashSet<>();
	private static final long COMPANY_ID = 10;
	private static final String END_POINT_URL = "Url";
	private static final String PASSWORD = "Password";
	private static final int[] SERVICE_IDS = new int[] {};
	private static final ServiceUnit[] SERVICE_UNITS = new ServiceUnit[] {};
	private static final String UPRN = "12345678";
	private static final String USERNAME = "User Name";

	@InjectMocks
	private EchoWasteConnector echoWasteConnector;

	@Mock
	private EchoCompanyConfiguration mockEchoCompanyConfiguration;

	@Mock
	private EchoWasteService mockEchoWasteService;

	@Mock
	private ResponseParserService mockResponseParserService;

	@Mock
	private Optional<JSONObject> mockServiceUnitMap;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockEchoWasteService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = echoWasteConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockEchoWasteService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = echoWasteConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorGettingConfiguration_ThenThrowWasteRetrievalException() throws Exception {
		when(mockEchoWasteService.getConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		echoWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorGettingEndpointURL_ThenThrowWasteRetrievalException() throws Exception {
		when(mockEchoWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockEchoCompanyConfiguration);

		when(mockEchoWasteService.getServiceUnitsForObjectEndpointURL(mockEchoCompanyConfiguration)).thenThrow(new ConfigurationException());

		echoWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorGettingPassword_ThenThrowWasteRetrievalException() throws Exception {
		when(mockEchoWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockEchoCompanyConfiguration);

		when(mockEchoWasteService.getServiceUnitsForObjectEndpointURL(mockEchoCompanyConfiguration)).thenReturn(END_POINT_URL);
		when(mockEchoWasteService.getUsername(mockEchoCompanyConfiguration)).thenReturn(USERNAME);
		when(mockEchoWasteService.getPassword(mockEchoCompanyConfiguration)).thenThrow(new ConfigurationException());

		echoWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorGettingServiceUnitMap_ThenThrowWasteRetrievalException() throws Exception {
		when(mockEchoWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockEchoCompanyConfiguration);

		when(mockEchoWasteService.getServiceUnitsForObjectEndpointURL(mockEchoCompanyConfiguration)).thenReturn(END_POINT_URL);
		when(mockEchoWasteService.getUsername(mockEchoCompanyConfiguration)).thenReturn(USERNAME);
		when(mockEchoWasteService.getPassword(mockEchoCompanyConfiguration)).thenReturn(PASSWORD);
		when(mockEchoWasteService.getServiceUnitMap(mockEchoCompanyConfiguration)).thenThrow(new JSONException());

		echoWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorGettingServiceUnitsForObject_ThenThrowWasteRetrievalException() throws Exception {
		when(mockEchoWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockEchoCompanyConfiguration);

		when(mockEchoWasteService.getServiceUnitsForObjectEndpointURL(mockEchoCompanyConfiguration)).thenReturn(END_POINT_URL);
		when(mockEchoWasteService.getUsername(mockEchoCompanyConfiguration)).thenReturn(USERNAME);
		when(mockEchoWasteService.getPassword(mockEchoCompanyConfiguration)).thenReturn(PASSWORD);
		when(mockEchoWasteService.getServiceUnitMap(mockEchoCompanyConfiguration)).thenReturn(mockServiceUnitMap);

		when(mockEchoWasteService.getServiceUnitIds(mockServiceUnitMap)).thenReturn(SERVICE_IDS);

		when(mockEchoWasteService.getServiceUnitsForObject(UPRN, SERVICE_IDS, END_POINT_URL, USERNAME, PASSWORD)).thenThrow(new WasteRetrievalException());

		echoWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test(expected = WasteRetrievalException.class)
	public void getServiceUnitsForObject_WhenErrorGettingUsername_ThenThrowWasteRetrievalException() throws Exception {
		when(mockEchoWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockEchoCompanyConfiguration);

		when(mockEchoWasteService.getServiceUnitsForObjectEndpointURL(mockEchoCompanyConfiguration)).thenReturn(END_POINT_URL);
		when(mockEchoWasteService.getUsername(mockEchoCompanyConfiguration)).thenThrow(new ConfigurationException());

		echoWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);
	}

	@Test
	public void getServiceUnitsForObject_WhenNoErrors_ThenReturnBinCollections() throws Exception {

		when(mockEchoWasteService.getConfiguration(COMPANY_ID)).thenReturn(mockEchoCompanyConfiguration);

		when(mockEchoWasteService.getServiceUnitsForObjectEndpointURL(mockEchoCompanyConfiguration)).thenReturn(END_POINT_URL);
		when(mockEchoWasteService.getUsername(mockEchoCompanyConfiguration)).thenReturn(USERNAME);
		when(mockEchoWasteService.getPassword(mockEchoCompanyConfiguration)).thenReturn(PASSWORD);
		when(mockEchoWasteService.getServiceUnitMap(mockEchoCompanyConfiguration)).thenReturn(mockServiceUnitMap);

		when(mockEchoWasteService.getServiceUnitIds(mockServiceUnitMap)).thenReturn(SERVICE_IDS);

		when(mockEchoWasteService.getServiceUnitsForObject(UPRN, SERVICE_IDS, END_POINT_URL, USERNAME, PASSWORD)).thenReturn(SERVICE_UNITS);
		when(mockResponseParserService.getBinCollections(mockServiceUnitMap, SERVICE_UNITS)).thenReturn(BIN_COLLECTIONS);

		Set<BinCollection> result = echoWasteConnector.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN);

		assertEquals(BIN_COLLECTIONS, result);
	}
}
