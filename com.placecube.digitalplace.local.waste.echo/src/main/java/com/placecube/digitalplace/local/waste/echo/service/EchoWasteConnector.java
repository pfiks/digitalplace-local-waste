package com.placecube.digitalplace.local.waste.echo.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit;
import com.placecube.digitalplace.local.waste.echo.configuration.EchoCompanyConfiguration;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.service.WasteConnector;

@Component(immediate = true, service = WasteConnector.class)
public class EchoWasteConnector implements WasteConnector {

	private static final Log LOG = LogFactoryUtil.getLog(EchoWasteConnector.class);

	@Reference
	private EchoWasteService echoWasteService;

	@Reference
	private ResponseParserService responseParserService;

	@Override
	public String createAssistedBinCollectionJob(long companyId, AssistedBinCollectionJob assistedBinCollectionJob) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public WasteSubscriptionResponse createBinSubscription(long companyId, String uprn, String serviceType, String firstName, String lastName, String emailAddress, String telephone, int numberOfBins)
			throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, List<String> itemsToBeCollected,
			String locationOfItems, Date collectionDate) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createFlyTippingReport(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, String typeOfRubbish, String sizeOfRubbish,
			String details, String coordinates, String locationDetails, String dateOfIncident) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public WasteSubscriptionResponse createGardenWasteSubscription(long companyId, GardenWasteSubscription gardenWasteSubscription) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createMissedBinCollectionJob(long companyId, MissedBinCollectionJob missedBinCollectionJob) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createNewWasteContainerRequest(long companyId, NewContainerRequest newContainerRequest) throws WasteRetrievalException {
		return "REF-12345678";
	}

	@Override
	public boolean enabled(long companyId) {
		try {
			return echoWasteService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public List<WasteSubscriptionResponse> getAllWasteSubscriptions(long companyId, SubscriptionType subscriptionType) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRN(long companyId, String uprn) throws WasteRetrievalException {

		try {

			EchoCompanyConfiguration configuration = echoWasteService.getConfiguration(companyId);

			String endpointURL = echoWasteService.getServiceUnitsForObjectEndpointURL(configuration);
			String username = echoWasteService.getUsername(configuration);
			String password = echoWasteService.getPassword(configuration);
			Optional<JSONObject> serviceUnitMap = echoWasteService.getServiceUnitMap(configuration);

			int[] serviceIds = echoWasteService.getServiceUnitIds(serviceUnitMap);

			ServiceUnit[] serviceUnits = echoWasteService.getServiceUnitsForObject(uprn, serviceIds, endpointURL, username, password);
			return responseParserService.getBinCollections(serviceUnitMap, serviceUnits);

		} catch (Exception e) {
			LOG.error(e);
			throw new WasteRetrievalException("Error getting bin collection dates: " + e.getMessage());
		}
	}

	@Override
	public Optional<BulkyCollectionDate> getBulkyCollectionDateByUPRN(long companyId, String uprn) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String getMissedBinCollectionResponse(long companyId, String uprn, String date) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String getName(long companyId) {
		return "Waste - Echo";
	}

	@Override
	public int howManyBinsSubscribed(long companyId, String uprn, String serviceType) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public int howManyBinsSubscribedForPeriod(long companyId, String uprn, String serviceType, String startDate, String endDate) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String binType, String formInstanceId)
			throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, BulkyWasteCollectionRequest bulkyWasteCollectionRequest, Date date) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public List<Date> getBulkyCollectionSlotsByUPRNAndService(long companyId, String uprn, String serviceId, int numberOfDatesToReturn) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForWasteContainerRequest(long companyId, String uprn, String binType, String binSize, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForBulkyWasteCollection(long companyId, String uprn, List<String> itemsForCollection, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public boolean isPropertyEligibleForMissedBinCollectionReport(long companyId, String uprn, String binType, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRNAndService(long companyId, String uprn, String serviceName, String formInstanceId) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

	@Deprecated
	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) throws WasteRetrievalException {
		throw new WasteRetrievalException("Not implemented yet!");
	}

}
