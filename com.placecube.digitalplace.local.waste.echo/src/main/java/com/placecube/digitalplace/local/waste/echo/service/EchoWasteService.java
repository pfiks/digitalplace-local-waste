package com.placecube.digitalplace.local.waste.echo.service;

import org.apache.axis.client.Call;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Optional;

import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceHttpsStub;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceTask;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceTaskSchedule;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit;
import com.placecube.digitalplace.local.waste.echo.axis.Service_Service;
import com.placecube.digitalplace.local.waste.echo.axis.util.DateTimeOffset;
import com.placecube.digitalplace.local.waste.echo.configuration.EchoCompanyConfiguration;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;

@Component(immediate = true, service = EchoWasteService.class)
public class EchoWasteService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private EchoAxisService echoAxisService;

	public EchoCompanyConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(EchoCompanyConfiguration.class, companyId);
	}

	public Optional<LocalDate> getNextDateForServiceUnit(ServiceTask... serviceTasks) {
		for (ServiceTask serviceTask : serviceTasks) {
			ServiceTaskSchedule[] serviceTaskSchedules = serviceTask.getServiceTaskSchedules();
			if (serviceTaskSchedules != null) {
				for (ServiceTaskSchedule serviceTaskSchedule : serviceTaskSchedules) {
					if (serviceTaskSchedule.getNextInstance() != null) {
						DateTimeOffset dateTimeOffset = serviceTaskSchedule.getNextInstance().getCurrentScheduledDate();
						short offsetMinutes = dateTimeOffset.getOffsetMinutes();
						return Optional.of(dateTimeOffset.getDateTime().toInstant().atZone(ZoneOffset.ofHoursMinutes(offsetMinutes / 60, offsetMinutes % 60)).toLocalDate());
					}
				}
			}
		}
		return Optional.empty();
	}

	public String getPassword(EchoCompanyConfiguration configuration) throws ConfigurationException {
		String password = configuration.password();

		if (Validator.isNull(password)) {
			throw new ConfigurationException("Password configuration cannot be empty");
		}

		return password;
	}

	public String getServiceUnitLabel(String id, ServiceUnit serviceUnit, Optional<JSONObject> serviceUnitMap) {
		String label = serviceUnit.getServiceName();

		if (serviceUnitMap.isPresent() && serviceUnitMap.get().has(id)) {
			label = serviceUnitMap.get().getString(id);
		}

		return label;
	}

	public ServiceUnit[] getServiceUnitsForObject(String uprn, int[] serviceIds, String endpointURL, String username, String password) throws WasteRetrievalException {
		try {
			Service_Service service = echoAxisService.getServiceLocator();
			ServiceHttpsStub stub = (ServiceHttpsStub) service.getServiceHttps();

			echoAxisService.setActionHeader(stub);
			echoAxisService.setToHeader(stub);
			echoAxisService.setSecurityHeader(stub, username, password);

			stub._setProperty(Call.CHECK_MUST_UNDERSTAND, Boolean.FALSE);

			return stub.getServiceUnitsForObject(echoAxisService.getObjectRef(uprn), echoAxisService.getServiceUnitQuery(serviceIds));

		} catch (ServiceException | SOAPException | RemoteException e) {
			throw new WasteRetrievalException("Error calling the echo web service: " + e.getMessage(), e);
		}
	}

	public String getServiceUnitsForObjectEndpointURL(EchoCompanyConfiguration configuration) throws ConfigurationException {
		String url = configuration.getServiceUnitsForObjectEndpointURL();

		if (Validator.isNull(url)) {
			throw new ConfigurationException("ServiceUnitsForObject Endpoint URL configuration cannot be empty");
		}

		return url;
	}

	public int[] getServiceUnitIds(Optional<JSONObject> serviceUnitMap) {
		if (serviceUnitMap.isPresent()) {
			return serviceUnitMap.get().keySet().stream().mapToInt((String i) -> Integer.parseInt(i)).toArray();
		} else {
			return new int[0];
		}
	}

	public Optional<JSONObject> getServiceUnitMap(EchoCompanyConfiguration configuration) throws JSONException {
		String jsonMap = configuration.serviceUnitMap();

		if (Validator.isNotNull(jsonMap)) {
			return Optional.of(JSONFactoryUtil.createJSONObject(jsonMap));
		} else {
			return Optional.empty();
		}
	}

	public String getUsername(EchoCompanyConfiguration configuration) throws ConfigurationException {
		String username = configuration.username();

		if (Validator.isNull(username)) {
			throw new ConfigurationException("Username configuration cannot be empty");
		}

		return username;
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		EchoCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(EchoCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}
}
