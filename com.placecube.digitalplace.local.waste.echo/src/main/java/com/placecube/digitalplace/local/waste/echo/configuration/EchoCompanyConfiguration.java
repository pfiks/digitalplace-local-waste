package com.placecube.digitalplace.local.waste.echo.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.waste.echo.configuration.EchoCompanyConfiguration", localization = "content/Language", name = "waste-echo")
public interface EchoCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "https://int.veolia.echoweb.co.uk/api/v1/service.svc/https", name = "get-service-units-for-object-endpoint-url")
	String getServiceUnitsForObjectEndpointURL();

	@Meta.AD(required = false, deflt = "", name = "username")
	String username();

	@Meta.AD(required = false, deflt = "", name = "password", type = Meta.Type.Password)
	String password();

	@Meta.AD(required = false, deflt = "", name = "service-unit-map")
	String serviceUnitMap();
}
