package com.placecube.digitalplace.local.waste.echo.service;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;

import org.apache.axis.message.SOAPHeaderElement;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.ArrayUtil;
import com.placecube.digitalplace.local.waste.echo.axis.ObjectRef;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceHttpsStub;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnitQuery;
import com.placecube.digitalplace.local.waste.echo.axis.Service_Service;
import com.placecube.digitalplace.local.waste.echo.axis.Service_ServiceLocator;
import com.placecube.digitalplace.local.waste.echo.axis.constants.ObjectRefKey;
import com.placecube.digitalplace.local.waste.echo.axis.constants.ObjectRefType;

@Component(immediate = true, service = EchoAxisService.class)
public class EchoAxisService {

	public ObjectRef getObjectRef(String uprn) {
		ObjectRef objectRef = new ObjectRef();

		objectRef.setType(ObjectRefType.POINT_ADDRESS.getStrVal());
		objectRef.setKey(ObjectRefKey.UPRN.getStrVal());
		objectRef.setValue(new Object[] { uprn });

		return objectRef;
	}

	public Service_Service getServiceLocator() {
		return new Service_ServiceLocator();
	}

	public ServiceUnitQuery getServiceUnitQuery(int[] serviceIds) {
		ServiceUnitQuery serviceUnitQuery = new ServiceUnitQuery();

		if (ArrayUtil.isNotEmpty(serviceIds)) {
			serviceUnitQuery.setServiceIds(serviceIds);
		}

		serviceUnitQuery.setIncludeTaskInstances(true);

		return serviceUnitQuery;
	}

	public void setActionHeader(ServiceHttpsStub stub) {
		SOAPHeaderElement action = new SOAPHeaderElement(new QName("wsa:Action"), "http://www.twistedfish.com/xmlns/echo/api/v1/Service/GetServiceUnitsForObject");
		action.setActor(null);
		action.setNamespaceURI("http://www.w3.org/2005/08/addressing");
		stub.setHeader(action);
	}

	public void setSecurityHeader(ServiceHttpsStub stub, String username, String password) throws SOAPException {
		QName headerName = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security");
		SOAPHeaderElement header = new SOAPHeaderElement(headerName);
		header.setActor(null);
		header.setPrefix("wsse");
		header.setMustUnderstand(true);

		SOAPElement utElem = header.addChildElement("UsernameToken");
		SOAPElement userNameElem = utElem.addChildElement("Username");
		userNameElem.setValue(username);
		SOAPElement passwordElem = utElem.addChildElement("Password");
		passwordElem.setValue(password);

		stub.setHeader(header);
	}

	public void setToHeader(ServiceHttpsStub stub) {
		SOAPHeaderElement to = new SOAPHeaderElement(new QName("wsa:To"), stub._getProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY));
		to.setActor(null);
		to.setNamespaceURI("http://www.w3.org/2005/08/addressing");
		stub.setHeader(to);
	}
}