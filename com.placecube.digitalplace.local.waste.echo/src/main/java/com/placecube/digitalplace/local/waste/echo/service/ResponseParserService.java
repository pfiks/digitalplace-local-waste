package com.placecube.digitalplace.local.waste.echo.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceTask;
import com.placecube.digitalplace.local.waste.echo.axis.ServiceUnit;
import com.placecube.digitalplace.local.waste.model.BinCollection;

@Component(immediate = true, service = ResponseParserService.class)
public class ResponseParserService {

	@Reference
	private EchoWasteService echoWasteService;

	public Set<BinCollection> getBinCollections(Optional<JSONObject> serviceUnitMap, ServiceUnit... serviceUnits) {

		Set<BinCollection> binCollections = new LinkedHashSet<>();

		for (ServiceUnit serviceUnit : serviceUnits) {

			ServiceTask[] serviceTasks = serviceUnit.getServiceTasks();
			Optional<LocalDate> nextCollectionDate = echoWasteService.getNextDateForServiceUnit(serviceTasks);

			if (nextCollectionDate.isPresent()) {
				String id = Integer.toString(serviceUnit.getServiceId());

				String label = echoWasteService.getServiceUnitLabel(id, serviceUnit, serviceUnitMap);

				Map<Locale, String> labelMap = new HashMap<>();
				labelMap.put(LocaleUtil.getDefault(), label);

				String frequency = "Collection is weekly on a Wednesday";

				LocalDate followingCollectionDate = nextCollectionDate.get().plusWeeks(1);

				BinCollection binCollection = new BinCollection.BinCollectionBuilder(id, nextCollectionDate.get(), labelMap).frequency(frequency).followingCollectionDate(followingCollectionDate)
						.build();

				binCollections.add(binCollection);
			}
		}

		return binCollections;
	}

}
