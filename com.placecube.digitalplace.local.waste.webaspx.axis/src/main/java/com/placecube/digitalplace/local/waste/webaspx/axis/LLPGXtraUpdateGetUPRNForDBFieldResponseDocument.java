/*
 * An XML document type.
 * Localname: LLPGXtraUpdateGetUPRNForDBFieldResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LLPGXtraUpdateGetUPRNForDBFieldResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one LLPGXtraUpdateGetUPRNForDBFieldResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface LLPGXtraUpdateGetUPRNForDBFieldResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtraupdategetuprnfordbfieldresponse5500doctype");
    
    /**
     * Gets the "LLPGXtraUpdateGetUPRNForDBFieldResponse" element
     */
    LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse getLLPGXtraUpdateGetUPRNForDBFieldResponse();
    
    /**
     * Sets the "LLPGXtraUpdateGetUPRNForDBFieldResponse" element
     */
    void setLLPGXtraUpdateGetUPRNForDBFieldResponse(LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse llpgXtraUpdateGetUPRNForDBFieldResponse);
    
    /**
     * Appends and returns a new empty "LLPGXtraUpdateGetUPRNForDBFieldResponse" element
     */
    LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse addNewLLPGXtraUpdateGetUPRNForDBFieldResponse();
    
    /**
     * An XML LLPGXtraUpdateGetUPRNForDBFieldResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface LLPGXtraUpdateGetUPRNForDBFieldResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraUpdateGetUPRNForDBFieldResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtraupdategetuprnfordbfieldresponsea8e2elemtype");
        
        /**
         * Gets the "LLPGXtraUpdateGetUPRNForDBFieldResult" element
         */
        LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult getLLPGXtraUpdateGetUPRNForDBFieldResult();
        
        /**
         * True if has "LLPGXtraUpdateGetUPRNForDBFieldResult" element
         */
        boolean isSetLLPGXtraUpdateGetUPRNForDBFieldResult();
        
        /**
         * Sets the "LLPGXtraUpdateGetUPRNForDBFieldResult" element
         */
        void setLLPGXtraUpdateGetUPRNForDBFieldResult(LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult llpgXtraUpdateGetUPRNForDBFieldResult);
        
        /**
         * Appends and returns a new empty "LLPGXtraUpdateGetUPRNForDBFieldResult" element
         */
        LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult addNewLLPGXtraUpdateGetUPRNForDBFieldResult();
        
        /**
         * Unsets the "LLPGXtraUpdateGetUPRNForDBFieldResult" element
         */
        void unsetLLPGXtraUpdateGetUPRNForDBFieldResult();
        
        /**
         * An XML LLPGXtraUpdateGetUPRNForDBFieldResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface LLPGXtraUpdateGetUPRNForDBFieldResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraUpdateGetUPRNForDBFieldResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtraupdategetuprnfordbfieldresult6260elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult newInstance() {
                  return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse newInstance() {
              return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument newInstance() {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static LLPGXtraUpdateGetUPRNForDBFieldResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
