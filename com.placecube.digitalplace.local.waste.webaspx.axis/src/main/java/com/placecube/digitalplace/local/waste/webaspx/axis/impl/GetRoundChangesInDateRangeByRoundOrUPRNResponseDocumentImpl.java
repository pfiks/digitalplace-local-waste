/*
 * An XML document type.
 * Localname: GetRoundChangesInDateRangeByRoundOrUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument;

/**
 * A document containing one
 * GetRoundChangesInDateRangeByRoundOrUPRNResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetRoundChangesInDateRangeByRoundOrUPRNResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
		implements GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument {

	/**
	 * An XML
	 * GetRoundChangesInDateRangeByRoundOrUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetRoundChangesInDateRangeByRoundOrUPRNResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse {

		/**
		 * An XML
		 * GetRoundChangesInDateRangeByRoundOrUPRNResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetRoundChangesInDateRangeByRoundOrUPRNResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult {

			private static final long serialVersionUID = 1L;

			public GetRoundChangesInDateRangeByRoundOrUPRNResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"GetRoundChangesInDateRangeByRoundOrUPRNResult");

		private static final long serialVersionUID = 1L;

		public GetRoundChangesInDateRangeByRoundOrUPRNResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
		 */
		@Override
		public GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult addNewGetRoundChangesInDateRangeByRoundOrUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult target = null;
				target = (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult) get_store()
						.add_element_user(GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
		 */
		@Override
		public GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult getGetRoundChangesInDateRangeByRoundOrUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult target = null;
				target = (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult) get_store()
						.find_element_user(GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
		 */
		@Override
		public boolean isSetGetRoundChangesInDateRangeByRoundOrUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
		 */
		@Override
		public void setGetRoundChangesInDateRangeByRoundOrUPRNResult(
				GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult getRoundChangesInDateRangeByRoundOrUPRNResult) {
			generatedSetterHelperImpl(getRoundChangesInDateRangeByRoundOrUPRNResult, GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
		 */
		@Override
		public void unsetGetRoundChangesInDateRangeByRoundOrUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"GetRoundChangesInDateRangeByRoundOrUPRNResponse");

	private static final long serialVersionUID = 1L;

	public GetRoundChangesInDateRangeByRoundOrUPRNResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "GetRoundChangesInDateRangeByRoundOrUPRNResponse" element
	 */
	@Override
	public GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse addNewGetRoundChangesInDateRangeByRoundOrUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse target = null;
			target = (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse) get_store()
					.add_element_user(GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetRoundChangesInDateRangeByRoundOrUPRNResponse" element
	 */
	@Override
	public GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse getGetRoundChangesInDateRangeByRoundOrUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse target = null;
			target = (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse) get_store()
					.find_element_user(GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetRoundChangesInDateRangeByRoundOrUPRNResponse" element
	 */
	@Override
	public void setGetRoundChangesInDateRangeByRoundOrUPRNResponse(
			GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse getRoundChangesInDateRangeByRoundOrUPRNResponse) {
		generatedSetterHelperImpl(getRoundChangesInDateRangeByRoundOrUPRNResponse, GETROUNDCHANGESINDATERANGEBYROUNDORUPRNRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
