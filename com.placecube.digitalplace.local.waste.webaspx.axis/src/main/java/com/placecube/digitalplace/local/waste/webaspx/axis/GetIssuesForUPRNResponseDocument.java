/*
 * An XML document type.
 * Localname: GetIssuesForUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetIssuesForUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetIssuesForUPRNResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetIssuesForUPRNResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetIssuesForUPRNResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getissuesforuprnresponsebfeddoctype");
    
    /**
     * Gets the "GetIssuesForUPRNResponse" element
     */
    GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse getGetIssuesForUPRNResponse();
    
    /**
     * Sets the "GetIssuesForUPRNResponse" element
     */
    void setGetIssuesForUPRNResponse(GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse getIssuesForUPRNResponse);
    
    /**
     * Appends and returns a new empty "GetIssuesForUPRNResponse" element
     */
    GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse addNewGetIssuesForUPRNResponse();
    
    /**
     * An XML GetIssuesForUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetIssuesForUPRNResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetIssuesForUPRNResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getissuesforuprnresponse96e0elemtype");
        
        /**
         * Gets the "GetIssuesForUPRNResult" element
         */
        GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult getGetIssuesForUPRNResult();
        
        /**
         * True if has "GetIssuesForUPRNResult" element
         */
        boolean isSetGetIssuesForUPRNResult();
        
        /**
         * Sets the "GetIssuesForUPRNResult" element
         */
        void setGetIssuesForUPRNResult(GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult getIssuesForUPRNResult);
        
        /**
         * Appends and returns a new empty "GetIssuesForUPRNResult" element
         */
        GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult addNewGetIssuesForUPRNResult();
        
        /**
         * Unsets the "GetIssuesForUPRNResult" element
         */
        void unsetGetIssuesForUPRNResult();
        
        /**
         * An XML GetIssuesForUPRNResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetIssuesForUPRNResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetIssuesForUPRNResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getissuesforuprnresultcfe9elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult newInstance() {
                  return (GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse newInstance() {
              return (GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetIssuesForUPRNResponseDocument newInstance() {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetIssuesForUPRNResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetIssuesForUPRNResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetIssuesForUPRNResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetIssuesForUPRNResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetIssuesForUPRNResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetIssuesForUPRNResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetIssuesForUPRNResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetIssuesForUPRNResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetIssuesForUPRNResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetIssuesForUPRNResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetIssuesForUPRNResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetIssuesForUPRNResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetIssuesForUPRNResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetIssuesForUPRNResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetIssuesForUPRNResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetIssuesForUPRNResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetIssuesForUPRNResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetIssuesForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
