/*
 * An XML document type.
 * Localname: WriteIncabLiveDataResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: WriteIncabLiveDataResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.WriteIncabLiveDataResponseDocument;

/**
 * A document containing one
 * WriteIncabLiveDataResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class WriteIncabLiveDataResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements WriteIncabLiveDataResponseDocument {

	/**
	 * An XML
	 * WriteIncabLiveDataResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class WriteIncabLiveDataResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse {

		/**
		 * An XML
		 * WriteIncabLiveDataResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class WriteIncabLiveDataResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult {

			private static final long serialVersionUID = 1L;

			public WriteIncabLiveDataResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final long serialVersionUID = 1L;

		private static final javax.xml.namespace.QName WRITEINCABLIVEDATARESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "WriteIncabLiveDataResult");

		public WriteIncabLiveDataResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "WriteIncabLiveDataResult" element
		 */
		@Override
		public WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult addNewWriteIncabLiveDataResult() {
			synchronized (monitor()) {
				check_orphaned();
				WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult target = null;
				target = (WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult) get_store().add_element_user(WRITEINCABLIVEDATARESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "WriteIncabLiveDataResult" element
		 */
		@Override
		public WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult getWriteIncabLiveDataResult() {
			synchronized (monitor()) {
				check_orphaned();
				WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult target = null;
				target = (WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult) get_store().find_element_user(WRITEINCABLIVEDATARESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "WriteIncabLiveDataResult" element
		 */
		@Override
		public boolean isSetWriteIncabLiveDataResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WRITEINCABLIVEDATARESULT$0) != 0;
			}
		}

		/**
		 * Sets the "WriteIncabLiveDataResult" element
		 */
		@Override
		public void setWriteIncabLiveDataResult(WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult writeIncabLiveDataResult) {
			generatedSetterHelperImpl(writeIncabLiveDataResult, WRITEINCABLIVEDATARESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "WriteIncabLiveDataResult" element
		 */
		@Override
		public void unsetWriteIncabLiveDataResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WRITEINCABLIVEDATARESULT$0, 0);
			}
		}
	}

	private static final long serialVersionUID = 1L;

	private static final javax.xml.namespace.QName WRITEINCABLIVEDATARESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "WriteIncabLiveDataResponse");

	public WriteIncabLiveDataResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "WriteIncabLiveDataResponse" element
	 */
	@Override
	public WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse addNewWriteIncabLiveDataResponse() {
		synchronized (monitor()) {
			check_orphaned();
			WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse target = null;
			target = (WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse) get_store().add_element_user(WRITEINCABLIVEDATARESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "WriteIncabLiveDataResponse" element
	 */
	@Override
	public WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse getWriteIncabLiveDataResponse() {
		synchronized (monitor()) {
			check_orphaned();
			WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse target = null;
			target = (WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse) get_store().find_element_user(WRITEINCABLIVEDATARESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "WriteIncabLiveDataResponse" element
	 */
	@Override
	public void setWriteIncabLiveDataResponse(WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse writeIncabLiveDataResponse) {
		generatedSetterHelperImpl(writeIncabLiveDataResponse, WRITEINCABLIVEDATARESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
