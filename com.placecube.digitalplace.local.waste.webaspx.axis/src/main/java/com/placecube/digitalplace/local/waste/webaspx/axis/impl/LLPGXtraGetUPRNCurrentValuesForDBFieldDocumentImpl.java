/*
 * An XML document type.
 * Localname: LLPGXtraGetUPRNCurrentValuesForDBField
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LLPGXtraGetUPRNCurrentValuesForDBFieldDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldDocument;

/**
 * A document containing one
 * LLPGXtraGetUPRNCurrentValuesForDBField(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class LLPGXtraGetUPRNCurrentValuesForDBFieldDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements LLPGXtraGetUPRNCurrentValuesForDBFieldDocument {

	/**
	 * An XML
	 * LLPGXtraGetUPRNCurrentValuesForDBField(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class LLPGXtraGetUPRNCurrentValuesForDBFieldImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName DATABASEFIELDNAME$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "databaseFieldName");

		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName UPRNS$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRNs");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public LLPGXtraGetUPRNCurrentValuesForDBFieldImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "databaseFieldName" element
		 */
		@Override
		public java.lang.String getDatabaseFieldName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATABASEFIELDNAME$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRNs" element
		 */
		@Override
		public java.lang.String getUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRNS$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "databaseFieldName" element
		 */
		@Override
		public boolean isSetDatabaseFieldName() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DATABASEFIELDNAME$10) != 0;
			}
		}

		/**
		 * True if has "UPRNs" element
		 */
		@Override
		public boolean isSetUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRNS$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "databaseFieldName" element
		 */
		@Override
		public void setDatabaseFieldName(java.lang.String databaseFieldName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATABASEFIELDNAME$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DATABASEFIELDNAME$10);
				}
				target.setStringValue(databaseFieldName);
			}
		}

		/**
		 * Sets the "UPRNs" element
		 */
		@Override
		public void setUPRNs(java.lang.String uprNs) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRNS$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRNS$8);
				}
				target.setStringValue(uprNs);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "databaseFieldName" element
		 */
		@Override
		public void unsetDatabaseFieldName() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DATABASEFIELDNAME$10, 0);
			}
		}

		/**
		 * Unsets the "UPRNs" element
		 */
		@Override
		public void unsetUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRNS$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "databaseFieldName" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDatabaseFieldName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATABASEFIELDNAME$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRNs" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRNS$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "databaseFieldName" element
		 */
		@Override
		public void xsetDatabaseFieldName(org.apache.xmlbeans.XmlString databaseFieldName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATABASEFIELDNAME$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DATABASEFIELDNAME$10);
				}
				target.set(databaseFieldName);
			}
		}

		/**
		 * Sets (as xml) the "UPRNs" element
		 */
		@Override
		public void xsetUPRNs(org.apache.xmlbeans.XmlString uprNs) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRNS$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRNS$8);
				}
				target.set(uprNs);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELD$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"LLPGXtraGetUPRNCurrentValuesForDBField");

	private static final long serialVersionUID = 1L;

	public LLPGXtraGetUPRNCurrentValuesForDBFieldDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "LLPGXtraGetUPRNCurrentValuesForDBField"
	 * element
	 */
	@Override
	public LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField addNewLLPGXtraGetUPRNCurrentValuesForDBField() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField target = null;
			target = (LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField) get_store().add_element_user(LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELD$0);
			return target;
		}
	}

	/**
	 * Gets the "LLPGXtraGetUPRNCurrentValuesForDBField" element
	 */
	@Override
	public LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField getLLPGXtraGetUPRNCurrentValuesForDBField() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField target = null;
			target = (LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField) get_store().find_element_user(LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELD$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "LLPGXtraGetUPRNCurrentValuesForDBField" element
	 */
	@Override
	public void setLLPGXtraGetUPRNCurrentValuesForDBField(LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.LLPGXtraGetUPRNCurrentValuesForDBField llpgXtraGetUPRNCurrentValuesForDBField) {
		generatedSetterHelperImpl(llpgXtraGetUPRNCurrentValuesForDBField, LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELD$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
