/*
 * An XML document type.
 * Localname: LLPGXtraUpdateGetUPRNForDBFieldResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LLPGXtraUpdateGetUPRNForDBFieldResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetUPRNForDBFieldResponseDocument;

/**
 * A document containing one
 * LLPGXtraUpdateGetUPRNForDBFieldResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class LLPGXtraUpdateGetUPRNForDBFieldResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements LLPGXtraUpdateGetUPRNForDBFieldResponseDocument {

	/**
	 * An XML
	 * LLPGXtraUpdateGetUPRNForDBFieldResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class LLPGXtraUpdateGetUPRNForDBFieldResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse {

		/**
		 * An XML
		 * LLPGXtraUpdateGetUPRNForDBFieldResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class LLPGXtraUpdateGetUPRNForDBFieldResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult {

			private static final long serialVersionUID = 1L;

			public LLPGXtraUpdateGetUPRNForDBFieldResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName LLPGXTRAUPDATEGETUPRNFORDBFIELDRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"LLPGXtraUpdateGetUPRNForDBFieldResult");

		private static final long serialVersionUID = 1L;

		public LLPGXtraUpdateGetUPRNForDBFieldResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "LLPGXtraUpdateGetUPRNForDBFieldResult" element
		 */
		@Override
		public LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult addNewLLPGXtraUpdateGetUPRNForDBFieldResult() {
			synchronized (monitor()) {
				check_orphaned();
				LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult target = null;
				target = (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult) get_store()
						.add_element_user(LLPGXTRAUPDATEGETUPRNFORDBFIELDRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "LLPGXtraUpdateGetUPRNForDBFieldResult" element
		 */
		@Override
		public LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult getLLPGXtraUpdateGetUPRNForDBFieldResult() {
			synchronized (monitor()) {
				check_orphaned();
				LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult target = null;
				target = (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult) get_store()
						.find_element_user(LLPGXTRAUPDATEGETUPRNFORDBFIELDRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "LLPGXtraUpdateGetUPRNForDBFieldResult" element
		 */
		@Override
		public boolean isSetLLPGXtraUpdateGetUPRNForDBFieldResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(LLPGXTRAUPDATEGETUPRNFORDBFIELDRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "LLPGXtraUpdateGetUPRNForDBFieldResult" element
		 */
		@Override
		public void setLLPGXtraUpdateGetUPRNForDBFieldResult(
				LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse.LLPGXtraUpdateGetUPRNForDBFieldResult llpgXtraUpdateGetUPRNForDBFieldResult) {
			generatedSetterHelperImpl(llpgXtraUpdateGetUPRNForDBFieldResult, LLPGXTRAUPDATEGETUPRNFORDBFIELDRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "LLPGXtraUpdateGetUPRNForDBFieldResult" element
		 */
		@Override
		public void unsetLLPGXtraUpdateGetUPRNForDBFieldResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(LLPGXTRAUPDATEGETUPRNFORDBFIELDRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName LLPGXTRAUPDATEGETUPRNFORDBFIELDRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"LLPGXtraUpdateGetUPRNForDBFieldResponse");

	private static final long serialVersionUID = 1L;

	public LLPGXtraUpdateGetUPRNForDBFieldResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "LLPGXtraUpdateGetUPRNForDBFieldResponse"
	 * element
	 */
	@Override
	public LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse addNewLLPGXtraUpdateGetUPRNForDBFieldResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse target = null;
			target = (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse) get_store().add_element_user(LLPGXTRAUPDATEGETUPRNFORDBFIELDRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "LLPGXtraUpdateGetUPRNForDBFieldResponse" element
	 */
	@Override
	public LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse getLLPGXtraUpdateGetUPRNForDBFieldResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse target = null;
			target = (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse) get_store().find_element_user(LLPGXTRAUPDATEGETUPRNFORDBFIELDRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "LLPGXtraUpdateGetUPRNForDBFieldResponse" element
	 */
	@Override
	public void setLLPGXtraUpdateGetUPRNForDBFieldResponse(LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.LLPGXtraUpdateGetUPRNForDBFieldResponse llpgXtraUpdateGetUPRNForDBFieldResponse) {
		generatedSetterHelperImpl(llpgXtraUpdateGetUPRNForDBFieldResponse, LLPGXTRAUPDATEGETUPRNFORDBFIELDRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
