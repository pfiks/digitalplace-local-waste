/*
 * An XML document type.
 * Localname: AHP_ExtendSubscriptionResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPExtendSubscriptionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one AHP_ExtendSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface AHPExtendSubscriptionResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPExtendSubscriptionResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpextendsubscriptionresponseab2adoctype");
    
    /**
     * Gets the "AHP_ExtendSubscriptionResponse" element
     */
    AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse getAHPExtendSubscriptionResponse();
    
    /**
     * Sets the "AHP_ExtendSubscriptionResponse" element
     */
    void setAHPExtendSubscriptionResponse(AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse ahpExtendSubscriptionResponse);
    
    /**
     * Appends and returns a new empty "AHP_ExtendSubscriptionResponse" element
     */
    AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse addNewAHPExtendSubscriptionResponse();
    
    /**
     * An XML AHP_ExtendSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface AHPExtendSubscriptionResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPExtendSubscriptionResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpextendsubscriptionresponse6b80elemtype");
        
        /**
         * Gets the "AHP_ExtendSubscriptionResult" element
         */
        AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult getAHPExtendSubscriptionResult();
        
        /**
         * True if has "AHP_ExtendSubscriptionResult" element
         */
        boolean isSetAHPExtendSubscriptionResult();
        
        /**
         * Sets the "AHP_ExtendSubscriptionResult" element
         */
        void setAHPExtendSubscriptionResult(AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult ahpExtendSubscriptionResult);
        
        /**
         * Appends and returns a new empty "AHP_ExtendSubscriptionResult" element
         */
        AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult addNewAHPExtendSubscriptionResult();
        
        /**
         * Unsets the "AHP_ExtendSubscriptionResult" element
         */
        void unsetAHPExtendSubscriptionResult();
        
        /**
         * An XML AHP_ExtendSubscriptionResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface AHPExtendSubscriptionResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPExtendSubscriptionResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpextendsubscriptionresult7f06elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult newInstance() {
                  return (AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse newInstance() {
              return (AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static AHPExtendSubscriptionResponseDocument newInstance() {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static AHPExtendSubscriptionResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static AHPExtendSubscriptionResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static AHPExtendSubscriptionResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static AHPExtendSubscriptionResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AHPExtendSubscriptionResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AHPExtendSubscriptionResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AHPExtendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
