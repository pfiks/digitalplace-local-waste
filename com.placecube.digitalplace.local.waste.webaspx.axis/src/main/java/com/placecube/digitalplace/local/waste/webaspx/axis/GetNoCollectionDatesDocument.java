/*
 * An XML document type.
 * Localname: getNoCollectionDates
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetNoCollectionDatesDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getNoCollectionDates(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetNoCollectionDatesDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetNoCollectionDatesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getnocollectiondates1cecdoctype");
    
    /**
     * Gets the "getNoCollectionDates" element
     */
    GetNoCollectionDatesDocument.GetNoCollectionDates getGetNoCollectionDates();
    
    /**
     * Sets the "getNoCollectionDates" element
     */
    void setGetNoCollectionDates(GetNoCollectionDatesDocument.GetNoCollectionDates getNoCollectionDates);
    
    /**
     * Appends and returns a new empty "getNoCollectionDates" element
     */
    GetNoCollectionDatesDocument.GetNoCollectionDates addNewGetNoCollectionDates();
    
    /**
     * An XML getNoCollectionDates(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetNoCollectionDates extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetNoCollectionDates.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getnocollectiondates2200elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "AllService3LRoundnameUPRN" element
         */
        java.lang.String getAllService3LRoundnameUPRN();
        
        /**
         * Gets (as xml) the "AllService3LRoundnameUPRN" element
         */
        org.apache.xmlbeans.XmlString xgetAllService3LRoundnameUPRN();
        
        /**
         * True if has "AllService3LRoundnameUPRN" element
         */
        boolean isSetAllService3LRoundnameUPRN();
        
        /**
         * Sets the "AllService3LRoundnameUPRN" element
         */
        void setAllService3LRoundnameUPRN(java.lang.String allService3LRoundnameUPRN);
        
        /**
         * Sets (as xml) the "AllService3LRoundnameUPRN" element
         */
        void xsetAllService3LRoundnameUPRN(org.apache.xmlbeans.XmlString allService3LRoundnameUPRN);
        
        /**
         * Unsets the "AllService3LRoundnameUPRN" element
         */
        void unsetAllService3LRoundnameUPRN();
        
        /**
         * Gets the "AllService3LRoundnameUPRNValue" element
         */
        java.lang.String getAllService3LRoundnameUPRNValue();
        
        /**
         * Gets (as xml) the "AllService3LRoundnameUPRNValue" element
         */
        org.apache.xmlbeans.XmlString xgetAllService3LRoundnameUPRNValue();
        
        /**
         * True if has "AllService3LRoundnameUPRNValue" element
         */
        boolean isSetAllService3LRoundnameUPRNValue();
        
        /**
         * Sets the "AllService3LRoundnameUPRNValue" element
         */
        void setAllService3LRoundnameUPRNValue(java.lang.String allService3LRoundnameUPRNValue);
        
        /**
         * Sets (as xml) the "AllService3LRoundnameUPRNValue" element
         */
        void xsetAllService3LRoundnameUPRNValue(org.apache.xmlbeans.XmlString allService3LRoundnameUPRNValue);
        
        /**
         * Unsets the "AllService3LRoundnameUPRNValue" element
         */
        void unsetAllService3LRoundnameUPRNValue();
        
        /**
         * Gets the "startDateDDsMMsYYYY" element
         */
        java.lang.String getStartDateDDsMMsYYYY();
        
        /**
         * Gets (as xml) the "startDateDDsMMsYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetStartDateDDsMMsYYYY();
        
        /**
         * True if has "startDateDDsMMsYYYY" element
         */
        boolean isSetStartDateDDsMMsYYYY();
        
        /**
         * Sets the "startDateDDsMMsYYYY" element
         */
        void setStartDateDDsMMsYYYY(java.lang.String startDateDDsMMsYYYY);
        
        /**
         * Sets (as xml) the "startDateDDsMMsYYYY" element
         */
        void xsetStartDateDDsMMsYYYY(org.apache.xmlbeans.XmlString startDateDDsMMsYYYY);
        
        /**
         * Unsets the "startDateDDsMMsYYYY" element
         */
        void unsetStartDateDDsMMsYYYY();
        
        /**
         * Gets the "endDateDDsMMsYYYY" element
         */
        java.lang.String getEndDateDDsMMsYYYY();
        
        /**
         * Gets (as xml) the "endDateDDsMMsYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetEndDateDDsMMsYYYY();
        
        /**
         * True if has "endDateDDsMMsYYYY" element
         */
        boolean isSetEndDateDDsMMsYYYY();
        
        /**
         * Sets the "endDateDDsMMsYYYY" element
         */
        void setEndDateDDsMMsYYYY(java.lang.String endDateDDsMMsYYYY);
        
        /**
         * Sets (as xml) the "endDateDDsMMsYYYY" element
         */
        void xsetEndDateDDsMMsYYYY(org.apache.xmlbeans.XmlString endDateDDsMMsYYYY);
        
        /**
         * Unsets the "endDateDDsMMsYYYY" element
         */
        void unsetEndDateDDsMMsYYYY();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetNoCollectionDatesDocument.GetNoCollectionDates newInstance() {
              return (GetNoCollectionDatesDocument.GetNoCollectionDates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetNoCollectionDatesDocument.GetNoCollectionDates newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetNoCollectionDatesDocument.GetNoCollectionDates) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetNoCollectionDatesDocument newInstance() {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetNoCollectionDatesDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetNoCollectionDatesDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetNoCollectionDatesDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetNoCollectionDatesDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetNoCollectionDatesDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetNoCollectionDatesDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetNoCollectionDatesDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetNoCollectionDatesDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetNoCollectionDatesDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetNoCollectionDatesDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetNoCollectionDatesDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetNoCollectionDatesDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetNoCollectionDatesDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetNoCollectionDatesDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetNoCollectionDatesDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetNoCollectionDatesDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetNoCollectionDatesDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetNoCollectionDatesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
