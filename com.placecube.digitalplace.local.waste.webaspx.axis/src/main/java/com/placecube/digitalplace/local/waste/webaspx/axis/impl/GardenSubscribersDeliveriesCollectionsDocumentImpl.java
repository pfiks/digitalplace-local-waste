/*
 * An XML document type.
 * Localname: Garden_Subscribers_Deliveries_Collections
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscribersDeliveriesCollectionsDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsDocument;

/**
 * A document containing one
 * Garden_Subscribers_Deliveries_Collections(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GardenSubscribersDeliveriesCollectionsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenSubscribersDeliveriesCollectionsDocument {

	/**
	 * An XML
	 * Garden_Subscribers_Deliveries_Collections(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GardenSubscribersDeliveriesCollectionsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName DATETOCHECKONYYYYMMDD$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "dateToCheckOn_yyyyMMdd");

		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName SUBSCRIBERSDELIVERIESORCOLLECTIONS$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"subscribersDeliveriesOrCollections");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public GardenSubscribersDeliveriesCollectionsImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "dateToCheckOn_yyyyMMdd" element
		 */
		@Override
		public java.lang.String getDateToCheckOnYyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATETOCHECKONYYYYMMDD$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "subscribersDeliveriesOrCollections" element
		 */
		@Override
		public java.lang.String getSubscribersDeliveriesOrCollections() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SUBSCRIBERSDELIVERIESORCOLLECTIONS$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "dateToCheckOn_yyyyMMdd" element
		 */
		@Override
		public boolean isSetDateToCheckOnYyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DATETOCHECKONYYYYMMDD$10) != 0;
			}
		}

		/**
		 * True if has "subscribersDeliveriesOrCollections" element
		 */
		@Override
		public boolean isSetSubscribersDeliveriesOrCollections() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SUBSCRIBERSDELIVERIESORCOLLECTIONS$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "dateToCheckOn_yyyyMMdd" element
		 */
		@Override
		public void setDateToCheckOnYyyyMMdd(java.lang.String dateToCheckOnYyyyMMdd) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATETOCHECKONYYYYMMDD$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DATETOCHECKONYYYYMMDD$10);
				}
				target.setStringValue(dateToCheckOnYyyyMMdd);
			}
		}

		/**
		 * Sets the "subscribersDeliveriesOrCollections" element
		 */
		@Override
		public void setSubscribersDeliveriesOrCollections(java.lang.String subscribersDeliveriesOrCollections) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SUBSCRIBERSDELIVERIESORCOLLECTIONS$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(SUBSCRIBERSDELIVERIESORCOLLECTIONS$8);
				}
				target.setStringValue(subscribersDeliveriesOrCollections);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "dateToCheckOn_yyyyMMdd" element
		 */
		@Override
		public void unsetDateToCheckOnYyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DATETOCHECKONYYYYMMDD$10, 0);
			}
		}

		/**
		 * Unsets the "subscribersDeliveriesOrCollections" element
		 */
		@Override
		public void unsetSubscribersDeliveriesOrCollections() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SUBSCRIBERSDELIVERIESORCOLLECTIONS$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "dateToCheckOn_yyyyMMdd" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDateToCheckOnYyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATETOCHECKONYYYYMMDD$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "subscribersDeliveriesOrCollections" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetSubscribersDeliveriesOrCollections() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SUBSCRIBERSDELIVERIESORCOLLECTIONS$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "dateToCheckOn_yyyyMMdd" element
		 */
		@Override
		public void xsetDateToCheckOnYyyyMMdd(org.apache.xmlbeans.XmlString dateToCheckOnYyyyMMdd) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATETOCHECKONYYYYMMDD$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DATETOCHECKONYYYYMMDD$10);
				}
				target.set(dateToCheckOnYyyyMMdd);
			}
		}

		/**
		 * Sets (as xml) the "subscribersDeliveriesOrCollections" element
		 */
		@Override
		public void xsetSubscribersDeliveriesOrCollections(org.apache.xmlbeans.XmlString subscribersDeliveriesOrCollections) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SUBSCRIBERSDELIVERIESORCOLLECTIONS$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(SUBSCRIBERSDELIVERIESORCOLLECTIONS$8);
				}
				target.set(subscribersDeliveriesOrCollections);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName GARDENSUBSCRIBERSDELIVERIESCOLLECTIONS$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"Garden_Subscribers_Deliveries_Collections");

	private static final long serialVersionUID = 1L;

	public GardenSubscribersDeliveriesCollectionsDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "Garden_Subscribers_Deliveries_Collections" element
	 */
	@Override
	public GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections addNewGardenSubscribersDeliveriesCollections() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections target = null;
			target = (GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections) get_store().add_element_user(GARDENSUBSCRIBERSDELIVERIESCOLLECTIONS$0);
			return target;
		}
	}

	/**
	 * Gets the "Garden_Subscribers_Deliveries_Collections" element
	 */
	@Override
	public GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections getGardenSubscribersDeliveriesCollections() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections target = null;
			target = (GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections) get_store().find_element_user(GARDENSUBSCRIBERSDELIVERIESCOLLECTIONS$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "Garden_Subscribers_Deliveries_Collections" element
	 */
	@Override
	public void setGardenSubscribersDeliveriesCollections(GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections gardenSubscribersDeliveriesCollections) {
		generatedSetterHelperImpl(gardenSubscribersDeliveriesCollections, GARDENSUBSCRIBERSDELIVERIESCOLLECTIONS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
