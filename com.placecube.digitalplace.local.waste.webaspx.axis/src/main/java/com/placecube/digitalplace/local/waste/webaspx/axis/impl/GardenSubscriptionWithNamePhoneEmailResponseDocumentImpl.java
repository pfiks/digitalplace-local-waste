/*
 * An XML document type.
 * Localname: GardenSubscription_WithNamePhoneEmailResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscriptionWithNamePhoneEmailResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionWithNamePhoneEmailResponseDocument;

/**
 * A document containing one
 * GardenSubscription_WithNamePhoneEmailResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GardenSubscriptionWithNamePhoneEmailResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenSubscriptionWithNamePhoneEmailResponseDocument {

	/**
	 * An XML
	 * GardenSubscription_WithNamePhoneEmailResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GardenSubscriptionWithNamePhoneEmailResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse {

		/**
		 * An XML
		 * GardenSubscription_WithNamePhoneEmailResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GardenSubscriptionWithNamePhoneEmailResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult {

			private static final long serialVersionUID = 1L;

			public GardenSubscriptionWithNamePhoneEmailResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"GardenSubscription_WithNamePhoneEmailResult");

		private static final long serialVersionUID = 1L;

		public GardenSubscriptionWithNamePhoneEmailResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "GardenSubscription_WithNamePhoneEmailResult" element
		 */
		@Override
		public GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult addNewGardenSubscriptionWithNamePhoneEmailResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult target = null;
				target = (GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult) get_store()
						.add_element_user(GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GardenSubscription_WithNamePhoneEmailResult" element
		 */
		@Override
		public GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult getGardenSubscriptionWithNamePhoneEmailResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult target = null;
				target = (GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult) get_store()
						.find_element_user(GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GardenSubscription_WithNamePhoneEmailResult" element
		 */
		@Override
		public boolean isSetGardenSubscriptionWithNamePhoneEmailResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GardenSubscription_WithNamePhoneEmailResult" element
		 */
		@Override
		public void setGardenSubscriptionWithNamePhoneEmailResult(
				GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult gardenSubscriptionWithNamePhoneEmailResult) {
			generatedSetterHelperImpl(gardenSubscriptionWithNamePhoneEmailResult, GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GardenSubscription_WithNamePhoneEmailResult" element
		 */
		@Override
		public void unsetGardenSubscriptionWithNamePhoneEmailResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"GardenSubscription_WithNamePhoneEmailResponse");

	private static final long serialVersionUID = 1L;

	public GardenSubscriptionWithNamePhoneEmailResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "GardenSubscription_WithNamePhoneEmailResponse" element
	 */
	@Override
	public GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse addNewGardenSubscriptionWithNamePhoneEmailResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse target = null;
			target = (GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse) get_store().add_element_user(GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GardenSubscription_WithNamePhoneEmailResponse" element
	 */
	@Override
	public GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse getGardenSubscriptionWithNamePhoneEmailResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse target = null;
			target = (GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse) get_store().find_element_user(GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESPONSE$0,
					0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GardenSubscription_WithNamePhoneEmailResponse" element
	 */
	@Override
	public void setGardenSubscriptionWithNamePhoneEmailResponse(
			GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse gardenSubscriptionWithNamePhoneEmailResponse) {
		generatedSetterHelperImpl(gardenSubscriptionWithNamePhoneEmailResponse, GARDENSUBSCRIPTIONWITHNAMEPHONEEMAILRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
