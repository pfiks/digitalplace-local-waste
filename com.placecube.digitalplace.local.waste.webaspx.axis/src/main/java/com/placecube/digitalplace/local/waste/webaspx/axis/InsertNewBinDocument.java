/*
 * An XML document type.
 * Localname: insertNewBin
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: InsertNewBinDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one insertNewBin(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface InsertNewBinDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(InsertNewBinDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("insertnewbinef7cdoctype");
    
    /**
     * Gets the "insertNewBin" element
     */
    InsertNewBinDocument.InsertNewBin getInsertNewBin();
    
    /**
     * Sets the "insertNewBin" element
     */
    void setInsertNewBin(InsertNewBinDocument.InsertNewBin insertNewBin);
    
    /**
     * Appends and returns a new empty "insertNewBin" element
     */
    InsertNewBinDocument.InsertNewBin addNewInsertNewBin();
    
    /**
     * An XML insertNewBin(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface InsertNewBin extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(InsertNewBin.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("insertnewbin5500elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "UPRN" element
         */
        java.lang.String getUPRN();
        
        /**
         * Gets (as xml) the "UPRN" element
         */
        org.apache.xmlbeans.XmlString xgetUPRN();
        
        /**
         * True if has "UPRN" element
         */
        boolean isSetUPRN();
        
        /**
         * Sets the "UPRN" element
         */
        void setUPRN(java.lang.String uprn);
        
        /**
         * Sets (as xml) the "UPRN" element
         */
        void xsetUPRN(org.apache.xmlbeans.XmlString uprn);
        
        /**
         * Unsets the "UPRN" element
         */
        void unsetUPRN();
        
        /**
         * Gets the "payRef" element
         */
        java.lang.String getPayRef();
        
        /**
         * Gets (as xml) the "payRef" element
         */
        org.apache.xmlbeans.XmlString xgetPayRef();
        
        /**
         * True if has "payRef" element
         */
        boolean isSetPayRef();
        
        /**
         * Sets the "payRef" element
         */
        void setPayRef(java.lang.String payRef);
        
        /**
         * Sets (as xml) the "payRef" element
         */
        void xsetPayRef(org.apache.xmlbeans.XmlString payRef);
        
        /**
         * Unsets the "payRef" element
         */
        void unsetPayRef();
        
        /**
         * Gets the "DeliverYN" element
         */
        java.lang.String getDeliverYN();
        
        /**
         * Gets (as xml) the "DeliverYN" element
         */
        org.apache.xmlbeans.XmlString xgetDeliverYN();
        
        /**
         * True if has "DeliverYN" element
         */
        boolean isSetDeliverYN();
        
        /**
         * Sets the "DeliverYN" element
         */
        void setDeliverYN(java.lang.String deliverYN);
        
        /**
         * Sets (as xml) the "DeliverYN" element
         */
        void xsetDeliverYN(org.apache.xmlbeans.XmlString deliverYN);
        
        /**
         * Unsets the "DeliverYN" element
         */
        void unsetDeliverYN();
        
        /**
         * Gets the "CollectYN" element
         */
        java.lang.String getCollectYN();
        
        /**
         * Gets (as xml) the "CollectYN" element
         */
        org.apache.xmlbeans.XmlString xgetCollectYN();
        
        /**
         * True if has "CollectYN" element
         */
        boolean isSetCollectYN();
        
        /**
         * Sets the "CollectYN" element
         */
        void setCollectYN(java.lang.String collectYN);
        
        /**
         * Sets (as xml) the "CollectYN" element
         */
        void xsetCollectYN(org.apache.xmlbeans.XmlString collectYN);
        
        /**
         * Unsets the "CollectYN" element
         */
        void unsetCollectYN();
        
        /**
         * Gets the "StartDateDDsMMsYYYY" element
         */
        java.lang.String getStartDateDDsMMsYYYY();
        
        /**
         * Gets (as xml) the "StartDateDDsMMsYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetStartDateDDsMMsYYYY();
        
        /**
         * True if has "StartDateDDsMMsYYYY" element
         */
        boolean isSetStartDateDDsMMsYYYY();
        
        /**
         * Sets the "StartDateDDsMMsYYYY" element
         */
        void setStartDateDDsMMsYYYY(java.lang.String startDateDDsMMsYYYY);
        
        /**
         * Sets (as xml) the "StartDateDDsMMsYYYY" element
         */
        void xsetStartDateDDsMMsYYYY(org.apache.xmlbeans.XmlString startDateDDsMMsYYYY);
        
        /**
         * Unsets the "StartDateDDsMMsYYYY" element
         */
        void unsetStartDateDDsMMsYYYY();
        
        /**
         * Gets the "EndDateDDsMMsYYYY" element
         */
        java.lang.String getEndDateDDsMMsYYYY();
        
        /**
         * Gets (as xml) the "EndDateDDsMMsYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetEndDateDDsMMsYYYY();
        
        /**
         * True if has "EndDateDDsMMsYYYY" element
         */
        boolean isSetEndDateDDsMMsYYYY();
        
        /**
         * Sets the "EndDateDDsMMsYYYY" element
         */
        void setEndDateDDsMMsYYYY(java.lang.String endDateDDsMMsYYYY);
        
        /**
         * Sets (as xml) the "EndDateDDsMMsYYYY" element
         */
        void xsetEndDateDDsMMsYYYY(org.apache.xmlbeans.XmlString endDateDDsMMsYYYY);
        
        /**
         * Unsets the "EndDateDDsMMsYYYY" element
         */
        void unsetEndDateDDsMMsYYYY();
        
        /**
         * Gets the "ReportedDateDDsMMsYYYY" element
         */
        java.lang.String getReportedDateDDsMMsYYYY();
        
        /**
         * Gets (as xml) the "ReportedDateDDsMMsYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetReportedDateDDsMMsYYYY();
        
        /**
         * True if has "ReportedDateDDsMMsYYYY" element
         */
        boolean isSetReportedDateDDsMMsYYYY();
        
        /**
         * Sets the "ReportedDateDDsMMsYYYY" element
         */
        void setReportedDateDDsMMsYYYY(java.lang.String reportedDateDDsMMsYYYY);
        
        /**
         * Sets (as xml) the "ReportedDateDDsMMsYYYY" element
         */
        void xsetReportedDateDDsMMsYYYY(org.apache.xmlbeans.XmlString reportedDateDDsMMsYYYY);
        
        /**
         * Unsets the "ReportedDateDDsMMsYYYY" element
         */
        void unsetReportedDateDDsMMsYYYY();
        
        /**
         * Gets the "CompletedDateDDsMMsYYYY" element
         */
        java.lang.String getCompletedDateDDsMMsYYYY();
        
        /**
         * Gets (as xml) the "CompletedDateDDsMMsYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetCompletedDateDDsMMsYYYY();
        
        /**
         * True if has "CompletedDateDDsMMsYYYY" element
         */
        boolean isSetCompletedDateDDsMMsYYYY();
        
        /**
         * Sets the "CompletedDateDDsMMsYYYY" element
         */
        void setCompletedDateDDsMMsYYYY(java.lang.String completedDateDDsMMsYYYY);
        
        /**
         * Sets (as xml) the "CompletedDateDDsMMsYYYY" element
         */
        void xsetCompletedDateDDsMMsYYYY(org.apache.xmlbeans.XmlString completedDateDDsMMsYYYY);
        
        /**
         * Unsets the "CompletedDateDDsMMsYYYY" element
         */
        void unsetCompletedDateDDsMMsYYYY();
        
        /**
         * Gets the "noOfNewContainersRequired" element
         */
        java.lang.String getNoOfNewContainersRequired();
        
        /**
         * Gets (as xml) the "noOfNewContainersRequired" element
         */
        org.apache.xmlbeans.XmlString xgetNoOfNewContainersRequired();
        
        /**
         * True if has "noOfNewContainersRequired" element
         */
        boolean isSetNoOfNewContainersRequired();
        
        /**
         * Sets the "noOfNewContainersRequired" element
         */
        void setNoOfNewContainersRequired(java.lang.String noOfNewContainersRequired);
        
        /**
         * Sets (as xml) the "noOfNewContainersRequired" element
         */
        void xsetNoOfNewContainersRequired(org.apache.xmlbeans.XmlString noOfNewContainersRequired);
        
        /**
         * Unsets the "noOfNewContainersRequired" element
         */
        void unsetNoOfNewContainersRequired();
        
        /**
         * Gets the "BinOrBag" element
         */
        java.lang.String getBinOrBag();
        
        /**
         * Gets (as xml) the "BinOrBag" element
         */
        org.apache.xmlbeans.XmlString xgetBinOrBag();
        
        /**
         * True if has "BinOrBag" element
         */
        boolean isSetBinOrBag();
        
        /**
         * Sets the "BinOrBag" element
         */
        void setBinOrBag(java.lang.String binOrBag);
        
        /**
         * Sets (as xml) the "BinOrBag" element
         */
        void xsetBinOrBag(org.apache.xmlbeans.XmlString binOrBag);
        
        /**
         * Unsets the "BinOrBag" element
         */
        void unsetBinOrBag();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static InsertNewBinDocument.InsertNewBin newInstance() {
              return (InsertNewBinDocument.InsertNewBin) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static InsertNewBinDocument.InsertNewBin newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (InsertNewBinDocument.InsertNewBin) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static InsertNewBinDocument newInstance() {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static InsertNewBinDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static InsertNewBinDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static InsertNewBinDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static InsertNewBinDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static InsertNewBinDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static InsertNewBinDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static InsertNewBinDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static InsertNewBinDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static InsertNewBinDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static InsertNewBinDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static InsertNewBinDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static InsertNewBinDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static InsertNewBinDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static InsertNewBinDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static InsertNewBinDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static InsertNewBinDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static InsertNewBinDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (InsertNewBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
