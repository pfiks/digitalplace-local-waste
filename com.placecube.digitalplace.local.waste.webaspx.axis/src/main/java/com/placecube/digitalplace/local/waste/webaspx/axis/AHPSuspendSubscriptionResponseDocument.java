/*
 * An XML document type.
 * Localname: AHP_SuspendSubscriptionResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPSuspendSubscriptionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one AHP_SuspendSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface AHPSuspendSubscriptionResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPSuspendSubscriptionResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpsuspendsubscriptionresponsec28edoctype");
    
    /**
     * Gets the "AHP_SuspendSubscriptionResponse" element
     */
    AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse getAHPSuspendSubscriptionResponse();
    
    /**
     * Sets the "AHP_SuspendSubscriptionResponse" element
     */
    void setAHPSuspendSubscriptionResponse(AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse ahpSuspendSubscriptionResponse);
    
    /**
     * Appends and returns a new empty "AHP_SuspendSubscriptionResponse" element
     */
    AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse addNewAHPSuspendSubscriptionResponse();
    
    /**
     * An XML AHP_SuspendSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface AHPSuspendSubscriptionResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPSuspendSubscriptionResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpsuspendsubscriptionresponse337eelemtype");
        
        /**
         * Gets the "AHP_SuspendSubscriptionResult" element
         */
        AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult getAHPSuspendSubscriptionResult();
        
        /**
         * True if has "AHP_SuspendSubscriptionResult" element
         */
        boolean isSetAHPSuspendSubscriptionResult();
        
        /**
         * Sets the "AHP_SuspendSubscriptionResult" element
         */
        void setAHPSuspendSubscriptionResult(AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult ahpSuspendSubscriptionResult);
        
        /**
         * Appends and returns a new empty "AHP_SuspendSubscriptionResult" element
         */
        AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult addNewAHPSuspendSubscriptionResult();
        
        /**
         * Unsets the "AHP_SuspendSubscriptionResult" element
         */
        void unsetAHPSuspendSubscriptionResult();
        
        /**
         * An XML AHP_SuspendSubscriptionResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface AHPSuspendSubscriptionResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPSuspendSubscriptionResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpsuspendsubscriptionresult0c0aelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult newInstance() {
                  return (AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse newInstance() {
              return (AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static AHPSuspendSubscriptionResponseDocument newInstance() {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static AHPSuspendSubscriptionResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static AHPSuspendSubscriptionResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static AHPSuspendSubscriptionResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static AHPSuspendSubscriptionResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AHPSuspendSubscriptionResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AHPSuspendSubscriptionResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AHPSuspendSubscriptionResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
