/**
 * WSCollExternal.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.0 Built on : Aug 01,
 * 2021 (07:27:19 HST)
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;

/*
 *  WSCollExternal java interface
 */

public interface WSCollExternal {

	/**
	 * Auto generated method signature Test Webservice to check communication.
	 * Returns Hello : Testname as a string
	 *
	 * @param aA_HelloWorld_String_Test64
	 */
	AAHelloWorldStringTestResponseDocument aA_HelloWorld_String_Test(AAHelloWorldStringTestDocument aA_HelloWorld_String_Test64) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Test Webservice to check communication.
	 * Returns Hello : Testname as a XML
	 *
	 * @param aA_HelloWorld_XML_Test110
	 */
	AAHelloWorldXMLTestResponseDocument aA_HelloWorld_XML_Test(AAHelloWorldXMLTestDocument aA_HelloWorld_XML_Test110) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature AHP Webservice - change address
	 *
	 * @param aHP_ChangeAddress76
	 */
	AHPChangeAddressResponseDocument aHP_ChangeAddress(AHPChangeAddressDocument aHP_ChangeAddress76) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature AHP Webservice - Extend Subscription date
	 *
	 * @param aHP_ExtendSubscription124
	 */
	AHPExtendSubscriptionResponseDocument aHP_ExtendSubscription(AHPExtendSubscriptionDocument aHP_ExtendSubscription124) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature AHP Webservice - Get AHP details for UPRN
	 *
	 * @param aHP_GetDetails88
	 */
	AHPGetDetailsResponseDocument aHP_GetDetails(AHPGetDetailsDocument aHP_GetDetails88) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature AHP Webservice - Register new or update
	 * details of existing
	 *
	 * @param aHP_NewUpdate94
	 */
	AHPNewUpdateResponseDocument aHP_NewUpdate(AHPNewUpdateDocument aHP_NewUpdate94) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature AHP Webservice - Order new Bags
	 *
	 * @param aHP_OrderNewBags0
	 */
	AHPOrderNewBagsResponseDocument aHP_OrderNewBags(AHPOrderNewBagsDocument aHP_OrderNewBags0) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature AHP Webservice - remove Subscription
	 *
	 * @param aHP_RemoveSubscription112
	 */
	AHPRemoveSubscriptionResponseDocument aHP_RemoveSubscription(AHPRemoveSubscriptionDocument aHP_RemoveSubscription112) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature AHP Webservice - suspend subscription
	 *
	 * @param aHP_SuspendSubscription8
	 */
	AHPSuspendSubscriptionResponseDocument aHP_SuspendSubscription(AHPSuspendSubscriptionDocument aHP_SuspendSubscription8) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Deletes a container. Requires binID and
	 * UPRN
	 *
	 * @param binDelete34
	 */
	BinDeleteResponseDocument binDelete(BinDeleteDocument binDelete34) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Inserts a container into the collections
	 * database bins table. Bin must exist in BinShop. Use in conjunction with
	 * GetAvailableContainers to get available containers for this client
	 *
	 * @param binInsert44
	 */
	BinInsertResponseDocument binInsert(BinInsertDocument binInsert44) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Updates a container in the bins table by
	 * id of bin. BinType must be in Binshop
	 *
	 * @param binUpdate42
	 */
	BinUpdateResponseDocument binUpdate(BinUpdateDocument binUpdate42) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns a line for each UPRN in the
	 * databsae that has a collection along with the unique calendar combination
	 * reference - all uprns with same reference have the same rounds schedule
	 * across all services
	 *
	 * @param cachedCalendar118
	 */
	CachedCalendarResponseDocument cachedCalendar(CachedCalendarDocument cachedCalendar118) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns assisted details for a UPRN
	 *
	 * @param checkAssisted70
	 */
	CheckAssistedResponseDocument checkAssisted(CheckAssistedDocument checkAssisted70) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Incab Comments - Get Current Value
	 *
	 * @param commentsForIncabGetExisting104
	 */
	CommentsForIncabGetExistingResponseDocument commentsForIncabGetExisting(CommentsForIncabGetExistingDocument commentsForIncabGetExisting104) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Incab Comments - update
	 *
	 * @param commentsForIncabUpdate28
	 */
	CommentsForIncabUpdateResponseDocument commentsForIncabUpdate(CommentsForIncabUpdateDocument commentsForIncabUpdate28) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns the number of bins on a given
	 * service for a UPRN
	 *
	 * @param countBinsForService12
	 */
	CountBinsForServiceResponseDocument countBinsForService(CountBinsForServiceDocument countBinsForService12) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Counts the number of Garden subscriptions
	 * between start and end date
	 *
	 * @param countSubscriptions100
	 */
	CountSubscriptionsResponseDocument countSubscriptions(CountSubscriptionsDocument countSubscriptions100) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Deletes a Garden Bin By id or name for a
	 * UPRN - **Jadu
	 *
	 * @param deleteBin74
	 */
	DeleteBinResponseDocument deleteBin(DeleteBinDocument deleteBin74) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Garden Webservice - Moves Garden detail
	 * from one UPRN to another
	 *
	 * @param garden_ChangeAddress60
	 */
	GardenChangeAddressResponseDocument garden_ChangeAddress(GardenChangeAddressDocument garden_ChangeAddress60) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Garden Webservice - remove Subscription
	 * and set end Date of Container to the supplied end date. IF end date is
	 * blank, then todays date will be used.
	 *
	 * @param garden_RemoveSubscription30
	 */
	GardenRemoveSubscriptionResponseDocument garden_RemoveSubscription(GardenRemoveSubscriptionDocument garden_RemoveSubscription30) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Garden Subscribers - Get all current
	 * subscribers, Garden Deliveries /Collections on a certain date.
	 *
	 * @param garden_Subscribers_Deliveries_Collections54
	 */
	GardenSubscribersDeliveriesCollectionsResponseDocument garden_Subscribers_Deliveries_Collections(GardenSubscribersDeliveriesCollectionsDocument garden_Subscribers_Deliveries_Collections54)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Functionality for a new Garden
	 * Subscription / or update existing subscription
	 *
	 * @param gardenSubscription22
	 */
	GardenSubscriptionResponseDocument gardenSubscription(GardenSubscriptionDocument gardenSubscription22) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Garden Webservice - Get Garden
	 * Subscription details for UPRN
	 *
	 * @param gardenSubscription_GetDetails58
	 */
	GardenSubscriptionGetDetailsResponseDocument gardenSubscription_GetDetails(GardenSubscriptionGetDetailsDocument gardenSubscription_GetDetails58) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Functionality for a new Garden
	 * Subscription / or update existing subscription includes Name Phone and
	 * Email
	 *
	 * @param gardenSubscription_WithNamePhoneEmail10
	 */
	GardenSubscriptionWithNamePhoneEmailResponseDocument gardenSubscription_WithNamePhoneEmail(GardenSubscriptionWithNamePhoneEmailDocument gardenSubscription_WithNamePhoneEmail10)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns matching addresses from given
	 * address details
	 *
	 * @param getAddressFromPostcode32
	 */
	GetAddressFromPostcodeResponseDocument getAddressFromPostcode(GetAddressFromPostcodeDocument getAddressFromPostcode32) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns and Address from a UPRN or
	 * matching UPRNs from address details
	 *
	 * @param getAddressOrUPRN26
	 */
	GetAddressOrUPRNResponseDocument getAddressOrUPRN(GetAddressOrUPRNDocument getAddressOrUPRN26) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns all issues of the issueType
	 * between start and End dates
	 *
	 * @param getAllIssuesForIssueType36
	 */
	GetAllIssuesForIssueTypeResponseDocument getAllIssuesForIssueType(GetAllIssuesForIssueTypeDocument getAllIssuesForIssueType36) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns All rounds for every UPRN
	 *
	 * @param getAllRoundDetails40
	 */
	GetAllRoundDetailsResponseDocument getAllRoundDetails(GetAllRoundDetailsDocument getAllRoundDetails40) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns All UPRNs and round information
	 * of properties collected on this date - does not currently cater for
	 * complex trade parameters
	 *
	 * @param getAllUPRNsForDate90
	 */
	GetAllUPRNsForDateResponseDocument getAllUPRNsForDate(GetAllUPRNsForDateDocument getAllUPRNsForDate90) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Please use
	 * GetAvailableContainersForClientOrProperty as a better and updated
	 * alternative. Returns all current known container types in the database.
	 *
	 * @param getAvailableContainers48
	 */
	GetAvailableContainersResponseDocument getAvailableContainers(GetAvailableContainersDocument getAvailableContainers48) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns all current known container types
	 * in the database for a client or (if a UPRN is supplied) at a property
	 *
	 * @param getAvailableContainersForClientOrProperty86
	 */
	GetAvailableContainersForClientOrPropertyResponseDocument getAvailableContainersForClientOrProperty(GetAvailableContainersForClientOrPropertyDocument getAvailableContainersForClientOrProperty86)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns all current known rounds in the
	 * database
	 *
	 * @param getAvailableRounds84
	 */
	GetAvailableRoundsResponseDocument getAvailableRounds(GetAvailableRoundsDocument getAvailableRounds84) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns container details for a service
	 * for a given UPRN. Includes ParentUPRN containers
	 *
	 * @param getBinDetailsForService102
	 */
	GetBinDetailsForServiceResponseDocument getBinDetailsForService(GetBinDetailsForServiceDocument getBinDetailsForService102) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns a list of the current incab
	 * Issues Codes and texts for use when parsing issue messsages from Incab
	 *
	 * @param getInCabIssueTextAndIssueCode106
	 */
	GetInCabIssueTextAndIssueCodeResponseDocument getInCabIssueTextAndIssueCode(GetInCabIssueTextAndIssueCodeDocument getInCabIssueTextAndIssueCode106) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns all issues for the populated
	 * filtered values on a date(ddMMyyyy) between a start and end time (HH:mm)
	 *
	 * @param getIssues68
	 */
	GetIssuesResponseDocument getIssues(GetIssuesDocument getIssues68) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature **Requires Webaspx InCab. Returns latest
	 * issues and collection status from the last time a truck was out for each
	 * service for this UPRN
	 *
	 * @param getIssuesAndCollectionStatusForUPRN126
	 */
	GetIssuesAndCollectionStatusForUPRNResponseDocument getIssuesAndCollectionStatusForUPRN(GetIssuesAndCollectionStatusForUPRNDocument getIssuesAndCollectionStatusForUPRN126)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns all Issues for a given UPRN for a
	 * specific date ** Requires Webaspx InCab system ** DateReq in format
	 * ddMMyyyy
	 *
	 * @param getIssuesForUPRN66
	 */
	GetIssuesForUPRNResponseDocument getIssuesForUPRN(GetIssuesForUPRNDocument getIssuesForUPRN66) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns last known truck position for a
	 * given registrations, date and time ** Requires Webaspx InCab System **
	 *
	 * @param getLastTruckPosition116
	 */
	GetLastTruckPositionResponseDocument getLastTruckPosition(GetLastTruckPositionDocument getLastTruckPosition116) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns the current truck positions that
	 * are collecting this UPRN today ** Requires Webaspx InCab System **
	 *
	 * @param getLatestTruckPositions24
	 */
	GetLatestTruckPositionsResponseDocument getLatestTruckPositions(GetLatestTruckPositionsDocument getLatestTruckPositions24) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature The main use of this webservice is to
	 * provide the current No Collection Dates for a suspended service, such as
	 * no Garden collection through winter. This will provide the date the
	 * Garden Suspension started and the date the garden suspension ends
	 * relevant to todays date. It will also supply details of the no collect
	 * dates relevant to the parameters supplied. Typically returned on UPRN
	 * level, but can be called for All, Service or Roundname. (Default is All)
	 *
	 * @param getNoCollectionDates6
	 */
	GetNoCollectionDatesResponseDocument getNoCollectionDates(GetNoCollectionDatesDocument getNoCollectionDates6) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns the next collection dates for a
	 * potential garden subscriber
	 *
	 * @param getPotentialCollectionDatesForNewGardenSubscriber2
	 */
	GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument getPotentialCollectionDatesForNewGardenSubscriber(
			GetPotentialCollectionDatesForNewGardenSubscriberDocument getPotentialCollectionDatesForNewGardenSubscriber2) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns the collections dates for each
	 * container for a particular UPRn inbetween start and end dates. **Leave
	 * BinID blank as historical unused parameter
	 *
	 * @param getRoundAndBinInfoForUPRNForNewAdjustedDates120
	 */
	GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument getRoundAndBinInfoForUPRNForNewAdjustedDates(
			GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument getRoundAndBinInfoForUPRNForNewAdjustedDates120) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns html of Friendly Calendar info
	 * and html table of calendar)
	 *
	 * @param getRoundCalendarForUPRN4
	 */
	GetRoundCalendarForUPRNResponseDocument getRoundCalendarForUPRN(GetRoundCalendarForUPRNDocument getRoundCalendarForUPRN4) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns details of any round changes made
	 * within a date range for a round or UPRN
	 *
	 * @param getRoundChangesInDateRangeByRoundOrUPRN16
	 */
	GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument getRoundChangesInDateRangeByRoundOrUPRN(GetRoundChangesInDateRangeByRoundOrUPRNDocument getRoundChangesInDateRangeByRoundOrUPRN16)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature returns next collection for each service
	 * this UPRN from a given start date
	 *
	 * @param getRoundForUPRN114
	 */
	GetRoundForUPRNResponseDocument getRoundForUPRN(GetRoundForUPRNDocument getRoundForUPRN114) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns the relevant roundname that
	 * collects this UPRN and 3 letter abbreviated service - Ref Rec Grn Gls Pls
	 * Box TRf TRc
	 *
	 * @param getRoundNameForUPRNService122
	 */
	GetRoundNameForUPRNServiceResponseDocument getRoundNameForUPRNService(GetRoundNameForUPRNServiceDocument getRoundNameForUPRNService122) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature ** Requires Webaspx InCab. Returns the
	 * registrations of the vehicles out on the given date(ddMMyyyy) after the
	 * given timeReq
	 *
	 * @param getTrucksOutToday92
	 */
	GetTrucksOutTodayResponseDocument getTrucksOutToday(GetTrucksOutTodayDocument getTrucksOutToday92) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns the Zone AssistedStatus and bag
	 * indicator for a UPRN - used for certain clients CRM generated garden
	 * service
	 *
	 * @param getZoneBagAssist56
	 */
	GetZoneBagAssistResponseDocument getZoneBagAssist(GetZoneBagAssistDocument getZoneBagAssist56) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Inserts a 240L Garden bin with supplied
	 * parameters. For a specific bin use BinInsert webservice
	 *
	 * @param insertNewBin14
	 */
	InsertNewBinResponseDocument insertNewBin(InsertNewBinDocument insertNewBin14) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature ** Internal Webaspx Use keeps database
	 * awake
	 *
	 * @param keepAliveCall108
	 */
	KeepAliveCallResponseDocument keepAliveCall(KeepAliveCallDocument keepAliveCall108) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns UPRN, Address details and count
	 * of this binType for all UPRNs with Start and End Dates
	 *
	 * @param largeHouseholds46
	 */
	LargeHouseholdsResponseDocument largeHouseholds(LargeHouseholdsDocument largeHouseholds46) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature **Requires Webaspx InCab. Returns latest
	 * issues and collection status from the last time a truck was out for each
	 * service for this UPRN. Also supplies GPS trial for each truck
	 *
	 * @param lastCollected62
	 */
	LastCollectedResponseDocument lastCollected(LastCollectedDocument lastCollected62) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns current values for the given
	 * databaseFieldName for a hash delimited list of UPRNs
	 *
	 * @param lLPGXtraGetUPRNCurrentValuesForDBField72
	 */
	LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument lLPGXtraGetUPRNCurrentValuesForDBField(LLPGXtraGetUPRNCurrentValuesForDBFieldDocument lLPGXtraGetUPRNCurrentValuesForDBField72)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns a list of editable LLPGXtra
	 * fields by the LLPGXtraUpdateGetUPRNForDBField webservice
	 *
	 * @param lLPGXtraUpdateGetAvailableFieldDetails52
	 */
	LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument lLPGXtraUpdateGetAvailableFieldDetails(LLPGXtraUpdateGetAvailableFieldDetailsDocument lLPGXtraUpdateGetAvailableFieldDetails52)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Updates the LLPGXtra databaseFieldName
	 * for a hash delimited list of UPRNs with the given newValue
	 *
	 * @param lLPGXtraUpdateGetUPRNForDBField96
	 */
	LLPGXtraUpdateGetUPRNForDBFieldResponseDocument lLPGXtraUpdateGetUPRNForDBField(LLPGXtraUpdateGetUPRNForDBFieldDocument lLPGXtraUpdateGetUPRNForDBField96) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns all live containers inbetween
	 * given dates
	 *
	 * @param queryBinEndDates80
	 */
	QueryBinEndDatesResponseDocument queryBinEndDates(QueryBinEndDatesDocument queryBinEndDates80) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns all live containers of a certain
	 * type
	 *
	 * @param queryBinOnType20
	 */
	QueryBinOnTypeResponseDocument queryBinOnType(QueryBinOnTypeDocument queryBinOnType20) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Sets missed bin details by UPRN and
	 * service
	 *
	 * @param readWriteMissedBin18
	 */
	ReadWriteMissedBinResponseDocument readWriteMissedBin(ReadWriteMissedBinDocument readWriteMissedBin18) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Sets missed bin details by UPRN and
	 * container type
	 *
	 * @param readWriteMissedBinByContainerType98
	 */
	ReadWriteMissedBinByContainerTypeResponseDocument readWriteMissedBinByContainerType(ReadWriteMissedBinByContainerTypeDocument readWriteMissedBinByContainerType98) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Returns all UPRNs with Address of all
	 * UPRNs with more than one standard container for the service requested
	 * with an optional start and end date. If service is blank or start/end
	 * date is blank, all will be returned
	 *
	 * @param showAdditionalBins78
	 */
	ShowAdditionalBinsResponseDocument showAdditionalBins(ShowAdditionalBinsDocument showAdditionalBins78) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Test
	 * Webservice to check communication. Returns Hello : Testname as a string
	 *
	 * @param aA_HelloWorld_String_Test64
	 */
	void startaA_HelloWorld_String_Test(AAHelloWorldStringTestDocument aA_HelloWorld_String_Test64, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Test
	 * Webservice to check communication. Returns Hello : Testname as a XML
	 *
	 * @param aA_HelloWorld_XML_Test110
	 */
	void startaA_HelloWorld_XML_Test(AAHelloWorldXMLTestDocument aA_HelloWorld_XML_Test110, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - change address
	 *
	 * @param aHP_ChangeAddress76
	 */
	void startaHP_ChangeAddress(AHPChangeAddressDocument aHP_ChangeAddress76, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - Extend Subscription date
	 *
	 * @param aHP_ExtendSubscription124
	 */
	void startaHP_ExtendSubscription(AHPExtendSubscriptionDocument aHP_ExtendSubscription124, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - Get AHP details for UPRN
	 *
	 * @param aHP_GetDetails88
	 */
	void startaHP_GetDetails(AHPGetDetailsDocument aHP_GetDetails88, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - Register new or update details of existing
	 *
	 * @param aHP_NewUpdate94
	 */
	void startaHP_NewUpdate(AHPNewUpdateDocument aHP_NewUpdate94, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - Order new Bags
	 *
	 * @param aHP_OrderNewBags0
	 */
	void startaHP_OrderNewBags(AHPOrderNewBagsDocument aHP_OrderNewBags0, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - remove Subscription
	 *
	 * @param aHP_RemoveSubscription112
	 */
	void startaHP_RemoveSubscription(AHPRemoveSubscriptionDocument aHP_RemoveSubscription112, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - suspend subscription
	 *
	 * @param aHP_SuspendSubscription8
	 */
	void startaHP_SuspendSubscription(AHPSuspendSubscriptionDocument aHP_SuspendSubscription8, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Deletes a
	 * container. Requires binID and UPRN
	 *
	 * @param binDelete34
	 */
	void startbinDelete(BinDeleteDocument binDelete34, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Inserts a
	 * container into the collections database bins table. Bin must exist in
	 * BinShop. Use in conjunction with GetAvailableContainers to get available
	 * containers for this client
	 *
	 * @param binInsert44
	 */
	void startbinInsert(BinInsertDocument binInsert44, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Updates a
	 * container in the bins table by id of bin. BinType must be in Binshop
	 *
	 * @param binUpdate42
	 */
	void startbinUpdate(BinUpdateDocument binUpdate42, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns a
	 * line for each UPRN in the databsae that has a collection along with the
	 * unique calendar combination reference - all uprns with same reference
	 * have the same rounds schedule across all services
	 *
	 * @param cachedCalendar118
	 */
	void startcachedCalendar(CachedCalendarDocument cachedCalendar118, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * assisted details for a UPRN
	 *
	 * @param checkAssisted70
	 */
	void startcheckAssisted(CheckAssistedDocument checkAssisted70, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Incab
	 * Comments - Get Current Value
	 *
	 * @param commentsForIncabGetExisting104
	 */
	void startcommentsForIncabGetExisting(CommentsForIncabGetExistingDocument commentsForIncabGetExisting104, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Incab
	 * Comments - update
	 *
	 * @param commentsForIncabUpdate28
	 */
	void startcommentsForIncabUpdate(CommentsForIncabUpdateDocument commentsForIncabUpdate28, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * number of bins on a given service for a UPRN
	 *
	 * @param countBinsForService12
	 */
	void startcountBinsForService(CountBinsForServiceDocument countBinsForService12, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Counts the
	 * number of Garden subscriptions between start and end date
	 *
	 * @param countSubscriptions100
	 */
	void startcountSubscriptions(CountSubscriptionsDocument countSubscriptions100, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Deletes a
	 * Garden Bin By id or name for a UPRN - **Jadu
	 *
	 * @param deleteBin74
	 */
	void startdeleteBin(DeleteBinDocument deleteBin74, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Garden
	 * Webservice - Moves Garden detail from one UPRN to another
	 *
	 * @param garden_ChangeAddress60
	 */
	void startgarden_ChangeAddress(GardenChangeAddressDocument garden_ChangeAddress60, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Garden
	 * Webservice - remove Subscription and set end Date of Container to the
	 * supplied end date. IF end date is blank, then todays date will be used.
	 *
	 * @param garden_RemoveSubscription30
	 */
	void startgarden_RemoveSubscription(GardenRemoveSubscriptionDocument garden_RemoveSubscription30, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Garden
	 * Subscribers - Get all current subscribers, Garden Deliveries /Collections
	 * on a certain date.
	 *
	 * @param garden_Subscribers_Deliveries_Collections54
	 */
	void startgarden_Subscribers_Deliveries_Collections(GardenSubscribersDeliveriesCollectionsDocument garden_Subscribers_Deliveries_Collections54, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations
	 * Functionality for a new Garden Subscription / or update existing
	 * subscription
	 *
	 * @param gardenSubscription22
	 */
	void startgardenSubscription(GardenSubscriptionDocument gardenSubscription22, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Garden
	 * Webservice - Get Garden Subscription details for UPRN
	 *
	 * @param gardenSubscription_GetDetails58
	 */
	void startgardenSubscription_GetDetails(GardenSubscriptionGetDetailsDocument gardenSubscription_GetDetails58, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations
	 * Functionality for a new Garden Subscription / or update existing
	 * subscription includes Name Phone and Email
	 *
	 * @param gardenSubscription_WithNamePhoneEmail10
	 */
	void startgardenSubscription_WithNamePhoneEmail(GardenSubscriptionWithNamePhoneEmailDocument gardenSubscription_WithNamePhoneEmail10, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * matching addresses from given address details
	 *
	 * @param getAddressFromPostcode32
	 */
	void startgetAddressFromPostcode(GetAddressFromPostcodeDocument getAddressFromPostcode32, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns and
	 * Address from a UPRN or matching UPRNs from address details
	 *
	 * @param getAddressOrUPRN26
	 */
	void startgetAddressOrUPRN(GetAddressOrUPRNDocument getAddressOrUPRN26, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * issues of the issueType between start and End dates
	 *
	 * @param getAllIssuesForIssueType36
	 */
	void startgetAllIssuesForIssueType(GetAllIssuesForIssueTypeDocument getAllIssuesForIssueType36, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns All
	 * rounds for every UPRN
	 *
	 * @param getAllRoundDetails40
	 */
	void startgetAllRoundDetails(GetAllRoundDetailsDocument getAllRoundDetails40, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns All
	 * UPRNs and round information of properties collected on this date - does
	 * not currently cater for complex trade parameters
	 *
	 * @param getAllUPRNsForDate90
	 */
	void startgetAllUPRNsForDate(GetAllUPRNsForDateDocument getAllUPRNsForDate90, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Please use
	 * GetAvailableContainersForClientOrProperty as a better and updated
	 * alternative. Returns all current known container types in the database.
	 *
	 * @param getAvailableContainers48
	 */
	void startgetAvailableContainers(GetAvailableContainersDocument getAvailableContainers48, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * current known container types in the database for a client or (if a UPRN
	 * is supplied) at a property
	 *
	 * @param getAvailableContainersForClientOrProperty86
	 */
	void startgetAvailableContainersForClientOrProperty(GetAvailableContainersForClientOrPropertyDocument getAvailableContainersForClientOrProperty86, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * current known rounds in the database
	 *
	 * @param getAvailableRounds84
	 */
	void startgetAvailableRounds(GetAvailableRoundsDocument getAvailableRounds84, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * container details for a service for a given UPRN. Includes ParentUPRN
	 * containers
	 *
	 * @param getBinDetailsForService102
	 */
	void startgetBinDetailsForService(GetBinDetailsForServiceDocument getBinDetailsForService102, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns a
	 * list of the current incab Issues Codes and texts for use when parsing
	 * issue messsages from Incab
	 *
	 * @param getInCabIssueTextAndIssueCode106
	 */
	void startgetInCabIssueTextAndIssueCode(GetInCabIssueTextAndIssueCodeDocument getInCabIssueTextAndIssueCode106, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * issues for the populated filtered values on a date(ddMMyyyy) between a
	 * start and end time (HH:mm)
	 *
	 * @param getIssues68
	 */
	void startgetIssues(GetIssuesDocument getIssues68, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations **Requires
	 * Webaspx InCab. Returns latest issues and collection status from the last
	 * time a truck was out for each service for this UPRN
	 *
	 * @param getIssuesAndCollectionStatusForUPRN126
	 */
	void startgetIssuesAndCollectionStatusForUPRN(GetIssuesAndCollectionStatusForUPRNDocument getIssuesAndCollectionStatusForUPRN126, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * Issues for a given UPRN for a specific date ** Requires Webaspx InCab
	 * system ** DateReq in format ddMMyyyy
	 *
	 * @param getIssuesForUPRN66
	 */
	void startgetIssuesForUPRN(GetIssuesForUPRNDocument getIssuesForUPRN66, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns last
	 * known truck position for a given registrations, date and time ** Requires
	 * Webaspx InCab System **
	 *
	 * @param getLastTruckPosition116
	 */
	void startgetLastTruckPosition(GetLastTruckPositionDocument getLastTruckPosition116, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * current truck positions that are collecting this UPRN today ** Requires
	 * Webaspx InCab System **
	 *
	 * @param getLatestTruckPositions24
	 */
	void startgetLatestTruckPositions(GetLatestTruckPositionsDocument getLatestTruckPositions24, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations The main use
	 * of this webservice is to provide the current No Collection Dates for a
	 * suspended service, such as no Garden collection through winter. This will
	 * provide the date the Garden Suspension started and the date the garden
	 * suspension ends relevant to todays date. It will also supply details of
	 * the no collect dates relevant to the parameters supplied. Typically
	 * returned on UPRN level, but can be called for All, Service or Roundname.
	 * (Default is All)
	 *
	 * @param getNoCollectionDates6
	 */
	void startgetNoCollectionDates(GetNoCollectionDatesDocument getNoCollectionDates6, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * next collection dates for a potential garden subscriber
	 *
	 * @param getPotentialCollectionDatesForNewGardenSubscriber2
	 */
	void startgetPotentialCollectionDatesForNewGardenSubscriber(GetPotentialCollectionDatesForNewGardenSubscriberDocument getPotentialCollectionDatesForNewGardenSubscriber2,
			final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * collections dates for each container for a particular UPRn inbetween
	 * start and end dates. **Leave BinID blank as historical unused parameter
	 *
	 * @param getRoundAndBinInfoForUPRNForNewAdjustedDates120
	 */
	void startgetRoundAndBinInfoForUPRNForNewAdjustedDates(GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument getRoundAndBinInfoForUPRNForNewAdjustedDates120,
			final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns html
	 * of Friendly Calendar info and html table of calendar)
	 *
	 * @param getRoundCalendarForUPRN4
	 */
	void startgetRoundCalendarForUPRN(GetRoundCalendarForUPRNDocument getRoundCalendarForUPRN4, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * details of any round changes made within a date range for a round or UPRN
	 *
	 * @param getRoundChangesInDateRangeByRoundOrUPRN16
	 */
	void startgetRoundChangesInDateRangeByRoundOrUPRN(GetRoundChangesInDateRangeByRoundOrUPRNDocument getRoundChangesInDateRangeByRoundOrUPRN16, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations returns next
	 * collection for each service this UPRN from a given start date
	 *
	 * @param getRoundForUPRN114
	 */
	void startgetRoundForUPRN(GetRoundForUPRNDocument getRoundForUPRN114, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * relevant roundname that collects this UPRN and 3 letter abbreviated
	 * service - Ref Rec Grn Gls Pls Box TRf TRc
	 *
	 * @param getRoundNameForUPRNService122
	 */
	void startgetRoundNameForUPRNService(GetRoundNameForUPRNServiceDocument getRoundNameForUPRNService122, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations ** Requires
	 * Webaspx InCab. Returns the registrations of the vehicles out on the given
	 * date(ddMMyyyy) after the given timeReq
	 *
	 * @param getTrucksOutToday92
	 */
	void startgetTrucksOutToday(GetTrucksOutTodayDocument getTrucksOutToday92, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * Zone AssistedStatus and bag indicator for a UPRN - used for certain
	 * clients CRM generated garden service
	 *
	 * @param getZoneBagAssist56
	 */
	void startgetZoneBagAssist(GetZoneBagAssistDocument getZoneBagAssist56, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Inserts a
	 * 240L Garden bin with supplied parameters. For a specific bin use
	 * BinInsert webservice
	 *
	 * @param insertNewBin14
	 */
	void startinsertNewBin(InsertNewBinDocument insertNewBin14, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations ** Internal
	 * Webaspx Use keeps database awake
	 *
	 * @param keepAliveCall108
	 */
	void startkeepAliveCall(KeepAliveCallDocument keepAliveCall108, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * UPRN, Address details and count of this binType for all UPRNs with Start
	 * and End Dates
	 *
	 * @param largeHouseholds46
	 */
	void startlargeHouseholds(LargeHouseholdsDocument largeHouseholds46, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations **Requires
	 * Webaspx InCab. Returns latest issues and collection status from the last
	 * time a truck was out for each service for this UPRN. Also supplies GPS
	 * trial for each truck
	 *
	 * @param lastCollected62
	 */
	void startlastCollected(LastCollectedDocument lastCollected62, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * current values for the given databaseFieldName for a hash delimited list
	 * of UPRNs
	 *
	 * @param lLPGXtraGetUPRNCurrentValuesForDBField72
	 */
	void startlLPGXtraGetUPRNCurrentValuesForDBField(LLPGXtraGetUPRNCurrentValuesForDBFieldDocument lLPGXtraGetUPRNCurrentValuesForDBField72, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns a
	 * list of editable LLPGXtra fields by the LLPGXtraUpdateGetUPRNForDBField
	 * webservice
	 *
	 * @param lLPGXtraUpdateGetAvailableFieldDetails52
	 */
	void startlLPGXtraUpdateGetAvailableFieldDetails(LLPGXtraUpdateGetAvailableFieldDetailsDocument lLPGXtraUpdateGetAvailableFieldDetails52, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Updates the
	 * LLPGXtra databaseFieldName for a hash delimited list of UPRNs with the
	 * given newValue
	 *
	 * @param lLPGXtraUpdateGetUPRNForDBField96
	 */
	void startlLPGXtraUpdateGetUPRNForDBField(LLPGXtraUpdateGetUPRNForDBFieldDocument lLPGXtraUpdateGetUPRNForDBField96, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * live containers inbetween given dates
	 *
	 * @param queryBinEndDates80
	 */
	void startqueryBinEndDates(QueryBinEndDatesDocument queryBinEndDates80, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * live containers of a certain type
	 *
	 * @param queryBinOnType20
	 */
	void startqueryBinOnType(QueryBinOnTypeDocument queryBinOnType20, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Sets missed
	 * bin details by UPRN and service
	 *
	 * @param readWriteMissedBin18
	 */
	void startreadWriteMissedBin(ReadWriteMissedBinDocument readWriteMissedBin18, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Sets missed
	 * bin details by UPRN and container type
	 *
	 * @param readWriteMissedBinByContainerType98
	 */
	void startreadWriteMissedBinByContainerType(ReadWriteMissedBinByContainerTypeDocument readWriteMissedBinByContainerType98, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * UPRNs with Address of all UPRNs with more than one standard container for
	 * the service requested with an optional start and end date. If service is
	 * blank or start/end date is blank, all will be returned
	 *
	 * @param showAdditionalBins78
	 */
	void startshowAdditionalBins(ShowAdditionalBinsDocument showAdditionalBins78, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Updates the
	 * assisted indicators in the collections database, sets assisted start and
	 * end dates and updates Comments for InCab with the assisted location. Will
	 * also update any open assisted Workflow cases or the last closed to
	 * replicate new value. Updates the assisted indicators in the collections
	 * database, sets assisted start and end dates and updates Comments for
	 * InCab with the assisted location
	 *
	 * @param updateAssisted38
	 */
	void startupdateAssisted(UpdateAssistedDocument updateAssisted38, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations Used by
	 * various CRMs SWN RGB for updating Garden Bin Details
	 *
	 * @param updateBinDetails50
	 */
	void startupdateBinDetails(UpdateBinDetailsDocument updateBinDetails50, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations **Internal
	 * Webaspx Use - writes various collected data to a collated table
	 *
	 * @param writeIncabLiveData82
	 */
	void startwriteIncabLiveData(WriteIncabLiveDataDocument writeIncabLiveData82, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Updates the assisted indicators in the
	 * collections database, sets assisted start and end dates and updates
	 * Comments for InCab with the assisted location. Will also update any open
	 * assisted Workflow cases or the last closed to replicate new value.
	 * Updates the assisted indicators in the collections database, sets
	 * assisted start and end dates and updates Comments for InCab with the
	 * assisted location
	 *
	 * @param updateAssisted38
	 */
	UpdateAssistedResponseDocument updateAssisted(UpdateAssistedDocument updateAssisted38) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature Used by various CRMs SWN RGB for updating
	 * Garden Bin Details
	 *
	 * @param updateBinDetails50
	 */
	UpdateBinDetailsResponseDocument updateBinDetails(UpdateBinDetailsDocument updateBinDetails50) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature **Internal Webaspx Use - writes various
	 * collected data to a collated table
	 *
	 * @param writeIncabLiveData82
	 */
	WriteIncabLiveDataResponseDocument writeIncabLiveData(WriteIncabLiveDataDocument writeIncabLiveData82) throws java.rmi.RemoteException;

	//
}
