/*
 * An XML document type.
 * Localname: KeepAliveCall
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: KeepAliveCallDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.KeepAliveCallDocument;

/**
 * A document containing one
 * KeepAliveCall(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class KeepAliveCallDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements KeepAliveCallDocument {

	/**
	 * An XML KeepAliveCall(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class KeepAliveCallImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements KeepAliveCallDocument.KeepAliveCall {

		private static final javax.xml.namespace.QName CLIENTFULL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "clientFull");

		private static final long serialVersionUID = 1L;

		public KeepAliveCallImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "clientFull" element
		 */
		@Override
		public java.lang.String getClientFull() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CLIENTFULL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "clientFull" element
		 */
		@Override
		public boolean isSetClientFull() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CLIENTFULL$0) != 0;
			}
		}

		/**
		 * Sets the "clientFull" element
		 */
		@Override
		public void setClientFull(java.lang.String clientFull) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CLIENTFULL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CLIENTFULL$0);
				}
				target.setStringValue(clientFull);
			}
		}

		/**
		 * Unsets the "clientFull" element
		 */
		@Override
		public void unsetClientFull() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CLIENTFULL$0, 0);
			}
		}

		/**
		 * Gets (as xml) the "clientFull" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetClientFull() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CLIENTFULL$0, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "clientFull" element
		 */
		@Override
		public void xsetClientFull(org.apache.xmlbeans.XmlString clientFull) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CLIENTFULL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CLIENTFULL$0);
				}
				target.set(clientFull);
			}
		}
	}

	private static final javax.xml.namespace.QName KEEPALIVECALL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "KeepAliveCall");

	private static final long serialVersionUID = 1L;

	public KeepAliveCallDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "KeepAliveCall" element
	 */
	@Override
	public KeepAliveCallDocument.KeepAliveCall addNewKeepAliveCall() {
		synchronized (monitor()) {
			check_orphaned();
			KeepAliveCallDocument.KeepAliveCall target = null;
			target = (KeepAliveCallDocument.KeepAliveCall) get_store().add_element_user(KEEPALIVECALL$0);
			return target;
		}
	}

	/**
	 * Gets the "KeepAliveCall" element
	 */
	@Override
	public KeepAliveCallDocument.KeepAliveCall getKeepAliveCall() {
		synchronized (monitor()) {
			check_orphaned();
			KeepAliveCallDocument.KeepAliveCall target = null;
			target = (KeepAliveCallDocument.KeepAliveCall) get_store().find_element_user(KEEPALIVECALL$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "KeepAliveCall" element
	 */
	@Override
	public void setKeepAliveCall(KeepAliveCallDocument.KeepAliveCall keepAliveCall) {
		generatedSetterHelperImpl(keepAliveCall, KEEPALIVECALL$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
