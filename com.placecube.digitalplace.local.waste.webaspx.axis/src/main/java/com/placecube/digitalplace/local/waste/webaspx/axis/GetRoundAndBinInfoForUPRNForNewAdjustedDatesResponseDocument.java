/*
 * An XML document type.
 * Localname: getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundandbininfoforuprnfornewadjusteddatesresponsee1b3doctype");
    
    /**
     * Gets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse" element
     */
    GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse getGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse();
    
    /**
     * Sets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse" element
     */
    void setGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse(GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse);
    
    /**
     * Appends and returns a new empty "getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse" element
     */
    GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse addNewGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse();
    
    /**
     * An XML getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundandbininfoforuprnfornewadjusteddatesresponse7a20elemtype");
        
        /**
         * Gets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult" element
         */
        GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult getGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult();
        
        /**
         * True if has "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult" element
         */
        boolean isSetGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult();
        
        /**
         * Sets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult" element
         */
        void setGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult(GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult getRoundAndBinInfoForUPRNForNewAdjustedDatesResult);
        
        /**
         * Appends and returns a new empty "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult" element
         */
        GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult addNewGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult();
        
        /**
         * Unsets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult" element
         */
        void unsetGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult();
        
        /**
         * An XML getRoundAndBinInfoForUPRNForNewAdjustedDatesResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundandbininfoforuprnfornewadjusteddatesresultb06felemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult newInstance() {
                  return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse newInstance() {
              return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument newInstance() {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
