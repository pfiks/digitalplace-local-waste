/*
 * An XML document type.
 * Localname: CommentsForIncabGetExistingResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CommentsForIncabGetExistingResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabGetExistingResponseDocument;

/**
 * A document containing one
 * CommentsForIncabGetExistingResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class CommentsForIncabGetExistingResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CommentsForIncabGetExistingResponseDocument {

	/**
	 * An XML
	 * CommentsForIncabGetExistingResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class CommentsForIncabGetExistingResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse {

		/**
		 * An XML
		 * CommentsForIncabGetExistingResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class CommentsForIncabGetExistingResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult {

			private static final long serialVersionUID = 1L;

			public CommentsForIncabGetExistingResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName COMMENTSFORINCABGETEXISTINGRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"CommentsForIncabGetExistingResult");

		private static final long serialVersionUID = 1L;

		public CommentsForIncabGetExistingResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "CommentsForIncabGetExistingResult"
		 * element
		 */
		@Override
		public CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult addNewCommentsForIncabGetExistingResult() {
			synchronized (monitor()) {
				check_orphaned();
				CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult target = null;
				target = (CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult) get_store()
						.add_element_user(COMMENTSFORINCABGETEXISTINGRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "CommentsForIncabGetExistingResult" element
		 */
		@Override
		public CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult getCommentsForIncabGetExistingResult() {
			synchronized (monitor()) {
				check_orphaned();
				CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult target = null;
				target = (CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult) get_store()
						.find_element_user(COMMENTSFORINCABGETEXISTINGRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "CommentsForIncabGetExistingResult" element
		 */
		@Override
		public boolean isSetCommentsForIncabGetExistingResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COMMENTSFORINCABGETEXISTINGRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "CommentsForIncabGetExistingResult" element
		 */
		@Override
		public void setCommentsForIncabGetExistingResult(
				CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult commentsForIncabGetExistingResult) {
			generatedSetterHelperImpl(commentsForIncabGetExistingResult, COMMENTSFORINCABGETEXISTINGRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "CommentsForIncabGetExistingResult" element
		 */
		@Override
		public void unsetCommentsForIncabGetExistingResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COMMENTSFORINCABGETEXISTINGRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName COMMENTSFORINCABGETEXISTINGRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"CommentsForIncabGetExistingResponse");

	private static final long serialVersionUID = 1L;

	public CommentsForIncabGetExistingResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "CommentsForIncabGetExistingResponse"
	 * element
	 */
	@Override
	public CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse addNewCommentsForIncabGetExistingResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse target = null;
			target = (CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse) get_store().add_element_user(COMMENTSFORINCABGETEXISTINGRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "CommentsForIncabGetExistingResponse" element
	 */
	@Override
	public CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse getCommentsForIncabGetExistingResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse target = null;
			target = (CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse) get_store().find_element_user(COMMENTSFORINCABGETEXISTINGRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "CommentsForIncabGetExistingResponse" element
	 */
	@Override
	public void setCommentsForIncabGetExistingResponse(CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse commentsForIncabGetExistingResponse) {
		generatedSetterHelperImpl(commentsForIncabGetExistingResponse, COMMENTSFORINCABGETEXISTINGRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
