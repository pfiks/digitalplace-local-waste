/*
 * An XML document type.
 * Localname: ShowAdditionalBinsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: ShowAdditionalBinsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.ShowAdditionalBinsResponseDocument;

/**
 * A document containing one
 * ShowAdditionalBinsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class ShowAdditionalBinsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ShowAdditionalBinsResponseDocument {

	/**
	 * An XML
	 * ShowAdditionalBinsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class ShowAdditionalBinsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse {

		/**
		 * An XML
		 * ShowAdditionalBinsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class ShowAdditionalBinsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult {

			private static final long serialVersionUID = 1L;

			public ShowAdditionalBinsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final long serialVersionUID = 1L;

		private static final javax.xml.namespace.QName SHOWADDITIONALBINSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ShowAdditionalBinsResult");

		public ShowAdditionalBinsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "ShowAdditionalBinsResult" element
		 */
		@Override
		public ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult addNewShowAdditionalBinsResult() {
			synchronized (monitor()) {
				check_orphaned();
				ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult target = null;
				target = (ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult) get_store().add_element_user(SHOWADDITIONALBINSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "ShowAdditionalBinsResult" element
		 */
		@Override
		public ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult getShowAdditionalBinsResult() {
			synchronized (monitor()) {
				check_orphaned();
				ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult target = null;
				target = (ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult) get_store().find_element_user(SHOWADDITIONALBINSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "ShowAdditionalBinsResult" element
		 */
		@Override
		public boolean isSetShowAdditionalBinsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SHOWADDITIONALBINSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "ShowAdditionalBinsResult" element
		 */
		@Override
		public void setShowAdditionalBinsResult(ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult showAdditionalBinsResult) {
			generatedSetterHelperImpl(showAdditionalBinsResult, SHOWADDITIONALBINSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "ShowAdditionalBinsResult" element
		 */
		@Override
		public void unsetShowAdditionalBinsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SHOWADDITIONALBINSRESULT$0, 0);
			}
		}
	}

	private static final long serialVersionUID = 1L;

	private static final javax.xml.namespace.QName SHOWADDITIONALBINSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ShowAdditionalBinsResponse");

	public ShowAdditionalBinsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "ShowAdditionalBinsResponse" element
	 */
	@Override
	public ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse addNewShowAdditionalBinsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse target = null;
			target = (ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse) get_store().add_element_user(SHOWADDITIONALBINSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "ShowAdditionalBinsResponse" element
	 */
	@Override
	public ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse getShowAdditionalBinsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse target = null;
			target = (ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse) get_store().find_element_user(SHOWADDITIONALBINSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "ShowAdditionalBinsResponse" element
	 */
	@Override
	public void setShowAdditionalBinsResponse(ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse showAdditionalBinsResponse) {
		generatedSetterHelperImpl(showAdditionalBinsResponse, SHOWADDITIONALBINSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
