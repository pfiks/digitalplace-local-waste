/*
 * An XML document type.
 * Localname: getAllIssuesForIssueTypeResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAllIssuesForIssueTypeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeResponseDocument;

/**
 * A document containing one
 * getAllIssuesForIssueTypeResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAllIssuesForIssueTypeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAllIssuesForIssueTypeResponseDocument {

	/**
	 * An XML
	 * getAllIssuesForIssueTypeResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAllIssuesForIssueTypeResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse {

		/**
		 * An XML
		 * getAllIssuesForIssueTypeResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetAllIssuesForIssueTypeResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult {

			private static final long serialVersionUID = 1L;

			public GetAllIssuesForIssueTypeResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETALLISSUESFORISSUETYPERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"getAllIssuesForIssueTypeResult");

		private static final long serialVersionUID = 1L;

		public GetAllIssuesForIssueTypeResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "getAllIssuesForIssueTypeResult"
		 * element
		 */
		@Override
		public GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult addNewGetAllIssuesForIssueTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult target = null;
				target = (GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult) get_store().add_element_user(GETALLISSUESFORISSUETYPERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getAllIssuesForIssueTypeResult" element
		 */
		@Override
		public GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult getGetAllIssuesForIssueTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult target = null;
				target = (GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult) get_store().find_element_user(GETALLISSUESFORISSUETYPERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getAllIssuesForIssueTypeResult" element
		 */
		@Override
		public boolean isSetGetAllIssuesForIssueTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETALLISSUESFORISSUETYPERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getAllIssuesForIssueTypeResult" element
		 */
		@Override
		public void setGetAllIssuesForIssueTypeResult(GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult getAllIssuesForIssueTypeResult) {
			generatedSetterHelperImpl(getAllIssuesForIssueTypeResult, GETALLISSUESFORISSUETYPERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getAllIssuesForIssueTypeResult" element
		 */
		@Override
		public void unsetGetAllIssuesForIssueTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETALLISSUESFORISSUETYPERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETALLISSUESFORISSUETYPERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"getAllIssuesForIssueTypeResponse");

	private static final long serialVersionUID = 1L;

	public GetAllIssuesForIssueTypeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getAllIssuesForIssueTypeResponse"
	 * element
	 */
	@Override
	public GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse addNewGetAllIssuesForIssueTypeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse target = null;
			target = (GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse) get_store().add_element_user(GETALLISSUESFORISSUETYPERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getAllIssuesForIssueTypeResponse" element
	 */
	@Override
	public GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse getGetAllIssuesForIssueTypeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse target = null;
			target = (GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse) get_store().find_element_user(GETALLISSUESFORISSUETYPERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getAllIssuesForIssueTypeResponse" element
	 */
	@Override
	public void setGetAllIssuesForIssueTypeResponse(GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse getAllIssuesForIssueTypeResponse) {
		generatedSetterHelperImpl(getAllIssuesForIssueTypeResponse, GETALLISSUESFORISSUETYPERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
