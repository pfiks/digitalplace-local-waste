/*
 * An XML document type.
 * Localname: AA_HelloWorld_String_TestResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AAHelloWorldStringTestResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;

/**
 * A document containing one
 * AA_HelloWorld_String_TestResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public interface AAHelloWorldStringTestResponseDocument extends org.apache.xmlbeans.XmlObject {

	/**
	 * An XML
	 * AA_HelloWorld_String_TestResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public interface AAHelloWorldStringTestResponse extends org.apache.xmlbeans.XmlObject {

		/**
		 * A factory class with static methods for creating instances of this
		 * type.
		 */

		public static final class Factory {

			private Factory() {
			} // No instance of this class allowed

			public static AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse newInstance() {
				return (AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader()
						.newInstance(type, null);
			}

			public static AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
				return (AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader()
						.newInstance(type, options);
			}
		}

		org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType) org.apache.xmlbeans.XmlBeans
				.typeSystemForClassLoader(AAHelloWorldStringTestResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396")
				.resolveHandle("aahelloworldstringtestresponse85f8elemtype");

		/**
		 * Gets the "AA_HelloWorld_String_TestResult" element
		 */
		java.lang.String getAAHelloWorldStringTestResult();

		/**
		 * True if has "AA_HelloWorld_String_TestResult" element
		 */
		boolean isSetAAHelloWorldStringTestResult();

		/**
		 * Sets the "AA_HelloWorld_String_TestResult" element
		 */
		void setAAHelloWorldStringTestResult(java.lang.String aaHelloWorldStringTestResult);

		/**
		 * Unsets the "AA_HelloWorld_String_TestResult" element
		 */
		void unsetAAHelloWorldStringTestResult();

		/**
		 * Gets (as xml) the "AA_HelloWorld_String_TestResult" element
		 */
		org.apache.xmlbeans.XmlString xgetAAHelloWorldStringTestResult();

		/**
		 * Sets (as xml) the "AA_HelloWorld_String_TestResult" element
		 */
		void xsetAAHelloWorldStringTestResult(org.apache.xmlbeans.XmlString aaHelloWorldStringTestResult);
	}

	/**
	 * A factory class with static methods for creating instances of this type.
	 */

	public static final class Factory {

		private Factory() {
		} // No instance of this class allowed

		public static AAHelloWorldStringTestResponseDocument newInstance() {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance(type, null);
		}

		public static AAHelloWorldStringTestResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance(type, options);
		}

		/** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
		@Deprecated
		public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis)
				throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
			return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xis, type, null);
		}

		/** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
		@Deprecated
		public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
			return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xis, type, options);
		}

		/** @param file the file from which to load an xml document */
		public static AAHelloWorldStringTestResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(file, type, null);
		}

		public static AAHelloWorldStringTestResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(file, type, options);
		}

		public static AAHelloWorldStringTestResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(is, type, null);
		}

		public static AAHelloWorldStringTestResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(is, type, options);
		}

		public static AAHelloWorldStringTestResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(r, type, null);
		}

		public static AAHelloWorldStringTestResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(r, type, options);
		}

		/** @param xmlAsString the string value to parse */
		public static AAHelloWorldStringTestResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(xmlAsString, type, null);
		}

		public static AAHelloWorldStringTestResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(xmlAsString, type, options);
		}

		public static AAHelloWorldStringTestResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(u, type, null);
		}

		public static AAHelloWorldStringTestResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(u, type, options);
		}

		public static AAHelloWorldStringTestResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(sr, type, null);
		}

		public static AAHelloWorldStringTestResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(sr, type, options);
		}

		/** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
		@Deprecated
		public static AAHelloWorldStringTestResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis)
				throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(xis, type, null);
		}

		/** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
		@Deprecated
		public static AAHelloWorldStringTestResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(xis, type, options);
		}

		public static AAHelloWorldStringTestResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(node, type, null);
		}

		public static AAHelloWorldStringTestResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(node, type, options);
		}
	}

	org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType) org.apache.xmlbeans.XmlBeans
			.typeSystemForClassLoader(AAHelloWorldStringTestResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396")
			.resolveHandle("aahelloworldstringtestresponse852bdoctype");

	/**
	 * Appends and returns a new empty "AA_HelloWorld_String_TestResponse"
	 * element
	 */
	AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse addNewAAHelloWorldStringTestResponse();

	/**
	 * Gets the "AA_HelloWorld_String_TestResponse" element
	 */
	AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse getAAHelloWorldStringTestResponse();

	/**
	 * Sets the "AA_HelloWorld_String_TestResponse" element
	 */
	void setAAHelloWorldStringTestResponse(AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse aaHelloWorldStringTestResponse);
}
