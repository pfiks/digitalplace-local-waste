/*
 * An XML document type.
 * Localname: BinUpdate
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: BinUpdateDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateDocument;

/**
 * A document containing one
 * BinUpdate(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class BinUpdateDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinUpdateDocument {

	/**
	 * An XML BinUpdate(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class BinUpdateImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinUpdateDocument.BinUpdate {

		private static final javax.xml.namespace.QName BINID$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binID");

		private static final javax.xml.namespace.QName BINTYPE$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binType");

		private static final javax.xml.namespace.QName COLLECTYN$18 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CollectYN");
		private static final javax.xml.namespace.QName COMPLETEDDATEDDSMMSYYYY$26 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CompletedDateDDsMMsYYYY");
		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");
		private static final javax.xml.namespace.QName DELIVERYN$16 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "DeliverYN");
		private static final javax.xml.namespace.QName ENDDATEDDSMMSYYYY$22 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "EndDateDDsMMsYYYY");
		private static final javax.xml.namespace.QName LEAVEBLANKSASISYN$28 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LeaveBlanksAsIsYN");
		private static final javax.xml.namespace.QName PAYREF$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "payRef");
		private static final javax.xml.namespace.QName REPORTEDDATEDDSMMSYYYY$24 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ReportedDateDDsMMsYYYY");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName STARTDATEDDSMMSYYYY$20 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "StartDateDDsMMsYYYY");
		private static final javax.xml.namespace.QName UPRN$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRN");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public BinUpdateImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "binID" element
		 */
		@Override
		public java.lang.String getBinID() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINID$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "binType" element
		 */
		@Override
		public java.lang.String getBinType() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINTYPE$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "CollectYN" element
		 */
		@Override
		public java.lang.String getCollectYN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COLLECTYN$18, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "CompletedDateDDsMMsYYYY" element
		 */
		@Override
		public java.lang.String getCompletedDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COMPLETEDDATEDDSMMSYYYY$26, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "DeliverYN" element
		 */
		@Override
		public java.lang.String getDeliverYN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DELIVERYN$16, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "EndDateDDsMMsYYYY" element
		 */
		@Override
		public java.lang.String getEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ENDDATEDDSMMSYYYY$22, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "LeaveBlanksAsIsYN" element
		 */
		@Override
		public java.lang.String getLeaveBlanksAsIsYN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(LEAVEBLANKSASISYN$28, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "payRef" element
		 */
		@Override
		public java.lang.String getPayRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(PAYREF$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "ReportedDateDDsMMsYYYY" element
		 */
		@Override
		public java.lang.String getReportedDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(REPORTEDDATEDDSMMSYYYY$24, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "StartDateDDsMMsYYYY" element
		 */
		@Override
		public java.lang.String getStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STARTDATEDDSMMSYYYY$20, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRN" element
		 */
		@Override
		public java.lang.String getUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "binID" element
		 */
		@Override
		public boolean isSetBinID() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BINID$10) != 0;
			}
		}

		/**
		 * True if has "binType" element
		 */
		@Override
		public boolean isSetBinType() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BINTYPE$12) != 0;
			}
		}

		/**
		 * True if has "CollectYN" element
		 */
		@Override
		public boolean isSetCollectYN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COLLECTYN$18) != 0;
			}
		}

		/**
		 * True if has "CompletedDateDDsMMsYYYY" element
		 */
		@Override
		public boolean isSetCompletedDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COMPLETEDDATEDDSMMSYYYY$26) != 0;
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "DeliverYN" element
		 */
		@Override
		public boolean isSetDeliverYN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DELIVERYN$16) != 0;
			}
		}

		/**
		 * True if has "EndDateDDsMMsYYYY" element
		 */
		@Override
		public boolean isSetEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ENDDATEDDSMMSYYYY$22) != 0;
			}
		}

		/**
		 * True if has "LeaveBlanksAsIsYN" element
		 */
		@Override
		public boolean isSetLeaveBlanksAsIsYN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(LEAVEBLANKSASISYN$28) != 0;
			}
		}

		/**
		 * True if has "payRef" element
		 */
		@Override
		public boolean isSetPayRef() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(PAYREF$14) != 0;
			}
		}

		/**
		 * True if has "ReportedDateDDsMMsYYYY" element
		 */
		@Override
		public boolean isSetReportedDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(REPORTEDDATEDDSMMSYYYY$24) != 0;
			}
		}

		/**
		 * True if has "StartDateDDsMMsYYYY" element
		 */
		@Override
		public boolean isSetStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(STARTDATEDDSMMSYYYY$20) != 0;
			}
		}

		/**
		 * True if has "UPRN" element
		 */
		@Override
		public boolean isSetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRN$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "binID" element
		 */
		@Override
		public void setBinID(java.lang.String binID) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINID$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(BINID$10);
				}
				target.setStringValue(binID);
			}
		}

		/**
		 * Sets the "binType" element
		 */
		@Override
		public void setBinType(java.lang.String binType) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINTYPE$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(BINTYPE$12);
				}
				target.setStringValue(binType);
			}
		}

		/**
		 * Sets the "CollectYN" element
		 */
		@Override
		public void setCollectYN(java.lang.String collectYN) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COLLECTYN$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COLLECTYN$18);
				}
				target.setStringValue(collectYN);
			}
		}

		/**
		 * Sets the "CompletedDateDDsMMsYYYY" element
		 */
		@Override
		public void setCompletedDateDDsMMsYYYY(java.lang.String completedDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COMPLETEDDATEDDSMMSYYYY$26, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COMPLETEDDATEDDSMMSYYYY$26);
				}
				target.setStringValue(completedDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "DeliverYN" element
		 */
		@Override
		public void setDeliverYN(java.lang.String deliverYN) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DELIVERYN$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DELIVERYN$16);
				}
				target.setStringValue(deliverYN);
			}
		}

		/**
		 * Sets the "EndDateDDsMMsYYYY" element
		 */
		@Override
		public void setEndDateDDsMMsYYYY(java.lang.String endDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ENDDATEDDSMMSYYYY$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ENDDATEDDSMMSYYYY$22);
				}
				target.setStringValue(endDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets the "LeaveBlanksAsIsYN" element
		 */
		@Override
		public void setLeaveBlanksAsIsYN(java.lang.String leaveBlanksAsIsYN) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(LEAVEBLANKSASISYN$28, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(LEAVEBLANKSASISYN$28);
				}
				target.setStringValue(leaveBlanksAsIsYN);
			}
		}

		/**
		 * Sets the "payRef" element
		 */
		@Override
		public void setPayRef(java.lang.String payRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(PAYREF$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(PAYREF$14);
				}
				target.setStringValue(payRef);
			}
		}

		/**
		 * Sets the "ReportedDateDDsMMsYYYY" element
		 */
		@Override
		public void setReportedDateDDsMMsYYYY(java.lang.String reportedDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(REPORTEDDATEDDSMMSYYYY$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(REPORTEDDATEDDSMMSYYYY$24);
				}
				target.setStringValue(reportedDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets the "StartDateDDsMMsYYYY" element
		 */
		@Override
		public void setStartDateDDsMMsYYYY(java.lang.String startDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STARTDATEDDSMMSYYYY$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(STARTDATEDDSMMSYYYY$20);
				}
				target.setStringValue(startDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets the "UPRN" element
		 */
		@Override
		public void setUPRN(java.lang.String uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRN$8);
				}
				target.setStringValue(uprn);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "binID" element
		 */
		@Override
		public void unsetBinID() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BINID$10, 0);
			}
		}

		/**
		 * Unsets the "binType" element
		 */
		@Override
		public void unsetBinType() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BINTYPE$12, 0);
			}
		}

		/**
		 * Unsets the "CollectYN" element
		 */
		@Override
		public void unsetCollectYN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COLLECTYN$18, 0);
			}
		}

		/**
		 * Unsets the "CompletedDateDDsMMsYYYY" element
		 */
		@Override
		public void unsetCompletedDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COMPLETEDDATEDDSMMSYYYY$26, 0);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "DeliverYN" element
		 */
		@Override
		public void unsetDeliverYN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DELIVERYN$16, 0);
			}
		}

		/**
		 * Unsets the "EndDateDDsMMsYYYY" element
		 */
		@Override
		public void unsetEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ENDDATEDDSMMSYYYY$22, 0);
			}
		}

		/**
		 * Unsets the "LeaveBlanksAsIsYN" element
		 */
		@Override
		public void unsetLeaveBlanksAsIsYN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(LEAVEBLANKSASISYN$28, 0);
			}
		}

		/**
		 * Unsets the "payRef" element
		 */
		@Override
		public void unsetPayRef() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(PAYREF$14, 0);
			}
		}

		/**
		 * Unsets the "ReportedDateDDsMMsYYYY" element
		 */
		@Override
		public void unsetReportedDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(REPORTEDDATEDDSMMSYYYY$24, 0);
			}
		}

		/**
		 * Unsets the "StartDateDDsMMsYYYY" element
		 */
		@Override
		public void unsetStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(STARTDATEDDSMMSYYYY$20, 0);
			}
		}

		/**
		 * Unsets the "UPRN" element
		 */
		@Override
		public void unsetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRN$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "binID" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetBinID() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINID$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "binType" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetBinType() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINTYPE$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "CollectYN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCollectYN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COLLECTYN$18, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "CompletedDateDDsMMsYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCompletedDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COMPLETEDDATEDDSMMSYYYY$26, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "DeliverYN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDeliverYN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DELIVERYN$16, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "EndDateDDsMMsYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ENDDATEDDSMMSYYYY$22, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "LeaveBlanksAsIsYN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetLeaveBlanksAsIsYN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(LEAVEBLANKSASISYN$28, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "payRef" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetPayRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(PAYREF$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "ReportedDateDDsMMsYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetReportedDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(REPORTEDDATEDDSMMSYYYY$24, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "StartDateDDsMMsYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STARTDATEDDSMMSYYYY$20, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "binID" element
		 */
		@Override
		public void xsetBinID(org.apache.xmlbeans.XmlString binID) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINID$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(BINID$10);
				}
				target.set(binID);
			}
		}

		/**
		 * Sets (as xml) the "binType" element
		 */
		@Override
		public void xsetBinType(org.apache.xmlbeans.XmlString binType) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINTYPE$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(BINTYPE$12);
				}
				target.set(binType);
			}
		}

		/**
		 * Sets (as xml) the "CollectYN" element
		 */
		@Override
		public void xsetCollectYN(org.apache.xmlbeans.XmlString collectYN) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COLLECTYN$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COLLECTYN$18);
				}
				target.set(collectYN);
			}
		}

		/**
		 * Sets (as xml) the "CompletedDateDDsMMsYYYY" element
		 */
		@Override
		public void xsetCompletedDateDDsMMsYYYY(org.apache.xmlbeans.XmlString completedDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COMPLETEDDATEDDSMMSYYYY$26, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COMPLETEDDATEDDSMMSYYYY$26);
				}
				target.set(completedDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "DeliverYN" element
		 */
		@Override
		public void xsetDeliverYN(org.apache.xmlbeans.XmlString deliverYN) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DELIVERYN$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DELIVERYN$16);
				}
				target.set(deliverYN);
			}
		}

		/**
		 * Sets (as xml) the "EndDateDDsMMsYYYY" element
		 */
		@Override
		public void xsetEndDateDDsMMsYYYY(org.apache.xmlbeans.XmlString endDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ENDDATEDDSMMSYYYY$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ENDDATEDDSMMSYYYY$22);
				}
				target.set(endDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets (as xml) the "LeaveBlanksAsIsYN" element
		 */
		@Override
		public void xsetLeaveBlanksAsIsYN(org.apache.xmlbeans.XmlString leaveBlanksAsIsYN) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(LEAVEBLANKSASISYN$28, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(LEAVEBLANKSASISYN$28);
				}
				target.set(leaveBlanksAsIsYN);
			}
		}

		/**
		 * Sets (as xml) the "payRef" element
		 */
		@Override
		public void xsetPayRef(org.apache.xmlbeans.XmlString payRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(PAYREF$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(PAYREF$14);
				}
				target.set(payRef);
			}
		}

		/**
		 * Sets (as xml) the "ReportedDateDDsMMsYYYY" element
		 */
		@Override
		public void xsetReportedDateDDsMMsYYYY(org.apache.xmlbeans.XmlString reportedDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(REPORTEDDATEDDSMMSYYYY$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(REPORTEDDATEDDSMMSYYYY$24);
				}
				target.set(reportedDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets (as xml) the "StartDateDDsMMsYYYY" element
		 */
		@Override
		public void xsetStartDateDDsMMsYYYY(org.apache.xmlbeans.XmlString startDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STARTDATEDDSMMSYYYY$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(STARTDATEDDSMMSYYYY$20);
				}
				target.set(startDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets (as xml) the "UPRN" element
		 */
		@Override
		public void xsetUPRN(org.apache.xmlbeans.XmlString uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRN$8);
				}
				target.set(uprn);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName BINUPDATE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinUpdate");

	private static final long serialVersionUID = 1L;

	public BinUpdateDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "BinUpdate" element
	 */
	@Override
	public BinUpdateDocument.BinUpdate addNewBinUpdate() {
		synchronized (monitor()) {
			check_orphaned();
			BinUpdateDocument.BinUpdate target = null;
			target = (BinUpdateDocument.BinUpdate) get_store().add_element_user(BINUPDATE$0);
			return target;
		}
	}

	/**
	 * Gets the "BinUpdate" element
	 */
	@Override
	public BinUpdateDocument.BinUpdate getBinUpdate() {
		synchronized (monitor()) {
			check_orphaned();
			BinUpdateDocument.BinUpdate target = null;
			target = (BinUpdateDocument.BinUpdate) get_store().find_element_user(BINUPDATE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "BinUpdate" element
	 */
	@Override
	public void setBinUpdate(BinUpdateDocument.BinUpdate binUpdate) {
		generatedSetterHelperImpl(binUpdate, BINUPDATE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
