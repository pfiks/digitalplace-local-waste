/*
 * An XML document type.
 * Localname: insertNewBinResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: InsertNewBinResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.InsertNewBinResponseDocument;

/**
 * A document containing one
 * insertNewBinResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class InsertNewBinResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements InsertNewBinResponseDocument {

	/**
	 * An XML
	 * insertNewBinResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class InsertNewBinResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements InsertNewBinResponseDocument.InsertNewBinResponse {

		/**
		 * An XML
		 * insertNewBinResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class InsertNewBinResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult {

			private static final long serialVersionUID = 1L;

			public InsertNewBinResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName INSERTNEWBINRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "insertNewBinResult");

		private static final long serialVersionUID = 1L;

		public InsertNewBinResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "insertNewBinResult" element
		 */
		@Override
		public InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult addNewInsertNewBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult target = null;
				target = (InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult) get_store().add_element_user(INSERTNEWBINRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "insertNewBinResult" element
		 */
		@Override
		public InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult getInsertNewBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult target = null;
				target = (InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult) get_store().find_element_user(INSERTNEWBINRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "insertNewBinResult" element
		 */
		@Override
		public boolean isSetInsertNewBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(INSERTNEWBINRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "insertNewBinResult" element
		 */
		@Override
		public void setInsertNewBinResult(InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult insertNewBinResult) {
			generatedSetterHelperImpl(insertNewBinResult, INSERTNEWBINRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "insertNewBinResult" element
		 */
		@Override
		public void unsetInsertNewBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(INSERTNEWBINRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName INSERTNEWBINRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "insertNewBinResponse");

	private static final long serialVersionUID = 1L;

	public InsertNewBinResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "insertNewBinResponse" element
	 */
	@Override
	public InsertNewBinResponseDocument.InsertNewBinResponse addNewInsertNewBinResponse() {
		synchronized (monitor()) {
			check_orphaned();
			InsertNewBinResponseDocument.InsertNewBinResponse target = null;
			target = (InsertNewBinResponseDocument.InsertNewBinResponse) get_store().add_element_user(INSERTNEWBINRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "insertNewBinResponse" element
	 */
	@Override
	public InsertNewBinResponseDocument.InsertNewBinResponse getInsertNewBinResponse() {
		synchronized (monitor()) {
			check_orphaned();
			InsertNewBinResponseDocument.InsertNewBinResponse target = null;
			target = (InsertNewBinResponseDocument.InsertNewBinResponse) get_store().find_element_user(INSERTNEWBINRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "insertNewBinResponse" element
	 */
	@Override
	public void setInsertNewBinResponse(InsertNewBinResponseDocument.InsertNewBinResponse insertNewBinResponse) {
		generatedSetterHelperImpl(insertNewBinResponse, INSERTNEWBINRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
