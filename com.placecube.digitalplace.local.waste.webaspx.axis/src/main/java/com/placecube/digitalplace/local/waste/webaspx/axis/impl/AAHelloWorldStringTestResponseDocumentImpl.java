/*
 * An XML document type.
 * Localname: AA_HelloWorld_String_TestResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AAHelloWorldStringTestResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestResponseDocument;

/**
 * A document containing one
 * AA_HelloWorld_String_TestResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AAHelloWorldStringTestResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AAHelloWorldStringTestResponseDocument {

	/**
	 * An XML
	 * AA_HelloWorld_String_TestResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AAHelloWorldStringTestResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse {

		private static final javax.xml.namespace.QName AAHELLOWORLDSTRINGTESTRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"AA_HelloWorld_String_TestResult");

		private static final long serialVersionUID = 1L;

		public AAHelloWorldStringTestResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "AA_HelloWorld_String_TestResult" element
		 */
		@Override
		public java.lang.String getAAHelloWorldStringTestResult() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AAHELLOWORLDSTRINGTESTRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "AA_HelloWorld_String_TestResult" element
		 */
		@Override
		public boolean isSetAAHelloWorldStringTestResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AAHELLOWORLDSTRINGTESTRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AA_HelloWorld_String_TestResult" element
		 */
		@Override
		public void setAAHelloWorldStringTestResult(java.lang.String aaHelloWorldStringTestResult) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AAHELLOWORLDSTRINGTESTRESULT$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(AAHELLOWORLDSTRINGTESTRESULT$0);
				}
				target.setStringValue(aaHelloWorldStringTestResult);
			}
		}

		/**
		 * Unsets the "AA_HelloWorld_String_TestResult" element
		 */
		@Override
		public void unsetAAHelloWorldStringTestResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AAHELLOWORLDSTRINGTESTRESULT$0, 0);
			}
		}

		/**
		 * Gets (as xml) the "AA_HelloWorld_String_TestResult" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAAHelloWorldStringTestResult() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AAHELLOWORLDSTRINGTESTRESULT$0, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "AA_HelloWorld_String_TestResult" element
		 */
		@Override
		public void xsetAAHelloWorldStringTestResult(org.apache.xmlbeans.XmlString aaHelloWorldStringTestResult) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AAHELLOWORLDSTRINGTESTRESULT$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(AAHELLOWORLDSTRINGTESTRESULT$0);
				}
				target.set(aaHelloWorldStringTestResult);
			}
		}
	}

	private static final javax.xml.namespace.QName AAHELLOWORLDSTRINGTESTRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"AA_HelloWorld_String_TestResponse");

	private static final long serialVersionUID = 1L;

	public AAHelloWorldStringTestResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AA_HelloWorld_String_TestResponse"
	 * element
	 */
	@Override
	public AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse addNewAAHelloWorldStringTestResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse target = null;
			target = (AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse) get_store().add_element_user(AAHELLOWORLDSTRINGTESTRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AA_HelloWorld_String_TestResponse" element
	 */
	@Override
	public AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse getAAHelloWorldStringTestResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse target = null;
			target = (AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse) get_store().find_element_user(AAHELLOWORLDSTRINGTESTRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AA_HelloWorld_String_TestResponse" element
	 */
	@Override
	public void setAAHelloWorldStringTestResponse(AAHelloWorldStringTestResponseDocument.AAHelloWorldStringTestResponse aaHelloWorldStringTestResponse) {
		generatedSetterHelperImpl(aaHelloWorldStringTestResponse, AAHELLOWORLDSTRINGTESTRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
