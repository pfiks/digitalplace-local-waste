/*
 * An XML document type.
 * Localname: KeepAliveCallResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: KeepAliveCallResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.KeepAliveCallResponseDocument;

/**
 * A document containing one
 * KeepAliveCallResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class KeepAliveCallResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements KeepAliveCallResponseDocument {

	/**
	 * An XML
	 * KeepAliveCallResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class KeepAliveCallResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements KeepAliveCallResponseDocument.KeepAliveCallResponse {

		/**
		 * An XML
		 * KeepAliveCallResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class KeepAliveCallResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements KeepAliveCallResponseDocument.KeepAliveCallResponse.KeepAliveCallResult {

			private static final long serialVersionUID = 1L;

			public KeepAliveCallResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName KEEPALIVECALLRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "KeepAliveCallResult");

		private static final long serialVersionUID = 1L;

		public KeepAliveCallResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "KeepAliveCallResult" element
		 */
		@Override
		public KeepAliveCallResponseDocument.KeepAliveCallResponse.KeepAliveCallResult addNewKeepAliveCallResult() {
			synchronized (monitor()) {
				check_orphaned();
				KeepAliveCallResponseDocument.KeepAliveCallResponse.KeepAliveCallResult target = null;
				target = (KeepAliveCallResponseDocument.KeepAliveCallResponse.KeepAliveCallResult) get_store().add_element_user(KEEPALIVECALLRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "KeepAliveCallResult" element
		 */
		@Override
		public KeepAliveCallResponseDocument.KeepAliveCallResponse.KeepAliveCallResult getKeepAliveCallResult() {
			synchronized (monitor()) {
				check_orphaned();
				KeepAliveCallResponseDocument.KeepAliveCallResponse.KeepAliveCallResult target = null;
				target = (KeepAliveCallResponseDocument.KeepAliveCallResponse.KeepAliveCallResult) get_store().find_element_user(KEEPALIVECALLRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "KeepAliveCallResult" element
		 */
		@Override
		public boolean isSetKeepAliveCallResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(KEEPALIVECALLRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "KeepAliveCallResult" element
		 */
		@Override
		public void setKeepAliveCallResult(KeepAliveCallResponseDocument.KeepAliveCallResponse.KeepAliveCallResult keepAliveCallResult) {
			generatedSetterHelperImpl(keepAliveCallResult, KEEPALIVECALLRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "KeepAliveCallResult" element
		 */
		@Override
		public void unsetKeepAliveCallResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(KEEPALIVECALLRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName KEEPALIVECALLRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "KeepAliveCallResponse");

	private static final long serialVersionUID = 1L;

	public KeepAliveCallResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "KeepAliveCallResponse" element
	 */
	@Override
	public KeepAliveCallResponseDocument.KeepAliveCallResponse addNewKeepAliveCallResponse() {
		synchronized (monitor()) {
			check_orphaned();
			KeepAliveCallResponseDocument.KeepAliveCallResponse target = null;
			target = (KeepAliveCallResponseDocument.KeepAliveCallResponse) get_store().add_element_user(KEEPALIVECALLRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "KeepAliveCallResponse" element
	 */
	@Override
	public KeepAliveCallResponseDocument.KeepAliveCallResponse getKeepAliveCallResponse() {
		synchronized (monitor()) {
			check_orphaned();
			KeepAliveCallResponseDocument.KeepAliveCallResponse target = null;
			target = (KeepAliveCallResponseDocument.KeepAliveCallResponse) get_store().find_element_user(KEEPALIVECALLRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "KeepAliveCallResponse" element
	 */
	@Override
	public void setKeepAliveCallResponse(KeepAliveCallResponseDocument.KeepAliveCallResponse keepAliveCallResponse) {
		generatedSetterHelperImpl(keepAliveCallResponse, KEEPALIVECALLRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
