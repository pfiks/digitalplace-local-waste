/*
 * An XML document type.
 * Localname: getLatestTruckPositionsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetLatestTruckPositionsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetLatestTruckPositionsResponseDocument;

/**
 * A document containing one
 * getLatestTruckPositionsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetLatestTruckPositionsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetLatestTruckPositionsResponseDocument {

	/**
	 * An XML
	 * getLatestTruckPositionsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetLatestTruckPositionsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse {

		/**
		 * An XML
		 * getLatestTruckPositionsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetLatestTruckPositionsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse.GetLatestTruckPositionsResult {

			private static final long serialVersionUID = 1L;

			public GetLatestTruckPositionsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETLATESTTRUCKPOSITIONSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"getLatestTruckPositionsResult");

		private static final long serialVersionUID = 1L;

		public GetLatestTruckPositionsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "getLatestTruckPositionsResult"
		 * element
		 */
		@Override
		public GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse.GetLatestTruckPositionsResult addNewGetLatestTruckPositionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse.GetLatestTruckPositionsResult target = null;
				target = (GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse.GetLatestTruckPositionsResult) get_store().add_element_user(GETLATESTTRUCKPOSITIONSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getLatestTruckPositionsResult" element
		 */
		@Override
		public GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse.GetLatestTruckPositionsResult getGetLatestTruckPositionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse.GetLatestTruckPositionsResult target = null;
				target = (GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse.GetLatestTruckPositionsResult) get_store().find_element_user(GETLATESTTRUCKPOSITIONSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getLatestTruckPositionsResult" element
		 */
		@Override
		public boolean isSetGetLatestTruckPositionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETLATESTTRUCKPOSITIONSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getLatestTruckPositionsResult" element
		 */
		@Override
		public void setGetLatestTruckPositionsResult(GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse.GetLatestTruckPositionsResult getLatestTruckPositionsResult) {
			generatedSetterHelperImpl(getLatestTruckPositionsResult, GETLATESTTRUCKPOSITIONSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getLatestTruckPositionsResult" element
		 */
		@Override
		public void unsetGetLatestTruckPositionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETLATESTTRUCKPOSITIONSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETLATESTTRUCKPOSITIONSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"getLatestTruckPositionsResponse");

	private static final long serialVersionUID = 1L;

	public GetLatestTruckPositionsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getLatestTruckPositionsResponse" element
	 */
	@Override
	public GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse addNewGetLatestTruckPositionsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse target = null;
			target = (GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse) get_store().add_element_user(GETLATESTTRUCKPOSITIONSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getLatestTruckPositionsResponse" element
	 */
	@Override
	public GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse getGetLatestTruckPositionsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse target = null;
			target = (GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse) get_store().find_element_user(GETLATESTTRUCKPOSITIONSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getLatestTruckPositionsResponse" element
	 */
	@Override
	public void setGetLatestTruckPositionsResponse(GetLatestTruckPositionsResponseDocument.GetLatestTruckPositionsResponse getLatestTruckPositionsResponse) {
		generatedSetterHelperImpl(getLatestTruckPositionsResponse, GETLATESTTRUCKPOSITIONSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
