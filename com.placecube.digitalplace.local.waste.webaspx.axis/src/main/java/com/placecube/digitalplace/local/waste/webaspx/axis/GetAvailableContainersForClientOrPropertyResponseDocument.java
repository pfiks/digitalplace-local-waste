/*
 * An XML document type.
 * Localname: GetAvailableContainersForClientOrPropertyResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAvailableContainersForClientOrPropertyResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetAvailableContainersForClientOrPropertyResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetAvailableContainersForClientOrPropertyResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAvailableContainersForClientOrPropertyResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getavailablecontainersforclientorpropertyresponse9db2doctype");
    
    /**
     * Gets the "GetAvailableContainersForClientOrPropertyResponse" element
     */
    GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse getGetAvailableContainersForClientOrPropertyResponse();
    
    /**
     * Sets the "GetAvailableContainersForClientOrPropertyResponse" element
     */
    void setGetAvailableContainersForClientOrPropertyResponse(GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse getAvailableContainersForClientOrPropertyResponse);
    
    /**
     * Appends and returns a new empty "GetAvailableContainersForClientOrPropertyResponse" element
     */
    GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse addNewGetAvailableContainersForClientOrPropertyResponse();
    
    /**
     * An XML GetAvailableContainersForClientOrPropertyResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetAvailableContainersForClientOrPropertyResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAvailableContainersForClientOrPropertyResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getavailablecontainersforclientorpropertyresponsef586elemtype");
        
        /**
         * Gets the "GetAvailableContainersForClientOrPropertyResult" element
         */
        GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult getGetAvailableContainersForClientOrPropertyResult();
        
        /**
         * True if has "GetAvailableContainersForClientOrPropertyResult" element
         */
        boolean isSetGetAvailableContainersForClientOrPropertyResult();
        
        /**
         * Sets the "GetAvailableContainersForClientOrPropertyResult" element
         */
        void setGetAvailableContainersForClientOrPropertyResult(GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult getAvailableContainersForClientOrPropertyResult);
        
        /**
         * Appends and returns a new empty "GetAvailableContainersForClientOrPropertyResult" element
         */
        GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult addNewGetAvailableContainersForClientOrPropertyResult();
        
        /**
         * Unsets the "GetAvailableContainersForClientOrPropertyResult" element
         */
        void unsetGetAvailableContainersForClientOrPropertyResult();
        
        /**
         * An XML GetAvailableContainersForClientOrPropertyResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetAvailableContainersForClientOrPropertyResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAvailableContainersForClientOrPropertyResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getavailablecontainersforclientorpropertyresultb9f6elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult newInstance() {
                  return (GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse newInstance() {
              return (GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetAvailableContainersForClientOrPropertyResponseDocument newInstance() {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAvailableContainersForClientOrPropertyResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAvailableContainersForClientOrPropertyResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
