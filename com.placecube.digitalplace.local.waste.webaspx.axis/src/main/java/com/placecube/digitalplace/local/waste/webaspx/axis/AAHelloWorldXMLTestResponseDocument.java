/*
 * An XML document type.
 * Localname: AA_HelloWorld_XML_TestResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AAHelloWorldXMLTestResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one AA_HelloWorld_XML_TestResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface AAHelloWorldXMLTestResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AAHelloWorldXMLTestResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("aahelloworldxmltestresponse5fb7doctype");
    
    /**
     * Gets the "AA_HelloWorld_XML_TestResponse" element
     */
    AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse getAAHelloWorldXMLTestResponse();
    
    /**
     * Sets the "AA_HelloWorld_XML_TestResponse" element
     */
    void setAAHelloWorldXMLTestResponse(AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse aaHelloWorldXMLTestResponse);
    
    /**
     * Appends and returns a new empty "AA_HelloWorld_XML_TestResponse" element
     */
    AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse addNewAAHelloWorldXMLTestResponse();
    
    /**
     * An XML AA_HelloWorld_XML_TestResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface AAHelloWorldXMLTestResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AAHelloWorldXMLTestResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("aahelloworldxmltestresponsef120elemtype");
        
        /**
         * Gets the "AA_HelloWorld_XML_TestResult" element
         */
        AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult getAAHelloWorldXMLTestResult();
        
        /**
         * True if has "AA_HelloWorld_XML_TestResult" element
         */
        boolean isSetAAHelloWorldXMLTestResult();
        
        /**
         * Sets the "AA_HelloWorld_XML_TestResult" element
         */
        void setAAHelloWorldXMLTestResult(AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult aaHelloWorldXMLTestResult);
        
        /**
         * Appends and returns a new empty "AA_HelloWorld_XML_TestResult" element
         */
        AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult addNewAAHelloWorldXMLTestResult();
        
        /**
         * Unsets the "AA_HelloWorld_XML_TestResult" element
         */
        void unsetAAHelloWorldXMLTestResult();
        
        /**
         * An XML AA_HelloWorld_XML_TestResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface AAHelloWorldXMLTestResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AAHelloWorldXMLTestResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("aahelloworldxmltestresultd133elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult newInstance() {
                  return (AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse newInstance() {
              return (AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static AAHelloWorldXMLTestResponseDocument newInstance() {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static AAHelloWorldXMLTestResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static AAHelloWorldXMLTestResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static AAHelloWorldXMLTestResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static AAHelloWorldXMLTestResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AAHelloWorldXMLTestResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AAHelloWorldXMLTestResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AAHelloWorldXMLTestResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
