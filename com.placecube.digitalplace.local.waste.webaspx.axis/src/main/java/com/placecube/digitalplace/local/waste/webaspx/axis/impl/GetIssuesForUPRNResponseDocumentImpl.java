/*
 * An XML document type.
 * Localname: GetIssuesForUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetIssuesForUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesForUPRNResponseDocument;

/**
 * A document containing one
 * GetIssuesForUPRNResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetIssuesForUPRNResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesForUPRNResponseDocument {

	/**
	 * An XML
	 * GetIssuesForUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetIssuesForUPRNResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse {

		/**
		 * An XML
		 * GetIssuesForUPRNResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetIssuesForUPRNResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult {

			private static final long serialVersionUID = 1L;

			public GetIssuesForUPRNResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETISSUESFORUPRNRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssuesForUPRNResult");

		private static final long serialVersionUID = 1L;

		public GetIssuesForUPRNResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetIssuesForUPRNResult" element
		 */
		@Override
		public GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult addNewGetIssuesForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult target = null;
				target = (GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult) get_store().add_element_user(GETISSUESFORUPRNRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetIssuesForUPRNResult" element
		 */
		@Override
		public GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult getGetIssuesForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult target = null;
				target = (GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult) get_store().find_element_user(GETISSUESFORUPRNRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetIssuesForUPRNResult" element
		 */
		@Override
		public boolean isSetGetIssuesForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETISSUESFORUPRNRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetIssuesForUPRNResult" element
		 */
		@Override
		public void setGetIssuesForUPRNResult(GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse.GetIssuesForUPRNResult getIssuesForUPRNResult) {
			generatedSetterHelperImpl(getIssuesForUPRNResult, GETISSUESFORUPRNRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetIssuesForUPRNResult" element
		 */
		@Override
		public void unsetGetIssuesForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETISSUESFORUPRNRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETISSUESFORUPRNRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssuesForUPRNResponse");

	private static final long serialVersionUID = 1L;

	public GetIssuesForUPRNResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetIssuesForUPRNResponse" element
	 */
	@Override
	public GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse addNewGetIssuesForUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse target = null;
			target = (GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse) get_store().add_element_user(GETISSUESFORUPRNRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetIssuesForUPRNResponse" element
	 */
	@Override
	public GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse getGetIssuesForUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse target = null;
			target = (GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse) get_store().find_element_user(GETISSUESFORUPRNRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetIssuesForUPRNResponse" element
	 */
	@Override
	public void setGetIssuesForUPRNResponse(GetIssuesForUPRNResponseDocument.GetIssuesForUPRNResponse getIssuesForUPRNResponse) {
		generatedSetterHelperImpl(getIssuesForUPRNResponse, GETISSUESFORUPRNRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
