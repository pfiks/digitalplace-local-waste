/*
 * An XML document type.
 * Localname: LLPGXtraGetUPRNCurrentValuesForDBFieldResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument;

/**
 * A document containing one
 * LLPGXtraGetUPRNCurrentValuesForDBFieldResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
		implements LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument {

	/**
	 * An XML
	 * LLPGXtraGetUPRNCurrentValuesForDBFieldResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class LLPGXtraGetUPRNCurrentValuesForDBFieldResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse {

		/**
		 * An XML
		 * LLPGXtraGetUPRNCurrentValuesForDBFieldResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class LLPGXtraGetUPRNCurrentValuesForDBFieldResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult {

			private static final long serialVersionUID = 1L;

			public LLPGXtraGetUPRNCurrentValuesForDBFieldResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"LLPGXtraGetUPRNCurrentValuesForDBFieldResult");

		private static final long serialVersionUID = 1L;

		public LLPGXtraGetUPRNCurrentValuesForDBFieldResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
		 */
		@Override
		public LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult addNewLLPGXtraGetUPRNCurrentValuesForDBFieldResult() {
			synchronized (monitor()) {
				check_orphaned();
				LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult target = null;
				target = (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult) get_store()
						.add_element_user(LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
		 */
		@Override
		public LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult getLLPGXtraGetUPRNCurrentValuesForDBFieldResult() {
			synchronized (monitor()) {
				check_orphaned();
				LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult target = null;
				target = (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult) get_store()
						.find_element_user(LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
		 */
		@Override
		public boolean isSetLLPGXtraGetUPRNCurrentValuesForDBFieldResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
		 */
		@Override
		public void setLLPGXtraGetUPRNCurrentValuesForDBFieldResult(
				LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult llpgXtraGetUPRNCurrentValuesForDBFieldResult) {
			generatedSetterHelperImpl(llpgXtraGetUPRNCurrentValuesForDBFieldResult, LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
		 */
		@Override
		public void unsetLLPGXtraGetUPRNCurrentValuesForDBFieldResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"LLPGXtraGetUPRNCurrentValuesForDBFieldResponse");

	private static final long serialVersionUID = 1L;

	public LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "LLPGXtraGetUPRNCurrentValuesForDBFieldResponse" element
	 */
	@Override
	public LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse addNewLLPGXtraGetUPRNCurrentValuesForDBFieldResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse target = null;
			target = (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse) get_store()
					.add_element_user(LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResponse" element
	 */
	@Override
	public LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse getLLPGXtraGetUPRNCurrentValuesForDBFieldResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse target = null;
			target = (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse) get_store()
					.find_element_user(LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResponse" element
	 */
	@Override
	public void setLLPGXtraGetUPRNCurrentValuesForDBFieldResponse(
			LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse llpgXtraGetUPRNCurrentValuesForDBFieldResponse) {
		generatedSetterHelperImpl(llpgXtraGetUPRNCurrentValuesForDBFieldResponse, LLPGXTRAGETUPRNCURRENTVALUESFORDBFIELDRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
