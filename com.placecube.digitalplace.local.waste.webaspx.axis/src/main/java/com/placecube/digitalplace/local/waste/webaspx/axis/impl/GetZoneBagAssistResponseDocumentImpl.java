/*
 * An XML document type.
 * Localname: GetZoneBagAssistResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetZoneBagAssistResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetZoneBagAssistResponseDocument;

/**
 * A document containing one
 * GetZoneBagAssistResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetZoneBagAssistResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetZoneBagAssistResponseDocument {

	/**
	 * An XML
	 * GetZoneBagAssistResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetZoneBagAssistResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse {

		/**
		 * An XML
		 * GetZoneBagAssistResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetZoneBagAssistResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse.GetZoneBagAssistResult {

			private static final long serialVersionUID = 1L;

			public GetZoneBagAssistResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETZONEBAGASSISTRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetZoneBagAssistResult");

		private static final long serialVersionUID = 1L;

		public GetZoneBagAssistResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetZoneBagAssistResult" element
		 */
		@Override
		public GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse.GetZoneBagAssistResult addNewGetZoneBagAssistResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse.GetZoneBagAssistResult target = null;
				target = (GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse.GetZoneBagAssistResult) get_store().add_element_user(GETZONEBAGASSISTRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetZoneBagAssistResult" element
		 */
		@Override
		public GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse.GetZoneBagAssistResult getGetZoneBagAssistResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse.GetZoneBagAssistResult target = null;
				target = (GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse.GetZoneBagAssistResult) get_store().find_element_user(GETZONEBAGASSISTRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetZoneBagAssistResult" element
		 */
		@Override
		public boolean isSetGetZoneBagAssistResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETZONEBAGASSISTRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetZoneBagAssistResult" element
		 */
		@Override
		public void setGetZoneBagAssistResult(GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse.GetZoneBagAssistResult getZoneBagAssistResult) {
			generatedSetterHelperImpl(getZoneBagAssistResult, GETZONEBAGASSISTRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetZoneBagAssistResult" element
		 */
		@Override
		public void unsetGetZoneBagAssistResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETZONEBAGASSISTRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETZONEBAGASSISTRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetZoneBagAssistResponse");

	private static final long serialVersionUID = 1L;

	public GetZoneBagAssistResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetZoneBagAssistResponse" element
	 */
	@Override
	public GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse addNewGetZoneBagAssistResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse target = null;
			target = (GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse) get_store().add_element_user(GETZONEBAGASSISTRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetZoneBagAssistResponse" element
	 */
	@Override
	public GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse getGetZoneBagAssistResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse target = null;
			target = (GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse) get_store().find_element_user(GETZONEBAGASSISTRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetZoneBagAssistResponse" element
	 */
	@Override
	public void setGetZoneBagAssistResponse(GetZoneBagAssistResponseDocument.GetZoneBagAssistResponse getZoneBagAssistResponse) {
		generatedSetterHelperImpl(getZoneBagAssistResponse, GETZONEBAGASSISTRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
