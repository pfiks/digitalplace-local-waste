/*
 * An XML document type.
 * Localname: readWriteMissedBin
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: ReadWriteMissedBinDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one readWriteMissedBin(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface ReadWriteMissedBinDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ReadWriteMissedBinDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("readwritemissedbin969fdoctype");
    
    /**
     * Gets the "readWriteMissedBin" element
     */
    ReadWriteMissedBinDocument.ReadWriteMissedBin getReadWriteMissedBin();
    
    /**
     * Sets the "readWriteMissedBin" element
     */
    void setReadWriteMissedBin(ReadWriteMissedBinDocument.ReadWriteMissedBin readWriteMissedBin);
    
    /**
     * Appends and returns a new empty "readWriteMissedBin" element
     */
    ReadWriteMissedBinDocument.ReadWriteMissedBin addNewReadWriteMissedBin();
    
    /**
     * An XML readWriteMissedBin(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface ReadWriteMissedBin extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ReadWriteMissedBin.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("readwritemissedbinc120elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "UPRN" element
         */
        java.lang.String getUPRN();
        
        /**
         * Gets (as xml) the "UPRN" element
         */
        org.apache.xmlbeans.XmlString xgetUPRN();
        
        /**
         * True if has "UPRN" element
         */
        boolean isSetUPRN();
        
        /**
         * Sets the "UPRN" element
         */
        void setUPRN(java.lang.String uprn);
        
        /**
         * Sets (as xml) the "UPRN" element
         */
        void xsetUPRN(org.apache.xmlbeans.XmlString uprn);
        
        /**
         * Unsets the "UPRN" element
         */
        void unsetUPRN();
        
        /**
         * Gets the "service3L" element
         */
        java.lang.String getService3L();
        
        /**
         * Gets (as xml) the "service3L" element
         */
        org.apache.xmlbeans.XmlString xgetService3L();
        
        /**
         * True if has "service3L" element
         */
        boolean isSetService3L();
        
        /**
         * Sets the "service3L" element
         */
        void setService3L(java.lang.String service3L);
        
        /**
         * Sets (as xml) the "service3L" element
         */
        void xsetService3L(org.apache.xmlbeans.XmlString service3L);
        
        /**
         * Unsets the "service3L" element
         */
        void unsetService3L();
        
        /**
         * Gets the "missedYN" element
         */
        java.lang.String getMissedYN();
        
        /**
         * Gets (as xml) the "missedYN" element
         */
        org.apache.xmlbeans.XmlString xgetMissedYN();
        
        /**
         * True if has "missedYN" element
         */
        boolean isSetMissedYN();
        
        /**
         * Sets the "missedYN" element
         */
        void setMissedYN(java.lang.String missedYN);
        
        /**
         * Sets (as xml) the "missedYN" element
         */
        void xsetMissedYN(org.apache.xmlbeans.XmlString missedYN);
        
        /**
         * Unsets the "missedYN" element
         */
        void unsetMissedYN();
        
        /**
         * Gets the "missedDateDDsMMsYY" element
         */
        java.lang.String getMissedDateDDsMMsYY();
        
        /**
         * Gets (as xml) the "missedDateDDsMMsYY" element
         */
        org.apache.xmlbeans.XmlString xgetMissedDateDDsMMsYY();
        
        /**
         * True if has "missedDateDDsMMsYY" element
         */
        boolean isSetMissedDateDDsMMsYY();
        
        /**
         * Sets the "missedDateDDsMMsYY" element
         */
        void setMissedDateDDsMMsYY(java.lang.String missedDateDDsMMsYY);
        
        /**
         * Sets (as xml) the "missedDateDDsMMsYY" element
         */
        void xsetMissedDateDDsMMsYY(org.apache.xmlbeans.XmlString missedDateDDsMMsYY);
        
        /**
         * Unsets the "missedDateDDsMMsYY" element
         */
        void unsetMissedDateDDsMMsYY();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static ReadWriteMissedBinDocument.ReadWriteMissedBin newInstance() {
              return (ReadWriteMissedBinDocument.ReadWriteMissedBin) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static ReadWriteMissedBinDocument.ReadWriteMissedBin newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (ReadWriteMissedBinDocument.ReadWriteMissedBin) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static ReadWriteMissedBinDocument newInstance() {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static ReadWriteMissedBinDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static ReadWriteMissedBinDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static ReadWriteMissedBinDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static ReadWriteMissedBinDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static ReadWriteMissedBinDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static ReadWriteMissedBinDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static ReadWriteMissedBinDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static ReadWriteMissedBinDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static ReadWriteMissedBinDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static ReadWriteMissedBinDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static ReadWriteMissedBinDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static ReadWriteMissedBinDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static ReadWriteMissedBinDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static ReadWriteMissedBinDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static ReadWriteMissedBinDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static ReadWriteMissedBinDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static ReadWriteMissedBinDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (ReadWriteMissedBinDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
