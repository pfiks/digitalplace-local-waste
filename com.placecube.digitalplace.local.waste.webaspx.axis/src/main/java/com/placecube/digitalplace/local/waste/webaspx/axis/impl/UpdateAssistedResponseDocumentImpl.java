/*
 * An XML document type.
 * Localname: UpdateAssistedResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: UpdateAssistedResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedResponseDocument;

/**
 * A document containing one
 * UpdateAssistedResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class UpdateAssistedResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements UpdateAssistedResponseDocument {

	/**
	 * An XML
	 * UpdateAssistedResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class UpdateAssistedResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements UpdateAssistedResponseDocument.UpdateAssistedResponse {

		/**
		 * An XML
		 * UpdateAssistedResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class UpdateAssistedResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements UpdateAssistedResponseDocument.UpdateAssistedResponse.UpdateAssistedResult {

			private static final long serialVersionUID = 1L;

			public UpdateAssistedResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final long serialVersionUID = 1L;

		private static final javax.xml.namespace.QName UPDATEASSISTEDRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateAssistedResult");

		public UpdateAssistedResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "UpdateAssistedResult" element
		 */
		@Override
		public UpdateAssistedResponseDocument.UpdateAssistedResponse.UpdateAssistedResult addNewUpdateAssistedResult() {
			synchronized (monitor()) {
				check_orphaned();
				UpdateAssistedResponseDocument.UpdateAssistedResponse.UpdateAssistedResult target = null;
				target = (UpdateAssistedResponseDocument.UpdateAssistedResponse.UpdateAssistedResult) get_store().add_element_user(UPDATEASSISTEDRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "UpdateAssistedResult" element
		 */
		@Override
		public UpdateAssistedResponseDocument.UpdateAssistedResponse.UpdateAssistedResult getUpdateAssistedResult() {
			synchronized (monitor()) {
				check_orphaned();
				UpdateAssistedResponseDocument.UpdateAssistedResponse.UpdateAssistedResult target = null;
				target = (UpdateAssistedResponseDocument.UpdateAssistedResponse.UpdateAssistedResult) get_store().find_element_user(UPDATEASSISTEDRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "UpdateAssistedResult" element
		 */
		@Override
		public boolean isSetUpdateAssistedResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPDATEASSISTEDRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "UpdateAssistedResult" element
		 */
		@Override
		public void setUpdateAssistedResult(UpdateAssistedResponseDocument.UpdateAssistedResponse.UpdateAssistedResult updateAssistedResult) {
			generatedSetterHelperImpl(updateAssistedResult, UPDATEASSISTEDRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "UpdateAssistedResult" element
		 */
		@Override
		public void unsetUpdateAssistedResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPDATEASSISTEDRESULT$0, 0);
			}
		}
	}

	private static final long serialVersionUID = 1L;

	private static final javax.xml.namespace.QName UPDATEASSISTEDRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateAssistedResponse");

	public UpdateAssistedResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "UpdateAssistedResponse" element
	 */
	@Override
	public UpdateAssistedResponseDocument.UpdateAssistedResponse addNewUpdateAssistedResponse() {
		synchronized (monitor()) {
			check_orphaned();
			UpdateAssistedResponseDocument.UpdateAssistedResponse target = null;
			target = (UpdateAssistedResponseDocument.UpdateAssistedResponse) get_store().add_element_user(UPDATEASSISTEDRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "UpdateAssistedResponse" element
	 */
	@Override
	public UpdateAssistedResponseDocument.UpdateAssistedResponse getUpdateAssistedResponse() {
		synchronized (monitor()) {
			check_orphaned();
			UpdateAssistedResponseDocument.UpdateAssistedResponse target = null;
			target = (UpdateAssistedResponseDocument.UpdateAssistedResponse) get_store().find_element_user(UPDATEASSISTEDRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "UpdateAssistedResponse" element
	 */
	@Override
	public void setUpdateAssistedResponse(UpdateAssistedResponseDocument.UpdateAssistedResponse updateAssistedResponse) {
		generatedSetterHelperImpl(updateAssistedResponse, UPDATEASSISTEDRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
