/*
 * An XML document type.
 * Localname: getAllIssuesForIssueTypeResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAllIssuesForIssueTypeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getAllIssuesForIssueTypeResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetAllIssuesForIssueTypeResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllIssuesForIssueTypeResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getallissuesforissuetyperesponsedd8cdoctype");
    
    /**
     * Gets the "getAllIssuesForIssueTypeResponse" element
     */
    GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse getGetAllIssuesForIssueTypeResponse();
    
    /**
     * Sets the "getAllIssuesForIssueTypeResponse" element
     */
    void setGetAllIssuesForIssueTypeResponse(GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse getAllIssuesForIssueTypeResponse);
    
    /**
     * Appends and returns a new empty "getAllIssuesForIssueTypeResponse" element
     */
    GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse addNewGetAllIssuesForIssueTypeResponse();
    
    /**
     * An XML getAllIssuesForIssueTypeResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetAllIssuesForIssueTypeResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllIssuesForIssueTypeResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getallissuesforissuetyperesponse8680elemtype");
        
        /**
         * Gets the "getAllIssuesForIssueTypeResult" element
         */
        GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult getGetAllIssuesForIssueTypeResult();
        
        /**
         * True if has "getAllIssuesForIssueTypeResult" element
         */
        boolean isSetGetAllIssuesForIssueTypeResult();
        
        /**
         * Sets the "getAllIssuesForIssueTypeResult" element
         */
        void setGetAllIssuesForIssueTypeResult(GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult getAllIssuesForIssueTypeResult);
        
        /**
         * Appends and returns a new empty "getAllIssuesForIssueTypeResult" element
         */
        GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult addNewGetAllIssuesForIssueTypeResult();
        
        /**
         * Unsets the "getAllIssuesForIssueTypeResult" element
         */
        void unsetGetAllIssuesForIssueTypeResult();
        
        /**
         * An XML getAllIssuesForIssueTypeResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetAllIssuesForIssueTypeResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllIssuesForIssueTypeResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getallissuesforissuetyperesult7ca8elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult newInstance() {
                  return (GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse.GetAllIssuesForIssueTypeResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse newInstance() {
              return (GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetAllIssuesForIssueTypeResponseDocument.GetAllIssuesForIssueTypeResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetAllIssuesForIssueTypeResponseDocument newInstance() {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetAllIssuesForIssueTypeResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAllIssuesForIssueTypeResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAllIssuesForIssueTypeResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAllIssuesForIssueTypeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
