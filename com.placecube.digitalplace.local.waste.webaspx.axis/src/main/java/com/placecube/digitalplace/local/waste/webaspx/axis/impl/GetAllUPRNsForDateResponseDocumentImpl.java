/*
 * An XML document type.
 * Localname: GetAllUPRNsForDateResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAllUPRNsForDateResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllUPRNsForDateResponseDocument;

/**
 * A document containing one
 * GetAllUPRNsForDateResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAllUPRNsForDateResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAllUPRNsForDateResponseDocument {

	/**
	 * An XML
	 * GetAllUPRNsForDateResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAllUPRNsForDateResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse {

		/**
		 * An XML
		 * GetAllUPRNsForDateResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetAllUPRNsForDateResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult {

			private static final long serialVersionUID = 1L;

			public GetAllUPRNsForDateResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETALLUPRNSFORDATERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAllUPRNsForDateResult");

		private static final long serialVersionUID = 1L;

		public GetAllUPRNsForDateResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetAllUPRNsForDateResult" element
		 */
		@Override
		public GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult addNewGetAllUPRNsForDateResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult target = null;
				target = (GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult) get_store().add_element_user(GETALLUPRNSFORDATERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetAllUPRNsForDateResult" element
		 */
		@Override
		public GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult getGetAllUPRNsForDateResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult target = null;
				target = (GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult) get_store().find_element_user(GETALLUPRNSFORDATERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetAllUPRNsForDateResult" element
		 */
		@Override
		public boolean isSetGetAllUPRNsForDateResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETALLUPRNSFORDATERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetAllUPRNsForDateResult" element
		 */
		@Override
		public void setGetAllUPRNsForDateResult(GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult getAllUPRNsForDateResult) {
			generatedSetterHelperImpl(getAllUPRNsForDateResult, GETALLUPRNSFORDATERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetAllUPRNsForDateResult" element
		 */
		@Override
		public void unsetGetAllUPRNsForDateResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETALLUPRNSFORDATERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETALLUPRNSFORDATERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAllUPRNsForDateResponse");

	private static final long serialVersionUID = 1L;

	public GetAllUPRNsForDateResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetAllUPRNsForDateResponse" element
	 */
	@Override
	public GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse addNewGetAllUPRNsForDateResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse target = null;
			target = (GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse) get_store().add_element_user(GETALLUPRNSFORDATERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetAllUPRNsForDateResponse" element
	 */
	@Override
	public GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse getGetAllUPRNsForDateResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse target = null;
			target = (GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse) get_store().find_element_user(GETALLUPRNSFORDATERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetAllUPRNsForDateResponse" element
	 */
	@Override
	public void setGetAllUPRNsForDateResponse(GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse getAllUPRNsForDateResponse) {
		generatedSetterHelperImpl(getAllUPRNsForDateResponse, GETALLUPRNSFORDATERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
