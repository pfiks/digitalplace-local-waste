/*
 * An XML document type.
 * Localname: getAllIssuesForIssueType
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAllIssuesForIssueTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getAllIssuesForIssueType(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetAllIssuesForIssueTypeDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllIssuesForIssueTypeDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getallissuesforissuetype6eebdoctype");
    
    /**
     * Gets the "getAllIssuesForIssueType" element
     */
    GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType getGetAllIssuesForIssueType();
    
    /**
     * Sets the "getAllIssuesForIssueType" element
     */
    void setGetAllIssuesForIssueType(GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType getAllIssuesForIssueType);
    
    /**
     * Appends and returns a new empty "getAllIssuesForIssueType" element
     */
    GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType addNewGetAllIssuesForIssueType();
    
    /**
     * An XML getAllIssuesForIssueType(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetAllIssuesForIssueType extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllIssuesForIssueType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getallissuesforissuetype2820elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "issueType" element
         */
        java.lang.String getIssueType();
        
        /**
         * Gets (as xml) the "issueType" element
         */
        org.apache.xmlbeans.XmlString xgetIssueType();
        
        /**
         * True if has "issueType" element
         */
        boolean isSetIssueType();
        
        /**
         * Sets the "issueType" element
         */
        void setIssueType(java.lang.String issueType);
        
        /**
         * Sets (as xml) the "issueType" element
         */
        void xsetIssueType(org.apache.xmlbeans.XmlString issueType);
        
        /**
         * Unsets the "issueType" element
         */
        void unsetIssueType();
        
        /**
         * Gets the "startDateDDMMYYYY" element
         */
        java.lang.String getStartDateDDMMYYYY();
        
        /**
         * Gets (as xml) the "startDateDDMMYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetStartDateDDMMYYYY();
        
        /**
         * True if has "startDateDDMMYYYY" element
         */
        boolean isSetStartDateDDMMYYYY();
        
        /**
         * Sets the "startDateDDMMYYYY" element
         */
        void setStartDateDDMMYYYY(java.lang.String startDateDDMMYYYY);
        
        /**
         * Sets (as xml) the "startDateDDMMYYYY" element
         */
        void xsetStartDateDDMMYYYY(org.apache.xmlbeans.XmlString startDateDDMMYYYY);
        
        /**
         * Unsets the "startDateDDMMYYYY" element
         */
        void unsetStartDateDDMMYYYY();
        
        /**
         * Gets the "endDateDDMMYYYY" element
         */
        java.lang.String getEndDateDDMMYYYY();
        
        /**
         * Gets (as xml) the "endDateDDMMYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetEndDateDDMMYYYY();
        
        /**
         * True if has "endDateDDMMYYYY" element
         */
        boolean isSetEndDateDDMMYYYY();
        
        /**
         * Sets the "endDateDDMMYYYY" element
         */
        void setEndDateDDMMYYYY(java.lang.String endDateDDMMYYYY);
        
        /**
         * Sets (as xml) the "endDateDDMMYYYY" element
         */
        void xsetEndDateDDMMYYYY(org.apache.xmlbeans.XmlString endDateDDMMYYYY);
        
        /**
         * Unsets the "endDateDDMMYYYY" element
         */
        void unsetEndDateDDMMYYYY();
        
        /**
         * Gets the "digTime" element
         */
        java.lang.String getDigTime();
        
        /**
         * Gets (as xml) the "digTime" element
         */
        org.apache.xmlbeans.XmlString xgetDigTime();
        
        /**
         * True if has "digTime" element
         */
        boolean isSetDigTime();
        
        /**
         * Sets the "digTime" element
         */
        void setDigTime(java.lang.String digTime);
        
        /**
         * Sets (as xml) the "digTime" element
         */
        void xsetDigTime(org.apache.xmlbeans.XmlString digTime);
        
        /**
         * Unsets the "digTime" element
         */
        void unsetDigTime();
        
        /**
         * Gets the "service" element
         */
        java.lang.String getService();
        
        /**
         * Gets (as xml) the "service" element
         */
        org.apache.xmlbeans.XmlString xgetService();
        
        /**
         * True if has "service" element
         */
        boolean isSetService();
        
        /**
         * Sets the "service" element
         */
        void setService(java.lang.String service);
        
        /**
         * Sets (as xml) the "service" element
         */
        void xsetService(org.apache.xmlbeans.XmlString service);
        
        /**
         * Unsets the "service" element
         */
        void unsetService();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType newInstance() {
              return (GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetAllIssuesForIssueTypeDocument newInstance() {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetAllIssuesForIssueTypeDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetAllIssuesForIssueTypeDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetAllIssuesForIssueTypeDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetAllIssuesForIssueTypeDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAllIssuesForIssueTypeDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAllIssuesForIssueTypeDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAllIssuesForIssueTypeDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
