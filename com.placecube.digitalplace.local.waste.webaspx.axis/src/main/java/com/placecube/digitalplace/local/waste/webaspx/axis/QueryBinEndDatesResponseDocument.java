/*
 * An XML document type.
 * Localname: QueryBinEndDatesResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: QueryBinEndDatesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one QueryBinEndDatesResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface QueryBinEndDatesResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(QueryBinEndDatesResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("querybinenddatesresponse8746doctype");
    
    /**
     * Gets the "QueryBinEndDatesResponse" element
     */
    QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse getQueryBinEndDatesResponse();
    
    /**
     * Sets the "QueryBinEndDatesResponse" element
     */
    void setQueryBinEndDatesResponse(QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse queryBinEndDatesResponse);
    
    /**
     * Appends and returns a new empty "QueryBinEndDatesResponse" element
     */
    QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse addNewQueryBinEndDatesResponse();
    
    /**
     * An XML QueryBinEndDatesResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface QueryBinEndDatesResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(QueryBinEndDatesResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("querybinenddatesresponsec740elemtype");
        
        /**
         * Gets the "QueryBinEndDatesResult" element
         */
        QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult getQueryBinEndDatesResult();
        
        /**
         * True if has "QueryBinEndDatesResult" element
         */
        boolean isSetQueryBinEndDatesResult();
        
        /**
         * Sets the "QueryBinEndDatesResult" element
         */
        void setQueryBinEndDatesResult(QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult queryBinEndDatesResult);
        
        /**
         * Appends and returns a new empty "QueryBinEndDatesResult" element
         */
        QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult addNewQueryBinEndDatesResult();
        
        /**
         * Unsets the "QueryBinEndDatesResult" element
         */
        void unsetQueryBinEndDatesResult();
        
        /**
         * An XML QueryBinEndDatesResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface QueryBinEndDatesResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(QueryBinEndDatesResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("querybinenddatesresultfd22elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult newInstance() {
                  return (QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse newInstance() {
              return (QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static QueryBinEndDatesResponseDocument newInstance() {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static QueryBinEndDatesResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static QueryBinEndDatesResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static QueryBinEndDatesResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static QueryBinEndDatesResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static QueryBinEndDatesResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static QueryBinEndDatesResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static QueryBinEndDatesResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static QueryBinEndDatesResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static QueryBinEndDatesResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static QueryBinEndDatesResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static QueryBinEndDatesResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static QueryBinEndDatesResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static QueryBinEndDatesResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static QueryBinEndDatesResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static QueryBinEndDatesResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static QueryBinEndDatesResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static QueryBinEndDatesResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (QueryBinEndDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
