/*
 * An XML document type.
 * Localname: GetBinDetailsForServiceResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetBinDetailsForServiceResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetBinDetailsForServiceResponseDocument;

/**
 * A document containing one
 * GetBinDetailsForServiceResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetBinDetailsForServiceResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetBinDetailsForServiceResponseDocument {

	/**
	 * An XML
	 * GetBinDetailsForServiceResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetBinDetailsForServiceResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse {

		/**
		 * An XML
		 * GetBinDetailsForServiceResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetBinDetailsForServiceResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult {

			private static final long serialVersionUID = 1L;

			public GetBinDetailsForServiceResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETBINDETAILSFORSERVICERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"GetBinDetailsForServiceResult");

		private static final long serialVersionUID = 1L;

		public GetBinDetailsForServiceResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetBinDetailsForServiceResult"
		 * element
		 */
		@Override
		public GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult addNewGetBinDetailsForServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult target = null;
				target = (GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult) get_store().add_element_user(GETBINDETAILSFORSERVICERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetBinDetailsForServiceResult" element
		 */
		@Override
		public GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult getGetBinDetailsForServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult target = null;
				target = (GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult) get_store().find_element_user(GETBINDETAILSFORSERVICERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetBinDetailsForServiceResult" element
		 */
		@Override
		public boolean isSetGetBinDetailsForServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETBINDETAILSFORSERVICERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetBinDetailsForServiceResult" element
		 */
		@Override
		public void setGetBinDetailsForServiceResult(GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult getBinDetailsForServiceResult) {
			generatedSetterHelperImpl(getBinDetailsForServiceResult, GETBINDETAILSFORSERVICERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetBinDetailsForServiceResult" element
		 */
		@Override
		public void unsetGetBinDetailsForServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETBINDETAILSFORSERVICERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETBINDETAILSFORSERVICERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"GetBinDetailsForServiceResponse");

	private static final long serialVersionUID = 1L;

	public GetBinDetailsForServiceResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetBinDetailsForServiceResponse" element
	 */
	@Override
	public GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse addNewGetBinDetailsForServiceResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse target = null;
			target = (GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse) get_store().add_element_user(GETBINDETAILSFORSERVICERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetBinDetailsForServiceResponse" element
	 */
	@Override
	public GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse getGetBinDetailsForServiceResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse target = null;
			target = (GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse) get_store().find_element_user(GETBINDETAILSFORSERVICERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetBinDetailsForServiceResponse" element
	 */
	@Override
	public void setGetBinDetailsForServiceResponse(GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse getBinDetailsForServiceResponse) {
		generatedSetterHelperImpl(getBinDetailsForServiceResponse, GETBINDETAILSFORSERVICERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
