/*
 * An XML document type.
 * Localname: countSubscriptions
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CountSubscriptionsDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsDocument;

/**
 * A document containing one
 * countSubscriptions(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class CountSubscriptionsDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CountSubscriptionsDocument {

	/**
	 * An XML
	 * countSubscriptions(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class CountSubscriptionsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CountSubscriptionsDocument.CountSubscriptions {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName ENDDATEDDSMMSYYYY$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "endDateDDsMMsYYYY");

		private static final javax.xml.namespace.QName RETURNUPRNS$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "returnUPRNs");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName STARTDATEDDSMMSYYYY$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "startDateDDsMMsYYYY");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public CountSubscriptionsImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "endDateDDsMMsYYYY" element
		 */
		@Override
		public java.lang.String getEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ENDDATEDDSMMSYYYY$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "returnUPRNs" element
		 */
		@Override
		public java.lang.String getReturnUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(RETURNUPRNS$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "startDateDDsMMsYYYY" element
		 */
		@Override
		public java.lang.String getStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STARTDATEDDSMMSYYYY$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "endDateDDsMMsYYYY" element
		 */
		@Override
		public boolean isSetEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ENDDATEDDSMMSYYYY$10) != 0;
			}
		}

		/**
		 * True if has "returnUPRNs" element
		 */
		@Override
		public boolean isSetReturnUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(RETURNUPRNS$12) != 0;
			}
		}

		/**
		 * True if has "startDateDDsMMsYYYY" element
		 */
		@Override
		public boolean isSetStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(STARTDATEDDSMMSYYYY$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "endDateDDsMMsYYYY" element
		 */
		@Override
		public void setEndDateDDsMMsYYYY(java.lang.String endDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ENDDATEDDSMMSYYYY$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ENDDATEDDSMMSYYYY$10);
				}
				target.setStringValue(endDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets the "returnUPRNs" element
		 */
		@Override
		public void setReturnUPRNs(java.lang.String returnUPRNs) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(RETURNUPRNS$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(RETURNUPRNS$12);
				}
				target.setStringValue(returnUPRNs);
			}
		}

		/**
		 * Sets the "startDateDDsMMsYYYY" element
		 */
		@Override
		public void setStartDateDDsMMsYYYY(java.lang.String startDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STARTDATEDDSMMSYYYY$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(STARTDATEDDSMMSYYYY$8);
				}
				target.setStringValue(startDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "endDateDDsMMsYYYY" element
		 */
		@Override
		public void unsetEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ENDDATEDDSMMSYYYY$10, 0);
			}
		}

		/**
		 * Unsets the "returnUPRNs" element
		 */
		@Override
		public void unsetReturnUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(RETURNUPRNS$12, 0);
			}
		}

		/**
		 * Unsets the "startDateDDsMMsYYYY" element
		 */
		@Override
		public void unsetStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(STARTDATEDDSMMSYYYY$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "endDateDDsMMsYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ENDDATEDDSMMSYYYY$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "returnUPRNs" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetReturnUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(RETURNUPRNS$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "startDateDDsMMsYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STARTDATEDDSMMSYYYY$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "endDateDDsMMsYYYY" element
		 */
		@Override
		public void xsetEndDateDDsMMsYYYY(org.apache.xmlbeans.XmlString endDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ENDDATEDDSMMSYYYY$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ENDDATEDDSMMSYYYY$10);
				}
				target.set(endDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets (as xml) the "returnUPRNs" element
		 */
		@Override
		public void xsetReturnUPRNs(org.apache.xmlbeans.XmlString returnUPRNs) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(RETURNUPRNS$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(RETURNUPRNS$12);
				}
				target.set(returnUPRNs);
			}
		}

		/**
		 * Sets (as xml) the "startDateDDsMMsYYYY" element
		 */
		@Override
		public void xsetStartDateDDsMMsYYYY(org.apache.xmlbeans.XmlString startDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STARTDATEDDSMMSYYYY$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(STARTDATEDDSMMSYYYY$8);
				}
				target.set(startDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName COUNTSUBSCRIPTIONS$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countSubscriptions");

	private static final long serialVersionUID = 1L;

	public CountSubscriptionsDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "countSubscriptions" element
	 */
	@Override
	public CountSubscriptionsDocument.CountSubscriptions addNewCountSubscriptions() {
		synchronized (monitor()) {
			check_orphaned();
			CountSubscriptionsDocument.CountSubscriptions target = null;
			target = (CountSubscriptionsDocument.CountSubscriptions) get_store().add_element_user(COUNTSUBSCRIPTIONS$0);
			return target;
		}
	}

	/**
	 * Gets the "countSubscriptions" element
	 */
	@Override
	public CountSubscriptionsDocument.CountSubscriptions getCountSubscriptions() {
		synchronized (monitor()) {
			check_orphaned();
			CountSubscriptionsDocument.CountSubscriptions target = null;
			target = (CountSubscriptionsDocument.CountSubscriptions) get_store().find_element_user(COUNTSUBSCRIPTIONS$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "countSubscriptions" element
	 */
	@Override
	public void setCountSubscriptions(CountSubscriptionsDocument.CountSubscriptions countSubscriptions) {
		generatedSetterHelperImpl(countSubscriptions, COUNTSUBSCRIPTIONS$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
