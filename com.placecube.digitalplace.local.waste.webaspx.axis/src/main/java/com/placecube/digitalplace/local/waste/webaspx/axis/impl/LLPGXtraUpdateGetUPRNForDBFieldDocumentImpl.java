/*
 * An XML document type.
 * Localname: LLPGXtraUpdateGetUPRNForDBField
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LLPGXtraUpdateGetUPRNForDBFieldDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetUPRNForDBFieldDocument;

/**
 * A document containing one
 * LLPGXtraUpdateGetUPRNForDBField(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class LLPGXtraUpdateGetUPRNForDBFieldDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements LLPGXtraUpdateGetUPRNForDBFieldDocument {

	/**
	 * An XML
	 * LLPGXtraUpdateGetUPRNForDBField(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class LLPGXtraUpdateGetUPRNForDBFieldImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName DATABASEFIELDNAME$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "databaseFieldName");

		private static final javax.xml.namespace.QName NEWVALUE$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "newValue");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName UPRNS$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRNs");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public LLPGXtraUpdateGetUPRNForDBFieldImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "databaseFieldName" element
		 */
		@Override
		public java.lang.String getDatabaseFieldName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATABASEFIELDNAME$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "newValue" element
		 */
		@Override
		public java.lang.String getNewValue() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(NEWVALUE$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRNs" element
		 */
		@Override
		public java.lang.String getUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRNS$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "databaseFieldName" element
		 */
		@Override
		public boolean isSetDatabaseFieldName() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DATABASEFIELDNAME$10) != 0;
			}
		}

		/**
		 * True if has "newValue" element
		 */
		@Override
		public boolean isSetNewValue() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(NEWVALUE$12) != 0;
			}
		}

		/**
		 * True if has "UPRNs" element
		 */
		@Override
		public boolean isSetUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRNS$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "databaseFieldName" element
		 */
		@Override
		public void setDatabaseFieldName(java.lang.String databaseFieldName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATABASEFIELDNAME$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DATABASEFIELDNAME$10);
				}
				target.setStringValue(databaseFieldName);
			}
		}

		/**
		 * Sets the "newValue" element
		 */
		@Override
		public void setNewValue(java.lang.String newValue) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(NEWVALUE$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(NEWVALUE$12);
				}
				target.setStringValue(newValue);
			}
		}

		/**
		 * Sets the "UPRNs" element
		 */
		@Override
		public void setUPRNs(java.lang.String uprNs) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRNS$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRNS$8);
				}
				target.setStringValue(uprNs);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "databaseFieldName" element
		 */
		@Override
		public void unsetDatabaseFieldName() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DATABASEFIELDNAME$10, 0);
			}
		}

		/**
		 * Unsets the "newValue" element
		 */
		@Override
		public void unsetNewValue() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(NEWVALUE$12, 0);
			}
		}

		/**
		 * Unsets the "UPRNs" element
		 */
		@Override
		public void unsetUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRNS$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "databaseFieldName" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDatabaseFieldName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATABASEFIELDNAME$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "newValue" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetNewValue() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(NEWVALUE$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRNs" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRNs() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRNS$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "databaseFieldName" element
		 */
		@Override
		public void xsetDatabaseFieldName(org.apache.xmlbeans.XmlString databaseFieldName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATABASEFIELDNAME$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DATABASEFIELDNAME$10);
				}
				target.set(databaseFieldName);
			}
		}

		/**
		 * Sets (as xml) the "newValue" element
		 */
		@Override
		public void xsetNewValue(org.apache.xmlbeans.XmlString newValue) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(NEWVALUE$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(NEWVALUE$12);
				}
				target.set(newValue);
			}
		}

		/**
		 * Sets (as xml) the "UPRNs" element
		 */
		@Override
		public void xsetUPRNs(org.apache.xmlbeans.XmlString uprNs) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRNS$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRNS$8);
				}
				target.set(uprNs);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName LLPGXTRAUPDATEGETUPRNFORDBFIELD$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"LLPGXtraUpdateGetUPRNForDBField");

	private static final long serialVersionUID = 1L;

	public LLPGXtraUpdateGetUPRNForDBFieldDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "LLPGXtraUpdateGetUPRNForDBField" element
	 */
	@Override
	public LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField addNewLLPGXtraUpdateGetUPRNForDBField() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField target = null;
			target = (LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField) get_store().add_element_user(LLPGXTRAUPDATEGETUPRNFORDBFIELD$0);
			return target;
		}
	}

	/**
	 * Gets the "LLPGXtraUpdateGetUPRNForDBField" element
	 */
	@Override
	public LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField getLLPGXtraUpdateGetUPRNForDBField() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField target = null;
			target = (LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField) get_store().find_element_user(LLPGXTRAUPDATEGETUPRNFORDBFIELD$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "LLPGXtraUpdateGetUPRNForDBField" element
	 */
	@Override
	public void setLLPGXtraUpdateGetUPRNForDBField(LLPGXtraUpdateGetUPRNForDBFieldDocument.LLPGXtraUpdateGetUPRNForDBField llpgXtraUpdateGetUPRNForDBField) {
		generatedSetterHelperImpl(llpgXtraUpdateGetUPRNForDBField, LLPGXTRAUPDATEGETUPRNFORDBFIELD$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
