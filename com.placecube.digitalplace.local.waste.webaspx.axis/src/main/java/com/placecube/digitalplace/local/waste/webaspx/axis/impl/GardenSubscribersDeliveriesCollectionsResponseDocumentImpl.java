/*
 * An XML document type.
 * Localname: Garden_Subscribers_Deliveries_CollectionsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscribersDeliveriesCollectionsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscribersDeliveriesCollectionsResponseDocument;

/**
 * A document containing one
 * Garden_Subscribers_Deliveries_CollectionsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GardenSubscribersDeliveriesCollectionsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
		implements GardenSubscribersDeliveriesCollectionsResponseDocument {

	/**
	 * An XML
	 * Garden_Subscribers_Deliveries_CollectionsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GardenSubscribersDeliveriesCollectionsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse {

		/**
		 * An XML
		 * Garden_Subscribers_Deliveries_CollectionsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GardenSubscribersDeliveriesCollectionsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult {

			private static final long serialVersionUID = 1L;

			public GardenSubscribersDeliveriesCollectionsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"Garden_Subscribers_Deliveries_CollectionsResult");

		private static final long serialVersionUID = 1L;

		public GardenSubscribersDeliveriesCollectionsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "Garden_Subscribers_Deliveries_CollectionsResult" element
		 */
		@Override
		public GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult addNewGardenSubscribersDeliveriesCollectionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult target = null;
				target = (GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult) get_store()
						.add_element_user(GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "Garden_Subscribers_Deliveries_CollectionsResult" element
		 */
		@Override
		public GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult getGardenSubscribersDeliveriesCollectionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult target = null;
				target = (GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult) get_store()
						.find_element_user(GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "Garden_Subscribers_Deliveries_CollectionsResult" element
		 */
		@Override
		public boolean isSetGardenSubscribersDeliveriesCollectionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "Garden_Subscribers_Deliveries_CollectionsResult" element
		 */
		@Override
		public void setGardenSubscribersDeliveriesCollectionsResult(
				GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult gardenSubscribersDeliveriesCollectionsResult) {
			generatedSetterHelperImpl(gardenSubscribersDeliveriesCollectionsResult, GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "Garden_Subscribers_Deliveries_CollectionsResult" element
		 */
		@Override
		public void unsetGardenSubscribersDeliveriesCollectionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"Garden_Subscribers_Deliveries_CollectionsResponse");

	private static final long serialVersionUID = 1L;

	public GardenSubscribersDeliveriesCollectionsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "Garden_Subscribers_Deliveries_CollectionsResponse" element
	 */
	@Override
	public GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse addNewGardenSubscribersDeliveriesCollectionsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse target = null;
			target = (GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse) get_store()
					.add_element_user(GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "Garden_Subscribers_Deliveries_CollectionsResponse" element
	 */
	@Override
	public GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse getGardenSubscribersDeliveriesCollectionsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse target = null;
			target = (GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse) get_store()
					.find_element_user(GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "Garden_Subscribers_Deliveries_CollectionsResponse" element
	 */
	@Override
	public void setGardenSubscribersDeliveriesCollectionsResponse(
			GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse gardenSubscribersDeliveriesCollectionsResponse) {
		generatedSetterHelperImpl(gardenSubscribersDeliveriesCollectionsResponse, GARDENSUBSCRIBERSDELIVERIESCOLLECTIONSRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
