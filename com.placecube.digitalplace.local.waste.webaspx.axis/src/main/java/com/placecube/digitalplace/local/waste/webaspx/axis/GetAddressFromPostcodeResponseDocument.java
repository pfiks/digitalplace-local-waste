/*
 * An XML document type.
 * Localname: getAddressFromPostcodeResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAddressFromPostcodeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getAddressFromPostcodeResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetAddressFromPostcodeResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAddressFromPostcodeResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getaddressfrompostcoderesponse67f2doctype");
    
    /**
     * Gets the "getAddressFromPostcodeResponse" element
     */
    GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse getGetAddressFromPostcodeResponse();
    
    /**
     * Sets the "getAddressFromPostcodeResponse" element
     */
    void setGetAddressFromPostcodeResponse(GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse getAddressFromPostcodeResponse);
    
    /**
     * Appends and returns a new empty "getAddressFromPostcodeResponse" element
     */
    GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse addNewGetAddressFromPostcodeResponse();
    
    /**
     * An XML getAddressFromPostcodeResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetAddressFromPostcodeResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAddressFromPostcodeResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getaddressfrompostcoderesponse2480elemtype");
        
        /**
         * Gets the "getAddressFromPostcodeResult" element
         */
        GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult getGetAddressFromPostcodeResult();
        
        /**
         * True if has "getAddressFromPostcodeResult" element
         */
        boolean isSetGetAddressFromPostcodeResult();
        
        /**
         * Sets the "getAddressFromPostcodeResult" element
         */
        void setGetAddressFromPostcodeResult(GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult getAddressFromPostcodeResult);
        
        /**
         * Appends and returns a new empty "getAddressFromPostcodeResult" element
         */
        GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult addNewGetAddressFromPostcodeResult();
        
        /**
         * Unsets the "getAddressFromPostcodeResult" element
         */
        void unsetGetAddressFromPostcodeResult();
        
        /**
         * An XML getAddressFromPostcodeResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetAddressFromPostcodeResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAddressFromPostcodeResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getaddressfrompostcoderesultb4ceelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult newInstance() {
                  return (GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse newInstance() {
              return (GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetAddressFromPostcodeResponseDocument newInstance() {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetAddressFromPostcodeResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetAddressFromPostcodeResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetAddressFromPostcodeResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetAddressFromPostcodeResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAddressFromPostcodeResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAddressFromPostcodeResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAddressFromPostcodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
