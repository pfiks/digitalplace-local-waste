/*
 * An XML document type.
 * Localname: getAllIssuesForIssueType
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAllIssuesForIssueTypeDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllIssuesForIssueTypeDocument;

/**
 * A document containing one
 * getAllIssuesForIssueType(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAllIssuesForIssueTypeDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAllIssuesForIssueTypeDocument {

	/**
	 * An XML
	 * getAllIssuesForIssueType(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAllIssuesForIssueTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName DIGTIME$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "digTime");

		private static final javax.xml.namespace.QName ENDDATEDDMMYYYY$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "endDateDDMMYYYY");
		private static final javax.xml.namespace.QName ISSUETYPE$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "issueType");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName SERVICE$16 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "service");
		private static final javax.xml.namespace.QName STARTDATEDDMMYYYY$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "startDateDDMMYYYY");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public GetAllIssuesForIssueTypeImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "digTime" element
		 */
		@Override
		public java.lang.String getDigTime() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DIGTIME$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "endDateDDMMYYYY" element
		 */
		@Override
		public java.lang.String getEndDateDDMMYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ENDDATEDDMMYYYY$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "issueType" element
		 */
		@Override
		public java.lang.String getIssueType() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUETYPE$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "service" element
		 */
		@Override
		public java.lang.String getService() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SERVICE$16, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "startDateDDMMYYYY" element
		 */
		@Override
		public java.lang.String getStartDateDDMMYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STARTDATEDDMMYYYY$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "digTime" element
		 */
		@Override
		public boolean isSetDigTime() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DIGTIME$14) != 0;
			}
		}

		/**
		 * True if has "endDateDDMMYYYY" element
		 */
		@Override
		public boolean isSetEndDateDDMMYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ENDDATEDDMMYYYY$12) != 0;
			}
		}

		/**
		 * True if has "issueType" element
		 */
		@Override
		public boolean isSetIssueType() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ISSUETYPE$8) != 0;
			}
		}

		/**
		 * True if has "service" element
		 */
		@Override
		public boolean isSetService() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SERVICE$16) != 0;
			}
		}

		/**
		 * True if has "startDateDDMMYYYY" element
		 */
		@Override
		public boolean isSetStartDateDDMMYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(STARTDATEDDMMYYYY$10) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "digTime" element
		 */
		@Override
		public void setDigTime(java.lang.String digTime) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DIGTIME$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DIGTIME$14);
				}
				target.setStringValue(digTime);
			}
		}

		/**
		 * Sets the "endDateDDMMYYYY" element
		 */
		@Override
		public void setEndDateDDMMYYYY(java.lang.String endDateDDMMYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ENDDATEDDMMYYYY$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ENDDATEDDMMYYYY$12);
				}
				target.setStringValue(endDateDDMMYYYY);
			}
		}

		/**
		 * Sets the "issueType" element
		 */
		@Override
		public void setIssueType(java.lang.String issueType) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUETYPE$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ISSUETYPE$8);
				}
				target.setStringValue(issueType);
			}
		}

		/**
		 * Sets the "service" element
		 */
		@Override
		public void setService(java.lang.String service) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SERVICE$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(SERVICE$16);
				}
				target.setStringValue(service);
			}
		}

		/**
		 * Sets the "startDateDDMMYYYY" element
		 */
		@Override
		public void setStartDateDDMMYYYY(java.lang.String startDateDDMMYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STARTDATEDDMMYYYY$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(STARTDATEDDMMYYYY$10);
				}
				target.setStringValue(startDateDDMMYYYY);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "digTime" element
		 */
		@Override
		public void unsetDigTime() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DIGTIME$14, 0);
			}
		}

		/**
		 * Unsets the "endDateDDMMYYYY" element
		 */
		@Override
		public void unsetEndDateDDMMYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ENDDATEDDMMYYYY$12, 0);
			}
		}

		/**
		 * Unsets the "issueType" element
		 */
		@Override
		public void unsetIssueType() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ISSUETYPE$8, 0);
			}
		}

		/**
		 * Unsets the "service" element
		 */
		@Override
		public void unsetService() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SERVICE$16, 0);
			}
		}

		/**
		 * Unsets the "startDateDDMMYYYY" element
		 */
		@Override
		public void unsetStartDateDDMMYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(STARTDATEDDMMYYYY$10, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "digTime" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDigTime() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DIGTIME$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "endDateDDMMYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetEndDateDDMMYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ENDDATEDDMMYYYY$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "issueType" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetIssueType() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUETYPE$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "service" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetService() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SERVICE$16, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "startDateDDMMYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetStartDateDDMMYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STARTDATEDDMMYYYY$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "digTime" element
		 */
		@Override
		public void xsetDigTime(org.apache.xmlbeans.XmlString digTime) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DIGTIME$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DIGTIME$14);
				}
				target.set(digTime);
			}
		}

		/**
		 * Sets (as xml) the "endDateDDMMYYYY" element
		 */
		@Override
		public void xsetEndDateDDMMYYYY(org.apache.xmlbeans.XmlString endDateDDMMYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ENDDATEDDMMYYYY$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ENDDATEDDMMYYYY$12);
				}
				target.set(endDateDDMMYYYY);
			}
		}

		/**
		 * Sets (as xml) the "issueType" element
		 */
		@Override
		public void xsetIssueType(org.apache.xmlbeans.XmlString issueType) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUETYPE$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ISSUETYPE$8);
				}
				target.set(issueType);
			}
		}

		/**
		 * Sets (as xml) the "service" element
		 */
		@Override
		public void xsetService(org.apache.xmlbeans.XmlString service) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SERVICE$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(SERVICE$16);
				}
				target.set(service);
			}
		}

		/**
		 * Sets (as xml) the "startDateDDMMYYYY" element
		 */
		@Override
		public void xsetStartDateDDMMYYYY(org.apache.xmlbeans.XmlString startDateDDMMYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STARTDATEDDMMYYYY$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(STARTDATEDDMMYYYY$10);
				}
				target.set(startDateDDMMYYYY);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName GETALLISSUESFORISSUETYPE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllIssuesForIssueType");

	private static final long serialVersionUID = 1L;

	public GetAllIssuesForIssueTypeDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getAllIssuesForIssueType" element
	 */
	@Override
	public GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType addNewGetAllIssuesForIssueType() {
		synchronized (monitor()) {
			check_orphaned();
			GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType target = null;
			target = (GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType) get_store().add_element_user(GETALLISSUESFORISSUETYPE$0);
			return target;
		}
	}

	/**
	 * Gets the "getAllIssuesForIssueType" element
	 */
	@Override
	public GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType getGetAllIssuesForIssueType() {
		synchronized (monitor()) {
			check_orphaned();
			GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType target = null;
			target = (GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType) get_store().find_element_user(GETALLISSUESFORISSUETYPE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getAllIssuesForIssueType" element
	 */
	@Override
	public void setGetAllIssuesForIssueType(GetAllIssuesForIssueTypeDocument.GetAllIssuesForIssueType getAllIssuesForIssueType) {
		generatedSetterHelperImpl(getAllIssuesForIssueType, GETALLISSUESFORISSUETYPE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
