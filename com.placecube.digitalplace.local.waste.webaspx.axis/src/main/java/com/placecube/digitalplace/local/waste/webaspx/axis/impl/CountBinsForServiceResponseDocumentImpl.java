/*
 * An XML document type.
 * Localname: countBinsForServiceResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CountBinsForServiceResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.CountBinsForServiceResponseDocument;

/**
 * A document containing one
 * countBinsForServiceResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class CountBinsForServiceResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CountBinsForServiceResponseDocument {

	/**
	 * An XML
	 * countBinsForServiceResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class CountBinsForServiceResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CountBinsForServiceResponseDocument.CountBinsForServiceResponse {

		/**
		 * An XML
		 * countBinsForServiceResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class CountBinsForServiceResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements CountBinsForServiceResponseDocument.CountBinsForServiceResponse.CountBinsForServiceResult {

			private static final long serialVersionUID = 1L;

			public CountBinsForServiceResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName COUNTBINSFORSERVICERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countBinsForServiceResult");

		private static final long serialVersionUID = 1L;

		public CountBinsForServiceResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "countBinsForServiceResult" element
		 */
		@Override
		public CountBinsForServiceResponseDocument.CountBinsForServiceResponse.CountBinsForServiceResult addNewCountBinsForServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				CountBinsForServiceResponseDocument.CountBinsForServiceResponse.CountBinsForServiceResult target = null;
				target = (CountBinsForServiceResponseDocument.CountBinsForServiceResponse.CountBinsForServiceResult) get_store().add_element_user(COUNTBINSFORSERVICERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "countBinsForServiceResult" element
		 */
		@Override
		public CountBinsForServiceResponseDocument.CountBinsForServiceResponse.CountBinsForServiceResult getCountBinsForServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				CountBinsForServiceResponseDocument.CountBinsForServiceResponse.CountBinsForServiceResult target = null;
				target = (CountBinsForServiceResponseDocument.CountBinsForServiceResponse.CountBinsForServiceResult) get_store().find_element_user(COUNTBINSFORSERVICERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "countBinsForServiceResult" element
		 */
		@Override
		public boolean isSetCountBinsForServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNTBINSFORSERVICERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "countBinsForServiceResult" element
		 */
		@Override
		public void setCountBinsForServiceResult(CountBinsForServiceResponseDocument.CountBinsForServiceResponse.CountBinsForServiceResult countBinsForServiceResult) {
			generatedSetterHelperImpl(countBinsForServiceResult, COUNTBINSFORSERVICERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "countBinsForServiceResult" element
		 */
		@Override
		public void unsetCountBinsForServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNTBINSFORSERVICERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName COUNTBINSFORSERVICERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countBinsForServiceResponse");

	private static final long serialVersionUID = 1L;

	public CountBinsForServiceResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "countBinsForServiceResponse" element
	 */
	@Override
	public CountBinsForServiceResponseDocument.CountBinsForServiceResponse addNewCountBinsForServiceResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CountBinsForServiceResponseDocument.CountBinsForServiceResponse target = null;
			target = (CountBinsForServiceResponseDocument.CountBinsForServiceResponse) get_store().add_element_user(COUNTBINSFORSERVICERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "countBinsForServiceResponse" element
	 */
	@Override
	public CountBinsForServiceResponseDocument.CountBinsForServiceResponse getCountBinsForServiceResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CountBinsForServiceResponseDocument.CountBinsForServiceResponse target = null;
			target = (CountBinsForServiceResponseDocument.CountBinsForServiceResponse) get_store().find_element_user(COUNTBINSFORSERVICERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "countBinsForServiceResponse" element
	 */
	@Override
	public void setCountBinsForServiceResponse(CountBinsForServiceResponseDocument.CountBinsForServiceResponse countBinsForServiceResponse) {
		generatedSetterHelperImpl(countBinsForServiceResponse, COUNTBINSFORSERVICERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
