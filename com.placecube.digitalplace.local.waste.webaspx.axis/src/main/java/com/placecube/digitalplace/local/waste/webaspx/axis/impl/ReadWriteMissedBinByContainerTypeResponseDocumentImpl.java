/*
 * An XML document type.
 * Localname: readWriteMissedBinByContainerTypeResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: ReadWriteMissedBinByContainerTypeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinByContainerTypeResponseDocument;

/**
 * A document containing one
 * readWriteMissedBinByContainerTypeResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class ReadWriteMissedBinByContainerTypeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ReadWriteMissedBinByContainerTypeResponseDocument {

	/**
	 * An XML
	 * readWriteMissedBinByContainerTypeResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class ReadWriteMissedBinByContainerTypeResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements ReadWriteMissedBinByContainerTypeResponseDocument.ReadWriteMissedBinByContainerTypeResponse {

		private static final javax.xml.namespace.QName READWRITEMISSEDBINBYCONTAINERTYPERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"readWriteMissedBinByContainerTypeResult");

		private static final long serialVersionUID = 1L;

		public ReadWriteMissedBinByContainerTypeResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "readWriteMissedBinByContainerTypeResult" element
		 */
		@Override
		public java.lang.String getReadWriteMissedBinByContainerTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(READWRITEMISSEDBINBYCONTAINERTYPERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "readWriteMissedBinByContainerTypeResult" element
		 */
		@Override
		public boolean isSetReadWriteMissedBinByContainerTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(READWRITEMISSEDBINBYCONTAINERTYPERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "readWriteMissedBinByContainerTypeResult" element
		 */
		@Override
		public void setReadWriteMissedBinByContainerTypeResult(java.lang.String readWriteMissedBinByContainerTypeResult) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(READWRITEMISSEDBINBYCONTAINERTYPERESULT$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(READWRITEMISSEDBINBYCONTAINERTYPERESULT$0);
				}
				target.setStringValue(readWriteMissedBinByContainerTypeResult);
			}
		}

		/**
		 * Unsets the "readWriteMissedBinByContainerTypeResult" element
		 */
		@Override
		public void unsetReadWriteMissedBinByContainerTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(READWRITEMISSEDBINBYCONTAINERTYPERESULT$0, 0);
			}
		}

		/**
		 * Gets (as xml) the "readWriteMissedBinByContainerTypeResult" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetReadWriteMissedBinByContainerTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(READWRITEMISSEDBINBYCONTAINERTYPERESULT$0, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "readWriteMissedBinByContainerTypeResult" element
		 */
		@Override
		public void xsetReadWriteMissedBinByContainerTypeResult(org.apache.xmlbeans.XmlString readWriteMissedBinByContainerTypeResult) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(READWRITEMISSEDBINBYCONTAINERTYPERESULT$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(READWRITEMISSEDBINBYCONTAINERTYPERESULT$0);
				}
				target.set(readWriteMissedBinByContainerTypeResult);
			}
		}
	}

	private static final javax.xml.namespace.QName READWRITEMISSEDBINBYCONTAINERTYPERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"readWriteMissedBinByContainerTypeResponse");

	private static final long serialVersionUID = 1L;

	public ReadWriteMissedBinByContainerTypeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "readWriteMissedBinByContainerTypeResponse" element
	 */
	@Override
	public ReadWriteMissedBinByContainerTypeResponseDocument.ReadWriteMissedBinByContainerTypeResponse addNewReadWriteMissedBinByContainerTypeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			ReadWriteMissedBinByContainerTypeResponseDocument.ReadWriteMissedBinByContainerTypeResponse target = null;
			target = (ReadWriteMissedBinByContainerTypeResponseDocument.ReadWriteMissedBinByContainerTypeResponse) get_store().add_element_user(READWRITEMISSEDBINBYCONTAINERTYPERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "readWriteMissedBinByContainerTypeResponse" element
	 */
	@Override
	public ReadWriteMissedBinByContainerTypeResponseDocument.ReadWriteMissedBinByContainerTypeResponse getReadWriteMissedBinByContainerTypeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			ReadWriteMissedBinByContainerTypeResponseDocument.ReadWriteMissedBinByContainerTypeResponse target = null;
			target = (ReadWriteMissedBinByContainerTypeResponseDocument.ReadWriteMissedBinByContainerTypeResponse) get_store().find_element_user(READWRITEMISSEDBINBYCONTAINERTYPERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "readWriteMissedBinByContainerTypeResponse" element
	 */
	@Override
	public void setReadWriteMissedBinByContainerTypeResponse(ReadWriteMissedBinByContainerTypeResponseDocument.ReadWriteMissedBinByContainerTypeResponse readWriteMissedBinByContainerTypeResponse) {
		generatedSetterHelperImpl(readWriteMissedBinByContainerTypeResponse, READWRITEMISSEDBINBYCONTAINERTYPERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
