/*
 * An XML document type.
 * Localname: AHP_RemoveSubscriptionResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPRemoveSubscriptionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AHPRemoveSubscriptionResponseDocument;

/**
 * A document containing one
 * AHP_RemoveSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AHPRemoveSubscriptionResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPRemoveSubscriptionResponseDocument {

	/**
	 * An XML
	 * AHP_RemoveSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AHPRemoveSubscriptionResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse {

		/**
		 * An XML
		 * AHP_RemoveSubscriptionResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class AHPRemoveSubscriptionResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse.AHPRemoveSubscriptionResult {

			private static final long serialVersionUID = 1L;

			public AHPRemoveSubscriptionResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName AHPREMOVESUBSCRIPTIONRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_RemoveSubscriptionResult");

		private static final long serialVersionUID = 1L;

		public AHPRemoveSubscriptionResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "AHP_RemoveSubscriptionResult"
		 * element
		 */
		@Override
		public AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse.AHPRemoveSubscriptionResult addNewAHPRemoveSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse.AHPRemoveSubscriptionResult target = null;
				target = (AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse.AHPRemoveSubscriptionResult) get_store().add_element_user(AHPREMOVESUBSCRIPTIONRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "AHP_RemoveSubscriptionResult" element
		 */
		@Override
		public AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse.AHPRemoveSubscriptionResult getAHPRemoveSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse.AHPRemoveSubscriptionResult target = null;
				target = (AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse.AHPRemoveSubscriptionResult) get_store().find_element_user(AHPREMOVESUBSCRIPTIONRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "AHP_RemoveSubscriptionResult" element
		 */
		@Override
		public boolean isSetAHPRemoveSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPREMOVESUBSCRIPTIONRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AHP_RemoveSubscriptionResult" element
		 */
		@Override
		public void setAHPRemoveSubscriptionResult(AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse.AHPRemoveSubscriptionResult ahpRemoveSubscriptionResult) {
			generatedSetterHelperImpl(ahpRemoveSubscriptionResult, AHPREMOVESUBSCRIPTIONRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "AHP_RemoveSubscriptionResult" element
		 */
		@Override
		public void unsetAHPRemoveSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPREMOVESUBSCRIPTIONRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName AHPREMOVESUBSCRIPTIONRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_RemoveSubscriptionResponse");

	private static final long serialVersionUID = 1L;

	public AHPRemoveSubscriptionResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AHP_RemoveSubscriptionResponse" element
	 */
	@Override
	public AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse addNewAHPRemoveSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse target = null;
			target = (AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse) get_store().add_element_user(AHPREMOVESUBSCRIPTIONRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AHP_RemoveSubscriptionResponse" element
	 */
	@Override
	public AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse getAHPRemoveSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse target = null;
			target = (AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse) get_store().find_element_user(AHPREMOVESUBSCRIPTIONRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AHP_RemoveSubscriptionResponse" element
	 */
	@Override
	public void setAHPRemoveSubscriptionResponse(AHPRemoveSubscriptionResponseDocument.AHPRemoveSubscriptionResponse ahpRemoveSubscriptionResponse) {
		generatedSetterHelperImpl(ahpRemoveSubscriptionResponse, AHPREMOVESUBSCRIPTIONRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
