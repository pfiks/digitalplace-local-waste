/*
 * An XML document type.
 * Localname: getRoundForUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetRoundForUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundForUPRNResponseDocument;

/**
 * A document containing one
 * getRoundForUPRNResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetRoundForUPRNResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetRoundForUPRNResponseDocument {

	/**
	 * An XML
	 * getRoundForUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetRoundForUPRNResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse {

		/**
		 * An XML
		 * getRoundForUPRNResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetRoundForUPRNResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse.GetRoundForUPRNResult {

			private static final long serialVersionUID = 1L;

			public GetRoundForUPRNResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETROUNDFORUPRNRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundForUPRNResult");

		private static final long serialVersionUID = 1L;

		public GetRoundForUPRNResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "getRoundForUPRNResult" element
		 */
		@Override
		public GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse.GetRoundForUPRNResult addNewGetRoundForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse.GetRoundForUPRNResult target = null;
				target = (GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse.GetRoundForUPRNResult) get_store().add_element_user(GETROUNDFORUPRNRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getRoundForUPRNResult" element
		 */
		@Override
		public GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse.GetRoundForUPRNResult getGetRoundForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse.GetRoundForUPRNResult target = null;
				target = (GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse.GetRoundForUPRNResult) get_store().find_element_user(GETROUNDFORUPRNRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getRoundForUPRNResult" element
		 */
		@Override
		public boolean isSetGetRoundForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETROUNDFORUPRNRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getRoundForUPRNResult" element
		 */
		@Override
		public void setGetRoundForUPRNResult(GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse.GetRoundForUPRNResult getRoundForUPRNResult) {
			generatedSetterHelperImpl(getRoundForUPRNResult, GETROUNDFORUPRNRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getRoundForUPRNResult" element
		 */
		@Override
		public void unsetGetRoundForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETROUNDFORUPRNRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETROUNDFORUPRNRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundForUPRNResponse");

	private static final long serialVersionUID = 1L;

	public GetRoundForUPRNResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getRoundForUPRNResponse" element
	 */
	@Override
	public GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse addNewGetRoundForUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse target = null;
			target = (GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse) get_store().add_element_user(GETROUNDFORUPRNRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getRoundForUPRNResponse" element
	 */
	@Override
	public GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse getGetRoundForUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse target = null;
			target = (GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse) get_store().find_element_user(GETROUNDFORUPRNRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getRoundForUPRNResponse" element
	 */
	@Override
	public void setGetRoundForUPRNResponse(GetRoundForUPRNResponseDocument.GetRoundForUPRNResponse getRoundForUPRNResponse) {
		generatedSetterHelperImpl(getRoundForUPRNResponse, GETROUNDFORUPRNRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
