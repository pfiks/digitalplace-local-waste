/*
 * An XML document type.
 * Localname: GetAllRoundDetails
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAllRoundDetailsDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetAllRoundDetails(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetAllRoundDetailsDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllRoundDetailsDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getallrounddetails7a1bdoctype");
    
    /**
     * Gets the "GetAllRoundDetails" element
     */
    GetAllRoundDetailsDocument.GetAllRoundDetails getGetAllRoundDetails();
    
    /**
     * Sets the "GetAllRoundDetails" element
     */
    void setGetAllRoundDetails(GetAllRoundDetailsDocument.GetAllRoundDetails getAllRoundDetails);
    
    /**
     * Appends and returns a new empty "GetAllRoundDetails" element
     */
    GetAllRoundDetailsDocument.GetAllRoundDetails addNewGetAllRoundDetails();
    
    /**
     * An XML GetAllRoundDetails(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetAllRoundDetails extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllRoundDetails.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getallrounddetails06a0elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetAllRoundDetailsDocument.GetAllRoundDetails newInstance() {
              return (GetAllRoundDetailsDocument.GetAllRoundDetails) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetAllRoundDetailsDocument.GetAllRoundDetails newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetAllRoundDetailsDocument.GetAllRoundDetails) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetAllRoundDetailsDocument newInstance() {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetAllRoundDetailsDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetAllRoundDetailsDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetAllRoundDetailsDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetAllRoundDetailsDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetAllRoundDetailsDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetAllRoundDetailsDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetAllRoundDetailsDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetAllRoundDetailsDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetAllRoundDetailsDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetAllRoundDetailsDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetAllRoundDetailsDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetAllRoundDetailsDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetAllRoundDetailsDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetAllRoundDetailsDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetAllRoundDetailsDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAllRoundDetailsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAllRoundDetailsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAllRoundDetailsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
