/**
 * WSCollExternalCallbackHandler.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.0 Built on : Aug 01,
 * 2021 (07:27:19 HST)
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;

/**
 * WSCollExternalCallbackHandler Callback class, Users can extend this class and
 * implement their own receiveResult and receiveError methods.
 */
public abstract class WSCollExternalCallbackHandler {

	protected Object clientData;

	/** Please use this constructor if you don't want to set any clientData */
	public WSCollExternalCallbackHandler() {
		this.clientData = null;
	}

	/**
	 * User can pass in any object that needs to be accessed once the
	 * NonBlocking Web service call is finished and appropriate method of this
	 * CallBack is called.
	 *
	 * @param clientData Object mechanism by which the user can pass in user
	 *            data that will be avilable at the time this callback is
	 *            called.
	 */
	public WSCollExternalCallbackHandler(Object clientData) {
		this.clientData = clientData;
	}

	/** Get the client data */
	public Object getClientData() {
		return clientData;
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aA_HelloWorld_String_Test operation
	 */
	public void receiveErroraA_HelloWorld_String_Test(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aA_HelloWorld_XML_Test operation
	 */
	public void receiveErroraA_HelloWorld_XML_Test(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aHP_ChangeAddress operation
	 */
	public void receiveErroraHP_ChangeAddress(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aHP_ExtendSubscription operation
	 */
	public void receiveErroraHP_ExtendSubscription(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aHP_GetDetails operation
	 */
	public void receiveErroraHP_GetDetails(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aHP_NewUpdate operation
	 */
	public void receiveErroraHP_NewUpdate(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aHP_OrderNewBags operation
	 */
	public void receiveErroraHP_OrderNewBags(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aHP_RemoveSubscription operation
	 */
	public void receiveErroraHP_RemoveSubscription(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from aHP_SuspendSubscription operation
	 */
	public void receiveErroraHP_SuspendSubscription(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from binDelete operation
	 */
	public void receiveErrorbinDelete(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from binInsert operation
	 */
	public void receiveErrorbinInsert(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from binUpdate operation
	 */
	public void receiveErrorbinUpdate(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from cachedCalendar operation
	 */
	public void receiveErrorcachedCalendar(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from checkAssisted operation
	 */
	public void receiveErrorcheckAssisted(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from commentsForIncabGetExisting operation
	 */
	public void receiveErrorcommentsForIncabGetExisting(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from commentsForIncabUpdate operation
	 */
	public void receiveErrorcommentsForIncabUpdate(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from countBinsForService operation
	 */
	public void receiveErrorcountBinsForService(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from countSubscriptions operation
	 */
	public void receiveErrorcountSubscriptions(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from deleteBin operation
	 */
	public void receiveErrordeleteBin(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from garden_ChangeAddress operation
	 */
	public void receiveErrorgarden_ChangeAddress(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from garden_RemoveSubscription operation
	 */
	public void receiveErrorgarden_RemoveSubscription(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from garden_Subscribers_Deliveries_Collections operation
	 */
	public void receiveErrorgarden_Subscribers_Deliveries_Collections(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from gardenSubscription operation
	 */
	public void receiveErrorgardenSubscription(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from gardenSubscription_GetDetails operation
	 */
	public void receiveErrorgardenSubscription_GetDetails(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from gardenSubscription_WithNamePhoneEmail operation
	 */
	public void receiveErrorgardenSubscription_WithNamePhoneEmail(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getAddressFromPostcode operation
	 */
	public void receiveErrorgetAddressFromPostcode(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getAddressOrUPRN operation
	 */
	public void receiveErrorgetAddressOrUPRN(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getAllIssuesForIssueType operation
	 */
	public void receiveErrorgetAllIssuesForIssueType(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getAllRoundDetails operation
	 */
	public void receiveErrorgetAllRoundDetails(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getAllUPRNsForDate operation
	 */
	public void receiveErrorgetAllUPRNsForDate(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getAvailableContainers operation
	 */
	public void receiveErrorgetAvailableContainers(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getAvailableContainersForClientOrProperty operation
	 */
	public void receiveErrorgetAvailableContainersForClientOrProperty(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getAvailableRounds operation
	 */
	public void receiveErrorgetAvailableRounds(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getBinDetailsForService operation
	 */
	public void receiveErrorgetBinDetailsForService(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getInCabIssueTextAndIssueCode operation
	 */
	public void receiveErrorgetInCabIssueTextAndIssueCode(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getIssues operation
	 */
	public void receiveErrorgetIssues(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getIssuesAndCollectionStatusForUPRN operation
	 */
	public void receiveErrorgetIssuesAndCollectionStatusForUPRN(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getIssuesForUPRN operation
	 */
	public void receiveErrorgetIssuesForUPRN(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getLastTruckPosition operation
	 */
	public void receiveErrorgetLastTruckPosition(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getLatestTruckPositions operation
	 */
	public void receiveErrorgetLatestTruckPositions(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getNoCollectionDates operation
	 */
	public void receiveErrorgetNoCollectionDates(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getPotentialCollectionDatesForNewGardenSubscriber
	 * operation
	 */
	public void receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getRoundAndBinInfoForUPRNForNewAdjustedDates
	 * operation
	 */
	public void receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getRoundCalendarForUPRN operation
	 */
	public void receiveErrorgetRoundCalendarForUPRN(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getRoundChangesInDateRangeByRoundOrUPRN operation
	 */
	public void receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getRoundForUPRN operation
	 */
	public void receiveErrorgetRoundForUPRN(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getRoundNameForUPRNService operation
	 */
	public void receiveErrorgetRoundNameForUPRNService(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getTrucksOutToday operation
	 */
	public void receiveErrorgetTrucksOutToday(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from getZoneBagAssist operation
	 */
	public void receiveErrorgetZoneBagAssist(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from insertNewBin operation
	 */
	public void receiveErrorinsertNewBin(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from keepAliveCall operation
	 */
	public void receiveErrorkeepAliveCall(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from largeHouseholds operation
	 */
	public void receiveErrorlargeHouseholds(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from lastCollected operation
	 */
	public void receiveErrorlastCollected(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from lLPGXtraGetUPRNCurrentValuesForDBField operation
	 */
	public void receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from lLPGXtraUpdateGetAvailableFieldDetails operation
	 */
	public void receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from lLPGXtraUpdateGetUPRNForDBField operation
	 */
	public void receiveErrorlLPGXtraUpdateGetUPRNForDBField(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from queryBinEndDates operation
	 */
	public void receiveErrorqueryBinEndDates(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from queryBinOnType operation
	 */
	public void receiveErrorqueryBinOnType(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from readWriteMissedBin operation
	 */
	public void receiveErrorreadWriteMissedBin(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from readWriteMissedBinByContainerType operation
	 */
	public void receiveErrorreadWriteMissedBinByContainerType(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from showAdditionalBins operation
	 */
	public void receiveErrorshowAdditionalBins(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from updateAssisted operation
	 */
	public void receiveErrorupdateAssisted(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from updateBinDetails operation
	 */
	public void receiveErrorupdateBinDetails(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 Error handler override this method for handling
	 * error response from writeIncabLiveData operation
	 */
	public void receiveErrorwriteIncabLiveData(java.lang.Exception e) {
	}

	/**
	 * auto generated Axis2 call back method for aA_HelloWorld_String_Test
	 * method override this method for handling normal response from
	 * aA_HelloWorld_String_Test operation
	 */
	public void receiveResultaA_HelloWorld_String_Test(AAHelloWorldStringTestResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for aA_HelloWorld_XML_Test method
	 * override this method for handling normal response from
	 * aA_HelloWorld_XML_Test operation
	 */
	public void receiveResultaA_HelloWorld_XML_Test(AAHelloWorldXMLTestResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for aHP_ChangeAddress method
	 * override this method for handling normal response from aHP_ChangeAddress
	 * operation
	 */
	public void receiveResultaHP_ChangeAddress(AHPChangeAddressResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for aHP_ExtendSubscription method
	 * override this method for handling normal response from
	 * aHP_ExtendSubscription operation
	 */
	public void receiveResultaHP_ExtendSubscription(AHPExtendSubscriptionResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for aHP_GetDetails method override
	 * this method for handling normal response from aHP_GetDetails operation
	 */
	public void receiveResultaHP_GetDetails(AHPGetDetailsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for aHP_NewUpdate method override
	 * this method for handling normal response from aHP_NewUpdate operation
	 */
	public void receiveResultaHP_NewUpdate(AHPNewUpdateResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for aHP_OrderNewBags method
	 * override this method for handling normal response from aHP_OrderNewBags
	 * operation
	 */
	public void receiveResultaHP_OrderNewBags(AHPOrderNewBagsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for aHP_RemoveSubscription method
	 * override this method for handling normal response from
	 * aHP_RemoveSubscription operation
	 */
	public void receiveResultaHP_RemoveSubscription(AHPRemoveSubscriptionResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for aHP_SuspendSubscription method
	 * override this method for handling normal response from
	 * aHP_SuspendSubscription operation
	 */
	public void receiveResultaHP_SuspendSubscription(AHPSuspendSubscriptionResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for binDelete method override this
	 * method for handling normal response from binDelete operation
	 */
	public void receiveResultbinDelete(BinDeleteResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for binInsert method override this
	 * method for handling normal response from binInsert operation
	 */
	public void receiveResultbinInsert(BinInsertResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for binUpdate method override this
	 * method for handling normal response from binUpdate operation
	 */
	public void receiveResultbinUpdate(BinUpdateResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for cachedCalendar method override
	 * this method for handling normal response from cachedCalendar operation
	 */
	public void receiveResultcachedCalendar(CachedCalendarResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for checkAssisted method override
	 * this method for handling normal response from checkAssisted operation
	 */
	public void receiveResultcheckAssisted(CheckAssistedResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for commentsForIncabGetExisting
	 * method override this method for handling normal response from
	 * commentsForIncabGetExisting operation
	 */
	public void receiveResultcommentsForIncabGetExisting(CommentsForIncabGetExistingResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for commentsForIncabUpdate method
	 * override this method for handling normal response from
	 * commentsForIncabUpdate operation
	 */
	public void receiveResultcommentsForIncabUpdate(CommentsForIncabUpdateResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for countBinsForService method
	 * override this method for handling normal response from
	 * countBinsForService operation
	 */
	public void receiveResultcountBinsForService(CountBinsForServiceResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for countSubscriptions method
	 * override this method for handling normal response from countSubscriptions
	 * operation
	 */
	public void receiveResultcountSubscriptions(CountSubscriptionsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for deleteBin method override this
	 * method for handling normal response from deleteBin operation
	 */
	public void receiveResultdeleteBin(DeleteBinResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for garden_ChangeAddress method
	 * override this method for handling normal response from
	 * garden_ChangeAddress operation
	 */
	public void receiveResultgarden_ChangeAddress(GardenChangeAddressResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for garden_RemoveSubscription
	 * method override this method for handling normal response from
	 * garden_RemoveSubscription operation
	 */
	public void receiveResultgarden_RemoveSubscription(GardenRemoveSubscriptionResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * garden_Subscribers_Deliveries_Collections method override this method for
	 * handling normal response from garden_Subscribers_Deliveries_Collections
	 * operation
	 */
	public void receiveResultgarden_Subscribers_Deliveries_Collections(GardenSubscribersDeliveriesCollectionsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for gardenSubscription method
	 * override this method for handling normal response from gardenSubscription
	 * operation
	 */
	public void receiveResultgardenSubscription(GardenSubscriptionResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for gardenSubscription_GetDetails
	 * method override this method for handling normal response from
	 * gardenSubscription_GetDetails operation
	 */
	public void receiveResultgardenSubscription_GetDetails(GardenSubscriptionGetDetailsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * gardenSubscription_WithNamePhoneEmail method override this method for
	 * handling normal response from gardenSubscription_WithNamePhoneEmail
	 * operation
	 */
	public void receiveResultgardenSubscription_WithNamePhoneEmail(GardenSubscriptionWithNamePhoneEmailResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getAddressFromPostcode method
	 * override this method for handling normal response from
	 * getAddressFromPostcode operation
	 */
	public void receiveResultgetAddressFromPostcode(GetAddressFromPostcodeResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getAddressOrUPRN method
	 * override this method for handling normal response from getAddressOrUPRN
	 * operation
	 */
	public void receiveResultgetAddressOrUPRN(GetAddressOrUPRNResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getAllIssuesForIssueType method
	 * override this method for handling normal response from
	 * getAllIssuesForIssueType operation
	 */
	public void receiveResultgetAllIssuesForIssueType(GetAllIssuesForIssueTypeResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getAllRoundDetails method
	 * override this method for handling normal response from getAllRoundDetails
	 * operation
	 */
	public void receiveResultgetAllRoundDetails(GetAllRoundDetailsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getAllUPRNsForDate method
	 * override this method for handling normal response from getAllUPRNsForDate
	 * operation
	 */
	public void receiveResultgetAllUPRNsForDate(GetAllUPRNsForDateResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getAvailableContainers method
	 * override this method for handling normal response from
	 * getAvailableContainers operation
	 */
	public void receiveResultgetAvailableContainers(GetAvailableContainersResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * getAvailableContainersForClientOrProperty method override this method for
	 * handling normal response from getAvailableContainersForClientOrProperty
	 * operation
	 */
	public void receiveResultgetAvailableContainersForClientOrProperty(GetAvailableContainersForClientOrPropertyResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getAvailableRounds method
	 * override this method for handling normal response from getAvailableRounds
	 * operation
	 */
	public void receiveResultgetAvailableRounds(GetAvailableRoundsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getBinDetailsForService method
	 * override this method for handling normal response from
	 * getBinDetailsForService operation
	 */
	public void receiveResultgetBinDetailsForService(GetBinDetailsForServiceResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getInCabIssueTextAndIssueCode
	 * method override this method for handling normal response from
	 * getInCabIssueTextAndIssueCode operation
	 */
	public void receiveResultgetInCabIssueTextAndIssueCode(GetInCabIssueTextAndIssueCodeResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getIssues method override this
	 * method for handling normal response from getIssues operation
	 */
	public void receiveResultgetIssues(GetIssuesResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * getIssuesAndCollectionStatusForUPRN method override this method for
	 * handling normal response from getIssuesAndCollectionStatusForUPRN
	 * operation
	 */
	public void receiveResultgetIssuesAndCollectionStatusForUPRN(GetIssuesAndCollectionStatusForUPRNResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getIssuesForUPRN method
	 * override this method for handling normal response from getIssuesForUPRN
	 * operation
	 */
	public void receiveResultgetIssuesForUPRN(GetIssuesForUPRNResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getLastTruckPosition method
	 * override this method for handling normal response from
	 * getLastTruckPosition operation
	 */
	public void receiveResultgetLastTruckPosition(GetLastTruckPositionResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getLatestTruckPositions method
	 * override this method for handling normal response from
	 * getLatestTruckPositions operation
	 */
	public void receiveResultgetLatestTruckPositions(GetLatestTruckPositionsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getNoCollectionDates method
	 * override this method for handling normal response from
	 * getNoCollectionDates operation
	 */
	public void receiveResultgetNoCollectionDates(GetNoCollectionDatesResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * getPotentialCollectionDatesForNewGardenSubscriber method override this
	 * method for handling normal response from
	 * getPotentialCollectionDatesForNewGardenSubscriber operation
	 */
	public void receiveResultgetPotentialCollectionDatesForNewGardenSubscriber(GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * getRoundAndBinInfoForUPRNForNewAdjustedDates method override this method
	 * for handling normal response from
	 * getRoundAndBinInfoForUPRNForNewAdjustedDates operation
	 */
	public void receiveResultgetRoundAndBinInfoForUPRNForNewAdjustedDates(GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getRoundCalendarForUPRN method
	 * override this method for handling normal response from
	 * getRoundCalendarForUPRN operation
	 */
	public void receiveResultgetRoundCalendarForUPRN(GetRoundCalendarForUPRNResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * getRoundChangesInDateRangeByRoundOrUPRN method override this method for
	 * handling normal response from getRoundChangesInDateRangeByRoundOrUPRN
	 * operation
	 */
	public void receiveResultgetRoundChangesInDateRangeByRoundOrUPRN(GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getRoundForUPRN method override
	 * this method for handling normal response from getRoundForUPRN operation
	 */
	public void receiveResultgetRoundForUPRN(GetRoundForUPRNResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getRoundNameForUPRNService
	 * method override this method for handling normal response from
	 * getRoundNameForUPRNService operation
	 */
	public void receiveResultgetRoundNameForUPRNService(GetRoundNameForUPRNServiceResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getTrucksOutToday method
	 * override this method for handling normal response from getTrucksOutToday
	 * operation
	 */
	public void receiveResultgetTrucksOutToday(GetTrucksOutTodayResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for getZoneBagAssist method
	 * override this method for handling normal response from getZoneBagAssist
	 * operation
	 */
	public void receiveResultgetZoneBagAssist(GetZoneBagAssistResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for insertNewBin method override
	 * this method for handling normal response from insertNewBin operation
	 */
	public void receiveResultinsertNewBin(InsertNewBinResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for keepAliveCall method override
	 * this method for handling normal response from keepAliveCall operation
	 */
	public void receiveResultkeepAliveCall(KeepAliveCallResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for largeHouseholds method override
	 * this method for handling normal response from largeHouseholds operation
	 */
	public void receiveResultlargeHouseholds(LargeHouseholdsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for lastCollected method override
	 * this method for handling normal response from lastCollected operation
	 */
	public void receiveResultlastCollected(LastCollectedResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * lLPGXtraGetUPRNCurrentValuesForDBField method override this method for
	 * handling normal response from lLPGXtraGetUPRNCurrentValuesForDBField
	 * operation
	 */
	public void receiveResultlLPGXtraGetUPRNCurrentValuesForDBField(LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * lLPGXtraUpdateGetAvailableFieldDetails method override this method for
	 * handling normal response from lLPGXtraUpdateGetAvailableFieldDetails
	 * operation
	 */
	public void receiveResultlLPGXtraUpdateGetAvailableFieldDetails(LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for lLPGXtraUpdateGetUPRNForDBField
	 * method override this method for handling normal response from
	 * lLPGXtraUpdateGetUPRNForDBField operation
	 */
	public void receiveResultlLPGXtraUpdateGetUPRNForDBField(LLPGXtraUpdateGetUPRNForDBFieldResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for queryBinEndDates method
	 * override this method for handling normal response from queryBinEndDates
	 * operation
	 */
	public void receiveResultqueryBinEndDates(QueryBinEndDatesResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for queryBinOnType method override
	 * this method for handling normal response from queryBinOnType operation
	 */
	public void receiveResultqueryBinOnType(QueryBinOnTypeResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for readWriteMissedBin method
	 * override this method for handling normal response from readWriteMissedBin
	 * operation
	 */
	public void receiveResultreadWriteMissedBin(ReadWriteMissedBinResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for
	 * readWriteMissedBinByContainerType method override this method for
	 * handling normal response from readWriteMissedBinByContainerType operation
	 */
	public void receiveResultreadWriteMissedBinByContainerType(ReadWriteMissedBinByContainerTypeResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for showAdditionalBins method
	 * override this method for handling normal response from showAdditionalBins
	 * operation
	 */
	public void receiveResultshowAdditionalBins(ShowAdditionalBinsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for updateAssisted method override
	 * this method for handling normal response from updateAssisted operation
	 */
	public void receiveResultupdateAssisted(UpdateAssistedResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for updateBinDetails method
	 * override this method for handling normal response from updateBinDetails
	 * operation
	 */
	public void receiveResultupdateBinDetails(UpdateBinDetailsResponseDocument result) {
	}

	/**
	 * auto generated Axis2 call back method for writeIncabLiveData method
	 * override this method for handling normal response from writeIncabLiveData
	 * operation
	 */
	public void receiveResultwriteIncabLiveData(WriteIncabLiveDataResponseDocument result) {
	}
}
