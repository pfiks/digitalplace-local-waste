/*
 * An XML document type.
 * Localname: UpdateAssisted
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: UpdateAssistedDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one UpdateAssisted(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface UpdateAssistedDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(UpdateAssistedDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("updateassistedd8eddoctype");
    
    /**
     * Gets the "UpdateAssisted" element
     */
    UpdateAssistedDocument.UpdateAssisted getUpdateAssisted();
    
    /**
     * Sets the "UpdateAssisted" element
     */
    void setUpdateAssisted(UpdateAssistedDocument.UpdateAssisted updateAssisted);
    
    /**
     * Appends and returns a new empty "UpdateAssisted" element
     */
    UpdateAssistedDocument.UpdateAssisted addNewUpdateAssisted();
    
    /**
     * An XML UpdateAssisted(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface UpdateAssisted extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(UpdateAssisted.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("updateassisted57e0elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "UPRN" element
         */
        java.lang.String getUPRN();
        
        /**
         * Gets (as xml) the "UPRN" element
         */
        org.apache.xmlbeans.XmlString xgetUPRN();
        
        /**
         * True if has "UPRN" element
         */
        boolean isSetUPRN();
        
        /**
         * Sets the "UPRN" element
         */
        void setUPRN(java.lang.String uprn);
        
        /**
         * Sets (as xml) the "UPRN" element
         */
        void xsetUPRN(org.apache.xmlbeans.XmlString uprn);
        
        /**
         * Unsets the "UPRN" element
         */
        void unsetUPRN();
        
        /**
         * Gets the "AssistedYN01234" element
         */
        java.lang.String getAssistedYN01234();
        
        /**
         * Gets (as xml) the "AssistedYN01234" element
         */
        org.apache.xmlbeans.XmlString xgetAssistedYN01234();
        
        /**
         * True if has "AssistedYN01234" element
         */
        boolean isSetAssistedYN01234();
        
        /**
         * Sets the "AssistedYN01234" element
         */
        void setAssistedYN01234(java.lang.String assistedYN01234);
        
        /**
         * Sets (as xml) the "AssistedYN01234" element
         */
        void xsetAssistedYN01234(org.apache.xmlbeans.XmlString assistedYN01234);
        
        /**
         * Unsets the "AssistedYN01234" element
         */
        void unsetAssistedYN01234();
        
        /**
         * Gets the "AssistedStartDateDDsMMsYYYY" element
         */
        java.lang.String getAssistedStartDateDDsMMsYYYY();
        
        /**
         * Gets (as xml) the "AssistedStartDateDDsMMsYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetAssistedStartDateDDsMMsYYYY();
        
        /**
         * True if has "AssistedStartDateDDsMMsYYYY" element
         */
        boolean isSetAssistedStartDateDDsMMsYYYY();
        
        /**
         * Sets the "AssistedStartDateDDsMMsYYYY" element
         */
        void setAssistedStartDateDDsMMsYYYY(java.lang.String assistedStartDateDDsMMsYYYY);
        
        /**
         * Sets (as xml) the "AssistedStartDateDDsMMsYYYY" element
         */
        void xsetAssistedStartDateDDsMMsYYYY(org.apache.xmlbeans.XmlString assistedStartDateDDsMMsYYYY);
        
        /**
         * Unsets the "AssistedStartDateDDsMMsYYYY" element
         */
        void unsetAssistedStartDateDDsMMsYYYY();
        
        /**
         * Gets the "AssistedEndDateDDsMMsYYYY" element
         */
        java.lang.String getAssistedEndDateDDsMMsYYYY();
        
        /**
         * Gets (as xml) the "AssistedEndDateDDsMMsYYYY" element
         */
        org.apache.xmlbeans.XmlString xgetAssistedEndDateDDsMMsYYYY();
        
        /**
         * True if has "AssistedEndDateDDsMMsYYYY" element
         */
        boolean isSetAssistedEndDateDDsMMsYYYY();
        
        /**
         * Sets the "AssistedEndDateDDsMMsYYYY" element
         */
        void setAssistedEndDateDDsMMsYYYY(java.lang.String assistedEndDateDDsMMsYYYY);
        
        /**
         * Sets (as xml) the "AssistedEndDateDDsMMsYYYY" element
         */
        void xsetAssistedEndDateDDsMMsYYYY(org.apache.xmlbeans.XmlString assistedEndDateDDsMMsYYYY);
        
        /**
         * Unsets the "AssistedEndDateDDsMMsYYYY" element
         */
        void unsetAssistedEndDateDDsMMsYYYY();
        
        /**
         * Gets the "AssistedLocation" element
         */
        java.lang.String getAssistedLocation();
        
        /**
         * Gets (as xml) the "AssistedLocation" element
         */
        org.apache.xmlbeans.XmlString xgetAssistedLocation();
        
        /**
         * True if has "AssistedLocation" element
         */
        boolean isSetAssistedLocation();
        
        /**
         * Sets the "AssistedLocation" element
         */
        void setAssistedLocation(java.lang.String assistedLocation);
        
        /**
         * Sets (as xml) the "AssistedLocation" element
         */
        void xsetAssistedLocation(org.apache.xmlbeans.XmlString assistedLocation);
        
        /**
         * Unsets the "AssistedLocation" element
         */
        void unsetAssistedLocation();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static UpdateAssistedDocument.UpdateAssisted newInstance() {
              return (UpdateAssistedDocument.UpdateAssisted) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static UpdateAssistedDocument.UpdateAssisted newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (UpdateAssistedDocument.UpdateAssisted) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static UpdateAssistedDocument newInstance() {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static UpdateAssistedDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static UpdateAssistedDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static UpdateAssistedDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static UpdateAssistedDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static UpdateAssistedDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static UpdateAssistedDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static UpdateAssistedDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static UpdateAssistedDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static UpdateAssistedDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static UpdateAssistedDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static UpdateAssistedDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static UpdateAssistedDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static UpdateAssistedDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static UpdateAssistedDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static UpdateAssistedDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static UpdateAssistedDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static UpdateAssistedDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (UpdateAssistedDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
