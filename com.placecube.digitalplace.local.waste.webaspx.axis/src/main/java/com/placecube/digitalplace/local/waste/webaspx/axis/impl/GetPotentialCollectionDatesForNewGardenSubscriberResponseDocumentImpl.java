/*
 * An XML document type.
 * Localname: getPotentialCollectionDatesForNewGardenSubscriberResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument;

/**
 * A document containing one
 * getPotentialCollectionDatesForNewGardenSubscriberResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetPotentialCollectionDatesForNewGardenSubscriberResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
		implements GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument {

	/**
	 * An XML
	 * getPotentialCollectionDatesForNewGardenSubscriberResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetPotentialCollectionDatesForNewGardenSubscriberResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse {

		/**
		 * An XML
		 * getPotentialCollectionDatesForNewGardenSubscriberResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetPotentialCollectionDatesForNewGardenSubscriberResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements
				GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult {

			private static final long serialVersionUID = 1L;

			public GetPotentialCollectionDatesForNewGardenSubscriberResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"getPotentialCollectionDatesForNewGardenSubscriberResult");

		private static final long serialVersionUID = 1L;

		public GetPotentialCollectionDatesForNewGardenSubscriberResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "getPotentialCollectionDatesForNewGardenSubscriberResult" element
		 */
		@Override
		public GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult addNewGetPotentialCollectionDatesForNewGardenSubscriberResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult target = null;
				target = (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult) get_store()
						.add_element_user(GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getPotentialCollectionDatesForNewGardenSubscriberResult"
		 * element
		 */
		@Override
		public GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult getGetPotentialCollectionDatesForNewGardenSubscriberResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult target = null;
				target = (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult) get_store()
						.find_element_user(GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getPotentialCollectionDatesForNewGardenSubscriberResult"
		 * element
		 */
		@Override
		public boolean isSetGetPotentialCollectionDatesForNewGardenSubscriberResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getPotentialCollectionDatesForNewGardenSubscriberResult"
		 * element
		 */
		@Override
		public void setGetPotentialCollectionDatesForNewGardenSubscriberResult(
				GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult getPotentialCollectionDatesForNewGardenSubscriberResult) {
			generatedSetterHelperImpl(getPotentialCollectionDatesForNewGardenSubscriberResult, GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getPotentialCollectionDatesForNewGardenSubscriberResult"
		 * element
		 */
		@Override
		public void unsetGetPotentialCollectionDatesForNewGardenSubscriberResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"getPotentialCollectionDatesForNewGardenSubscriberResponse");

	private static final long serialVersionUID = 1L;

	public GetPotentialCollectionDatesForNewGardenSubscriberResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "getPotentialCollectionDatesForNewGardenSubscriberResponse" element
	 */
	@Override
	public GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse addNewGetPotentialCollectionDatesForNewGardenSubscriberResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse target = null;
			target = (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse) get_store()
					.add_element_user(GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getPotentialCollectionDatesForNewGardenSubscriberResponse"
	 * element
	 */
	@Override
	public GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse getGetPotentialCollectionDatesForNewGardenSubscriberResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse target = null;
			target = (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse) get_store()
					.find_element_user(GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getPotentialCollectionDatesForNewGardenSubscriberResponse"
	 * element
	 */
	@Override
	public void setGetPotentialCollectionDatesForNewGardenSubscriberResponse(
			GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse getPotentialCollectionDatesForNewGardenSubscriberResponse) {
		generatedSetterHelperImpl(getPotentialCollectionDatesForNewGardenSubscriberResponse, GETPOTENTIALCOLLECTIONDATESFORNEWGARDENSUBSCRIBERRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
