/*
 * An XML document type.
 * Localname: getPotentialCollectionDatesForNewGardenSubscriberResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getPotentialCollectionDatesForNewGardenSubscriberResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getpotentialcollectiondatesfornewgardensubscriberresponse963edoctype");
    
    /**
     * Gets the "getPotentialCollectionDatesForNewGardenSubscriberResponse" element
     */
    GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse getGetPotentialCollectionDatesForNewGardenSubscriberResponse();
    
    /**
     * Sets the "getPotentialCollectionDatesForNewGardenSubscriberResponse" element
     */
    void setGetPotentialCollectionDatesForNewGardenSubscriberResponse(GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse getPotentialCollectionDatesForNewGardenSubscriberResponse);
    
    /**
     * Appends and returns a new empty "getPotentialCollectionDatesForNewGardenSubscriberResponse" element
     */
    GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse addNewGetPotentialCollectionDatesForNewGardenSubscriberResponse();
    
    /**
     * An XML getPotentialCollectionDatesForNewGardenSubscriberResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetPotentialCollectionDatesForNewGardenSubscriberResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetPotentialCollectionDatesForNewGardenSubscriberResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getpotentialcollectiondatesfornewgardensubscriberresponse879eelemtype");
        
        /**
         * Gets the "getPotentialCollectionDatesForNewGardenSubscriberResult" element
         */
        GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult getGetPotentialCollectionDatesForNewGardenSubscriberResult();
        
        /**
         * True if has "getPotentialCollectionDatesForNewGardenSubscriberResult" element
         */
        boolean isSetGetPotentialCollectionDatesForNewGardenSubscriberResult();
        
        /**
         * Sets the "getPotentialCollectionDatesForNewGardenSubscriberResult" element
         */
        void setGetPotentialCollectionDatesForNewGardenSubscriberResult(GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult getPotentialCollectionDatesForNewGardenSubscriberResult);
        
        /**
         * Appends and returns a new empty "getPotentialCollectionDatesForNewGardenSubscriberResult" element
         */
        GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult addNewGetPotentialCollectionDatesForNewGardenSubscriberResult();
        
        /**
         * Unsets the "getPotentialCollectionDatesForNewGardenSubscriberResult" element
         */
        void unsetGetPotentialCollectionDatesForNewGardenSubscriberResult();
        
        /**
         * An XML getPotentialCollectionDatesForNewGardenSubscriberResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetPotentialCollectionDatesForNewGardenSubscriberResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetPotentialCollectionDatesForNewGardenSubscriberResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getpotentialcollectiondatesfornewgardensubscriberresultd29aelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult newInstance() {
                  return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse.GetPotentialCollectionDatesForNewGardenSubscriberResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse newInstance() {
              return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.GetPotentialCollectionDatesForNewGardenSubscriberResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument newInstance() {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
