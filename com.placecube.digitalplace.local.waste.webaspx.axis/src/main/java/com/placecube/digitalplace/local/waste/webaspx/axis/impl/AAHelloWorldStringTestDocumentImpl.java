/*
 * An XML document type.
 * Localname: AA_HelloWorld_String_Test
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AAHelloWorldStringTestDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldStringTestDocument;

/**
 * A document containing one
 * AA_HelloWorld_String_Test(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AAHelloWorldStringTestDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AAHelloWorldStringTestDocument {

	/**
	 * An XML
	 * AA_HelloWorld_String_Test(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AAHelloWorldStringTestImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AAHelloWorldStringTestDocument.AAHelloWorldStringTest {

		private static final long serialVersionUID = 1L;

		private static final javax.xml.namespace.QName TESTNAME$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "testName");

		public AAHelloWorldStringTestImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "testName" element
		 */
		@Override
		public java.lang.String getTestName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TESTNAME$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "testName" element
		 */
		@Override
		public boolean isSetTestName() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(TESTNAME$0) != 0;
			}
		}

		/**
		 * Sets the "testName" element
		 */
		@Override
		public void setTestName(java.lang.String testName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TESTNAME$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(TESTNAME$0);
				}
				target.setStringValue(testName);
			}
		}

		/**
		 * Unsets the "testName" element
		 */
		@Override
		public void unsetTestName() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(TESTNAME$0, 0);
			}
		}

		/**
		 * Gets (as xml) the "testName" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetTestName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TESTNAME$0, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "testName" element
		 */
		@Override
		public void xsetTestName(org.apache.xmlbeans.XmlString testName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TESTNAME$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(TESTNAME$0);
				}
				target.set(testName);
			}
		}
	}

	private static final javax.xml.namespace.QName AAHELLOWORLDSTRINGTEST$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AA_HelloWorld_String_Test");

	private static final long serialVersionUID = 1L;

	public AAHelloWorldStringTestDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AA_HelloWorld_String_Test" element
	 */
	@Override
	public AAHelloWorldStringTestDocument.AAHelloWorldStringTest addNewAAHelloWorldStringTest() {
		synchronized (monitor()) {
			check_orphaned();
			AAHelloWorldStringTestDocument.AAHelloWorldStringTest target = null;
			target = (AAHelloWorldStringTestDocument.AAHelloWorldStringTest) get_store().add_element_user(AAHELLOWORLDSTRINGTEST$0);
			return target;
		}
	}

	/**
	 * Gets the "AA_HelloWorld_String_Test" element
	 */
	@Override
	public AAHelloWorldStringTestDocument.AAHelloWorldStringTest getAAHelloWorldStringTest() {
		synchronized (monitor()) {
			check_orphaned();
			AAHelloWorldStringTestDocument.AAHelloWorldStringTest target = null;
			target = (AAHelloWorldStringTestDocument.AAHelloWorldStringTest) get_store().find_element_user(AAHELLOWORLDSTRINGTEST$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AA_HelloWorld_String_Test" element
	 */
	@Override
	public void setAAHelloWorldStringTest(AAHelloWorldStringTestDocument.AAHelloWorldStringTest aaHelloWorldStringTest) {
		generatedSetterHelperImpl(aaHelloWorldStringTest, AAHELLOWORLDSTRINGTEST$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
