/*
 * An XML document type.
 * Localname: Garden_Subscribers_Deliveries_CollectionsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscribersDeliveriesCollectionsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one Garden_Subscribers_Deliveries_CollectionsResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GardenSubscribersDeliveriesCollectionsResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscribersDeliveriesCollectionsResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscribersdeliveriescollectionsresponsea7d1doctype");
    
    /**
     * Gets the "Garden_Subscribers_Deliveries_CollectionsResponse" element
     */
    GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse getGardenSubscribersDeliveriesCollectionsResponse();
    
    /**
     * Sets the "Garden_Subscribers_Deliveries_CollectionsResponse" element
     */
    void setGardenSubscribersDeliveriesCollectionsResponse(GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse gardenSubscribersDeliveriesCollectionsResponse);
    
    /**
     * Appends and returns a new empty "Garden_Subscribers_Deliveries_CollectionsResponse" element
     */
    GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse addNewGardenSubscribersDeliveriesCollectionsResponse();
    
    /**
     * An XML Garden_Subscribers_Deliveries_CollectionsResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GardenSubscribersDeliveriesCollectionsResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscribersDeliveriesCollectionsResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscribersdeliveriescollectionsresponse8844elemtype");
        
        /**
         * Gets the "Garden_Subscribers_Deliveries_CollectionsResult" element
         */
        GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult getGardenSubscribersDeliveriesCollectionsResult();
        
        /**
         * True if has "Garden_Subscribers_Deliveries_CollectionsResult" element
         */
        boolean isSetGardenSubscribersDeliveriesCollectionsResult();
        
        /**
         * Sets the "Garden_Subscribers_Deliveries_CollectionsResult" element
         */
        void setGardenSubscribersDeliveriesCollectionsResult(GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult gardenSubscribersDeliveriesCollectionsResult);
        
        /**
         * Appends and returns a new empty "Garden_Subscribers_Deliveries_CollectionsResult" element
         */
        GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult addNewGardenSubscribersDeliveriesCollectionsResult();
        
        /**
         * Unsets the "Garden_Subscribers_Deliveries_CollectionsResult" element
         */
        void unsetGardenSubscribersDeliveriesCollectionsResult();
        
        /**
         * An XML Garden_Subscribers_Deliveries_CollectionsResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GardenSubscribersDeliveriesCollectionsResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscribersDeliveriesCollectionsResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscribersdeliveriescollectionsresult4f93elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult newInstance() {
                  return (GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse.GardenSubscribersDeliveriesCollectionsResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse newInstance() {
              return (GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GardenSubscribersDeliveriesCollectionsResponseDocument.GardenSubscribersDeliveriesCollectionsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GardenSubscribersDeliveriesCollectionsResponseDocument newInstance() {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscribersDeliveriesCollectionsResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscribersDeliveriesCollectionsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
