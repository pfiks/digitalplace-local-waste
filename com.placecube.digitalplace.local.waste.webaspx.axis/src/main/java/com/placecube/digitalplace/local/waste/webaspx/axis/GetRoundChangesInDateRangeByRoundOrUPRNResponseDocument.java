/*
 * An XML document type.
 * Localname: GetRoundChangesInDateRangeByRoundOrUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetRoundChangesInDateRangeByRoundOrUPRNResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundchangesindaterangebyroundoruprnresponse1d5ddoctype");
    
    /**
     * Gets the "GetRoundChangesInDateRangeByRoundOrUPRNResponse" element
     */
    GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse getGetRoundChangesInDateRangeByRoundOrUPRNResponse();
    
    /**
     * Sets the "GetRoundChangesInDateRangeByRoundOrUPRNResponse" element
     */
    void setGetRoundChangesInDateRangeByRoundOrUPRNResponse(GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse getRoundChangesInDateRangeByRoundOrUPRNResponse);
    
    /**
     * Appends and returns a new empty "GetRoundChangesInDateRangeByRoundOrUPRNResponse" element
     */
    GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse addNewGetRoundChangesInDateRangeByRoundOrUPRNResponse();
    
    /**
     * An XML GetRoundChangesInDateRangeByRoundOrUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetRoundChangesInDateRangeByRoundOrUPRNResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundChangesInDateRangeByRoundOrUPRNResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundchangesindaterangebyroundoruprnresponse755celemtype");
        
        /**
         * Gets the "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
         */
        GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult getGetRoundChangesInDateRangeByRoundOrUPRNResult();
        
        /**
         * True if has "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
         */
        boolean isSetGetRoundChangesInDateRangeByRoundOrUPRNResult();
        
        /**
         * Sets the "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
         */
        void setGetRoundChangesInDateRangeByRoundOrUPRNResult(GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult getRoundChangesInDateRangeByRoundOrUPRNResult);
        
        /**
         * Appends and returns a new empty "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
         */
        GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult addNewGetRoundChangesInDateRangeByRoundOrUPRNResult();
        
        /**
         * Unsets the "GetRoundChangesInDateRangeByRoundOrUPRNResult" element
         */
        void unsetGetRoundChangesInDateRangeByRoundOrUPRNResult();
        
        /**
         * An XML GetRoundChangesInDateRangeByRoundOrUPRNResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetRoundChangesInDateRangeByRoundOrUPRNResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundChangesInDateRangeByRoundOrUPRNResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundchangesindaterangebyroundoruprnresulteaf7elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult newInstance() {
                  return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse.GetRoundChangesInDateRangeByRoundOrUPRNResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse newInstance() {
              return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.GetRoundChangesInDateRangeByRoundOrUPRNResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument newInstance() {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
