/*
 * An XML document type.
 * Localname: GetIssues
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetIssuesDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetIssues(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetIssuesDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetIssuesDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getissues2ddadoctype");
    
    /**
     * Gets the "GetIssues" element
     */
    GetIssuesDocument.GetIssues getGetIssues();
    
    /**
     * Sets the "GetIssues" element
     */
    void setGetIssues(GetIssuesDocument.GetIssues getIssues);
    
    /**
     * Appends and returns a new empty "GetIssues" element
     */
    GetIssuesDocument.GetIssues addNewGetIssues();
    
    /**
     * An XML GetIssues(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetIssues extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetIssues.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getissuesd2d6elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "dateReq" element
         */
        java.lang.String getDateReq();
        
        /**
         * Gets (as xml) the "dateReq" element
         */
        org.apache.xmlbeans.XmlString xgetDateReq();
        
        /**
         * True if has "dateReq" element
         */
        boolean isSetDateReq();
        
        /**
         * Sets the "dateReq" element
         */
        void setDateReq(java.lang.String dateReq);
        
        /**
         * Sets (as xml) the "dateReq" element
         */
        void xsetDateReq(org.apache.xmlbeans.XmlString dateReq);
        
        /**
         * Unsets the "dateReq" element
         */
        void unsetDateReq();
        
        /**
         * Gets the "issueStartTime" element
         */
        java.lang.String getIssueStartTime();
        
        /**
         * Gets (as xml) the "issueStartTime" element
         */
        org.apache.xmlbeans.XmlString xgetIssueStartTime();
        
        /**
         * True if has "issueStartTime" element
         */
        boolean isSetIssueStartTime();
        
        /**
         * Sets the "issueStartTime" element
         */
        void setIssueStartTime(java.lang.String issueStartTime);
        
        /**
         * Sets (as xml) the "issueStartTime" element
         */
        void xsetIssueStartTime(org.apache.xmlbeans.XmlString issueStartTime);
        
        /**
         * Unsets the "issueStartTime" element
         */
        void unsetIssueStartTime();
        
        /**
         * Gets the "issueEndTime" element
         */
        java.lang.String getIssueEndTime();
        
        /**
         * Gets (as xml) the "issueEndTime" element
         */
        org.apache.xmlbeans.XmlString xgetIssueEndTime();
        
        /**
         * True if has "issueEndTime" element
         */
        boolean isSetIssueEndTime();
        
        /**
         * Sets the "issueEndTime" element
         */
        void setIssueEndTime(java.lang.String issueEndTime);
        
        /**
         * Sets (as xml) the "issueEndTime" element
         */
        void xsetIssueEndTime(org.apache.xmlbeans.XmlString issueEndTime);
        
        /**
         * Unsets the "issueEndTime" element
         */
        void unsetIssueEndTime();
        
        /**
         * Gets the "issueSortField" element
         */
        java.lang.String getIssueSortField();
        
        /**
         * Gets (as xml) the "issueSortField" element
         */
        org.apache.xmlbeans.XmlString xgetIssueSortField();
        
        /**
         * True if has "issueSortField" element
         */
        boolean isSetIssueSortField();
        
        /**
         * Sets the "issueSortField" element
         */
        void setIssueSortField(java.lang.String issueSortField);
        
        /**
         * Sets (as xml) the "issueSortField" element
         */
        void xsetIssueSortField(org.apache.xmlbeans.XmlString issueSortField);
        
        /**
         * Unsets the "issueSortField" element
         */
        void unsetIssueSortField();
        
        /**
         * Gets the "issueSortOrder" element
         */
        java.lang.String getIssueSortOrder();
        
        /**
         * Gets (as xml) the "issueSortOrder" element
         */
        org.apache.xmlbeans.XmlString xgetIssueSortOrder();
        
        /**
         * True if has "issueSortOrder" element
         */
        boolean isSetIssueSortOrder();
        
        /**
         * Sets the "issueSortOrder" element
         */
        void setIssueSortOrder(java.lang.String issueSortOrder);
        
        /**
         * Sets (as xml) the "issueSortOrder" element
         */
        void xsetIssueSortOrder(org.apache.xmlbeans.XmlString issueSortOrder);
        
        /**
         * Unsets the "issueSortOrder" element
         */
        void unsetIssueSortOrder();
        
        /**
         * Gets the "issueFilterName" element
         */
        java.lang.String getIssueFilterName();
        
        /**
         * Gets (as xml) the "issueFilterName" element
         */
        org.apache.xmlbeans.XmlString xgetIssueFilterName();
        
        /**
         * True if has "issueFilterName" element
         */
        boolean isSetIssueFilterName();
        
        /**
         * Sets the "issueFilterName" element
         */
        void setIssueFilterName(java.lang.String issueFilterName);
        
        /**
         * Sets (as xml) the "issueFilterName" element
         */
        void xsetIssueFilterName(org.apache.xmlbeans.XmlString issueFilterName);
        
        /**
         * Unsets the "issueFilterName" element
         */
        void unsetIssueFilterName();
        
        /**
         * Gets the "issueFilterType" element
         */
        java.lang.String getIssueFilterType();
        
        /**
         * Gets (as xml) the "issueFilterType" element
         */
        org.apache.xmlbeans.XmlString xgetIssueFilterType();
        
        /**
         * True if has "issueFilterType" element
         */
        boolean isSetIssueFilterType();
        
        /**
         * Sets the "issueFilterType" element
         */
        void setIssueFilterType(java.lang.String issueFilterType);
        
        /**
         * Sets (as xml) the "issueFilterType" element
         */
        void xsetIssueFilterType(org.apache.xmlbeans.XmlString issueFilterType);
        
        /**
         * Unsets the "issueFilterType" element
         */
        void unsetIssueFilterType();
        
        /**
         * Gets the "searchAllDates" element
         */
        java.lang.String getSearchAllDates();
        
        /**
         * Gets (as xml) the "searchAllDates" element
         */
        org.apache.xmlbeans.XmlString xgetSearchAllDates();
        
        /**
         * True if has "searchAllDates" element
         */
        boolean isSetSearchAllDates();
        
        /**
         * Sets the "searchAllDates" element
         */
        void setSearchAllDates(java.lang.String searchAllDates);
        
        /**
         * Sets (as xml) the "searchAllDates" element
         */
        void xsetSearchAllDates(org.apache.xmlbeans.XmlString searchAllDates);
        
        /**
         * Unsets the "searchAllDates" element
         */
        void unsetSearchAllDates();
        
        /**
         * Gets the "propNoName" element
         */
        java.lang.String getPropNoName();
        
        /**
         * Gets (as xml) the "propNoName" element
         */
        org.apache.xmlbeans.XmlString xgetPropNoName();
        
        /**
         * True if has "propNoName" element
         */
        boolean isSetPropNoName();
        
        /**
         * Sets the "propNoName" element
         */
        void setPropNoName(java.lang.String propNoName);
        
        /**
         * Sets (as xml) the "propNoName" element
         */
        void xsetPropNoName(org.apache.xmlbeans.XmlString propNoName);
        
        /**
         * Unsets the "propNoName" element
         */
        void unsetPropNoName();
        
        /**
         * Gets the "propStreet" element
         */
        java.lang.String getPropStreet();
        
        /**
         * Gets (as xml) the "propStreet" element
         */
        org.apache.xmlbeans.XmlString xgetPropStreet();
        
        /**
         * True if has "propStreet" element
         */
        boolean isSetPropStreet();
        
        /**
         * Sets the "propStreet" element
         */
        void setPropStreet(java.lang.String propStreet);
        
        /**
         * Sets (as xml) the "propStreet" element
         */
        void xsetPropStreet(org.apache.xmlbeans.XmlString propStreet);
        
        /**
         * Unsets the "propStreet" element
         */
        void unsetPropStreet();
        
        /**
         * Gets the "UPRN" element
         */
        java.lang.String getUPRN();
        
        /**
         * Gets (as xml) the "UPRN" element
         */
        org.apache.xmlbeans.XmlString xgetUPRN();
        
        /**
         * True if has "UPRN" element
         */
        boolean isSetUPRN();
        
        /**
         * Sets the "UPRN" element
         */
        void setUPRN(java.lang.String uprn);
        
        /**
         * Sets (as xml) the "UPRN" element
         */
        void xsetUPRN(org.apache.xmlbeans.XmlString uprn);
        
        /**
         * Unsets the "UPRN" element
         */
        void unsetUPRN();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetIssuesDocument.GetIssues newInstance() {
              return (GetIssuesDocument.GetIssues) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetIssuesDocument.GetIssues newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetIssuesDocument.GetIssues) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetIssuesDocument newInstance() {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetIssuesDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetIssuesDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetIssuesDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetIssuesDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetIssuesDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetIssuesDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetIssuesDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetIssuesDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetIssuesDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetIssuesDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetIssuesDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetIssuesDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetIssuesDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetIssuesDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetIssuesDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetIssuesDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetIssuesDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetIssuesDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
