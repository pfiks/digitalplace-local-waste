/*
 * An XML document type.
 * Localname: GetTrucksOutTodayResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetTrucksOutTodayResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetTrucksOutTodayResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetTrucksOutTodayResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetTrucksOutTodayResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gettrucksouttodayresponsecaa8doctype");
    
    /**
     * Gets the "GetTrucksOutTodayResponse" element
     */
    GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse getGetTrucksOutTodayResponse();
    
    /**
     * Sets the "GetTrucksOutTodayResponse" element
     */
    void setGetTrucksOutTodayResponse(GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse getTrucksOutTodayResponse);
    
    /**
     * Appends and returns a new empty "GetTrucksOutTodayResponse" element
     */
    GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse addNewGetTrucksOutTodayResponse();
    
    /**
     * An XML GetTrucksOutTodayResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetTrucksOutTodayResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetTrucksOutTodayResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gettrucksouttodayresponse5172elemtype");
        
        /**
         * Gets the "GetTrucksOutTodayResult" element
         */
        GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult getGetTrucksOutTodayResult();
        
        /**
         * True if has "GetTrucksOutTodayResult" element
         */
        boolean isSetGetTrucksOutTodayResult();
        
        /**
         * Sets the "GetTrucksOutTodayResult" element
         */
        void setGetTrucksOutTodayResult(GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult getTrucksOutTodayResult);
        
        /**
         * Appends and returns a new empty "GetTrucksOutTodayResult" element
         */
        GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult addNewGetTrucksOutTodayResult();
        
        /**
         * Unsets the "GetTrucksOutTodayResult" element
         */
        void unsetGetTrucksOutTodayResult();
        
        /**
         * An XML GetTrucksOutTodayResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetTrucksOutTodayResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetTrucksOutTodayResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gettrucksouttodayresult4158elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult newInstance() {
                  return (GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse newInstance() {
              return (GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetTrucksOutTodayResponseDocument newInstance() {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetTrucksOutTodayResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetTrucksOutTodayResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetTrucksOutTodayResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetTrucksOutTodayResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetTrucksOutTodayResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetTrucksOutTodayResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetTrucksOutTodayResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetTrucksOutTodayResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetTrucksOutTodayResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetTrucksOutTodayResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetTrucksOutTodayResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetTrucksOutTodayResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetTrucksOutTodayResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetTrucksOutTodayResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetTrucksOutTodayResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetTrucksOutTodayResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetTrucksOutTodayResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetTrucksOutTodayResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
