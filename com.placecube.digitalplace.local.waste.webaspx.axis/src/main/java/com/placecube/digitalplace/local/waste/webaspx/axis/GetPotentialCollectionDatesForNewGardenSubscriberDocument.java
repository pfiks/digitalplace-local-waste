/*
 * An XML document type.
 * Localname: getPotentialCollectionDatesForNewGardenSubscriber
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetPotentialCollectionDatesForNewGardenSubscriberDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getPotentialCollectionDatesForNewGardenSubscriber(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetPotentialCollectionDatesForNewGardenSubscriberDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetPotentialCollectionDatesForNewGardenSubscriberDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getpotentialcollectiondatesfornewgardensubscriberf99ddoctype");
    
    /**
     * Gets the "getPotentialCollectionDatesForNewGardenSubscriber" element
     */
    GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber getGetPotentialCollectionDatesForNewGardenSubscriber();
    
    /**
     * Sets the "getPotentialCollectionDatesForNewGardenSubscriber" element
     */
    void setGetPotentialCollectionDatesForNewGardenSubscriber(GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber getPotentialCollectionDatesForNewGardenSubscriber);
    
    /**
     * Appends and returns a new empty "getPotentialCollectionDatesForNewGardenSubscriber" element
     */
    GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber addNewGetPotentialCollectionDatesForNewGardenSubscriber();
    
    /**
     * An XML getPotentialCollectionDatesForNewGardenSubscriber(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetPotentialCollectionDatesForNewGardenSubscriber extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetPotentialCollectionDatesForNewGardenSubscriber.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getpotentialcollectiondatesfornewgardensubscriber1ddcelemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "UPRN" element
         */
        java.lang.String getUPRN();
        
        /**
         * Gets (as xml) the "UPRN" element
         */
        org.apache.xmlbeans.XmlString xgetUPRN();
        
        /**
         * True if has "UPRN" element
         */
        boolean isSetUPRN();
        
        /**
         * Sets the "UPRN" element
         */
        void setUPRN(java.lang.String uprn);
        
        /**
         * Sets (as xml) the "UPRN" element
         */
        void xsetUPRN(org.apache.xmlbeans.XmlString uprn);
        
        /**
         * Unsets the "UPRN" element
         */
        void unsetUPRN();
        
        /**
         * Gets the "startDate_ddsMMsyyyy" element
         */
        java.lang.String getStartDateDdsMMsyyyy();
        
        /**
         * Gets (as xml) the "startDate_ddsMMsyyyy" element
         */
        org.apache.xmlbeans.XmlString xgetStartDateDdsMMsyyyy();
        
        /**
         * True if has "startDate_ddsMMsyyyy" element
         */
        boolean isSetStartDateDdsMMsyyyy();
        
        /**
         * Sets the "startDate_ddsMMsyyyy" element
         */
        void setStartDateDdsMMsyyyy(java.lang.String startDateDdsMMsyyyy);
        
        /**
         * Sets (as xml) the "startDate_ddsMMsyyyy" element
         */
        void xsetStartDateDdsMMsyyyy(org.apache.xmlbeans.XmlString startDateDdsMMsyyyy);
        
        /**
         * Unsets the "startDate_ddsMMsyyyy" element
         */
        void unsetStartDateDdsMMsyyyy();
        
        /**
         * Gets the "endDate_ddsMMsyyyy" element
         */
        java.lang.String getEndDateDdsMMsyyyy();
        
        /**
         * Gets (as xml) the "endDate_ddsMMsyyyy" element
         */
        org.apache.xmlbeans.XmlString xgetEndDateDdsMMsyyyy();
        
        /**
         * True if has "endDate_ddsMMsyyyy" element
         */
        boolean isSetEndDateDdsMMsyyyy();
        
        /**
         * Sets the "endDate_ddsMMsyyyy" element
         */
        void setEndDateDdsMMsyyyy(java.lang.String endDateDdsMMsyyyy);
        
        /**
         * Sets (as xml) the "endDate_ddsMMsyyyy" element
         */
        void xsetEndDateDdsMMsyyyy(org.apache.xmlbeans.XmlString endDateDdsMMsyyyy);
        
        /**
         * Unsets the "endDate_ddsMMsyyyy" element
         */
        void unsetEndDateDdsMMsyyyy();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber newInstance() {
              return (GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetPotentialCollectionDatesForNewGardenSubscriberDocument.GetPotentialCollectionDatesForNewGardenSubscriber) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument newInstance() {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetPotentialCollectionDatesForNewGardenSubscriberDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetPotentialCollectionDatesForNewGardenSubscriberDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
