/*
 * An XML document type.
 * Localname: AHP_GetDetailsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPGetDetailsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AHPGetDetailsResponseDocument;

/**
 * A document containing one
 * AHP_GetDetailsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AHPGetDetailsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPGetDetailsResponseDocument {

	/**
	 * An XML
	 * AHP_GetDetailsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AHPGetDetailsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPGetDetailsResponseDocument.AHPGetDetailsResponse {

		/**
		 * An XML
		 * AHP_GetDetailsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class AHPGetDetailsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPGetDetailsResponseDocument.AHPGetDetailsResponse.AHPGetDetailsResult {

			private static final long serialVersionUID = 1L;

			public AHPGetDetailsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName AHPGETDETAILSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_GetDetailsResult");

		private static final long serialVersionUID = 1L;

		public AHPGetDetailsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "AHP_GetDetailsResult" element
		 */
		@Override
		public AHPGetDetailsResponseDocument.AHPGetDetailsResponse.AHPGetDetailsResult addNewAHPGetDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPGetDetailsResponseDocument.AHPGetDetailsResponse.AHPGetDetailsResult target = null;
				target = (AHPGetDetailsResponseDocument.AHPGetDetailsResponse.AHPGetDetailsResult) get_store().add_element_user(AHPGETDETAILSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "AHP_GetDetailsResult" element
		 */
		@Override
		public AHPGetDetailsResponseDocument.AHPGetDetailsResponse.AHPGetDetailsResult getAHPGetDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPGetDetailsResponseDocument.AHPGetDetailsResponse.AHPGetDetailsResult target = null;
				target = (AHPGetDetailsResponseDocument.AHPGetDetailsResponse.AHPGetDetailsResult) get_store().find_element_user(AHPGETDETAILSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "AHP_GetDetailsResult" element
		 */
		@Override
		public boolean isSetAHPGetDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPGETDETAILSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AHP_GetDetailsResult" element
		 */
		@Override
		public void setAHPGetDetailsResult(AHPGetDetailsResponseDocument.AHPGetDetailsResponse.AHPGetDetailsResult ahpGetDetailsResult) {
			generatedSetterHelperImpl(ahpGetDetailsResult, AHPGETDETAILSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "AHP_GetDetailsResult" element
		 */
		@Override
		public void unsetAHPGetDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPGETDETAILSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName AHPGETDETAILSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_GetDetailsResponse");

	private static final long serialVersionUID = 1L;

	public AHPGetDetailsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AHP_GetDetailsResponse" element
	 */
	@Override
	public AHPGetDetailsResponseDocument.AHPGetDetailsResponse addNewAHPGetDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPGetDetailsResponseDocument.AHPGetDetailsResponse target = null;
			target = (AHPGetDetailsResponseDocument.AHPGetDetailsResponse) get_store().add_element_user(AHPGETDETAILSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AHP_GetDetailsResponse" element
	 */
	@Override
	public AHPGetDetailsResponseDocument.AHPGetDetailsResponse getAHPGetDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPGetDetailsResponseDocument.AHPGetDetailsResponse target = null;
			target = (AHPGetDetailsResponseDocument.AHPGetDetailsResponse) get_store().find_element_user(AHPGETDETAILSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AHP_GetDetailsResponse" element
	 */
	@Override
	public void setAHPGetDetailsResponse(AHPGetDetailsResponseDocument.AHPGetDetailsResponse ahpGetDetailsResponse) {
		generatedSetterHelperImpl(ahpGetDetailsResponse, AHPGETDETAILSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
