/*
 * An XML document type.
 * Localname: AHP_SuspendSubscriptionResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPSuspendSubscriptionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AHPSuspendSubscriptionResponseDocument;

/**
 * A document containing one
 * AHP_SuspendSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AHPSuspendSubscriptionResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPSuspendSubscriptionResponseDocument {

	/**
	 * An XML
	 * AHP_SuspendSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AHPSuspendSubscriptionResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse {

		/**
		 * An XML
		 * AHP_SuspendSubscriptionResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class AHPSuspendSubscriptionResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult {

			private static final long serialVersionUID = 1L;

			public AHPSuspendSubscriptionResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName AHPSUSPENDSUBSCRIPTIONRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_SuspendSubscriptionResult");

		private static final long serialVersionUID = 1L;

		public AHPSuspendSubscriptionResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "AHP_SuspendSubscriptionResult"
		 * element
		 */
		@Override
		public AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult addNewAHPSuspendSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult target = null;
				target = (AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult) get_store().add_element_user(AHPSUSPENDSUBSCRIPTIONRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "AHP_SuspendSubscriptionResult" element
		 */
		@Override
		public AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult getAHPSuspendSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult target = null;
				target = (AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult) get_store().find_element_user(AHPSUSPENDSUBSCRIPTIONRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "AHP_SuspendSubscriptionResult" element
		 */
		@Override
		public boolean isSetAHPSuspendSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPSUSPENDSUBSCRIPTIONRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AHP_SuspendSubscriptionResult" element
		 */
		@Override
		public void setAHPSuspendSubscriptionResult(AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse.AHPSuspendSubscriptionResult ahpSuspendSubscriptionResult) {
			generatedSetterHelperImpl(ahpSuspendSubscriptionResult, AHPSUSPENDSUBSCRIPTIONRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "AHP_SuspendSubscriptionResult" element
		 */
		@Override
		public void unsetAHPSuspendSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPSUSPENDSUBSCRIPTIONRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName AHPSUSPENDSUBSCRIPTIONRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_SuspendSubscriptionResponse");

	private static final long serialVersionUID = 1L;

	public AHPSuspendSubscriptionResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AHP_SuspendSubscriptionResponse" element
	 */
	@Override
	public AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse addNewAHPSuspendSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse target = null;
			target = (AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse) get_store().add_element_user(AHPSUSPENDSUBSCRIPTIONRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AHP_SuspendSubscriptionResponse" element
	 */
	@Override
	public AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse getAHPSuspendSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse target = null;
			target = (AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse) get_store().find_element_user(AHPSUSPENDSUBSCRIPTIONRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AHP_SuspendSubscriptionResponse" element
	 */
	@Override
	public void setAHPSuspendSubscriptionResponse(AHPSuspendSubscriptionResponseDocument.AHPSuspendSubscriptionResponse ahpSuspendSubscriptionResponse) {
		generatedSetterHelperImpl(ahpSuspendSubscriptionResponse, AHPSUSPENDSUBSCRIPTIONRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
