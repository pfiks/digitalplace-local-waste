/*
 * An XML document type.
 * Localname: BinInsertResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: BinInsertResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.BinInsertResponseDocument;

/**
 * A document containing one
 * BinInsertResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class BinInsertResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinInsertResponseDocument {

	/**
	 * An XML BinInsertResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class BinInsertResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinInsertResponseDocument.BinInsertResponse {

		/**
		 * An XML
		 * BinInsertResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class BinInsertResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinInsertResponseDocument.BinInsertResponse.BinInsertResult {

			private static final long serialVersionUID = 1L;

			public BinInsertResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName BININSERTRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinInsertResult");

		private static final long serialVersionUID = 1L;

		public BinInsertResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "BinInsertResult" element
		 */
		@Override
		public BinInsertResponseDocument.BinInsertResponse.BinInsertResult addNewBinInsertResult() {
			synchronized (monitor()) {
				check_orphaned();
				BinInsertResponseDocument.BinInsertResponse.BinInsertResult target = null;
				target = (BinInsertResponseDocument.BinInsertResponse.BinInsertResult) get_store().add_element_user(BININSERTRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "BinInsertResult" element
		 */
		@Override
		public BinInsertResponseDocument.BinInsertResponse.BinInsertResult getBinInsertResult() {
			synchronized (monitor()) {
				check_orphaned();
				BinInsertResponseDocument.BinInsertResponse.BinInsertResult target = null;
				target = (BinInsertResponseDocument.BinInsertResponse.BinInsertResult) get_store().find_element_user(BININSERTRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "BinInsertResult" element
		 */
		@Override
		public boolean isSetBinInsertResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BININSERTRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "BinInsertResult" element
		 */
		@Override
		public void setBinInsertResult(BinInsertResponseDocument.BinInsertResponse.BinInsertResult binInsertResult) {
			generatedSetterHelperImpl(binInsertResult, BININSERTRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "BinInsertResult" element
		 */
		@Override
		public void unsetBinInsertResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BININSERTRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName BININSERTRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinInsertResponse");

	private static final long serialVersionUID = 1L;

	public BinInsertResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "BinInsertResponse" element
	 */
	@Override
	public BinInsertResponseDocument.BinInsertResponse addNewBinInsertResponse() {
		synchronized (monitor()) {
			check_orphaned();
			BinInsertResponseDocument.BinInsertResponse target = null;
			target = (BinInsertResponseDocument.BinInsertResponse) get_store().add_element_user(BININSERTRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "BinInsertResponse" element
	 */
	@Override
	public BinInsertResponseDocument.BinInsertResponse getBinInsertResponse() {
		synchronized (monitor()) {
			check_orphaned();
			BinInsertResponseDocument.BinInsertResponse target = null;
			target = (BinInsertResponseDocument.BinInsertResponse) get_store().find_element_user(BININSERTRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "BinInsertResponse" element
	 */
	@Override
	public void setBinInsertResponse(BinInsertResponseDocument.BinInsertResponse binInsertResponse) {
		generatedSetterHelperImpl(binInsertResponse, BININSERTRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
