/**
 * WSCollExternalStub.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.0 Built on : Aug 01,
 * 2021 (07:27:19 HST)
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;

import javax.xml.namespace.QName;

/*
 *  WSCollExternalStub java implementation
 */

public class WSCollExternalStub extends org.apache.axis2.client.Stub implements WSCollExternal {

	private static int counter = 0;

	protected org.apache.axis2.description.AxisOperation[] _operations;
	private final org.apache.xmlbeans.XmlOptions _xmlOptions;
	private java.util.Map<org.apache.axis2.client.FaultMapKey, String> faultExceptionClassNameMap = new java.util.HashMap<>();

	// hashmaps to keep the fault mapping
	private java.util.Map<org.apache.axis2.client.FaultMapKey, String> faultExceptionNameMap = new java.util.HashMap<>();

	private java.util.Map<org.apache.axis2.client.FaultMapKey, String> faultMessageMap = new java.util.HashMap<>();

	private javax.xml.namespace.QName[] opNameArray = null;

	/** Default Constructor */
	public WSCollExternalStub() throws org.apache.axis2.AxisFault {

		this("https://collections-rugby.azurewebsites.net/WSCollExternal.asmx");
	}

	/** Constructor taking the target endpoint */
	public WSCollExternalStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
		this(null, targetEndpoint);
	}

	/** Default Constructor */
	public WSCollExternalStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {

		this(configurationContext, "https://collections-rugby.azurewebsites.net/WSCollExternal.asmx");
	}

	/** Constructor that takes in a configContext */
	public WSCollExternalStub(org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
		this(configurationContext, targetEndpoint, false);
	}

	/** Constructor that takes in a configContext and useseperate listner */
	public WSCollExternalStub(org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint, boolean useSeparateListener) throws org.apache.axis2.AxisFault {
		// To populate AxisService
		populateAxisService();
		populateFaults();

		_serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext, _service);

		_serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(targetEndpoint));
		_serviceClient.getOptions().setUseSeparateListener(useSeparateListener);

		// Set the soap version
		_serviceClient.getOptions().setSoapVersionURI(org.apache.axiom.soap.SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
	}

	private static synchronized java.lang.String getUniqueSuffix() {
		// reset the counter if it is greater than 99999
		if (counter > 99999) {
			counter = 0;
		}
		counter = counter + 1;
		return java.lang.Long.toString(java.lang.System.currentTimeMillis()) + "_" + counter;
	}

	{
		_xmlOptions = new org.apache.xmlbeans.XmlOptions();
		_xmlOptions.setSaveNoXmlDecl();
		_xmlOptions.setSaveAggressiveNamespaces();
		_xmlOptions.setSaveNamespacesFirst();
	}

	/**
	 * Get the {@link org.apache.xmlbeans.XmlOptions} object that the stub uses
	 * when serializing objects to XML.
	 *
	 * @return the options used for serialization
	 */
	public org.apache.xmlbeans.XmlOptions _getXmlOptions() {
		return _xmlOptions;
	}

	/**
	 * Auto generated method signature Test Webservice to check communication.
	 * Returns Hello : Testname as a string
	 *
	 * @see WSCollExternal#aA_HelloWorld_String_Test
	 * @param aA_HelloWorld_String_Test192
	 */
	@Override
	public AAHelloWorldStringTestResponseDocument aA_HelloWorld_String_Test(AAHelloWorldStringTestDocument aA_HelloWorld_String_Test192) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[32].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AA_HelloWorld_String_Test");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aA_HelloWorld_String_Test192,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aA_HelloWorld_String_Test")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AA_HelloWorld_String_Test"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AAHelloWorldStringTestResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AAHelloWorldStringTestResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_String_Test"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_String_Test"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_String_Test"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Test Webservice to check communication.
	 * Returns Hello : Testname as a XML
	 *
	 * @see WSCollExternal#aA_HelloWorld_XML_Test
	 * @param aA_HelloWorld_XML_Test238
	 */
	@Override
	public AAHelloWorldXMLTestResponseDocument aA_HelloWorld_XML_Test(AAHelloWorldXMLTestDocument aA_HelloWorld_XML_Test238) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[55].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AA_HelloWorld_XML_Test");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aA_HelloWorld_XML_Test238,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aA_HelloWorld_XML_Test")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AA_HelloWorld_XML_Test"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AAHelloWorldXMLTestResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AAHelloWorldXMLTestResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_XML_Test"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_XML_Test"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_XML_Test"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature AHP Webservice - change address
	 *
	 * @see WSCollExternal#aHP_ChangeAddress
	 * @param aHP_ChangeAddress204
	 */
	@Override
	public AHPChangeAddressResponseDocument aHP_ChangeAddress(AHPChangeAddressDocument aHP_ChangeAddress204) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[38].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_ChangeAddress");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_ChangeAddress204,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_ChangeAddress")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_ChangeAddress"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AHPChangeAddressResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AHPChangeAddressResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ChangeAddress"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ChangeAddress"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ChangeAddress"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature AHP Webservice - Extend Subscription date
	 *
	 * @see WSCollExternal#aHP_ExtendSubscription
	 * @param aHP_ExtendSubscription252
	 */
	@Override
	public AHPExtendSubscriptionResponseDocument aHP_ExtendSubscription(AHPExtendSubscriptionDocument aHP_ExtendSubscription252) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[62].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_ExtendSubscription");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_ExtendSubscription252,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_ExtendSubscription")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_ExtendSubscription"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AHPExtendSubscriptionResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AHPExtendSubscriptionResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ExtendSubscription"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ExtendSubscription"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ExtendSubscription"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature AHP Webservice - Get AHP details for UPRN
	 *
	 * @see WSCollExternal#aHP_GetDetails
	 * @param aHP_GetDetails216
	 */
	@Override
	public AHPGetDetailsResponseDocument aHP_GetDetails(AHPGetDetailsDocument aHP_GetDetails216) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[44].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_GetDetails");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_GetDetails216,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_GetDetails")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_GetDetails"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AHPGetDetailsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AHPGetDetailsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_GetDetails"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_GetDetails"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_GetDetails"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature AHP Webservice - Register new or update
	 * details of existing
	 *
	 * @see WSCollExternal#aHP_NewUpdate
	 * @param aHP_NewUpdate222
	 */
	@Override
	public AHPNewUpdateResponseDocument aHP_NewUpdate(AHPNewUpdateDocument aHP_NewUpdate222) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[47].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_NewUpdate");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_NewUpdate222,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_NewUpdate")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_NewUpdate"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AHPNewUpdateResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AHPNewUpdateResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_NewUpdate"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_NewUpdate"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_NewUpdate"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature AHP Webservice - Order new Bags
	 *
	 * @see WSCollExternal#aHP_OrderNewBags
	 * @param aHP_OrderNewBags128
	 */
	@Override
	public AHPOrderNewBagsResponseDocument aHP_OrderNewBags(AHPOrderNewBagsDocument aHP_OrderNewBags128) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_OrderNewBags");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_OrderNewBags128,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_OrderNewBags")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_OrderNewBags"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AHPOrderNewBagsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AHPOrderNewBagsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_OrderNewBags"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_OrderNewBags"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_OrderNewBags"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature AHP Webservice - remove Subscription
	 *
	 * @see WSCollExternal#aHP_RemoveSubscription
	 * @param aHP_RemoveSubscription240
	 */
	@Override
	public AHPRemoveSubscriptionResponseDocument aHP_RemoveSubscription(AHPRemoveSubscriptionDocument aHP_RemoveSubscription240) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[56].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_RemoveSubscription");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_RemoveSubscription240,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_RemoveSubscription")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_RemoveSubscription"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AHPRemoveSubscriptionResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AHPRemoveSubscriptionResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_RemoveSubscription"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_RemoveSubscription"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_RemoveSubscription"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature AHP Webservice - suspend subscription
	 *
	 * @see WSCollExternal#aHP_SuspendSubscription
	 * @param aHP_SuspendSubscription136
	 */
	@Override
	public AHPSuspendSubscriptionResponseDocument aHP_SuspendSubscription(AHPSuspendSubscriptionDocument aHP_SuspendSubscription136) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[4].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_SuspendSubscription");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_SuspendSubscription136,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_SuspendSubscription")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_SuspendSubscription"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), AHPSuspendSubscriptionResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (AHPSuspendSubscriptionResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_SuspendSubscription"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_SuspendSubscription"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_SuspendSubscription"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Deletes a container. Requires binID and
	 * UPRN
	 *
	 * @see WSCollExternal#binDelete
	 * @param binDelete162
	 */
	@Override
	public BinDeleteResponseDocument binDelete(BinDeleteDocument binDelete162) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[17].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/BinDelete");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), binDelete162,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binDelete")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinDelete"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), BinDeleteResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (BinDeleteResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinDelete"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinDelete"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinDelete"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Inserts a container into the collections
	 * database bins table. Bin must exist in BinShop. Use in conjunction with
	 * GetAvailableContainers to get available containers for this client
	 *
	 * @see WSCollExternal#binInsert
	 * @param binInsert172
	 */
	@Override
	public BinInsertResponseDocument binInsert(BinInsertDocument binInsert172) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[22].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/BinInsert");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), binInsert172,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binInsert")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinInsert"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), BinInsertResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (BinInsertResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinInsert"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinInsert"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinInsert"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Updates a container in the bins table by
	 * id of bin. BinType must be in Binshop
	 *
	 * @see WSCollExternal#binUpdate
	 * @param binUpdate170
	 */
	@Override
	public BinUpdateResponseDocument binUpdate(BinUpdateDocument binUpdate170) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[21].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/BinUpdate");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), binUpdate170,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binUpdate")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinUpdate"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), BinUpdateResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (BinUpdateResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinUpdate"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinUpdate"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinUpdate"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns a line for each UPRN in the
	 * databsae that has a collection along with the unique calendar combination
	 * reference - all uprns with same reference have the same rounds schedule
	 * across all services
	 *
	 * @see WSCollExternal#cachedCalendar
	 * @param cachedCalendar246
	 */
	@Override
	public CachedCalendarResponseDocument cachedCalendar(CachedCalendarDocument cachedCalendar246) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[59].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/CachedCalendar");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), cachedCalendar246,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "cachedCalendar")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CachedCalendar"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), CachedCalendarResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (CachedCalendarResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CachedCalendar"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CachedCalendar"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CachedCalendar"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns assisted details for a UPRN
	 *
	 * @see WSCollExternal#checkAssisted
	 * @param checkAssisted198
	 */
	@Override
	public CheckAssistedResponseDocument checkAssisted(CheckAssistedDocument checkAssisted198) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[35].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/CheckAssisted");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), checkAssisted198,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "checkAssisted")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CheckAssisted"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), CheckAssistedResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (CheckAssistedResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CheckAssisted"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CheckAssisted"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CheckAssisted"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Incab Comments - Get Current Value
	 *
	 * @see WSCollExternal#commentsForIncabGetExisting
	 * @param commentsForIncabGetExisting232
	 */
	@Override
	public CommentsForIncabGetExistingResponseDocument commentsForIncabGetExisting(CommentsForIncabGetExistingDocument commentsForIncabGetExisting232) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[52].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/CommentsForIncabGetExisting");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), commentsForIncabGetExisting232,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "commentsForIncabGetExisting")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CommentsForIncabGetExisting"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), CommentsForIncabGetExistingResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (CommentsForIncabGetExistingResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabGetExisting"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabGetExisting"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabGetExisting"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Incab Comments - update
	 *
	 * @see WSCollExternal#commentsForIncabUpdate
	 * @param commentsForIncabUpdate156
	 */
	@Override
	public CommentsForIncabUpdateResponseDocument commentsForIncabUpdate(CommentsForIncabUpdateDocument commentsForIncabUpdate156) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[14].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/CommentsForIncabUpdate");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), commentsForIncabUpdate156,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "commentsForIncabUpdate")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CommentsForIncabUpdate"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), CommentsForIncabUpdateResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (CommentsForIncabUpdateResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabUpdate"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabUpdate"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabUpdate"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns the number of bins on a given
	 * service for a UPRN
	 *
	 * @see WSCollExternal#countBinsForService
	 * @param countBinsForService140
	 */
	@Override
	public CountBinsForServiceResponseDocument countBinsForService(CountBinsForServiceDocument countBinsForService140) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[6].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/countBinsForService");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), countBinsForService140,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countBinsForService")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countBinsForService"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), CountBinsForServiceResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (CountBinsForServiceResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countBinsForService"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countBinsForService"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countBinsForService"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Counts the number of Garden subscriptions
	 * between start and end date
	 *
	 * @see WSCollExternal#countSubscriptions
	 * @param countSubscriptions228
	 */
	@Override
	public CountSubscriptionsResponseDocument countSubscriptions(CountSubscriptionsDocument countSubscriptions228) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[50].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/countSubscriptions");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), countSubscriptions228,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countSubscriptions")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countSubscriptions"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), CountSubscriptionsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (CountSubscriptionsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countSubscriptions"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countSubscriptions"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countSubscriptions"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Deletes a Garden Bin By id or name for a
	 * UPRN - **Jadu
	 *
	 * @see WSCollExternal#deleteBin
	 * @param deleteBin202
	 */
	@Override
	public DeleteBinResponseDocument deleteBin(DeleteBinDocument deleteBin202) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[37].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/DeleteBin");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), deleteBin202,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "deleteBin")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "DeleteBin"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), DeleteBinResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (DeleteBinResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "DeleteBin"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "DeleteBin"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "DeleteBin"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	public org.apache.xmlbeans.XmlObject fromOM(org.apache.axiom.om.OMElement param, java.lang.Class type) throws org.apache.axis2.AxisFault {
		try {

			if (AHPOrderNewBagsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPOrderNewBagsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPOrderNewBagsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPOrderNewBagsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetPotentialCollectionDatesForNewGardenSubscriberDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetPotentialCollectionDatesForNewGardenSubscriberDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundCalendarForUPRNDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundCalendarForUPRNDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundCalendarForUPRNResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundCalendarForUPRNResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetNoCollectionDatesDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetNoCollectionDatesDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetNoCollectionDatesResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetNoCollectionDatesResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPSuspendSubscriptionDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPSuspendSubscriptionDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPSuspendSubscriptionResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPSuspendSubscriptionResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenSubscriptionWithNamePhoneEmailDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenSubscriptionWithNamePhoneEmailDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenSubscriptionWithNamePhoneEmailResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenSubscriptionWithNamePhoneEmailResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CountBinsForServiceDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CountBinsForServiceDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CountBinsForServiceResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CountBinsForServiceResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (InsertNewBinDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return InsertNewBinDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (InsertNewBinResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return InsertNewBinResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundChangesInDateRangeByRoundOrUPRNDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundChangesInDateRangeByRoundOrUPRNDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (ReadWriteMissedBinDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return ReadWriteMissedBinDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (ReadWriteMissedBinResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return ReadWriteMissedBinResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (QueryBinOnTypeDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return QueryBinOnTypeDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (QueryBinOnTypeResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return QueryBinOnTypeResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenSubscriptionDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenSubscriptionDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenSubscriptionResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenSubscriptionResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetLatestTruckPositionsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetLatestTruckPositionsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetLatestTruckPositionsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetLatestTruckPositionsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAddressOrUPRNDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAddressOrUPRNDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAddressOrUPRNResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAddressOrUPRNResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CommentsForIncabUpdateDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CommentsForIncabUpdateDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CommentsForIncabUpdateResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CommentsForIncabUpdateResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenRemoveSubscriptionDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenRemoveSubscriptionDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenRemoveSubscriptionResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenRemoveSubscriptionResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAddressFromPostcodeDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAddressFromPostcodeDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAddressFromPostcodeResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAddressFromPostcodeResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (BinDeleteDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return BinDeleteDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (BinDeleteResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return BinDeleteResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAllIssuesForIssueTypeDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAllIssuesForIssueTypeDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAllIssuesForIssueTypeResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAllIssuesForIssueTypeResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (UpdateAssistedDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return UpdateAssistedDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (UpdateAssistedResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return UpdateAssistedResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAllRoundDetailsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAllRoundDetailsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAllRoundDetailsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAllRoundDetailsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (BinUpdateDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return BinUpdateDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (BinUpdateResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return BinUpdateResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (BinInsertDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return BinInsertDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (BinInsertResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return BinInsertResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LargeHouseholdsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LargeHouseholdsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LargeHouseholdsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LargeHouseholdsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAvailableContainersDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAvailableContainersDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAvailableContainersResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAvailableContainersResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (UpdateBinDetailsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return UpdateBinDetailsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (UpdateBinDetailsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return UpdateBinDetailsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LLPGXtraUpdateGetAvailableFieldDetailsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LLPGXtraUpdateGetAvailableFieldDetailsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenSubscribersDeliveriesCollectionsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenSubscribersDeliveriesCollectionsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenSubscribersDeliveriesCollectionsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenSubscribersDeliveriesCollectionsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetZoneBagAssistDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetZoneBagAssistDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetZoneBagAssistResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetZoneBagAssistResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenSubscriptionGetDetailsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenSubscriptionGetDetailsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenSubscriptionGetDetailsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenSubscriptionGetDetailsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenChangeAddressDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenChangeAddressDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GardenChangeAddressResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GardenChangeAddressResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LastCollectedDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LastCollectedDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LastCollectedResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LastCollectedResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AAHelloWorldStringTestDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AAHelloWorldStringTestDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AAHelloWorldStringTestResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AAHelloWorldStringTestResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetIssuesForUPRNDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetIssuesForUPRNDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetIssuesForUPRNResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetIssuesForUPRNResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetIssuesDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetIssuesDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetIssuesResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetIssuesResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CheckAssistedDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CheckAssistedDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CheckAssistedResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CheckAssistedResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LLPGXtraGetUPRNCurrentValuesForDBFieldDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (DeleteBinDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return DeleteBinDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (DeleteBinResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return DeleteBinResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPChangeAddressDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPChangeAddressDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPChangeAddressResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPChangeAddressResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (ShowAdditionalBinsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return ShowAdditionalBinsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (ShowAdditionalBinsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return ShowAdditionalBinsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (QueryBinEndDatesDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return QueryBinEndDatesDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (QueryBinEndDatesResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return QueryBinEndDatesResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (WriteIncabLiveDataDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return WriteIncabLiveDataDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (WriteIncabLiveDataResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return WriteIncabLiveDataResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAvailableRoundsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAvailableRoundsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAvailableRoundsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAvailableRoundsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAvailableContainersForClientOrPropertyDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAvailableContainersForClientOrPropertyDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAvailableContainersForClientOrPropertyResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAvailableContainersForClientOrPropertyResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPGetDetailsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPGetDetailsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPGetDetailsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPGetDetailsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAllUPRNsForDateDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAllUPRNsForDateDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetAllUPRNsForDateResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetAllUPRNsForDateResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetTrucksOutTodayDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetTrucksOutTodayDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetTrucksOutTodayResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetTrucksOutTodayResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPNewUpdateDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPNewUpdateDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPNewUpdateResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPNewUpdateResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LLPGXtraUpdateGetUPRNForDBFieldDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LLPGXtraUpdateGetUPRNForDBFieldDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (ReadWriteMissedBinByContainerTypeDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return ReadWriteMissedBinByContainerTypeDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (ReadWriteMissedBinByContainerTypeResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return ReadWriteMissedBinByContainerTypeResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CountSubscriptionsDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CountSubscriptionsDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CountSubscriptionsResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CountSubscriptionsResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetBinDetailsForServiceDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetBinDetailsForServiceDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetBinDetailsForServiceResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetBinDetailsForServiceResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CommentsForIncabGetExistingDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CommentsForIncabGetExistingDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CommentsForIncabGetExistingResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CommentsForIncabGetExistingResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetInCabIssueTextAndIssueCodeDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetInCabIssueTextAndIssueCodeDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetInCabIssueTextAndIssueCodeResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetInCabIssueTextAndIssueCodeResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (KeepAliveCallDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return KeepAliveCallDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (KeepAliveCallResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return KeepAliveCallResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AAHelloWorldXMLTestDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AAHelloWorldXMLTestDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AAHelloWorldXMLTestResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AAHelloWorldXMLTestResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPRemoveSubscriptionDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPRemoveSubscriptionDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPRemoveSubscriptionResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPRemoveSubscriptionResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundForUPRNDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundForUPRNDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundForUPRNResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundForUPRNResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetLastTruckPositionDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetLastTruckPositionDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetLastTruckPositionResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetLastTruckPositionResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CachedCalendarDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CachedCalendarDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (CachedCalendarResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return CachedCalendarResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundNameForUPRNServiceDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundNameForUPRNServiceDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetRoundNameForUPRNServiceResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetRoundNameForUPRNServiceResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPExtendSubscriptionDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPExtendSubscriptionDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (AHPExtendSubscriptionResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return AHPExtendSubscriptionResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetIssuesAndCollectionStatusForUPRNDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetIssuesAndCollectionStatusForUPRNDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

			if (GetIssuesAndCollectionStatusForUPRNResponseDocument.class.equals(type)) {
				org.apache.axiom.om.OMXMLStreamReaderConfiguration configuration = new org.apache.axiom.om.OMXMLStreamReaderConfiguration();
				configuration.setPreserveNamespaceContext(true);
				return GetIssuesAndCollectionStatusForUPRNResponseDocument.Factory.parse(param.getXMLStreamReader(false, configuration));
			}

		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
		return null;
	}

	/**
	 * Auto generated method signature Garden Webservice - Moves Garden detail
	 * from one UPRN to another
	 *
	 * @see WSCollExternal#garden_ChangeAddress
	 * @param garden_ChangeAddress188
	 */
	@Override
	public GardenChangeAddressResponseDocument garden_ChangeAddress(GardenChangeAddressDocument garden_ChangeAddress188) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[30].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/Garden_ChangeAddress");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), garden_ChangeAddress188,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_ChangeAddress")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Garden_ChangeAddress"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GardenChangeAddressResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GardenChangeAddressResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_ChangeAddress"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_ChangeAddress"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_ChangeAddress"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Garden Webservice - remove Subscription
	 * and set end Date of Container to the supplied end date. IF end date is
	 * blank, then todays date will be used.
	 *
	 * @see WSCollExternal#garden_RemoveSubscription
	 * @param garden_RemoveSubscription158
	 */
	@Override
	public GardenRemoveSubscriptionResponseDocument garden_RemoveSubscription(GardenRemoveSubscriptionDocument garden_RemoveSubscription158) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[15].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/Garden_RemoveSubscription");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), garden_RemoveSubscription158,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_RemoveSubscription")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Garden_RemoveSubscription"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GardenRemoveSubscriptionResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GardenRemoveSubscriptionResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_RemoveSubscription"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_RemoveSubscription"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_RemoveSubscription"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Garden Subscribers - Get all current
	 * subscribers, Garden Deliveries /Collections on a certain date.
	 *
	 * @see WSCollExternal#garden_Subscribers_Deliveries_Collections
	 * @param garden_Subscribers_Deliveries_Collections182
	 */
	@Override
	public GardenSubscribersDeliveriesCollectionsResponseDocument garden_Subscribers_Deliveries_Collections(GardenSubscribersDeliveriesCollectionsDocument garden_Subscribers_Deliveries_Collections182)
			throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[27].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/Garden_Subscribers_Deliveries_Collections");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), garden_Subscribers_Deliveries_Collections182,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_Subscribers_Deliveries_Collections")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Garden_Subscribers_Deliveries_Collections"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GardenSubscribersDeliveriesCollectionsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GardenSubscribersDeliveriesCollectionsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_Subscribers_Deliveries_Collections"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_Subscribers_Deliveries_Collections"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_Subscribers_Deliveries_Collections"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Functionality for a new Garden
	 * Subscription / or update existing subscription
	 *
	 * @see WSCollExternal#gardenSubscription
	 * @param gardenSubscription150
	 */
	@Override
	public GardenSubscriptionResponseDocument gardenSubscription(GardenSubscriptionDocument gardenSubscription150) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[11].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GardenSubscription");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), gardenSubscription150,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscription"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GardenSubscriptionResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GardenSubscriptionResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Garden Webservice - Get Garden
	 * Subscription details for UPRN
	 *
	 * @see WSCollExternal#gardenSubscription_GetDetails
	 * @param gardenSubscription_GetDetails186
	 */
	@Override
	public GardenSubscriptionGetDetailsResponseDocument gardenSubscription_GetDetails(GardenSubscriptionGetDetailsDocument gardenSubscription_GetDetails186) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[29].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GardenSubscription_GetDetails");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), gardenSubscription_GetDetails186,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription_GetDetails")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscription_GetDetails"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GardenSubscriptionGetDetailsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GardenSubscriptionGetDetailsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_GetDetails"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_GetDetails"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_GetDetails"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Functionality for a new Garden
	 * Subscription / or update existing subscription includes Name Phone and
	 * Email
	 *
	 * @see WSCollExternal#gardenSubscription_WithNamePhoneEmail
	 * @param gardenSubscription_WithNamePhoneEmail138
	 */
	@Override
	public GardenSubscriptionWithNamePhoneEmailResponseDocument gardenSubscription_WithNamePhoneEmail(GardenSubscriptionWithNamePhoneEmailDocument gardenSubscription_WithNamePhoneEmail138)
			throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[5].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GardenSubscription_WithNamePhoneEmail");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), gardenSubscription_WithNamePhoneEmail138,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription_WithNamePhoneEmail")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscription_WithNamePhoneEmail"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GardenSubscriptionWithNamePhoneEmailResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GardenSubscriptionWithNamePhoneEmailResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_WithNamePhoneEmail"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_WithNamePhoneEmail"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_WithNamePhoneEmail"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns matching addresses from given
	 * address details
	 *
	 * @see WSCollExternal#getAddressFromPostcode
	 * @param getAddressFromPostcode160
	 */
	@Override
	public GetAddressFromPostcodeResponseDocument getAddressFromPostcode(GetAddressFromPostcodeDocument getAddressFromPostcode160) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[16].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getAddressFromPostcode");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAddressFromPostcode160,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressFromPostcode")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressFromPostcode"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetAddressFromPostcodeResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetAddressFromPostcodeResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAddressFromPostcode"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAddressFromPostcode"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAddressFromPostcode"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns and Address from a UPRN or
	 * matching UPRNs from address details
	 *
	 * @see WSCollExternal#getAddressOrUPRN
	 * @param getAddressOrUPRN154
	 */
	@Override
	public GetAddressOrUPRNResponseDocument getAddressOrUPRN(GetAddressOrUPRNDocument getAddressOrUPRN154) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[13].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAddressOrUPRN");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAddressOrUPRN154,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressOrUPRN")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAddressOrUPRN"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetAddressOrUPRNResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetAddressOrUPRNResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAddressOrUPRN"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAddressOrUPRN"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAddressOrUPRN"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns all issues of the issueType
	 * between start and End dates
	 *
	 * @see WSCollExternal#getAllIssuesForIssueType
	 * @param getAllIssuesForIssueType164
	 */
	@Override
	public GetAllIssuesForIssueTypeResponseDocument getAllIssuesForIssueType(GetAllIssuesForIssueTypeDocument getAllIssuesForIssueType164) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[18].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getAllIssuesForIssueType");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAllIssuesForIssueType164,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllIssuesForIssueType")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllIssuesForIssueType"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetAllIssuesForIssueTypeResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetAllIssuesForIssueTypeResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAllIssuesForIssueType"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAllIssuesForIssueType"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAllIssuesForIssueType"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns All rounds for every UPRN
	 *
	 * @see WSCollExternal#getAllRoundDetails
	 * @param getAllRoundDetails168
	 */
	@Override
	public GetAllRoundDetailsResponseDocument getAllRoundDetails(GetAllRoundDetailsDocument getAllRoundDetails168) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[20].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAllRoundDetails");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAllRoundDetails168,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllRoundDetails")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAllRoundDetails"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetAllRoundDetailsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetAllRoundDetailsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllRoundDetails"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllRoundDetails"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllRoundDetails"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns All UPRNs and round information
	 * of properties collected on this date - does not currently cater for
	 * complex trade parameters
	 *
	 * @see WSCollExternal#getAllUPRNsForDate
	 * @param getAllUPRNsForDate218
	 */
	@Override
	public GetAllUPRNsForDateResponseDocument getAllUPRNsForDate(GetAllUPRNsForDateDocument getAllUPRNsForDate218) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[45].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAllUPRNsForDate");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAllUPRNsForDate218,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllUPRNsForDate")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAllUPRNsForDate"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetAllUPRNsForDateResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetAllUPRNsForDateResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllUPRNsForDate"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllUPRNsForDate"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllUPRNsForDate"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Please use
	 * GetAvailableContainersForClientOrProperty as a better and updated
	 * alternative. Returns all current known container types in the database.
	 *
	 * @see WSCollExternal#getAvailableContainers
	 * @param getAvailableContainers176
	 */
	@Override
	public GetAvailableContainersResponseDocument getAvailableContainers(GetAvailableContainersDocument getAvailableContainers176) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[24].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAvailableContainers");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAvailableContainers176,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableContainers")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableContainers"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetAvailableContainersResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetAvailableContainersResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainers"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainers"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainers"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns all current known container types
	 * in the database for a client or (if a UPRN is supplied) at a property
	 *
	 * @see WSCollExternal#getAvailableContainersForClientOrProperty
	 * @param getAvailableContainersForClientOrProperty214
	 */
	@Override
	public GetAvailableContainersForClientOrPropertyResponseDocument getAvailableContainersForClientOrProperty(
			GetAvailableContainersForClientOrPropertyDocument getAvailableContainersForClientOrProperty214) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[43].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAvailableContainersForClientOrProperty");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAvailableContainersForClientOrProperty214,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableContainersForClientOrProperty")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableContainersForClientOrProperty"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetAvailableContainersForClientOrPropertyResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetAvailableContainersForClientOrPropertyResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainersForClientOrProperty"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainersForClientOrProperty"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainersForClientOrProperty"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns all current known rounds in the
	 * database
	 *
	 * @see WSCollExternal#getAvailableRounds
	 * @param getAvailableRounds212
	 */
	@Override
	public GetAvailableRoundsResponseDocument getAvailableRounds(GetAvailableRoundsDocument getAvailableRounds212) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[42].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAvailableRounds");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAvailableRounds212,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableRounds")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableRounds"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetAvailableRoundsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetAvailableRoundsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableRounds"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableRounds"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableRounds"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns container details for a service
	 * for a given UPRN. Includes ParentUPRN containers
	 *
	 * @see WSCollExternal#getBinDetailsForService
	 * @param getBinDetailsForService230
	 */
	@Override
	public GetBinDetailsForServiceResponseDocument getBinDetailsForService(GetBinDetailsForServiceDocument getBinDetailsForService230) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[51].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetBinDetailsForService");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getBinDetailsForService230,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getBinDetailsForService")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetBinDetailsForService"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetBinDetailsForServiceResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetBinDetailsForServiceResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetBinDetailsForService"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetBinDetailsForService"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetBinDetailsForService"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns a list of the current incab
	 * Issues Codes and texts for use when parsing issue messsages from Incab
	 *
	 * @see WSCollExternal#getInCabIssueTextAndIssueCode
	 * @param getInCabIssueTextAndIssueCode234
	 */
	@Override
	public GetInCabIssueTextAndIssueCodeResponseDocument getInCabIssueTextAndIssueCode(GetInCabIssueTextAndIssueCodeDocument getInCabIssueTextAndIssueCode234) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[53].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetInCabIssueTextAndIssueCode");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getInCabIssueTextAndIssueCode234,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getInCabIssueTextAndIssueCode")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetInCabIssueTextAndIssueCode"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetInCabIssueTextAndIssueCodeResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetInCabIssueTextAndIssueCodeResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetInCabIssueTextAndIssueCode"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetInCabIssueTextAndIssueCode"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetInCabIssueTextAndIssueCode"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns all issues for the populated
	 * filtered values on a date(ddMMyyyy) between a start and end time (HH:mm)
	 *
	 * @see WSCollExternal#getIssues
	 * @param getIssues196
	 */
	@Override
	public GetIssuesResponseDocument getIssues(GetIssuesDocument getIssues196) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[34].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetIssues");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getIssues196,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssues")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssues"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetIssuesResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetIssuesResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssues"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssues"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssues"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature **Requires Webaspx InCab. Returns latest
	 * issues and collection status from the last time a truck was out for each
	 * service for this UPRN
	 *
	 * @see WSCollExternal#getIssuesAndCollectionStatusForUPRN
	 * @param getIssuesAndCollectionStatusForUPRN254
	 */
	@Override
	public GetIssuesAndCollectionStatusForUPRNResponseDocument getIssuesAndCollectionStatusForUPRN(GetIssuesAndCollectionStatusForUPRNDocument getIssuesAndCollectionStatusForUPRN254)
			throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[63].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getIssuesAndCollectionStatusForUPRN");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getIssuesAndCollectionStatusForUPRN254,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssuesAndCollectionStatusForUPRN")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssuesAndCollectionStatusForUPRN"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetIssuesAndCollectionStatusForUPRNResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetIssuesAndCollectionStatusForUPRNResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getIssuesAndCollectionStatusForUPRN"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getIssuesAndCollectionStatusForUPRN"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getIssuesAndCollectionStatusForUPRN"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns all Issues for a given UPRN for a
	 * specific date ** Requires Webaspx InCab system ** DateReq in format
	 * ddMMyyyy
	 *
	 * @see WSCollExternal#getIssuesForUPRN
	 * @param getIssuesForUPRN194
	 */
	@Override
	public GetIssuesForUPRNResponseDocument getIssuesForUPRN(GetIssuesForUPRNDocument getIssuesForUPRN194) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[33].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetIssuesForUPRN");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getIssuesForUPRN194,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssuesForUPRN")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssuesForUPRN"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetIssuesForUPRNResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetIssuesForUPRNResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssuesForUPRN"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssuesForUPRN"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssuesForUPRN"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns last known truck position for a
	 * given registrations, date and time ** Requires Webaspx InCab System **
	 *
	 * @see WSCollExternal#getLastTruckPosition
	 * @param getLastTruckPosition244
	 */
	@Override
	public GetLastTruckPositionResponseDocument getLastTruckPosition(GetLastTruckPositionDocument getLastTruckPosition244) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[58].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetLastTruckPosition");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getLastTruckPosition244,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getLastTruckPosition")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetLastTruckPosition"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetLastTruckPositionResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetLastTruckPositionResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetLastTruckPosition"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetLastTruckPosition"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetLastTruckPosition"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns the current truck positions that
	 * are collecting this UPRN today ** Requires Webaspx InCab System **
	 *
	 * @see WSCollExternal#getLatestTruckPositions
	 * @param getLatestTruckPositions152
	 */
	@Override
	public GetLatestTruckPositionsResponseDocument getLatestTruckPositions(GetLatestTruckPositionsDocument getLatestTruckPositions152) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[12].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getLatestTruckPositions");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getLatestTruckPositions152,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getLatestTruckPositions")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getLatestTruckPositions"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetLatestTruckPositionsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetLatestTruckPositionsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getLatestTruckPositions"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getLatestTruckPositions"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getLatestTruckPositions"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature The main use of this webservice is to
	 * provide the current No Collection Dates for a suspended service, such as
	 * no Garden collection through winter. This will provide the date the
	 * Garden Suspension started and the date the garden suspension ends
	 * relevant to todays date. It will also supply details of the no collect
	 * dates relevant to the parameters supplied. Typically returned on UPRN
	 * level, but can be called for All, Service or Roundname. (Default is All)
	 *
	 * @see WSCollExternal#getNoCollectionDates
	 * @param getNoCollectionDates134
	 */
	@Override
	public GetNoCollectionDatesResponseDocument getNoCollectionDates(GetNoCollectionDatesDocument getNoCollectionDates134) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[3].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getNoCollectionDates");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getNoCollectionDates134,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getNoCollectionDates")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getNoCollectionDates"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetNoCollectionDatesResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetNoCollectionDatesResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getNoCollectionDates"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getNoCollectionDates"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getNoCollectionDates"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns the next collection dates for a
	 * potential garden subscriber
	 *
	 * @see WSCollExternal#getPotentialCollectionDatesForNewGardenSubscriber
	 * @param getPotentialCollectionDatesForNewGardenSubscriber130
	 */
	@Override
	public GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument getPotentialCollectionDatesForNewGardenSubscriber(
			GetPotentialCollectionDatesForNewGardenSubscriberDocument getPotentialCollectionDatesForNewGardenSubscriber130) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getPotentialCollectionDatesForNewGardenSubscriber");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getPotentialCollectionDatesForNewGardenSubscriber130,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getPotentialCollectionDatesForNewGardenSubscriber")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getPotentialCollectionDatesForNewGardenSubscriber"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getPotentialCollectionDatesForNewGardenSubscriber"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap
								.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getPotentialCollectionDatesForNewGardenSubscriber"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getPotentialCollectionDatesForNewGardenSubscriber"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns the collections dates for each
	 * container for a particular UPRn inbetween start and end dates. **Leave
	 * BinID blank as historical unused parameter
	 *
	 * @see WSCollExternal#getRoundAndBinInfoForUPRNForNewAdjustedDates
	 * @param getRoundAndBinInfoForUPRNForNewAdjustedDates248
	 */
	@Override
	public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument getRoundAndBinInfoForUPRNForNewAdjustedDates(
			GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument getRoundAndBinInfoForUPRNForNewAdjustedDates248) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[60].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getRoundAndBinInfoForUPRNForNewAdjustedDates");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundAndBinInfoForUPRNForNewAdjustedDates248,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundAndBinInfoForUPRNForNewAdjustedDates")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundAndBinInfoForUPRNForNewAdjustedDates"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundAndBinInfoForUPRNForNewAdjustedDates"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap
								.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundAndBinInfoForUPRNForNewAdjustedDates"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundAndBinInfoForUPRNForNewAdjustedDates"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns html of Friendly Calendar info
	 * and html table of calendar)
	 *
	 * @see WSCollExternal#getRoundCalendarForUPRN
	 * @param getRoundCalendarForUPRN132
	 */
	@Override
	public GetRoundCalendarForUPRNResponseDocument getRoundCalendarForUPRN(GetRoundCalendarForUPRNDocument getRoundCalendarForUPRN132) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getRoundCalendarForUPRN");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundCalendarForUPRN132,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundCalendarForUPRN")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundCalendarForUPRN"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetRoundCalendarForUPRNResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetRoundCalendarForUPRNResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundCalendarForUPRN"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundCalendarForUPRN"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundCalendarForUPRN"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns details of any round changes made
	 * within a date range for a round or UPRN
	 *
	 * @see WSCollExternal#getRoundChangesInDateRangeByRoundOrUPRN
	 * @param getRoundChangesInDateRangeByRoundOrUPRN144
	 */
	@Override
	public GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument getRoundChangesInDateRangeByRoundOrUPRN(GetRoundChangesInDateRangeByRoundOrUPRNDocument getRoundChangesInDateRangeByRoundOrUPRN144)
			throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[8].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetRoundChangesInDateRangeByRoundOrUPRN");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundChangesInDateRangeByRoundOrUPRN144,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundChangesInDateRangeByRoundOrUPRN")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetRoundChangesInDateRangeByRoundOrUPRN"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetRoundChangesInDateRangeByRoundOrUPRN"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetRoundChangesInDateRangeByRoundOrUPRN"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetRoundChangesInDateRangeByRoundOrUPRN"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature returns next collection for each service
	 * this UPRN from a given start date
	 *
	 * @see WSCollExternal#getRoundForUPRN
	 * @param getRoundForUPRN242
	 */
	@Override
	public GetRoundForUPRNResponseDocument getRoundForUPRN(GetRoundForUPRNDocument getRoundForUPRN242) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[57].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getRoundForUPRN");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundForUPRN242,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundForUPRN")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundForUPRN"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetRoundForUPRNResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetRoundForUPRNResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundForUPRN"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundForUPRN"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundForUPRN"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns the relevant roundname that
	 * collects this UPRN and 3 letter abbreviated service - Ref Rec Grn Gls Pls
	 * Box TRf TRc
	 *
	 * @see WSCollExternal#getRoundNameForUPRNService
	 * @param getRoundNameForUPRNService250
	 */
	@Override
	public GetRoundNameForUPRNServiceResponseDocument getRoundNameForUPRNService(GetRoundNameForUPRNServiceDocument getRoundNameForUPRNService250) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[61].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getRoundNameForUPRNService");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundNameForUPRNService250,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundNameForUPRNService")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundNameForUPRNService"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetRoundNameForUPRNServiceResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetRoundNameForUPRNServiceResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundNameForUPRNService"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundNameForUPRNService"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundNameForUPRNService"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature ** Requires Webaspx InCab. Returns the
	 * registrations of the vehicles out on the given date(ddMMyyyy) after the
	 * given timeReq
	 *
	 * @see WSCollExternal#getTrucksOutToday
	 * @param getTrucksOutToday220
	 */
	@Override
	public GetTrucksOutTodayResponseDocument getTrucksOutToday(GetTrucksOutTodayDocument getTrucksOutToday220) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[46].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetTrucksOutToday");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getTrucksOutToday220,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getTrucksOutToday")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetTrucksOutToday"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetTrucksOutTodayResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetTrucksOutTodayResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetTrucksOutToday"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetTrucksOutToday"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetTrucksOutToday"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns the Zone AssistedStatus and bag
	 * indicator for a UPRN - used for certain clients CRM generated garden
	 * service
	 *
	 * @see WSCollExternal#getZoneBagAssist
	 * @param getZoneBagAssist184
	 */
	@Override
	public GetZoneBagAssistResponseDocument getZoneBagAssist(GetZoneBagAssistDocument getZoneBagAssist184) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[28].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetZoneBagAssist");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getZoneBagAssist184,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getZoneBagAssist")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetZoneBagAssist"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), GetZoneBagAssistResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (GetZoneBagAssistResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetZoneBagAssist"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetZoneBagAssist"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetZoneBagAssist"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Inserts a 240L Garden bin with supplied
	 * parameters. For a specific bin use BinInsert webservice
	 *
	 * @see WSCollExternal#insertNewBin
	 * @param insertNewBin142
	 */
	@Override
	public InsertNewBinResponseDocument insertNewBin(InsertNewBinDocument insertNewBin142) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[7].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/insertNewBin");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), insertNewBin142,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "insertNewBin")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "insertNewBin"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), InsertNewBinResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (InsertNewBinResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "insertNewBin"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "insertNewBin"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "insertNewBin"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature ** Internal Webaspx Use keeps database
	 * awake
	 *
	 * @see WSCollExternal#keepAliveCall
	 * @param keepAliveCall236
	 */
	@Override
	public KeepAliveCallResponseDocument keepAliveCall(KeepAliveCallDocument keepAliveCall236) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[54].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/KeepAliveCall");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), keepAliveCall236,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "keepAliveCall")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "KeepAliveCall"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), KeepAliveCallResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (KeepAliveCallResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "KeepAliveCall"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "KeepAliveCall"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "KeepAliveCall"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns UPRN, Address details and count
	 * of this binType for all UPRNs with Start and End Dates
	 *
	 * @see WSCollExternal#largeHouseholds
	 * @param largeHouseholds174
	 */
	@Override
	public LargeHouseholdsResponseDocument largeHouseholds(LargeHouseholdsDocument largeHouseholds174) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[23].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LargeHouseholds");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), largeHouseholds174,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "largeHouseholds")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LargeHouseholds"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), LargeHouseholdsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (LargeHouseholdsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LargeHouseholds"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LargeHouseholds"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LargeHouseholds"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature **Requires Webaspx InCab. Returns latest
	 * issues and collection status from the last time a truck was out for each
	 * service for this UPRN. Also supplies GPS trial for each truck
	 *
	 * @see WSCollExternal#lastCollected
	 * @param lastCollected190
	 */
	@Override
	public LastCollectedResponseDocument lastCollected(LastCollectedDocument lastCollected190) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[31].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LastCollected");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), lastCollected190,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lastCollected")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LastCollected"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), LastCollectedResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (LastCollectedResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LastCollected"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LastCollected"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LastCollected"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns current values for the given
	 * databaseFieldName for a hash delimited list of UPRNs
	 *
	 * @see WSCollExternal#lLPGXtraGetUPRNCurrentValuesForDBField
	 * @param lLPGXtraGetUPRNCurrentValuesForDBField200
	 */
	@Override
	public LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument lLPGXtraGetUPRNCurrentValuesForDBField(LLPGXtraGetUPRNCurrentValuesForDBFieldDocument lLPGXtraGetUPRNCurrentValuesForDBField200)
			throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[36].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LLPGXtraGetUPRNCurrentValuesForDBField");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), lLPGXtraGetUPRNCurrentValuesForDBField200,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraGetUPRNCurrentValuesForDBField")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LLPGXtraGetUPRNCurrentValuesForDBField"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraGetUPRNCurrentValuesForDBField"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraGetUPRNCurrentValuesForDBField"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraGetUPRNCurrentValuesForDBField"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns a list of editable LLPGXtra
	 * fields by the LLPGXtraUpdateGetUPRNForDBField webservice
	 *
	 * @see WSCollExternal#lLPGXtraUpdateGetAvailableFieldDetails
	 * @param lLPGXtraUpdateGetAvailableFieldDetails180
	 */
	@Override
	public LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument lLPGXtraUpdateGetAvailableFieldDetails(LLPGXtraUpdateGetAvailableFieldDetailsDocument lLPGXtraUpdateGetAvailableFieldDetails180)
			throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[26].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LLPGXtraUpdateGetAvailableFieldDetails");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), lLPGXtraUpdateGetAvailableFieldDetails180,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraUpdateGetAvailableFieldDetails")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LLPGXtraUpdateGetAvailableFieldDetails"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetAvailableFieldDetails"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetAvailableFieldDetails"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetAvailableFieldDetails"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Updates the LLPGXtra databaseFieldName
	 * for a hash delimited list of UPRNs with the given newValue
	 *
	 * @see WSCollExternal#lLPGXtraUpdateGetUPRNForDBField
	 * @param lLPGXtraUpdateGetUPRNForDBField224
	 */
	@Override
	public LLPGXtraUpdateGetUPRNForDBFieldResponseDocument lLPGXtraUpdateGetUPRNForDBField(LLPGXtraUpdateGetUPRNForDBFieldDocument lLPGXtraUpdateGetUPRNForDBField224) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[48].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LLPGXtraUpdateGetUPRNForDBField");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), lLPGXtraUpdateGetUPRNForDBField224,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraUpdateGetUPRNForDBField")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LLPGXtraUpdateGetUPRNForDBField"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetUPRNForDBField"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetUPRNForDBField"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetUPRNForDBField"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	private boolean optimizeContent(javax.xml.namespace.QName opName) {

		if (opNameArray == null) {
			return false;
		}
		for (QName element : opNameArray) {
			if (opName.equals(element)) {
				return true;
			}
		}
		return false;
	}
	// https://collections-rugby.azurewebsites.net/WSCollExternal.asmx

	private void populateAxisService() throws org.apache.axis2.AxisFault {

		// creating the Service with a unique name
		_service = new org.apache.axis2.description.AxisService("WSCollExternal" + getUniqueSuffix());
		addAnonymousOperations();

		// creating the operations
		org.apache.axis2.description.AxisOperation __operation;

		_operations = new org.apache.axis2.description.AxisOperation[64];

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_OrderNewBags"));
		_service.addOperation(__operation);

		_operations[0] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getPotentialCollectionDatesForNewGardenSubscriber"));
		_service.addOperation(__operation);

		_operations[1] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundCalendarForUPRN"));
		_service.addOperation(__operation);

		_operations[2] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getNoCollectionDates"));
		_service.addOperation(__operation);

		_operations[3] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_SuspendSubscription"));
		_service.addOperation(__operation);

		_operations[4] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription_WithNamePhoneEmail"));
		_service.addOperation(__operation);

		_operations[5] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countBinsForService"));
		_service.addOperation(__operation);

		_operations[6] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "insertNewBin"));
		_service.addOperation(__operation);

		_operations[7] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundChangesInDateRangeByRoundOrUPRN"));
		_service.addOperation(__operation);

		_operations[8] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBin"));
		_service.addOperation(__operation);

		_operations[9] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "queryBinOnType"));
		_service.addOperation(__operation);

		_operations[10] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription"));
		_service.addOperation(__operation);

		_operations[11] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getLatestTruckPositions"));
		_service.addOperation(__operation);

		_operations[12] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressOrUPRN"));
		_service.addOperation(__operation);

		_operations[13] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "commentsForIncabUpdate"));
		_service.addOperation(__operation);

		_operations[14] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_RemoveSubscription"));
		_service.addOperation(__operation);

		_operations[15] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressFromPostcode"));
		_service.addOperation(__operation);

		_operations[16] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binDelete"));
		_service.addOperation(__operation);

		_operations[17] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllIssuesForIssueType"));
		_service.addOperation(__operation);

		_operations[18] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "updateAssisted"));
		_service.addOperation(__operation);

		_operations[19] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllRoundDetails"));
		_service.addOperation(__operation);

		_operations[20] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binUpdate"));
		_service.addOperation(__operation);

		_operations[21] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binInsert"));
		_service.addOperation(__operation);

		_operations[22] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "largeHouseholds"));
		_service.addOperation(__operation);

		_operations[23] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableContainers"));
		_service.addOperation(__operation);

		_operations[24] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "updateBinDetails"));
		_service.addOperation(__operation);

		_operations[25] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraUpdateGetAvailableFieldDetails"));
		_service.addOperation(__operation);

		_operations[26] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_Subscribers_Deliveries_Collections"));
		_service.addOperation(__operation);

		_operations[27] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getZoneBagAssist"));
		_service.addOperation(__operation);

		_operations[28] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription_GetDetails"));
		_service.addOperation(__operation);

		_operations[29] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_ChangeAddress"));
		_service.addOperation(__operation);

		_operations[30] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lastCollected"));
		_service.addOperation(__operation);

		_operations[31] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aA_HelloWorld_String_Test"));
		_service.addOperation(__operation);

		_operations[32] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssuesForUPRN"));
		_service.addOperation(__operation);

		_operations[33] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssues"));
		_service.addOperation(__operation);

		_operations[34] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "checkAssisted"));
		_service.addOperation(__operation);

		_operations[35] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraGetUPRNCurrentValuesForDBField"));
		_service.addOperation(__operation);

		_operations[36] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "deleteBin"));
		_service.addOperation(__operation);

		_operations[37] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_ChangeAddress"));
		_service.addOperation(__operation);

		_operations[38] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "showAdditionalBins"));
		_service.addOperation(__operation);

		_operations[39] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "queryBinEndDates"));
		_service.addOperation(__operation);

		_operations[40] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "writeIncabLiveData"));
		_service.addOperation(__operation);

		_operations[41] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableRounds"));
		_service.addOperation(__operation);

		_operations[42] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableContainersForClientOrProperty"));
		_service.addOperation(__operation);

		_operations[43] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_GetDetails"));
		_service.addOperation(__operation);

		_operations[44] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllUPRNsForDate"));
		_service.addOperation(__operation);

		_operations[45] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getTrucksOutToday"));
		_service.addOperation(__operation);

		_operations[46] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_NewUpdate"));
		_service.addOperation(__operation);

		_operations[47] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraUpdateGetUPRNForDBField"));
		_service.addOperation(__operation);

		_operations[48] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBinByContainerType"));
		_service.addOperation(__operation);

		_operations[49] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countSubscriptions"));
		_service.addOperation(__operation);

		_operations[50] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getBinDetailsForService"));
		_service.addOperation(__operation);

		_operations[51] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "commentsForIncabGetExisting"));
		_service.addOperation(__operation);

		_operations[52] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getInCabIssueTextAndIssueCode"));
		_service.addOperation(__operation);

		_operations[53] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "keepAliveCall"));
		_service.addOperation(__operation);

		_operations[54] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aA_HelloWorld_XML_Test"));
		_service.addOperation(__operation);

		_operations[55] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_RemoveSubscription"));
		_service.addOperation(__operation);

		_operations[56] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundForUPRN"));
		_service.addOperation(__operation);

		_operations[57] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getLastTruckPosition"));
		_service.addOperation(__operation);

		_operations[58] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "cachedCalendar"));
		_service.addOperation(__operation);

		_operations[59] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundAndBinInfoForUPRNForNewAdjustedDates"));
		_service.addOperation(__operation);

		_operations[60] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundNameForUPRNService"));
		_service.addOperation(__operation);

		_operations[61] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_ExtendSubscription"));
		_service.addOperation(__operation);

		_operations[62] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssuesAndCollectionStatusForUPRN"));
		_service.addOperation(__operation);

		_operations[63] = __operation;
	}

	// populates the faults
	private void populateFaults() {
	}

	/**
	 * Auto generated method signature Returns all live containers inbetween
	 * given dates
	 *
	 * @see WSCollExternal#queryBinEndDates
	 * @param queryBinEndDates208
	 */
	@Override
	public QueryBinEndDatesResponseDocument queryBinEndDates(QueryBinEndDatesDocument queryBinEndDates208) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[40].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/QueryBinEndDates");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), queryBinEndDates208,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "queryBinEndDates")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "QueryBinEndDates"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), QueryBinEndDatesResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (QueryBinEndDatesResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinEndDates"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinEndDates"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinEndDates"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns all live containers of a certain
	 * type
	 *
	 * @see WSCollExternal#queryBinOnType
	 * @param queryBinOnType148
	 */
	@Override
	public QueryBinOnTypeResponseDocument queryBinOnType(QueryBinOnTypeDocument queryBinOnType148) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[10].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/QueryBinOnType");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), queryBinOnType148,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "queryBinOnType")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "QueryBinOnType"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), QueryBinOnTypeResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (QueryBinOnTypeResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinOnType"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinOnType"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinOnType"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Sets missed bin details by UPRN and
	 * service
	 *
	 * @see WSCollExternal#readWriteMissedBin
	 * @param readWriteMissedBin146
	 */
	@Override
	public ReadWriteMissedBinResponseDocument readWriteMissedBin(ReadWriteMissedBinDocument readWriteMissedBin146) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[9].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/readWriteMissedBin");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), readWriteMissedBin146,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBin")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBin"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ReadWriteMissedBinResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (ReadWriteMissedBinResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBin"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBin"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBin"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Sets missed bin details by UPRN and
	 * container type
	 *
	 * @see WSCollExternal#readWriteMissedBinByContainerType
	 * @param readWriteMissedBinByContainerType226
	 */
	@Override
	public ReadWriteMissedBinByContainerTypeResponseDocument readWriteMissedBinByContainerType(ReadWriteMissedBinByContainerTypeDocument readWriteMissedBinByContainerType226)
			throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[49].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/readWriteMissedBinByContainerType");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), readWriteMissedBinByContainerType226,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBinByContainerType")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBinByContainerType"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ReadWriteMissedBinByContainerTypeResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (ReadWriteMissedBinByContainerTypeResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBinByContainerType"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBinByContainerType"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBinByContainerType"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Returns all UPRNs with Address of all
	 * UPRNs with more than one standard container for the service requested
	 * with an optional start and end date. If service is blank or start/end
	 * date is blank, all will be returned
	 *
	 * @see WSCollExternal#showAdditionalBins
	 * @param showAdditionalBins206
	 */
	@Override
	public ShowAdditionalBinsResponseDocument showAdditionalBins(ShowAdditionalBinsDocument showAdditionalBins206) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[39].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/ShowAdditionalBins");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), showAdditionalBins206,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "showAdditionalBins")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ShowAdditionalBins"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ShowAdditionalBinsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (ShowAdditionalBinsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ShowAdditionalBins"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ShowAdditionalBins"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ShowAdditionalBins"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Test
	 * Webservice to check communication. Returns Hello : Testname as a string
	 *
	 * @see WSCollExternal#startaA_HelloWorld_String_Test
	 * @param aA_HelloWorld_String_Test192
	 */
	@Override
	public void startaA_HelloWorld_String_Test(AAHelloWorldStringTestDocument aA_HelloWorld_String_Test192, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[32].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AA_HelloWorld_String_Test");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aA_HelloWorld_String_Test192,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aA_HelloWorld_String_Test")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AA_HelloWorld_String_Test"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraA_HelloWorld_String_Test(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_String_Test"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_String_Test"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_String_Test"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraA_HelloWorld_String_Test(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_String_Test(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_String_Test(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_String_Test(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_String_Test(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_String_Test(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_String_Test(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_String_Test(f);
							}
						} else {
							callback.receiveErroraA_HelloWorld_String_Test(f);
						}
					} else {
						callback.receiveErroraA_HelloWorld_String_Test(f);
					}
				} else {
					callback.receiveErroraA_HelloWorld_String_Test(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AAHelloWorldStringTestResponseDocument.class);
					callback.receiveResultaA_HelloWorld_String_Test((AAHelloWorldStringTestResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraA_HelloWorld_String_Test(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[32].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[32].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Test
	 * Webservice to check communication. Returns Hello : Testname as a XML
	 *
	 * @see WSCollExternal#startaA_HelloWorld_XML_Test
	 * @param aA_HelloWorld_XML_Test238
	 */
	@Override
	public void startaA_HelloWorld_XML_Test(AAHelloWorldXMLTestDocument aA_HelloWorld_XML_Test238, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[55].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AA_HelloWorld_XML_Test");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aA_HelloWorld_XML_Test238,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aA_HelloWorld_XML_Test")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AA_HelloWorld_XML_Test"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraA_HelloWorld_XML_Test(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_XML_Test"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_XML_Test"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AA_HelloWorld_XML_Test"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraA_HelloWorld_XML_Test(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_XML_Test(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_XML_Test(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_XML_Test(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_XML_Test(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_XML_Test(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_XML_Test(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraA_HelloWorld_XML_Test(f);
							}
						} else {
							callback.receiveErroraA_HelloWorld_XML_Test(f);
						}
					} else {
						callback.receiveErroraA_HelloWorld_XML_Test(f);
					}
				} else {
					callback.receiveErroraA_HelloWorld_XML_Test(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AAHelloWorldXMLTestResponseDocument.class);
					callback.receiveResultaA_HelloWorld_XML_Test((AAHelloWorldXMLTestResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraA_HelloWorld_XML_Test(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[55].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[55].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - change address
	 *
	 * @see WSCollExternal#startaHP_ChangeAddress
	 * @param aHP_ChangeAddress204
	 */
	@Override
	public void startaHP_ChangeAddress(AHPChangeAddressDocument aHP_ChangeAddress204, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[38].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_ChangeAddress");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_ChangeAddress204,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_ChangeAddress")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_ChangeAddress"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraHP_ChangeAddress(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ChangeAddress"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ChangeAddress"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ChangeAddress"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraHP_ChangeAddress(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ChangeAddress(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ChangeAddress(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ChangeAddress(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ChangeAddress(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ChangeAddress(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ChangeAddress(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ChangeAddress(f);
							}
						} else {
							callback.receiveErroraHP_ChangeAddress(f);
						}
					} else {
						callback.receiveErroraHP_ChangeAddress(f);
					}
				} else {
					callback.receiveErroraHP_ChangeAddress(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AHPChangeAddressResponseDocument.class);
					callback.receiveResultaHP_ChangeAddress((AHPChangeAddressResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraHP_ChangeAddress(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[38].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[38].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - Extend Subscription date
	 *
	 * @see WSCollExternal#startaHP_ExtendSubscription
	 * @param aHP_ExtendSubscription252
	 */
	@Override
	public void startaHP_ExtendSubscription(AHPExtendSubscriptionDocument aHP_ExtendSubscription252, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[62].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_ExtendSubscription");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_ExtendSubscription252,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_ExtendSubscription")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_ExtendSubscription"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraHP_ExtendSubscription(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ExtendSubscription"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ExtendSubscription"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_ExtendSubscription"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraHP_ExtendSubscription(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ExtendSubscription(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ExtendSubscription(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ExtendSubscription(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ExtendSubscription(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ExtendSubscription(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ExtendSubscription(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_ExtendSubscription(f);
							}
						} else {
							callback.receiveErroraHP_ExtendSubscription(f);
						}
					} else {
						callback.receiveErroraHP_ExtendSubscription(f);
					}
				} else {
					callback.receiveErroraHP_ExtendSubscription(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AHPExtendSubscriptionResponseDocument.class);
					callback.receiveResultaHP_ExtendSubscription((AHPExtendSubscriptionResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraHP_ExtendSubscription(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[62].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[62].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - Get AHP details for UPRN
	 *
	 * @see WSCollExternal#startaHP_GetDetails
	 * @param aHP_GetDetails216
	 */
	@Override
	public void startaHP_GetDetails(AHPGetDetailsDocument aHP_GetDetails216, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[44].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_GetDetails");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_GetDetails216,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_GetDetails")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_GetDetails"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraHP_GetDetails(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_GetDetails"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_GetDetails"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_GetDetails"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraHP_GetDetails(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_GetDetails(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_GetDetails(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_GetDetails(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_GetDetails(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_GetDetails(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_GetDetails(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_GetDetails(f);
							}
						} else {
							callback.receiveErroraHP_GetDetails(f);
						}
					} else {
						callback.receiveErroraHP_GetDetails(f);
					}
				} else {
					callback.receiveErroraHP_GetDetails(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AHPGetDetailsResponseDocument.class);
					callback.receiveResultaHP_GetDetails((AHPGetDetailsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraHP_GetDetails(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[44].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[44].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - Register new or update details of existing
	 *
	 * @see WSCollExternal#startaHP_NewUpdate
	 * @param aHP_NewUpdate222
	 */
	@Override
	public void startaHP_NewUpdate(AHPNewUpdateDocument aHP_NewUpdate222, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[47].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_NewUpdate");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_NewUpdate222,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_NewUpdate")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_NewUpdate"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraHP_NewUpdate(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_NewUpdate"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_NewUpdate"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_NewUpdate"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraHP_NewUpdate(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_NewUpdate(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_NewUpdate(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_NewUpdate(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_NewUpdate(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_NewUpdate(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_NewUpdate(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_NewUpdate(f);
							}
						} else {
							callback.receiveErroraHP_NewUpdate(f);
						}
					} else {
						callback.receiveErroraHP_NewUpdate(f);
					}
				} else {
					callback.receiveErroraHP_NewUpdate(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AHPNewUpdateResponseDocument.class);
					callback.receiveResultaHP_NewUpdate((AHPNewUpdateResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraHP_NewUpdate(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[47].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[47].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - Order new Bags
	 *
	 * @see WSCollExternal#startaHP_OrderNewBags
	 * @param aHP_OrderNewBags128
	 */
	@Override
	public void startaHP_OrderNewBags(AHPOrderNewBagsDocument aHP_OrderNewBags128, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_OrderNewBags");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_OrderNewBags128,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_OrderNewBags")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_OrderNewBags"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraHP_OrderNewBags(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_OrderNewBags"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_OrderNewBags"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_OrderNewBags"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraHP_OrderNewBags(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_OrderNewBags(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_OrderNewBags(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_OrderNewBags(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_OrderNewBags(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_OrderNewBags(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_OrderNewBags(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_OrderNewBags(f);
							}
						} else {
							callback.receiveErroraHP_OrderNewBags(f);
						}
					} else {
						callback.receiveErroraHP_OrderNewBags(f);
					}
				} else {
					callback.receiveErroraHP_OrderNewBags(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AHPOrderNewBagsResponseDocument.class);
					callback.receiveResultaHP_OrderNewBags((AHPOrderNewBagsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraHP_OrderNewBags(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[0].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[0].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - remove Subscription
	 *
	 * @see WSCollExternal#startaHP_RemoveSubscription
	 * @param aHP_RemoveSubscription240
	 */
	@Override
	public void startaHP_RemoveSubscription(AHPRemoveSubscriptionDocument aHP_RemoveSubscription240, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[56].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_RemoveSubscription");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_RemoveSubscription240,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_RemoveSubscription")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_RemoveSubscription"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraHP_RemoveSubscription(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_RemoveSubscription"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_RemoveSubscription"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_RemoveSubscription"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraHP_RemoveSubscription(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_RemoveSubscription(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_RemoveSubscription(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_RemoveSubscription(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_RemoveSubscription(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_RemoveSubscription(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_RemoveSubscription(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_RemoveSubscription(f);
							}
						} else {
							callback.receiveErroraHP_RemoveSubscription(f);
						}
					} else {
						callback.receiveErroraHP_RemoveSubscription(f);
					}
				} else {
					callback.receiveErroraHP_RemoveSubscription(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AHPRemoveSubscriptionResponseDocument.class);
					callback.receiveResultaHP_RemoveSubscription((AHPRemoveSubscriptionResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraHP_RemoveSubscription(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[56].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[56].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations AHP
	 * Webservice - suspend subscription
	 *
	 * @see WSCollExternal#startaHP_SuspendSubscription
	 * @param aHP_SuspendSubscription136
	 */
	@Override
	public void startaHP_SuspendSubscription(AHPSuspendSubscriptionDocument aHP_SuspendSubscription136, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[4].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/AHP_SuspendSubscription");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), aHP_SuspendSubscription136,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "aHP_SuspendSubscription")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_SuspendSubscription"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErroraHP_SuspendSubscription(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_SuspendSubscription"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_SuspendSubscription"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "AHP_SuspendSubscription"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErroraHP_SuspendSubscription(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_SuspendSubscription(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_SuspendSubscription(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_SuspendSubscription(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_SuspendSubscription(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_SuspendSubscription(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_SuspendSubscription(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErroraHP_SuspendSubscription(f);
							}
						} else {
							callback.receiveErroraHP_SuspendSubscription(f);
						}
					} else {
						callback.receiveErroraHP_SuspendSubscription(f);
					}
				} else {
					callback.receiveErroraHP_SuspendSubscription(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), AHPSuspendSubscriptionResponseDocument.class);
					callback.receiveResultaHP_SuspendSubscription((AHPSuspendSubscriptionResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErroraHP_SuspendSubscription(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[4].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[4].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Deletes a
	 * container. Requires binID and UPRN
	 *
	 * @see WSCollExternal#startbinDelete
	 * @param binDelete162
	 */
	@Override
	public void startbinDelete(BinDeleteDocument binDelete162, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[17].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/BinDelete");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), binDelete162,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binDelete")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinDelete"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorbinDelete(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinDelete"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinDelete"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinDelete"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorbinDelete(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinDelete(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinDelete(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinDelete(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinDelete(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinDelete(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinDelete(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinDelete(f);
							}
						} else {
							callback.receiveErrorbinDelete(f);
						}
					} else {
						callback.receiveErrorbinDelete(f);
					}
				} else {
					callback.receiveErrorbinDelete(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), BinDeleteResponseDocument.class);
					callback.receiveResultbinDelete((BinDeleteResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorbinDelete(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[17].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[17].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Inserts a
	 * container into the collections database bins table. Bin must exist in
	 * BinShop. Use in conjunction with GetAvailableContainers to get available
	 * containers for this client
	 *
	 * @see WSCollExternal#startbinInsert
	 * @param binInsert172
	 */
	@Override
	public void startbinInsert(BinInsertDocument binInsert172, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[22].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/BinInsert");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), binInsert172,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binInsert")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinInsert"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorbinInsert(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinInsert"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinInsert"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinInsert"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorbinInsert(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinInsert(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinInsert(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinInsert(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinInsert(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinInsert(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinInsert(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinInsert(f);
							}
						} else {
							callback.receiveErrorbinInsert(f);
						}
					} else {
						callback.receiveErrorbinInsert(f);
					}
				} else {
					callback.receiveErrorbinInsert(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), BinInsertResponseDocument.class);
					callback.receiveResultbinInsert((BinInsertResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorbinInsert(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[22].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[22].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Updates a
	 * container in the bins table by id of bin. BinType must be in Binshop
	 *
	 * @see WSCollExternal#startbinUpdate
	 * @param binUpdate170
	 */
	@Override
	public void startbinUpdate(BinUpdateDocument binUpdate170, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[21].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/BinUpdate");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), binUpdate170,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binUpdate")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinUpdate"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorbinUpdate(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinUpdate"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinUpdate"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "BinUpdate"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorbinUpdate(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinUpdate(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinUpdate(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinUpdate(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinUpdate(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinUpdate(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinUpdate(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorbinUpdate(f);
							}
						} else {
							callback.receiveErrorbinUpdate(f);
						}
					} else {
						callback.receiveErrorbinUpdate(f);
					}
				} else {
					callback.receiveErrorbinUpdate(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), BinUpdateResponseDocument.class);
					callback.receiveResultbinUpdate((BinUpdateResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorbinUpdate(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[21].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[21].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns a
	 * line for each UPRN in the databsae that has a collection along with the
	 * unique calendar combination reference - all uprns with same reference
	 * have the same rounds schedule across all services
	 *
	 * @see WSCollExternal#startcachedCalendar
	 * @param cachedCalendar246
	 */
	@Override
	public void startcachedCalendar(CachedCalendarDocument cachedCalendar246, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[59].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/CachedCalendar");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), cachedCalendar246,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "cachedCalendar")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CachedCalendar"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorcachedCalendar(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CachedCalendar"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CachedCalendar"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CachedCalendar"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorcachedCalendar(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcachedCalendar(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcachedCalendar(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcachedCalendar(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcachedCalendar(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcachedCalendar(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcachedCalendar(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcachedCalendar(f);
							}
						} else {
							callback.receiveErrorcachedCalendar(f);
						}
					} else {
						callback.receiveErrorcachedCalendar(f);
					}
				} else {
					callback.receiveErrorcachedCalendar(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), CachedCalendarResponseDocument.class);
					callback.receiveResultcachedCalendar((CachedCalendarResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorcachedCalendar(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[59].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[59].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * assisted details for a UPRN
	 *
	 * @see WSCollExternal#startcheckAssisted
	 * @param checkAssisted198
	 */
	@Override
	public void startcheckAssisted(CheckAssistedDocument checkAssisted198, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[35].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/CheckAssisted");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), checkAssisted198,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "checkAssisted")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CheckAssisted"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorcheckAssisted(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CheckAssisted"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CheckAssisted"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CheckAssisted"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorcheckAssisted(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcheckAssisted(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcheckAssisted(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcheckAssisted(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcheckAssisted(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcheckAssisted(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcheckAssisted(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcheckAssisted(f);
							}
						} else {
							callback.receiveErrorcheckAssisted(f);
						}
					} else {
						callback.receiveErrorcheckAssisted(f);
					}
				} else {
					callback.receiveErrorcheckAssisted(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), CheckAssistedResponseDocument.class);
					callback.receiveResultcheckAssisted((CheckAssistedResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorcheckAssisted(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[35].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[35].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Incab
	 * Comments - Get Current Value
	 *
	 * @see WSCollExternal#startcommentsForIncabGetExisting
	 * @param commentsForIncabGetExisting232
	 */
	@Override
	public void startcommentsForIncabGetExisting(CommentsForIncabGetExistingDocument commentsForIncabGetExisting232, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[52].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/CommentsForIncabGetExisting");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), commentsForIncabGetExisting232,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "commentsForIncabGetExisting")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CommentsForIncabGetExisting"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorcommentsForIncabGetExisting(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabGetExisting"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabGetExisting"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabGetExisting"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorcommentsForIncabGetExisting(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabGetExisting(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabGetExisting(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabGetExisting(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabGetExisting(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabGetExisting(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabGetExisting(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabGetExisting(f);
							}
						} else {
							callback.receiveErrorcommentsForIncabGetExisting(f);
						}
					} else {
						callback.receiveErrorcommentsForIncabGetExisting(f);
					}
				} else {
					callback.receiveErrorcommentsForIncabGetExisting(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), CommentsForIncabGetExistingResponseDocument.class);
					callback.receiveResultcommentsForIncabGetExisting((CommentsForIncabGetExistingResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorcommentsForIncabGetExisting(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[52].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[52].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Incab
	 * Comments - update
	 *
	 * @see WSCollExternal#startcommentsForIncabUpdate
	 * @param commentsForIncabUpdate156
	 */
	@Override
	public void startcommentsForIncabUpdate(CommentsForIncabUpdateDocument commentsForIncabUpdate156, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[14].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/CommentsForIncabUpdate");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), commentsForIncabUpdate156,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "commentsForIncabUpdate")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CommentsForIncabUpdate"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorcommentsForIncabUpdate(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabUpdate"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabUpdate"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "CommentsForIncabUpdate"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorcommentsForIncabUpdate(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabUpdate(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabUpdate(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabUpdate(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabUpdate(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabUpdate(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabUpdate(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcommentsForIncabUpdate(f);
							}
						} else {
							callback.receiveErrorcommentsForIncabUpdate(f);
						}
					} else {
						callback.receiveErrorcommentsForIncabUpdate(f);
					}
				} else {
					callback.receiveErrorcommentsForIncabUpdate(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), CommentsForIncabUpdateResponseDocument.class);
					callback.receiveResultcommentsForIncabUpdate((CommentsForIncabUpdateResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorcommentsForIncabUpdate(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[14].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[14].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * number of bins on a given service for a UPRN
	 *
	 * @see WSCollExternal#startcountBinsForService
	 * @param countBinsForService140
	 */
	@Override
	public void startcountBinsForService(CountBinsForServiceDocument countBinsForService140, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[6].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/countBinsForService");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), countBinsForService140,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countBinsForService")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countBinsForService"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorcountBinsForService(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countBinsForService"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countBinsForService"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countBinsForService"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorcountBinsForService(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountBinsForService(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountBinsForService(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountBinsForService(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountBinsForService(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountBinsForService(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountBinsForService(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountBinsForService(f);
							}
						} else {
							callback.receiveErrorcountBinsForService(f);
						}
					} else {
						callback.receiveErrorcountBinsForService(f);
					}
				} else {
					callback.receiveErrorcountBinsForService(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), CountBinsForServiceResponseDocument.class);
					callback.receiveResultcountBinsForService((CountBinsForServiceResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorcountBinsForService(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[6].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[6].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Counts the
	 * number of Garden subscriptions between start and end date
	 *
	 * @see WSCollExternal#startcountSubscriptions
	 * @param countSubscriptions228
	 */
	@Override
	public void startcountSubscriptions(CountSubscriptionsDocument countSubscriptions228, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[50].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/countSubscriptions");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), countSubscriptions228,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countSubscriptions")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countSubscriptions"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorcountSubscriptions(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countSubscriptions"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countSubscriptions"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "countSubscriptions"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorcountSubscriptions(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountSubscriptions(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountSubscriptions(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountSubscriptions(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountSubscriptions(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountSubscriptions(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountSubscriptions(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorcountSubscriptions(f);
							}
						} else {
							callback.receiveErrorcountSubscriptions(f);
						}
					} else {
						callback.receiveErrorcountSubscriptions(f);
					}
				} else {
					callback.receiveErrorcountSubscriptions(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), CountSubscriptionsResponseDocument.class);
					callback.receiveResultcountSubscriptions((CountSubscriptionsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorcountSubscriptions(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[50].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[50].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Deletes a
	 * Garden Bin By id or name for a UPRN - **Jadu
	 *
	 * @see WSCollExternal#startdeleteBin
	 * @param deleteBin202
	 */
	@Override
	public void startdeleteBin(DeleteBinDocument deleteBin202, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[37].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/DeleteBin");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), deleteBin202,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "deleteBin")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "DeleteBin"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrordeleteBin(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "DeleteBin"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "DeleteBin"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "DeleteBin"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrordeleteBin(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrordeleteBin(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrordeleteBin(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrordeleteBin(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrordeleteBin(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrordeleteBin(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrordeleteBin(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrordeleteBin(f);
							}
						} else {
							callback.receiveErrordeleteBin(f);
						}
					} else {
						callback.receiveErrordeleteBin(f);
					}
				} else {
					callback.receiveErrordeleteBin(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), DeleteBinResponseDocument.class);
					callback.receiveResultdeleteBin((DeleteBinResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrordeleteBin(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[37].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[37].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Garden
	 * Webservice - Moves Garden detail from one UPRN to another
	 *
	 * @see WSCollExternal#startgarden_ChangeAddress
	 * @param garden_ChangeAddress188
	 */
	@Override
	public void startgarden_ChangeAddress(GardenChangeAddressDocument garden_ChangeAddress188, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[30].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/Garden_ChangeAddress");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), garden_ChangeAddress188,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_ChangeAddress")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Garden_ChangeAddress"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgarden_ChangeAddress(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_ChangeAddress"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_ChangeAddress"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_ChangeAddress"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgarden_ChangeAddress(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_ChangeAddress(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_ChangeAddress(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_ChangeAddress(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_ChangeAddress(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_ChangeAddress(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_ChangeAddress(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_ChangeAddress(f);
							}
						} else {
							callback.receiveErrorgarden_ChangeAddress(f);
						}
					} else {
						callback.receiveErrorgarden_ChangeAddress(f);
					}
				} else {
					callback.receiveErrorgarden_ChangeAddress(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GardenChangeAddressResponseDocument.class);
					callback.receiveResultgarden_ChangeAddress((GardenChangeAddressResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgarden_ChangeAddress(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[30].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[30].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Garden
	 * Webservice - remove Subscription and set end Date of Container to the
	 * supplied end date. IF end date is blank, then todays date will be used.
	 *
	 * @see WSCollExternal#startgarden_RemoveSubscription
	 * @param garden_RemoveSubscription158
	 */
	@Override
	public void startgarden_RemoveSubscription(GardenRemoveSubscriptionDocument garden_RemoveSubscription158, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[15].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/Garden_RemoveSubscription");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), garden_RemoveSubscription158,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_RemoveSubscription")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Garden_RemoveSubscription"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgarden_RemoveSubscription(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_RemoveSubscription"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_RemoveSubscription"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_RemoveSubscription"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgarden_RemoveSubscription(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_RemoveSubscription(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_RemoveSubscription(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_RemoveSubscription(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_RemoveSubscription(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_RemoveSubscription(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_RemoveSubscription(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_RemoveSubscription(f);
							}
						} else {
							callback.receiveErrorgarden_RemoveSubscription(f);
						}
					} else {
						callback.receiveErrorgarden_RemoveSubscription(f);
					}
				} else {
					callback.receiveErrorgarden_RemoveSubscription(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GardenRemoveSubscriptionResponseDocument.class);
					callback.receiveResultgarden_RemoveSubscription((GardenRemoveSubscriptionResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgarden_RemoveSubscription(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[15].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[15].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Garden
	 * Subscribers - Get all current subscribers, Garden Deliveries /Collections
	 * on a certain date.
	 *
	 * @see WSCollExternal#startgarden_Subscribers_Deliveries_Collections
	 * @param garden_Subscribers_Deliveries_Collections182
	 */
	@Override
	public void startgarden_Subscribers_Deliveries_Collections(GardenSubscribersDeliveriesCollectionsDocument garden_Subscribers_Deliveries_Collections182,
			final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[27].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/Garden_Subscribers_Deliveries_Collections");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), garden_Subscribers_Deliveries_Collections182,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "garden_Subscribers_Deliveries_Collections")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Garden_Subscribers_Deliveries_Collections"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgarden_Subscribers_Deliveries_Collections(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_Subscribers_Deliveries_Collections"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_Subscribers_Deliveries_Collections"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Garden_Subscribers_Deliveries_Collections"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgarden_Subscribers_Deliveries_Collections(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
							}
						} else {
							callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
						}
					} else {
						callback.receiveErrorgarden_Subscribers_Deliveries_Collections(f);
					}
				} else {
					callback.receiveErrorgarden_Subscribers_Deliveries_Collections(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GardenSubscribersDeliveriesCollectionsResponseDocument.class);
					callback.receiveResultgarden_Subscribers_Deliveries_Collections((GardenSubscribersDeliveriesCollectionsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgarden_Subscribers_Deliveries_Collections(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[27].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[27].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations
	 * Functionality for a new Garden Subscription / or update existing
	 * subscription
	 *
	 * @see WSCollExternal#startgardenSubscription
	 * @param gardenSubscription150
	 */
	@Override
	public void startgardenSubscription(GardenSubscriptionDocument gardenSubscription150, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[11].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GardenSubscription");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), gardenSubscription150,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscription"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgardenSubscription(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgardenSubscription(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription(f);
							}
						} else {
							callback.receiveErrorgardenSubscription(f);
						}
					} else {
						callback.receiveErrorgardenSubscription(f);
					}
				} else {
					callback.receiveErrorgardenSubscription(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GardenSubscriptionResponseDocument.class);
					callback.receiveResultgardenSubscription((GardenSubscriptionResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgardenSubscription(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[11].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[11].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Garden
	 * Webservice - Get Garden Subscription details for UPRN
	 *
	 * @see WSCollExternal#startgardenSubscription_GetDetails
	 * @param gardenSubscription_GetDetails186
	 */
	@Override
	public void startgardenSubscription_GetDetails(GardenSubscriptionGetDetailsDocument gardenSubscription_GetDetails186, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[29].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GardenSubscription_GetDetails");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), gardenSubscription_GetDetails186,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription_GetDetails")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscription_GetDetails"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgardenSubscription_GetDetails(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_GetDetails"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_GetDetails"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_GetDetails"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgardenSubscription_GetDetails(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_GetDetails(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_GetDetails(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_GetDetails(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_GetDetails(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_GetDetails(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_GetDetails(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_GetDetails(f);
							}
						} else {
							callback.receiveErrorgardenSubscription_GetDetails(f);
						}
					} else {
						callback.receiveErrorgardenSubscription_GetDetails(f);
					}
				} else {
					callback.receiveErrorgardenSubscription_GetDetails(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GardenSubscriptionGetDetailsResponseDocument.class);
					callback.receiveResultgardenSubscription_GetDetails((GardenSubscriptionGetDetailsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgardenSubscription_GetDetails(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[29].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[29].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations
	 * Functionality for a new Garden Subscription / or update existing
	 * subscription includes Name Phone and Email
	 *
	 * @see WSCollExternal#startgardenSubscription_WithNamePhoneEmail
	 * @param gardenSubscription_WithNamePhoneEmail138
	 */
	@Override
	public void startgardenSubscription_WithNamePhoneEmail(GardenSubscriptionWithNamePhoneEmailDocument gardenSubscription_WithNamePhoneEmail138, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[5].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GardenSubscription_WithNamePhoneEmail");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), gardenSubscription_WithNamePhoneEmail138,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "gardenSubscription_WithNamePhoneEmail")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscription_WithNamePhoneEmail"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgardenSubscription_WithNamePhoneEmail(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_WithNamePhoneEmail"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_WithNamePhoneEmail"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GardenSubscription_WithNamePhoneEmail"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgardenSubscription_WithNamePhoneEmail(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
							}
						} else {
							callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
						}
					} else {
						callback.receiveErrorgardenSubscription_WithNamePhoneEmail(f);
					}
				} else {
					callback.receiveErrorgardenSubscription_WithNamePhoneEmail(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GardenSubscriptionWithNamePhoneEmailResponseDocument.class);
					callback.receiveResultgardenSubscription_WithNamePhoneEmail((GardenSubscriptionWithNamePhoneEmailResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgardenSubscription_WithNamePhoneEmail(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[5].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[5].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * matching addresses from given address details
	 *
	 * @see WSCollExternal#startgetAddressFromPostcode
	 * @param getAddressFromPostcode160
	 */
	@Override
	public void startgetAddressFromPostcode(GetAddressFromPostcodeDocument getAddressFromPostcode160, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[16].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getAddressFromPostcode");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAddressFromPostcode160,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressFromPostcode")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressFromPostcode"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetAddressFromPostcode(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAddressFromPostcode"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAddressFromPostcode"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAddressFromPostcode"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetAddressFromPostcode(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressFromPostcode(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressFromPostcode(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressFromPostcode(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressFromPostcode(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressFromPostcode(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressFromPostcode(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressFromPostcode(f);
							}
						} else {
							callback.receiveErrorgetAddressFromPostcode(f);
						}
					} else {
						callback.receiveErrorgetAddressFromPostcode(f);
					}
				} else {
					callback.receiveErrorgetAddressFromPostcode(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetAddressFromPostcodeResponseDocument.class);
					callback.receiveResultgetAddressFromPostcode((GetAddressFromPostcodeResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetAddressFromPostcode(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[16].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[16].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns and
	 * Address from a UPRN or matching UPRNs from address details
	 *
	 * @see WSCollExternal#startgetAddressOrUPRN
	 * @param getAddressOrUPRN154
	 */
	@Override
	public void startgetAddressOrUPRN(GetAddressOrUPRNDocument getAddressOrUPRN154, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[13].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAddressOrUPRN");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAddressOrUPRN154,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressOrUPRN")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAddressOrUPRN"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetAddressOrUPRN(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAddressOrUPRN"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAddressOrUPRN"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAddressOrUPRN"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetAddressOrUPRN(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressOrUPRN(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressOrUPRN(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressOrUPRN(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressOrUPRN(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressOrUPRN(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressOrUPRN(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAddressOrUPRN(f);
							}
						} else {
							callback.receiveErrorgetAddressOrUPRN(f);
						}
					} else {
						callback.receiveErrorgetAddressOrUPRN(f);
					}
				} else {
					callback.receiveErrorgetAddressOrUPRN(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetAddressOrUPRNResponseDocument.class);
					callback.receiveResultgetAddressOrUPRN((GetAddressOrUPRNResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetAddressOrUPRN(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[13].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[13].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * issues of the issueType between start and End dates
	 *
	 * @see WSCollExternal#startgetAllIssuesForIssueType
	 * @param getAllIssuesForIssueType164
	 */
	@Override
	public void startgetAllIssuesForIssueType(GetAllIssuesForIssueTypeDocument getAllIssuesForIssueType164, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[18].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getAllIssuesForIssueType");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAllIssuesForIssueType164,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllIssuesForIssueType")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllIssuesForIssueType"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetAllIssuesForIssueType(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAllIssuesForIssueType"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAllIssuesForIssueType"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getAllIssuesForIssueType"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetAllIssuesForIssueType(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllIssuesForIssueType(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllIssuesForIssueType(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllIssuesForIssueType(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllIssuesForIssueType(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllIssuesForIssueType(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllIssuesForIssueType(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllIssuesForIssueType(f);
							}
						} else {
							callback.receiveErrorgetAllIssuesForIssueType(f);
						}
					} else {
						callback.receiveErrorgetAllIssuesForIssueType(f);
					}
				} else {
					callback.receiveErrorgetAllIssuesForIssueType(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetAllIssuesForIssueTypeResponseDocument.class);
					callback.receiveResultgetAllIssuesForIssueType((GetAllIssuesForIssueTypeResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetAllIssuesForIssueType(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[18].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[18].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns All
	 * rounds for every UPRN
	 *
	 * @see WSCollExternal#startgetAllRoundDetails
	 * @param getAllRoundDetails168
	 */
	@Override
	public void startgetAllRoundDetails(GetAllRoundDetailsDocument getAllRoundDetails168, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[20].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAllRoundDetails");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAllRoundDetails168,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllRoundDetails")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAllRoundDetails"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetAllRoundDetails(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllRoundDetails"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllRoundDetails"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllRoundDetails"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetAllRoundDetails(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllRoundDetails(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllRoundDetails(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllRoundDetails(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllRoundDetails(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllRoundDetails(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllRoundDetails(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllRoundDetails(f);
							}
						} else {
							callback.receiveErrorgetAllRoundDetails(f);
						}
					} else {
						callback.receiveErrorgetAllRoundDetails(f);
					}
				} else {
					callback.receiveErrorgetAllRoundDetails(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetAllRoundDetailsResponseDocument.class);
					callback.receiveResultgetAllRoundDetails((GetAllRoundDetailsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetAllRoundDetails(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[20].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[20].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns All
	 * UPRNs and round information of properties collected on this date - does
	 * not currently cater for complex trade parameters
	 *
	 * @see WSCollExternal#startgetAllUPRNsForDate
	 * @param getAllUPRNsForDate218
	 */
	@Override
	public void startgetAllUPRNsForDate(GetAllUPRNsForDateDocument getAllUPRNsForDate218, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[45].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAllUPRNsForDate");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAllUPRNsForDate218,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAllUPRNsForDate")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAllUPRNsForDate"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetAllUPRNsForDate(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllUPRNsForDate"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllUPRNsForDate"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAllUPRNsForDate"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetAllUPRNsForDate(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllUPRNsForDate(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllUPRNsForDate(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllUPRNsForDate(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllUPRNsForDate(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllUPRNsForDate(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllUPRNsForDate(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAllUPRNsForDate(f);
							}
						} else {
							callback.receiveErrorgetAllUPRNsForDate(f);
						}
					} else {
						callback.receiveErrorgetAllUPRNsForDate(f);
					}
				} else {
					callback.receiveErrorgetAllUPRNsForDate(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetAllUPRNsForDateResponseDocument.class);
					callback.receiveResultgetAllUPRNsForDate((GetAllUPRNsForDateResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetAllUPRNsForDate(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[45].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[45].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Please use
	 * GetAvailableContainersForClientOrProperty as a better and updated
	 * alternative. Returns all current known container types in the database.
	 *
	 * @see WSCollExternal#startgetAvailableContainers
	 * @param getAvailableContainers176
	 */
	@Override
	public void startgetAvailableContainers(GetAvailableContainersDocument getAvailableContainers176, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[24].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAvailableContainers");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAvailableContainers176,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableContainers")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableContainers"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetAvailableContainers(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainers"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainers"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainers"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetAvailableContainers(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainers(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainers(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainers(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainers(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainers(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainers(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainers(f);
							}
						} else {
							callback.receiveErrorgetAvailableContainers(f);
						}
					} else {
						callback.receiveErrorgetAvailableContainers(f);
					}
				} else {
					callback.receiveErrorgetAvailableContainers(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetAvailableContainersResponseDocument.class);
					callback.receiveResultgetAvailableContainers((GetAvailableContainersResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetAvailableContainers(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[24].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[24].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * current known container types in the database for a client or (if a UPRN
	 * is supplied) at a property
	 *
	 * @see WSCollExternal#startgetAvailableContainersForClientOrProperty
	 * @param getAvailableContainersForClientOrProperty214
	 */
	@Override
	public void startgetAvailableContainersForClientOrProperty(GetAvailableContainersForClientOrPropertyDocument getAvailableContainersForClientOrProperty214,
			final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[43].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAvailableContainersForClientOrProperty");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAvailableContainersForClientOrProperty214,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableContainersForClientOrProperty")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableContainersForClientOrProperty"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetAvailableContainersForClientOrProperty(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainersForClientOrProperty"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainersForClientOrProperty"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableContainersForClientOrProperty"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetAvailableContainersForClientOrProperty(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
							}
						} else {
							callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
						}
					} else {
						callback.receiveErrorgetAvailableContainersForClientOrProperty(f);
					}
				} else {
					callback.receiveErrorgetAvailableContainersForClientOrProperty(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetAvailableContainersForClientOrPropertyResponseDocument.class);
					callback.receiveResultgetAvailableContainersForClientOrProperty((GetAvailableContainersForClientOrPropertyResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetAvailableContainersForClientOrProperty(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[43].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[43].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * current known rounds in the database
	 *
	 * @see WSCollExternal#startgetAvailableRounds
	 * @param getAvailableRounds212
	 */
	@Override
	public void startgetAvailableRounds(GetAvailableRoundsDocument getAvailableRounds212, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[42].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetAvailableRounds");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getAvailableRounds212,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAvailableRounds")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableRounds"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetAvailableRounds(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableRounds"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableRounds"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetAvailableRounds"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetAvailableRounds(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableRounds(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableRounds(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableRounds(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableRounds(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableRounds(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableRounds(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetAvailableRounds(f);
							}
						} else {
							callback.receiveErrorgetAvailableRounds(f);
						}
					} else {
						callback.receiveErrorgetAvailableRounds(f);
					}
				} else {
					callback.receiveErrorgetAvailableRounds(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetAvailableRoundsResponseDocument.class);
					callback.receiveResultgetAvailableRounds((GetAvailableRoundsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetAvailableRounds(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[42].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[42].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * container details for a service for a given UPRN. Includes ParentUPRN
	 * containers
	 *
	 * @see WSCollExternal#startgetBinDetailsForService
	 * @param getBinDetailsForService230
	 */
	@Override
	public void startgetBinDetailsForService(GetBinDetailsForServiceDocument getBinDetailsForService230, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[51].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetBinDetailsForService");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getBinDetailsForService230,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getBinDetailsForService")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetBinDetailsForService"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetBinDetailsForService(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetBinDetailsForService"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetBinDetailsForService"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetBinDetailsForService"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetBinDetailsForService(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetBinDetailsForService(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetBinDetailsForService(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetBinDetailsForService(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetBinDetailsForService(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetBinDetailsForService(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetBinDetailsForService(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetBinDetailsForService(f);
							}
						} else {
							callback.receiveErrorgetBinDetailsForService(f);
						}
					} else {
						callback.receiveErrorgetBinDetailsForService(f);
					}
				} else {
					callback.receiveErrorgetBinDetailsForService(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetBinDetailsForServiceResponseDocument.class);
					callback.receiveResultgetBinDetailsForService((GetBinDetailsForServiceResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetBinDetailsForService(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[51].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[51].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns a
	 * list of the current incab Issues Codes and texts for use when parsing
	 * issue messsages from Incab
	 *
	 * @see WSCollExternal#startgetInCabIssueTextAndIssueCode
	 * @param getInCabIssueTextAndIssueCode234
	 */
	@Override
	public void startgetInCabIssueTextAndIssueCode(GetInCabIssueTextAndIssueCodeDocument getInCabIssueTextAndIssueCode234, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[53].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetInCabIssueTextAndIssueCode");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getInCabIssueTextAndIssueCode234,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getInCabIssueTextAndIssueCode")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetInCabIssueTextAndIssueCode"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetInCabIssueTextAndIssueCode(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetInCabIssueTextAndIssueCode"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetInCabIssueTextAndIssueCode"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetInCabIssueTextAndIssueCode"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetInCabIssueTextAndIssueCode(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
							}
						} else {
							callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
						}
					} else {
						callback.receiveErrorgetInCabIssueTextAndIssueCode(f);
					}
				} else {
					callback.receiveErrorgetInCabIssueTextAndIssueCode(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetInCabIssueTextAndIssueCodeResponseDocument.class);
					callback.receiveResultgetInCabIssueTextAndIssueCode((GetInCabIssueTextAndIssueCodeResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetInCabIssueTextAndIssueCode(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[53].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[53].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * issues for the populated filtered values on a date(ddMMyyyy) between a
	 * start and end time (HH:mm)
	 *
	 * @see WSCollExternal#startgetIssues
	 * @param getIssues196
	 */
	@Override
	public void startgetIssues(GetIssuesDocument getIssues196, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[34].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetIssues");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getIssues196,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssues")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssues"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetIssues(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssues"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssues"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssues"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetIssues(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssues(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssues(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssues(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssues(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssues(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssues(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssues(f);
							}
						} else {
							callback.receiveErrorgetIssues(f);
						}
					} else {
						callback.receiveErrorgetIssues(f);
					}
				} else {
					callback.receiveErrorgetIssues(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetIssuesResponseDocument.class);
					callback.receiveResultgetIssues((GetIssuesResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetIssues(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[34].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[34].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations **Requires
	 * Webaspx InCab. Returns latest issues and collection status from the last
	 * time a truck was out for each service for this UPRN
	 *
	 * @see WSCollExternal#startgetIssuesAndCollectionStatusForUPRN
	 * @param getIssuesAndCollectionStatusForUPRN254
	 */
	@Override
	public void startgetIssuesAndCollectionStatusForUPRN(GetIssuesAndCollectionStatusForUPRNDocument getIssuesAndCollectionStatusForUPRN254, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[63].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getIssuesAndCollectionStatusForUPRN");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getIssuesAndCollectionStatusForUPRN254,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssuesAndCollectionStatusForUPRN")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssuesAndCollectionStatusForUPRN"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getIssuesAndCollectionStatusForUPRN"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getIssuesAndCollectionStatusForUPRN"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getIssuesAndCollectionStatusForUPRN"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
							}
						} else {
							callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
						}
					} else {
						callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(f);
					}
				} else {
					callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetIssuesAndCollectionStatusForUPRNResponseDocument.class);
					callback.receiveResultgetIssuesAndCollectionStatusForUPRN((GetIssuesAndCollectionStatusForUPRNResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetIssuesAndCollectionStatusForUPRN(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[63].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[63].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * Issues for a given UPRN for a specific date ** Requires Webaspx InCab
	 * system ** DateReq in format ddMMyyyy
	 *
	 * @see WSCollExternal#startgetIssuesForUPRN
	 * @param getIssuesForUPRN194
	 */
	@Override
	public void startgetIssuesForUPRN(GetIssuesForUPRNDocument getIssuesForUPRN194, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[33].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetIssuesForUPRN");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getIssuesForUPRN194,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getIssuesForUPRN")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssuesForUPRN"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetIssuesForUPRN(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssuesForUPRN"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssuesForUPRN"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetIssuesForUPRN"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetIssuesForUPRN(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesForUPRN(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesForUPRN(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesForUPRN(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesForUPRN(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesForUPRN(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesForUPRN(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetIssuesForUPRN(f);
							}
						} else {
							callback.receiveErrorgetIssuesForUPRN(f);
						}
					} else {
						callback.receiveErrorgetIssuesForUPRN(f);
					}
				} else {
					callback.receiveErrorgetIssuesForUPRN(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetIssuesForUPRNResponseDocument.class);
					callback.receiveResultgetIssuesForUPRN((GetIssuesForUPRNResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetIssuesForUPRN(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[33].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[33].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns last
	 * known truck position for a given registrations, date and time ** Requires
	 * Webaspx InCab System **
	 *
	 * @see WSCollExternal#startgetLastTruckPosition
	 * @param getLastTruckPosition244
	 */
	@Override
	public void startgetLastTruckPosition(GetLastTruckPositionDocument getLastTruckPosition244, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[58].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetLastTruckPosition");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getLastTruckPosition244,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getLastTruckPosition")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetLastTruckPosition"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetLastTruckPosition(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetLastTruckPosition"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetLastTruckPosition"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetLastTruckPosition"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetLastTruckPosition(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLastTruckPosition(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLastTruckPosition(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLastTruckPosition(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLastTruckPosition(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLastTruckPosition(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLastTruckPosition(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLastTruckPosition(f);
							}
						} else {
							callback.receiveErrorgetLastTruckPosition(f);
						}
					} else {
						callback.receiveErrorgetLastTruckPosition(f);
					}
				} else {
					callback.receiveErrorgetLastTruckPosition(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetLastTruckPositionResponseDocument.class);
					callback.receiveResultgetLastTruckPosition((GetLastTruckPositionResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetLastTruckPosition(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[58].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[58].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * current truck positions that are collecting this UPRN today ** Requires
	 * Webaspx InCab System **
	 *
	 * @see WSCollExternal#startgetLatestTruckPositions
	 * @param getLatestTruckPositions152
	 */
	@Override
	public void startgetLatestTruckPositions(GetLatestTruckPositionsDocument getLatestTruckPositions152, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[12].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getLatestTruckPositions");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getLatestTruckPositions152,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getLatestTruckPositions")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getLatestTruckPositions"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetLatestTruckPositions(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getLatestTruckPositions"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getLatestTruckPositions"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getLatestTruckPositions"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetLatestTruckPositions(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLatestTruckPositions(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLatestTruckPositions(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLatestTruckPositions(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLatestTruckPositions(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLatestTruckPositions(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLatestTruckPositions(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetLatestTruckPositions(f);
							}
						} else {
							callback.receiveErrorgetLatestTruckPositions(f);
						}
					} else {
						callback.receiveErrorgetLatestTruckPositions(f);
					}
				} else {
					callback.receiveErrorgetLatestTruckPositions(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetLatestTruckPositionsResponseDocument.class);
					callback.receiveResultgetLatestTruckPositions((GetLatestTruckPositionsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetLatestTruckPositions(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[12].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[12].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations The main use
	 * of this webservice is to provide the current No Collection Dates for a
	 * suspended service, such as no Garden collection through winter. This will
	 * provide the date the Garden Suspension started and the date the garden
	 * suspension ends relevant to todays date. It will also supply details of
	 * the no collect dates relevant to the parameters supplied. Typically
	 * returned on UPRN level, but can be called for All, Service or Roundname.
	 * (Default is All)
	 *
	 * @see WSCollExternal#startgetNoCollectionDates
	 * @param getNoCollectionDates134
	 */
	@Override
	public void startgetNoCollectionDates(GetNoCollectionDatesDocument getNoCollectionDates134, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[3].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getNoCollectionDates");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getNoCollectionDates134,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getNoCollectionDates")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getNoCollectionDates"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetNoCollectionDates(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getNoCollectionDates"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getNoCollectionDates"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getNoCollectionDates"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetNoCollectionDates(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetNoCollectionDates(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetNoCollectionDates(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetNoCollectionDates(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetNoCollectionDates(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetNoCollectionDates(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetNoCollectionDates(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetNoCollectionDates(f);
							}
						} else {
							callback.receiveErrorgetNoCollectionDates(f);
						}
					} else {
						callback.receiveErrorgetNoCollectionDates(f);
					}
				} else {
					callback.receiveErrorgetNoCollectionDates(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetNoCollectionDatesResponseDocument.class);
					callback.receiveResultgetNoCollectionDates((GetNoCollectionDatesResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetNoCollectionDates(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[3].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[3].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * next collection dates for a potential garden subscriber
	 *
	 * @see WSCollExternal#startgetPotentialCollectionDatesForNewGardenSubscriber
	 * @param getPotentialCollectionDatesForNewGardenSubscriber130
	 */
	@Override
	public void startgetPotentialCollectionDatesForNewGardenSubscriber(GetPotentialCollectionDatesForNewGardenSubscriberDocument getPotentialCollectionDatesForNewGardenSubscriber130,
			final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getPotentialCollectionDatesForNewGardenSubscriber");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getPotentialCollectionDatesForNewGardenSubscriber130,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getPotentialCollectionDatesForNewGardenSubscriber")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getPotentialCollectionDatesForNewGardenSubscriber"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getPotentialCollectionDatesForNewGardenSubscriber"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getPotentialCollectionDatesForNewGardenSubscriber"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getPotentialCollectionDatesForNewGardenSubscriber"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
							}
						} else {
							callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
						}
					} else {
						callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(f);
					}
				} else {
					callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument.class);
					callback.receiveResultgetPotentialCollectionDatesForNewGardenSubscriber((GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetPotentialCollectionDatesForNewGardenSubscriber(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[1].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[1].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * collections dates for each container for a particular UPRn inbetween
	 * start and end dates. **Leave BinID blank as historical unused parameter
	 *
	 * @see WSCollExternal#startgetRoundAndBinInfoForUPRNForNewAdjustedDates
	 * @param getRoundAndBinInfoForUPRNForNewAdjustedDates248
	 */
	@Override
	public void startgetRoundAndBinInfoForUPRNForNewAdjustedDates(GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument getRoundAndBinInfoForUPRNForNewAdjustedDates248,
			final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[60].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getRoundAndBinInfoForUPRNForNewAdjustedDates");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundAndBinInfoForUPRNForNewAdjustedDates248,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundAndBinInfoForUPRNForNewAdjustedDates")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundAndBinInfoForUPRNForNewAdjustedDates"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundAndBinInfoForUPRNForNewAdjustedDates"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundAndBinInfoForUPRNForNewAdjustedDates"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundAndBinInfoForUPRNForNewAdjustedDates"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
							}
						} else {
							callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
						}
					} else {
						callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(f);
					}
				} else {
					callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.class);
					callback.receiveResultgetRoundAndBinInfoForUPRNForNewAdjustedDates((GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetRoundAndBinInfoForUPRNForNewAdjustedDates(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[60].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[60].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns html
	 * of Friendly Calendar info and html table of calendar)
	 *
	 * @see WSCollExternal#startgetRoundCalendarForUPRN
	 * @param getRoundCalendarForUPRN132
	 */
	@Override
	public void startgetRoundCalendarForUPRN(GetRoundCalendarForUPRNDocument getRoundCalendarForUPRN132, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getRoundCalendarForUPRN");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundCalendarForUPRN132,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundCalendarForUPRN")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundCalendarForUPRN"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetRoundCalendarForUPRN(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundCalendarForUPRN"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundCalendarForUPRN"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundCalendarForUPRN"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetRoundCalendarForUPRN(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundCalendarForUPRN(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundCalendarForUPRN(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundCalendarForUPRN(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundCalendarForUPRN(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundCalendarForUPRN(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundCalendarForUPRN(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundCalendarForUPRN(f);
							}
						} else {
							callback.receiveErrorgetRoundCalendarForUPRN(f);
						}
					} else {
						callback.receiveErrorgetRoundCalendarForUPRN(f);
					}
				} else {
					callback.receiveErrorgetRoundCalendarForUPRN(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetRoundCalendarForUPRNResponseDocument.class);
					callback.receiveResultgetRoundCalendarForUPRN((GetRoundCalendarForUPRNResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetRoundCalendarForUPRN(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[2].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[2].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * details of any round changes made within a date range for a round or UPRN
	 *
	 * @see WSCollExternal#startgetRoundChangesInDateRangeByRoundOrUPRN
	 * @param getRoundChangesInDateRangeByRoundOrUPRN144
	 */
	@Override
	public void startgetRoundChangesInDateRangeByRoundOrUPRN(GetRoundChangesInDateRangeByRoundOrUPRNDocument getRoundChangesInDateRangeByRoundOrUPRN144, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[8].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetRoundChangesInDateRangeByRoundOrUPRN");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundChangesInDateRangeByRoundOrUPRN144,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundChangesInDateRangeByRoundOrUPRN")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetRoundChangesInDateRangeByRoundOrUPRN"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetRoundChangesInDateRangeByRoundOrUPRN"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetRoundChangesInDateRangeByRoundOrUPRN"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetRoundChangesInDateRangeByRoundOrUPRN"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
							}
						} else {
							callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
						}
					} else {
						callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(f);
					}
				} else {
					callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument.class);
					callback.receiveResultgetRoundChangesInDateRangeByRoundOrUPRN((GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetRoundChangesInDateRangeByRoundOrUPRN(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[8].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[8].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations returns next
	 * collection for each service this UPRN from a given start date
	 *
	 * @see WSCollExternal#startgetRoundForUPRN
	 * @param getRoundForUPRN242
	 */
	@Override
	public void startgetRoundForUPRN(GetRoundForUPRNDocument getRoundForUPRN242, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[57].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getRoundForUPRN");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundForUPRN242,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundForUPRN")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundForUPRN"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetRoundForUPRN(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundForUPRN"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundForUPRN"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundForUPRN"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetRoundForUPRN(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundForUPRN(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundForUPRN(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundForUPRN(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundForUPRN(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundForUPRN(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundForUPRN(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundForUPRN(f);
							}
						} else {
							callback.receiveErrorgetRoundForUPRN(f);
						}
					} else {
						callback.receiveErrorgetRoundForUPRN(f);
					}
				} else {
					callback.receiveErrorgetRoundForUPRN(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetRoundForUPRNResponseDocument.class);
					callback.receiveResultgetRoundForUPRN((GetRoundForUPRNResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetRoundForUPRN(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[57].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[57].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * relevant roundname that collects this UPRN and 3 letter abbreviated
	 * service - Ref Rec Grn Gls Pls Box TRf TRc
	 *
	 * @see WSCollExternal#startgetRoundNameForUPRNService
	 * @param getRoundNameForUPRNService250
	 */
	@Override
	public void startgetRoundNameForUPRNService(GetRoundNameForUPRNServiceDocument getRoundNameForUPRNService250, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[61].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/getRoundNameForUPRNService");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getRoundNameForUPRNService250,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundNameForUPRNService")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getRoundNameForUPRNService"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetRoundNameForUPRNService(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundNameForUPRNService"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundNameForUPRNService"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "getRoundNameForUPRNService"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetRoundNameForUPRNService(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundNameForUPRNService(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundNameForUPRNService(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundNameForUPRNService(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundNameForUPRNService(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundNameForUPRNService(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundNameForUPRNService(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetRoundNameForUPRNService(f);
							}
						} else {
							callback.receiveErrorgetRoundNameForUPRNService(f);
						}
					} else {
						callback.receiveErrorgetRoundNameForUPRNService(f);
					}
				} else {
					callback.receiveErrorgetRoundNameForUPRNService(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetRoundNameForUPRNServiceResponseDocument.class);
					callback.receiveResultgetRoundNameForUPRNService((GetRoundNameForUPRNServiceResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetRoundNameForUPRNService(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[61].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[61].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations ** Requires
	 * Webaspx InCab. Returns the registrations of the vehicles out on the given
	 * date(ddMMyyyy) after the given timeReq
	 *
	 * @see WSCollExternal#startgetTrucksOutToday
	 * @param getTrucksOutToday220
	 */
	@Override
	public void startgetTrucksOutToday(GetTrucksOutTodayDocument getTrucksOutToday220, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[46].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetTrucksOutToday");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getTrucksOutToday220,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getTrucksOutToday")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetTrucksOutToday"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetTrucksOutToday(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetTrucksOutToday"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetTrucksOutToday"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetTrucksOutToday"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetTrucksOutToday(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetTrucksOutToday(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetTrucksOutToday(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetTrucksOutToday(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetTrucksOutToday(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetTrucksOutToday(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetTrucksOutToday(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetTrucksOutToday(f);
							}
						} else {
							callback.receiveErrorgetTrucksOutToday(f);
						}
					} else {
						callback.receiveErrorgetTrucksOutToday(f);
					}
				} else {
					callback.receiveErrorgetTrucksOutToday(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetTrucksOutTodayResponseDocument.class);
					callback.receiveResultgetTrucksOutToday((GetTrucksOutTodayResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetTrucksOutToday(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[46].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[46].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns the
	 * Zone AssistedStatus and bag indicator for a UPRN - used for certain
	 * clients CRM generated garden service
	 *
	 * @see WSCollExternal#startgetZoneBagAssist
	 * @param getZoneBagAssist184
	 */
	@Override
	public void startgetZoneBagAssist(GetZoneBagAssistDocument getZoneBagAssist184, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[28].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/GetZoneBagAssist");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), getZoneBagAssist184,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getZoneBagAssist")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetZoneBagAssist"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorgetZoneBagAssist(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetZoneBagAssist"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetZoneBagAssist"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "GetZoneBagAssist"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorgetZoneBagAssist(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetZoneBagAssist(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetZoneBagAssist(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetZoneBagAssist(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetZoneBagAssist(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetZoneBagAssist(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetZoneBagAssist(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorgetZoneBagAssist(f);
							}
						} else {
							callback.receiveErrorgetZoneBagAssist(f);
						}
					} else {
						callback.receiveErrorgetZoneBagAssist(f);
					}
				} else {
					callback.receiveErrorgetZoneBagAssist(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), GetZoneBagAssistResponseDocument.class);
					callback.receiveResultgetZoneBagAssist((GetZoneBagAssistResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorgetZoneBagAssist(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[28].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[28].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Inserts a
	 * 240L Garden bin with supplied parameters. For a specific bin use
	 * BinInsert webservice
	 *
	 * @see WSCollExternal#startinsertNewBin
	 * @param insertNewBin142
	 */
	@Override
	public void startinsertNewBin(InsertNewBinDocument insertNewBin142, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[7].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/insertNewBin");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), insertNewBin142,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "insertNewBin")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "insertNewBin"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorinsertNewBin(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "insertNewBin"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "insertNewBin"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "insertNewBin"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorinsertNewBin(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorinsertNewBin(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorinsertNewBin(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorinsertNewBin(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorinsertNewBin(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorinsertNewBin(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorinsertNewBin(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorinsertNewBin(f);
							}
						} else {
							callback.receiveErrorinsertNewBin(f);
						}
					} else {
						callback.receiveErrorinsertNewBin(f);
					}
				} else {
					callback.receiveErrorinsertNewBin(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), InsertNewBinResponseDocument.class);
					callback.receiveResultinsertNewBin((InsertNewBinResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorinsertNewBin(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[7].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[7].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations ** Internal
	 * Webaspx Use keeps database awake
	 *
	 * @see WSCollExternal#startkeepAliveCall
	 * @param keepAliveCall236
	 */
	@Override
	public void startkeepAliveCall(KeepAliveCallDocument keepAliveCall236, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[54].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/KeepAliveCall");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), keepAliveCall236,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "keepAliveCall")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "KeepAliveCall"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorkeepAliveCall(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "KeepAliveCall"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "KeepAliveCall"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "KeepAliveCall"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorkeepAliveCall(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorkeepAliveCall(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorkeepAliveCall(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorkeepAliveCall(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorkeepAliveCall(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorkeepAliveCall(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorkeepAliveCall(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorkeepAliveCall(f);
							}
						} else {
							callback.receiveErrorkeepAliveCall(f);
						}
					} else {
						callback.receiveErrorkeepAliveCall(f);
					}
				} else {
					callback.receiveErrorkeepAliveCall(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), KeepAliveCallResponseDocument.class);
					callback.receiveResultkeepAliveCall((KeepAliveCallResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorkeepAliveCall(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[54].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[54].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * UPRN, Address details and count of this binType for all UPRNs with Start
	 * and End Dates
	 *
	 * @see WSCollExternal#startlargeHouseholds
	 * @param largeHouseholds174
	 */
	@Override
	public void startlargeHouseholds(LargeHouseholdsDocument largeHouseholds174, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[23].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LargeHouseholds");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), largeHouseholds174,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "largeHouseholds")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LargeHouseholds"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorlargeHouseholds(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LargeHouseholds"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LargeHouseholds"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LargeHouseholds"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorlargeHouseholds(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlargeHouseholds(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlargeHouseholds(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlargeHouseholds(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlargeHouseholds(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlargeHouseholds(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlargeHouseholds(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlargeHouseholds(f);
							}
						} else {
							callback.receiveErrorlargeHouseholds(f);
						}
					} else {
						callback.receiveErrorlargeHouseholds(f);
					}
				} else {
					callback.receiveErrorlargeHouseholds(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), LargeHouseholdsResponseDocument.class);
					callback.receiveResultlargeHouseholds((LargeHouseholdsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorlargeHouseholds(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[23].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[23].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations **Requires
	 * Webaspx InCab. Returns latest issues and collection status from the last
	 * time a truck was out for each service for this UPRN. Also supplies GPS
	 * trial for each truck
	 *
	 * @see WSCollExternal#startlastCollected
	 * @param lastCollected190
	 */
	@Override
	public void startlastCollected(LastCollectedDocument lastCollected190, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[31].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LastCollected");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), lastCollected190,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lastCollected")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LastCollected"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorlastCollected(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LastCollected"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LastCollected"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LastCollected"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorlastCollected(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlastCollected(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlastCollected(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlastCollected(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlastCollected(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlastCollected(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlastCollected(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlastCollected(f);
							}
						} else {
							callback.receiveErrorlastCollected(f);
						}
					} else {
						callback.receiveErrorlastCollected(f);
					}
				} else {
					callback.receiveErrorlastCollected(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), LastCollectedResponseDocument.class);
					callback.receiveResultlastCollected((LastCollectedResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorlastCollected(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[31].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[31].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns
	 * current values for the given databaseFieldName for a hash delimited list
	 * of UPRNs
	 *
	 * @see WSCollExternal#startlLPGXtraGetUPRNCurrentValuesForDBField
	 * @param lLPGXtraGetUPRNCurrentValuesForDBField200
	 */
	@Override
	public void startlLPGXtraGetUPRNCurrentValuesForDBField(LLPGXtraGetUPRNCurrentValuesForDBFieldDocument lLPGXtraGetUPRNCurrentValuesForDBField200, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[36].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LLPGXtraGetUPRNCurrentValuesForDBField");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), lLPGXtraGetUPRNCurrentValuesForDBField200,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraGetUPRNCurrentValuesForDBField")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LLPGXtraGetUPRNCurrentValuesForDBField"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraGetUPRNCurrentValuesForDBField"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraGetUPRNCurrentValuesForDBField"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraGetUPRNCurrentValuesForDBField"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
							}
						} else {
							callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
						}
					} else {
						callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(f);
					}
				} else {
					callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.class);
					callback.receiveResultlLPGXtraGetUPRNCurrentValuesForDBField((LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorlLPGXtraGetUPRNCurrentValuesForDBField(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[36].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[36].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns a
	 * list of editable LLPGXtra fields by the LLPGXtraUpdateGetUPRNForDBField
	 * webservice
	 *
	 * @see WSCollExternal#startlLPGXtraUpdateGetAvailableFieldDetails
	 * @param lLPGXtraUpdateGetAvailableFieldDetails180
	 */
	@Override
	public void startlLPGXtraUpdateGetAvailableFieldDetails(LLPGXtraUpdateGetAvailableFieldDetailsDocument lLPGXtraUpdateGetAvailableFieldDetails180, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[26].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LLPGXtraUpdateGetAvailableFieldDetails");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), lLPGXtraUpdateGetAvailableFieldDetails180,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraUpdateGetAvailableFieldDetails")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LLPGXtraUpdateGetAvailableFieldDetails"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetAvailableFieldDetails"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap
										.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetAvailableFieldDetails"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetAvailableFieldDetails"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
							}
						} else {
							callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
						}
					} else {
						callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(f);
					}
				} else {
					callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.class);
					callback.receiveResultlLPGXtraUpdateGetAvailableFieldDetails((LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorlLPGXtraUpdateGetAvailableFieldDetails(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[26].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[26].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Updates the
	 * LLPGXtra databaseFieldName for a hash delimited list of UPRNs with the
	 * given newValue
	 *
	 * @see WSCollExternal#startlLPGXtraUpdateGetUPRNForDBField
	 * @param lLPGXtraUpdateGetUPRNForDBField224
	 */
	@Override
	public void startlLPGXtraUpdateGetUPRNForDBField(LLPGXtraUpdateGetUPRNForDBFieldDocument lLPGXtraUpdateGetUPRNForDBField224, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[48].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/LLPGXtraUpdateGetUPRNForDBField");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), lLPGXtraUpdateGetUPRNForDBField224,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lLPGXtraUpdateGetUPRNForDBField")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LLPGXtraUpdateGetUPRNForDBField"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetUPRNForDBField"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetUPRNForDBField"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "LLPGXtraUpdateGetUPRNForDBField"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
							}
						} else {
							callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
						}
					} else {
						callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(f);
					}
				} else {
					callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), LLPGXtraUpdateGetUPRNForDBFieldResponseDocument.class);
					callback.receiveResultlLPGXtraUpdateGetUPRNForDBField((LLPGXtraUpdateGetUPRNForDBFieldResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorlLPGXtraUpdateGetUPRNForDBField(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[48].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[48].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * live containers inbetween given dates
	 *
	 * @see WSCollExternal#startqueryBinEndDates
	 * @param queryBinEndDates208
	 */
	@Override
	public void startqueryBinEndDates(QueryBinEndDatesDocument queryBinEndDates208, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[40].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/QueryBinEndDates");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), queryBinEndDates208,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "queryBinEndDates")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "QueryBinEndDates"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorqueryBinEndDates(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinEndDates"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinEndDates"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinEndDates"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorqueryBinEndDates(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinEndDates(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinEndDates(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinEndDates(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinEndDates(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinEndDates(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinEndDates(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinEndDates(f);
							}
						} else {
							callback.receiveErrorqueryBinEndDates(f);
						}
					} else {
						callback.receiveErrorqueryBinEndDates(f);
					}
				} else {
					callback.receiveErrorqueryBinEndDates(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), QueryBinEndDatesResponseDocument.class);
					callback.receiveResultqueryBinEndDates((QueryBinEndDatesResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorqueryBinEndDates(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[40].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[40].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * live containers of a certain type
	 *
	 * @see WSCollExternal#startqueryBinOnType
	 * @param queryBinOnType148
	 */
	@Override
	public void startqueryBinOnType(QueryBinOnTypeDocument queryBinOnType148, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[10].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/QueryBinOnType");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), queryBinOnType148,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "queryBinOnType")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "QueryBinOnType"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorqueryBinOnType(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinOnType"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinOnType"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "QueryBinOnType"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorqueryBinOnType(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinOnType(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinOnType(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinOnType(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinOnType(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinOnType(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinOnType(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorqueryBinOnType(f);
							}
						} else {
							callback.receiveErrorqueryBinOnType(f);
						}
					} else {
						callback.receiveErrorqueryBinOnType(f);
					}
				} else {
					callback.receiveErrorqueryBinOnType(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), QueryBinOnTypeResponseDocument.class);
					callback.receiveResultqueryBinOnType((QueryBinOnTypeResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorqueryBinOnType(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[10].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[10].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Sets missed
	 * bin details by UPRN and service
	 *
	 * @see WSCollExternal#startreadWriteMissedBin
	 * @param readWriteMissedBin146
	 */
	@Override
	public void startreadWriteMissedBin(ReadWriteMissedBinDocument readWriteMissedBin146, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[9].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/readWriteMissedBin");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), readWriteMissedBin146,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBin")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBin"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorreadWriteMissedBin(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBin"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBin"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBin"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorreadWriteMissedBin(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBin(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBin(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBin(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBin(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBin(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBin(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBin(f);
							}
						} else {
							callback.receiveErrorreadWriteMissedBin(f);
						}
					} else {
						callback.receiveErrorreadWriteMissedBin(f);
					}
				} else {
					callback.receiveErrorreadWriteMissedBin(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), ReadWriteMissedBinResponseDocument.class);
					callback.receiveResultreadWriteMissedBin((ReadWriteMissedBinResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorreadWriteMissedBin(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[9].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[9].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Sets missed
	 * bin details by UPRN and container type
	 *
	 * @see WSCollExternal#startreadWriteMissedBinByContainerType
	 * @param readWriteMissedBinByContainerType226
	 */
	@Override
	public void startreadWriteMissedBinByContainerType(ReadWriteMissedBinByContainerTypeDocument readWriteMissedBinByContainerType226, final WSCollExternalCallbackHandler callback)
			throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[49].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/readWriteMissedBinByContainerType");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), readWriteMissedBinByContainerType226,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBinByContainerType")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBinByContainerType"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorreadWriteMissedBinByContainerType(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBinByContainerType"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBinByContainerType"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "readWriteMissedBinByContainerType"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorreadWriteMissedBinByContainerType(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBinByContainerType(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBinByContainerType(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBinByContainerType(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBinByContainerType(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBinByContainerType(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBinByContainerType(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorreadWriteMissedBinByContainerType(f);
							}
						} else {
							callback.receiveErrorreadWriteMissedBinByContainerType(f);
						}
					} else {
						callback.receiveErrorreadWriteMissedBinByContainerType(f);
					}
				} else {
					callback.receiveErrorreadWriteMissedBinByContainerType(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), ReadWriteMissedBinByContainerTypeResponseDocument.class);
					callback.receiveResultreadWriteMissedBinByContainerType((ReadWriteMissedBinByContainerTypeResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorreadWriteMissedBinByContainerType(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[49].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[49].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Returns all
	 * UPRNs with Address of all UPRNs with more than one standard container for
	 * the service requested with an optional start and end date. If service is
	 * blank or start/end date is blank, all will be returned
	 *
	 * @see WSCollExternal#startshowAdditionalBins
	 * @param showAdditionalBins206
	 */
	@Override
	public void startshowAdditionalBins(ShowAdditionalBinsDocument showAdditionalBins206, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[39].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/ShowAdditionalBins");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), showAdditionalBins206,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "showAdditionalBins")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ShowAdditionalBins"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorshowAdditionalBins(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ShowAdditionalBins"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ShowAdditionalBins"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ShowAdditionalBins"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorshowAdditionalBins(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorshowAdditionalBins(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorshowAdditionalBins(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorshowAdditionalBins(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorshowAdditionalBins(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorshowAdditionalBins(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorshowAdditionalBins(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorshowAdditionalBins(f);
							}
						} else {
							callback.receiveErrorshowAdditionalBins(f);
						}
					} else {
						callback.receiveErrorshowAdditionalBins(f);
					}
				} else {
					callback.receiveErrorshowAdditionalBins(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), ShowAdditionalBinsResponseDocument.class);
					callback.receiveResultshowAdditionalBins((ShowAdditionalBinsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorshowAdditionalBins(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[39].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[39].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Updates the
	 * assisted indicators in the collections database, sets assisted start and
	 * end dates and updates Comments for InCab with the assisted location. Will
	 * also update any open assisted Workflow cases or the last closed to
	 * replicate new value. Updates the assisted indicators in the collections
	 * database, sets assisted start and end dates and updates Comments for
	 * InCab with the assisted location
	 *
	 * @see WSCollExternal#startupdateAssisted
	 * @param updateAssisted166
	 */
	@Override
	public void startupdateAssisted(UpdateAssistedDocument updateAssisted166, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[19].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/UpdateAssisted");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), updateAssisted166,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "updateAssisted")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateAssisted"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorupdateAssisted(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateAssisted"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateAssisted"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateAssisted"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorupdateAssisted(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateAssisted(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateAssisted(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateAssisted(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateAssisted(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateAssisted(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateAssisted(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateAssisted(f);
							}
						} else {
							callback.receiveErrorupdateAssisted(f);
						}
					} else {
						callback.receiveErrorupdateAssisted(f);
					}
				} else {
					callback.receiveErrorupdateAssisted(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), UpdateAssistedResponseDocument.class);
					callback.receiveResultupdateAssisted((UpdateAssistedResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorupdateAssisted(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[19].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[19].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations Used by
	 * various CRMs SWN RGB for updating Garden Bin Details
	 *
	 * @see WSCollExternal#startupdateBinDetails
	 * @param updateBinDetails178
	 */
	@Override
	public void startupdateBinDetails(UpdateBinDetailsDocument updateBinDetails178, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[25].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/UpdateBinDetails");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), updateBinDetails178,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "updateBinDetails")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateBinDetails"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorupdateBinDetails(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateBinDetails"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateBinDetails"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateBinDetails"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorupdateBinDetails(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateBinDetails(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateBinDetails(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateBinDetails(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateBinDetails(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateBinDetails(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateBinDetails(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorupdateBinDetails(f);
							}
						} else {
							callback.receiveErrorupdateBinDetails(f);
						}
					} else {
						callback.receiveErrorupdateBinDetails(f);
					}
				} else {
					callback.receiveErrorupdateBinDetails(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), UpdateBinDetailsResponseDocument.class);
					callback.receiveResultupdateBinDetails((UpdateBinDetailsResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorupdateBinDetails(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[25].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[25].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	/**
	 * Auto generated method signature for Asynchronous Invocations **Internal
	 * Webaspx Use - writes various collected data to a collated table
	 *
	 * @see WSCollExternal#startwriteIncabLiveData
	 * @param writeIncabLiveData210
	 */
	@Override
	public void startwriteIncabLiveData(WriteIncabLiveDataDocument writeIncabLiveData210, final WSCollExternalCallbackHandler callback) throws java.rmi.RemoteException {

		org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[41].getName());
		_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/WriteIncabLiveData");
		_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

		addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

		// create SOAP envelope with that payload
		org.apache.axiom.soap.SOAPEnvelope env = null;
		final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

		// Style is Doc.

		env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), writeIncabLiveData210,
				optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "writeIncabLiveData")),
				new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "WriteIncabLiveData"));

		// adding SOAP soap_headers
		_serviceClient.addHeadersToEnvelope(env);
		// create message context with that soap envelope
		_messageContext.setEnvelope(env);

		// add the message context to the operation client
		_operationClient.addMessageContext(_messageContext);

		_operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {

			@Override
			public void onComplete() {
				try {
					_messageContext.getTransportOut().getSender().cleanup(_messageContext);
				} catch (org.apache.axis2.AxisFault axisFault) {
					callback.receiveErrorwriteIncabLiveData(axisFault);
				}
			}

			@Override
			public void onError(java.lang.Exception error) {
				if (error instanceof org.apache.axis2.AxisFault) {
					org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
					org.apache.axiom.om.OMElement faultElt = f.getDetail();
					if (faultElt != null) {
						if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WriteIncabLiveData"))) {
							// make the fault by reflection
							try {
								java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WriteIncabLiveData"));
								java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
								java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
								java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
								// message class
								java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WriteIncabLiveData"));
								java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
								java.lang.Object messageObject = fromOM(faultElt, messageClass);
								java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
								m.invoke(ex, new java.lang.Object[] { messageObject });

								callback.receiveErrorwriteIncabLiveData(new java.rmi.RemoteException(ex.getMessage(), ex));
							} catch (java.lang.ClassCastException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorwriteIncabLiveData(f);
							} catch (java.lang.ClassNotFoundException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorwriteIncabLiveData(f);
							} catch (java.lang.NoSuchMethodException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorwriteIncabLiveData(f);
							} catch (java.lang.reflect.InvocationTargetException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorwriteIncabLiveData(f);
							} catch (java.lang.IllegalAccessException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorwriteIncabLiveData(f);
							} catch (java.lang.InstantiationException e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorwriteIncabLiveData(f);
							} catch (org.apache.axis2.AxisFault e) {
								// we cannot intantiate the class - throw the
								// original Axis fault
								callback.receiveErrorwriteIncabLiveData(f);
							}
						} else {
							callback.receiveErrorwriteIncabLiveData(f);
						}
					} else {
						callback.receiveErrorwriteIncabLiveData(f);
					}
				} else {
					callback.receiveErrorwriteIncabLiveData(error);
				}
			}

			@Override
			public void onFault(org.apache.axis2.context.MessageContext faultContext) {
				org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
				onError(fault);
			}

			@Override
			public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
				try {
					org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

					java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(), WriteIncabLiveDataResponseDocument.class);
					callback.receiveResultwriteIncabLiveData((WriteIncabLiveDataResponseDocument) object);

				} catch (org.apache.axis2.AxisFault e) {
					callback.receiveErrorwriteIncabLiveData(e);
				}
			}
		});

		org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
		if (_operations[41].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
			_callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
			_operations[41].setMessageReceiver(_callbackReceiver);
		}

		// execute the operation client
		_operationClient.execute(false);
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AAHelloWorldStringTestDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AAHelloWorldXMLTestDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AHPChangeAddressDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AHPExtendSubscriptionDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AHPGetDetailsDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AHPNewUpdateDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AHPOrderNewBagsDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AHPRemoveSubscriptionDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, AHPSuspendSubscriptionDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, BinDeleteDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, BinInsertDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, BinUpdateDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, CachedCalendarDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, CheckAssistedDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, CommentsForIncabGetExistingDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, CommentsForIncabUpdateDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, CountBinsForServiceDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, CountSubscriptionsDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, DeleteBinDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GardenChangeAddressDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GardenRemoveSubscriptionDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GardenSubscribersDeliveriesCollectionsDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GardenSubscriptionDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GardenSubscriptionGetDetailsDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GardenSubscriptionWithNamePhoneEmailDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetAddressFromPostcodeDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetAddressOrUPRNDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetAllIssuesForIssueTypeDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetAllRoundDetailsDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetAllUPRNsForDateDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetAvailableContainersDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetAvailableContainersForClientOrPropertyDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetAvailableRoundsDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetBinDetailsForServiceDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetInCabIssueTextAndIssueCodeDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetIssuesAndCollectionStatusForUPRNDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetIssuesDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetIssuesForUPRNDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetLastTruckPositionDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetLatestTruckPositionsDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetNoCollectionDatesDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetPotentialCollectionDatesForNewGardenSubscriberDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetRoundCalendarForUPRNDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetRoundChangesInDateRangeByRoundOrUPRNDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetRoundForUPRNDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetRoundNameForUPRNServiceDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetTrucksOutTodayDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, GetZoneBagAssistDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, InsertNewBinDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, KeepAliveCallDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, LargeHouseholdsDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, LastCollectedDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, LLPGXtraGetUPRNCurrentValuesForDBFieldDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, LLPGXtraUpdateGetAvailableFieldDetailsDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, LLPGXtraUpdateGetUPRNForDBFieldDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, QueryBinEndDatesDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, QueryBinOnTypeDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ReadWriteMissedBinByContainerTypeDocument param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ReadWriteMissedBinDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ShowAdditionalBinsDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, UpdateAssistedDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, UpdateBinDetailsDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, WriteIncabLiveDataDocument param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {
		org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
		if (param != null) {
			envelope.getBody().addChild(toOM(param, optimizeContent));
		}
		return envelope;
	}

	private org.apache.axiom.om.OMElement toOM(final AAHelloWorldStringTestDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AAHelloWorldStringTestDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AAHelloWorldStringTestResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final AAHelloWorldXMLTestDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AAHelloWorldXMLTestDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AAHelloWorldXMLTestResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final AHPChangeAddressDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AHPChangeAddressDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AHPChangeAddressResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final AHPExtendSubscriptionDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AHPExtendSubscriptionDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AHPExtendSubscriptionResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final AHPGetDetailsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AHPGetDetailsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AHPGetDetailsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final AHPNewUpdateDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AHPNewUpdateDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AHPNewUpdateResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final AHPOrderNewBagsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AHPOrderNewBagsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AHPOrderNewBagsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final AHPRemoveSubscriptionDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AHPRemoveSubscriptionDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AHPRemoveSubscriptionResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final AHPSuspendSubscriptionDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(AHPSuspendSubscriptionDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final AHPSuspendSubscriptionResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final BinDeleteDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(BinDeleteDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final BinDeleteResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final BinInsertDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(BinInsertDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final BinInsertResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final BinUpdateDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(BinUpdateDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final BinUpdateResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final CachedCalendarDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(CachedCalendarDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final CachedCalendarResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final CheckAssistedDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(CheckAssistedDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final CheckAssistedResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final CommentsForIncabGetExistingDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(CommentsForIncabGetExistingDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final CommentsForIncabGetExistingResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final CommentsForIncabUpdateDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(CommentsForIncabUpdateDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final CommentsForIncabUpdateResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final CountBinsForServiceDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(CountBinsForServiceDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final CountBinsForServiceResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final CountSubscriptionsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(CountSubscriptionsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final CountSubscriptionsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final DeleteBinDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(DeleteBinDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final DeleteBinResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GardenChangeAddressDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GardenChangeAddressDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GardenChangeAddressResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GardenRemoveSubscriptionDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GardenRemoveSubscriptionDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GardenRemoveSubscriptionResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GardenSubscribersDeliveriesCollectionsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GardenSubscribersDeliveriesCollectionsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GardenSubscribersDeliveriesCollectionsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GardenSubscriptionDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GardenSubscriptionDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GardenSubscriptionGetDetailsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GardenSubscriptionGetDetailsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GardenSubscriptionGetDetailsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GardenSubscriptionResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GardenSubscriptionWithNamePhoneEmailDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GardenSubscriptionWithNamePhoneEmailDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GardenSubscriptionWithNamePhoneEmailResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetAddressFromPostcodeDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetAddressFromPostcodeDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetAddressFromPostcodeResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetAddressOrUPRNDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetAddressOrUPRNDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetAddressOrUPRNResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetAllIssuesForIssueTypeDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetAllIssuesForIssueTypeDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetAllIssuesForIssueTypeResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetAllRoundDetailsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetAllRoundDetailsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetAllRoundDetailsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetAllUPRNsForDateDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetAllUPRNsForDateDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetAllUPRNsForDateResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetAvailableContainersDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetAvailableContainersDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetAvailableContainersForClientOrPropertyDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetAvailableContainersForClientOrPropertyDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetAvailableContainersForClientOrPropertyResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetAvailableContainersResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetAvailableRoundsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetAvailableRoundsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetAvailableRoundsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetBinDetailsForServiceDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetBinDetailsForServiceDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetBinDetailsForServiceResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetInCabIssueTextAndIssueCodeDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetInCabIssueTextAndIssueCodeDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetInCabIssueTextAndIssueCodeResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetIssuesAndCollectionStatusForUPRNDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetIssuesAndCollectionStatusForUPRNDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetIssuesAndCollectionStatusForUPRNResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetIssuesDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetIssuesDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetIssuesForUPRNDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetIssuesForUPRNDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetIssuesForUPRNResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetIssuesResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetLastTruckPositionDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetLastTruckPositionDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetLastTruckPositionResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetLatestTruckPositionsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetLatestTruckPositionsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetLatestTruckPositionsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetNoCollectionDatesDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetNoCollectionDatesDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetNoCollectionDatesResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetPotentialCollectionDatesForNewGardenSubscriberDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetPotentialCollectionDatesForNewGardenSubscriberDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetPotentialCollectionDatesForNewGardenSubscriberResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetRoundAndBinInfoForUPRNForNewAdjustedDatesDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundCalendarForUPRNDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetRoundCalendarForUPRNDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundCalendarForUPRNResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundChangesInDateRangeByRoundOrUPRNDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetRoundChangesInDateRangeByRoundOrUPRNDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundChangesInDateRangeByRoundOrUPRNResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundForUPRNDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetRoundForUPRNDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundForUPRNResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundNameForUPRNServiceDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetRoundNameForUPRNServiceDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetRoundNameForUPRNServiceResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetTrucksOutTodayDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetTrucksOutTodayDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetTrucksOutTodayResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final GetZoneBagAssistDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(GetZoneBagAssistDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final GetZoneBagAssistResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final InsertNewBinDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(InsertNewBinDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final InsertNewBinResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final KeepAliveCallDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(KeepAliveCallDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final KeepAliveCallResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final LargeHouseholdsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(LargeHouseholdsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final LargeHouseholdsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final LastCollectedDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(LastCollectedDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final LastCollectedResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final LLPGXtraGetUPRNCurrentValuesForDBFieldDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(LLPGXtraGetUPRNCurrentValuesForDBFieldDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final LLPGXtraUpdateGetAvailableFieldDetailsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(LLPGXtraUpdateGetAvailableFieldDetailsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final LLPGXtraUpdateGetUPRNForDBFieldDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(LLPGXtraUpdateGetUPRNForDBFieldDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final LLPGXtraUpdateGetUPRNForDBFieldResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final QueryBinEndDatesDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(QueryBinEndDatesDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final QueryBinEndDatesResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final QueryBinOnTypeDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(QueryBinOnTypeDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final QueryBinOnTypeResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final ReadWriteMissedBinByContainerTypeDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(ReadWriteMissedBinByContainerTypeDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final ReadWriteMissedBinByContainerTypeResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final ReadWriteMissedBinDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(ReadWriteMissedBinDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final ReadWriteMissedBinResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final ShowAdditionalBinsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(ShowAdditionalBinsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final ShowAdditionalBinsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final UpdateAssistedDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(UpdateAssistedDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final UpdateAssistedResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final UpdateBinDetailsDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(UpdateBinDetailsDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final UpdateBinDetailsResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(final WriteIncabLiveDataDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(WriteIncabLiveDataDocument param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		return toOM(param);
	}

	private org.apache.axiom.om.OMElement toOM(final WriteIncabLiveDataResponseDocument param) throws org.apache.axis2.AxisFault {

		org.apache.axiom.om.OMXMLParserWrapper builder = org.apache.axiom.om.OMXMLBuilderFactory
				.createOMBuilder(new javax.xml.transform.sax.SAXSource(new org.apache.axis2.xmlbeans.XmlBeansXMLReader(param, _xmlOptions), new org.xml.sax.InputSource()));
		try {
			return builder.getDocumentElement(true);
		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/**
	 * Auto generated method signature Updates the assisted indicators in the
	 * collections database, sets assisted start and end dates and updates
	 * Comments for InCab with the assisted location. Will also update any open
	 * assisted Workflow cases or the last closed to replicate new value.
	 * Updates the assisted indicators in the collections database, sets
	 * assisted start and end dates and updates Comments for InCab with the
	 * assisted location
	 *
	 * @see WSCollExternal#updateAssisted
	 * @param updateAssisted166
	 */
	@Override
	public UpdateAssistedResponseDocument updateAssisted(UpdateAssistedDocument updateAssisted166) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[19].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/UpdateAssisted");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), updateAssisted166,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "updateAssisted")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateAssisted"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), UpdateAssistedResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (UpdateAssistedResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateAssisted"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateAssisted"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateAssisted"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature Used by various CRMs SWN RGB for updating
	 * Garden Bin Details
	 *
	 * @see WSCollExternal#updateBinDetails
	 * @param updateBinDetails178
	 */
	@Override
	public UpdateBinDetailsResponseDocument updateBinDetails(UpdateBinDetailsDocument updateBinDetails178) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[25].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/UpdateBinDetails");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), updateBinDetails178,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "updateBinDetails")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateBinDetails"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), UpdateBinDetailsResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (UpdateBinDetailsResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateBinDetails"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateBinDetails"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "UpdateBinDetails"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature **Internal Webaspx Use - writes various
	 * collected data to a collated table
	 *
	 * @see WSCollExternal#writeIncabLiveData
	 * @param writeIncabLiveData210
	 */
	@Override
	public WriteIncabLiveDataResponseDocument writeIncabLiveData(WriteIncabLiveDataDocument writeIncabLiveData210) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[41].getName());
			_operationClient.getOptions().setAction("http://webaspx-collections.azurewebsites.net/WriteIncabLiveData");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), writeIncabLiveData210,
					optimizeContent(new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "writeIncabLiveData")),
					new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "WriteIncabLiveData"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), WriteIncabLiveDataResponseDocument.class);
			org.apache.axis2.transport.TransportUtils.detachInputStream(_returnMessageContext);

			return (WriteIncabLiveDataResponseDocument) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WriteIncabLiveData"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WriteIncabLiveData"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WriteIncabLiveData"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}
}
