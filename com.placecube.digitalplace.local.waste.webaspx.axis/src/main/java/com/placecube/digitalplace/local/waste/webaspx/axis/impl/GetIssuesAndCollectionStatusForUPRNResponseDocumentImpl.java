/*
 * An XML document type.
 * Localname: getIssuesAndCollectionStatusForUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetIssuesAndCollectionStatusForUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNResponseDocument;

/**
 * A document containing one
 * getIssuesAndCollectionStatusForUPRNResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetIssuesAndCollectionStatusForUPRNResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesAndCollectionStatusForUPRNResponseDocument {

	/**
	 * An XML
	 * getIssuesAndCollectionStatusForUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetIssuesAndCollectionStatusForUPRNResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse {

		/**
		 * An XML
		 * getIssuesAndCollectionStatusForUPRNResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetIssuesAndCollectionStatusForUPRNResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult {

			private static final long serialVersionUID = 1L;

			public GetIssuesAndCollectionStatusForUPRNResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETISSUESANDCOLLECTIONSTATUSFORUPRNRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"getIssuesAndCollectionStatusForUPRNResult");

		private static final long serialVersionUID = 1L;

		public GetIssuesAndCollectionStatusForUPRNResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "getIssuesAndCollectionStatusForUPRNResult" element
		 */
		@Override
		public GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult addNewGetIssuesAndCollectionStatusForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult target = null;
				target = (GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult) get_store()
						.add_element_user(GETISSUESANDCOLLECTIONSTATUSFORUPRNRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getIssuesAndCollectionStatusForUPRNResult" element
		 */
		@Override
		public GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult getGetIssuesAndCollectionStatusForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult target = null;
				target = (GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult) get_store()
						.find_element_user(GETISSUESANDCOLLECTIONSTATUSFORUPRNRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getIssuesAndCollectionStatusForUPRNResult" element
		 */
		@Override
		public boolean isSetGetIssuesAndCollectionStatusForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETISSUESANDCOLLECTIONSTATUSFORUPRNRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getIssuesAndCollectionStatusForUPRNResult" element
		 */
		@Override
		public void setGetIssuesAndCollectionStatusForUPRNResult(
				GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult getIssuesAndCollectionStatusForUPRNResult) {
			generatedSetterHelperImpl(getIssuesAndCollectionStatusForUPRNResult, GETISSUESANDCOLLECTIONSTATUSFORUPRNRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getIssuesAndCollectionStatusForUPRNResult" element
		 */
		@Override
		public void unsetGetIssuesAndCollectionStatusForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETISSUESANDCOLLECTIONSTATUSFORUPRNRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETISSUESANDCOLLECTIONSTATUSFORUPRNRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"getIssuesAndCollectionStatusForUPRNResponse");

	private static final long serialVersionUID = 1L;

	public GetIssuesAndCollectionStatusForUPRNResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "getIssuesAndCollectionStatusForUPRNResponse" element
	 */
	@Override
	public GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse addNewGetIssuesAndCollectionStatusForUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse target = null;
			target = (GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse) get_store().add_element_user(GETISSUESANDCOLLECTIONSTATUSFORUPRNRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getIssuesAndCollectionStatusForUPRNResponse" element
	 */
	@Override
	public GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse getGetIssuesAndCollectionStatusForUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse target = null;
			target = (GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse) get_store().find_element_user(GETISSUESANDCOLLECTIONSTATUSFORUPRNRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getIssuesAndCollectionStatusForUPRNResponse" element
	 */
	@Override
	public void setGetIssuesAndCollectionStatusForUPRNResponse(
			GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse getIssuesAndCollectionStatusForUPRNResponse) {
		generatedSetterHelperImpl(getIssuesAndCollectionStatusForUPRNResponse, GETISSUESANDCOLLECTIONSTATUSFORUPRNRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
