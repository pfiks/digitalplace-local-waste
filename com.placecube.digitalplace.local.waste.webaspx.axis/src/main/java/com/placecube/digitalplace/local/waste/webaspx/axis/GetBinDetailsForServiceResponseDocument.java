/*
 * An XML document type.
 * Localname: GetBinDetailsForServiceResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetBinDetailsForServiceResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetBinDetailsForServiceResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetBinDetailsForServiceResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetBinDetailsForServiceResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getbindetailsforserviceresponse3388doctype");
    
    /**
     * Gets the "GetBinDetailsForServiceResponse" element
     */
    GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse getGetBinDetailsForServiceResponse();
    
    /**
     * Sets the "GetBinDetailsForServiceResponse" element
     */
    void setGetBinDetailsForServiceResponse(GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse getBinDetailsForServiceResponse);
    
    /**
     * Appends and returns a new empty "GetBinDetailsForServiceResponse" element
     */
    GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse addNewGetBinDetailsForServiceResponse();
    
    /**
     * An XML GetBinDetailsForServiceResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetBinDetailsForServiceResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetBinDetailsForServiceResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getbindetailsforserviceresponse56f2elemtype");
        
        /**
         * Gets the "GetBinDetailsForServiceResult" element
         */
        GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult getGetBinDetailsForServiceResult();
        
        /**
         * True if has "GetBinDetailsForServiceResult" element
         */
        boolean isSetGetBinDetailsForServiceResult();
        
        /**
         * Sets the "GetBinDetailsForServiceResult" element
         */
        void setGetBinDetailsForServiceResult(GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult getBinDetailsForServiceResult);
        
        /**
         * Appends and returns a new empty "GetBinDetailsForServiceResult" element
         */
        GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult addNewGetBinDetailsForServiceResult();
        
        /**
         * Unsets the "GetBinDetailsForServiceResult" element
         */
        void unsetGetBinDetailsForServiceResult();
        
        /**
         * An XML GetBinDetailsForServiceResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetBinDetailsForServiceResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetBinDetailsForServiceResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getbindetailsforserviceresult79f8elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult newInstance() {
                  return (GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse.GetBinDetailsForServiceResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse newInstance() {
              return (GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetBinDetailsForServiceResponseDocument.GetBinDetailsForServiceResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetBinDetailsForServiceResponseDocument newInstance() {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetBinDetailsForServiceResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetBinDetailsForServiceResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetBinDetailsForServiceResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetBinDetailsForServiceResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetBinDetailsForServiceResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetBinDetailsForServiceResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetBinDetailsForServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
