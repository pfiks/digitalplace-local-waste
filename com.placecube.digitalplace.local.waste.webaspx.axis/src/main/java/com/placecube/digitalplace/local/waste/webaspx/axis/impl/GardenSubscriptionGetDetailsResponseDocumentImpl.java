/*
 * An XML document type.
 * Localname: GardenSubscription_GetDetailsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscriptionGetDetailsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionGetDetailsResponseDocument;

/**
 * A document containing one
 * GardenSubscription_GetDetailsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GardenSubscriptionGetDetailsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenSubscriptionGetDetailsResponseDocument {

	/**
	 * An XML
	 * GardenSubscription_GetDetailsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GardenSubscriptionGetDetailsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse {

		/**
		 * An XML
		 * GardenSubscription_GetDetailsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GardenSubscriptionGetDetailsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult {

			private static final long serialVersionUID = 1L;

			public GardenSubscriptionGetDetailsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GARDENSUBSCRIPTIONGETDETAILSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"GardenSubscription_GetDetailsResult");

		private static final long serialVersionUID = 1L;

		public GardenSubscriptionGetDetailsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GardenSubscription_GetDetailsResult"
		 * element
		 */
		@Override
		public GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult addNewGardenSubscriptionGetDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult target = null;
				target = (GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult) get_store()
						.add_element_user(GARDENSUBSCRIPTIONGETDETAILSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GardenSubscription_GetDetailsResult" element
		 */
		@Override
		public GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult getGardenSubscriptionGetDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult target = null;
				target = (GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult) get_store()
						.find_element_user(GARDENSUBSCRIPTIONGETDETAILSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GardenSubscription_GetDetailsResult" element
		 */
		@Override
		public boolean isSetGardenSubscriptionGetDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GARDENSUBSCRIPTIONGETDETAILSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GardenSubscription_GetDetailsResult" element
		 */
		@Override
		public void setGardenSubscriptionGetDetailsResult(
				GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult gardenSubscriptionGetDetailsResult) {
			generatedSetterHelperImpl(gardenSubscriptionGetDetailsResult, GARDENSUBSCRIPTIONGETDETAILSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GardenSubscription_GetDetailsResult" element
		 */
		@Override
		public void unsetGardenSubscriptionGetDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GARDENSUBSCRIPTIONGETDETAILSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GARDENSUBSCRIPTIONGETDETAILSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"GardenSubscription_GetDetailsResponse");

	private static final long serialVersionUID = 1L;

	public GardenSubscriptionGetDetailsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GardenSubscription_GetDetailsResponse"
	 * element
	 */
	@Override
	public GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse addNewGardenSubscriptionGetDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse target = null;
			target = (GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse) get_store().add_element_user(GARDENSUBSCRIPTIONGETDETAILSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GardenSubscription_GetDetailsResponse" element
	 */
	@Override
	public GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse getGardenSubscriptionGetDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse target = null;
			target = (GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse) get_store().find_element_user(GARDENSUBSCRIPTIONGETDETAILSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GardenSubscription_GetDetailsResponse" element
	 */
	@Override
	public void setGardenSubscriptionGetDetailsResponse(GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse gardenSubscriptionGetDetailsResponse) {
		generatedSetterHelperImpl(gardenSubscriptionGetDetailsResponse, GARDENSUBSCRIPTIONGETDETAILSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
