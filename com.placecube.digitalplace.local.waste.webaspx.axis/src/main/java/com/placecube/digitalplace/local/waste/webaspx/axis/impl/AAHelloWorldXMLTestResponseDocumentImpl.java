/*
 * An XML document type.
 * Localname: AA_HelloWorld_XML_TestResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AAHelloWorldXMLTestResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AAHelloWorldXMLTestResponseDocument;

/**
 * A document containing one
 * AA_HelloWorld_XML_TestResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AAHelloWorldXMLTestResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AAHelloWorldXMLTestResponseDocument {

	/**
	 * An XML
	 * AA_HelloWorld_XML_TestResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AAHelloWorldXMLTestResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse {

		/**
		 * An XML
		 * AA_HelloWorld_XML_TestResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class AAHelloWorldXMLTestResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult {

			private static final long serialVersionUID = 1L;

			public AAHelloWorldXMLTestResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName AAHELLOWORLDXMLTESTRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AA_HelloWorld_XML_TestResult");

		private static final long serialVersionUID = 1L;

		public AAHelloWorldXMLTestResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "AA_HelloWorld_XML_TestResult"
		 * element
		 */
		@Override
		public AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult addNewAAHelloWorldXMLTestResult() {
			synchronized (monitor()) {
				check_orphaned();
				AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult target = null;
				target = (AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult) get_store().add_element_user(AAHELLOWORLDXMLTESTRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "AA_HelloWorld_XML_TestResult" element
		 */
		@Override
		public AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult getAAHelloWorldXMLTestResult() {
			synchronized (monitor()) {
				check_orphaned();
				AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult target = null;
				target = (AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult) get_store().find_element_user(AAHELLOWORLDXMLTESTRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "AA_HelloWorld_XML_TestResult" element
		 */
		@Override
		public boolean isSetAAHelloWorldXMLTestResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AAHELLOWORLDXMLTESTRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AA_HelloWorld_XML_TestResult" element
		 */
		@Override
		public void setAAHelloWorldXMLTestResult(AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse.AAHelloWorldXMLTestResult aaHelloWorldXMLTestResult) {
			generatedSetterHelperImpl(aaHelloWorldXMLTestResult, AAHELLOWORLDXMLTESTRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "AA_HelloWorld_XML_TestResult" element
		 */
		@Override
		public void unsetAAHelloWorldXMLTestResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AAHELLOWORLDXMLTESTRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName AAHELLOWORLDXMLTESTRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AA_HelloWorld_XML_TestResponse");

	private static final long serialVersionUID = 1L;

	public AAHelloWorldXMLTestResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AA_HelloWorld_XML_TestResponse" element
	 */
	@Override
	public AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse addNewAAHelloWorldXMLTestResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse target = null;
			target = (AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse) get_store().add_element_user(AAHELLOWORLDXMLTESTRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AA_HelloWorld_XML_TestResponse" element
	 */
	@Override
	public AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse getAAHelloWorldXMLTestResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse target = null;
			target = (AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse) get_store().find_element_user(AAHELLOWORLDXMLTESTRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AA_HelloWorld_XML_TestResponse" element
	 */
	@Override
	public void setAAHelloWorldXMLTestResponse(AAHelloWorldXMLTestResponseDocument.AAHelloWorldXMLTestResponse aaHelloWorldXMLTestResponse) {
		generatedSetterHelperImpl(aaHelloWorldXMLTestResponse, AAHELLOWORLDXMLTESTRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
