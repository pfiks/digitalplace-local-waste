/*
 * An XML document type.
 * Localname: GetAddressOrUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAddressOrUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressOrUPRNResponseDocument;

/**
 * A document containing one
 * GetAddressOrUPRNResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAddressOrUPRNResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAddressOrUPRNResponseDocument {

	/**
	 * An XML
	 * GetAddressOrUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAddressOrUPRNResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse {

		/**
		 * An XML
		 * GetAddressOrUPRNResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetAddressOrUPRNResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult {

			private static final long serialVersionUID = 1L;

			public GetAddressOrUPRNResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETADDRESSORUPRNRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAddressOrUPRNResult");

		private static final long serialVersionUID = 1L;

		public GetAddressOrUPRNResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetAddressOrUPRNResult" element
		 */
		@Override
		public GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult addNewGetAddressOrUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult target = null;
				target = (GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult) get_store().add_element_user(GETADDRESSORUPRNRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetAddressOrUPRNResult" element
		 */
		@Override
		public GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult getGetAddressOrUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult target = null;
				target = (GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult) get_store().find_element_user(GETADDRESSORUPRNRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetAddressOrUPRNResult" element
		 */
		@Override
		public boolean isSetGetAddressOrUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETADDRESSORUPRNRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetAddressOrUPRNResult" element
		 */
		@Override
		public void setGetAddressOrUPRNResult(GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult getAddressOrUPRNResult) {
			generatedSetterHelperImpl(getAddressOrUPRNResult, GETADDRESSORUPRNRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetAddressOrUPRNResult" element
		 */
		@Override
		public void unsetGetAddressOrUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETADDRESSORUPRNRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETADDRESSORUPRNRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAddressOrUPRNResponse");

	private static final long serialVersionUID = 1L;

	public GetAddressOrUPRNResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetAddressOrUPRNResponse" element
	 */
	@Override
	public GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse addNewGetAddressOrUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse target = null;
			target = (GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse) get_store().add_element_user(GETADDRESSORUPRNRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetAddressOrUPRNResponse" element
	 */
	@Override
	public GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse getGetAddressOrUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse target = null;
			target = (GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse) get_store().find_element_user(GETADDRESSORUPRNRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetAddressOrUPRNResponse" element
	 */
	@Override
	public void setGetAddressOrUPRNResponse(GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse getAddressOrUPRNResponse) {
		generatedSetterHelperImpl(getAddressOrUPRNResponse, GETADDRESSORUPRNRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
