/*
 * An XML document type.
 * Localname: getRoundNameForUPRNServiceResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetRoundNameForUPRNServiceResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getRoundNameForUPRNServiceResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetRoundNameForUPRNServiceResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundNameForUPRNServiceResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundnameforuprnserviceresponse7195doctype");
    
    /**
     * Gets the "getRoundNameForUPRNServiceResponse" element
     */
    GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse getGetRoundNameForUPRNServiceResponse();
    
    /**
     * Sets the "getRoundNameForUPRNServiceResponse" element
     */
    void setGetRoundNameForUPRNServiceResponse(GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse getRoundNameForUPRNServiceResponse);
    
    /**
     * Appends and returns a new empty "getRoundNameForUPRNServiceResponse" element
     */
    GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse addNewGetRoundNameForUPRNServiceResponse();
    
    /**
     * An XML getRoundNameForUPRNServiceResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetRoundNameForUPRNServiceResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundNameForUPRNServiceResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundnameforuprnserviceresponsebee0elemtype");
        
        /**
         * Gets the "getRoundNameForUPRNServiceResult" element
         */
        GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult getGetRoundNameForUPRNServiceResult();
        
        /**
         * True if has "getRoundNameForUPRNServiceResult" element
         */
        boolean isSetGetRoundNameForUPRNServiceResult();
        
        /**
         * Sets the "getRoundNameForUPRNServiceResult" element
         */
        void setGetRoundNameForUPRNServiceResult(GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult getRoundNameForUPRNServiceResult);
        
        /**
         * Appends and returns a new empty "getRoundNameForUPRNServiceResult" element
         */
        GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult addNewGetRoundNameForUPRNServiceResult();
        
        /**
         * Unsets the "getRoundNameForUPRNServiceResult" element
         */
        void unsetGetRoundNameForUPRNServiceResult();
        
        /**
         * An XML getRoundNameForUPRNServiceResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetRoundNameForUPRNServiceResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetRoundNameForUPRNServiceResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getroundnameforuprnserviceresultd851elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult newInstance() {
                  return (GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse newInstance() {
              return (GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetRoundNameForUPRNServiceResponseDocument newInstance() {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetRoundNameForUPRNServiceResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetRoundNameForUPRNServiceResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetRoundNameForUPRNServiceResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetRoundNameForUPRNServiceResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
