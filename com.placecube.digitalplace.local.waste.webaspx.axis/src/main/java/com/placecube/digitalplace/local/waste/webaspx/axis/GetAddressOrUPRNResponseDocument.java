/*
 * An XML document type.
 * Localname: GetAddressOrUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAddressOrUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetAddressOrUPRNResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetAddressOrUPRNResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAddressOrUPRNResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getaddressoruprnresponse9f35doctype");
    
    /**
     * Gets the "GetAddressOrUPRNResponse" element
     */
    GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse getGetAddressOrUPRNResponse();
    
    /**
     * Sets the "GetAddressOrUPRNResponse" element
     */
    void setGetAddressOrUPRNResponse(GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse getAddressOrUPRNResponse);
    
    /**
     * Appends and returns a new empty "GetAddressOrUPRNResponse" element
     */
    GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse addNewGetAddressOrUPRNResponse();
    
    /**
     * An XML GetAddressOrUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetAddressOrUPRNResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAddressOrUPRNResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getaddressoruprnresponse49e0elemtype");
        
        /**
         * Gets the "GetAddressOrUPRNResult" element
         */
        GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult getGetAddressOrUPRNResult();
        
        /**
         * True if has "GetAddressOrUPRNResult" element
         */
        boolean isSetGetAddressOrUPRNResult();
        
        /**
         * Sets the "GetAddressOrUPRNResult" element
         */
        void setGetAddressOrUPRNResult(GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult getAddressOrUPRNResult);
        
        /**
         * Appends and returns a new empty "GetAddressOrUPRNResult" element
         */
        GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult addNewGetAddressOrUPRNResult();
        
        /**
         * Unsets the "GetAddressOrUPRNResult" element
         */
        void unsetGetAddressOrUPRNResult();
        
        /**
         * An XML GetAddressOrUPRNResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetAddressOrUPRNResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAddressOrUPRNResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getaddressoruprnresultce31elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult newInstance() {
                  return (GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse.GetAddressOrUPRNResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse newInstance() {
              return (GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetAddressOrUPRNResponseDocument.GetAddressOrUPRNResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetAddressOrUPRNResponseDocument newInstance() {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetAddressOrUPRNResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetAddressOrUPRNResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetAddressOrUPRNResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetAddressOrUPRNResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetAddressOrUPRNResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetAddressOrUPRNResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetAddressOrUPRNResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetAddressOrUPRNResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetAddressOrUPRNResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetAddressOrUPRNResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetAddressOrUPRNResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetAddressOrUPRNResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetAddressOrUPRNResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetAddressOrUPRNResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetAddressOrUPRNResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAddressOrUPRNResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAddressOrUPRNResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAddressOrUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
