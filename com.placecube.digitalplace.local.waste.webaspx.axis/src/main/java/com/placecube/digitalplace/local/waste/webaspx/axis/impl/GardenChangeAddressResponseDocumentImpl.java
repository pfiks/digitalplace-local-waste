/*
 * An XML document type.
 * Localname: Garden_ChangeAddressResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenChangeAddressResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GardenChangeAddressResponseDocument;

/**
 * A document containing one
 * Garden_ChangeAddressResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GardenChangeAddressResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenChangeAddressResponseDocument {

	/**
	 * An XML
	 * Garden_ChangeAddressResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GardenChangeAddressResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenChangeAddressResponseDocument.GardenChangeAddressResponse {

		/**
		 * An XML
		 * Garden_ChangeAddressResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GardenChangeAddressResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GardenChangeAddressResponseDocument.GardenChangeAddressResponse.GardenChangeAddressResult {

			private static final long serialVersionUID = 1L;

			public GardenChangeAddressResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GARDENCHANGEADDRESSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Garden_ChangeAddressResult");

		private static final long serialVersionUID = 1L;

		public GardenChangeAddressResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "Garden_ChangeAddressResult" element
		 */
		@Override
		public GardenChangeAddressResponseDocument.GardenChangeAddressResponse.GardenChangeAddressResult addNewGardenChangeAddressResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenChangeAddressResponseDocument.GardenChangeAddressResponse.GardenChangeAddressResult target = null;
				target = (GardenChangeAddressResponseDocument.GardenChangeAddressResponse.GardenChangeAddressResult) get_store().add_element_user(GARDENCHANGEADDRESSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "Garden_ChangeAddressResult" element
		 */
		@Override
		public GardenChangeAddressResponseDocument.GardenChangeAddressResponse.GardenChangeAddressResult getGardenChangeAddressResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenChangeAddressResponseDocument.GardenChangeAddressResponse.GardenChangeAddressResult target = null;
				target = (GardenChangeAddressResponseDocument.GardenChangeAddressResponse.GardenChangeAddressResult) get_store().find_element_user(GARDENCHANGEADDRESSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "Garden_ChangeAddressResult" element
		 */
		@Override
		public boolean isSetGardenChangeAddressResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GARDENCHANGEADDRESSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "Garden_ChangeAddressResult" element
		 */
		@Override
		public void setGardenChangeAddressResult(GardenChangeAddressResponseDocument.GardenChangeAddressResponse.GardenChangeAddressResult gardenChangeAddressResult) {
			generatedSetterHelperImpl(gardenChangeAddressResult, GARDENCHANGEADDRESSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "Garden_ChangeAddressResult" element
		 */
		@Override
		public void unsetGardenChangeAddressResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GARDENCHANGEADDRESSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GARDENCHANGEADDRESSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Garden_ChangeAddressResponse");

	private static final long serialVersionUID = 1L;

	public GardenChangeAddressResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "Garden_ChangeAddressResponse" element
	 */
	@Override
	public GardenChangeAddressResponseDocument.GardenChangeAddressResponse addNewGardenChangeAddressResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenChangeAddressResponseDocument.GardenChangeAddressResponse target = null;
			target = (GardenChangeAddressResponseDocument.GardenChangeAddressResponse) get_store().add_element_user(GARDENCHANGEADDRESSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "Garden_ChangeAddressResponse" element
	 */
	@Override
	public GardenChangeAddressResponseDocument.GardenChangeAddressResponse getGardenChangeAddressResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenChangeAddressResponseDocument.GardenChangeAddressResponse target = null;
			target = (GardenChangeAddressResponseDocument.GardenChangeAddressResponse) get_store().find_element_user(GARDENCHANGEADDRESSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "Garden_ChangeAddressResponse" element
	 */
	@Override
	public void setGardenChangeAddressResponse(GardenChangeAddressResponseDocument.GardenChangeAddressResponse gardenChangeAddressResponse) {
		generatedSetterHelperImpl(gardenChangeAddressResponse, GARDENCHANGEADDRESSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
