/*
 * An XML document type.
 * Localname: AHP_NewUpdate
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPNewUpdateDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AHPNewUpdateDocument;

/**
 * A document containing one
 * AHP_NewUpdate(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class AHPNewUpdateDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPNewUpdateDocument {

	/**
	 * An XML AHP_NewUpdate(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AHPNewUpdateImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPNewUpdateDocument.AHPNewUpdate {

		private static final javax.xml.namespace.QName AHPADULTCOUNT$22 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ahpAdultCount");

		private static final javax.xml.namespace.QName AHPADULTENDDATE$24 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ahpAdultEndDate");

		private static final javax.xml.namespace.QName AHPCHILDCOUNT$28 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ahpChildCount");
		private static final javax.xml.namespace.QName AHPCHILDENDDATE$30 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ahpChildEndDate");
		private static final javax.xml.namespace.QName BAGORCADDY$16 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BagOrCaddy");
		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");
		private static final javax.xml.namespace.QName CRMAHPADULTREF$20 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "crmAhpAdultRef");
		private static final javax.xml.namespace.QName CRMAHPCHILDREF$26 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "crmAhpChildRef");
		private static final javax.xml.namespace.QName CRMENQUIRYID$32 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "crmEnquiryID");
		private static final javax.xml.namespace.QName CUSTEMAIL$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "custEmail");
		private static final javax.xml.namespace.QName CUSTNAME$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "custName");
		private static final javax.xml.namespace.QName CUSTPHONE$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "custPhone");
		private static final javax.xml.namespace.QName RELATION$18 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "relation");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName UPRN$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRN");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public AHPNewUpdateImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "ahpAdultCount" element
		 */
		@Override
		public java.lang.String getAhpAdultCount() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AHPADULTCOUNT$22, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "ahpAdultEndDate" element
		 */
		@Override
		public java.lang.String getAhpAdultEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AHPADULTENDDATE$24, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "ahpChildCount" element
		 */
		@Override
		public java.lang.String getAhpChildCount() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AHPCHILDCOUNT$28, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "ahpChildEndDate" element
		 */
		@Override
		public java.lang.String getAhpChildEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AHPCHILDENDDATE$30, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "BagOrCaddy" element
		 */
		@Override
		public java.lang.String getBagOrCaddy() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BAGORCADDY$16, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "crmAhpAdultRef" element
		 */
		@Override
		public java.lang.String getCrmAhpAdultRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CRMAHPADULTREF$20, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "crmAhpChildRef" element
		 */
		@Override
		public java.lang.String getCrmAhpChildRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CRMAHPCHILDREF$26, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "crmEnquiryID" element
		 */
		@Override
		public java.lang.String getCrmEnquiryID() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CRMENQUIRYID$32, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "custEmail" element
		 */
		@Override
		public java.lang.String getCustEmail() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CUSTEMAIL$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "custName" element
		 */
		@Override
		public java.lang.String getCustName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CUSTNAME$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "custPhone" element
		 */
		@Override
		public java.lang.String getCustPhone() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CUSTPHONE$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "relation" element
		 */
		@Override
		public java.lang.String getRelation() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(RELATION$18, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRN" element
		 */
		@Override
		public java.lang.String getUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "ahpAdultCount" element
		 */
		@Override
		public boolean isSetAhpAdultCount() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPADULTCOUNT$22) != 0;
			}
		}

		/**
		 * True if has "ahpAdultEndDate" element
		 */
		@Override
		public boolean isSetAhpAdultEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPADULTENDDATE$24) != 0;
			}
		}

		/**
		 * True if has "ahpChildCount" element
		 */
		@Override
		public boolean isSetAhpChildCount() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPCHILDCOUNT$28) != 0;
			}
		}

		/**
		 * True if has "ahpChildEndDate" element
		 */
		@Override
		public boolean isSetAhpChildEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPCHILDENDDATE$30) != 0;
			}
		}

		/**
		 * True if has "BagOrCaddy" element
		 */
		@Override
		public boolean isSetBagOrCaddy() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BAGORCADDY$16) != 0;
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "crmAhpAdultRef" element
		 */
		@Override
		public boolean isSetCrmAhpAdultRef() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CRMAHPADULTREF$20) != 0;
			}
		}

		/**
		 * True if has "crmAhpChildRef" element
		 */
		@Override
		public boolean isSetCrmAhpChildRef() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CRMAHPCHILDREF$26) != 0;
			}
		}

		/**
		 * True if has "crmEnquiryID" element
		 */
		@Override
		public boolean isSetCrmEnquiryID() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CRMENQUIRYID$32) != 0;
			}
		}

		/**
		 * True if has "custEmail" element
		 */
		@Override
		public boolean isSetCustEmail() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CUSTEMAIL$12) != 0;
			}
		}

		/**
		 * True if has "custName" element
		 */
		@Override
		public boolean isSetCustName() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CUSTNAME$10) != 0;
			}
		}

		/**
		 * True if has "custPhone" element
		 */
		@Override
		public boolean isSetCustPhone() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CUSTPHONE$14) != 0;
			}
		}

		/**
		 * True if has "relation" element
		 */
		@Override
		public boolean isSetRelation() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(RELATION$18) != 0;
			}
		}

		/**
		 * True if has "UPRN" element
		 */
		@Override
		public boolean isSetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRN$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "ahpAdultCount" element
		 */
		@Override
		public void setAhpAdultCount(java.lang.String ahpAdultCount) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AHPADULTCOUNT$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(AHPADULTCOUNT$22);
				}
				target.setStringValue(ahpAdultCount);
			}
		}

		/**
		 * Sets the "ahpAdultEndDate" element
		 */
		@Override
		public void setAhpAdultEndDate(java.lang.String ahpAdultEndDate) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AHPADULTENDDATE$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(AHPADULTENDDATE$24);
				}
				target.setStringValue(ahpAdultEndDate);
			}
		}

		/**
		 * Sets the "ahpChildCount" element
		 */
		@Override
		public void setAhpChildCount(java.lang.String ahpChildCount) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AHPCHILDCOUNT$28, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(AHPCHILDCOUNT$28);
				}
				target.setStringValue(ahpChildCount);
			}
		}

		/**
		 * Sets the "ahpChildEndDate" element
		 */
		@Override
		public void setAhpChildEndDate(java.lang.String ahpChildEndDate) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(AHPCHILDENDDATE$30, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(AHPCHILDENDDATE$30);
				}
				target.setStringValue(ahpChildEndDate);
			}
		}

		/**
		 * Sets the "BagOrCaddy" element
		 */
		@Override
		public void setBagOrCaddy(java.lang.String bagOrCaddy) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BAGORCADDY$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(BAGORCADDY$16);
				}
				target.setStringValue(bagOrCaddy);
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "crmAhpAdultRef" element
		 */
		@Override
		public void setCrmAhpAdultRef(java.lang.String crmAhpAdultRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CRMAHPADULTREF$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CRMAHPADULTREF$20);
				}
				target.setStringValue(crmAhpAdultRef);
			}
		}

		/**
		 * Sets the "crmAhpChildRef" element
		 */
		@Override
		public void setCrmAhpChildRef(java.lang.String crmAhpChildRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CRMAHPCHILDREF$26, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CRMAHPCHILDREF$26);
				}
				target.setStringValue(crmAhpChildRef);
			}
		}

		/**
		 * Sets the "crmEnquiryID" element
		 */
		@Override
		public void setCrmEnquiryID(java.lang.String crmEnquiryID) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CRMENQUIRYID$32, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CRMENQUIRYID$32);
				}
				target.setStringValue(crmEnquiryID);
			}
		}

		/**
		 * Sets the "custEmail" element
		 */
		@Override
		public void setCustEmail(java.lang.String custEmail) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CUSTEMAIL$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CUSTEMAIL$12);
				}
				target.setStringValue(custEmail);
			}
		}

		/**
		 * Sets the "custName" element
		 */
		@Override
		public void setCustName(java.lang.String custName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CUSTNAME$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CUSTNAME$10);
				}
				target.setStringValue(custName);
			}
		}

		/**
		 * Sets the "custPhone" element
		 */
		@Override
		public void setCustPhone(java.lang.String custPhone) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CUSTPHONE$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CUSTPHONE$14);
				}
				target.setStringValue(custPhone);
			}
		}

		/**
		 * Sets the "relation" element
		 */
		@Override
		public void setRelation(java.lang.String relation) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(RELATION$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(RELATION$18);
				}
				target.setStringValue(relation);
			}
		}

		/**
		 * Sets the "UPRN" element
		 */
		@Override
		public void setUPRN(java.lang.String uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRN$8);
				}
				target.setStringValue(uprn);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "ahpAdultCount" element
		 */
		@Override
		public void unsetAhpAdultCount() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPADULTCOUNT$22, 0);
			}
		}

		/**
		 * Unsets the "ahpAdultEndDate" element
		 */
		@Override
		public void unsetAhpAdultEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPADULTENDDATE$24, 0);
			}
		}

		/**
		 * Unsets the "ahpChildCount" element
		 */
		@Override
		public void unsetAhpChildCount() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPCHILDCOUNT$28, 0);
			}
		}

		/**
		 * Unsets the "ahpChildEndDate" element
		 */
		@Override
		public void unsetAhpChildEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPCHILDENDDATE$30, 0);
			}
		}

		/**
		 * Unsets the "BagOrCaddy" element
		 */
		@Override
		public void unsetBagOrCaddy() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BAGORCADDY$16, 0);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "crmAhpAdultRef" element
		 */
		@Override
		public void unsetCrmAhpAdultRef() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CRMAHPADULTREF$20, 0);
			}
		}

		/**
		 * Unsets the "crmAhpChildRef" element
		 */
		@Override
		public void unsetCrmAhpChildRef() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CRMAHPCHILDREF$26, 0);
			}
		}

		/**
		 * Unsets the "crmEnquiryID" element
		 */
		@Override
		public void unsetCrmEnquiryID() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CRMENQUIRYID$32, 0);
			}
		}

		/**
		 * Unsets the "custEmail" element
		 */
		@Override
		public void unsetCustEmail() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CUSTEMAIL$12, 0);
			}
		}

		/**
		 * Unsets the "custName" element
		 */
		@Override
		public void unsetCustName() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CUSTNAME$10, 0);
			}
		}

		/**
		 * Unsets the "custPhone" element
		 */
		@Override
		public void unsetCustPhone() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CUSTPHONE$14, 0);
			}
		}

		/**
		 * Unsets the "relation" element
		 */
		@Override
		public void unsetRelation() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(RELATION$18, 0);
			}
		}

		/**
		 * Unsets the "UPRN" element
		 */
		@Override
		public void unsetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRN$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "ahpAdultCount" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAhpAdultCount() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AHPADULTCOUNT$22, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "ahpAdultEndDate" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAhpAdultEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AHPADULTENDDATE$24, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "ahpChildCount" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAhpChildCount() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AHPCHILDCOUNT$28, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "ahpChildEndDate" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAhpChildEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AHPCHILDENDDATE$30, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "BagOrCaddy" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetBagOrCaddy() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BAGORCADDY$16, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "crmAhpAdultRef" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCrmAhpAdultRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CRMAHPADULTREF$20, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "crmAhpChildRef" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCrmAhpChildRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CRMAHPCHILDREF$26, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "crmEnquiryID" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCrmEnquiryID() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CRMENQUIRYID$32, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "custEmail" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCustEmail() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CUSTEMAIL$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "custName" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCustName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CUSTNAME$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "custPhone" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCustPhone() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CUSTPHONE$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "relation" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetRelation() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(RELATION$18, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "ahpAdultCount" element
		 */
		@Override
		public void xsetAhpAdultCount(org.apache.xmlbeans.XmlString ahpAdultCount) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AHPADULTCOUNT$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(AHPADULTCOUNT$22);
				}
				target.set(ahpAdultCount);
			}
		}

		/**
		 * Sets (as xml) the "ahpAdultEndDate" element
		 */
		@Override
		public void xsetAhpAdultEndDate(org.apache.xmlbeans.XmlString ahpAdultEndDate) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AHPADULTENDDATE$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(AHPADULTENDDATE$24);
				}
				target.set(ahpAdultEndDate);
			}
		}

		/**
		 * Sets (as xml) the "ahpChildCount" element
		 */
		@Override
		public void xsetAhpChildCount(org.apache.xmlbeans.XmlString ahpChildCount) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AHPCHILDCOUNT$28, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(AHPCHILDCOUNT$28);
				}
				target.set(ahpChildCount);
			}
		}

		/**
		 * Sets (as xml) the "ahpChildEndDate" element
		 */
		@Override
		public void xsetAhpChildEndDate(org.apache.xmlbeans.XmlString ahpChildEndDate) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(AHPCHILDENDDATE$30, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(AHPCHILDENDDATE$30);
				}
				target.set(ahpChildEndDate);
			}
		}

		/**
		 * Sets (as xml) the "BagOrCaddy" element
		 */
		@Override
		public void xsetBagOrCaddy(org.apache.xmlbeans.XmlString bagOrCaddy) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BAGORCADDY$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(BAGORCADDY$16);
				}
				target.set(bagOrCaddy);
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "crmAhpAdultRef" element
		 */
		@Override
		public void xsetCrmAhpAdultRef(org.apache.xmlbeans.XmlString crmAhpAdultRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CRMAHPADULTREF$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CRMAHPADULTREF$20);
				}
				target.set(crmAhpAdultRef);
			}
		}

		/**
		 * Sets (as xml) the "crmAhpChildRef" element
		 */
		@Override
		public void xsetCrmAhpChildRef(org.apache.xmlbeans.XmlString crmAhpChildRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CRMAHPCHILDREF$26, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CRMAHPCHILDREF$26);
				}
				target.set(crmAhpChildRef);
			}
		}

		/**
		 * Sets (as xml) the "crmEnquiryID" element
		 */
		@Override
		public void xsetCrmEnquiryID(org.apache.xmlbeans.XmlString crmEnquiryID) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CRMENQUIRYID$32, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CRMENQUIRYID$32);
				}
				target.set(crmEnquiryID);
			}
		}

		/**
		 * Sets (as xml) the "custEmail" element
		 */
		@Override
		public void xsetCustEmail(org.apache.xmlbeans.XmlString custEmail) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CUSTEMAIL$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CUSTEMAIL$12);
				}
				target.set(custEmail);
			}
		}

		/**
		 * Sets (as xml) the "custName" element
		 */
		@Override
		public void xsetCustName(org.apache.xmlbeans.XmlString custName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CUSTNAME$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CUSTNAME$10);
				}
				target.set(custName);
			}
		}

		/**
		 * Sets (as xml) the "custPhone" element
		 */
		@Override
		public void xsetCustPhone(org.apache.xmlbeans.XmlString custPhone) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CUSTPHONE$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CUSTPHONE$14);
				}
				target.set(custPhone);
			}
		}

		/**
		 * Sets (as xml) the "relation" element
		 */
		@Override
		public void xsetRelation(org.apache.xmlbeans.XmlString relation) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(RELATION$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(RELATION$18);
				}
				target.set(relation);
			}
		}

		/**
		 * Sets (as xml) the "UPRN" element
		 */
		@Override
		public void xsetUPRN(org.apache.xmlbeans.XmlString uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRN$8);
				}
				target.set(uprn);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName AHPNEWUPDATE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_NewUpdate");

	private static final long serialVersionUID = 1L;

	public AHPNewUpdateDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AHP_NewUpdate" element
	 */
	@Override
	public AHPNewUpdateDocument.AHPNewUpdate addNewAHPNewUpdate() {
		synchronized (monitor()) {
			check_orphaned();
			AHPNewUpdateDocument.AHPNewUpdate target = null;
			target = (AHPNewUpdateDocument.AHPNewUpdate) get_store().add_element_user(AHPNEWUPDATE$0);
			return target;
		}
	}

	/**
	 * Gets the "AHP_NewUpdate" element
	 */
	@Override
	public AHPNewUpdateDocument.AHPNewUpdate getAHPNewUpdate() {
		synchronized (monitor()) {
			check_orphaned();
			AHPNewUpdateDocument.AHPNewUpdate target = null;
			target = (AHPNewUpdateDocument.AHPNewUpdate) get_store().find_element_user(AHPNEWUPDATE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AHP_NewUpdate" element
	 */
	@Override
	public void setAHPNewUpdate(AHPNewUpdateDocument.AHPNewUpdate ahpNewUpdate) {
		generatedSetterHelperImpl(ahpNewUpdate, AHPNEWUPDATE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
