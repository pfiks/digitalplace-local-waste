/*
 * An XML document type.
 * Localname: GetAvailableRoundsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAvailableRoundsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableRoundsResponseDocument;

/**
 * A document containing one
 * GetAvailableRoundsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAvailableRoundsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAvailableRoundsResponseDocument {

	/**
	 * An XML
	 * GetAvailableRoundsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAvailableRoundsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse {

		/**
		 * An XML
		 * GetAvailableRoundsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetAvailableRoundsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse.GetAvailableRoundsResult {

			private static final long serialVersionUID = 1L;

			public GetAvailableRoundsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETAVAILABLEROUNDSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableRoundsResult");

		private static final long serialVersionUID = 1L;

		public GetAvailableRoundsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetAvailableRoundsResult" element
		 */
		@Override
		public GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse.GetAvailableRoundsResult addNewGetAvailableRoundsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse.GetAvailableRoundsResult target = null;
				target = (GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse.GetAvailableRoundsResult) get_store().add_element_user(GETAVAILABLEROUNDSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetAvailableRoundsResult" element
		 */
		@Override
		public GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse.GetAvailableRoundsResult getGetAvailableRoundsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse.GetAvailableRoundsResult target = null;
				target = (GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse.GetAvailableRoundsResult) get_store().find_element_user(GETAVAILABLEROUNDSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetAvailableRoundsResult" element
		 */
		@Override
		public boolean isSetGetAvailableRoundsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETAVAILABLEROUNDSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetAvailableRoundsResult" element
		 */
		@Override
		public void setGetAvailableRoundsResult(GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse.GetAvailableRoundsResult getAvailableRoundsResult) {
			generatedSetterHelperImpl(getAvailableRoundsResult, GETAVAILABLEROUNDSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetAvailableRoundsResult" element
		 */
		@Override
		public void unsetGetAvailableRoundsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETAVAILABLEROUNDSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETAVAILABLEROUNDSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableRoundsResponse");

	private static final long serialVersionUID = 1L;

	public GetAvailableRoundsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetAvailableRoundsResponse" element
	 */
	@Override
	public GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse addNewGetAvailableRoundsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse target = null;
			target = (GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse) get_store().add_element_user(GETAVAILABLEROUNDSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetAvailableRoundsResponse" element
	 */
	@Override
	public GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse getGetAvailableRoundsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse target = null;
			target = (GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse) get_store().find_element_user(GETAVAILABLEROUNDSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetAvailableRoundsResponse" element
	 */
	@Override
	public void setGetAvailableRoundsResponse(GetAvailableRoundsResponseDocument.GetAvailableRoundsResponse getAvailableRoundsResponse) {
		generatedSetterHelperImpl(getAvailableRoundsResponse, GETAVAILABLEROUNDSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
