/*
 * An XML document type.
 * Localname: GardenSubscriptionResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscriptionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionResponseDocument;

/**
 * A document containing one
 * GardenSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GardenSubscriptionResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenSubscriptionResponseDocument {

	/**
	 * An XML
	 * GardenSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GardenSubscriptionResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenSubscriptionResponseDocument.GardenSubscriptionResponse {

		/**
		 * An XML
		 * GardenSubscriptionResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GardenSubscriptionResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GardenSubscriptionResponseDocument.GardenSubscriptionResponse.GardenSubscriptionResult {

			private static final long serialVersionUID = 1L;

			public GardenSubscriptionResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GARDENSUBSCRIPTIONRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscriptionResult");

		private static final long serialVersionUID = 1L;

		public GardenSubscriptionResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GardenSubscriptionResult" element
		 */
		@Override
		public GardenSubscriptionResponseDocument.GardenSubscriptionResponse.GardenSubscriptionResult addNewGardenSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenSubscriptionResponseDocument.GardenSubscriptionResponse.GardenSubscriptionResult target = null;
				target = (GardenSubscriptionResponseDocument.GardenSubscriptionResponse.GardenSubscriptionResult) get_store().add_element_user(GARDENSUBSCRIPTIONRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GardenSubscriptionResult" element
		 */
		@Override
		public GardenSubscriptionResponseDocument.GardenSubscriptionResponse.GardenSubscriptionResult getGardenSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenSubscriptionResponseDocument.GardenSubscriptionResponse.GardenSubscriptionResult target = null;
				target = (GardenSubscriptionResponseDocument.GardenSubscriptionResponse.GardenSubscriptionResult) get_store().find_element_user(GARDENSUBSCRIPTIONRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GardenSubscriptionResult" element
		 */
		@Override
		public boolean isSetGardenSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GARDENSUBSCRIPTIONRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GardenSubscriptionResult" element
		 */
		@Override
		public void setGardenSubscriptionResult(GardenSubscriptionResponseDocument.GardenSubscriptionResponse.GardenSubscriptionResult gardenSubscriptionResult) {
			generatedSetterHelperImpl(gardenSubscriptionResult, GARDENSUBSCRIPTIONRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GardenSubscriptionResult" element
		 */
		@Override
		public void unsetGardenSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GARDENSUBSCRIPTIONRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GARDENSUBSCRIPTIONRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscriptionResponse");

	private static final long serialVersionUID = 1L;

	public GardenSubscriptionResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GardenSubscriptionResponse" element
	 */
	@Override
	public GardenSubscriptionResponseDocument.GardenSubscriptionResponse addNewGardenSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscriptionResponseDocument.GardenSubscriptionResponse target = null;
			target = (GardenSubscriptionResponseDocument.GardenSubscriptionResponse) get_store().add_element_user(GARDENSUBSCRIPTIONRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GardenSubscriptionResponse" element
	 */
	@Override
	public GardenSubscriptionResponseDocument.GardenSubscriptionResponse getGardenSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscriptionResponseDocument.GardenSubscriptionResponse target = null;
			target = (GardenSubscriptionResponseDocument.GardenSubscriptionResponse) get_store().find_element_user(GARDENSUBSCRIPTIONRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GardenSubscriptionResponse" element
	 */
	@Override
	public void setGardenSubscriptionResponse(GardenSubscriptionResponseDocument.GardenSubscriptionResponse gardenSubscriptionResponse) {
		generatedSetterHelperImpl(gardenSubscriptionResponse, GARDENSUBSCRIPTIONRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
