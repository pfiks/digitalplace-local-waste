/*
 * An XML document type.
 * Localname: CheckAssistedResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CheckAssistedResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.CheckAssistedResponseDocument;

/**
 * A document containing one
 * CheckAssistedResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class CheckAssistedResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CheckAssistedResponseDocument {

	/**
	 * An XML
	 * CheckAssistedResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class CheckAssistedResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CheckAssistedResponseDocument.CheckAssistedResponse {

		/**
		 * An XML
		 * CheckAssistedResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class CheckAssistedResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CheckAssistedResponseDocument.CheckAssistedResponse.CheckAssistedResult {

			private static final long serialVersionUID = 1L;

			public CheckAssistedResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName CHECKASSISTEDRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CheckAssistedResult");

		private static final long serialVersionUID = 1L;

		public CheckAssistedResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "CheckAssistedResult" element
		 */
		@Override
		public CheckAssistedResponseDocument.CheckAssistedResponse.CheckAssistedResult addNewCheckAssistedResult() {
			synchronized (monitor()) {
				check_orphaned();
				CheckAssistedResponseDocument.CheckAssistedResponse.CheckAssistedResult target = null;
				target = (CheckAssistedResponseDocument.CheckAssistedResponse.CheckAssistedResult) get_store().add_element_user(CHECKASSISTEDRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "CheckAssistedResult" element
		 */
		@Override
		public CheckAssistedResponseDocument.CheckAssistedResponse.CheckAssistedResult getCheckAssistedResult() {
			synchronized (monitor()) {
				check_orphaned();
				CheckAssistedResponseDocument.CheckAssistedResponse.CheckAssistedResult target = null;
				target = (CheckAssistedResponseDocument.CheckAssistedResponse.CheckAssistedResult) get_store().find_element_user(CHECKASSISTEDRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "CheckAssistedResult" element
		 */
		@Override
		public boolean isSetCheckAssistedResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CHECKASSISTEDRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "CheckAssistedResult" element
		 */
		@Override
		public void setCheckAssistedResult(CheckAssistedResponseDocument.CheckAssistedResponse.CheckAssistedResult checkAssistedResult) {
			generatedSetterHelperImpl(checkAssistedResult, CHECKASSISTEDRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "CheckAssistedResult" element
		 */
		@Override
		public void unsetCheckAssistedResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CHECKASSISTEDRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName CHECKASSISTEDRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CheckAssistedResponse");

	private static final long serialVersionUID = 1L;

	public CheckAssistedResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "CheckAssistedResponse" element
	 */
	@Override
	public CheckAssistedResponseDocument.CheckAssistedResponse addNewCheckAssistedResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CheckAssistedResponseDocument.CheckAssistedResponse target = null;
			target = (CheckAssistedResponseDocument.CheckAssistedResponse) get_store().add_element_user(CHECKASSISTEDRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "CheckAssistedResponse" element
	 */
	@Override
	public CheckAssistedResponseDocument.CheckAssistedResponse getCheckAssistedResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CheckAssistedResponseDocument.CheckAssistedResponse target = null;
			target = (CheckAssistedResponseDocument.CheckAssistedResponse) get_store().find_element_user(CHECKASSISTEDRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "CheckAssistedResponse" element
	 */
	@Override
	public void setCheckAssistedResponse(CheckAssistedResponseDocument.CheckAssistedResponse checkAssistedResponse) {
		generatedSetterHelperImpl(checkAssistedResponse, CHECKASSISTEDRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
