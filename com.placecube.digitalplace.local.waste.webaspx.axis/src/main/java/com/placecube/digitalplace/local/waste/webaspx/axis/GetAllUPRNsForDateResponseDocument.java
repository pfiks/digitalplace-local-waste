/*
 * An XML document type.
 * Localname: GetAllUPRNsForDateResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAllUPRNsForDateResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetAllUPRNsForDateResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetAllUPRNsForDateResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllUPRNsForDateResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getalluprnsfordateresponsee3c3doctype");
    
    /**
     * Gets the "GetAllUPRNsForDateResponse" element
     */
    GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse getGetAllUPRNsForDateResponse();
    
    /**
     * Sets the "GetAllUPRNsForDateResponse" element
     */
    void setGetAllUPRNsForDateResponse(GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse getAllUPRNsForDateResponse);
    
    /**
     * Appends and returns a new empty "GetAllUPRNsForDateResponse" element
     */
    GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse addNewGetAllUPRNsForDateResponse();
    
    /**
     * An XML GetAllUPRNsForDateResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetAllUPRNsForDateResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllUPRNsForDateResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getalluprnsfordateresponse21a0elemtype");
        
        /**
         * Gets the "GetAllUPRNsForDateResult" element
         */
        GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult getGetAllUPRNsForDateResult();
        
        /**
         * True if has "GetAllUPRNsForDateResult" element
         */
        boolean isSetGetAllUPRNsForDateResult();
        
        /**
         * Sets the "GetAllUPRNsForDateResult" element
         */
        void setGetAllUPRNsForDateResult(GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult getAllUPRNsForDateResult);
        
        /**
         * Appends and returns a new empty "GetAllUPRNsForDateResult" element
         */
        GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult addNewGetAllUPRNsForDateResult();
        
        /**
         * Unsets the "GetAllUPRNsForDateResult" element
         */
        void unsetGetAllUPRNsForDateResult();
        
        /**
         * An XML GetAllUPRNsForDateResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetAllUPRNsForDateResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetAllUPRNsForDateResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getalluprnsfordateresultdc3felemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult newInstance() {
                  return (GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse.GetAllUPRNsForDateResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse newInstance() {
              return (GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetAllUPRNsForDateResponseDocument.GetAllUPRNsForDateResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetAllUPRNsForDateResponseDocument newInstance() {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetAllUPRNsForDateResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetAllUPRNsForDateResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetAllUPRNsForDateResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetAllUPRNsForDateResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAllUPRNsForDateResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetAllUPRNsForDateResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetAllUPRNsForDateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
