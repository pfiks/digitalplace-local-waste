/*
 * An XML document type.
 * Localname: getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument;

/**
 * A document containing one
 * getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
		implements GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument {

	/**
	 * An XML
	 * getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse {

		/**
		 * An XML
		 * getRoundAndBinInfoForUPRNForNewAdjustedDatesResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetRoundAndBinInfoForUPRNForNewAdjustedDatesResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult {

			private static final long serialVersionUID = 1L;

			public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"getRoundAndBinInfoForUPRNForNewAdjustedDatesResult");

		private static final long serialVersionUID = 1L;

		public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult" element
		 */
		@Override
		public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult addNewGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult target = null;
				target = (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult) get_store()
						.add_element_user(GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult" element
		 */
		@Override
		public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult getGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult target = null;
				target = (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult) get_store()
						.find_element_user(GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult"
		 * element
		 */
		@Override
		public boolean isSetGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult" element
		 */
		@Override
		public void setGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult(
				GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResult getRoundAndBinInfoForUPRNForNewAdjustedDatesResult) {
			generatedSetterHelperImpl(getRoundAndBinInfoForUPRNForNewAdjustedDatesResult, GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResult"
		 * element
		 */
		@Override
		public void unsetGetRoundAndBinInfoForUPRNForNewAdjustedDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse");

	private static final long serialVersionUID = 1L;

	public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse" element
	 */
	@Override
	public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse addNewGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse target = null;
			target = (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse) get_store()
					.add_element_user(GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse" element
	 */
	@Override
	public GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse getGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse target = null;
			target = (GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse) get_store()
					.find_element_user(GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse" element
	 */
	@Override
	public void setGetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse(
			GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponseDocument.GetRoundAndBinInfoForUPRNForNewAdjustedDatesResponse getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse) {
		generatedSetterHelperImpl(getRoundAndBinInfoForUPRNForNewAdjustedDatesResponse, GETROUNDANDBININFOFORUPRNFORNEWADJUSTEDDATESRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
