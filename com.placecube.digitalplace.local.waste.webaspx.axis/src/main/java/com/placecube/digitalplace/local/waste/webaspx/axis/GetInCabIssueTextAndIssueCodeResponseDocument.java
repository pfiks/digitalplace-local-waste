/*
 * An XML document type.
 * Localname: GetInCabIssueTextAndIssueCodeResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetInCabIssueTextAndIssueCodeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GetInCabIssueTextAndIssueCodeResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetInCabIssueTextAndIssueCodeResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetInCabIssueTextAndIssueCodeResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getincabissuetextandissuecoderesponse8137doctype");
    
    /**
     * Gets the "GetInCabIssueTextAndIssueCodeResponse" element
     */
    GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse getGetInCabIssueTextAndIssueCodeResponse();
    
    /**
     * Sets the "GetInCabIssueTextAndIssueCodeResponse" element
     */
    void setGetInCabIssueTextAndIssueCodeResponse(GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse getInCabIssueTextAndIssueCodeResponse);
    
    /**
     * Appends and returns a new empty "GetInCabIssueTextAndIssueCodeResponse" element
     */
    GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse addNewGetInCabIssueTextAndIssueCodeResponse();
    
    /**
     * An XML GetInCabIssueTextAndIssueCodeResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetInCabIssueTextAndIssueCodeResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetInCabIssueTextAndIssueCodeResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getincabissuetextandissuecoderesponseeb10elemtype");
        
        /**
         * Gets the "GetInCabIssueTextAndIssueCodeResult" element
         */
        GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult getGetInCabIssueTextAndIssueCodeResult();
        
        /**
         * True if has "GetInCabIssueTextAndIssueCodeResult" element
         */
        boolean isSetGetInCabIssueTextAndIssueCodeResult();
        
        /**
         * Sets the "GetInCabIssueTextAndIssueCodeResult" element
         */
        void setGetInCabIssueTextAndIssueCodeResult(GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult getInCabIssueTextAndIssueCodeResult);
        
        /**
         * Appends and returns a new empty "GetInCabIssueTextAndIssueCodeResult" element
         */
        GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult addNewGetInCabIssueTextAndIssueCodeResult();
        
        /**
         * Unsets the "GetInCabIssueTextAndIssueCodeResult" element
         */
        void unsetGetInCabIssueTextAndIssueCodeResult();
        
        /**
         * An XML GetInCabIssueTextAndIssueCodeResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetInCabIssueTextAndIssueCodeResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetInCabIssueTextAndIssueCodeResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getincabissuetextandissuecoderesultc7c5elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult newInstance() {
                  return (GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse newInstance() {
              return (GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetInCabIssueTextAndIssueCodeResponseDocument newInstance() {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetInCabIssueTextAndIssueCodeResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetInCabIssueTextAndIssueCodeResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
