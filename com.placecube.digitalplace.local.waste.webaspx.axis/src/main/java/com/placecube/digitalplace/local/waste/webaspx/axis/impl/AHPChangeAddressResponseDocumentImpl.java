/*
 * An XML document type.
 * Localname: AHP_ChangeAddressResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPChangeAddressResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AHPChangeAddressResponseDocument;

/**
 * A document containing one
 * AHP_ChangeAddressResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AHPChangeAddressResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPChangeAddressResponseDocument {

	/**
	 * An XML
	 * AHP_ChangeAddressResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AHPChangeAddressResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPChangeAddressResponseDocument.AHPChangeAddressResponse {

		/**
		 * An XML
		 * AHP_ChangeAddressResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class AHPChangeAddressResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult {

			private static final long serialVersionUID = 1L;

			public AHPChangeAddressResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName AHPCHANGEADDRESSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_ChangeAddressResult");

		private static final long serialVersionUID = 1L;

		public AHPChangeAddressResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "AHP_ChangeAddressResult" element
		 */
		@Override
		public AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult addNewAHPChangeAddressResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult target = null;
				target = (AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult) get_store().add_element_user(AHPCHANGEADDRESSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "AHP_ChangeAddressResult" element
		 */
		@Override
		public AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult getAHPChangeAddressResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult target = null;
				target = (AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult) get_store().find_element_user(AHPCHANGEADDRESSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "AHP_ChangeAddressResult" element
		 */
		@Override
		public boolean isSetAHPChangeAddressResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPCHANGEADDRESSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AHP_ChangeAddressResult" element
		 */
		@Override
		public void setAHPChangeAddressResult(AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult ahpChangeAddressResult) {
			generatedSetterHelperImpl(ahpChangeAddressResult, AHPCHANGEADDRESSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "AHP_ChangeAddressResult" element
		 */
		@Override
		public void unsetAHPChangeAddressResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPCHANGEADDRESSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName AHPCHANGEADDRESSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_ChangeAddressResponse");

	private static final long serialVersionUID = 1L;

	public AHPChangeAddressResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AHP_ChangeAddressResponse" element
	 */
	@Override
	public AHPChangeAddressResponseDocument.AHPChangeAddressResponse addNewAHPChangeAddressResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPChangeAddressResponseDocument.AHPChangeAddressResponse target = null;
			target = (AHPChangeAddressResponseDocument.AHPChangeAddressResponse) get_store().add_element_user(AHPCHANGEADDRESSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AHP_ChangeAddressResponse" element
	 */
	@Override
	public AHPChangeAddressResponseDocument.AHPChangeAddressResponse getAHPChangeAddressResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPChangeAddressResponseDocument.AHPChangeAddressResponse target = null;
			target = (AHPChangeAddressResponseDocument.AHPChangeAddressResponse) get_store().find_element_user(AHPCHANGEADDRESSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AHP_ChangeAddressResponse" element
	 */
	@Override
	public void setAHPChangeAddressResponse(AHPChangeAddressResponseDocument.AHPChangeAddressResponse ahpChangeAddressResponse) {
		generatedSetterHelperImpl(ahpChangeAddressResponse, AHPCHANGEADDRESSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
