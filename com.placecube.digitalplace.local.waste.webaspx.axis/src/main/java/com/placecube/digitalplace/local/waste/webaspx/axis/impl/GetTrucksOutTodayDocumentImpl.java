/*
 * An XML document type.
 * Localname: GetTrucksOutToday
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetTrucksOutTodayDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayDocument;

/**
 * A document containing one
 * GetTrucksOutToday(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class GetTrucksOutTodayDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetTrucksOutTodayDocument {

	/**
	 * An XML GetTrucksOutToday(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetTrucksOutTodayImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetTrucksOutTodayDocument.GetTrucksOutToday {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName DATEREQ$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "dateReq");

		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName TIMEREQ$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "timeReq");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public GetTrucksOutTodayImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "dateReq" element
		 */
		@Override
		public java.lang.String getDateReq() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATEREQ$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "timeReq" element
		 */
		@Override
		public java.lang.String getTimeReq() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TIMEREQ$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "dateReq" element
		 */
		@Override
		public boolean isSetDateReq() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DATEREQ$8) != 0;
			}
		}

		/**
		 * True if has "timeReq" element
		 */
		@Override
		public boolean isSetTimeReq() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(TIMEREQ$10) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "dateReq" element
		 */
		@Override
		public void setDateReq(java.lang.String dateReq) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATEREQ$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DATEREQ$8);
				}
				target.setStringValue(dateReq);
			}
		}

		/**
		 * Sets the "timeReq" element
		 */
		@Override
		public void setTimeReq(java.lang.String timeReq) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TIMEREQ$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(TIMEREQ$10);
				}
				target.setStringValue(timeReq);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "dateReq" element
		 */
		@Override
		public void unsetDateReq() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DATEREQ$8, 0);
			}
		}

		/**
		 * Unsets the "timeReq" element
		 */
		@Override
		public void unsetTimeReq() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(TIMEREQ$10, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "dateReq" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDateReq() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATEREQ$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "timeReq" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetTimeReq() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TIMEREQ$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "dateReq" element
		 */
		@Override
		public void xsetDateReq(org.apache.xmlbeans.XmlString dateReq) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATEREQ$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DATEREQ$8);
				}
				target.set(dateReq);
			}
		}

		/**
		 * Sets (as xml) the "timeReq" element
		 */
		@Override
		public void xsetTimeReq(org.apache.xmlbeans.XmlString timeReq) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TIMEREQ$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(TIMEREQ$10);
				}
				target.set(timeReq);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName GETTRUCKSOUTTODAY$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetTrucksOutToday");

	private static final long serialVersionUID = 1L;

	public GetTrucksOutTodayDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetTrucksOutToday" element
	 */
	@Override
	public GetTrucksOutTodayDocument.GetTrucksOutToday addNewGetTrucksOutToday() {
		synchronized (monitor()) {
			check_orphaned();
			GetTrucksOutTodayDocument.GetTrucksOutToday target = null;
			target = (GetTrucksOutTodayDocument.GetTrucksOutToday) get_store().add_element_user(GETTRUCKSOUTTODAY$0);
			return target;
		}
	}

	/**
	 * Gets the "GetTrucksOutToday" element
	 */
	@Override
	public GetTrucksOutTodayDocument.GetTrucksOutToday getGetTrucksOutToday() {
		synchronized (monitor()) {
			check_orphaned();
			GetTrucksOutTodayDocument.GetTrucksOutToday target = null;
			target = (GetTrucksOutTodayDocument.GetTrucksOutToday) get_store().find_element_user(GETTRUCKSOUTTODAY$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetTrucksOutToday" element
	 */
	@Override
	public void setGetTrucksOutToday(GetTrucksOutTodayDocument.GetTrucksOutToday getTrucksOutToday) {
		generatedSetterHelperImpl(getTrucksOutToday, GETTRUCKSOUTTODAY$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
