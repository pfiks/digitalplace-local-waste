/*
 * An XML document type.
 * Localname: readWriteMissedBinResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: ReadWriteMissedBinResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinResponseDocument;

/**
 * A document containing one
 * readWriteMissedBinResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class ReadWriteMissedBinResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ReadWriteMissedBinResponseDocument {

	/**
	 * An XML
	 * readWriteMissedBinResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class ReadWriteMissedBinResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ReadWriteMissedBinResponseDocument.ReadWriteMissedBinResponse {

		private static final javax.xml.namespace.QName READWRITEMISSEDBINRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBinResult");

		private static final long serialVersionUID = 1L;

		public ReadWriteMissedBinResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "readWriteMissedBinResult" element
		 */
		@Override
		public java.lang.String getReadWriteMissedBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(READWRITEMISSEDBINRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "readWriteMissedBinResult" element
		 */
		@Override
		public boolean isSetReadWriteMissedBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(READWRITEMISSEDBINRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "readWriteMissedBinResult" element
		 */
		@Override
		public void setReadWriteMissedBinResult(java.lang.String readWriteMissedBinResult) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(READWRITEMISSEDBINRESULT$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(READWRITEMISSEDBINRESULT$0);
				}
				target.setStringValue(readWriteMissedBinResult);
			}
		}

		/**
		 * Unsets the "readWriteMissedBinResult" element
		 */
		@Override
		public void unsetReadWriteMissedBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(READWRITEMISSEDBINRESULT$0, 0);
			}
		}

		/**
		 * Gets (as xml) the "readWriteMissedBinResult" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetReadWriteMissedBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(READWRITEMISSEDBINRESULT$0, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "readWriteMissedBinResult" element
		 */
		@Override
		public void xsetReadWriteMissedBinResult(org.apache.xmlbeans.XmlString readWriteMissedBinResult) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(READWRITEMISSEDBINRESULT$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(READWRITEMISSEDBINRESULT$0);
				}
				target.set(readWriteMissedBinResult);
			}
		}
	}

	private static final javax.xml.namespace.QName READWRITEMISSEDBINRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBinResponse");

	private static final long serialVersionUID = 1L;

	public ReadWriteMissedBinResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "readWriteMissedBinResponse" element
	 */
	@Override
	public ReadWriteMissedBinResponseDocument.ReadWriteMissedBinResponse addNewReadWriteMissedBinResponse() {
		synchronized (monitor()) {
			check_orphaned();
			ReadWriteMissedBinResponseDocument.ReadWriteMissedBinResponse target = null;
			target = (ReadWriteMissedBinResponseDocument.ReadWriteMissedBinResponse) get_store().add_element_user(READWRITEMISSEDBINRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "readWriteMissedBinResponse" element
	 */
	@Override
	public ReadWriteMissedBinResponseDocument.ReadWriteMissedBinResponse getReadWriteMissedBinResponse() {
		synchronized (monitor()) {
			check_orphaned();
			ReadWriteMissedBinResponseDocument.ReadWriteMissedBinResponse target = null;
			target = (ReadWriteMissedBinResponseDocument.ReadWriteMissedBinResponse) get_store().find_element_user(READWRITEMISSEDBINRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "readWriteMissedBinResponse" element
	 */
	@Override
	public void setReadWriteMissedBinResponse(ReadWriteMissedBinResponseDocument.ReadWriteMissedBinResponse readWriteMissedBinResponse) {
		generatedSetterHelperImpl(readWriteMissedBinResponse, READWRITEMISSEDBINRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
