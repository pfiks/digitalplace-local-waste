/*
 * An XML document type.
 * Localname: WriteIncabLiveData
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: WriteIncabLiveDataDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one WriteIncabLiveData(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface WriteIncabLiveDataDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(WriteIncabLiveDataDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("writeincablivedatab9b2doctype");
    
    /**
     * Gets the "WriteIncabLiveData" element
     */
    WriteIncabLiveDataDocument.WriteIncabLiveData getWriteIncabLiveData();
    
    /**
     * Sets the "WriteIncabLiveData" element
     */
    void setWriteIncabLiveData(WriteIncabLiveDataDocument.WriteIncabLiveData writeIncabLiveData);
    
    /**
     * Appends and returns a new empty "WriteIncabLiveData" element
     */
    WriteIncabLiveDataDocument.WriteIncabLiveData addNewWriteIncabLiveData();
    
    /**
     * An XML WriteIncabLiveData(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface WriteIncabLiveData extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(WriteIncabLiveData.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("writeincablivedata7b00elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "Service" element
         */
        java.lang.String getService();
        
        /**
         * Gets (as xml) the "Service" element
         */
        org.apache.xmlbeans.XmlString xgetService();
        
        /**
         * True if has "Service" element
         */
        boolean isSetService();
        
        /**
         * Sets the "Service" element
         */
        void setService(java.lang.String service);
        
        /**
         * Sets (as xml) the "Service" element
         */
        void xsetService(org.apache.xmlbeans.XmlString service);
        
        /**
         * Unsets the "Service" element
         */
        void unsetService();
        
        /**
         * Gets the "TimeHHMM" element
         */
        java.lang.String getTimeHHMM();
        
        /**
         * Gets (as xml) the "TimeHHMM" element
         */
        org.apache.xmlbeans.XmlString xgetTimeHHMM();
        
        /**
         * True if has "TimeHHMM" element
         */
        boolean isSetTimeHHMM();
        
        /**
         * Sets the "TimeHHMM" element
         */
        void setTimeHHMM(java.lang.String timeHHMM);
        
        /**
         * Sets (as xml) the "TimeHHMM" element
         */
        void xsetTimeHHMM(org.apache.xmlbeans.XmlString timeHHMM);
        
        /**
         * Unsets the "TimeHHMM" element
         */
        void unsetTimeHHMM();
        
        /**
         * Gets the "Miles" element
         */
        java.lang.String getMiles();
        
        /**
         * Gets (as xml) the "Miles" element
         */
        org.apache.xmlbeans.XmlString xgetMiles();
        
        /**
         * True if has "Miles" element
         */
        boolean isSetMiles();
        
        /**
         * Sets the "Miles" element
         */
        void setMiles(java.lang.String miles);
        
        /**
         * Sets (as xml) the "Miles" element
         */
        void xsetMiles(org.apache.xmlbeans.XmlString miles);
        
        /**
         * Unsets the "Miles" element
         */
        void unsetMiles();
        
        /**
         * Gets the "HoursWorkedHHMM" element
         */
        java.lang.String getHoursWorkedHHMM();
        
        /**
         * Gets (as xml) the "HoursWorkedHHMM" element
         */
        org.apache.xmlbeans.XmlString xgetHoursWorkedHHMM();
        
        /**
         * True if has "HoursWorkedHHMM" element
         */
        boolean isSetHoursWorkedHHMM();
        
        /**
         * Sets the "HoursWorkedHHMM" element
         */
        void setHoursWorkedHHMM(java.lang.String hoursWorkedHHMM);
        
        /**
         * Sets (as xml) the "HoursWorkedHHMM" element
         */
        void xsetHoursWorkedHHMM(org.apache.xmlbeans.XmlString hoursWorkedHHMM);
        
        /**
         * Unsets the "HoursWorkedHHMM" element
         */
        void unsetHoursWorkedHHMM();
        
        /**
         * Gets the "Logins" element
         */
        java.lang.String getLogins();
        
        /**
         * Gets (as xml) the "Logins" element
         */
        org.apache.xmlbeans.XmlString xgetLogins();
        
        /**
         * True if has "Logins" element
         */
        boolean isSetLogins();
        
        /**
         * Sets the "Logins" element
         */
        void setLogins(java.lang.String logins);
        
        /**
         * Sets (as xml) the "Logins" element
         */
        void xsetLogins(org.apache.xmlbeans.XmlString logins);
        
        /**
         * Unsets the "Logins" element
         */
        void unsetLogins();
        
        /**
         * Gets the "DriverChecksGood" element
         */
        java.lang.String getDriverChecksGood();
        
        /**
         * Gets (as xml) the "DriverChecksGood" element
         */
        org.apache.xmlbeans.XmlString xgetDriverChecksGood();
        
        /**
         * True if has "DriverChecksGood" element
         */
        boolean isSetDriverChecksGood();
        
        /**
         * Sets the "DriverChecksGood" element
         */
        void setDriverChecksGood(java.lang.String driverChecksGood);
        
        /**
         * Sets (as xml) the "DriverChecksGood" element
         */
        void xsetDriverChecksGood(org.apache.xmlbeans.XmlString driverChecksGood);
        
        /**
         * Unsets the "DriverChecksGood" element
         */
        void unsetDriverChecksGood();
        
        /**
         * Gets the "DriverChecksBad" element
         */
        java.lang.String getDriverChecksBad();
        
        /**
         * Gets (as xml) the "DriverChecksBad" element
         */
        org.apache.xmlbeans.XmlString xgetDriverChecksBad();
        
        /**
         * True if has "DriverChecksBad" element
         */
        boolean isSetDriverChecksBad();
        
        /**
         * Sets the "DriverChecksBad" element
         */
        void setDriverChecksBad(java.lang.String driverChecksBad);
        
        /**
         * Sets (as xml) the "DriverChecksBad" element
         */
        void xsetDriverChecksBad(org.apache.xmlbeans.XmlString driverChecksBad);
        
        /**
         * Unsets the "DriverChecksBad" element
         */
        void unsetDriverChecksBad();
        
        /**
         * Gets the "CompletedStreets" element
         */
        java.lang.String getCompletedStreets();
        
        /**
         * Gets (as xml) the "CompletedStreets" element
         */
        org.apache.xmlbeans.XmlString xgetCompletedStreets();
        
        /**
         * True if has "CompletedStreets" element
         */
        boolean isSetCompletedStreets();
        
        /**
         * Sets the "CompletedStreets" element
         */
        void setCompletedStreets(java.lang.String completedStreets);
        
        /**
         * Sets (as xml) the "CompletedStreets" element
         */
        void xsetCompletedStreets(org.apache.xmlbeans.XmlString completedStreets);
        
        /**
         * Unsets the "CompletedStreets" element
         */
        void unsetCompletedStreets();
        
        /**
         * Gets the "Issues" element
         */
        java.lang.String getIssues();
        
        /**
         * Gets (as xml) the "Issues" element
         */
        org.apache.xmlbeans.XmlString xgetIssues();
        
        /**
         * True if has "Issues" element
         */
        boolean isSetIssues();
        
        /**
         * Sets the "Issues" element
         */
        void setIssues(java.lang.String issues);
        
        /**
         * Sets (as xml) the "Issues" element
         */
        void xsetIssues(org.apache.xmlbeans.XmlString issues);
        
        /**
         * Unsets the "Issues" element
         */
        void unsetIssues();
        
        /**
         * Gets the "BinNotOutIssues" element
         */
        java.lang.String getBinNotOutIssues();
        
        /**
         * Gets (as xml) the "BinNotOutIssues" element
         */
        org.apache.xmlbeans.XmlString xgetBinNotOutIssues();
        
        /**
         * True if has "BinNotOutIssues" element
         */
        boolean isSetBinNotOutIssues();
        
        /**
         * Sets the "BinNotOutIssues" element
         */
        void setBinNotOutIssues(java.lang.String binNotOutIssues);
        
        /**
         * Sets (as xml) the "BinNotOutIssues" element
         */
        void xsetBinNotOutIssues(org.apache.xmlbeans.XmlString binNotOutIssues);
        
        /**
         * Unsets the "BinNotOutIssues" element
         */
        void unsetBinNotOutIssues();
        
        /**
         * Gets the "ContaminationIssues" element
         */
        java.lang.String getContaminationIssues();
        
        /**
         * Gets (as xml) the "ContaminationIssues" element
         */
        org.apache.xmlbeans.XmlString xgetContaminationIssues();
        
        /**
         * True if has "ContaminationIssues" element
         */
        boolean isSetContaminationIssues();
        
        /**
         * Sets the "ContaminationIssues" element
         */
        void setContaminationIssues(java.lang.String contaminationIssues);
        
        /**
         * Sets (as xml) the "ContaminationIssues" element
         */
        void xsetContaminationIssues(org.apache.xmlbeans.XmlString contaminationIssues);
        
        /**
         * Unsets the "ContaminationIssues" element
         */
        void unsetContaminationIssues();
        
        /**
         * Gets the "DamageIssues" element
         */
        java.lang.String getDamageIssues();
        
        /**
         * Gets (as xml) the "DamageIssues" element
         */
        org.apache.xmlbeans.XmlString xgetDamageIssues();
        
        /**
         * True if has "DamageIssues" element
         */
        boolean isSetDamageIssues();
        
        /**
         * Sets the "DamageIssues" element
         */
        void setDamageIssues(java.lang.String damageIssues);
        
        /**
         * Sets (as xml) the "DamageIssues" element
         */
        void xsetDamageIssues(org.apache.xmlbeans.XmlString damageIssues);
        
        /**
         * Unsets the "DamageIssues" element
         */
        void unsetDamageIssues();
        
        /**
         * Gets the "OtherIssues" element
         */
        java.lang.String getOtherIssues();
        
        /**
         * Gets (as xml) the "OtherIssues" element
         */
        org.apache.xmlbeans.XmlString xgetOtherIssues();
        
        /**
         * True if has "OtherIssues" element
         */
        boolean isSetOtherIssues();
        
        /**
         * Sets the "OtherIssues" element
         */
        void setOtherIssues(java.lang.String otherIssues);
        
        /**
         * Sets (as xml) the "OtherIssues" element
         */
        void xsetOtherIssues(org.apache.xmlbeans.XmlString otherIssues);
        
        /**
         * Unsets the "OtherIssues" element
         */
        void unsetOtherIssues();
        
        /**
         * Gets the "ActionIssues" element
         */
        java.lang.String getActionIssues();
        
        /**
         * Gets (as xml) the "ActionIssues" element
         */
        org.apache.xmlbeans.XmlString xgetActionIssues();
        
        /**
         * True if has "ActionIssues" element
         */
        boolean isSetActionIssues();
        
        /**
         * Sets the "ActionIssues" element
         */
        void setActionIssues(java.lang.String actionIssues);
        
        /**
         * Sets (as xml) the "ActionIssues" element
         */
        void xsetActionIssues(org.apache.xmlbeans.XmlString actionIssues);
        
        /**
         * Unsets the "ActionIssues" element
         */
        void unsetActionIssues();
        
        /**
         * Gets the "HazardsIssues" element
         */
        java.lang.String getHazardsIssues();
        
        /**
         * Gets (as xml) the "HazardsIssues" element
         */
        org.apache.xmlbeans.XmlString xgetHazardsIssues();
        
        /**
         * True if has "HazardsIssues" element
         */
        boolean isSetHazardsIssues();
        
        /**
         * Sets the "HazardsIssues" element
         */
        void setHazardsIssues(java.lang.String hazardsIssues);
        
        /**
         * Sets (as xml) the "HazardsIssues" element
         */
        void xsetHazardsIssues(org.apache.xmlbeans.XmlString hazardsIssues);
        
        /**
         * Unsets the "HazardsIssues" element
         */
        void unsetHazardsIssues();
        
        /**
         * Gets the "Collected" element
         */
        java.lang.String getCollected();
        
        /**
         * Gets (as xml) the "Collected" element
         */
        org.apache.xmlbeans.XmlString xgetCollected();
        
        /**
         * True if has "Collected" element
         */
        boolean isSetCollected();
        
        /**
         * Sets the "Collected" element
         */
        void setCollected(java.lang.String collected);
        
        /**
         * Sets (as xml) the "Collected" element
         */
        void xsetCollected(org.apache.xmlbeans.XmlString collected);
        
        /**
         * Unsets the "Collected" element
         */
        void unsetCollected();
        
        /**
         * Gets the "UnCollected" element
         */
        java.lang.String getUnCollected();
        
        /**
         * Gets (as xml) the "UnCollected" element
         */
        org.apache.xmlbeans.XmlString xgetUnCollected();
        
        /**
         * True if has "UnCollected" element
         */
        boolean isSetUnCollected();
        
        /**
         * Sets the "UnCollected" element
         */
        void setUnCollected(java.lang.String unCollected);
        
        /**
         * Sets (as xml) the "UnCollected" element
         */
        void xsetUnCollected(org.apache.xmlbeans.XmlString unCollected);
        
        /**
         * Unsets the "UnCollected" element
         */
        void unsetUnCollected();
        
        /**
         * Gets the "GPSPoints" element
         */
        java.lang.String getGPSPoints();
        
        /**
         * Gets (as xml) the "GPSPoints" element
         */
        org.apache.xmlbeans.XmlString xgetGPSPoints();
        
        /**
         * True if has "GPSPoints" element
         */
        boolean isSetGPSPoints();
        
        /**
         * Sets the "GPSPoints" element
         */
        void setGPSPoints(java.lang.String gpsPoints);
        
        /**
         * Sets (as xml) the "GPSPoints" element
         */
        void xsetGPSPoints(org.apache.xmlbeans.XmlString gpsPoints);
        
        /**
         * Unsets the "GPSPoints" element
         */
        void unsetGPSPoints();
        
        /**
         * Gets the "TotalDistance" element
         */
        java.lang.String getTotalDistance();
        
        /**
         * Gets (as xml) the "TotalDistance" element
         */
        org.apache.xmlbeans.XmlString xgetTotalDistance();
        
        /**
         * True if has "TotalDistance" element
         */
        boolean isSetTotalDistance();
        
        /**
         * Sets the "TotalDistance" element
         */
        void setTotalDistance(java.lang.String totalDistance);
        
        /**
         * Sets (as xml) the "TotalDistance" element
         */
        void xsetTotalDistance(org.apache.xmlbeans.XmlString totalDistance);
        
        /**
         * Unsets the "TotalDistance" element
         */
        void unsetTotalDistance();
        
        /**
         * Gets the "TipVisits" element
         */
        java.lang.String getTipVisits();
        
        /**
         * Gets (as xml) the "TipVisits" element
         */
        org.apache.xmlbeans.XmlString xgetTipVisits();
        
        /**
         * True if has "TipVisits" element
         */
        boolean isSetTipVisits();
        
        /**
         * Sets the "TipVisits" element
         */
        void setTipVisits(java.lang.String tipVisits);
        
        /**
         * Sets (as xml) the "TipVisits" element
         */
        void xsetTipVisits(org.apache.xmlbeans.XmlString tipVisits);
        
        /**
         * Unsets the "TipVisits" element
         */
        void unsetTipVisits();
        
        /**
         * Gets the "TipYield" element
         */
        java.lang.String getTipYield();
        
        /**
         * Gets (as xml) the "TipYield" element
         */
        org.apache.xmlbeans.XmlString xgetTipYield();
        
        /**
         * True if has "TipYield" element
         */
        boolean isSetTipYield();
        
        /**
         * Sets the "TipYield" element
         */
        void setTipYield(java.lang.String tipYield);
        
        /**
         * Sets (as xml) the "TipYield" element
         */
        void xsetTipYield(org.apache.xmlbeans.XmlString tipYield);
        
        /**
         * Unsets the "TipYield" element
         */
        void unsetTipYield();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static WriteIncabLiveDataDocument.WriteIncabLiveData newInstance() {
              return (WriteIncabLiveDataDocument.WriteIncabLiveData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static WriteIncabLiveDataDocument.WriteIncabLiveData newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (WriteIncabLiveDataDocument.WriteIncabLiveData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static WriteIncabLiveDataDocument newInstance() {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static WriteIncabLiveDataDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static WriteIncabLiveDataDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static WriteIncabLiveDataDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static WriteIncabLiveDataDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static WriteIncabLiveDataDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static WriteIncabLiveDataDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static WriteIncabLiveDataDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static WriteIncabLiveDataDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static WriteIncabLiveDataDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static WriteIncabLiveDataDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static WriteIncabLiveDataDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static WriteIncabLiveDataDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static WriteIncabLiveDataDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static WriteIncabLiveDataDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static WriteIncabLiveDataDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static WriteIncabLiveDataDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static WriteIncabLiveDataDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (WriteIncabLiveDataDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
