/*
 * An XML document type.
 * Localname: CommentsForIncabGetExistingResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CommentsForIncabGetExistingResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one CommentsForIncabGetExistingResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface CommentsForIncabGetExistingResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CommentsForIncabGetExistingResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("commentsforincabgetexistingresponsede22doctype");
    
    /**
     * Gets the "CommentsForIncabGetExistingResponse" element
     */
    CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse getCommentsForIncabGetExistingResponse();
    
    /**
     * Sets the "CommentsForIncabGetExistingResponse" element
     */
    void setCommentsForIncabGetExistingResponse(CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse commentsForIncabGetExistingResponse);
    
    /**
     * Appends and returns a new empty "CommentsForIncabGetExistingResponse" element
     */
    CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse addNewCommentsForIncabGetExistingResponse();
    
    /**
     * An XML CommentsForIncabGetExistingResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface CommentsForIncabGetExistingResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CommentsForIncabGetExistingResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("commentsforincabgetexistingresponse7b26elemtype");
        
        /**
         * Gets the "CommentsForIncabGetExistingResult" element
         */
        CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult getCommentsForIncabGetExistingResult();
        
        /**
         * True if has "CommentsForIncabGetExistingResult" element
         */
        boolean isSetCommentsForIncabGetExistingResult();
        
        /**
         * Sets the "CommentsForIncabGetExistingResult" element
         */
        void setCommentsForIncabGetExistingResult(CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult commentsForIncabGetExistingResult);
        
        /**
         * Appends and returns a new empty "CommentsForIncabGetExistingResult" element
         */
        CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult addNewCommentsForIncabGetExistingResult();
        
        /**
         * Unsets the "CommentsForIncabGetExistingResult" element
         */
        void unsetCommentsForIncabGetExistingResult();
        
        /**
         * An XML CommentsForIncabGetExistingResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface CommentsForIncabGetExistingResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CommentsForIncabGetExistingResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("commentsforincabgetexistingresultb1c6elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult newInstance() {
                  return (CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse.CommentsForIncabGetExistingResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse newInstance() {
              return (CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (CommentsForIncabGetExistingResponseDocument.CommentsForIncabGetExistingResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static CommentsForIncabGetExistingResponseDocument newInstance() {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static CommentsForIncabGetExistingResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static CommentsForIncabGetExistingResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static CommentsForIncabGetExistingResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static CommentsForIncabGetExistingResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static CommentsForIncabGetExistingResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static CommentsForIncabGetExistingResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (CommentsForIncabGetExistingResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
