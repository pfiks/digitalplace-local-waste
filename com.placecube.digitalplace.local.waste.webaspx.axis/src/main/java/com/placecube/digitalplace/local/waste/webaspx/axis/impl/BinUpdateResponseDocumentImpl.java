/*
 * An XML document type.
 * Localname: BinUpdateResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: BinUpdateResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.BinUpdateResponseDocument;

/**
 * A document containing one
 * BinUpdateResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class BinUpdateResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinUpdateResponseDocument {

	/**
	 * An XML BinUpdateResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class BinUpdateResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinUpdateResponseDocument.BinUpdateResponse {

		/**
		 * An XML
		 * BinUpdateResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class BinUpdateResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinUpdateResponseDocument.BinUpdateResponse.BinUpdateResult {

			private static final long serialVersionUID = 1L;

			public BinUpdateResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName BINUPDATERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinUpdateResult");

		private static final long serialVersionUID = 1L;

		public BinUpdateResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "BinUpdateResult" element
		 */
		@Override
		public BinUpdateResponseDocument.BinUpdateResponse.BinUpdateResult addNewBinUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				BinUpdateResponseDocument.BinUpdateResponse.BinUpdateResult target = null;
				target = (BinUpdateResponseDocument.BinUpdateResponse.BinUpdateResult) get_store().add_element_user(BINUPDATERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "BinUpdateResult" element
		 */
		@Override
		public BinUpdateResponseDocument.BinUpdateResponse.BinUpdateResult getBinUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				BinUpdateResponseDocument.BinUpdateResponse.BinUpdateResult target = null;
				target = (BinUpdateResponseDocument.BinUpdateResponse.BinUpdateResult) get_store().find_element_user(BINUPDATERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "BinUpdateResult" element
		 */
		@Override
		public boolean isSetBinUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BINUPDATERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "BinUpdateResult" element
		 */
		@Override
		public void setBinUpdateResult(BinUpdateResponseDocument.BinUpdateResponse.BinUpdateResult binUpdateResult) {
			generatedSetterHelperImpl(binUpdateResult, BINUPDATERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "BinUpdateResult" element
		 */
		@Override
		public void unsetBinUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BINUPDATERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName BINUPDATERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinUpdateResponse");

	private static final long serialVersionUID = 1L;

	public BinUpdateResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "BinUpdateResponse" element
	 */
	@Override
	public BinUpdateResponseDocument.BinUpdateResponse addNewBinUpdateResponse() {
		synchronized (monitor()) {
			check_orphaned();
			BinUpdateResponseDocument.BinUpdateResponse target = null;
			target = (BinUpdateResponseDocument.BinUpdateResponse) get_store().add_element_user(BINUPDATERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "BinUpdateResponse" element
	 */
	@Override
	public BinUpdateResponseDocument.BinUpdateResponse getBinUpdateResponse() {
		synchronized (monitor()) {
			check_orphaned();
			BinUpdateResponseDocument.BinUpdateResponse target = null;
			target = (BinUpdateResponseDocument.BinUpdateResponse) get_store().find_element_user(BINUPDATERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "BinUpdateResponse" element
	 */
	@Override
	public void setBinUpdateResponse(BinUpdateResponseDocument.BinUpdateResponse binUpdateResponse) {
		generatedSetterHelperImpl(binUpdateResponse, BINUPDATERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
