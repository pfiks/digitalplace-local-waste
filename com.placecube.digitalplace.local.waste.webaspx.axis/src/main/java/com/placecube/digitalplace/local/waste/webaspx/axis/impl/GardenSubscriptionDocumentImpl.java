/*
 * An XML document type.
 * Localname: GardenSubscription
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscriptionDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GardenSubscriptionDocument;

/**
 * A document containing one
 * GardenSubscription(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class GardenSubscriptionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenSubscriptionDocument {

	/**
	 * An XML
	 * GardenSubscription(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GardenSubscriptionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenSubscriptionDocument.GardenSubscription {

		private static final javax.xml.namespace.QName BINSATPROPERTY$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binsAtProperty");

		private static final javax.xml.namespace.QName BINTYPE$20 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "binType");

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");
		private static final javax.xml.namespace.QName CRMGARDENREF$24 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "crmGardenRef");
		private static final javax.xml.namespace.QName GARDENCONTAINERDELIVERYCOMMENTS$22 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"gardenContainerDeliveryComments");
		private static final javax.xml.namespace.QName NEWPAYREF$18 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "newPayRef");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName SUBSCRIPTIONENDDATE$16 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "subscriptionEndDate");
		private static final javax.xml.namespace.QName SUBSCRIPTIONSTARTDATE$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "subscriptionStartDate");
		private static final javax.xml.namespace.QName TOTALNOSUBSREQUIRED$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "totalNoSubsRequired");
		private static final javax.xml.namespace.QName UPRN$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRN");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public GardenSubscriptionImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "binsAtProperty" element
		 */
		@Override
		public java.lang.String getBinsAtProperty() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINSATPROPERTY$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "binType" element
		 */
		@Override
		public java.lang.String getBinType() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINTYPE$20, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "crmGardenRef" element
		 */
		@Override
		public java.lang.String getCrmGardenRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CRMGARDENREF$24, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "gardenContainerDeliveryComments" element
		 */
		@Override
		public java.lang.String getGardenContainerDeliveryComments() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(GARDENCONTAINERDELIVERYCOMMENTS$22, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "newPayRef" element
		 */
		@Override
		public java.lang.String getNewPayRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(NEWPAYREF$18, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "subscriptionEndDate" element
		 */
		@Override
		public java.lang.String getSubscriptionEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SUBSCRIPTIONENDDATE$16, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "subscriptionStartDate" element
		 */
		@Override
		public java.lang.String getSubscriptionStartDate() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SUBSCRIPTIONSTARTDATE$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "totalNoSubsRequired" element
		 */
		@Override
		public java.lang.String getTotalNoSubsRequired() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TOTALNOSUBSREQUIRED$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRN" element
		 */
		@Override
		public java.lang.String getUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "binsAtProperty" element
		 */
		@Override
		public boolean isSetBinsAtProperty() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BINSATPROPERTY$10) != 0;
			}
		}

		/**
		 * True if has "binType" element
		 */
		@Override
		public boolean isSetBinType() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BINTYPE$20) != 0;
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "crmGardenRef" element
		 */
		@Override
		public boolean isSetCrmGardenRef() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CRMGARDENREF$24) != 0;
			}
		}

		/**
		 * True if has "gardenContainerDeliveryComments" element
		 */
		@Override
		public boolean isSetGardenContainerDeliveryComments() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GARDENCONTAINERDELIVERYCOMMENTS$22) != 0;
			}
		}

		/**
		 * True if has "newPayRef" element
		 */
		@Override
		public boolean isSetNewPayRef() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(NEWPAYREF$18) != 0;
			}
		}

		/**
		 * True if has "subscriptionEndDate" element
		 */
		@Override
		public boolean isSetSubscriptionEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SUBSCRIPTIONENDDATE$16) != 0;
			}
		}

		/**
		 * True if has "subscriptionStartDate" element
		 */
		@Override
		public boolean isSetSubscriptionStartDate() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SUBSCRIPTIONSTARTDATE$14) != 0;
			}
		}

		/**
		 * True if has "totalNoSubsRequired" element
		 */
		@Override
		public boolean isSetTotalNoSubsRequired() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(TOTALNOSUBSREQUIRED$12) != 0;
			}
		}

		/**
		 * True if has "UPRN" element
		 */
		@Override
		public boolean isSetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRN$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "binsAtProperty" element
		 */
		@Override
		public void setBinsAtProperty(java.lang.String binsAtProperty) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINSATPROPERTY$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(BINSATPROPERTY$10);
				}
				target.setStringValue(binsAtProperty);
			}
		}

		/**
		 * Sets the "binType" element
		 */
		@Override
		public void setBinType(java.lang.String binType) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINTYPE$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(BINTYPE$20);
				}
				target.setStringValue(binType);
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "crmGardenRef" element
		 */
		@Override
		public void setCrmGardenRef(java.lang.String crmGardenRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CRMGARDENREF$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CRMGARDENREF$24);
				}
				target.setStringValue(crmGardenRef);
			}
		}

		/**
		 * Sets the "gardenContainerDeliveryComments" element
		 */
		@Override
		public void setGardenContainerDeliveryComments(java.lang.String gardenContainerDeliveryComments) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(GARDENCONTAINERDELIVERYCOMMENTS$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(GARDENCONTAINERDELIVERYCOMMENTS$22);
				}
				target.setStringValue(gardenContainerDeliveryComments);
			}
		}

		/**
		 * Sets the "newPayRef" element
		 */
		@Override
		public void setNewPayRef(java.lang.String newPayRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(NEWPAYREF$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(NEWPAYREF$18);
				}
				target.setStringValue(newPayRef);
			}
		}

		/**
		 * Sets the "subscriptionEndDate" element
		 */
		@Override
		public void setSubscriptionEndDate(java.lang.String subscriptionEndDate) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SUBSCRIPTIONENDDATE$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(SUBSCRIPTIONENDDATE$16);
				}
				target.setStringValue(subscriptionEndDate);
			}
		}

		/**
		 * Sets the "subscriptionStartDate" element
		 */
		@Override
		public void setSubscriptionStartDate(java.lang.String subscriptionStartDate) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SUBSCRIPTIONSTARTDATE$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(SUBSCRIPTIONSTARTDATE$14);
				}
				target.setStringValue(subscriptionStartDate);
			}
		}

		/**
		 * Sets the "totalNoSubsRequired" element
		 */
		@Override
		public void setTotalNoSubsRequired(java.lang.String totalNoSubsRequired) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TOTALNOSUBSREQUIRED$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(TOTALNOSUBSREQUIRED$12);
				}
				target.setStringValue(totalNoSubsRequired);
			}
		}

		/**
		 * Sets the "UPRN" element
		 */
		@Override
		public void setUPRN(java.lang.String uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRN$8);
				}
				target.setStringValue(uprn);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "binsAtProperty" element
		 */
		@Override
		public void unsetBinsAtProperty() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BINSATPROPERTY$10, 0);
			}
		}

		/**
		 * Unsets the "binType" element
		 */
		@Override
		public void unsetBinType() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BINTYPE$20, 0);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "crmGardenRef" element
		 */
		@Override
		public void unsetCrmGardenRef() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CRMGARDENREF$24, 0);
			}
		}

		/**
		 * Unsets the "gardenContainerDeliveryComments" element
		 */
		@Override
		public void unsetGardenContainerDeliveryComments() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GARDENCONTAINERDELIVERYCOMMENTS$22, 0);
			}
		}

		/**
		 * Unsets the "newPayRef" element
		 */
		@Override
		public void unsetNewPayRef() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(NEWPAYREF$18, 0);
			}
		}

		/**
		 * Unsets the "subscriptionEndDate" element
		 */
		@Override
		public void unsetSubscriptionEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SUBSCRIPTIONENDDATE$16, 0);
			}
		}

		/**
		 * Unsets the "subscriptionStartDate" element
		 */
		@Override
		public void unsetSubscriptionStartDate() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SUBSCRIPTIONSTARTDATE$14, 0);
			}
		}

		/**
		 * Unsets the "totalNoSubsRequired" element
		 */
		@Override
		public void unsetTotalNoSubsRequired() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(TOTALNOSUBSREQUIRED$12, 0);
			}
		}

		/**
		 * Unsets the "UPRN" element
		 */
		@Override
		public void unsetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRN$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "binsAtProperty" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetBinsAtProperty() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINSATPROPERTY$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "binType" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetBinType() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINTYPE$20, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "crmGardenRef" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCrmGardenRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CRMGARDENREF$24, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "gardenContainerDeliveryComments" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetGardenContainerDeliveryComments() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(GARDENCONTAINERDELIVERYCOMMENTS$22, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "newPayRef" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetNewPayRef() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(NEWPAYREF$18, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "subscriptionEndDate" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetSubscriptionEndDate() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SUBSCRIPTIONENDDATE$16, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "subscriptionStartDate" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetSubscriptionStartDate() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SUBSCRIPTIONSTARTDATE$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "totalNoSubsRequired" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetTotalNoSubsRequired() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TOTALNOSUBSREQUIRED$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "binsAtProperty" element
		 */
		@Override
		public void xsetBinsAtProperty(org.apache.xmlbeans.XmlString binsAtProperty) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINSATPROPERTY$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(BINSATPROPERTY$10);
				}
				target.set(binsAtProperty);
			}
		}

		/**
		 * Sets (as xml) the "binType" element
		 */
		@Override
		public void xsetBinType(org.apache.xmlbeans.XmlString binType) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINTYPE$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(BINTYPE$20);
				}
				target.set(binType);
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "crmGardenRef" element
		 */
		@Override
		public void xsetCrmGardenRef(org.apache.xmlbeans.XmlString crmGardenRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CRMGARDENREF$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CRMGARDENREF$24);
				}
				target.set(crmGardenRef);
			}
		}

		/**
		 * Sets (as xml) the "gardenContainerDeliveryComments" element
		 */
		@Override
		public void xsetGardenContainerDeliveryComments(org.apache.xmlbeans.XmlString gardenContainerDeliveryComments) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(GARDENCONTAINERDELIVERYCOMMENTS$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(GARDENCONTAINERDELIVERYCOMMENTS$22);
				}
				target.set(gardenContainerDeliveryComments);
			}
		}

		/**
		 * Sets (as xml) the "newPayRef" element
		 */
		@Override
		public void xsetNewPayRef(org.apache.xmlbeans.XmlString newPayRef) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(NEWPAYREF$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(NEWPAYREF$18);
				}
				target.set(newPayRef);
			}
		}

		/**
		 * Sets (as xml) the "subscriptionEndDate" element
		 */
		@Override
		public void xsetSubscriptionEndDate(org.apache.xmlbeans.XmlString subscriptionEndDate) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SUBSCRIPTIONENDDATE$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(SUBSCRIPTIONENDDATE$16);
				}
				target.set(subscriptionEndDate);
			}
		}

		/**
		 * Sets (as xml) the "subscriptionStartDate" element
		 */
		@Override
		public void xsetSubscriptionStartDate(org.apache.xmlbeans.XmlString subscriptionStartDate) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SUBSCRIPTIONSTARTDATE$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(SUBSCRIPTIONSTARTDATE$14);
				}
				target.set(subscriptionStartDate);
			}
		}

		/**
		 * Sets (as xml) the "totalNoSubsRequired" element
		 */
		@Override
		public void xsetTotalNoSubsRequired(org.apache.xmlbeans.XmlString totalNoSubsRequired) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TOTALNOSUBSREQUIRED$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(TOTALNOSUBSREQUIRED$12);
				}
				target.set(totalNoSubsRequired);
			}
		}

		/**
		 * Sets (as xml) the "UPRN" element
		 */
		@Override
		public void xsetUPRN(org.apache.xmlbeans.XmlString uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRN$8);
				}
				target.set(uprn);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName GARDENSUBSCRIPTION$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GardenSubscription");

	private static final long serialVersionUID = 1L;

	public GardenSubscriptionDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GardenSubscription" element
	 */
	@Override
	public GardenSubscriptionDocument.GardenSubscription addNewGardenSubscription() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscriptionDocument.GardenSubscription target = null;
			target = (GardenSubscriptionDocument.GardenSubscription) get_store().add_element_user(GARDENSUBSCRIPTION$0);
			return target;
		}
	}

	/**
	 * Gets the "GardenSubscription" element
	 */
	@Override
	public GardenSubscriptionDocument.GardenSubscription getGardenSubscription() {
		synchronized (monitor()) {
			check_orphaned();
			GardenSubscriptionDocument.GardenSubscription target = null;
			target = (GardenSubscriptionDocument.GardenSubscription) get_store().find_element_user(GARDENSUBSCRIPTION$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GardenSubscription" element
	 */
	@Override
	public void setGardenSubscription(GardenSubscriptionDocument.GardenSubscription gardenSubscription) {
		generatedSetterHelperImpl(gardenSubscription, GARDENSUBSCRIPTION$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
