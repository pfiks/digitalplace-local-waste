/*
 * An XML document type.
 * Localname: getIssuesAndCollectionStatusForUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetIssuesAndCollectionStatusForUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getIssuesAndCollectionStatusForUPRNResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetIssuesAndCollectionStatusForUPRNResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetIssuesAndCollectionStatusForUPRNResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getissuesandcollectionstatusforuprnresponse2274doctype");
    
    /**
     * Gets the "getIssuesAndCollectionStatusForUPRNResponse" element
     */
    GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse getGetIssuesAndCollectionStatusForUPRNResponse();
    
    /**
     * Sets the "getIssuesAndCollectionStatusForUPRNResponse" element
     */
    void setGetIssuesAndCollectionStatusForUPRNResponse(GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse getIssuesAndCollectionStatusForUPRNResponse);
    
    /**
     * Appends and returns a new empty "getIssuesAndCollectionStatusForUPRNResponse" element
     */
    GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse addNewGetIssuesAndCollectionStatusForUPRNResponse();
    
    /**
     * An XML getIssuesAndCollectionStatusForUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetIssuesAndCollectionStatusForUPRNResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetIssuesAndCollectionStatusForUPRNResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getissuesandcollectionstatusforuprnresponse6f4aelemtype");
        
        /**
         * Gets the "getIssuesAndCollectionStatusForUPRNResult" element
         */
        GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult getGetIssuesAndCollectionStatusForUPRNResult();
        
        /**
         * True if has "getIssuesAndCollectionStatusForUPRNResult" element
         */
        boolean isSetGetIssuesAndCollectionStatusForUPRNResult();
        
        /**
         * Sets the "getIssuesAndCollectionStatusForUPRNResult" element
         */
        void setGetIssuesAndCollectionStatusForUPRNResult(GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult getIssuesAndCollectionStatusForUPRNResult);
        
        /**
         * Appends and returns a new empty "getIssuesAndCollectionStatusForUPRNResult" element
         */
        GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult addNewGetIssuesAndCollectionStatusForUPRNResult();
        
        /**
         * Unsets the "getIssuesAndCollectionStatusForUPRNResult" element
         */
        void unsetGetIssuesAndCollectionStatusForUPRNResult();
        
        /**
         * An XML getIssuesAndCollectionStatusForUPRNResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetIssuesAndCollectionStatusForUPRNResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetIssuesAndCollectionStatusForUPRNResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getissuesandcollectionstatusforuprnresult66bcelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult newInstance() {
                  return (GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse.GetIssuesAndCollectionStatusForUPRNResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse newInstance() {
              return (GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetIssuesAndCollectionStatusForUPRNResponseDocument.GetIssuesAndCollectionStatusForUPRNResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument newInstance() {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetIssuesAndCollectionStatusForUPRNResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetIssuesAndCollectionStatusForUPRNResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
