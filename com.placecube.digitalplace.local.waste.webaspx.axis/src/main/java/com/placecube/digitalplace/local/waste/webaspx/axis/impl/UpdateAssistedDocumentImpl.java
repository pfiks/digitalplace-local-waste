/*
 * An XML document type.
 * Localname: UpdateAssisted
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: UpdateAssistedDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateAssistedDocument;

/**
 * A document containing one
 * UpdateAssisted(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class UpdateAssistedDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements UpdateAssistedDocument {

	/**
	 * An XML UpdateAssisted(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class UpdateAssistedImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements UpdateAssistedDocument.UpdateAssisted {

		private static final javax.xml.namespace.QName ASSISTEDENDDATEDDSMMSYYYY$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AssistedEndDateDDsMMsYYYY");

		private static final javax.xml.namespace.QName ASSISTEDLOCATION$16 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AssistedLocation");

		private static final javax.xml.namespace.QName ASSISTEDSTARTDATEDDSMMSYYYY$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AssistedStartDateDDsMMsYYYY");
		private static final javax.xml.namespace.QName ASSISTEDYN01234$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AssistedYN01234");
		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName UPRN$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRN");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public UpdateAssistedImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "AssistedEndDateDDsMMsYYYY" element
		 */
		@Override
		public java.lang.String getAssistedEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ASSISTEDENDDATEDDSMMSYYYY$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "AssistedLocation" element
		 */
		@Override
		public java.lang.String getAssistedLocation() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ASSISTEDLOCATION$16, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "AssistedStartDateDDsMMsYYYY" element
		 */
		@Override
		public java.lang.String getAssistedStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ASSISTEDSTARTDATEDDSMMSYYYY$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "AssistedYN01234" element
		 */
		@Override
		public java.lang.String getAssistedYN01234() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ASSISTEDYN01234$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRN" element
		 */
		@Override
		public java.lang.String getUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "AssistedEndDateDDsMMsYYYY" element
		 */
		@Override
		public boolean isSetAssistedEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ASSISTEDENDDATEDDSMMSYYYY$14) != 0;
			}
		}

		/**
		 * True if has "AssistedLocation" element
		 */
		@Override
		public boolean isSetAssistedLocation() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ASSISTEDLOCATION$16) != 0;
			}
		}

		/**
		 * True if has "AssistedStartDateDDsMMsYYYY" element
		 */
		@Override
		public boolean isSetAssistedStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ASSISTEDSTARTDATEDDSMMSYYYY$12) != 0;
			}
		}

		/**
		 * True if has "AssistedYN01234" element
		 */
		@Override
		public boolean isSetAssistedYN01234() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ASSISTEDYN01234$10) != 0;
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "UPRN" element
		 */
		@Override
		public boolean isSetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRN$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "AssistedEndDateDDsMMsYYYY" element
		 */
		@Override
		public void setAssistedEndDateDDsMMsYYYY(java.lang.String assistedEndDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ASSISTEDENDDATEDDSMMSYYYY$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ASSISTEDENDDATEDDSMMSYYYY$14);
				}
				target.setStringValue(assistedEndDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets the "AssistedLocation" element
		 */
		@Override
		public void setAssistedLocation(java.lang.String assistedLocation) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ASSISTEDLOCATION$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ASSISTEDLOCATION$16);
				}
				target.setStringValue(assistedLocation);
			}
		}

		/**
		 * Sets the "AssistedStartDateDDsMMsYYYY" element
		 */
		@Override
		public void setAssistedStartDateDDsMMsYYYY(java.lang.String assistedStartDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ASSISTEDSTARTDATEDDSMMSYYYY$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ASSISTEDSTARTDATEDDSMMSYYYY$12);
				}
				target.setStringValue(assistedStartDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets the "AssistedYN01234" element
		 */
		@Override
		public void setAssistedYN01234(java.lang.String assistedYN01234) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ASSISTEDYN01234$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ASSISTEDYN01234$10);
				}
				target.setStringValue(assistedYN01234);
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "UPRN" element
		 */
		@Override
		public void setUPRN(java.lang.String uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRN$8);
				}
				target.setStringValue(uprn);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "AssistedEndDateDDsMMsYYYY" element
		 */
		@Override
		public void unsetAssistedEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ASSISTEDENDDATEDDSMMSYYYY$14, 0);
			}
		}

		/**
		 * Unsets the "AssistedLocation" element
		 */
		@Override
		public void unsetAssistedLocation() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ASSISTEDLOCATION$16, 0);
			}
		}

		/**
		 * Unsets the "AssistedStartDateDDsMMsYYYY" element
		 */
		@Override
		public void unsetAssistedStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ASSISTEDSTARTDATEDDSMMSYYYY$12, 0);
			}
		}

		/**
		 * Unsets the "AssistedYN01234" element
		 */
		@Override
		public void unsetAssistedYN01234() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ASSISTEDYN01234$10, 0);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "UPRN" element
		 */
		@Override
		public void unsetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRN$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "AssistedEndDateDDsMMsYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAssistedEndDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ASSISTEDENDDATEDDSMMSYYYY$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "AssistedLocation" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAssistedLocation() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ASSISTEDLOCATION$16, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "AssistedStartDateDDsMMsYYYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAssistedStartDateDDsMMsYYYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ASSISTEDSTARTDATEDDSMMSYYYY$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "AssistedYN01234" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetAssistedYN01234() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ASSISTEDYN01234$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "AssistedEndDateDDsMMsYYYY" element
		 */
		@Override
		public void xsetAssistedEndDateDDsMMsYYYY(org.apache.xmlbeans.XmlString assistedEndDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ASSISTEDENDDATEDDSMMSYYYY$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ASSISTEDENDDATEDDSMMSYYYY$14);
				}
				target.set(assistedEndDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets (as xml) the "AssistedLocation" element
		 */
		@Override
		public void xsetAssistedLocation(org.apache.xmlbeans.XmlString assistedLocation) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ASSISTEDLOCATION$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ASSISTEDLOCATION$16);
				}
				target.set(assistedLocation);
			}
		}

		/**
		 * Sets (as xml) the "AssistedStartDateDDsMMsYYYY" element
		 */
		@Override
		public void xsetAssistedStartDateDDsMMsYYYY(org.apache.xmlbeans.XmlString assistedStartDateDDsMMsYYYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ASSISTEDSTARTDATEDDSMMSYYYY$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ASSISTEDSTARTDATEDDSMMSYYYY$12);
				}
				target.set(assistedStartDateDDsMMsYYYY);
			}
		}

		/**
		 * Sets (as xml) the "AssistedYN01234" element
		 */
		@Override
		public void xsetAssistedYN01234(org.apache.xmlbeans.XmlString assistedYN01234) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ASSISTEDYN01234$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ASSISTEDYN01234$10);
				}
				target.set(assistedYN01234);
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "UPRN" element
		 */
		@Override
		public void xsetUPRN(org.apache.xmlbeans.XmlString uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRN$8);
				}
				target.set(uprn);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final long serialVersionUID = 1L;

	private static final javax.xml.namespace.QName UPDATEASSISTED$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateAssisted");

	public UpdateAssistedDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "UpdateAssisted" element
	 */
	@Override
	public UpdateAssistedDocument.UpdateAssisted addNewUpdateAssisted() {
		synchronized (monitor()) {
			check_orphaned();
			UpdateAssistedDocument.UpdateAssisted target = null;
			target = (UpdateAssistedDocument.UpdateAssisted) get_store().add_element_user(UPDATEASSISTED$0);
			return target;
		}
	}

	/**
	 * Gets the "UpdateAssisted" element
	 */
	@Override
	public UpdateAssistedDocument.UpdateAssisted getUpdateAssisted() {
		synchronized (monitor()) {
			check_orphaned();
			UpdateAssistedDocument.UpdateAssisted target = null;
			target = (UpdateAssistedDocument.UpdateAssisted) get_store().find_element_user(UPDATEASSISTED$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "UpdateAssisted" element
	 */
	@Override
	public void setUpdateAssisted(UpdateAssistedDocument.UpdateAssisted updateAssisted) {
		generatedSetterHelperImpl(updateAssisted, UPDATEASSISTED$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
