/*
 * An XML document type.
 * Localname: Garden_Subscribers_Deliveries_Collections
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscribersDeliveriesCollectionsDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one Garden_Subscribers_Deliveries_Collections(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GardenSubscribersDeliveriesCollectionsDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscribersDeliveriesCollectionsDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscribersdeliveriescollections4e30doctype");
    
    /**
     * Gets the "Garden_Subscribers_Deliveries_Collections" element
     */
    GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections getGardenSubscribersDeliveriesCollections();
    
    /**
     * Sets the "Garden_Subscribers_Deliveries_Collections" element
     */
    void setGardenSubscribersDeliveriesCollections(GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections gardenSubscribersDeliveriesCollections);
    
    /**
     * Appends and returns a new empty "Garden_Subscribers_Deliveries_Collections" element
     */
    GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections addNewGardenSubscribersDeliveriesCollections();
    
    /**
     * An XML Garden_Subscribers_Deliveries_Collections(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GardenSubscribersDeliveriesCollections extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscribersDeliveriesCollections.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscribersdeliveriescollections3682elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "subscribersDeliveriesOrCollections" element
         */
        java.lang.String getSubscribersDeliveriesOrCollections();
        
        /**
         * Gets (as xml) the "subscribersDeliveriesOrCollections" element
         */
        org.apache.xmlbeans.XmlString xgetSubscribersDeliveriesOrCollections();
        
        /**
         * True if has "subscribersDeliveriesOrCollections" element
         */
        boolean isSetSubscribersDeliveriesOrCollections();
        
        /**
         * Sets the "subscribersDeliveriesOrCollections" element
         */
        void setSubscribersDeliveriesOrCollections(java.lang.String subscribersDeliveriesOrCollections);
        
        /**
         * Sets (as xml) the "subscribersDeliveriesOrCollections" element
         */
        void xsetSubscribersDeliveriesOrCollections(org.apache.xmlbeans.XmlString subscribersDeliveriesOrCollections);
        
        /**
         * Unsets the "subscribersDeliveriesOrCollections" element
         */
        void unsetSubscribersDeliveriesOrCollections();
        
        /**
         * Gets the "dateToCheckOn_yyyyMMdd" element
         */
        java.lang.String getDateToCheckOnYyyyMMdd();
        
        /**
         * Gets (as xml) the "dateToCheckOn_yyyyMMdd" element
         */
        org.apache.xmlbeans.XmlString xgetDateToCheckOnYyyyMMdd();
        
        /**
         * True if has "dateToCheckOn_yyyyMMdd" element
         */
        boolean isSetDateToCheckOnYyyyMMdd();
        
        /**
         * Sets the "dateToCheckOn_yyyyMMdd" element
         */
        void setDateToCheckOnYyyyMMdd(java.lang.String dateToCheckOnYyyyMMdd);
        
        /**
         * Sets (as xml) the "dateToCheckOn_yyyyMMdd" element
         */
        void xsetDateToCheckOnYyyyMMdd(org.apache.xmlbeans.XmlString dateToCheckOnYyyyMMdd);
        
        /**
         * Unsets the "dateToCheckOn_yyyyMMdd" element
         */
        void unsetDateToCheckOnYyyyMMdd();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections newInstance() {
              return (GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GardenSubscribersDeliveriesCollectionsDocument.GardenSubscribersDeliveriesCollections) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GardenSubscribersDeliveriesCollectionsDocument newInstance() {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GardenSubscribersDeliveriesCollectionsDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscribersDeliveriesCollectionsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscribersDeliveriesCollectionsDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscribersDeliveriesCollectionsDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
