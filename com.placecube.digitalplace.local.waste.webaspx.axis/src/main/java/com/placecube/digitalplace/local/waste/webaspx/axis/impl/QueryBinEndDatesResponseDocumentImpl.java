/*
 * An XML document type.
 * Localname: QueryBinEndDatesResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: QueryBinEndDatesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinEndDatesResponseDocument;

/**
 * A document containing one
 * QueryBinEndDatesResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class QueryBinEndDatesResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements QueryBinEndDatesResponseDocument {

	/**
	 * An XML
	 * QueryBinEndDatesResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class QueryBinEndDatesResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse {

		/**
		 * An XML
		 * QueryBinEndDatesResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class QueryBinEndDatesResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult {

			private static final long serialVersionUID = 1L;

			public QueryBinEndDatesResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName QUERYBINENDDATESRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "QueryBinEndDatesResult");

		private static final long serialVersionUID = 1L;

		public QueryBinEndDatesResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "QueryBinEndDatesResult" element
		 */
		@Override
		public QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult addNewQueryBinEndDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult target = null;
				target = (QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult) get_store().add_element_user(QUERYBINENDDATESRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "QueryBinEndDatesResult" element
		 */
		@Override
		public QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult getQueryBinEndDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult target = null;
				target = (QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult) get_store().find_element_user(QUERYBINENDDATESRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "QueryBinEndDatesResult" element
		 */
		@Override
		public boolean isSetQueryBinEndDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(QUERYBINENDDATESRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "QueryBinEndDatesResult" element
		 */
		@Override
		public void setQueryBinEndDatesResult(QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse.QueryBinEndDatesResult queryBinEndDatesResult) {
			generatedSetterHelperImpl(queryBinEndDatesResult, QUERYBINENDDATESRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "QueryBinEndDatesResult" element
		 */
		@Override
		public void unsetQueryBinEndDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(QUERYBINENDDATESRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName QUERYBINENDDATESRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "QueryBinEndDatesResponse");

	private static final long serialVersionUID = 1L;

	public QueryBinEndDatesResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "QueryBinEndDatesResponse" element
	 */
	@Override
	public QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse addNewQueryBinEndDatesResponse() {
		synchronized (monitor()) {
			check_orphaned();
			QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse target = null;
			target = (QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse) get_store().add_element_user(QUERYBINENDDATESRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "QueryBinEndDatesResponse" element
	 */
	@Override
	public QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse getQueryBinEndDatesResponse() {
		synchronized (monitor()) {
			check_orphaned();
			QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse target = null;
			target = (QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse) get_store().find_element_user(QUERYBINENDDATESRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "QueryBinEndDatesResponse" element
	 */
	@Override
	public void setQueryBinEndDatesResponse(QueryBinEndDatesResponseDocument.QueryBinEndDatesResponse queryBinEndDatesResponse) {
		generatedSetterHelperImpl(queryBinEndDatesResponse, QUERYBINENDDATESRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
