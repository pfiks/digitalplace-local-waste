/*
 * An XML document type.
 * Localname: LargeHouseholdsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LargeHouseholdsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.LargeHouseholdsResponseDocument;

/**
 * A document containing one
 * LargeHouseholdsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class LargeHouseholdsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements LargeHouseholdsResponseDocument {

	/**
	 * An XML
	 * LargeHouseholdsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class LargeHouseholdsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements LargeHouseholdsResponseDocument.LargeHouseholdsResponse {

		/**
		 * An XML
		 * LargeHouseholdsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class LargeHouseholdsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements LargeHouseholdsResponseDocument.LargeHouseholdsResponse.LargeHouseholdsResult {

			private static final long serialVersionUID = 1L;

			public LargeHouseholdsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName LARGEHOUSEHOLDSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LargeHouseholdsResult");

		private static final long serialVersionUID = 1L;

		public LargeHouseholdsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "LargeHouseholdsResult" element
		 */
		@Override
		public LargeHouseholdsResponseDocument.LargeHouseholdsResponse.LargeHouseholdsResult addNewLargeHouseholdsResult() {
			synchronized (monitor()) {
				check_orphaned();
				LargeHouseholdsResponseDocument.LargeHouseholdsResponse.LargeHouseholdsResult target = null;
				target = (LargeHouseholdsResponseDocument.LargeHouseholdsResponse.LargeHouseholdsResult) get_store().add_element_user(LARGEHOUSEHOLDSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "LargeHouseholdsResult" element
		 */
		@Override
		public LargeHouseholdsResponseDocument.LargeHouseholdsResponse.LargeHouseholdsResult getLargeHouseholdsResult() {
			synchronized (monitor()) {
				check_orphaned();
				LargeHouseholdsResponseDocument.LargeHouseholdsResponse.LargeHouseholdsResult target = null;
				target = (LargeHouseholdsResponseDocument.LargeHouseholdsResponse.LargeHouseholdsResult) get_store().find_element_user(LARGEHOUSEHOLDSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "LargeHouseholdsResult" element
		 */
		@Override
		public boolean isSetLargeHouseholdsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(LARGEHOUSEHOLDSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "LargeHouseholdsResult" element
		 */
		@Override
		public void setLargeHouseholdsResult(LargeHouseholdsResponseDocument.LargeHouseholdsResponse.LargeHouseholdsResult largeHouseholdsResult) {
			generatedSetterHelperImpl(largeHouseholdsResult, LARGEHOUSEHOLDSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "LargeHouseholdsResult" element
		 */
		@Override
		public void unsetLargeHouseholdsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(LARGEHOUSEHOLDSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName LARGEHOUSEHOLDSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LargeHouseholdsResponse");

	private static final long serialVersionUID = 1L;

	public LargeHouseholdsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "LargeHouseholdsResponse" element
	 */
	@Override
	public LargeHouseholdsResponseDocument.LargeHouseholdsResponse addNewLargeHouseholdsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LargeHouseholdsResponseDocument.LargeHouseholdsResponse target = null;
			target = (LargeHouseholdsResponseDocument.LargeHouseholdsResponse) get_store().add_element_user(LARGEHOUSEHOLDSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "LargeHouseholdsResponse" element
	 */
	@Override
	public LargeHouseholdsResponseDocument.LargeHouseholdsResponse getLargeHouseholdsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LargeHouseholdsResponseDocument.LargeHouseholdsResponse target = null;
			target = (LargeHouseholdsResponseDocument.LargeHouseholdsResponse) get_store().find_element_user(LARGEHOUSEHOLDSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "LargeHouseholdsResponse" element
	 */
	@Override
	public void setLargeHouseholdsResponse(LargeHouseholdsResponseDocument.LargeHouseholdsResponse largeHouseholdsResponse) {
		generatedSetterHelperImpl(largeHouseholdsResponse, LARGEHOUSEHOLDSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
