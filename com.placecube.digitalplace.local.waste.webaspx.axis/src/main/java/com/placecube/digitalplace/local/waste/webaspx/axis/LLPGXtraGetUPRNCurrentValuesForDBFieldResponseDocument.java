/*
 * An XML document type.
 * Localname: LLPGXtraGetUPRNCurrentValuesForDBFieldResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one LLPGXtraGetUPRNCurrentValuesForDBFieldResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtragetuprncurrentvaluesfordbfieldresponse2280doctype");
    
    /**
     * Gets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResponse" element
     */
    LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse getLLPGXtraGetUPRNCurrentValuesForDBFieldResponse();
    
    /**
     * Sets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResponse" element
     */
    void setLLPGXtraGetUPRNCurrentValuesForDBFieldResponse(LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse llpgXtraGetUPRNCurrentValuesForDBFieldResponse);
    
    /**
     * Appends and returns a new empty "LLPGXtraGetUPRNCurrentValuesForDBFieldResponse" element
     */
    LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse addNewLLPGXtraGetUPRNCurrentValuesForDBFieldResponse();
    
    /**
     * An XML LLPGXtraGetUPRNCurrentValuesForDBFieldResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface LLPGXtraGetUPRNCurrentValuesForDBFieldResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtragetuprncurrentvaluesfordbfieldresponse0c40elemtype");
        
        /**
         * Gets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
         */
        LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult getLLPGXtraGetUPRNCurrentValuesForDBFieldResult();
        
        /**
         * True if has "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
         */
        boolean isSetLLPGXtraGetUPRNCurrentValuesForDBFieldResult();
        
        /**
         * Sets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
         */
        void setLLPGXtraGetUPRNCurrentValuesForDBFieldResult(LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult llpgXtraGetUPRNCurrentValuesForDBFieldResult);
        
        /**
         * Appends and returns a new empty "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
         */
        LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult addNewLLPGXtraGetUPRNCurrentValuesForDBFieldResult();
        
        /**
         * Unsets the "LLPGXtraGetUPRNCurrentValuesForDBFieldResult" element
         */
        void unsetLLPGXtraGetUPRNCurrentValuesForDBFieldResult();
        
        /**
         * An XML LLPGXtraGetUPRNCurrentValuesForDBFieldResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface LLPGXtraGetUPRNCurrentValuesForDBFieldResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraGetUPRNCurrentValuesForDBFieldResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtragetuprncurrentvaluesfordbfieldresult591celemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult newInstance() {
                  return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse.LLPGXtraGetUPRNCurrentValuesForDBFieldResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse newInstance() {
              return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument.LLPGXtraGetUPRNCurrentValuesForDBFieldResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument newInstance() {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (LLPGXtraGetUPRNCurrentValuesForDBFieldResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
