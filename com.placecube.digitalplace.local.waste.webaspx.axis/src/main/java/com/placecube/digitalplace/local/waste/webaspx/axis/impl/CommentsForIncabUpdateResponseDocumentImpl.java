/*
 * An XML document type.
 * Localname: CommentsForIncabUpdateResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CommentsForIncabUpdateResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.CommentsForIncabUpdateResponseDocument;

/**
 * A document containing one
 * CommentsForIncabUpdateResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class CommentsForIncabUpdateResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CommentsForIncabUpdateResponseDocument {

	/**
	 * An XML
	 * CommentsForIncabUpdateResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class CommentsForIncabUpdateResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse {

		/**
		 * An XML
		 * CommentsForIncabUpdateResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class CommentsForIncabUpdateResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult {

			private static final long serialVersionUID = 1L;

			public CommentsForIncabUpdateResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName COMMENTSFORINCABUPDATERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CommentsForIncabUpdateResult");

		private static final long serialVersionUID = 1L;

		public CommentsForIncabUpdateResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "CommentsForIncabUpdateResult"
		 * element
		 */
		@Override
		public CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult addNewCommentsForIncabUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult target = null;
				target = (CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult) get_store().add_element_user(COMMENTSFORINCABUPDATERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "CommentsForIncabUpdateResult" element
		 */
		@Override
		public CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult getCommentsForIncabUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult target = null;
				target = (CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult) get_store().find_element_user(COMMENTSFORINCABUPDATERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "CommentsForIncabUpdateResult" element
		 */
		@Override
		public boolean isSetCommentsForIncabUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COMMENTSFORINCABUPDATERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "CommentsForIncabUpdateResult" element
		 */
		@Override
		public void setCommentsForIncabUpdateResult(CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult commentsForIncabUpdateResult) {
			generatedSetterHelperImpl(commentsForIncabUpdateResult, COMMENTSFORINCABUPDATERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "CommentsForIncabUpdateResult" element
		 */
		@Override
		public void unsetCommentsForIncabUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COMMENTSFORINCABUPDATERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName COMMENTSFORINCABUPDATERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CommentsForIncabUpdateResponse");

	private static final long serialVersionUID = 1L;

	public CommentsForIncabUpdateResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "CommentsForIncabUpdateResponse" element
	 */
	@Override
	public CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse addNewCommentsForIncabUpdateResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse target = null;
			target = (CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse) get_store().add_element_user(COMMENTSFORINCABUPDATERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "CommentsForIncabUpdateResponse" element
	 */
	@Override
	public CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse getCommentsForIncabUpdateResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse target = null;
			target = (CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse) get_store().find_element_user(COMMENTSFORINCABUPDATERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "CommentsForIncabUpdateResponse" element
	 */
	@Override
	public void setCommentsForIncabUpdateResponse(CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse commentsForIncabUpdateResponse) {
		generatedSetterHelperImpl(commentsForIncabUpdateResponse, COMMENTSFORINCABUPDATERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
