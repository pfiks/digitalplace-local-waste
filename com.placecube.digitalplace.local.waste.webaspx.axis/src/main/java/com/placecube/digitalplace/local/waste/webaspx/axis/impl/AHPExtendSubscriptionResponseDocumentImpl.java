/*
 * An XML document type.
 * Localname: AHP_ExtendSubscriptionResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPExtendSubscriptionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AHPExtendSubscriptionResponseDocument;

/**
 * A document containing one
 * AHP_ExtendSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AHPExtendSubscriptionResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPExtendSubscriptionResponseDocument {

	/**
	 * An XML
	 * AHP_ExtendSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AHPExtendSubscriptionResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse {

		/**
		 * An XML
		 * AHP_ExtendSubscriptionResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class AHPExtendSubscriptionResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult {

			private static final long serialVersionUID = 1L;

			public AHPExtendSubscriptionResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName AHPEXTENDSUBSCRIPTIONRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_ExtendSubscriptionResult");

		private static final long serialVersionUID = 1L;

		public AHPExtendSubscriptionResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "AHP_ExtendSubscriptionResult"
		 * element
		 */
		@Override
		public AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult addNewAHPExtendSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult target = null;
				target = (AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult) get_store().add_element_user(AHPEXTENDSUBSCRIPTIONRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "AHP_ExtendSubscriptionResult" element
		 */
		@Override
		public AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult getAHPExtendSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult target = null;
				target = (AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult) get_store().find_element_user(AHPEXTENDSUBSCRIPTIONRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "AHP_ExtendSubscriptionResult" element
		 */
		@Override
		public boolean isSetAHPExtendSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPEXTENDSUBSCRIPTIONRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AHP_ExtendSubscriptionResult" element
		 */
		@Override
		public void setAHPExtendSubscriptionResult(AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse.AHPExtendSubscriptionResult ahpExtendSubscriptionResult) {
			generatedSetterHelperImpl(ahpExtendSubscriptionResult, AHPEXTENDSUBSCRIPTIONRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "AHP_ExtendSubscriptionResult" element
		 */
		@Override
		public void unsetAHPExtendSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPEXTENDSUBSCRIPTIONRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName AHPEXTENDSUBSCRIPTIONRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_ExtendSubscriptionResponse");

	private static final long serialVersionUID = 1L;

	public AHPExtendSubscriptionResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AHP_ExtendSubscriptionResponse" element
	 */
	@Override
	public AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse addNewAHPExtendSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse target = null;
			target = (AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse) get_store().add_element_user(AHPEXTENDSUBSCRIPTIONRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AHP_ExtendSubscriptionResponse" element
	 */
	@Override
	public AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse getAHPExtendSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse target = null;
			target = (AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse) get_store().find_element_user(AHPEXTENDSUBSCRIPTIONRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AHP_ExtendSubscriptionResponse" element
	 */
	@Override
	public void setAHPExtendSubscriptionResponse(AHPExtendSubscriptionResponseDocument.AHPExtendSubscriptionResponse ahpExtendSubscriptionResponse) {
		generatedSetterHelperImpl(ahpExtendSubscriptionResponse, AHPEXTENDSUBSCRIPTIONRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
