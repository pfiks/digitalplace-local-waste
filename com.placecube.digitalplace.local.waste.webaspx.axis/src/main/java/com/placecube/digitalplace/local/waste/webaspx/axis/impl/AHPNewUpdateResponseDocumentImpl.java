/*
 * An XML document type.
 * Localname: AHP_NewUpdateResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPNewUpdateResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AHPNewUpdateResponseDocument;

/**
 * A document containing one
 * AHP_NewUpdateResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AHPNewUpdateResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPNewUpdateResponseDocument {

	/**
	 * An XML
	 * AHP_NewUpdateResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AHPNewUpdateResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPNewUpdateResponseDocument.AHPNewUpdateResponse {

		/**
		 * An XML
		 * AHP_NewUpdateResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class AHPNewUpdateResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPNewUpdateResponseDocument.AHPNewUpdateResponse.AHPNewUpdateResult {

			private static final long serialVersionUID = 1L;

			public AHPNewUpdateResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName AHPNEWUPDATERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_NewUpdateResult");

		private static final long serialVersionUID = 1L;

		public AHPNewUpdateResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "AHP_NewUpdateResult" element
		 */
		@Override
		public AHPNewUpdateResponseDocument.AHPNewUpdateResponse.AHPNewUpdateResult addNewAHPNewUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPNewUpdateResponseDocument.AHPNewUpdateResponse.AHPNewUpdateResult target = null;
				target = (AHPNewUpdateResponseDocument.AHPNewUpdateResponse.AHPNewUpdateResult) get_store().add_element_user(AHPNEWUPDATERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "AHP_NewUpdateResult" element
		 */
		@Override
		public AHPNewUpdateResponseDocument.AHPNewUpdateResponse.AHPNewUpdateResult getAHPNewUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPNewUpdateResponseDocument.AHPNewUpdateResponse.AHPNewUpdateResult target = null;
				target = (AHPNewUpdateResponseDocument.AHPNewUpdateResponse.AHPNewUpdateResult) get_store().find_element_user(AHPNEWUPDATERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "AHP_NewUpdateResult" element
		 */
		@Override
		public boolean isSetAHPNewUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPNEWUPDATERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AHP_NewUpdateResult" element
		 */
		@Override
		public void setAHPNewUpdateResult(AHPNewUpdateResponseDocument.AHPNewUpdateResponse.AHPNewUpdateResult ahpNewUpdateResult) {
			generatedSetterHelperImpl(ahpNewUpdateResult, AHPNEWUPDATERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "AHP_NewUpdateResult" element
		 */
		@Override
		public void unsetAHPNewUpdateResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPNEWUPDATERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName AHPNEWUPDATERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_NewUpdateResponse");

	private static final long serialVersionUID = 1L;

	public AHPNewUpdateResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AHP_NewUpdateResponse" element
	 */
	@Override
	public AHPNewUpdateResponseDocument.AHPNewUpdateResponse addNewAHPNewUpdateResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPNewUpdateResponseDocument.AHPNewUpdateResponse target = null;
			target = (AHPNewUpdateResponseDocument.AHPNewUpdateResponse) get_store().add_element_user(AHPNEWUPDATERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AHP_NewUpdateResponse" element
	 */
	@Override
	public AHPNewUpdateResponseDocument.AHPNewUpdateResponse getAHPNewUpdateResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPNewUpdateResponseDocument.AHPNewUpdateResponse target = null;
			target = (AHPNewUpdateResponseDocument.AHPNewUpdateResponse) get_store().find_element_user(AHPNEWUPDATERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AHP_NewUpdateResponse" element
	 */
	@Override
	public void setAHPNewUpdateResponse(AHPNewUpdateResponseDocument.AHPNewUpdateResponse ahpNewUpdateResponse) {
		generatedSetterHelperImpl(ahpNewUpdateResponse, AHPNEWUPDATERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
