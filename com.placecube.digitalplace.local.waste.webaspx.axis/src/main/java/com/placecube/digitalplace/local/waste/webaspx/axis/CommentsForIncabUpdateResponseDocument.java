/*
 * An XML document type.
 * Localname: CommentsForIncabUpdateResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CommentsForIncabUpdateResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one CommentsForIncabUpdateResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface CommentsForIncabUpdateResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CommentsForIncabUpdateResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("commentsforincabupdateresponse9b10doctype");
    
    /**
     * Gets the "CommentsForIncabUpdateResponse" element
     */
    CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse getCommentsForIncabUpdateResponse();
    
    /**
     * Sets the "CommentsForIncabUpdateResponse" element
     */
    void setCommentsForIncabUpdateResponse(CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse commentsForIncabUpdateResponse);
    
    /**
     * Appends and returns a new empty "CommentsForIncabUpdateResponse" element
     */
    CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse addNewCommentsForIncabUpdateResponse();
    
    /**
     * An XML CommentsForIncabUpdateResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface CommentsForIncabUpdateResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CommentsForIncabUpdateResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("commentsforincabupdateresponse8040elemtype");
        
        /**
         * Gets the "CommentsForIncabUpdateResult" element
         */
        CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult getCommentsForIncabUpdateResult();
        
        /**
         * True if has "CommentsForIncabUpdateResult" element
         */
        boolean isSetCommentsForIncabUpdateResult();
        
        /**
         * Sets the "CommentsForIncabUpdateResult" element
         */
        void setCommentsForIncabUpdateResult(CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult commentsForIncabUpdateResult);
        
        /**
         * Appends and returns a new empty "CommentsForIncabUpdateResult" element
         */
        CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult addNewCommentsForIncabUpdateResult();
        
        /**
         * Unsets the "CommentsForIncabUpdateResult" element
         */
        void unsetCommentsForIncabUpdateResult();
        
        /**
         * An XML CommentsForIncabUpdateResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface CommentsForIncabUpdateResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CommentsForIncabUpdateResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("commentsforincabupdateresult53acelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult newInstance() {
                  return (CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse.CommentsForIncabUpdateResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse newInstance() {
              return (CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (CommentsForIncabUpdateResponseDocument.CommentsForIncabUpdateResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static CommentsForIncabUpdateResponseDocument newInstance() {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static CommentsForIncabUpdateResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static CommentsForIncabUpdateResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static CommentsForIncabUpdateResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static CommentsForIncabUpdateResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static CommentsForIncabUpdateResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static CommentsForIncabUpdateResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (CommentsForIncabUpdateResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
