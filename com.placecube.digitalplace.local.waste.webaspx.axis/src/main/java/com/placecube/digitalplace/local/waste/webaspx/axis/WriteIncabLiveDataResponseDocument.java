/*
 * An XML document type.
 * Localname: WriteIncabLiveDataResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: WriteIncabLiveDataResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one WriteIncabLiveDataResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface WriteIncabLiveDataResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(WriteIncabLiveDataResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("writeincablivedataresponse7153doctype");
    
    /**
     * Gets the "WriteIncabLiveDataResponse" element
     */
    WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse getWriteIncabLiveDataResponse();
    
    /**
     * Sets the "WriteIncabLiveDataResponse" element
     */
    void setWriteIncabLiveDataResponse(WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse writeIncabLiveDataResponse);
    
    /**
     * Appends and returns a new empty "WriteIncabLiveDataResponse" element
     */
    WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse addNewWriteIncabLiveDataResponse();
    
    /**
     * An XML WriteIncabLiveDataResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface WriteIncabLiveDataResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(WriteIncabLiveDataResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("writeincablivedataresponsecba0elemtype");
        
        /**
         * Gets the "WriteIncabLiveDataResult" element
         */
        WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult getWriteIncabLiveDataResult();
        
        /**
         * True if has "WriteIncabLiveDataResult" element
         */
        boolean isSetWriteIncabLiveDataResult();
        
        /**
         * Sets the "WriteIncabLiveDataResult" element
         */
        void setWriteIncabLiveDataResult(WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult writeIncabLiveDataResult);
        
        /**
         * Appends and returns a new empty "WriteIncabLiveDataResult" element
         */
        WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult addNewWriteIncabLiveDataResult();
        
        /**
         * Unsets the "WriteIncabLiveDataResult" element
         */
        void unsetWriteIncabLiveDataResult();
        
        /**
         * An XML WriteIncabLiveDataResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface WriteIncabLiveDataResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(WriteIncabLiveDataResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("writeincablivedataresult23cfelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult newInstance() {
                  return (WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse.WriteIncabLiveDataResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse newInstance() {
              return (WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (WriteIncabLiveDataResponseDocument.WriteIncabLiveDataResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static WriteIncabLiveDataResponseDocument newInstance() {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static WriteIncabLiveDataResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static WriteIncabLiveDataResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static WriteIncabLiveDataResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static WriteIncabLiveDataResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static WriteIncabLiveDataResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static WriteIncabLiveDataResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static WriteIncabLiveDataResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static WriteIncabLiveDataResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static WriteIncabLiveDataResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static WriteIncabLiveDataResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static WriteIncabLiveDataResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static WriteIncabLiveDataResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static WriteIncabLiveDataResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static WriteIncabLiveDataResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static WriteIncabLiveDataResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static WriteIncabLiveDataResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static WriteIncabLiveDataResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (WriteIncabLiveDataResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
