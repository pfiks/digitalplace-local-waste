/*
 * An XML document type.
 * Localname: DeleteBinResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: DeleteBinResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.DeleteBinResponseDocument;

/**
 * A document containing one
 * DeleteBinResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class DeleteBinResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements DeleteBinResponseDocument {

	/**
	 * An XML DeleteBinResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class DeleteBinResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements DeleteBinResponseDocument.DeleteBinResponse {

		/**
		 * An XML
		 * DeleteBinResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class DeleteBinResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements DeleteBinResponseDocument.DeleteBinResponse.DeleteBinResult {

			private static final long serialVersionUID = 1L;

			public DeleteBinResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName DELETEBINRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "DeleteBinResult");

		private static final long serialVersionUID = 1L;

		public DeleteBinResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "DeleteBinResult" element
		 */
		@Override
		public DeleteBinResponseDocument.DeleteBinResponse.DeleteBinResult addNewDeleteBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				DeleteBinResponseDocument.DeleteBinResponse.DeleteBinResult target = null;
				target = (DeleteBinResponseDocument.DeleteBinResponse.DeleteBinResult) get_store().add_element_user(DELETEBINRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "DeleteBinResult" element
		 */
		@Override
		public DeleteBinResponseDocument.DeleteBinResponse.DeleteBinResult getDeleteBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				DeleteBinResponseDocument.DeleteBinResponse.DeleteBinResult target = null;
				target = (DeleteBinResponseDocument.DeleteBinResponse.DeleteBinResult) get_store().find_element_user(DELETEBINRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "DeleteBinResult" element
		 */
		@Override
		public boolean isSetDeleteBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DELETEBINRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "DeleteBinResult" element
		 */
		@Override
		public void setDeleteBinResult(DeleteBinResponseDocument.DeleteBinResponse.DeleteBinResult deleteBinResult) {
			generatedSetterHelperImpl(deleteBinResult, DELETEBINRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "DeleteBinResult" element
		 */
		@Override
		public void unsetDeleteBinResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DELETEBINRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName DELETEBINRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "DeleteBinResponse");

	private static final long serialVersionUID = 1L;

	public DeleteBinResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "DeleteBinResponse" element
	 */
	@Override
	public DeleteBinResponseDocument.DeleteBinResponse addNewDeleteBinResponse() {
		synchronized (monitor()) {
			check_orphaned();
			DeleteBinResponseDocument.DeleteBinResponse target = null;
			target = (DeleteBinResponseDocument.DeleteBinResponse) get_store().add_element_user(DELETEBINRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "DeleteBinResponse" element
	 */
	@Override
	public DeleteBinResponseDocument.DeleteBinResponse getDeleteBinResponse() {
		synchronized (monitor()) {
			check_orphaned();
			DeleteBinResponseDocument.DeleteBinResponse target = null;
			target = (DeleteBinResponseDocument.DeleteBinResponse) get_store().find_element_user(DELETEBINRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "DeleteBinResponse" element
	 */
	@Override
	public void setDeleteBinResponse(DeleteBinResponseDocument.DeleteBinResponse deleteBinResponse) {
		generatedSetterHelperImpl(deleteBinResponse, DELETEBINRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
