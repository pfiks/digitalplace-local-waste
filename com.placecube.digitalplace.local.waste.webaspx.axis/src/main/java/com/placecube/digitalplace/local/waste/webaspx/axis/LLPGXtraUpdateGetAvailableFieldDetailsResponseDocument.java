/*
 * An XML document type.
 * Localname: LLPGXtraUpdateGetAvailableFieldDetailsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one LLPGXtraUpdateGetAvailableFieldDetailsResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtraupdategetavailablefielddetailsresponse2863doctype");
    
    /**
     * Gets the "LLPGXtraUpdateGetAvailableFieldDetailsResponse" element
     */
    LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse getLLPGXtraUpdateGetAvailableFieldDetailsResponse();
    
    /**
     * Sets the "LLPGXtraUpdateGetAvailableFieldDetailsResponse" element
     */
    void setLLPGXtraUpdateGetAvailableFieldDetailsResponse(LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse llpgXtraUpdateGetAvailableFieldDetailsResponse);
    
    /**
     * Appends and returns a new empty "LLPGXtraUpdateGetAvailableFieldDetailsResponse" element
     */
    LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse addNewLLPGXtraUpdateGetAvailableFieldDetailsResponse();
    
    /**
     * An XML LLPGXtraUpdateGetAvailableFieldDetailsResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface LLPGXtraUpdateGetAvailableFieldDetailsResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraUpdateGetAvailableFieldDetailsResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtraupdategetavailablefielddetailsresponsefaa0elemtype");
        
        /**
         * Gets the "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
         */
        LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult getLLPGXtraUpdateGetAvailableFieldDetailsResult();
        
        /**
         * True if has "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
         */
        boolean isSetLLPGXtraUpdateGetAvailableFieldDetailsResult();
        
        /**
         * Sets the "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
         */
        void setLLPGXtraUpdateGetAvailableFieldDetailsResult(LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult llpgXtraUpdateGetAvailableFieldDetailsResult);
        
        /**
         * Appends and returns a new empty "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
         */
        LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult addNewLLPGXtraUpdateGetAvailableFieldDetailsResult();
        
        /**
         * Unsets the "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
         */
        void unsetLLPGXtraUpdateGetAvailableFieldDetailsResult();
        
        /**
         * An XML LLPGXtraUpdateGetAvailableFieldDetailsResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface LLPGXtraUpdateGetAvailableFieldDetailsResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(LLPGXtraUpdateGetAvailableFieldDetailsResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("llpgxtraupdategetavailablefielddetailsresult695felemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult newInstance() {
                  return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse newInstance() {
              return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument newInstance() {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
