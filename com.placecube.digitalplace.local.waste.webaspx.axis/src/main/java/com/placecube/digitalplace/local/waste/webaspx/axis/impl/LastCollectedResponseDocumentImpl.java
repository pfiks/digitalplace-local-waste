/*
 * An XML document type.
 * Localname: LastCollectedResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LastCollectedResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.LastCollectedResponseDocument;

/**
 * A document containing one
 * LastCollectedResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class LastCollectedResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements LastCollectedResponseDocument {

	/**
	 * An XML
	 * LastCollectedResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class LastCollectedResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements LastCollectedResponseDocument.LastCollectedResponse {

		/**
		 * An XML
		 * LastCollectedResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class LastCollectedResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements LastCollectedResponseDocument.LastCollectedResponse.LastCollectedResult {

			private static final long serialVersionUID = 1L;

			public LastCollectedResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName LASTCOLLECTEDRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LastCollectedResult");

		private static final long serialVersionUID = 1L;

		public LastCollectedResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "LastCollectedResult" element
		 */
		@Override
		public LastCollectedResponseDocument.LastCollectedResponse.LastCollectedResult addNewLastCollectedResult() {
			synchronized (monitor()) {
				check_orphaned();
				LastCollectedResponseDocument.LastCollectedResponse.LastCollectedResult target = null;
				target = (LastCollectedResponseDocument.LastCollectedResponse.LastCollectedResult) get_store().add_element_user(LASTCOLLECTEDRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "LastCollectedResult" element
		 */
		@Override
		public LastCollectedResponseDocument.LastCollectedResponse.LastCollectedResult getLastCollectedResult() {
			synchronized (monitor()) {
				check_orphaned();
				LastCollectedResponseDocument.LastCollectedResponse.LastCollectedResult target = null;
				target = (LastCollectedResponseDocument.LastCollectedResponse.LastCollectedResult) get_store().find_element_user(LASTCOLLECTEDRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "LastCollectedResult" element
		 */
		@Override
		public boolean isSetLastCollectedResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(LASTCOLLECTEDRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "LastCollectedResult" element
		 */
		@Override
		public void setLastCollectedResult(LastCollectedResponseDocument.LastCollectedResponse.LastCollectedResult lastCollectedResult) {
			generatedSetterHelperImpl(lastCollectedResult, LASTCOLLECTEDRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "LastCollectedResult" element
		 */
		@Override
		public void unsetLastCollectedResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(LASTCOLLECTEDRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName LASTCOLLECTEDRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "LastCollectedResponse");

	private static final long serialVersionUID = 1L;

	public LastCollectedResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "LastCollectedResponse" element
	 */
	@Override
	public LastCollectedResponseDocument.LastCollectedResponse addNewLastCollectedResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LastCollectedResponseDocument.LastCollectedResponse target = null;
			target = (LastCollectedResponseDocument.LastCollectedResponse) get_store().add_element_user(LASTCOLLECTEDRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "LastCollectedResponse" element
	 */
	@Override
	public LastCollectedResponseDocument.LastCollectedResponse getLastCollectedResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LastCollectedResponseDocument.LastCollectedResponse target = null;
			target = (LastCollectedResponseDocument.LastCollectedResponse) get_store().find_element_user(LASTCOLLECTEDRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "LastCollectedResponse" element
	 */
	@Override
	public void setLastCollectedResponse(LastCollectedResponseDocument.LastCollectedResponse lastCollectedResponse) {
		generatedSetterHelperImpl(lastCollectedResponse, LASTCOLLECTEDRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
