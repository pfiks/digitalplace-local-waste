/*
 * An XML document type.
 * Localname: getNoCollectionDatesResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetNoCollectionDatesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetNoCollectionDatesResponseDocument;

/**
 * A document containing one
 * getNoCollectionDatesResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetNoCollectionDatesResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetNoCollectionDatesResponseDocument {

	/**
	 * An XML
	 * getNoCollectionDatesResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetNoCollectionDatesResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse {

		/**
		 * An XML
		 * getNoCollectionDatesResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetNoCollectionDatesResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult {

			private static final long serialVersionUID = 1L;

			public GetNoCollectionDatesResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETNOCOLLECTIONDATESRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getNoCollectionDatesResult");

		private static final long serialVersionUID = 1L;

		public GetNoCollectionDatesResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "getNoCollectionDatesResult" element
		 */
		@Override
		public GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult addNewGetNoCollectionDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult target = null;
				target = (GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult) get_store().add_element_user(GETNOCOLLECTIONDATESRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getNoCollectionDatesResult" element
		 */
		@Override
		public GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult getGetNoCollectionDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult target = null;
				target = (GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult) get_store().find_element_user(GETNOCOLLECTIONDATESRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getNoCollectionDatesResult" element
		 */
		@Override
		public boolean isSetGetNoCollectionDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETNOCOLLECTIONDATESRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getNoCollectionDatesResult" element
		 */
		@Override
		public void setGetNoCollectionDatesResult(GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult getNoCollectionDatesResult) {
			generatedSetterHelperImpl(getNoCollectionDatesResult, GETNOCOLLECTIONDATESRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getNoCollectionDatesResult" element
		 */
		@Override
		public void unsetGetNoCollectionDatesResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETNOCOLLECTIONDATESRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETNOCOLLECTIONDATESRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getNoCollectionDatesResponse");

	private static final long serialVersionUID = 1L;

	public GetNoCollectionDatesResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getNoCollectionDatesResponse" element
	 */
	@Override
	public GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse addNewGetNoCollectionDatesResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse target = null;
			target = (GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse) get_store().add_element_user(GETNOCOLLECTIONDATESRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getNoCollectionDatesResponse" element
	 */
	@Override
	public GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse getGetNoCollectionDatesResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse target = null;
			target = (GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse) get_store().find_element_user(GETNOCOLLECTIONDATESRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getNoCollectionDatesResponse" element
	 */
	@Override
	public void setGetNoCollectionDatesResponse(GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse getNoCollectionDatesResponse) {
		generatedSetterHelperImpl(getNoCollectionDatesResponse, GETNOCOLLECTIONDATESRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
