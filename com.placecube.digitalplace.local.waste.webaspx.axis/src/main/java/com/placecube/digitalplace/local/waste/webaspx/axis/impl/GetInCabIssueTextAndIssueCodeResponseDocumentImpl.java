/*
 * An XML document type.
 * Localname: GetInCabIssueTextAndIssueCodeResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetInCabIssueTextAndIssueCodeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetInCabIssueTextAndIssueCodeResponseDocument;

/**
 * A document containing one
 * GetInCabIssueTextAndIssueCodeResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetInCabIssueTextAndIssueCodeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetInCabIssueTextAndIssueCodeResponseDocument {

	/**
	 * An XML
	 * GetInCabIssueTextAndIssueCodeResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetInCabIssueTextAndIssueCodeResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse {

		/**
		 * An XML
		 * GetInCabIssueTextAndIssueCodeResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetInCabIssueTextAndIssueCodeResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult {

			private static final long serialVersionUID = 1L;

			public GetInCabIssueTextAndIssueCodeResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETINCABISSUETEXTANDISSUECODERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"GetInCabIssueTextAndIssueCodeResult");

		private static final long serialVersionUID = 1L;

		public GetInCabIssueTextAndIssueCodeResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetInCabIssueTextAndIssueCodeResult"
		 * element
		 */
		@Override
		public GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult addNewGetInCabIssueTextAndIssueCodeResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult target = null;
				target = (GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult) get_store()
						.add_element_user(GETINCABISSUETEXTANDISSUECODERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetInCabIssueTextAndIssueCodeResult" element
		 */
		@Override
		public GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult getGetInCabIssueTextAndIssueCodeResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult target = null;
				target = (GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult) get_store()
						.find_element_user(GETINCABISSUETEXTANDISSUECODERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetInCabIssueTextAndIssueCodeResult" element
		 */
		@Override
		public boolean isSetGetInCabIssueTextAndIssueCodeResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETINCABISSUETEXTANDISSUECODERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetInCabIssueTextAndIssueCodeResult" element
		 */
		@Override
		public void setGetInCabIssueTextAndIssueCodeResult(
				GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse.GetInCabIssueTextAndIssueCodeResult getInCabIssueTextAndIssueCodeResult) {
			generatedSetterHelperImpl(getInCabIssueTextAndIssueCodeResult, GETINCABISSUETEXTANDISSUECODERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetInCabIssueTextAndIssueCodeResult" element
		 */
		@Override
		public void unsetGetInCabIssueTextAndIssueCodeResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETINCABISSUETEXTANDISSUECODERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETINCABISSUETEXTANDISSUECODERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"GetInCabIssueTextAndIssueCodeResponse");

	private static final long serialVersionUID = 1L;

	public GetInCabIssueTextAndIssueCodeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetInCabIssueTextAndIssueCodeResponse"
	 * element
	 */
	@Override
	public GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse addNewGetInCabIssueTextAndIssueCodeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse target = null;
			target = (GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse) get_store().add_element_user(GETINCABISSUETEXTANDISSUECODERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetInCabIssueTextAndIssueCodeResponse" element
	 */
	@Override
	public GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse getGetInCabIssueTextAndIssueCodeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse target = null;
			target = (GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse) get_store().find_element_user(GETINCABISSUETEXTANDISSUECODERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetInCabIssueTextAndIssueCodeResponse" element
	 */
	@Override
	public void setGetInCabIssueTextAndIssueCodeResponse(GetInCabIssueTextAndIssueCodeResponseDocument.GetInCabIssueTextAndIssueCodeResponse getInCabIssueTextAndIssueCodeResponse) {
		generatedSetterHelperImpl(getInCabIssueTextAndIssueCodeResponse, GETINCABISSUETEXTANDISSUECODERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
