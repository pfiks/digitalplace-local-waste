/*
 * An XML document type.
 * Localname: GetIssuesResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetIssuesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesResponseDocument;

/**
 * A document containing one
 * GetIssuesResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class GetIssuesResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesResponseDocument {

	/**
	 * An XML GetIssuesResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetIssuesResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesResponseDocument.GetIssuesResponse {

		/**
		 * An XML
		 * GetIssuesResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetIssuesResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesResponseDocument.GetIssuesResponse.GetIssuesResult {

			private static final long serialVersionUID = 1L;

			public GetIssuesResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETISSUESRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssuesResult");

		private static final long serialVersionUID = 1L;

		public GetIssuesResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetIssuesResult" element
		 */
		@Override
		public GetIssuesResponseDocument.GetIssuesResponse.GetIssuesResult addNewGetIssuesResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetIssuesResponseDocument.GetIssuesResponse.GetIssuesResult target = null;
				target = (GetIssuesResponseDocument.GetIssuesResponse.GetIssuesResult) get_store().add_element_user(GETISSUESRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetIssuesResult" element
		 */
		@Override
		public GetIssuesResponseDocument.GetIssuesResponse.GetIssuesResult getGetIssuesResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetIssuesResponseDocument.GetIssuesResponse.GetIssuesResult target = null;
				target = (GetIssuesResponseDocument.GetIssuesResponse.GetIssuesResult) get_store().find_element_user(GETISSUESRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetIssuesResult" element
		 */
		@Override
		public boolean isSetGetIssuesResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETISSUESRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetIssuesResult" element
		 */
		@Override
		public void setGetIssuesResult(GetIssuesResponseDocument.GetIssuesResponse.GetIssuesResult getIssuesResult) {
			generatedSetterHelperImpl(getIssuesResult, GETISSUESRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetIssuesResult" element
		 */
		@Override
		public void unsetGetIssuesResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETISSUESRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETISSUESRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssuesResponse");

	private static final long serialVersionUID = 1L;

	public GetIssuesResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetIssuesResponse" element
	 */
	@Override
	public GetIssuesResponseDocument.GetIssuesResponse addNewGetIssuesResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesResponseDocument.GetIssuesResponse target = null;
			target = (GetIssuesResponseDocument.GetIssuesResponse) get_store().add_element_user(GETISSUESRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetIssuesResponse" element
	 */
	@Override
	public GetIssuesResponseDocument.GetIssuesResponse getGetIssuesResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesResponseDocument.GetIssuesResponse target = null;
			target = (GetIssuesResponseDocument.GetIssuesResponse) get_store().find_element_user(GETISSUESRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetIssuesResponse" element
	 */
	@Override
	public void setGetIssuesResponse(GetIssuesResponseDocument.GetIssuesResponse getIssuesResponse) {
		generatedSetterHelperImpl(getIssuesResponse, GETISSUESRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
