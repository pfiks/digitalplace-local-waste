/*
 * An XML document type.
 * Localname: UpdateBinDetailsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: UpdateBinDetailsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.UpdateBinDetailsResponseDocument;

/**
 * A document containing one
 * UpdateBinDetailsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class UpdateBinDetailsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements UpdateBinDetailsResponseDocument {

	/**
	 * An XML
	 * UpdateBinDetailsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class UpdateBinDetailsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse {

		/**
		 * An XML
		 * UpdateBinDetailsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class UpdateBinDetailsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse.UpdateBinDetailsResult {

			private static final long serialVersionUID = 1L;

			public UpdateBinDetailsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final long serialVersionUID = 1L;

		private static final javax.xml.namespace.QName UPDATEBINDETAILSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateBinDetailsResult");

		public UpdateBinDetailsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "UpdateBinDetailsResult" element
		 */
		@Override
		public UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse.UpdateBinDetailsResult addNewUpdateBinDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse.UpdateBinDetailsResult target = null;
				target = (UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse.UpdateBinDetailsResult) get_store().add_element_user(UPDATEBINDETAILSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "UpdateBinDetailsResult" element
		 */
		@Override
		public UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse.UpdateBinDetailsResult getUpdateBinDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse.UpdateBinDetailsResult target = null;
				target = (UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse.UpdateBinDetailsResult) get_store().find_element_user(UPDATEBINDETAILSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "UpdateBinDetailsResult" element
		 */
		@Override
		public boolean isSetUpdateBinDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPDATEBINDETAILSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "UpdateBinDetailsResult" element
		 */
		@Override
		public void setUpdateBinDetailsResult(UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse.UpdateBinDetailsResult updateBinDetailsResult) {
			generatedSetterHelperImpl(updateBinDetailsResult, UPDATEBINDETAILSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "UpdateBinDetailsResult" element
		 */
		@Override
		public void unsetUpdateBinDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPDATEBINDETAILSRESULT$0, 0);
			}
		}
	}

	private static final long serialVersionUID = 1L;

	private static final javax.xml.namespace.QName UPDATEBINDETAILSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UpdateBinDetailsResponse");

	public UpdateBinDetailsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "UpdateBinDetailsResponse" element
	 */
	@Override
	public UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse addNewUpdateBinDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse target = null;
			target = (UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse) get_store().add_element_user(UPDATEBINDETAILSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "UpdateBinDetailsResponse" element
	 */
	@Override
	public UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse getUpdateBinDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse target = null;
			target = (UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse) get_store().find_element_user(UPDATEBINDETAILSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "UpdateBinDetailsResponse" element
	 */
	@Override
	public void setUpdateBinDetailsResponse(UpdateBinDetailsResponseDocument.UpdateBinDetailsResponse updateBinDetailsResponse) {
		generatedSetterHelperImpl(updateBinDetailsResponse, UPDATEBINDETAILSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
