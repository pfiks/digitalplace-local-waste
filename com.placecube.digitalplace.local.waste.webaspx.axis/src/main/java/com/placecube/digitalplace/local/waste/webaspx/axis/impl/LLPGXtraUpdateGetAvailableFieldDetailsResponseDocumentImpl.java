/*
 * An XML document type.
 * Localname: LLPGXtraUpdateGetAvailableFieldDetailsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument;

/**
 * A document containing one
 * LLPGXtraUpdateGetAvailableFieldDetailsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class LLPGXtraUpdateGetAvailableFieldDetailsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
		implements LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument {

	/**
	 * An XML
	 * LLPGXtraUpdateGetAvailableFieldDetailsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class LLPGXtraUpdateGetAvailableFieldDetailsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse {

		/**
		 * An XML
		 * LLPGXtraUpdateGetAvailableFieldDetailsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class LLPGXtraUpdateGetAvailableFieldDetailsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult {

			private static final long serialVersionUID = 1L;

			public LLPGXtraUpdateGetAvailableFieldDetailsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"LLPGXtraUpdateGetAvailableFieldDetailsResult");

		private static final long serialVersionUID = 1L;

		public LLPGXtraUpdateGetAvailableFieldDetailsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
		 */
		@Override
		public LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult addNewLLPGXtraUpdateGetAvailableFieldDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult target = null;
				target = (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult) get_store()
						.add_element_user(LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
		 */
		@Override
		public LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult getLLPGXtraUpdateGetAvailableFieldDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult target = null;
				target = (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult) get_store()
						.find_element_user(LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
		 */
		@Override
		public boolean isSetLLPGXtraUpdateGetAvailableFieldDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
		 */
		@Override
		public void setLLPGXtraUpdateGetAvailableFieldDetailsResult(
				LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse.LLPGXtraUpdateGetAvailableFieldDetailsResult llpgXtraUpdateGetAvailableFieldDetailsResult) {
			generatedSetterHelperImpl(llpgXtraUpdateGetAvailableFieldDetailsResult, LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "LLPGXtraUpdateGetAvailableFieldDetailsResult" element
		 */
		@Override
		public void unsetLLPGXtraUpdateGetAvailableFieldDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"LLPGXtraUpdateGetAvailableFieldDetailsResponse");

	private static final long serialVersionUID = 1L;

	public LLPGXtraUpdateGetAvailableFieldDetailsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "LLPGXtraUpdateGetAvailableFieldDetailsResponse" element
	 */
	@Override
	public LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse addNewLLPGXtraUpdateGetAvailableFieldDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse target = null;
			target = (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse) get_store()
					.add_element_user(LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "LLPGXtraUpdateGetAvailableFieldDetailsResponse" element
	 */
	@Override
	public LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse getLLPGXtraUpdateGetAvailableFieldDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse target = null;
			target = (LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse) get_store()
					.find_element_user(LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "LLPGXtraUpdateGetAvailableFieldDetailsResponse" element
	 */
	@Override
	public void setLLPGXtraUpdateGetAvailableFieldDetailsResponse(
			LLPGXtraUpdateGetAvailableFieldDetailsResponseDocument.LLPGXtraUpdateGetAvailableFieldDetailsResponse llpgXtraUpdateGetAvailableFieldDetailsResponse) {
		generatedSetterHelperImpl(llpgXtraUpdateGetAvailableFieldDetailsResponse, LLPGXTRAUPDATEGETAVAILABLEFIELDDETAILSRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
