/*
 * An XML document type.
 * Localname: GardenSubscription
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscriptionDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GardenSubscription(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GardenSubscriptionDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscriptionDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscription29eedoctype");
    
    /**
     * Gets the "GardenSubscription" element
     */
    GardenSubscriptionDocument.GardenSubscription getGardenSubscription();
    
    /**
     * Sets the "GardenSubscription" element
     */
    void setGardenSubscription(GardenSubscriptionDocument.GardenSubscription gardenSubscription);
    
    /**
     * Appends and returns a new empty "GardenSubscription" element
     */
    GardenSubscriptionDocument.GardenSubscription addNewGardenSubscription();
    
    /**
     * An XML GardenSubscription(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GardenSubscription extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscription.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscription3880elemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "UPRN" element
         */
        java.lang.String getUPRN();
        
        /**
         * Gets (as xml) the "UPRN" element
         */
        org.apache.xmlbeans.XmlString xgetUPRN();
        
        /**
         * True if has "UPRN" element
         */
        boolean isSetUPRN();
        
        /**
         * Sets the "UPRN" element
         */
        void setUPRN(java.lang.String uprn);
        
        /**
         * Sets (as xml) the "UPRN" element
         */
        void xsetUPRN(org.apache.xmlbeans.XmlString uprn);
        
        /**
         * Unsets the "UPRN" element
         */
        void unsetUPRN();
        
        /**
         * Gets the "binsAtProperty" element
         */
        java.lang.String getBinsAtProperty();
        
        /**
         * Gets (as xml) the "binsAtProperty" element
         */
        org.apache.xmlbeans.XmlString xgetBinsAtProperty();
        
        /**
         * True if has "binsAtProperty" element
         */
        boolean isSetBinsAtProperty();
        
        /**
         * Sets the "binsAtProperty" element
         */
        void setBinsAtProperty(java.lang.String binsAtProperty);
        
        /**
         * Sets (as xml) the "binsAtProperty" element
         */
        void xsetBinsAtProperty(org.apache.xmlbeans.XmlString binsAtProperty);
        
        /**
         * Unsets the "binsAtProperty" element
         */
        void unsetBinsAtProperty();
        
        /**
         * Gets the "totalNoSubsRequired" element
         */
        java.lang.String getTotalNoSubsRequired();
        
        /**
         * Gets (as xml) the "totalNoSubsRequired" element
         */
        org.apache.xmlbeans.XmlString xgetTotalNoSubsRequired();
        
        /**
         * True if has "totalNoSubsRequired" element
         */
        boolean isSetTotalNoSubsRequired();
        
        /**
         * Sets the "totalNoSubsRequired" element
         */
        void setTotalNoSubsRequired(java.lang.String totalNoSubsRequired);
        
        /**
         * Sets (as xml) the "totalNoSubsRequired" element
         */
        void xsetTotalNoSubsRequired(org.apache.xmlbeans.XmlString totalNoSubsRequired);
        
        /**
         * Unsets the "totalNoSubsRequired" element
         */
        void unsetTotalNoSubsRequired();
        
        /**
         * Gets the "subscriptionStartDate" element
         */
        java.lang.String getSubscriptionStartDate();
        
        /**
         * Gets (as xml) the "subscriptionStartDate" element
         */
        org.apache.xmlbeans.XmlString xgetSubscriptionStartDate();
        
        /**
         * True if has "subscriptionStartDate" element
         */
        boolean isSetSubscriptionStartDate();
        
        /**
         * Sets the "subscriptionStartDate" element
         */
        void setSubscriptionStartDate(java.lang.String subscriptionStartDate);
        
        /**
         * Sets (as xml) the "subscriptionStartDate" element
         */
        void xsetSubscriptionStartDate(org.apache.xmlbeans.XmlString subscriptionStartDate);
        
        /**
         * Unsets the "subscriptionStartDate" element
         */
        void unsetSubscriptionStartDate();
        
        /**
         * Gets the "subscriptionEndDate" element
         */
        java.lang.String getSubscriptionEndDate();
        
        /**
         * Gets (as xml) the "subscriptionEndDate" element
         */
        org.apache.xmlbeans.XmlString xgetSubscriptionEndDate();
        
        /**
         * True if has "subscriptionEndDate" element
         */
        boolean isSetSubscriptionEndDate();
        
        /**
         * Sets the "subscriptionEndDate" element
         */
        void setSubscriptionEndDate(java.lang.String subscriptionEndDate);
        
        /**
         * Sets (as xml) the "subscriptionEndDate" element
         */
        void xsetSubscriptionEndDate(org.apache.xmlbeans.XmlString subscriptionEndDate);
        
        /**
         * Unsets the "subscriptionEndDate" element
         */
        void unsetSubscriptionEndDate();
        
        /**
         * Gets the "newPayRef" element
         */
        java.lang.String getNewPayRef();
        
        /**
         * Gets (as xml) the "newPayRef" element
         */
        org.apache.xmlbeans.XmlString xgetNewPayRef();
        
        /**
         * True if has "newPayRef" element
         */
        boolean isSetNewPayRef();
        
        /**
         * Sets the "newPayRef" element
         */
        void setNewPayRef(java.lang.String newPayRef);
        
        /**
         * Sets (as xml) the "newPayRef" element
         */
        void xsetNewPayRef(org.apache.xmlbeans.XmlString newPayRef);
        
        /**
         * Unsets the "newPayRef" element
         */
        void unsetNewPayRef();
        
        /**
         * Gets the "binType" element
         */
        java.lang.String getBinType();
        
        /**
         * Gets (as xml) the "binType" element
         */
        org.apache.xmlbeans.XmlString xgetBinType();
        
        /**
         * True if has "binType" element
         */
        boolean isSetBinType();
        
        /**
         * Sets the "binType" element
         */
        void setBinType(java.lang.String binType);
        
        /**
         * Sets (as xml) the "binType" element
         */
        void xsetBinType(org.apache.xmlbeans.XmlString binType);
        
        /**
         * Unsets the "binType" element
         */
        void unsetBinType();
        
        /**
         * Gets the "gardenContainerDeliveryComments" element
         */
        java.lang.String getGardenContainerDeliveryComments();
        
        /**
         * Gets (as xml) the "gardenContainerDeliveryComments" element
         */
        org.apache.xmlbeans.XmlString xgetGardenContainerDeliveryComments();
        
        /**
         * True if has "gardenContainerDeliveryComments" element
         */
        boolean isSetGardenContainerDeliveryComments();
        
        /**
         * Sets the "gardenContainerDeliveryComments" element
         */
        void setGardenContainerDeliveryComments(java.lang.String gardenContainerDeliveryComments);
        
        /**
         * Sets (as xml) the "gardenContainerDeliveryComments" element
         */
        void xsetGardenContainerDeliveryComments(org.apache.xmlbeans.XmlString gardenContainerDeliveryComments);
        
        /**
         * Unsets the "gardenContainerDeliveryComments" element
         */
        void unsetGardenContainerDeliveryComments();
        
        /**
         * Gets the "crmGardenRef" element
         */
        java.lang.String getCrmGardenRef();
        
        /**
         * Gets (as xml) the "crmGardenRef" element
         */
        org.apache.xmlbeans.XmlString xgetCrmGardenRef();
        
        /**
         * True if has "crmGardenRef" element
         */
        boolean isSetCrmGardenRef();
        
        /**
         * Sets the "crmGardenRef" element
         */
        void setCrmGardenRef(java.lang.String crmGardenRef);
        
        /**
         * Sets (as xml) the "crmGardenRef" element
         */
        void xsetCrmGardenRef(org.apache.xmlbeans.XmlString crmGardenRef);
        
        /**
         * Unsets the "crmGardenRef" element
         */
        void unsetCrmGardenRef();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GardenSubscriptionDocument.GardenSubscription newInstance() {
              return (GardenSubscriptionDocument.GardenSubscription) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GardenSubscriptionDocument.GardenSubscription newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GardenSubscriptionDocument.GardenSubscription) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GardenSubscriptionDocument newInstance() {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GardenSubscriptionDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GardenSubscriptionDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GardenSubscriptionDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GardenSubscriptionDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GardenSubscriptionDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GardenSubscriptionDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GardenSubscriptionDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GardenSubscriptionDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GardenSubscriptionDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GardenSubscriptionDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GardenSubscriptionDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GardenSubscriptionDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GardenSubscriptionDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GardenSubscriptionDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GardenSubscriptionDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscriptionDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscriptionDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscriptionDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
