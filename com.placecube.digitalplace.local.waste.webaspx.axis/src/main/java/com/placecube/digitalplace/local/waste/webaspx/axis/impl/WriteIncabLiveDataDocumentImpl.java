/*
 * An XML document type.
 * Localname: WriteIncabLiveData
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: WriteIncabLiveDataDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.WriteIncabLiveDataDocument;

/**
 * A document containing one
 * WriteIncabLiveData(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class WriteIncabLiveDataDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements WriteIncabLiveDataDocument {

	/**
	 * An XML
	 * WriteIncabLiveData(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class WriteIncabLiveDataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements WriteIncabLiveDataDocument.WriteIncabLiveData {

		private static final javax.xml.namespace.QName ACTIONISSUES$34 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ActionIssues");

		private static final javax.xml.namespace.QName BINNOTOUTISSUES$26 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinNotOutIssues");

		private static final javax.xml.namespace.QName COLLECTED$38 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Collected");
		private static final javax.xml.namespace.QName COMPLETEDSTREETS$22 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CompletedStreets");
		private static final javax.xml.namespace.QName CONTAMINATIONISSUES$28 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "ContaminationIssues");
		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");
		private static final javax.xml.namespace.QName DAMAGEISSUES$30 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "DamageIssues");
		private static final javax.xml.namespace.QName DRIVERCHECKSBAD$20 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "DriverChecksBad");
		private static final javax.xml.namespace.QName DRIVERCHECKSGOOD$18 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "DriverChecksGood");
		private static final javax.xml.namespace.QName GPSPOINTS$42 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GPSPoints");
		private static final javax.xml.namespace.QName HAZARDSISSUES$36 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "HazardsIssues");
		private static final javax.xml.namespace.QName HOURSWORKEDHHMM$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "HoursWorkedHHMM");
		private static final javax.xml.namespace.QName ISSUES$24 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Issues");
		private static final javax.xml.namespace.QName LOGINS$16 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Logins");
		private static final javax.xml.namespace.QName MILES$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Miles");
		private static final javax.xml.namespace.QName OTHERISSUES$32 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "OtherIssues");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName SERVICE$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "Service");
		private static final javax.xml.namespace.QName TIMEHHMM$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "TimeHHMM");
		private static final javax.xml.namespace.QName TIPVISITS$46 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "TipVisits");
		private static final javax.xml.namespace.QName TIPYIELD$48 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "TipYield");
		private static final javax.xml.namespace.QName TOTALDISTANCE$44 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "TotalDistance");
		private static final javax.xml.namespace.QName UNCOLLECTED$40 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UnCollected");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public WriteIncabLiveDataImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "ActionIssues" element
		 */
		@Override
		public java.lang.String getActionIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ACTIONISSUES$34, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "BinNotOutIssues" element
		 */
		@Override
		public java.lang.String getBinNotOutIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINNOTOUTISSUES$26, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "Collected" element
		 */
		@Override
		public java.lang.String getCollected() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COLLECTED$38, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "CompletedStreets" element
		 */
		@Override
		public java.lang.String getCompletedStreets() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COMPLETEDSTREETS$22, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "ContaminationIssues" element
		 */
		@Override
		public java.lang.String getContaminationIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CONTAMINATIONISSUES$28, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "DamageIssues" element
		 */
		@Override
		public java.lang.String getDamageIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DAMAGEISSUES$30, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "DriverChecksBad" element
		 */
		@Override
		public java.lang.String getDriverChecksBad() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DRIVERCHECKSBAD$20, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "DriverChecksGood" element
		 */
		@Override
		public java.lang.String getDriverChecksGood() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DRIVERCHECKSGOOD$18, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "GPSPoints" element
		 */
		@Override
		public java.lang.String getGPSPoints() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(GPSPOINTS$42, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "HazardsIssues" element
		 */
		@Override
		public java.lang.String getHazardsIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(HAZARDSISSUES$36, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "HoursWorkedHHMM" element
		 */
		@Override
		public java.lang.String getHoursWorkedHHMM() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(HOURSWORKEDHHMM$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "Issues" element
		 */
		@Override
		public java.lang.String getIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUES$24, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "Logins" element
		 */
		@Override
		public java.lang.String getLogins() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(LOGINS$16, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "Miles" element
		 */
		@Override
		public java.lang.String getMiles() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(MILES$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "OtherIssues" element
		 */
		@Override
		public java.lang.String getOtherIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(OTHERISSUES$32, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "Service" element
		 */
		@Override
		public java.lang.String getService() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SERVICE$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "TimeHHMM" element
		 */
		@Override
		public java.lang.String getTimeHHMM() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TIMEHHMM$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "TipVisits" element
		 */
		@Override
		public java.lang.String getTipVisits() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TIPVISITS$46, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "TipYield" element
		 */
		@Override
		public java.lang.String getTipYield() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TIPYIELD$48, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "TotalDistance" element
		 */
		@Override
		public java.lang.String getTotalDistance() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TOTALDISTANCE$44, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UnCollected" element
		 */
		@Override
		public java.lang.String getUnCollected() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UNCOLLECTED$40, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "ActionIssues" element
		 */
		@Override
		public boolean isSetActionIssues() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ACTIONISSUES$34) != 0;
			}
		}

		/**
		 * True if has "BinNotOutIssues" element
		 */
		@Override
		public boolean isSetBinNotOutIssues() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BINNOTOUTISSUES$26) != 0;
			}
		}

		/**
		 * True if has "Collected" element
		 */
		@Override
		public boolean isSetCollected() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COLLECTED$38) != 0;
			}
		}

		/**
		 * True if has "CompletedStreets" element
		 */
		@Override
		public boolean isSetCompletedStreets() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COMPLETEDSTREETS$22) != 0;
			}
		}

		/**
		 * True if has "ContaminationIssues" element
		 */
		@Override
		public boolean isSetContaminationIssues() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CONTAMINATIONISSUES$28) != 0;
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "DamageIssues" element
		 */
		@Override
		public boolean isSetDamageIssues() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DAMAGEISSUES$30) != 0;
			}
		}

		/**
		 * True if has "DriverChecksBad" element
		 */
		@Override
		public boolean isSetDriverChecksBad() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DRIVERCHECKSBAD$20) != 0;
			}
		}

		/**
		 * True if has "DriverChecksGood" element
		 */
		@Override
		public boolean isSetDriverChecksGood() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DRIVERCHECKSGOOD$18) != 0;
			}
		}

		/**
		 * True if has "GPSPoints" element
		 */
		@Override
		public boolean isSetGPSPoints() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GPSPOINTS$42) != 0;
			}
		}

		/**
		 * True if has "HazardsIssues" element
		 */
		@Override
		public boolean isSetHazardsIssues() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(HAZARDSISSUES$36) != 0;
			}
		}

		/**
		 * True if has "HoursWorkedHHMM" element
		 */
		@Override
		public boolean isSetHoursWorkedHHMM() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(HOURSWORKEDHHMM$14) != 0;
			}
		}

		/**
		 * True if has "Issues" element
		 */
		@Override
		public boolean isSetIssues() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ISSUES$24) != 0;
			}
		}

		/**
		 * True if has "Logins" element
		 */
		@Override
		public boolean isSetLogins() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(LOGINS$16) != 0;
			}
		}

		/**
		 * True if has "Miles" element
		 */
		@Override
		public boolean isSetMiles() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(MILES$12) != 0;
			}
		}

		/**
		 * True if has "OtherIssues" element
		 */
		@Override
		public boolean isSetOtherIssues() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(OTHERISSUES$32) != 0;
			}
		}

		/**
		 * True if has "Service" element
		 */
		@Override
		public boolean isSetService() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SERVICE$8) != 0;
			}
		}

		/**
		 * True if has "TimeHHMM" element
		 */
		@Override
		public boolean isSetTimeHHMM() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(TIMEHHMM$10) != 0;
			}
		}

		/**
		 * True if has "TipVisits" element
		 */
		@Override
		public boolean isSetTipVisits() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(TIPVISITS$46) != 0;
			}
		}

		/**
		 * True if has "TipYield" element
		 */
		@Override
		public boolean isSetTipYield() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(TIPYIELD$48) != 0;
			}
		}

		/**
		 * True if has "TotalDistance" element
		 */
		@Override
		public boolean isSetTotalDistance() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(TOTALDISTANCE$44) != 0;
			}
		}

		/**
		 * True if has "UnCollected" element
		 */
		@Override
		public boolean isSetUnCollected() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UNCOLLECTED$40) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "ActionIssues" element
		 */
		@Override
		public void setActionIssues(java.lang.String actionIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ACTIONISSUES$34, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ACTIONISSUES$34);
				}
				target.setStringValue(actionIssues);
			}
		}

		/**
		 * Sets the "BinNotOutIssues" element
		 */
		@Override
		public void setBinNotOutIssues(java.lang.String binNotOutIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(BINNOTOUTISSUES$26, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(BINNOTOUTISSUES$26);
				}
				target.setStringValue(binNotOutIssues);
			}
		}

		/**
		 * Sets the "Collected" element
		 */
		@Override
		public void setCollected(java.lang.String collected) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COLLECTED$38, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COLLECTED$38);
				}
				target.setStringValue(collected);
			}
		}

		/**
		 * Sets the "CompletedStreets" element
		 */
		@Override
		public void setCompletedStreets(java.lang.String completedStreets) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COMPLETEDSTREETS$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COMPLETEDSTREETS$22);
				}
				target.setStringValue(completedStreets);
			}
		}

		/**
		 * Sets the "ContaminationIssues" element
		 */
		@Override
		public void setContaminationIssues(java.lang.String contaminationIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(CONTAMINATIONISSUES$28, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(CONTAMINATIONISSUES$28);
				}
				target.setStringValue(contaminationIssues);
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "DamageIssues" element
		 */
		@Override
		public void setDamageIssues(java.lang.String damageIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DAMAGEISSUES$30, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DAMAGEISSUES$30);
				}
				target.setStringValue(damageIssues);
			}
		}

		/**
		 * Sets the "DriverChecksBad" element
		 */
		@Override
		public void setDriverChecksBad(java.lang.String driverChecksBad) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DRIVERCHECKSBAD$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DRIVERCHECKSBAD$20);
				}
				target.setStringValue(driverChecksBad);
			}
		}

		/**
		 * Sets the "DriverChecksGood" element
		 */
		@Override
		public void setDriverChecksGood(java.lang.String driverChecksGood) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DRIVERCHECKSGOOD$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DRIVERCHECKSGOOD$18);
				}
				target.setStringValue(driverChecksGood);
			}
		}

		/**
		 * Sets the "GPSPoints" element
		 */
		@Override
		public void setGPSPoints(java.lang.String gpsPoints) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(GPSPOINTS$42, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(GPSPOINTS$42);
				}
				target.setStringValue(gpsPoints);
			}
		}

		/**
		 * Sets the "HazardsIssues" element
		 */
		@Override
		public void setHazardsIssues(java.lang.String hazardsIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(HAZARDSISSUES$36, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(HAZARDSISSUES$36);
				}
				target.setStringValue(hazardsIssues);
			}
		}

		/**
		 * Sets the "HoursWorkedHHMM" element
		 */
		@Override
		public void setHoursWorkedHHMM(java.lang.String hoursWorkedHHMM) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(HOURSWORKEDHHMM$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(HOURSWORKEDHHMM$14);
				}
				target.setStringValue(hoursWorkedHHMM);
			}
		}

		/**
		 * Sets the "Issues" element
		 */
		@Override
		public void setIssues(java.lang.String issues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUES$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ISSUES$24);
				}
				target.setStringValue(issues);
			}
		}

		/**
		 * Sets the "Logins" element
		 */
		@Override
		public void setLogins(java.lang.String logins) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(LOGINS$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(LOGINS$16);
				}
				target.setStringValue(logins);
			}
		}

		/**
		 * Sets the "Miles" element
		 */
		@Override
		public void setMiles(java.lang.String miles) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(MILES$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(MILES$12);
				}
				target.setStringValue(miles);
			}
		}

		/**
		 * Sets the "OtherIssues" element
		 */
		@Override
		public void setOtherIssues(java.lang.String otherIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(OTHERISSUES$32, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(OTHERISSUES$32);
				}
				target.setStringValue(otherIssues);
			}
		}

		/**
		 * Sets the "Service" element
		 */
		@Override
		public void setService(java.lang.String service) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SERVICE$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(SERVICE$8);
				}
				target.setStringValue(service);
			}
		}

		/**
		 * Sets the "TimeHHMM" element
		 */
		@Override
		public void setTimeHHMM(java.lang.String timeHHMM) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TIMEHHMM$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(TIMEHHMM$10);
				}
				target.setStringValue(timeHHMM);
			}
		}

		/**
		 * Sets the "TipVisits" element
		 */
		@Override
		public void setTipVisits(java.lang.String tipVisits) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TIPVISITS$46, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(TIPVISITS$46);
				}
				target.setStringValue(tipVisits);
			}
		}

		/**
		 * Sets the "TipYield" element
		 */
		@Override
		public void setTipYield(java.lang.String tipYield) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TIPYIELD$48, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(TIPYIELD$48);
				}
				target.setStringValue(tipYield);
			}
		}

		/**
		 * Sets the "TotalDistance" element
		 */
		@Override
		public void setTotalDistance(java.lang.String totalDistance) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(TOTALDISTANCE$44, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(TOTALDISTANCE$44);
				}
				target.setStringValue(totalDistance);
			}
		}

		/**
		 * Sets the "UnCollected" element
		 */
		@Override
		public void setUnCollected(java.lang.String unCollected) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UNCOLLECTED$40, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UNCOLLECTED$40);
				}
				target.setStringValue(unCollected);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "ActionIssues" element
		 */
		@Override
		public void unsetActionIssues() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ACTIONISSUES$34, 0);
			}
		}

		/**
		 * Unsets the "BinNotOutIssues" element
		 */
		@Override
		public void unsetBinNotOutIssues() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BINNOTOUTISSUES$26, 0);
			}
		}

		/**
		 * Unsets the "Collected" element
		 */
		@Override
		public void unsetCollected() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COLLECTED$38, 0);
			}
		}

		/**
		 * Unsets the "CompletedStreets" element
		 */
		@Override
		public void unsetCompletedStreets() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COMPLETEDSTREETS$22, 0);
			}
		}

		/**
		 * Unsets the "ContaminationIssues" element
		 */
		@Override
		public void unsetContaminationIssues() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CONTAMINATIONISSUES$28, 0);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "DamageIssues" element
		 */
		@Override
		public void unsetDamageIssues() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DAMAGEISSUES$30, 0);
			}
		}

		/**
		 * Unsets the "DriverChecksBad" element
		 */
		@Override
		public void unsetDriverChecksBad() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DRIVERCHECKSBAD$20, 0);
			}
		}

		/**
		 * Unsets the "DriverChecksGood" element
		 */
		@Override
		public void unsetDriverChecksGood() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DRIVERCHECKSGOOD$18, 0);
			}
		}

		/**
		 * Unsets the "GPSPoints" element
		 */
		@Override
		public void unsetGPSPoints() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GPSPOINTS$42, 0);
			}
		}

		/**
		 * Unsets the "HazardsIssues" element
		 */
		@Override
		public void unsetHazardsIssues() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(HAZARDSISSUES$36, 0);
			}
		}

		/**
		 * Unsets the "HoursWorkedHHMM" element
		 */
		@Override
		public void unsetHoursWorkedHHMM() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(HOURSWORKEDHHMM$14, 0);
			}
		}

		/**
		 * Unsets the "Issues" element
		 */
		@Override
		public void unsetIssues() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ISSUES$24, 0);
			}
		}

		/**
		 * Unsets the "Logins" element
		 */
		@Override
		public void unsetLogins() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(LOGINS$16, 0);
			}
		}

		/**
		 * Unsets the "Miles" element
		 */
		@Override
		public void unsetMiles() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(MILES$12, 0);
			}
		}

		/**
		 * Unsets the "OtherIssues" element
		 */
		@Override
		public void unsetOtherIssues() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(OTHERISSUES$32, 0);
			}
		}

		/**
		 * Unsets the "Service" element
		 */
		@Override
		public void unsetService() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SERVICE$8, 0);
			}
		}

		/**
		 * Unsets the "TimeHHMM" element
		 */
		@Override
		public void unsetTimeHHMM() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(TIMEHHMM$10, 0);
			}
		}

		/**
		 * Unsets the "TipVisits" element
		 */
		@Override
		public void unsetTipVisits() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(TIPVISITS$46, 0);
			}
		}

		/**
		 * Unsets the "TipYield" element
		 */
		@Override
		public void unsetTipYield() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(TIPYIELD$48, 0);
			}
		}

		/**
		 * Unsets the "TotalDistance" element
		 */
		@Override
		public void unsetTotalDistance() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(TOTALDISTANCE$44, 0);
			}
		}

		/**
		 * Unsets the "UnCollected" element
		 */
		@Override
		public void unsetUnCollected() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UNCOLLECTED$40, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "ActionIssues" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetActionIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ACTIONISSUES$34, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "BinNotOutIssues" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetBinNotOutIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINNOTOUTISSUES$26, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "Collected" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCollected() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COLLECTED$38, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "CompletedStreets" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCompletedStreets() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COMPLETEDSTREETS$22, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "ContaminationIssues" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetContaminationIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CONTAMINATIONISSUES$28, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "DamageIssues" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDamageIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DAMAGEISSUES$30, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "DriverChecksBad" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDriverChecksBad() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DRIVERCHECKSBAD$20, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "DriverChecksGood" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDriverChecksGood() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DRIVERCHECKSGOOD$18, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "GPSPoints" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetGPSPoints() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(GPSPOINTS$42, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "HazardsIssues" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetHazardsIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(HAZARDSISSUES$36, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "HoursWorkedHHMM" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetHoursWorkedHHMM() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(HOURSWORKEDHHMM$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "Issues" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUES$24, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "Logins" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetLogins() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(LOGINS$16, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "Miles" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetMiles() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(MILES$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "OtherIssues" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetOtherIssues() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(OTHERISSUES$32, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "Service" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetService() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SERVICE$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "TimeHHMM" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetTimeHHMM() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TIMEHHMM$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "TipVisits" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetTipVisits() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TIPVISITS$46, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "TipYield" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetTipYield() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TIPYIELD$48, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "TotalDistance" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetTotalDistance() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TOTALDISTANCE$44, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UnCollected" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUnCollected() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UNCOLLECTED$40, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "ActionIssues" element
		 */
		@Override
		public void xsetActionIssues(org.apache.xmlbeans.XmlString actionIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ACTIONISSUES$34, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ACTIONISSUES$34);
				}
				target.set(actionIssues);
			}
		}

		/**
		 * Sets (as xml) the "BinNotOutIssues" element
		 */
		@Override
		public void xsetBinNotOutIssues(org.apache.xmlbeans.XmlString binNotOutIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(BINNOTOUTISSUES$26, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(BINNOTOUTISSUES$26);
				}
				target.set(binNotOutIssues);
			}
		}

		/**
		 * Sets (as xml) the "Collected" element
		 */
		@Override
		public void xsetCollected(org.apache.xmlbeans.XmlString collected) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COLLECTED$38, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COLLECTED$38);
				}
				target.set(collected);
			}
		}

		/**
		 * Sets (as xml) the "CompletedStreets" element
		 */
		@Override
		public void xsetCompletedStreets(org.apache.xmlbeans.XmlString completedStreets) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COMPLETEDSTREETS$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COMPLETEDSTREETS$22);
				}
				target.set(completedStreets);
			}
		}

		/**
		 * Sets (as xml) the "ContaminationIssues" element
		 */
		@Override
		public void xsetContaminationIssues(org.apache.xmlbeans.XmlString contaminationIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(CONTAMINATIONISSUES$28, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(CONTAMINATIONISSUES$28);
				}
				target.set(contaminationIssues);
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "DamageIssues" element
		 */
		@Override
		public void xsetDamageIssues(org.apache.xmlbeans.XmlString damageIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DAMAGEISSUES$30, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DAMAGEISSUES$30);
				}
				target.set(damageIssues);
			}
		}

		/**
		 * Sets (as xml) the "DriverChecksBad" element
		 */
		@Override
		public void xsetDriverChecksBad(org.apache.xmlbeans.XmlString driverChecksBad) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DRIVERCHECKSBAD$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DRIVERCHECKSBAD$20);
				}
				target.set(driverChecksBad);
			}
		}

		/**
		 * Sets (as xml) the "DriverChecksGood" element
		 */
		@Override
		public void xsetDriverChecksGood(org.apache.xmlbeans.XmlString driverChecksGood) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DRIVERCHECKSGOOD$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DRIVERCHECKSGOOD$18);
				}
				target.set(driverChecksGood);
			}
		}

		/**
		 * Sets (as xml) the "GPSPoints" element
		 */
		@Override
		public void xsetGPSPoints(org.apache.xmlbeans.XmlString gpsPoints) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(GPSPOINTS$42, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(GPSPOINTS$42);
				}
				target.set(gpsPoints);
			}
		}

		/**
		 * Sets (as xml) the "HazardsIssues" element
		 */
		@Override
		public void xsetHazardsIssues(org.apache.xmlbeans.XmlString hazardsIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(HAZARDSISSUES$36, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(HAZARDSISSUES$36);
				}
				target.set(hazardsIssues);
			}
		}

		/**
		 * Sets (as xml) the "HoursWorkedHHMM" element
		 */
		@Override
		public void xsetHoursWorkedHHMM(org.apache.xmlbeans.XmlString hoursWorkedHHMM) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(HOURSWORKEDHHMM$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(HOURSWORKEDHHMM$14);
				}
				target.set(hoursWorkedHHMM);
			}
		}

		/**
		 * Sets (as xml) the "Issues" element
		 */
		@Override
		public void xsetIssues(org.apache.xmlbeans.XmlString issues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUES$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ISSUES$24);
				}
				target.set(issues);
			}
		}

		/**
		 * Sets (as xml) the "Logins" element
		 */
		@Override
		public void xsetLogins(org.apache.xmlbeans.XmlString logins) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(LOGINS$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(LOGINS$16);
				}
				target.set(logins);
			}
		}

		/**
		 * Sets (as xml) the "Miles" element
		 */
		@Override
		public void xsetMiles(org.apache.xmlbeans.XmlString miles) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(MILES$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(MILES$12);
				}
				target.set(miles);
			}
		}

		/**
		 * Sets (as xml) the "OtherIssues" element
		 */
		@Override
		public void xsetOtherIssues(org.apache.xmlbeans.XmlString otherIssues) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(OTHERISSUES$32, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(OTHERISSUES$32);
				}
				target.set(otherIssues);
			}
		}

		/**
		 * Sets (as xml) the "Service" element
		 */
		@Override
		public void xsetService(org.apache.xmlbeans.XmlString service) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SERVICE$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(SERVICE$8);
				}
				target.set(service);
			}
		}

		/**
		 * Sets (as xml) the "TimeHHMM" element
		 */
		@Override
		public void xsetTimeHHMM(org.apache.xmlbeans.XmlString timeHHMM) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TIMEHHMM$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(TIMEHHMM$10);
				}
				target.set(timeHHMM);
			}
		}

		/**
		 * Sets (as xml) the "TipVisits" element
		 */
		@Override
		public void xsetTipVisits(org.apache.xmlbeans.XmlString tipVisits) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TIPVISITS$46, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(TIPVISITS$46);
				}
				target.set(tipVisits);
			}
		}

		/**
		 * Sets (as xml) the "TipYield" element
		 */
		@Override
		public void xsetTipYield(org.apache.xmlbeans.XmlString tipYield) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TIPYIELD$48, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(TIPYIELD$48);
				}
				target.set(tipYield);
			}
		}

		/**
		 * Sets (as xml) the "TotalDistance" element
		 */
		@Override
		public void xsetTotalDistance(org.apache.xmlbeans.XmlString totalDistance) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(TOTALDISTANCE$44, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(TOTALDISTANCE$44);
				}
				target.set(totalDistance);
			}
		}

		/**
		 * Sets (as xml) the "UnCollected" element
		 */
		@Override
		public void xsetUnCollected(org.apache.xmlbeans.XmlString unCollected) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UNCOLLECTED$40, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UNCOLLECTED$40);
				}
				target.set(unCollected);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final long serialVersionUID = 1L;

	private static final javax.xml.namespace.QName WRITEINCABLIVEDATA$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "WriteIncabLiveData");

	public WriteIncabLiveDataDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "WriteIncabLiveData" element
	 */
	@Override
	public WriteIncabLiveDataDocument.WriteIncabLiveData addNewWriteIncabLiveData() {
		synchronized (monitor()) {
			check_orphaned();
			WriteIncabLiveDataDocument.WriteIncabLiveData target = null;
			target = (WriteIncabLiveDataDocument.WriteIncabLiveData) get_store().add_element_user(WRITEINCABLIVEDATA$0);
			return target;
		}
	}

	/**
	 * Gets the "WriteIncabLiveData" element
	 */
	@Override
	public WriteIncabLiveDataDocument.WriteIncabLiveData getWriteIncabLiveData() {
		synchronized (monitor()) {
			check_orphaned();
			WriteIncabLiveDataDocument.WriteIncabLiveData target = null;
			target = (WriteIncabLiveDataDocument.WriteIncabLiveData) get_store().find_element_user(WRITEINCABLIVEDATA$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "WriteIncabLiveData" element
	 */
	@Override
	public void setWriteIncabLiveData(WriteIncabLiveDataDocument.WriteIncabLiveData writeIncabLiveData) {
		generatedSetterHelperImpl(writeIncabLiveData, WRITEINCABLIVEDATA$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
