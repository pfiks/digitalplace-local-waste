/*
 * An XML document type.
 * Localname: GetIssues
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetIssuesDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesDocument;

/**
 * A document containing one
 * GetIssues(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class GetIssuesDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesDocument {

	/**
	 * An XML GetIssues(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetIssuesImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesDocument.GetIssues {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName DATEREQ$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "dateReq");

		private static final javax.xml.namespace.QName ISSUEENDTIME$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "issueEndTime");
		private static final javax.xml.namespace.QName ISSUEFILTERNAME$18 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "issueFilterName");
		private static final javax.xml.namespace.QName ISSUEFILTERTYPE$20 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "issueFilterType");
		private static final javax.xml.namespace.QName ISSUESORTFIELD$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "issueSortField");
		private static final javax.xml.namespace.QName ISSUESORTORDER$16 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "issueSortOrder");
		private static final javax.xml.namespace.QName ISSUESTARTTIME$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "issueStartTime");
		private static final javax.xml.namespace.QName PROPNONAME$24 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "propNoName");
		private static final javax.xml.namespace.QName PROPSTREET$26 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "propStreet");
		private static final javax.xml.namespace.QName SEARCHALLDATES$22 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "searchAllDates");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName UPRN$28 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRN");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public GetIssuesImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "dateReq" element
		 */
		@Override
		public java.lang.String getDateReq() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATEREQ$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "issueEndTime" element
		 */
		@Override
		public java.lang.String getIssueEndTime() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUEENDTIME$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "issueFilterName" element
		 */
		@Override
		public java.lang.String getIssueFilterName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUEFILTERNAME$18, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "issueFilterType" element
		 */
		@Override
		public java.lang.String getIssueFilterType() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUEFILTERTYPE$20, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "issueSortField" element
		 */
		@Override
		public java.lang.String getIssueSortField() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUESORTFIELD$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "issueSortOrder" element
		 */
		@Override
		public java.lang.String getIssueSortOrder() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUESORTORDER$16, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "issueStartTime" element
		 */
		@Override
		public java.lang.String getIssueStartTime() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUESTARTTIME$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "propNoName" element
		 */
		@Override
		public java.lang.String getPropNoName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(PROPNONAME$24, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "propStreet" element
		 */
		@Override
		public java.lang.String getPropStreet() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(PROPSTREET$26, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "searchAllDates" element
		 */
		@Override
		public java.lang.String getSearchAllDates() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SEARCHALLDATES$22, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRN" element
		 */
		@Override
		public java.lang.String getUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$28, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "dateReq" element
		 */
		@Override
		public boolean isSetDateReq() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(DATEREQ$8) != 0;
			}
		}

		/**
		 * True if has "issueEndTime" element
		 */
		@Override
		public boolean isSetIssueEndTime() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ISSUEENDTIME$12) != 0;
			}
		}

		/**
		 * True if has "issueFilterName" element
		 */
		@Override
		public boolean isSetIssueFilterName() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ISSUEFILTERNAME$18) != 0;
			}
		}

		/**
		 * True if has "issueFilterType" element
		 */
		@Override
		public boolean isSetIssueFilterType() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ISSUEFILTERTYPE$20) != 0;
			}
		}

		/**
		 * True if has "issueSortField" element
		 */
		@Override
		public boolean isSetIssueSortField() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ISSUESORTFIELD$14) != 0;
			}
		}

		/**
		 * True if has "issueSortOrder" element
		 */
		@Override
		public boolean isSetIssueSortOrder() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ISSUESORTORDER$16) != 0;
			}
		}

		/**
		 * True if has "issueStartTime" element
		 */
		@Override
		public boolean isSetIssueStartTime() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ISSUESTARTTIME$10) != 0;
			}
		}

		/**
		 * True if has "propNoName" element
		 */
		@Override
		public boolean isSetPropNoName() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(PROPNONAME$24) != 0;
			}
		}

		/**
		 * True if has "propStreet" element
		 */
		@Override
		public boolean isSetPropStreet() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(PROPSTREET$26) != 0;
			}
		}

		/**
		 * True if has "searchAllDates" element
		 */
		@Override
		public boolean isSetSearchAllDates() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SEARCHALLDATES$22) != 0;
			}
		}

		/**
		 * True if has "UPRN" element
		 */
		@Override
		public boolean isSetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRN$28) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "dateReq" element
		 */
		@Override
		public void setDateReq(java.lang.String dateReq) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(DATEREQ$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(DATEREQ$8);
				}
				target.setStringValue(dateReq);
			}
		}

		/**
		 * Sets the "issueEndTime" element
		 */
		@Override
		public void setIssueEndTime(java.lang.String issueEndTime) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUEENDTIME$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ISSUEENDTIME$12);
				}
				target.setStringValue(issueEndTime);
			}
		}

		/**
		 * Sets the "issueFilterName" element
		 */
		@Override
		public void setIssueFilterName(java.lang.String issueFilterName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUEFILTERNAME$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ISSUEFILTERNAME$18);
				}
				target.setStringValue(issueFilterName);
			}
		}

		/**
		 * Sets the "issueFilterType" element
		 */
		@Override
		public void setIssueFilterType(java.lang.String issueFilterType) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUEFILTERTYPE$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ISSUEFILTERTYPE$20);
				}
				target.setStringValue(issueFilterType);
			}
		}

		/**
		 * Sets the "issueSortField" element
		 */
		@Override
		public void setIssueSortField(java.lang.String issueSortField) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUESORTFIELD$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ISSUESORTFIELD$14);
				}
				target.setStringValue(issueSortField);
			}
		}

		/**
		 * Sets the "issueSortOrder" element
		 */
		@Override
		public void setIssueSortOrder(java.lang.String issueSortOrder) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUESORTORDER$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ISSUESORTORDER$16);
				}
				target.setStringValue(issueSortOrder);
			}
		}

		/**
		 * Sets the "issueStartTime" element
		 */
		@Override
		public void setIssueStartTime(java.lang.String issueStartTime) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ISSUESTARTTIME$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ISSUESTARTTIME$10);
				}
				target.setStringValue(issueStartTime);
			}
		}

		/**
		 * Sets the "propNoName" element
		 */
		@Override
		public void setPropNoName(java.lang.String propNoName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(PROPNONAME$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(PROPNONAME$24);
				}
				target.setStringValue(propNoName);
			}
		}

		/**
		 * Sets the "propStreet" element
		 */
		@Override
		public void setPropStreet(java.lang.String propStreet) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(PROPSTREET$26, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(PROPSTREET$26);
				}
				target.setStringValue(propStreet);
			}
		}

		/**
		 * Sets the "searchAllDates" element
		 */
		@Override
		public void setSearchAllDates(java.lang.String searchAllDates) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SEARCHALLDATES$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(SEARCHALLDATES$22);
				}
				target.setStringValue(searchAllDates);
			}
		}

		/**
		 * Sets the "UPRN" element
		 */
		@Override
		public void setUPRN(java.lang.String uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$28, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRN$28);
				}
				target.setStringValue(uprn);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "dateReq" element
		 */
		@Override
		public void unsetDateReq() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(DATEREQ$8, 0);
			}
		}

		/**
		 * Unsets the "issueEndTime" element
		 */
		@Override
		public void unsetIssueEndTime() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ISSUEENDTIME$12, 0);
			}
		}

		/**
		 * Unsets the "issueFilterName" element
		 */
		@Override
		public void unsetIssueFilterName() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ISSUEFILTERNAME$18, 0);
			}
		}

		/**
		 * Unsets the "issueFilterType" element
		 */
		@Override
		public void unsetIssueFilterType() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ISSUEFILTERTYPE$20, 0);
			}
		}

		/**
		 * Unsets the "issueSortField" element
		 */
		@Override
		public void unsetIssueSortField() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ISSUESORTFIELD$14, 0);
			}
		}

		/**
		 * Unsets the "issueSortOrder" element
		 */
		@Override
		public void unsetIssueSortOrder() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ISSUESORTORDER$16, 0);
			}
		}

		/**
		 * Unsets the "issueStartTime" element
		 */
		@Override
		public void unsetIssueStartTime() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ISSUESTARTTIME$10, 0);
			}
		}

		/**
		 * Unsets the "propNoName" element
		 */
		@Override
		public void unsetPropNoName() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(PROPNONAME$24, 0);
			}
		}

		/**
		 * Unsets the "propStreet" element
		 */
		@Override
		public void unsetPropStreet() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(PROPSTREET$26, 0);
			}
		}

		/**
		 * Unsets the "searchAllDates" element
		 */
		@Override
		public void unsetSearchAllDates() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SEARCHALLDATES$22, 0);
			}
		}

		/**
		 * Unsets the "UPRN" element
		 */
		@Override
		public void unsetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRN$28, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "dateReq" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetDateReq() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATEREQ$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "issueEndTime" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetIssueEndTime() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUEENDTIME$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "issueFilterName" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetIssueFilterName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUEFILTERNAME$18, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "issueFilterType" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetIssueFilterType() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUEFILTERTYPE$20, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "issueSortField" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetIssueSortField() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUESORTFIELD$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "issueSortOrder" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetIssueSortOrder() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUESORTORDER$16, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "issueStartTime" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetIssueStartTime() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUESTARTTIME$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "propNoName" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetPropNoName() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(PROPNONAME$24, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "propStreet" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetPropStreet() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(PROPSTREET$26, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "searchAllDates" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetSearchAllDates() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SEARCHALLDATES$22, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$28, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "dateReq" element
		 */
		@Override
		public void xsetDateReq(org.apache.xmlbeans.XmlString dateReq) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(DATEREQ$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(DATEREQ$8);
				}
				target.set(dateReq);
			}
		}

		/**
		 * Sets (as xml) the "issueEndTime" element
		 */
		@Override
		public void xsetIssueEndTime(org.apache.xmlbeans.XmlString issueEndTime) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUEENDTIME$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ISSUEENDTIME$12);
				}
				target.set(issueEndTime);
			}
		}

		/**
		 * Sets (as xml) the "issueFilterName" element
		 */
		@Override
		public void xsetIssueFilterName(org.apache.xmlbeans.XmlString issueFilterName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUEFILTERNAME$18, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ISSUEFILTERNAME$18);
				}
				target.set(issueFilterName);
			}
		}

		/**
		 * Sets (as xml) the "issueFilterType" element
		 */
		@Override
		public void xsetIssueFilterType(org.apache.xmlbeans.XmlString issueFilterType) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUEFILTERTYPE$20, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ISSUEFILTERTYPE$20);
				}
				target.set(issueFilterType);
			}
		}

		/**
		 * Sets (as xml) the "issueSortField" element
		 */
		@Override
		public void xsetIssueSortField(org.apache.xmlbeans.XmlString issueSortField) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUESORTFIELD$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ISSUESORTFIELD$14);
				}
				target.set(issueSortField);
			}
		}

		/**
		 * Sets (as xml) the "issueSortOrder" element
		 */
		@Override
		public void xsetIssueSortOrder(org.apache.xmlbeans.XmlString issueSortOrder) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUESORTORDER$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ISSUESORTORDER$16);
				}
				target.set(issueSortOrder);
			}
		}

		/**
		 * Sets (as xml) the "issueStartTime" element
		 */
		@Override
		public void xsetIssueStartTime(org.apache.xmlbeans.XmlString issueStartTime) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ISSUESTARTTIME$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ISSUESTARTTIME$10);
				}
				target.set(issueStartTime);
			}
		}

		/**
		 * Sets (as xml) the "propNoName" element
		 */
		@Override
		public void xsetPropNoName(org.apache.xmlbeans.XmlString propNoName) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(PROPNONAME$24, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(PROPNONAME$24);
				}
				target.set(propNoName);
			}
		}

		/**
		 * Sets (as xml) the "propStreet" element
		 */
		@Override
		public void xsetPropStreet(org.apache.xmlbeans.XmlString propStreet) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(PROPSTREET$26, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(PROPSTREET$26);
				}
				target.set(propStreet);
			}
		}

		/**
		 * Sets (as xml) the "searchAllDates" element
		 */
		@Override
		public void xsetSearchAllDates(org.apache.xmlbeans.XmlString searchAllDates) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SEARCHALLDATES$22, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(SEARCHALLDATES$22);
				}
				target.set(searchAllDates);
			}
		}

		/**
		 * Sets (as xml) the "UPRN" element
		 */
		@Override
		public void xsetUPRN(org.apache.xmlbeans.XmlString uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$28, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRN$28);
				}
				target.set(uprn);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName GETISSUES$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetIssues");

	private static final long serialVersionUID = 1L;

	public GetIssuesDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetIssues" element
	 */
	@Override
	public GetIssuesDocument.GetIssues addNewGetIssues() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesDocument.GetIssues target = null;
			target = (GetIssuesDocument.GetIssues) get_store().add_element_user(GETISSUES$0);
			return target;
		}
	}

	/**
	 * Gets the "GetIssues" element
	 */
	@Override
	public GetIssuesDocument.GetIssues getGetIssues() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesDocument.GetIssues target = null;
			target = (GetIssuesDocument.GetIssues) get_store().find_element_user(GETISSUES$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetIssues" element
	 */
	@Override
	public void setGetIssues(GetIssuesDocument.GetIssues getIssues) {
		generatedSetterHelperImpl(getIssues, GETISSUES$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
