/*
 * An XML document type.
 * Localname: BinDeleteResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: BinDeleteResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.BinDeleteResponseDocument;

/**
 * A document containing one
 * BinDeleteResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class BinDeleteResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinDeleteResponseDocument {

	/**
	 * An XML BinDeleteResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class BinDeleteResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinDeleteResponseDocument.BinDeleteResponse {

		/**
		 * An XML
		 * BinDeleteResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class BinDeleteResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements BinDeleteResponseDocument.BinDeleteResponse.BinDeleteResult {

			private static final long serialVersionUID = 1L;

			public BinDeleteResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName BINDELETERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinDeleteResult");

		private static final long serialVersionUID = 1L;

		public BinDeleteResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "BinDeleteResult" element
		 */
		@Override
		public BinDeleteResponseDocument.BinDeleteResponse.BinDeleteResult addNewBinDeleteResult() {
			synchronized (monitor()) {
				check_orphaned();
				BinDeleteResponseDocument.BinDeleteResponse.BinDeleteResult target = null;
				target = (BinDeleteResponseDocument.BinDeleteResponse.BinDeleteResult) get_store().add_element_user(BINDELETERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "BinDeleteResult" element
		 */
		@Override
		public BinDeleteResponseDocument.BinDeleteResponse.BinDeleteResult getBinDeleteResult() {
			synchronized (monitor()) {
				check_orphaned();
				BinDeleteResponseDocument.BinDeleteResponse.BinDeleteResult target = null;
				target = (BinDeleteResponseDocument.BinDeleteResponse.BinDeleteResult) get_store().find_element_user(BINDELETERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "BinDeleteResult" element
		 */
		@Override
		public boolean isSetBinDeleteResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(BINDELETERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "BinDeleteResult" element
		 */
		@Override
		public void setBinDeleteResult(BinDeleteResponseDocument.BinDeleteResponse.BinDeleteResult binDeleteResult) {
			generatedSetterHelperImpl(binDeleteResult, BINDELETERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "BinDeleteResult" element
		 */
		@Override
		public void unsetBinDeleteResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(BINDELETERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName BINDELETERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "BinDeleteResponse");

	private static final long serialVersionUID = 1L;

	public BinDeleteResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "BinDeleteResponse" element
	 */
	@Override
	public BinDeleteResponseDocument.BinDeleteResponse addNewBinDeleteResponse() {
		synchronized (monitor()) {
			check_orphaned();
			BinDeleteResponseDocument.BinDeleteResponse target = null;
			target = (BinDeleteResponseDocument.BinDeleteResponse) get_store().add_element_user(BINDELETERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "BinDeleteResponse" element
	 */
	@Override
	public BinDeleteResponseDocument.BinDeleteResponse getBinDeleteResponse() {
		synchronized (monitor()) {
			check_orphaned();
			BinDeleteResponseDocument.BinDeleteResponse target = null;
			target = (BinDeleteResponseDocument.BinDeleteResponse) get_store().find_element_user(BINDELETERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "BinDeleteResponse" element
	 */
	@Override
	public void setBinDeleteResponse(BinDeleteResponseDocument.BinDeleteResponse binDeleteResponse) {
		generatedSetterHelperImpl(binDeleteResponse, BINDELETERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
