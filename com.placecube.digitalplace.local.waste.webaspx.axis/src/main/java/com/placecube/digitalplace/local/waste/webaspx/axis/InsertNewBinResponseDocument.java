/*
 * An XML document type.
 * Localname: insertNewBinResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: InsertNewBinResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one insertNewBinResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface InsertNewBinResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(InsertNewBinResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("insertnewbinresponse3d1ddoctype");
    
    /**
     * Gets the "insertNewBinResponse" element
     */
    InsertNewBinResponseDocument.InsertNewBinResponse getInsertNewBinResponse();
    
    /**
     * Sets the "insertNewBinResponse" element
     */
    void setInsertNewBinResponse(InsertNewBinResponseDocument.InsertNewBinResponse insertNewBinResponse);
    
    /**
     * Appends and returns a new empty "insertNewBinResponse" element
     */
    InsertNewBinResponseDocument.InsertNewBinResponse addNewInsertNewBinResponse();
    
    /**
     * An XML insertNewBinResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface InsertNewBinResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(InsertNewBinResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("insertnewbinresponse7ae0elemtype");
        
        /**
         * Gets the "insertNewBinResult" element
         */
        InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult getInsertNewBinResult();
        
        /**
         * True if has "insertNewBinResult" element
         */
        boolean isSetInsertNewBinResult();
        
        /**
         * Sets the "insertNewBinResult" element
         */
        void setInsertNewBinResult(InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult insertNewBinResult);
        
        /**
         * Appends and returns a new empty "insertNewBinResult" element
         */
        InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult addNewInsertNewBinResult();
        
        /**
         * Unsets the "insertNewBinResult" element
         */
        void unsetInsertNewBinResult();
        
        /**
         * An XML insertNewBinResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface InsertNewBinResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(InsertNewBinResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("insertnewbinresult4999elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult newInstance() {
                  return (InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (InsertNewBinResponseDocument.InsertNewBinResponse.InsertNewBinResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static InsertNewBinResponseDocument.InsertNewBinResponse newInstance() {
              return (InsertNewBinResponseDocument.InsertNewBinResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static InsertNewBinResponseDocument.InsertNewBinResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (InsertNewBinResponseDocument.InsertNewBinResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static InsertNewBinResponseDocument newInstance() {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static InsertNewBinResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static InsertNewBinResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static InsertNewBinResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static InsertNewBinResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static InsertNewBinResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static InsertNewBinResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static InsertNewBinResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static InsertNewBinResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static InsertNewBinResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static InsertNewBinResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static InsertNewBinResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static InsertNewBinResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static InsertNewBinResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static InsertNewBinResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static InsertNewBinResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static InsertNewBinResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static InsertNewBinResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (InsertNewBinResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
