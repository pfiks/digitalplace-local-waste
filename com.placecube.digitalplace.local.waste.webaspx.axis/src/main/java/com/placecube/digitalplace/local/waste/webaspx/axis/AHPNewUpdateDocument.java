/*
 * An XML document type.
 * Localname: AHP_NewUpdate
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPNewUpdateDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one AHP_NewUpdate(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface AHPNewUpdateDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPNewUpdateDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpnewupdate1e3ddoctype");
    
    /**
     * Gets the "AHP_NewUpdate" element
     */
    AHPNewUpdateDocument.AHPNewUpdate getAHPNewUpdate();
    
    /**
     * Sets the "AHP_NewUpdate" element
     */
    void setAHPNewUpdate(AHPNewUpdateDocument.AHPNewUpdate ahpNewUpdate);
    
    /**
     * Appends and returns a new empty "AHP_NewUpdate" element
     */
    AHPNewUpdateDocument.AHPNewUpdate addNewAHPNewUpdate();
    
    /**
     * An XML AHP_NewUpdate(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface AHPNewUpdate extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPNewUpdate.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpnewupdate471celemtype");
        
        /**
         * Gets the "council" element
         */
        java.lang.String getCouncil();
        
        /**
         * Gets (as xml) the "council" element
         */
        org.apache.xmlbeans.XmlString xgetCouncil();
        
        /**
         * True if has "council" element
         */
        boolean isSetCouncil();
        
        /**
         * Sets the "council" element
         */
        void setCouncil(java.lang.String council);
        
        /**
         * Sets (as xml) the "council" element
         */
        void xsetCouncil(org.apache.xmlbeans.XmlString council);
        
        /**
         * Unsets the "council" element
         */
        void unsetCouncil();
        
        /**
         * Gets the "webServicePassword" element
         */
        java.lang.String getWebServicePassword();
        
        /**
         * Gets (as xml) the "webServicePassword" element
         */
        org.apache.xmlbeans.XmlString xgetWebServicePassword();
        
        /**
         * True if has "webServicePassword" element
         */
        boolean isSetWebServicePassword();
        
        /**
         * Sets the "webServicePassword" element
         */
        void setWebServicePassword(java.lang.String webServicePassword);
        
        /**
         * Sets (as xml) the "webServicePassword" element
         */
        void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword);
        
        /**
         * Unsets the "webServicePassword" element
         */
        void unsetWebServicePassword();
        
        /**
         * Gets the "username" element
         */
        java.lang.String getUsername();
        
        /**
         * Gets (as xml) the "username" element
         */
        org.apache.xmlbeans.XmlString xgetUsername();
        
        /**
         * True if has "username" element
         */
        boolean isSetUsername();
        
        /**
         * Sets the "username" element
         */
        void setUsername(java.lang.String username);
        
        /**
         * Sets (as xml) the "username" element
         */
        void xsetUsername(org.apache.xmlbeans.XmlString username);
        
        /**
         * Unsets the "username" element
         */
        void unsetUsername();
        
        /**
         * Gets the "usernamePassword" element
         */
        java.lang.String getUsernamePassword();
        
        /**
         * Gets (as xml) the "usernamePassword" element
         */
        org.apache.xmlbeans.XmlString xgetUsernamePassword();
        
        /**
         * True if has "usernamePassword" element
         */
        boolean isSetUsernamePassword();
        
        /**
         * Sets the "usernamePassword" element
         */
        void setUsernamePassword(java.lang.String usernamePassword);
        
        /**
         * Sets (as xml) the "usernamePassword" element
         */
        void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword);
        
        /**
         * Unsets the "usernamePassword" element
         */
        void unsetUsernamePassword();
        
        /**
         * Gets the "UPRN" element
         */
        java.lang.String getUPRN();
        
        /**
         * Gets (as xml) the "UPRN" element
         */
        org.apache.xmlbeans.XmlString xgetUPRN();
        
        /**
         * True if has "UPRN" element
         */
        boolean isSetUPRN();
        
        /**
         * Sets the "UPRN" element
         */
        void setUPRN(java.lang.String uprn);
        
        /**
         * Sets (as xml) the "UPRN" element
         */
        void xsetUPRN(org.apache.xmlbeans.XmlString uprn);
        
        /**
         * Unsets the "UPRN" element
         */
        void unsetUPRN();
        
        /**
         * Gets the "custName" element
         */
        java.lang.String getCustName();
        
        /**
         * Gets (as xml) the "custName" element
         */
        org.apache.xmlbeans.XmlString xgetCustName();
        
        /**
         * True if has "custName" element
         */
        boolean isSetCustName();
        
        /**
         * Sets the "custName" element
         */
        void setCustName(java.lang.String custName);
        
        /**
         * Sets (as xml) the "custName" element
         */
        void xsetCustName(org.apache.xmlbeans.XmlString custName);
        
        /**
         * Unsets the "custName" element
         */
        void unsetCustName();
        
        /**
         * Gets the "custEmail" element
         */
        java.lang.String getCustEmail();
        
        /**
         * Gets (as xml) the "custEmail" element
         */
        org.apache.xmlbeans.XmlString xgetCustEmail();
        
        /**
         * True if has "custEmail" element
         */
        boolean isSetCustEmail();
        
        /**
         * Sets the "custEmail" element
         */
        void setCustEmail(java.lang.String custEmail);
        
        /**
         * Sets (as xml) the "custEmail" element
         */
        void xsetCustEmail(org.apache.xmlbeans.XmlString custEmail);
        
        /**
         * Unsets the "custEmail" element
         */
        void unsetCustEmail();
        
        /**
         * Gets the "custPhone" element
         */
        java.lang.String getCustPhone();
        
        /**
         * Gets (as xml) the "custPhone" element
         */
        org.apache.xmlbeans.XmlString xgetCustPhone();
        
        /**
         * True if has "custPhone" element
         */
        boolean isSetCustPhone();
        
        /**
         * Sets the "custPhone" element
         */
        void setCustPhone(java.lang.String custPhone);
        
        /**
         * Sets (as xml) the "custPhone" element
         */
        void xsetCustPhone(org.apache.xmlbeans.XmlString custPhone);
        
        /**
         * Unsets the "custPhone" element
         */
        void unsetCustPhone();
        
        /**
         * Gets the "BagOrCaddy" element
         */
        java.lang.String getBagOrCaddy();
        
        /**
         * Gets (as xml) the "BagOrCaddy" element
         */
        org.apache.xmlbeans.XmlString xgetBagOrCaddy();
        
        /**
         * True if has "BagOrCaddy" element
         */
        boolean isSetBagOrCaddy();
        
        /**
         * Sets the "BagOrCaddy" element
         */
        void setBagOrCaddy(java.lang.String bagOrCaddy);
        
        /**
         * Sets (as xml) the "BagOrCaddy" element
         */
        void xsetBagOrCaddy(org.apache.xmlbeans.XmlString bagOrCaddy);
        
        /**
         * Unsets the "BagOrCaddy" element
         */
        void unsetBagOrCaddy();
        
        /**
         * Gets the "relation" element
         */
        java.lang.String getRelation();
        
        /**
         * Gets (as xml) the "relation" element
         */
        org.apache.xmlbeans.XmlString xgetRelation();
        
        /**
         * True if has "relation" element
         */
        boolean isSetRelation();
        
        /**
         * Sets the "relation" element
         */
        void setRelation(java.lang.String relation);
        
        /**
         * Sets (as xml) the "relation" element
         */
        void xsetRelation(org.apache.xmlbeans.XmlString relation);
        
        /**
         * Unsets the "relation" element
         */
        void unsetRelation();
        
        /**
         * Gets the "crmAhpAdultRef" element
         */
        java.lang.String getCrmAhpAdultRef();
        
        /**
         * Gets (as xml) the "crmAhpAdultRef" element
         */
        org.apache.xmlbeans.XmlString xgetCrmAhpAdultRef();
        
        /**
         * True if has "crmAhpAdultRef" element
         */
        boolean isSetCrmAhpAdultRef();
        
        /**
         * Sets the "crmAhpAdultRef" element
         */
        void setCrmAhpAdultRef(java.lang.String crmAhpAdultRef);
        
        /**
         * Sets (as xml) the "crmAhpAdultRef" element
         */
        void xsetCrmAhpAdultRef(org.apache.xmlbeans.XmlString crmAhpAdultRef);
        
        /**
         * Unsets the "crmAhpAdultRef" element
         */
        void unsetCrmAhpAdultRef();
        
        /**
         * Gets the "ahpAdultCount" element
         */
        java.lang.String getAhpAdultCount();
        
        /**
         * Gets (as xml) the "ahpAdultCount" element
         */
        org.apache.xmlbeans.XmlString xgetAhpAdultCount();
        
        /**
         * True if has "ahpAdultCount" element
         */
        boolean isSetAhpAdultCount();
        
        /**
         * Sets the "ahpAdultCount" element
         */
        void setAhpAdultCount(java.lang.String ahpAdultCount);
        
        /**
         * Sets (as xml) the "ahpAdultCount" element
         */
        void xsetAhpAdultCount(org.apache.xmlbeans.XmlString ahpAdultCount);
        
        /**
         * Unsets the "ahpAdultCount" element
         */
        void unsetAhpAdultCount();
        
        /**
         * Gets the "ahpAdultEndDate" element
         */
        java.lang.String getAhpAdultEndDate();
        
        /**
         * Gets (as xml) the "ahpAdultEndDate" element
         */
        org.apache.xmlbeans.XmlString xgetAhpAdultEndDate();
        
        /**
         * True if has "ahpAdultEndDate" element
         */
        boolean isSetAhpAdultEndDate();
        
        /**
         * Sets the "ahpAdultEndDate" element
         */
        void setAhpAdultEndDate(java.lang.String ahpAdultEndDate);
        
        /**
         * Sets (as xml) the "ahpAdultEndDate" element
         */
        void xsetAhpAdultEndDate(org.apache.xmlbeans.XmlString ahpAdultEndDate);
        
        /**
         * Unsets the "ahpAdultEndDate" element
         */
        void unsetAhpAdultEndDate();
        
        /**
         * Gets the "crmAhpChildRef" element
         */
        java.lang.String getCrmAhpChildRef();
        
        /**
         * Gets (as xml) the "crmAhpChildRef" element
         */
        org.apache.xmlbeans.XmlString xgetCrmAhpChildRef();
        
        /**
         * True if has "crmAhpChildRef" element
         */
        boolean isSetCrmAhpChildRef();
        
        /**
         * Sets the "crmAhpChildRef" element
         */
        void setCrmAhpChildRef(java.lang.String crmAhpChildRef);
        
        /**
         * Sets (as xml) the "crmAhpChildRef" element
         */
        void xsetCrmAhpChildRef(org.apache.xmlbeans.XmlString crmAhpChildRef);
        
        /**
         * Unsets the "crmAhpChildRef" element
         */
        void unsetCrmAhpChildRef();
        
        /**
         * Gets the "ahpChildCount" element
         */
        java.lang.String getAhpChildCount();
        
        /**
         * Gets (as xml) the "ahpChildCount" element
         */
        org.apache.xmlbeans.XmlString xgetAhpChildCount();
        
        /**
         * True if has "ahpChildCount" element
         */
        boolean isSetAhpChildCount();
        
        /**
         * Sets the "ahpChildCount" element
         */
        void setAhpChildCount(java.lang.String ahpChildCount);
        
        /**
         * Sets (as xml) the "ahpChildCount" element
         */
        void xsetAhpChildCount(org.apache.xmlbeans.XmlString ahpChildCount);
        
        /**
         * Unsets the "ahpChildCount" element
         */
        void unsetAhpChildCount();
        
        /**
         * Gets the "ahpChildEndDate" element
         */
        java.lang.String getAhpChildEndDate();
        
        /**
         * Gets (as xml) the "ahpChildEndDate" element
         */
        org.apache.xmlbeans.XmlString xgetAhpChildEndDate();
        
        /**
         * True if has "ahpChildEndDate" element
         */
        boolean isSetAhpChildEndDate();
        
        /**
         * Sets the "ahpChildEndDate" element
         */
        void setAhpChildEndDate(java.lang.String ahpChildEndDate);
        
        /**
         * Sets (as xml) the "ahpChildEndDate" element
         */
        void xsetAhpChildEndDate(org.apache.xmlbeans.XmlString ahpChildEndDate);
        
        /**
         * Unsets the "ahpChildEndDate" element
         */
        void unsetAhpChildEndDate();
        
        /**
         * Gets the "crmEnquiryID" element
         */
        java.lang.String getCrmEnquiryID();
        
        /**
         * Gets (as xml) the "crmEnquiryID" element
         */
        org.apache.xmlbeans.XmlString xgetCrmEnquiryID();
        
        /**
         * True if has "crmEnquiryID" element
         */
        boolean isSetCrmEnquiryID();
        
        /**
         * Sets the "crmEnquiryID" element
         */
        void setCrmEnquiryID(java.lang.String crmEnquiryID);
        
        /**
         * Sets (as xml) the "crmEnquiryID" element
         */
        void xsetCrmEnquiryID(org.apache.xmlbeans.XmlString crmEnquiryID);
        
        /**
         * Unsets the "crmEnquiryID" element
         */
        void unsetCrmEnquiryID();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static AHPNewUpdateDocument.AHPNewUpdate newInstance() {
              return (AHPNewUpdateDocument.AHPNewUpdate) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static AHPNewUpdateDocument.AHPNewUpdate newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (AHPNewUpdateDocument.AHPNewUpdate) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static AHPNewUpdateDocument newInstance() {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static AHPNewUpdateDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static AHPNewUpdateDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static AHPNewUpdateDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static AHPNewUpdateDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static AHPNewUpdateDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static AHPNewUpdateDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static AHPNewUpdateDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static AHPNewUpdateDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static AHPNewUpdateDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static AHPNewUpdateDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static AHPNewUpdateDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static AHPNewUpdateDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static AHPNewUpdateDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static AHPNewUpdateDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static AHPNewUpdateDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AHPNewUpdateDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AHPNewUpdateDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AHPNewUpdateDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
