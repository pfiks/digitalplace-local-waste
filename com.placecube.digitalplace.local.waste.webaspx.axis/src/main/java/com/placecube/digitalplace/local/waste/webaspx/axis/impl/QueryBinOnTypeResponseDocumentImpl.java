/*
 * An XML document type.
 * Localname: QueryBinOnTypeResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: QueryBinOnTypeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.QueryBinOnTypeResponseDocument;

/**
 * A document containing one
 * QueryBinOnTypeResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class QueryBinOnTypeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements QueryBinOnTypeResponseDocument {

	/**
	 * An XML
	 * QueryBinOnTypeResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class QueryBinOnTypeResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse {

		/**
		 * An XML
		 * QueryBinOnTypeResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class QueryBinOnTypeResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse.QueryBinOnTypeResult {

			private static final long serialVersionUID = 1L;

			public QueryBinOnTypeResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName QUERYBINONTYPERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "QueryBinOnTypeResult");

		private static final long serialVersionUID = 1L;

		public QueryBinOnTypeResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "QueryBinOnTypeResult" element
		 */
		@Override
		public QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse.QueryBinOnTypeResult addNewQueryBinOnTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse.QueryBinOnTypeResult target = null;
				target = (QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse.QueryBinOnTypeResult) get_store().add_element_user(QUERYBINONTYPERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "QueryBinOnTypeResult" element
		 */
		@Override
		public QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse.QueryBinOnTypeResult getQueryBinOnTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse.QueryBinOnTypeResult target = null;
				target = (QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse.QueryBinOnTypeResult) get_store().find_element_user(QUERYBINONTYPERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "QueryBinOnTypeResult" element
		 */
		@Override
		public boolean isSetQueryBinOnTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(QUERYBINONTYPERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "QueryBinOnTypeResult" element
		 */
		@Override
		public void setQueryBinOnTypeResult(QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse.QueryBinOnTypeResult queryBinOnTypeResult) {
			generatedSetterHelperImpl(queryBinOnTypeResult, QUERYBINONTYPERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "QueryBinOnTypeResult" element
		 */
		@Override
		public void unsetQueryBinOnTypeResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(QUERYBINONTYPERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName QUERYBINONTYPERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "QueryBinOnTypeResponse");

	private static final long serialVersionUID = 1L;

	public QueryBinOnTypeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "QueryBinOnTypeResponse" element
	 */
	@Override
	public QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse addNewQueryBinOnTypeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse target = null;
			target = (QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse) get_store().add_element_user(QUERYBINONTYPERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "QueryBinOnTypeResponse" element
	 */
	@Override
	public QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse getQueryBinOnTypeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse target = null;
			target = (QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse) get_store().find_element_user(QUERYBINONTYPERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "QueryBinOnTypeResponse" element
	 */
	@Override
	public void setQueryBinOnTypeResponse(QueryBinOnTypeResponseDocument.QueryBinOnTypeResponse queryBinOnTypeResponse) {
		generatedSetterHelperImpl(queryBinOnTypeResponse, QUERYBINONTYPERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
