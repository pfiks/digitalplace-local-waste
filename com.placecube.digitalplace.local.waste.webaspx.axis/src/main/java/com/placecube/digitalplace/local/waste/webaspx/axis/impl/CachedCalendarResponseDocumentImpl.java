/*
 * An XML document type.
 * Localname: CachedCalendarResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CachedCalendarResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.CachedCalendarResponseDocument;

/**
 * A document containing one
 * CachedCalendarResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class CachedCalendarResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CachedCalendarResponseDocument {

	/**
	 * An XML
	 * CachedCalendarResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class CachedCalendarResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CachedCalendarResponseDocument.CachedCalendarResponse {

		/**
		 * An XML
		 * CachedCalendarResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class CachedCalendarResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements CachedCalendarResponseDocument.CachedCalendarResponse.CachedCalendarResult {

			private static final long serialVersionUID = 1L;

			public CachedCalendarResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName CACHEDCALENDARRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CachedCalendarResult");

		private static final long serialVersionUID = 1L;

		public CachedCalendarResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "CachedCalendarResult" element
		 */
		@Override
		public CachedCalendarResponseDocument.CachedCalendarResponse.CachedCalendarResult addNewCachedCalendarResult() {
			synchronized (monitor()) {
				check_orphaned();
				CachedCalendarResponseDocument.CachedCalendarResponse.CachedCalendarResult target = null;
				target = (CachedCalendarResponseDocument.CachedCalendarResponse.CachedCalendarResult) get_store().add_element_user(CACHEDCALENDARRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "CachedCalendarResult" element
		 */
		@Override
		public CachedCalendarResponseDocument.CachedCalendarResponse.CachedCalendarResult getCachedCalendarResult() {
			synchronized (monitor()) {
				check_orphaned();
				CachedCalendarResponseDocument.CachedCalendarResponse.CachedCalendarResult target = null;
				target = (CachedCalendarResponseDocument.CachedCalendarResponse.CachedCalendarResult) get_store().find_element_user(CACHEDCALENDARRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "CachedCalendarResult" element
		 */
		@Override
		public boolean isSetCachedCalendarResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(CACHEDCALENDARRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "CachedCalendarResult" element
		 */
		@Override
		public void setCachedCalendarResult(CachedCalendarResponseDocument.CachedCalendarResponse.CachedCalendarResult cachedCalendarResult) {
			generatedSetterHelperImpl(cachedCalendarResult, CACHEDCALENDARRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "CachedCalendarResult" element
		 */
		@Override
		public void unsetCachedCalendarResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(CACHEDCALENDARRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName CACHEDCALENDARRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "CachedCalendarResponse");

	private static final long serialVersionUID = 1L;

	public CachedCalendarResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "CachedCalendarResponse" element
	 */
	@Override
	public CachedCalendarResponseDocument.CachedCalendarResponse addNewCachedCalendarResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CachedCalendarResponseDocument.CachedCalendarResponse target = null;
			target = (CachedCalendarResponseDocument.CachedCalendarResponse) get_store().add_element_user(CACHEDCALENDARRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "CachedCalendarResponse" element
	 */
	@Override
	public CachedCalendarResponseDocument.CachedCalendarResponse getCachedCalendarResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CachedCalendarResponseDocument.CachedCalendarResponse target = null;
			target = (CachedCalendarResponseDocument.CachedCalendarResponse) get_store().find_element_user(CACHEDCALENDARRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "CachedCalendarResponse" element
	 */
	@Override
	public void setCachedCalendarResponse(CachedCalendarResponseDocument.CachedCalendarResponse cachedCalendarResponse) {
		generatedSetterHelperImpl(cachedCalendarResponse, CACHEDCALENDARRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
