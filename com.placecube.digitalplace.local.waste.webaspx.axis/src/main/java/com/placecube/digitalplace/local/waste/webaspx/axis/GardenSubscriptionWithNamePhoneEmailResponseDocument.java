/*
 * An XML document type.
 * Localname: GardenSubscription_WithNamePhoneEmailResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscriptionWithNamePhoneEmailResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GardenSubscription_WithNamePhoneEmailResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GardenSubscriptionWithNamePhoneEmailResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscriptionWithNamePhoneEmailResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscriptionwithnamephoneemailresponse71f7doctype");
    
    /**
     * Gets the "GardenSubscription_WithNamePhoneEmailResponse" element
     */
    GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse getGardenSubscriptionWithNamePhoneEmailResponse();
    
    /**
     * Sets the "GardenSubscription_WithNamePhoneEmailResponse" element
     */
    void setGardenSubscriptionWithNamePhoneEmailResponse(GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse gardenSubscriptionWithNamePhoneEmailResponse);
    
    /**
     * Appends and returns a new empty "GardenSubscription_WithNamePhoneEmailResponse" element
     */
    GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse addNewGardenSubscriptionWithNamePhoneEmailResponse();
    
    /**
     * An XML GardenSubscription_WithNamePhoneEmailResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GardenSubscriptionWithNamePhoneEmailResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscriptionWithNamePhoneEmailResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscriptionwithnamephoneemailresponse5290elemtype");
        
        /**
         * Gets the "GardenSubscription_WithNamePhoneEmailResult" element
         */
        GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult getGardenSubscriptionWithNamePhoneEmailResult();
        
        /**
         * True if has "GardenSubscription_WithNamePhoneEmailResult" element
         */
        boolean isSetGardenSubscriptionWithNamePhoneEmailResult();
        
        /**
         * Sets the "GardenSubscription_WithNamePhoneEmailResult" element
         */
        void setGardenSubscriptionWithNamePhoneEmailResult(GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult gardenSubscriptionWithNamePhoneEmailResult);
        
        /**
         * Appends and returns a new empty "GardenSubscription_WithNamePhoneEmailResult" element
         */
        GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult addNewGardenSubscriptionWithNamePhoneEmailResult();
        
        /**
         * Unsets the "GardenSubscription_WithNamePhoneEmailResult" element
         */
        void unsetGardenSubscriptionWithNamePhoneEmailResult();
        
        /**
         * An XML GardenSubscription_WithNamePhoneEmailResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GardenSubscriptionWithNamePhoneEmailResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscriptionWithNamePhoneEmailResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscriptionwithnamephoneemailresultf505elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult newInstance() {
                  return (GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse.GardenSubscriptionWithNamePhoneEmailResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse newInstance() {
              return (GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GardenSubscriptionWithNamePhoneEmailResponseDocument.GardenSubscriptionWithNamePhoneEmailResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument newInstance() {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscriptionWithNamePhoneEmailResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscriptionWithNamePhoneEmailResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
