/*
 * An XML document type.
 * Localname: GetAvailableContainersForClientOrPropertyResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAvailableContainersForClientOrPropertyResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersForClientOrPropertyResponseDocument;

/**
 * A document containing one
 * GetAvailableContainersForClientOrPropertyResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAvailableContainersForClientOrPropertyResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
		implements GetAvailableContainersForClientOrPropertyResponseDocument {

	/**
	 * An XML
	 * GetAvailableContainersForClientOrPropertyResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAvailableContainersForClientOrPropertyResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse {

		/**
		 * An XML
		 * GetAvailableContainersForClientOrPropertyResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetAvailableContainersForClientOrPropertyResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult {

			private static final long serialVersionUID = 1L;

			public GetAvailableContainersForClientOrPropertyResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"GetAvailableContainersForClientOrPropertyResult");

		private static final long serialVersionUID = 1L;

		public GetAvailableContainersForClientOrPropertyResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty
		 * "GetAvailableContainersForClientOrPropertyResult" element
		 */
		@Override
		public GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult addNewGetAvailableContainersForClientOrPropertyResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult target = null;
				target = (GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult) get_store()
						.add_element_user(GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetAvailableContainersForClientOrPropertyResult" element
		 */
		@Override
		public GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult getGetAvailableContainersForClientOrPropertyResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult target = null;
				target = (GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult) get_store()
						.find_element_user(GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetAvailableContainersForClientOrPropertyResult" element
		 */
		@Override
		public boolean isSetGetAvailableContainersForClientOrPropertyResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetAvailableContainersForClientOrPropertyResult" element
		 */
		@Override
		public void setGetAvailableContainersForClientOrPropertyResult(
				GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse.GetAvailableContainersForClientOrPropertyResult getAvailableContainersForClientOrPropertyResult) {
			generatedSetterHelperImpl(getAvailableContainersForClientOrPropertyResult, GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESULT$0, 0,
					org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetAvailableContainersForClientOrPropertyResult" element
		 */
		@Override
		public void unsetGetAvailableContainersForClientOrPropertyResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"GetAvailableContainersForClientOrPropertyResponse");

	private static final long serialVersionUID = 1L;

	public GetAvailableContainersForClientOrPropertyResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty
	 * "GetAvailableContainersForClientOrPropertyResponse" element
	 */
	@Override
	public GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse addNewGetAvailableContainersForClientOrPropertyResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse target = null;
			target = (GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse) get_store()
					.add_element_user(GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetAvailableContainersForClientOrPropertyResponse" element
	 */
	@Override
	public GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse getGetAvailableContainersForClientOrPropertyResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse target = null;
			target = (GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse) get_store()
					.find_element_user(GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetAvailableContainersForClientOrPropertyResponse" element
	 */
	@Override
	public void setGetAvailableContainersForClientOrPropertyResponse(
			GetAvailableContainersForClientOrPropertyResponseDocument.GetAvailableContainersForClientOrPropertyResponse getAvailableContainersForClientOrPropertyResponse) {
		generatedSetterHelperImpl(getAvailableContainersForClientOrPropertyResponse, GETAVAILABLECONTAINERSFORCLIENTORPROPERTYRESPONSE$0, 0,
				org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
