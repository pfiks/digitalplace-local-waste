/*
 * An XML document type.
 * Localname: readWriteMissedBin
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: ReadWriteMissedBinDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.ReadWriteMissedBinDocument;

/**
 * A document containing one
 * readWriteMissedBin(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class ReadWriteMissedBinDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ReadWriteMissedBinDocument {

	/**
	 * An XML
	 * readWriteMissedBin(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class ReadWriteMissedBinImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements ReadWriteMissedBinDocument.ReadWriteMissedBin {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName MISSEDDATEDDSMMSYY$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "missedDateDDsMMsYY");

		private static final javax.xml.namespace.QName MISSEDYN$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "missedYN");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName SERVICE3L$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "service3L");
		private static final javax.xml.namespace.QName UPRN$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRN");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public ReadWriteMissedBinImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "missedDateDDsMMsYY" element
		 */
		@Override
		public java.lang.String getMissedDateDDsMMsYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(MISSEDDATEDDSMMSYY$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "missedYN" element
		 */
		@Override
		public java.lang.String getMissedYN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(MISSEDYN$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "service3L" element
		 */
		@Override
		public java.lang.String getService3L() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SERVICE3L$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRN" element
		 */
		@Override
		public java.lang.String getUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "missedDateDDsMMsYY" element
		 */
		@Override
		public boolean isSetMissedDateDDsMMsYY() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(MISSEDDATEDDSMMSYY$14) != 0;
			}
		}

		/**
		 * True if has "missedYN" element
		 */
		@Override
		public boolean isSetMissedYN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(MISSEDYN$12) != 0;
			}
		}

		/**
		 * True if has "service3L" element
		 */
		@Override
		public boolean isSetService3L() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SERVICE3L$10) != 0;
			}
		}

		/**
		 * True if has "UPRN" element
		 */
		@Override
		public boolean isSetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRN$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "missedDateDDsMMsYY" element
		 */
		@Override
		public void setMissedDateDDsMMsYY(java.lang.String missedDateDDsMMsYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(MISSEDDATEDDSMMSYY$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(MISSEDDATEDDSMMSYY$14);
				}
				target.setStringValue(missedDateDDsMMsYY);
			}
		}

		/**
		 * Sets the "missedYN" element
		 */
		@Override
		public void setMissedYN(java.lang.String missedYN) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(MISSEDYN$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(MISSEDYN$12);
				}
				target.setStringValue(missedYN);
			}
		}

		/**
		 * Sets the "service3L" element
		 */
		@Override
		public void setService3L(java.lang.String service3L) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SERVICE3L$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(SERVICE3L$10);
				}
				target.setStringValue(service3L);
			}
		}

		/**
		 * Sets the "UPRN" element
		 */
		@Override
		public void setUPRN(java.lang.String uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRN$8);
				}
				target.setStringValue(uprn);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "missedDateDDsMMsYY" element
		 */
		@Override
		public void unsetMissedDateDDsMMsYY() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(MISSEDDATEDDSMMSYY$14, 0);
			}
		}

		/**
		 * Unsets the "missedYN" element
		 */
		@Override
		public void unsetMissedYN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(MISSEDYN$12, 0);
			}
		}

		/**
		 * Unsets the "service3L" element
		 */
		@Override
		public void unsetService3L() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SERVICE3L$10, 0);
			}
		}

		/**
		 * Unsets the "UPRN" element
		 */
		@Override
		public void unsetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRN$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "missedDateDDsMMsYY" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetMissedDateDDsMMsYY() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(MISSEDDATEDDSMMSYY$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "missedYN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetMissedYN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(MISSEDYN$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "service3L" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetService3L() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SERVICE3L$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "missedDateDDsMMsYY" element
		 */
		@Override
		public void xsetMissedDateDDsMMsYY(org.apache.xmlbeans.XmlString missedDateDDsMMsYY) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(MISSEDDATEDDSMMSYY$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(MISSEDDATEDDSMMSYY$14);
				}
				target.set(missedDateDDsMMsYY);
			}
		}

		/**
		 * Sets (as xml) the "missedYN" element
		 */
		@Override
		public void xsetMissedYN(org.apache.xmlbeans.XmlString missedYN) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(MISSEDYN$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(MISSEDYN$12);
				}
				target.set(missedYN);
			}
		}

		/**
		 * Sets (as xml) the "service3L" element
		 */
		@Override
		public void xsetService3L(org.apache.xmlbeans.XmlString service3L) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SERVICE3L$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(SERVICE3L$10);
				}
				target.set(service3L);
			}
		}

		/**
		 * Sets (as xml) the "UPRN" element
		 */
		@Override
		public void xsetUPRN(org.apache.xmlbeans.XmlString uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRN$8);
				}
				target.set(uprn);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName READWRITEMISSEDBIN$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "readWriteMissedBin");

	private static final long serialVersionUID = 1L;

	public ReadWriteMissedBinDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "readWriteMissedBin" element
	 */
	@Override
	public ReadWriteMissedBinDocument.ReadWriteMissedBin addNewReadWriteMissedBin() {
		synchronized (monitor()) {
			check_orphaned();
			ReadWriteMissedBinDocument.ReadWriteMissedBin target = null;
			target = (ReadWriteMissedBinDocument.ReadWriteMissedBin) get_store().add_element_user(READWRITEMISSEDBIN$0);
			return target;
		}
	}

	/**
	 * Gets the "readWriteMissedBin" element
	 */
	@Override
	public ReadWriteMissedBinDocument.ReadWriteMissedBin getReadWriteMissedBin() {
		synchronized (monitor()) {
			check_orphaned();
			ReadWriteMissedBinDocument.ReadWriteMissedBin target = null;
			target = (ReadWriteMissedBinDocument.ReadWriteMissedBin) get_store().find_element_user(READWRITEMISSEDBIN$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "readWriteMissedBin" element
	 */
	@Override
	public void setReadWriteMissedBin(ReadWriteMissedBinDocument.ReadWriteMissedBin readWriteMissedBin) {
		generatedSetterHelperImpl(readWriteMissedBin, READWRITEMISSEDBIN$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
