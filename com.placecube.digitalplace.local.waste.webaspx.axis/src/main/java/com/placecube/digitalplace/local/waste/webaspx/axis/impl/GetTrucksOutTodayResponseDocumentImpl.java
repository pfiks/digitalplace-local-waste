/*
 * An XML document type.
 * Localname: GetTrucksOutTodayResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetTrucksOutTodayResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetTrucksOutTodayResponseDocument;

/**
 * A document containing one
 * GetTrucksOutTodayResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetTrucksOutTodayResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetTrucksOutTodayResponseDocument {

	/**
	 * An XML
	 * GetTrucksOutTodayResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetTrucksOutTodayResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse {

		/**
		 * An XML
		 * GetTrucksOutTodayResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetTrucksOutTodayResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult {

			private static final long serialVersionUID = 1L;

			public GetTrucksOutTodayResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETTRUCKSOUTTODAYRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetTrucksOutTodayResult");

		private static final long serialVersionUID = 1L;

		public GetTrucksOutTodayResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetTrucksOutTodayResult" element
		 */
		@Override
		public GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult addNewGetTrucksOutTodayResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult target = null;
				target = (GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult) get_store().add_element_user(GETTRUCKSOUTTODAYRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetTrucksOutTodayResult" element
		 */
		@Override
		public GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult getGetTrucksOutTodayResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult target = null;
				target = (GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult) get_store().find_element_user(GETTRUCKSOUTTODAYRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetTrucksOutTodayResult" element
		 */
		@Override
		public boolean isSetGetTrucksOutTodayResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETTRUCKSOUTTODAYRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetTrucksOutTodayResult" element
		 */
		@Override
		public void setGetTrucksOutTodayResult(GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse.GetTrucksOutTodayResult getTrucksOutTodayResult) {
			generatedSetterHelperImpl(getTrucksOutTodayResult, GETTRUCKSOUTTODAYRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetTrucksOutTodayResult" element
		 */
		@Override
		public void unsetGetTrucksOutTodayResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETTRUCKSOUTTODAYRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETTRUCKSOUTTODAYRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetTrucksOutTodayResponse");

	private static final long serialVersionUID = 1L;

	public GetTrucksOutTodayResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetTrucksOutTodayResponse" element
	 */
	@Override
	public GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse addNewGetTrucksOutTodayResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse target = null;
			target = (GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse) get_store().add_element_user(GETTRUCKSOUTTODAYRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetTrucksOutTodayResponse" element
	 */
	@Override
	public GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse getGetTrucksOutTodayResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse target = null;
			target = (GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse) get_store().find_element_user(GETTRUCKSOUTTODAYRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetTrucksOutTodayResponse" element
	 */
	@Override
	public void setGetTrucksOutTodayResponse(GetTrucksOutTodayResponseDocument.GetTrucksOutTodayResponse getTrucksOutTodayResponse) {
		generatedSetterHelperImpl(getTrucksOutTodayResponse, GETTRUCKSOUTTODAYRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
