/*
 * An XML document type.
 * Localname: getNoCollectionDatesResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetNoCollectionDatesResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one getNoCollectionDatesResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GetNoCollectionDatesResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetNoCollectionDatesResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getnocollectiondatesresponsefa8ddoctype");
    
    /**
     * Gets the "getNoCollectionDatesResponse" element
     */
    GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse getGetNoCollectionDatesResponse();
    
    /**
     * Sets the "getNoCollectionDatesResponse" element
     */
    void setGetNoCollectionDatesResponse(GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse getNoCollectionDatesResponse);
    
    /**
     * Appends and returns a new empty "getNoCollectionDatesResponse" element
     */
    GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse addNewGetNoCollectionDatesResponse();
    
    /**
     * An XML getNoCollectionDatesResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GetNoCollectionDatesResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetNoCollectionDatesResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getnocollectiondatesresponse08e0elemtype");
        
        /**
         * Gets the "getNoCollectionDatesResult" element
         */
        GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult getGetNoCollectionDatesResult();
        
        /**
         * True if has "getNoCollectionDatesResult" element
         */
        boolean isSetGetNoCollectionDatesResult();
        
        /**
         * Sets the "getNoCollectionDatesResult" element
         */
        void setGetNoCollectionDatesResult(GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult getNoCollectionDatesResult);
        
        /**
         * Appends and returns a new empty "getNoCollectionDatesResult" element
         */
        GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult addNewGetNoCollectionDatesResult();
        
        /**
         * Unsets the "getNoCollectionDatesResult" element
         */
        void unsetGetNoCollectionDatesResult();
        
        /**
         * An XML getNoCollectionDatesResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GetNoCollectionDatesResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GetNoCollectionDatesResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("getnocollectiondatesresult6c09elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult newInstance() {
                  return (GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse.GetNoCollectionDatesResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse newInstance() {
              return (GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GetNoCollectionDatesResponseDocument.GetNoCollectionDatesResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GetNoCollectionDatesResponseDocument newInstance() {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GetNoCollectionDatesResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GetNoCollectionDatesResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GetNoCollectionDatesResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GetNoCollectionDatesResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GetNoCollectionDatesResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GetNoCollectionDatesResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GetNoCollectionDatesResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GetNoCollectionDatesResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GetNoCollectionDatesResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GetNoCollectionDatesResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GetNoCollectionDatesResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GetNoCollectionDatesResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GetNoCollectionDatesResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GetNoCollectionDatesResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GetNoCollectionDatesResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetNoCollectionDatesResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GetNoCollectionDatesResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GetNoCollectionDatesResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
