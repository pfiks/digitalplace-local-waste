/*
 * An XML document type.
 * Localname: string
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: StringDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.StringDocument;

/**
 * A document containing one
 * string(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public class StringDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements StringDocument {

	private static final long serialVersionUID = 1L;

	private static final javax.xml.namespace.QName STRING$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "string");

	public StringDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Gets the "string" element
	 */
	@Override
	public java.lang.String getString() {
		synchronized (monitor()) {
			check_orphaned();
			org.apache.xmlbeans.SimpleValue target = null;
			target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STRING$0, 0);
			if (target == null) {
				return null;
			}
			return target.getStringValue();
		}
	}

	/**
	 * Tests for nil "string" element
	 */
	@Override
	public boolean isNilString() {
		synchronized (monitor()) {
			check_orphaned();
			org.apache.xmlbeans.XmlString target = null;
			target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STRING$0, 0);
			if (target == null)
				return false;
			return target.isNil();
		}
	}

	/**
	 * Nils the "string" element
	 */
	@Override
	public void setNilString() {
		synchronized (monitor()) {
			check_orphaned();
			org.apache.xmlbeans.XmlString target = null;
			target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STRING$0, 0);
			if (target == null) {
				target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(STRING$0);
			}
			target.setNil();
		}
	}

	/**
	 * Sets the "string" element
	 */
	@Override
	public void setString(java.lang.String string) {
		synchronized (monitor()) {
			check_orphaned();
			org.apache.xmlbeans.SimpleValue target = null;
			target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STRING$0, 0);
			if (target == null) {
				target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(STRING$0);
			}
			target.setStringValue(string);
		}
	}

	/**
	 * Gets (as xml) the "string" element
	 */
	@Override
	public org.apache.xmlbeans.XmlString xgetString() {
		synchronized (monitor()) {
			check_orphaned();
			org.apache.xmlbeans.XmlString target = null;
			target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STRING$0, 0);
			return target;
		}
	}

	/**
	 * Sets (as xml) the "string" element
	 */
	@Override
	public void xsetString(org.apache.xmlbeans.XmlString string) {
		synchronized (monitor()) {
			check_orphaned();
			org.apache.xmlbeans.XmlString target = null;
			target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STRING$0, 0);
			if (target == null) {
				target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(STRING$0);
			}
			target.set(string);
		}
	}
}
