/*
 * An XML document type.
 * Localname: getIssuesAndCollectionStatusForUPRN
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetIssuesAndCollectionStatusForUPRNDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetIssuesAndCollectionStatusForUPRNDocument;

/**
 * A document containing one
 * getIssuesAndCollectionStatusForUPRN(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetIssuesAndCollectionStatusForUPRNDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetIssuesAndCollectionStatusForUPRNDocument {

	/**
	 * An XML
	 * getIssuesAndCollectionStatusForUPRN(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetIssuesAndCollectionStatusForUPRNImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN {

		private static final javax.xml.namespace.QName COUNCIL$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "council");

		private static final javax.xml.namespace.QName ENDDATEYYYYMMDD$14 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "endDateyyyyMMdd");

		private static final javax.xml.namespace.QName LASTTIMEONLY$10 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "lastTimeOnly");
		private static final long serialVersionUID = 1L;
		private static final javax.xml.namespace.QName SERVICEFILTER$16 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "serviceFilter");
		private static final javax.xml.namespace.QName STARTDATEYYYYMMDD$12 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "startDateyyyyMMdd");
		private static final javax.xml.namespace.QName UPRN$8 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "UPRN");
		private static final javax.xml.namespace.QName USERNAME$4 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "username");
		private static final javax.xml.namespace.QName USERNAMEPASSWORD$6 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "usernamePassword");
		private static final javax.xml.namespace.QName WEBSERVICEPASSWORD$2 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "webServicePassword");

		public GetIssuesAndCollectionStatusForUPRNImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "council" element
		 */
		@Override
		public java.lang.String getCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "endDateyyyyMMdd" element
		 */
		@Override
		public java.lang.String getEndDateyyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ENDDATEYYYYMMDD$14, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "lastTimeOnly" element
		 */
		@Override
		public java.lang.String getLastTimeOnly() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(LASTTIMEONLY$10, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "serviceFilter" element
		 */
		@Override
		public java.lang.String getServiceFilter() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SERVICEFILTER$16, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "startDateyyyyMMdd" element
		 */
		@Override
		public java.lang.String getStartDateyyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STARTDATEYYYYMMDD$12, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "UPRN" element
		 */
		@Override
		public java.lang.String getUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "username" element
		 */
		@Override
		public java.lang.String getUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "usernamePassword" element
		 */
		@Override
		public java.lang.String getUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * Gets the "webServicePassword" element
		 */
		@Override
		public java.lang.String getWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "council" element
		 */
		@Override
		public boolean isSetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNCIL$0) != 0;
			}
		}

		/**
		 * True if has "endDateyyyyMMdd" element
		 */
		@Override
		public boolean isSetEndDateyyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(ENDDATEYYYYMMDD$14) != 0;
			}
		}

		/**
		 * True if has "lastTimeOnly" element
		 */
		@Override
		public boolean isSetLastTimeOnly() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(LASTTIMEONLY$10) != 0;
			}
		}

		/**
		 * True if has "serviceFilter" element
		 */
		@Override
		public boolean isSetServiceFilter() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(SERVICEFILTER$16) != 0;
			}
		}

		/**
		 * True if has "startDateyyyyMMdd" element
		 */
		@Override
		public boolean isSetStartDateyyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(STARTDATEYYYYMMDD$12) != 0;
			}
		}

		/**
		 * True if has "UPRN" element
		 */
		@Override
		public boolean isSetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(UPRN$8) != 0;
			}
		}

		/**
		 * True if has "username" element
		 */
		@Override
		public boolean isSetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAME$4) != 0;
			}
		}

		/**
		 * True if has "usernamePassword" element
		 */
		@Override
		public boolean isSetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(USERNAMEPASSWORD$6) != 0;
			}
		}

		/**
		 * True if has "webServicePassword" element
		 */
		@Override
		public boolean isSetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(WEBSERVICEPASSWORD$2) != 0;
			}
		}

		/**
		 * Sets the "council" element
		 */
		@Override
		public void setCouncil(java.lang.String council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(COUNCIL$0);
				}
				target.setStringValue(council);
			}
		}

		/**
		 * Sets the "endDateyyyyMMdd" element
		 */
		@Override
		public void setEndDateyyyyMMdd(java.lang.String endDateyyyyMMdd) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(ENDDATEYYYYMMDD$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(ENDDATEYYYYMMDD$14);
				}
				target.setStringValue(endDateyyyyMMdd);
			}
		}

		/**
		 * Sets the "lastTimeOnly" element
		 */
		@Override
		public void setLastTimeOnly(java.lang.String lastTimeOnly) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(LASTTIMEONLY$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(LASTTIMEONLY$10);
				}
				target.setStringValue(lastTimeOnly);
			}
		}

		/**
		 * Sets the "serviceFilter" element
		 */
		@Override
		public void setServiceFilter(java.lang.String serviceFilter) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(SERVICEFILTER$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(SERVICEFILTER$16);
				}
				target.setStringValue(serviceFilter);
			}
		}

		/**
		 * Sets the "startDateyyyyMMdd" element
		 */
		@Override
		public void setStartDateyyyyMMdd(java.lang.String startDateyyyyMMdd) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(STARTDATEYYYYMMDD$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(STARTDATEYYYYMMDD$12);
				}
				target.setStringValue(startDateyyyyMMdd);
			}
		}

		/**
		 * Sets the "UPRN" element
		 */
		@Override
		public void setUPRN(java.lang.String uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(UPRN$8);
				}
				target.setStringValue(uprn);
			}
		}

		/**
		 * Sets the "username" element
		 */
		@Override
		public void setUsername(java.lang.String username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAME$4);
				}
				target.setStringValue(username);
			}
		}

		/**
		 * Sets the "usernamePassword" element
		 */
		@Override
		public void setUsernamePassword(java.lang.String usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.setStringValue(usernamePassword);
			}
		}

		/**
		 * Sets the "webServicePassword" element
		 */
		@Override
		public void setWebServicePassword(java.lang.String webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.setStringValue(webServicePassword);
			}
		}

		/**
		 * Unsets the "council" element
		 */
		@Override
		public void unsetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNCIL$0, 0);
			}
		}

		/**
		 * Unsets the "endDateyyyyMMdd" element
		 */
		@Override
		public void unsetEndDateyyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(ENDDATEYYYYMMDD$14, 0);
			}
		}

		/**
		 * Unsets the "lastTimeOnly" element
		 */
		@Override
		public void unsetLastTimeOnly() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(LASTTIMEONLY$10, 0);
			}
		}

		/**
		 * Unsets the "serviceFilter" element
		 */
		@Override
		public void unsetServiceFilter() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(SERVICEFILTER$16, 0);
			}
		}

		/**
		 * Unsets the "startDateyyyyMMdd" element
		 */
		@Override
		public void unsetStartDateyyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(STARTDATEYYYYMMDD$12, 0);
			}
		}

		/**
		 * Unsets the "UPRN" element
		 */
		@Override
		public void unsetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(UPRN$8, 0);
			}
		}

		/**
		 * Unsets the "username" element
		 */
		@Override
		public void unsetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAME$4, 0);
			}
		}

		/**
		 * Unsets the "usernamePassword" element
		 */
		@Override
		public void unsetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(USERNAMEPASSWORD$6, 0);
			}
		}

		/**
		 * Unsets the "webServicePassword" element
		 */
		@Override
		public void unsetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(WEBSERVICEPASSWORD$2, 0);
			}
		}

		/**
		 * Gets (as xml) the "council" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetCouncil() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "endDateyyyyMMdd" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetEndDateyyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ENDDATEYYYYMMDD$14, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "lastTimeOnly" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetLastTimeOnly() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(LASTTIMEONLY$10, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "serviceFilter" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetServiceFilter() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SERVICEFILTER$16, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "startDateyyyyMMdd" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetStartDateyyyyMMdd() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STARTDATEYYYYMMDD$12, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "UPRN" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUPRN() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "username" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsername() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "usernamePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetUsernamePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				return target;
			}
		}

		/**
		 * Gets (as xml) the "webServicePassword" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetWebServicePassword() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "council" element
		 */
		@Override
		public void xsetCouncil(org.apache.xmlbeans.XmlString council) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(COUNCIL$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(COUNCIL$0);
				}
				target.set(council);
			}
		}

		/**
		 * Sets (as xml) the "endDateyyyyMMdd" element
		 */
		@Override
		public void xsetEndDateyyyyMMdd(org.apache.xmlbeans.XmlString endDateyyyyMMdd) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(ENDDATEYYYYMMDD$14, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(ENDDATEYYYYMMDD$14);
				}
				target.set(endDateyyyyMMdd);
			}
		}

		/**
		 * Sets (as xml) the "lastTimeOnly" element
		 */
		@Override
		public void xsetLastTimeOnly(org.apache.xmlbeans.XmlString lastTimeOnly) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(LASTTIMEONLY$10, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(LASTTIMEONLY$10);
				}
				target.set(lastTimeOnly);
			}
		}

		/**
		 * Sets (as xml) the "serviceFilter" element
		 */
		@Override
		public void xsetServiceFilter(org.apache.xmlbeans.XmlString serviceFilter) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(SERVICEFILTER$16, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(SERVICEFILTER$16);
				}
				target.set(serviceFilter);
			}
		}

		/**
		 * Sets (as xml) the "startDateyyyyMMdd" element
		 */
		@Override
		public void xsetStartDateyyyyMMdd(org.apache.xmlbeans.XmlString startDateyyyyMMdd) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(STARTDATEYYYYMMDD$12, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(STARTDATEYYYYMMDD$12);
				}
				target.set(startDateyyyyMMdd);
			}
		}

		/**
		 * Sets (as xml) the "UPRN" element
		 */
		@Override
		public void xsetUPRN(org.apache.xmlbeans.XmlString uprn) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(UPRN$8, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(UPRN$8);
				}
				target.set(uprn);
			}
		}

		/**
		 * Sets (as xml) the "username" element
		 */
		@Override
		public void xsetUsername(org.apache.xmlbeans.XmlString username) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAME$4, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAME$4);
				}
				target.set(username);
			}
		}

		/**
		 * Sets (as xml) the "usernamePassword" element
		 */
		@Override
		public void xsetUsernamePassword(org.apache.xmlbeans.XmlString usernamePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(USERNAMEPASSWORD$6, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(USERNAMEPASSWORD$6);
				}
				target.set(usernamePassword);
			}
		}

		/**
		 * Sets (as xml) the "webServicePassword" element
		 */
		@Override
		public void xsetWebServicePassword(org.apache.xmlbeans.XmlString webServicePassword) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(WEBSERVICEPASSWORD$2, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(WEBSERVICEPASSWORD$2);
				}
				target.set(webServicePassword);
			}
		}
	}

	private static final javax.xml.namespace.QName GETISSUESANDCOLLECTIONSTATUSFORUPRN$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"getIssuesAndCollectionStatusForUPRN");

	private static final long serialVersionUID = 1L;

	public GetIssuesAndCollectionStatusForUPRNDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getIssuesAndCollectionStatusForUPRN"
	 * element
	 */
	@Override
	public GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN addNewGetIssuesAndCollectionStatusForUPRN() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN target = null;
			target = (GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN) get_store().add_element_user(GETISSUESANDCOLLECTIONSTATUSFORUPRN$0);
			return target;
		}
	}

	/**
	 * Gets the "getIssuesAndCollectionStatusForUPRN" element
	 */
	@Override
	public GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN getGetIssuesAndCollectionStatusForUPRN() {
		synchronized (monitor()) {
			check_orphaned();
			GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN target = null;
			target = (GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN) get_store().find_element_user(GETISSUESANDCOLLECTIONSTATUSFORUPRN$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getIssuesAndCollectionStatusForUPRN" element
	 */
	@Override
	public void setGetIssuesAndCollectionStatusForUPRN(GetIssuesAndCollectionStatusForUPRNDocument.GetIssuesAndCollectionStatusForUPRN getIssuesAndCollectionStatusForUPRN) {
		generatedSetterHelperImpl(getIssuesAndCollectionStatusForUPRN, GETISSUESANDCOLLECTIONSTATUSFORUPRN$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
