/*
 * An XML document type.
 * Localname: AA_HelloWorld_String_Test
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AAHelloWorldStringTestDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;

/**
 * A document containing one
 * AA_HelloWorld_String_Test(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public interface AAHelloWorldStringTestDocument extends org.apache.xmlbeans.XmlObject {

	/**
	 * An XML
	 * AA_HelloWorld_String_Test(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public interface AAHelloWorldStringTest extends org.apache.xmlbeans.XmlObject {

		/**
		 * A factory class with static methods for creating instances of this
		 * type.
		 */

		public static final class Factory {

			private Factory() {
			} // No instance of this class allowed

			public static AAHelloWorldStringTestDocument.AAHelloWorldStringTest newInstance() {
				return (AAHelloWorldStringTestDocument.AAHelloWorldStringTest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance(type, null);
			}

			public static AAHelloWorldStringTestDocument.AAHelloWorldStringTest newInstance(org.apache.xmlbeans.XmlOptions options) {
				return (AAHelloWorldStringTestDocument.AAHelloWorldStringTest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance(type, options);
			}
		}

		org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType) org.apache.xmlbeans.XmlBeans
				.typeSystemForClassLoader(AAHelloWorldStringTest.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396")
				.resolveHandle("aahelloworldstringtestc036elemtype");

		/**
		 * Gets the "testName" element
		 */
		java.lang.String getTestName();

		/**
		 * True if has "testName" element
		 */
		boolean isSetTestName();

		/**
		 * Sets the "testName" element
		 */
		void setTestName(java.lang.String testName);

		/**
		 * Unsets the "testName" element
		 */
		void unsetTestName();

		/**
		 * Gets (as xml) the "testName" element
		 */
		org.apache.xmlbeans.XmlString xgetTestName();

		/**
		 * Sets (as xml) the "testName" element
		 */
		void xsetTestName(org.apache.xmlbeans.XmlString testName);
	}

	/**
	 * A factory class with static methods for creating instances of this type.
	 */

	public static final class Factory {

		private Factory() {
		} // No instance of this class allowed

		public static AAHelloWorldStringTestDocument newInstance() {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance(type, null);
		}

		public static AAHelloWorldStringTestDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance(type, options);
		}

		/** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
		@Deprecated
		public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis)
				throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
			return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xis, type, null);
		}

		/** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
		@Deprecated
		public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
			return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream(xis, type, options);
		}

		/** @param file the file from which to load an xml document */
		public static AAHelloWorldStringTestDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(file, type, null);
		}

		public static AAHelloWorldStringTestDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(file, type, options);
		}

		public static AAHelloWorldStringTestDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(is, type, null);
		}

		public static AAHelloWorldStringTestDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(is, type, options);
		}

		public static AAHelloWorldStringTestDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(r, type, null);
		}

		public static AAHelloWorldStringTestDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(r, type, options);
		}

		/** @param xmlAsString the string value to parse */
		public static AAHelloWorldStringTestDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(xmlAsString, type, null);
		}

		public static AAHelloWorldStringTestDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(xmlAsString, type, options);
		}

		public static AAHelloWorldStringTestDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(u, type, null);
		}

		public static AAHelloWorldStringTestDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(u, type, options);
		}

		public static AAHelloWorldStringTestDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(sr, type, null);
		}

		public static AAHelloWorldStringTestDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(sr, type, options);
		}

		/** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
		@Deprecated
		public static AAHelloWorldStringTestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis)
				throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(xis, type, null);
		}

		/** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
		@Deprecated
		public static AAHelloWorldStringTestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options)
				throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(xis, type, options);
		}

		public static AAHelloWorldStringTestDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(node, type, null);
		}

		public static AAHelloWorldStringTestDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
			return (AAHelloWorldStringTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse(node, type, options);
		}
	}

	org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType) org.apache.xmlbeans.XmlBeans
			.typeSystemForClassLoader(AAHelloWorldStringTestDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396")
			.resolveHandle("aahelloworldstringtest258adoctype");

	/**
	 * Appends and returns a new empty "AA_HelloWorld_String_Test" element
	 */
	AAHelloWorldStringTestDocument.AAHelloWorldStringTest addNewAAHelloWorldStringTest();

	/**
	 * Gets the "AA_HelloWorld_String_Test" element
	 */
	AAHelloWorldStringTestDocument.AAHelloWorldStringTest getAAHelloWorldStringTest();

	/**
	 * Sets the "AA_HelloWorld_String_Test" element
	 */
	void setAAHelloWorldStringTest(AAHelloWorldStringTestDocument.AAHelloWorldStringTest aaHelloWorldStringTest);
}
