/*
 * An XML document type.
 * Localname: GetLastTruckPositionResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetLastTruckPositionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetLastTruckPositionResponseDocument;

/**
 * A document containing one
 * GetLastTruckPositionResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetLastTruckPositionResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetLastTruckPositionResponseDocument {

	/**
	 * An XML
	 * GetLastTruckPositionResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetLastTruckPositionResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse {

		/**
		 * An XML
		 * GetLastTruckPositionResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetLastTruckPositionResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse.GetLastTruckPositionResult {

			private static final long serialVersionUID = 1L;

			public GetLastTruckPositionResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETLASTTRUCKPOSITIONRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetLastTruckPositionResult");

		private static final long serialVersionUID = 1L;

		public GetLastTruckPositionResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetLastTruckPositionResult" element
		 */
		@Override
		public GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse.GetLastTruckPositionResult addNewGetLastTruckPositionResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse.GetLastTruckPositionResult target = null;
				target = (GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse.GetLastTruckPositionResult) get_store().add_element_user(GETLASTTRUCKPOSITIONRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetLastTruckPositionResult" element
		 */
		@Override
		public GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse.GetLastTruckPositionResult getGetLastTruckPositionResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse.GetLastTruckPositionResult target = null;
				target = (GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse.GetLastTruckPositionResult) get_store().find_element_user(GETLASTTRUCKPOSITIONRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetLastTruckPositionResult" element
		 */
		@Override
		public boolean isSetGetLastTruckPositionResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETLASTTRUCKPOSITIONRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetLastTruckPositionResult" element
		 */
		@Override
		public void setGetLastTruckPositionResult(GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse.GetLastTruckPositionResult getLastTruckPositionResult) {
			generatedSetterHelperImpl(getLastTruckPositionResult, GETLASTTRUCKPOSITIONRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetLastTruckPositionResult" element
		 */
		@Override
		public void unsetGetLastTruckPositionResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETLASTTRUCKPOSITIONRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETLASTTRUCKPOSITIONRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetLastTruckPositionResponse");

	private static final long serialVersionUID = 1L;

	public GetLastTruckPositionResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetLastTruckPositionResponse" element
	 */
	@Override
	public GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse addNewGetLastTruckPositionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse target = null;
			target = (GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse) get_store().add_element_user(GETLASTTRUCKPOSITIONRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetLastTruckPositionResponse" element
	 */
	@Override
	public GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse getGetLastTruckPositionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse target = null;
			target = (GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse) get_store().find_element_user(GETLASTTRUCKPOSITIONRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetLastTruckPositionResponse" element
	 */
	@Override
	public void setGetLastTruckPositionResponse(GetLastTruckPositionResponseDocument.GetLastTruckPositionResponse getLastTruckPositionResponse) {
		generatedSetterHelperImpl(getLastTruckPositionResponse, GETLASTTRUCKPOSITIONRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
