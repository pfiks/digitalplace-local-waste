/*
 * An XML document type.
 * Localname: GetAvailableContainersResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAvailableContainersResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAvailableContainersResponseDocument;

/**
 * A document containing one
 * GetAvailableContainersResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAvailableContainersResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAvailableContainersResponseDocument {

	/**
	 * An XML
	 * GetAvailableContainersResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAvailableContainersResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetAvailableContainersResponseDocument.GetAvailableContainersResponse {

		/**
		 * An XML
		 * GetAvailableContainersResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetAvailableContainersResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetAvailableContainersResponseDocument.GetAvailableContainersResponse.GetAvailableContainersResult {

			private static final long serialVersionUID = 1L;

			public GetAvailableContainersResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETAVAILABLECONTAINERSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableContainersResult");

		private static final long serialVersionUID = 1L;

		public GetAvailableContainersResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetAvailableContainersResult"
		 * element
		 */
		@Override
		public GetAvailableContainersResponseDocument.GetAvailableContainersResponse.GetAvailableContainersResult addNewGetAvailableContainersResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAvailableContainersResponseDocument.GetAvailableContainersResponse.GetAvailableContainersResult target = null;
				target = (GetAvailableContainersResponseDocument.GetAvailableContainersResponse.GetAvailableContainersResult) get_store().add_element_user(GETAVAILABLECONTAINERSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetAvailableContainersResult" element
		 */
		@Override
		public GetAvailableContainersResponseDocument.GetAvailableContainersResponse.GetAvailableContainersResult getGetAvailableContainersResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAvailableContainersResponseDocument.GetAvailableContainersResponse.GetAvailableContainersResult target = null;
				target = (GetAvailableContainersResponseDocument.GetAvailableContainersResponse.GetAvailableContainersResult) get_store().find_element_user(GETAVAILABLECONTAINERSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetAvailableContainersResult" element
		 */
		@Override
		public boolean isSetGetAvailableContainersResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETAVAILABLECONTAINERSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetAvailableContainersResult" element
		 */
		@Override
		public void setGetAvailableContainersResult(GetAvailableContainersResponseDocument.GetAvailableContainersResponse.GetAvailableContainersResult getAvailableContainersResult) {
			generatedSetterHelperImpl(getAvailableContainersResult, GETAVAILABLECONTAINERSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetAvailableContainersResult" element
		 */
		@Override
		public void unsetGetAvailableContainersResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETAVAILABLECONTAINERSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETAVAILABLECONTAINERSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAvailableContainersResponse");

	private static final long serialVersionUID = 1L;

	public GetAvailableContainersResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetAvailableContainersResponse" element
	 */
	@Override
	public GetAvailableContainersResponseDocument.GetAvailableContainersResponse addNewGetAvailableContainersResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAvailableContainersResponseDocument.GetAvailableContainersResponse target = null;
			target = (GetAvailableContainersResponseDocument.GetAvailableContainersResponse) get_store().add_element_user(GETAVAILABLECONTAINERSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetAvailableContainersResponse" element
	 */
	@Override
	public GetAvailableContainersResponseDocument.GetAvailableContainersResponse getGetAvailableContainersResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAvailableContainersResponseDocument.GetAvailableContainersResponse target = null;
			target = (GetAvailableContainersResponseDocument.GetAvailableContainersResponse) get_store().find_element_user(GETAVAILABLECONTAINERSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetAvailableContainersResponse" element
	 */
	@Override
	public void setGetAvailableContainersResponse(GetAvailableContainersResponseDocument.GetAvailableContainersResponse getAvailableContainersResponse) {
		generatedSetterHelperImpl(getAvailableContainersResponse, GETAVAILABLECONTAINERSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
