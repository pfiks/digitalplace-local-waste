/*
 * An XML document type.
 * Localname: AHP_ChangeAddressResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPChangeAddressResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one AHP_ChangeAddressResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface AHPChangeAddressResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPChangeAddressResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpchangeaddressresponsecbb9doctype");
    
    /**
     * Gets the "AHP_ChangeAddressResponse" element
     */
    AHPChangeAddressResponseDocument.AHPChangeAddressResponse getAHPChangeAddressResponse();
    
    /**
     * Sets the "AHP_ChangeAddressResponse" element
     */
    void setAHPChangeAddressResponse(AHPChangeAddressResponseDocument.AHPChangeAddressResponse ahpChangeAddressResponse);
    
    /**
     * Appends and returns a new empty "AHP_ChangeAddressResponse" element
     */
    AHPChangeAddressResponseDocument.AHPChangeAddressResponse addNewAHPChangeAddressResponse();
    
    /**
     * An XML AHP_ChangeAddressResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface AHPChangeAddressResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPChangeAddressResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpchangeaddressresponse0014elemtype");
        
        /**
         * Gets the "AHP_ChangeAddressResult" element
         */
        AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult getAHPChangeAddressResult();
        
        /**
         * True if has "AHP_ChangeAddressResult" element
         */
        boolean isSetAHPChangeAddressResult();
        
        /**
         * Sets the "AHP_ChangeAddressResult" element
         */
        void setAHPChangeAddressResult(AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult ahpChangeAddressResult);
        
        /**
         * Appends and returns a new empty "AHP_ChangeAddressResult" element
         */
        AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult addNewAHPChangeAddressResult();
        
        /**
         * Unsets the "AHP_ChangeAddressResult" element
         */
        void unsetAHPChangeAddressResult();
        
        /**
         * An XML AHP_ChangeAddressResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface AHPChangeAddressResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AHPChangeAddressResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("ahpchangeaddressresultda4belemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult newInstance() {
                  return (AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (AHPChangeAddressResponseDocument.AHPChangeAddressResponse.AHPChangeAddressResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static AHPChangeAddressResponseDocument.AHPChangeAddressResponse newInstance() {
              return (AHPChangeAddressResponseDocument.AHPChangeAddressResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static AHPChangeAddressResponseDocument.AHPChangeAddressResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (AHPChangeAddressResponseDocument.AHPChangeAddressResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static AHPChangeAddressResponseDocument newInstance() {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static AHPChangeAddressResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static AHPChangeAddressResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static AHPChangeAddressResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static AHPChangeAddressResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static AHPChangeAddressResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static AHPChangeAddressResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static AHPChangeAddressResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static AHPChangeAddressResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static AHPChangeAddressResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static AHPChangeAddressResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static AHPChangeAddressResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static AHPChangeAddressResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static AHPChangeAddressResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static AHPChangeAddressResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static AHPChangeAddressResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AHPChangeAddressResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AHPChangeAddressResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AHPChangeAddressResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
