/*
 * An XML document type.
 * Localname: getRoundCalendarForUPRNResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetRoundCalendarForUPRNResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundCalendarForUPRNResponseDocument;

/**
 * A document containing one
 * getRoundCalendarForUPRNResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetRoundCalendarForUPRNResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetRoundCalendarForUPRNResponseDocument {

	/**
	 * An XML
	 * getRoundCalendarForUPRNResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetRoundCalendarForUPRNResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetRoundCalendarForUPRNResponseDocument.GetRoundCalendarForUPRNResponse {

		private static final javax.xml.namespace.QName GETROUNDCALENDARFORUPRNRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"getRoundCalendarForUPRNResult");

		private static final long serialVersionUID = 1L;

		public GetRoundCalendarForUPRNResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Gets the "getRoundCalendarForUPRNResult" element
		 */
		@Override
		public java.lang.String getGetRoundCalendarForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(GETROUNDCALENDARFORUPRNRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target.getStringValue();
			}
		}

		/**
		 * True if has "getRoundCalendarForUPRNResult" element
		 */
		@Override
		public boolean isSetGetRoundCalendarForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETROUNDCALENDARFORUPRNRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getRoundCalendarForUPRNResult" element
		 */
		@Override
		public void setGetRoundCalendarForUPRNResult(java.lang.String getRoundCalendarForUPRNResult) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.SimpleValue target = null;
				target = (org.apache.xmlbeans.SimpleValue) get_store().find_element_user(GETROUNDCALENDARFORUPRNRESULT$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.SimpleValue) get_store().add_element_user(GETROUNDCALENDARFORUPRNRESULT$0);
				}
				target.setStringValue(getRoundCalendarForUPRNResult);
			}
		}

		/**
		 * Unsets the "getRoundCalendarForUPRNResult" element
		 */
		@Override
		public void unsetGetRoundCalendarForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETROUNDCALENDARFORUPRNRESULT$0, 0);
			}
		}

		/**
		 * Gets (as xml) the "getRoundCalendarForUPRNResult" element
		 */
		@Override
		public org.apache.xmlbeans.XmlString xgetGetRoundCalendarForUPRNResult() {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(GETROUNDCALENDARFORUPRNRESULT$0, 0);
				return target;
			}
		}

		/**
		 * Sets (as xml) the "getRoundCalendarForUPRNResult" element
		 */
		@Override
		public void xsetGetRoundCalendarForUPRNResult(org.apache.xmlbeans.XmlString getRoundCalendarForUPRNResult) {
			synchronized (monitor()) {
				check_orphaned();
				org.apache.xmlbeans.XmlString target = null;
				target = (org.apache.xmlbeans.XmlString) get_store().find_element_user(GETROUNDCALENDARFORUPRNRESULT$0, 0);
				if (target == null) {
					target = (org.apache.xmlbeans.XmlString) get_store().add_element_user(GETROUNDCALENDARFORUPRNRESULT$0);
				}
				target.set(getRoundCalendarForUPRNResult);
			}
		}
	}

	private static final javax.xml.namespace.QName GETROUNDCALENDARFORUPRNRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"getRoundCalendarForUPRNResponse");

	private static final long serialVersionUID = 1L;

	public GetRoundCalendarForUPRNResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getRoundCalendarForUPRNResponse" element
	 */
	@Override
	public GetRoundCalendarForUPRNResponseDocument.GetRoundCalendarForUPRNResponse addNewGetRoundCalendarForUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundCalendarForUPRNResponseDocument.GetRoundCalendarForUPRNResponse target = null;
			target = (GetRoundCalendarForUPRNResponseDocument.GetRoundCalendarForUPRNResponse) get_store().add_element_user(GETROUNDCALENDARFORUPRNRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getRoundCalendarForUPRNResponse" element
	 */
	@Override
	public GetRoundCalendarForUPRNResponseDocument.GetRoundCalendarForUPRNResponse getGetRoundCalendarForUPRNResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundCalendarForUPRNResponseDocument.GetRoundCalendarForUPRNResponse target = null;
			target = (GetRoundCalendarForUPRNResponseDocument.GetRoundCalendarForUPRNResponse) get_store().find_element_user(GETROUNDCALENDARFORUPRNRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getRoundCalendarForUPRNResponse" element
	 */
	@Override
	public void setGetRoundCalendarForUPRNResponse(GetRoundCalendarForUPRNResponseDocument.GetRoundCalendarForUPRNResponse getRoundCalendarForUPRNResponse) {
		generatedSetterHelperImpl(getRoundCalendarForUPRNResponse, GETROUNDCALENDARFORUPRNRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
