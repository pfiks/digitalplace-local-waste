/*
 * An XML document type.
 * Localname: ShowAdditionalBinsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: ShowAdditionalBinsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one ShowAdditionalBinsResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface ShowAdditionalBinsResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ShowAdditionalBinsResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("showadditionalbinsresponsefc8ddoctype");
    
    /**
     * Gets the "ShowAdditionalBinsResponse" element
     */
    ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse getShowAdditionalBinsResponse();
    
    /**
     * Sets the "ShowAdditionalBinsResponse" element
     */
    void setShowAdditionalBinsResponse(ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse showAdditionalBinsResponse);
    
    /**
     * Appends and returns a new empty "ShowAdditionalBinsResponse" element
     */
    ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse addNewShowAdditionalBinsResponse();
    
    /**
     * An XML ShowAdditionalBinsResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface ShowAdditionalBinsResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ShowAdditionalBinsResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("showadditionalbinsresponse8de0elemtype");
        
        /**
         * Gets the "ShowAdditionalBinsResult" element
         */
        ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult getShowAdditionalBinsResult();
        
        /**
         * True if has "ShowAdditionalBinsResult" element
         */
        boolean isSetShowAdditionalBinsResult();
        
        /**
         * Sets the "ShowAdditionalBinsResult" element
         */
        void setShowAdditionalBinsResult(ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult showAdditionalBinsResult);
        
        /**
         * Appends and returns a new empty "ShowAdditionalBinsResult" element
         */
        ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult addNewShowAdditionalBinsResult();
        
        /**
         * Unsets the "ShowAdditionalBinsResult" element
         */
        void unsetShowAdditionalBinsResult();
        
        /**
         * An XML ShowAdditionalBinsResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface ShowAdditionalBinsResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ShowAdditionalBinsResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("showadditionalbinsresult7b49elemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult newInstance() {
                  return (ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse.ShowAdditionalBinsResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse newInstance() {
              return (ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (ShowAdditionalBinsResponseDocument.ShowAdditionalBinsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static ShowAdditionalBinsResponseDocument newInstance() {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static ShowAdditionalBinsResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static ShowAdditionalBinsResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static ShowAdditionalBinsResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static ShowAdditionalBinsResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static ShowAdditionalBinsResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static ShowAdditionalBinsResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static ShowAdditionalBinsResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static ShowAdditionalBinsResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static ShowAdditionalBinsResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static ShowAdditionalBinsResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static ShowAdditionalBinsResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static ShowAdditionalBinsResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static ShowAdditionalBinsResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static ShowAdditionalBinsResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static ShowAdditionalBinsResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static ShowAdditionalBinsResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static ShowAdditionalBinsResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (ShowAdditionalBinsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
