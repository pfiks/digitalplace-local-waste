/*
 * An XML document type.
 * Localname: getRoundNameForUPRNServiceResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetRoundNameForUPRNServiceResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetRoundNameForUPRNServiceResponseDocument;

/**
 * A document containing one
 * getRoundNameForUPRNServiceResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetRoundNameForUPRNServiceResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetRoundNameForUPRNServiceResponseDocument {

	/**
	 * An XML
	 * getRoundNameForUPRNServiceResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetRoundNameForUPRNServiceResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse {

		/**
		 * An XML
		 * getRoundNameForUPRNServiceResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetRoundNameForUPRNServiceResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult {

			private static final long serialVersionUID = 1L;

			public GetRoundNameForUPRNServiceResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETROUNDNAMEFORUPRNSERVICERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"getRoundNameForUPRNServiceResult");

		private static final long serialVersionUID = 1L;

		public GetRoundNameForUPRNServiceResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "getRoundNameForUPRNServiceResult"
		 * element
		 */
		@Override
		public GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult addNewGetRoundNameForUPRNServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult target = null;
				target = (GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult) get_store()
						.add_element_user(GETROUNDNAMEFORUPRNSERVICERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getRoundNameForUPRNServiceResult" element
		 */
		@Override
		public GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult getGetRoundNameForUPRNServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult target = null;
				target = (GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult) get_store()
						.find_element_user(GETROUNDNAMEFORUPRNSERVICERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getRoundNameForUPRNServiceResult" element
		 */
		@Override
		public boolean isSetGetRoundNameForUPRNServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETROUNDNAMEFORUPRNSERVICERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getRoundNameForUPRNServiceResult" element
		 */
		@Override
		public void setGetRoundNameForUPRNServiceResult(
				GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse.GetRoundNameForUPRNServiceResult getRoundNameForUPRNServiceResult) {
			generatedSetterHelperImpl(getRoundNameForUPRNServiceResult, GETROUNDNAMEFORUPRNSERVICERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getRoundNameForUPRNServiceResult" element
		 */
		@Override
		public void unsetGetRoundNameForUPRNServiceResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETROUNDNAMEFORUPRNSERVICERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETROUNDNAMEFORUPRNSERVICERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"getRoundNameForUPRNServiceResponse");

	private static final long serialVersionUID = 1L;

	public GetRoundNameForUPRNServiceResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getRoundNameForUPRNServiceResponse"
	 * element
	 */
	@Override
	public GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse addNewGetRoundNameForUPRNServiceResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse target = null;
			target = (GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse) get_store().add_element_user(GETROUNDNAMEFORUPRNSERVICERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getRoundNameForUPRNServiceResponse" element
	 */
	@Override
	public GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse getGetRoundNameForUPRNServiceResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse target = null;
			target = (GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse) get_store().find_element_user(GETROUNDNAMEFORUPRNSERVICERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getRoundNameForUPRNServiceResponse" element
	 */
	@Override
	public void setGetRoundNameForUPRNServiceResponse(GetRoundNameForUPRNServiceResponseDocument.GetRoundNameForUPRNServiceResponse getRoundNameForUPRNServiceResponse) {
		generatedSetterHelperImpl(getRoundNameForUPRNServiceResponse, GETROUNDNAMEFORUPRNSERVICERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
