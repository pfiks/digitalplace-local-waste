/*
 * An XML document type.
 * Localname: AHP_OrderNewBagsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AHPOrderNewBagsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.AHPOrderNewBagsResponseDocument;

/**
 * A document containing one
 * AHP_OrderNewBagsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class AHPOrderNewBagsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPOrderNewBagsResponseDocument {

	/**
	 * An XML
	 * AHP_OrderNewBagsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class AHPOrderNewBagsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse {

		/**
		 * An XML
		 * AHP_OrderNewBagsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class AHPOrderNewBagsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse.AHPOrderNewBagsResult {

			private static final long serialVersionUID = 1L;

			public AHPOrderNewBagsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName AHPORDERNEWBAGSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_OrderNewBagsResult");

		private static final long serialVersionUID = 1L;

		public AHPOrderNewBagsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "AHP_OrderNewBagsResult" element
		 */
		@Override
		public AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse.AHPOrderNewBagsResult addNewAHPOrderNewBagsResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse.AHPOrderNewBagsResult target = null;
				target = (AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse.AHPOrderNewBagsResult) get_store().add_element_user(AHPORDERNEWBAGSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "AHP_OrderNewBagsResult" element
		 */
		@Override
		public AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse.AHPOrderNewBagsResult getAHPOrderNewBagsResult() {
			synchronized (monitor()) {
				check_orphaned();
				AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse.AHPOrderNewBagsResult target = null;
				target = (AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse.AHPOrderNewBagsResult) get_store().find_element_user(AHPORDERNEWBAGSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "AHP_OrderNewBagsResult" element
		 */
		@Override
		public boolean isSetAHPOrderNewBagsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(AHPORDERNEWBAGSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "AHP_OrderNewBagsResult" element
		 */
		@Override
		public void setAHPOrderNewBagsResult(AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse.AHPOrderNewBagsResult ahpOrderNewBagsResult) {
			generatedSetterHelperImpl(ahpOrderNewBagsResult, AHPORDERNEWBAGSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "AHP_OrderNewBagsResult" element
		 */
		@Override
		public void unsetAHPOrderNewBagsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(AHPORDERNEWBAGSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName AHPORDERNEWBAGSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "AHP_OrderNewBagsResponse");

	private static final long serialVersionUID = 1L;

	public AHPOrderNewBagsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "AHP_OrderNewBagsResponse" element
	 */
	@Override
	public AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse addNewAHPOrderNewBagsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse target = null;
			target = (AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse) get_store().add_element_user(AHPORDERNEWBAGSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "AHP_OrderNewBagsResponse" element
	 */
	@Override
	public AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse getAHPOrderNewBagsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse target = null;
			target = (AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse) get_store().find_element_user(AHPORDERNEWBAGSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "AHP_OrderNewBagsResponse" element
	 */
	@Override
	public void setAHPOrderNewBagsResponse(AHPOrderNewBagsResponseDocument.AHPOrderNewBagsResponse ahpOrderNewBagsResponse) {
		generatedSetterHelperImpl(ahpOrderNewBagsResponse, AHPORDERNEWBAGSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
