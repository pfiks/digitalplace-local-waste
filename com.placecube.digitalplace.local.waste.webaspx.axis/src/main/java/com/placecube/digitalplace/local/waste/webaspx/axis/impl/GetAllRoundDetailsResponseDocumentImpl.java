/*
 * An XML document type.
 * Localname: GetAllRoundDetailsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAllRoundDetailsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAllRoundDetailsResponseDocument;

/**
 * A document containing one
 * GetAllRoundDetailsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAllRoundDetailsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAllRoundDetailsResponseDocument {

	/**
	 * An XML
	 * GetAllRoundDetailsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAllRoundDetailsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse {

		/**
		 * An XML
		 * GetAllRoundDetailsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetAllRoundDetailsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse.GetAllRoundDetailsResult {

			private static final long serialVersionUID = 1L;

			public GetAllRoundDetailsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETALLROUNDDETAILSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAllRoundDetailsResult");

		private static final long serialVersionUID = 1L;

		public GetAllRoundDetailsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "GetAllRoundDetailsResult" element
		 */
		@Override
		public GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse.GetAllRoundDetailsResult addNewGetAllRoundDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse.GetAllRoundDetailsResult target = null;
				target = (GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse.GetAllRoundDetailsResult) get_store().add_element_user(GETALLROUNDDETAILSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "GetAllRoundDetailsResult" element
		 */
		@Override
		public GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse.GetAllRoundDetailsResult getGetAllRoundDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse.GetAllRoundDetailsResult target = null;
				target = (GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse.GetAllRoundDetailsResult) get_store().find_element_user(GETALLROUNDDETAILSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "GetAllRoundDetailsResult" element
		 */
		@Override
		public boolean isSetGetAllRoundDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETALLROUNDDETAILSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "GetAllRoundDetailsResult" element
		 */
		@Override
		public void setGetAllRoundDetailsResult(GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse.GetAllRoundDetailsResult getAllRoundDetailsResult) {
			generatedSetterHelperImpl(getAllRoundDetailsResult, GETALLROUNDDETAILSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "GetAllRoundDetailsResult" element
		 */
		@Override
		public void unsetGetAllRoundDetailsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETALLROUNDDETAILSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETALLROUNDDETAILSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "GetAllRoundDetailsResponse");

	private static final long serialVersionUID = 1L;

	public GetAllRoundDetailsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "GetAllRoundDetailsResponse" element
	 */
	@Override
	public GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse addNewGetAllRoundDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse target = null;
			target = (GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse) get_store().add_element_user(GETALLROUNDDETAILSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "GetAllRoundDetailsResponse" element
	 */
	@Override
	public GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse getGetAllRoundDetailsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse target = null;
			target = (GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse) get_store().find_element_user(GETALLROUNDDETAILSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "GetAllRoundDetailsResponse" element
	 */
	@Override
	public void setGetAllRoundDetailsResponse(GetAllRoundDetailsResponseDocument.GetAllRoundDetailsResponse getAllRoundDetailsResponse) {
		generatedSetterHelperImpl(getAllRoundDetailsResponse, GETALLROUNDDETAILSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
