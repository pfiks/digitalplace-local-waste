/*
 * An XML document type.
 * Localname: Garden_RemoveSubscriptionResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenRemoveSubscriptionResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GardenRemoveSubscriptionResponseDocument;

/**
 * A document containing one
 * Garden_RemoveSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GardenRemoveSubscriptionResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GardenRemoveSubscriptionResponseDocument {

	/**
	 * An XML
	 * Garden_RemoveSubscriptionResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GardenRemoveSubscriptionResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse {

		/**
		 * An XML
		 * Garden_RemoveSubscriptionResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GardenRemoveSubscriptionResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse.GardenRemoveSubscriptionResult {

			private static final long serialVersionUID = 1L;

			public GardenRemoveSubscriptionResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GARDENREMOVESUBSCRIPTIONRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
				"Garden_RemoveSubscriptionResult");

		private static final long serialVersionUID = 1L;

		public GardenRemoveSubscriptionResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "Garden_RemoveSubscriptionResult"
		 * element
		 */
		@Override
		public GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse.GardenRemoveSubscriptionResult addNewGardenRemoveSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse.GardenRemoveSubscriptionResult target = null;
				target = (GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse.GardenRemoveSubscriptionResult) get_store().add_element_user(GARDENREMOVESUBSCRIPTIONRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "Garden_RemoveSubscriptionResult" element
		 */
		@Override
		public GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse.GardenRemoveSubscriptionResult getGardenRemoveSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse.GardenRemoveSubscriptionResult target = null;
				target = (GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse.GardenRemoveSubscriptionResult) get_store().find_element_user(GARDENREMOVESUBSCRIPTIONRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "Garden_RemoveSubscriptionResult" element
		 */
		@Override
		public boolean isSetGardenRemoveSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GARDENREMOVESUBSCRIPTIONRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "Garden_RemoveSubscriptionResult" element
		 */
		@Override
		public void setGardenRemoveSubscriptionResult(GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse.GardenRemoveSubscriptionResult gardenRemoveSubscriptionResult) {
			generatedSetterHelperImpl(gardenRemoveSubscriptionResult, GARDENREMOVESUBSCRIPTIONRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "Garden_RemoveSubscriptionResult" element
		 */
		@Override
		public void unsetGardenRemoveSubscriptionResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GARDENREMOVESUBSCRIPTIONRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GARDENREMOVESUBSCRIPTIONRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/",
			"Garden_RemoveSubscriptionResponse");

	private static final long serialVersionUID = 1L;

	public GardenRemoveSubscriptionResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "Garden_RemoveSubscriptionResponse"
	 * element
	 */
	@Override
	public GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse addNewGardenRemoveSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse target = null;
			target = (GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse) get_store().add_element_user(GARDENREMOVESUBSCRIPTIONRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "Garden_RemoveSubscriptionResponse" element
	 */
	@Override
	public GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse getGardenRemoveSubscriptionResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse target = null;
			target = (GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse) get_store().find_element_user(GARDENREMOVESUBSCRIPTIONRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "Garden_RemoveSubscriptionResponse" element
	 */
	@Override
	public void setGardenRemoveSubscriptionResponse(GardenRemoveSubscriptionResponseDocument.GardenRemoveSubscriptionResponse gardenRemoveSubscriptionResponse) {
		generatedSetterHelperImpl(gardenRemoveSubscriptionResponse, GARDENREMOVESUBSCRIPTIONRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
