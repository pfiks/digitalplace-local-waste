/*
 * An XML document type.
 * Localname: getAddressFromPostcodeResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GetAddressFromPostcodeResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.GetAddressFromPostcodeResponseDocument;

/**
 * A document containing one
 * getAddressFromPostcodeResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class GetAddressFromPostcodeResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements GetAddressFromPostcodeResponseDocument {

	/**
	 * An XML
	 * getAddressFromPostcodeResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class GetAddressFromPostcodeResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
			implements GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse {

		/**
		 * An XML
		 * getAddressFromPostcodeResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class GetAddressFromPostcodeResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult {

			private static final long serialVersionUID = 1L;

			public GetAddressFromPostcodeResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName GETADDRESSFROMPOSTCODERESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressFromPostcodeResult");

		private static final long serialVersionUID = 1L;

		public GetAddressFromPostcodeResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "getAddressFromPostcodeResult"
		 * element
		 */
		@Override
		public GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult addNewGetAddressFromPostcodeResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult target = null;
				target = (GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult) get_store().add_element_user(GETADDRESSFROMPOSTCODERESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "getAddressFromPostcodeResult" element
		 */
		@Override
		public GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult getGetAddressFromPostcodeResult() {
			synchronized (monitor()) {
				check_orphaned();
				GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult target = null;
				target = (GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult) get_store().find_element_user(GETADDRESSFROMPOSTCODERESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "getAddressFromPostcodeResult" element
		 */
		@Override
		public boolean isSetGetAddressFromPostcodeResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(GETADDRESSFROMPOSTCODERESULT$0) != 0;
			}
		}

		/**
		 * Sets the "getAddressFromPostcodeResult" element
		 */
		@Override
		public void setGetAddressFromPostcodeResult(GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse.GetAddressFromPostcodeResult getAddressFromPostcodeResult) {
			generatedSetterHelperImpl(getAddressFromPostcodeResult, GETADDRESSFROMPOSTCODERESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "getAddressFromPostcodeResult" element
		 */
		@Override
		public void unsetGetAddressFromPostcodeResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(GETADDRESSFROMPOSTCODERESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName GETADDRESSFROMPOSTCODERESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "getAddressFromPostcodeResponse");

	private static final long serialVersionUID = 1L;

	public GetAddressFromPostcodeResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "getAddressFromPostcodeResponse" element
	 */
	@Override
	public GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse addNewGetAddressFromPostcodeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse target = null;
			target = (GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse) get_store().add_element_user(GETADDRESSFROMPOSTCODERESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "getAddressFromPostcodeResponse" element
	 */
	@Override
	public GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse getGetAddressFromPostcodeResponse() {
		synchronized (monitor()) {
			check_orphaned();
			GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse target = null;
			target = (GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse) get_store().find_element_user(GETADDRESSFROMPOSTCODERESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "getAddressFromPostcodeResponse" element
	 */
	@Override
	public void setGetAddressFromPostcodeResponse(GetAddressFromPostcodeResponseDocument.GetAddressFromPostcodeResponse getAddressFromPostcodeResponse) {
		generatedSetterHelperImpl(getAddressFromPostcodeResponse, GETADDRESSFROMPOSTCODERESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
