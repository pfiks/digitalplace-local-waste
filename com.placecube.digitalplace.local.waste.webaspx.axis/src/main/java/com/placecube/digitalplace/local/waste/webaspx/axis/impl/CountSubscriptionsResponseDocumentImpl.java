/*
 * An XML document type.
 * Localname: countSubscriptionsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: CountSubscriptionsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis.impl;

import com.placecube.digitalplace.local.waste.webaspx.axis.CountSubscriptionsResponseDocument;

/**
 * A document containing one
 * countSubscriptionsResponse(@http://webaspx-collections.azurewebsites.net/)
 * element.
 *
 * This is a complex type.
 */
public class CountSubscriptionsResponseDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CountSubscriptionsResponseDocument {

	/**
	 * An XML
	 * countSubscriptionsResponse(@http://webaspx-collections.azurewebsites.net/).
	 *
	 * This is a complex type.
	 */
	public static class CountSubscriptionsResponseImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements CountSubscriptionsResponseDocument.CountSubscriptionsResponse {

		/**
		 * An XML
		 * countSubscriptionsResult(@http://webaspx-collections.azurewebsites.net/).
		 *
		 * This is a complex type.
		 */
		public static class CountSubscriptionsResultImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl
				implements CountSubscriptionsResponseDocument.CountSubscriptionsResponse.CountSubscriptionsResult {

			private static final long serialVersionUID = 1L;

			public CountSubscriptionsResultImpl(org.apache.xmlbeans.SchemaType sType) {
				super(sType);
			}

		}

		private static final javax.xml.namespace.QName COUNTSUBSCRIPTIONSRESULT$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countSubscriptionsResult");

		private static final long serialVersionUID = 1L;

		public CountSubscriptionsResponseImpl(org.apache.xmlbeans.SchemaType sType) {
			super(sType);
		}

		/**
		 * Appends and returns a new empty "countSubscriptionsResult" element
		 */
		@Override
		public CountSubscriptionsResponseDocument.CountSubscriptionsResponse.CountSubscriptionsResult addNewCountSubscriptionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				CountSubscriptionsResponseDocument.CountSubscriptionsResponse.CountSubscriptionsResult target = null;
				target = (CountSubscriptionsResponseDocument.CountSubscriptionsResponse.CountSubscriptionsResult) get_store().add_element_user(COUNTSUBSCRIPTIONSRESULT$0);
				return target;
			}
		}

		/**
		 * Gets the "countSubscriptionsResult" element
		 */
		@Override
		public CountSubscriptionsResponseDocument.CountSubscriptionsResponse.CountSubscriptionsResult getCountSubscriptionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				CountSubscriptionsResponseDocument.CountSubscriptionsResponse.CountSubscriptionsResult target = null;
				target = (CountSubscriptionsResponseDocument.CountSubscriptionsResponse.CountSubscriptionsResult) get_store().find_element_user(COUNTSUBSCRIPTIONSRESULT$0, 0);
				if (target == null) {
					return null;
				}
				return target;
			}
		}

		/**
		 * True if has "countSubscriptionsResult" element
		 */
		@Override
		public boolean isSetCountSubscriptionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				return get_store().count_elements(COUNTSUBSCRIPTIONSRESULT$0) != 0;
			}
		}

		/**
		 * Sets the "countSubscriptionsResult" element
		 */
		@Override
		public void setCountSubscriptionsResult(CountSubscriptionsResponseDocument.CountSubscriptionsResponse.CountSubscriptionsResult countSubscriptionsResult) {
			generatedSetterHelperImpl(countSubscriptionsResult, COUNTSUBSCRIPTIONSRESULT$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
		}

		/**
		 * Unsets the "countSubscriptionsResult" element
		 */
		@Override
		public void unsetCountSubscriptionsResult() {
			synchronized (monitor()) {
				check_orphaned();
				get_store().remove_element(COUNTSUBSCRIPTIONSRESULT$0, 0);
			}
		}
	}

	private static final javax.xml.namespace.QName COUNTSUBSCRIPTIONSRESPONSE$0 = new javax.xml.namespace.QName("http://webaspx-collections.azurewebsites.net/", "countSubscriptionsResponse");

	private static final long serialVersionUID = 1L;

	public CountSubscriptionsResponseDocumentImpl(org.apache.xmlbeans.SchemaType sType) {
		super(sType);
	}

	/**
	 * Appends and returns a new empty "countSubscriptionsResponse" element
	 */
	@Override
	public CountSubscriptionsResponseDocument.CountSubscriptionsResponse addNewCountSubscriptionsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CountSubscriptionsResponseDocument.CountSubscriptionsResponse target = null;
			target = (CountSubscriptionsResponseDocument.CountSubscriptionsResponse) get_store().add_element_user(COUNTSUBSCRIPTIONSRESPONSE$0);
			return target;
		}
	}

	/**
	 * Gets the "countSubscriptionsResponse" element
	 */
	@Override
	public CountSubscriptionsResponseDocument.CountSubscriptionsResponse getCountSubscriptionsResponse() {
		synchronized (monitor()) {
			check_orphaned();
			CountSubscriptionsResponseDocument.CountSubscriptionsResponse target = null;
			target = (CountSubscriptionsResponseDocument.CountSubscriptionsResponse) get_store().find_element_user(COUNTSUBSCRIPTIONSRESPONSE$0, 0);
			if (target == null) {
				return null;
			}
			return target;
		}
	}

	/**
	 * Sets the "countSubscriptionsResponse" element
	 */
	@Override
	public void setCountSubscriptionsResponse(CountSubscriptionsResponseDocument.CountSubscriptionsResponse countSubscriptionsResponse) {
		generatedSetterHelperImpl(countSubscriptionsResponse, COUNTSUBSCRIPTIONSRESPONSE$0, 0, org.apache.xmlbeans.impl.values.XmlObjectBase.KIND_SETTERHELPER_SINGLETON);
	}
}
