/*
 * An XML document type.
 * Localname: GardenSubscription_GetDetailsResponse
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: GardenSubscriptionGetDetailsResponseDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one GardenSubscription_GetDetailsResponse(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface GardenSubscriptionGetDetailsResponseDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscriptionGetDetailsResponseDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscriptiongetdetailsresponse3e24doctype");
    
    /**
     * Gets the "GardenSubscription_GetDetailsResponse" element
     */
    GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse getGardenSubscriptionGetDetailsResponse();
    
    /**
     * Sets the "GardenSubscription_GetDetailsResponse" element
     */
    void setGardenSubscriptionGetDetailsResponse(GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse gardenSubscriptionGetDetailsResponse);
    
    /**
     * Appends and returns a new empty "GardenSubscription_GetDetailsResponse" element
     */
    GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse addNewGardenSubscriptionGetDetailsResponse();
    
    /**
     * An XML GardenSubscription_GetDetailsResponse(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface GardenSubscriptionGetDetailsResponse extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscriptionGetDetailsResponse.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscriptiongetdetailsresponse33eaelemtype");
        
        /**
         * Gets the "GardenSubscription_GetDetailsResult" element
         */
        GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult getGardenSubscriptionGetDetailsResult();
        
        /**
         * True if has "GardenSubscription_GetDetailsResult" element
         */
        boolean isSetGardenSubscriptionGetDetailsResult();
        
        /**
         * Sets the "GardenSubscription_GetDetailsResult" element
         */
        void setGardenSubscriptionGetDetailsResult(GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult gardenSubscriptionGetDetailsResult);
        
        /**
         * Appends and returns a new empty "GardenSubscription_GetDetailsResult" element
         */
        GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult addNewGardenSubscriptionGetDetailsResult();
        
        /**
         * Unsets the "GardenSubscription_GetDetailsResult" element
         */
        void unsetGardenSubscriptionGetDetailsResult();
        
        /**
         * An XML GardenSubscription_GetDetailsResult(@http://webaspx-collections.azurewebsites.net/).
         *
         * This is a complex type.
         */
        public interface GardenSubscriptionGetDetailsResult extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(GardenSubscriptionGetDetailsResult.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("gardensubscriptiongetdetailsresult42ccelemtype");
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult newInstance() {
                  return (GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse.GardenSubscriptionGetDetailsResult) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse newInstance() {
              return (GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (GardenSubscriptionGetDetailsResponseDocument.GardenSubscriptionGetDetailsResponse) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static GardenSubscriptionGetDetailsResponseDocument newInstance() {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static GardenSubscriptionGetDetailsResponseDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscriptionGetDetailsResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static GardenSubscriptionGetDetailsResponseDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (GardenSubscriptionGetDetailsResponseDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
