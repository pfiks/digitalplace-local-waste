/*
 * An XML document type.
 * Localname: AA_HelloWorld_XML_Test
 * Namespace: http://webaspx-collections.azurewebsites.net/
 * Java type: AAHelloWorldXMLTestDocument
 *
 * Automatically generated - do not modify.
 */
package com.placecube.digitalplace.local.waste.webaspx.axis;


/**
 * A document containing one AA_HelloWorld_XML_Test(@http://webaspx-collections.azurewebsites.net/) element.
 *
 * This is a complex type.
 */
public interface AAHelloWorldXMLTestDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AAHelloWorldXMLTestDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("aahelloworldxmltest4c16doctype");
    
    /**
     * Gets the "AA_HelloWorld_XML_Test" element
     */
    AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest getAAHelloWorldXMLTest();
    
    /**
     * Sets the "AA_HelloWorld_XML_Test" element
     */
    void setAAHelloWorldXMLTest(AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest aaHelloWorldXMLTest);
    
    /**
     * Appends and returns a new empty "AA_HelloWorld_XML_Test" element
     */
    AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest addNewAAHelloWorldXMLTest();
    
    /**
     * An XML AA_HelloWorld_XML_Test(@http://webaspx-collections.azurewebsites.net/).
     *
     * This is a complex type.
     */
    public interface AAHelloWorldXMLTest extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(AAHelloWorldXMLTest.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s732B39EEF3D8AFACF1BDA1F021236396").resolveHandle("aahelloworldxmltest9400elemtype");
        
        /**
         * Gets the "testName" element
         */
        java.lang.String getTestName();
        
        /**
         * Gets (as xml) the "testName" element
         */
        org.apache.xmlbeans.XmlString xgetTestName();
        
        /**
         * True if has "testName" element
         */
        boolean isSetTestName();
        
        /**
         * Sets the "testName" element
         */
        void setTestName(java.lang.String testName);
        
        /**
         * Sets (as xml) the "testName" element
         */
        void xsetTestName(org.apache.xmlbeans.XmlString testName);
        
        /**
         * Unsets the "testName" element
         */
        void unsetTestName();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest newInstance() {
              return (AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (AAHelloWorldXMLTestDocument.AAHelloWorldXMLTest) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static AAHelloWorldXMLTestDocument newInstance() {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static AAHelloWorldXMLTestDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static AAHelloWorldXMLTestDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static AAHelloWorldXMLTestDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static AAHelloWorldXMLTestDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static AAHelloWorldXMLTestDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static AAHelloWorldXMLTestDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static AAHelloWorldXMLTestDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static AAHelloWorldXMLTestDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static AAHelloWorldXMLTestDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static AAHelloWorldXMLTestDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static AAHelloWorldXMLTestDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static AAHelloWorldXMLTestDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static AAHelloWorldXMLTestDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static AAHelloWorldXMLTestDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static AAHelloWorldXMLTestDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AAHelloWorldXMLTestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static AAHelloWorldXMLTestDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (AAHelloWorldXMLTestDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
