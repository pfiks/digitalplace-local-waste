package com.placecube.digitalplace.local.waste.mock.service;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.waste.constants.SubscriptionType;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.AssistedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.model.BulkyCollectionDate;
import com.placecube.digitalplace.local.waste.model.BulkyWasteCollectionRequest;
import com.placecube.digitalplace.local.waste.model.GardenWasteSubscription;
import com.placecube.digitalplace.local.waste.model.MissedBinCollectionJob;
import com.placecube.digitalplace.local.waste.model.NewContainerRequest;
import com.placecube.digitalplace.local.waste.model.WasteSubscriptionResponse;
import com.placecube.digitalplace.local.waste.service.WasteConnector;

@Component(immediate = true, service = WasteConnector.class)
public class MockWasteConnector implements WasteConnector {

	private static final Log LOG = LogFactoryUtil.getLog(MockWasteConnector.class);

	private static final String DEFAULT_UPRN_NO_RESULTS = "1";
	private static final String UPRN_TOMORROW_ALL = "31241234";
	private static final String UPRN_TOMORROW_GARDEN = "41234512";
	private static final String UPRN_TOMORROW_GARDEN_REFUSE = "22123123";
	private static final String REFERENCE = "REF-12345678";

	@Reference
	private MockWasteService mockWasteService;

	@Override
	public String createAssistedBinCollectionJob(long companyId, AssistedBinCollectionJob assistedBinCollectionJob) {

		return REFERENCE;
	}

	@Override
	public WasteSubscriptionResponse createBinSubscription(long companyId, String uprn, String serviceType, String firstName, String lastName, String emailAddress, String telephone,
			int numberOfBins) {

		WasteSubscriptionResponse result = new WasteSubscriptionResponse(REFERENCE, SubscriptionType.GARDEN_WASTE);

		result.setUprn(uprn);
		result.setFirstName(firstName);
		result.setLastName(lastName);
		result.setEmailAddress(emailAddress);
		result.setTelephone(telephone);

		return result;
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, List<String> itemsToBeCollected,
			String locationOfItems, Date collectionDate) throws WasteRetrievalException {

		return REFERENCE;
	}

	@Override
	public String createFlyTippingReport(long companyId, String uprn, String firstName, String lastName, String emailAddress, String telephone, String typeOfRubbish, String sizeOfRubbish,
			String details, String coordinates, String locationDetails, String dateOfIncident) throws WasteRetrievalException {

		return REFERENCE;
	}

	@Override
	public WasteSubscriptionResponse createGardenWasteSubscription(long companyId, GardenWasteSubscription gardenWasteSubscription) {

		WasteSubscriptionResponse result = new WasteSubscriptionResponse(REFERENCE, SubscriptionType.GARDEN_WASTE);
		result.setUprn(gardenWasteSubscription.getUprn());
		result.setFirstName(gardenWasteSubscription.getFirstName());
		result.setLastName(gardenWasteSubscription.getLastName());
		result.setEmailAddress(gardenWasteSubscription.getEmailAddress());
		result.setTelephone(gardenWasteSubscription.getTelephone());
		result.setStartDate(LocalDate.now());
		result.setEndDate(LocalDate.now().plusDays(5));

		return result;
	}

	@Override
	public String createMissedBinCollectionJob(long companyId, MissedBinCollectionJob missedBinCollectionJob) {

		return REFERENCE;
	}

	@Override
	public String createNewWasteContainerRequest(long companyId, NewContainerRequest newContainerRequest) throws WasteRetrievalException {

		return REFERENCE;
	}

	@Override
	public boolean enabled(long companyId) {
		try {
			return mockWasteService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public List<WasteSubscriptionResponse> getAllWasteSubscriptions(long companyId, SubscriptionType subscriptionType) throws WasteRetrievalException {
		List<WasteSubscriptionResponse> results = new ArrayList<>();

		results.add(createSampleWasteSubscriptionResponse(UPRN_TOMORROW_ALL, "One", 1));
		results.add(createSampleWasteSubscriptionResponse(UPRN_TOMORROW_GARDEN, "Two", 5));
		results.add(createSampleWasteSubscriptionResponse(UPRN_TOMORROW_GARDEN_REFUSE, "Three", 10));
		results.add(createSampleWasteSubscriptionResponse("111111", "Four", 40));

		return results;
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRN(long companyId, String uprn) throws WasteRetrievalException {

		Set<BinCollection> binCollections = new HashSet<>();

		LocalDate now = LocalDate.now();

		if (UPRN_TOMORROW_ALL.equalsIgnoreCase(uprn)) {
			LocalDate followingCollectionDate = now.plusDays(1);
			binCollections.add(createSampleBinCollection("Black bin collection", "Collection is weekly on a Wednesday", "refuse", now, followingCollectionDate));
			binCollections.add(createSampleBinCollection("Recycling collection", "Collection is fortnightly on a Tuesday", "recycling", now, followingCollectionDate));
			binCollections.add(createSampleBinCollection("Garden collection", "Collection is fortnightly on a Tuesday", "garden", now, followingCollectionDate));

		} else if (UPRN_TOMORROW_GARDEN.equalsIgnoreCase(uprn)) {
			LocalDate followingCollectionDate = now.plusDays(1);
			binCollections.add(createSampleBinCollection("Garden collection", "Collection is fortnightly on a Tuesday", "garden", now, followingCollectionDate));

		} else if (UPRN_TOMORROW_GARDEN_REFUSE.equalsIgnoreCase(uprn)) {
			LocalDate followingCollectionDate = now.plusDays(1);
			binCollections.add(createSampleBinCollection("Black bin collection", "Collection is weekly on a Wednesday", "refuse", now, followingCollectionDate));
			binCollections.add(createSampleBinCollection("Garden collection", "Collection is fortnightly on a Tuesday", "garden", now, followingCollectionDate));
		} else if (DEFAULT_UPRN_NO_RESULTS.equalsIgnoreCase(uprn)) {
		} else {
			LocalDate collectionDate = now.plusWeeks(1);
			LocalDate followingCollectionDate = now.plusWeeks(2);
			binCollections.add(createSampleBinCollection("Black bin collection", "Collection is weekly on a Wednesday", "refuse", collectionDate, followingCollectionDate));
			binCollections.add(createSampleBinCollection("Recycling collection", "Collection is fortnightly on a Tuesday", "recycling", collectionDate, followingCollectionDate));
			binCollections.add(createSampleBinCollection("Garden collection", "Collection is fortnightly on a Tuesday", "garden", collectionDate, followingCollectionDate));
		}

		return binCollections;
	}

	@Override
	public Set<BinCollection> getBinCollectionDatesByUPRNAndService(long companyId, String uprn, String serviceName, String formInstanceId) throws WasteRetrievalException {
		return getBinCollectionDatesByUPRN(companyId, uprn);
	}

	@Override
	public Optional<BulkyCollectionDate> getBulkyCollectionDateByUPRN(long companyId, String uprn) throws WasteRetrievalException {

		int dayOfWeek = GregorianCalendar.getInstance().get(Calendar.DAY_OF_WEEK);
		String dayNameOfWeek = StringUtil.upperCaseFirstLetter(DayOfWeek.of(dayOfWeek).name().toLowerCase());

		return Optional.of(new BulkyCollectionDate(dayOfWeek, dayNameOfWeek));
	}

	@Override
	public String getMissedBinCollectionResponse(long companyId, String uprn, String date) throws WasteRetrievalException {
		return "The reason for not collecting your bin was XXX";
	}

	@Override
	public String getName(long companyId) {
		return "Waste - Mock";
	}

	@Override
	public int howManyBinsSubscribed(long companyId, String uprn, String serviceType) throws WasteRetrievalException {

		try {
			return Long.parseLong(uprn) % 2 == 0 ? 0 : 1;
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	@Override
	public int howManyBinsSubscribedForPeriod(long companyId, String uprn, String serviceType, String startDate, String endDate) throws WasteRetrievalException {

		try {
			return Long.parseLong(uprn) % 2 == 0 ? 0 : 1;
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY) {

		try {
			return Long.parseLong(uprn) % 2 == 0;
		} catch (NumberFormatException nfe) {
			return true;
		}
	}

	@Override
	public boolean isPropertyEligibleForGardenWasteSubscription(long companyId, String uprn, String startDateDDsMMsYYYY, String endDateDDsMMsYYYY, String binType, String formInstanceId) {

		try {
			return Long.parseLong(uprn) % 2 == 0;
		} catch (NumberFormatException nfe) {
			return true;
		}
	}

	@Override
	public boolean isPropertyEligibleForWasteContainerRequest(long companyId, String uprn, String binType, String binSize, String formInstanceId) throws WasteRetrievalException {
		try {
			return Long.parseLong(uprn) % 2 == 0;
		} catch (NumberFormatException nfe) {
			return true;
		}
	}

	@Override
	public boolean isPropertyEligibleForBulkyWasteCollection(long companyId, String uprn, List<String> itemsForCollection, String formInstanceId) throws WasteRetrievalException {
		try {
			return Long.parseLong(uprn) % 2 == 0;
		} catch (NumberFormatException nfe) {
			return true;
		}
	}

	@Override
	public boolean isPropertyEligibleForMissedBinCollectionReport(long companyId, String uprn, String binType, String formInstanceId) throws WasteRetrievalException {
		try {
			return Long.parseLong(uprn) % 2 == 0;
		} catch (NumberFormatException nfe) {
			return true;
		}
	}

	private BinCollection createSampleBinCollection(String name, String frequency, String type, LocalDate collectionDate, LocalDate followingCollectionDate) {
		Map<Locale, String> labelMap = Collections.singletonMap(LocaleUtil.getDefault(), name);
		String formattedCollectionDate = collectionDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		return new BinCollection.BinCollectionBuilder(type, collectionDate, labelMap).frequency(frequency).formattedCollectionDate(formattedCollectionDate)
				.followingCollectionDate(followingCollectionDate).build();
	}

	private WasteSubscriptionResponse createSampleWasteSubscriptionResponse(String uprn, String suffix, int expiryDaysToAdd) {
		WasteSubscriptionResponse result = new WasteSubscriptionResponse("ref-" + suffix, SubscriptionType.GARDEN_WASTE);
		result.setUprn(uprn);
		result.setEmailAddress("wasteExample@test." + suffix);
		result.setFirstName("Firstname_" + suffix);
		result.setLastName("LastName_" + suffix);
		LocalDate endDate = LocalDate.now().plusDays(expiryDaysToAdd);
		result.setEndDate(endDate);
		return result;
	}

	@Override
	public String createBulkyWasteCollectionJob(long companyId, BulkyWasteCollectionRequest bulkyWasteCollectionRequest, Date date) throws WasteRetrievalException {
		return REFERENCE;
	}

	@Override
	public List<Date> getBulkyCollectionSlotsByUPRNAndService(long companyId, String uprn, String serviceId, int numberOfDatesToReturn) throws WasteRetrievalException {
		LOG.debug("Get mock bulky collection slots for numberOfDatesToReturn: " + numberOfDatesToReturn);

		List<Date> results = new LinkedList<>();

		Instant instant = Instant.now();

		while (results.size() < numberOfDatesToReturn) {
			instant = instant.plus(7, ChronoUnit.DAYS);
			results.add(Date.from(instant));
		}
		return results;
	}

}
