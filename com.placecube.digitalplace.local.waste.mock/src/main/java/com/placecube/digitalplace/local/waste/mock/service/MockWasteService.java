package com.placecube.digitalplace.local.waste.mock.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.waste.mock.configuration.MockCompanyConfiguration;

@Component(immediate = true, service = MockWasteService.class)
public class MockWasteService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public boolean isEnabled(long companyId) throws ConfigurationException {
		MockCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}
}
