package com.placecube.digitalplace.local.waste.mock.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class MockWasteConnectorTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@InjectMocks
	private MockWasteConnector mockWasteConnector;

	@Mock
	private MockWasteService mockMockWasteService;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockMockWasteService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = mockWasteConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockMockWasteService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = mockWasteConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

}
