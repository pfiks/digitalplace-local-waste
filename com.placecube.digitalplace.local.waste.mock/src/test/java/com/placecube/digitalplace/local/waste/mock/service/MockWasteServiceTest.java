package com.placecube.digitalplace.local.waste.mock.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.waste.mock.configuration.MockCompanyConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class MockWasteServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private MockCompanyConfiguration mockMockCompanyConfiguration;

	@InjectMocks
	private MockWasteService mockWasteService;

	@Test(expected = ConfigurationException.class)
	public void isEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		mockWasteService.isEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMockCompanyConfiguration);
		when(mockMockCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = mockWasteService.isEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

}
