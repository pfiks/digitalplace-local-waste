<%@ include file="init.jsp" %>

<div class="page-wrapper collection-days-page">

	<h2><liferay-ui:message key="check-your-bin-collection-days"/></h2>

	<c:if test="${not empty fullAddress}">
		<div class="callout callout-success">
			<h4>
				<liferay-ui:message key="collection-days-for"/>
			</h4>
			<span  class="fullAddressDetails">
				${ fullAddress }
			</span>
		</div>
	</c:if>

	<c:choose>
		<c:when test="${not empty binCollections}">
			<h3><liferay-ui:message key="next-collection-dates"/></h3>

			<table class="table">
				<thead>
					<tr>
						<th scope="col"><liferay-ui:message key="collection-type" /></th>
						<th scope="col"><liferay-ui:message key="next-collection" /></th>
						<th scope="col"><liferay-ui:message key="frequency-collection" /></th>
						<th scope="col"><liferay-ui:message key="following-collection-dates" /></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${binCollections}" var="binCollection">
						<tr>
							<td>${ binCollection.getLabel(themeDisplay.getLocale()) }</td>
							<td>
								<fmt:parseDate value="${ binCollection.getCollectionDate() }" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
								<fmt:formatDate value="${ parsedDate }" pattern="EEEE dd MMM yyyy" />
							</td>
							<td>${ binCollection.getFrequency() }</td>
							<td>
								<fmt:parseDate value="${ binCollection.getFollowingCollectionDate() }" pattern="yyyy-MM-dd" var="parsedDate" type="date" />
								<fmt:formatDate value="${ parsedDate }" pattern="EEEE dd MMM yyyy" />
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		<c:otherwise>
			<h3><liferay-ui:message key="no-collection-dates"/></h3>
			<p><liferay-ui:message key="no-collection-dates-body"/></p>
		</c:otherwise>
	</c:choose>

	<br>

	<portlet:renderURL var="backURL" />
	<aui:button type="submit" value="back" cssClass="submit-button btn-prev" href="<%= backURL %>"></aui:button>

 </div>
