<%@ include file="init.jsp"%>

<portlet:renderURL var="getBinsURL">
	<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.GET_COLLECTION_DAYS %>" />
</portlet:renderURL>

<div class="page-wrapper address-lookup-page">

	<h2><liferay-ui:message key="check-your-bin-collection-days"/></h2>

	<blockquote>
		<span><liferay-ui:message key="collection-day-finder-help-message" /></span>
	</blockquote>

	<aui:form action="<%= getBinsURL %>" name="<portlet:namespace />fm">

		<dp-address:postcode-lookup selectedCallback="selectedCallback()" unselectedCallback="unselectedCallback()" />

		<aui:button-row>
			<aui:button id="fcd_submit" type="submit" value="find-collection-days" cssClass="submit-button" />
		</aui:button-row>

	</aui:form>
</div>

<script type="text/javascript">

	var ns = "<portlet:namespace/>";
	unselectedCallback();

	function selectedCallback() {
		document.getElementById(ns + 'fcd_submit').style.display = "block";
	}

	function unselectedCallback() {
		document.getElementById(ns + 'fcd_submit').style.display = "none";
	}
</script>
