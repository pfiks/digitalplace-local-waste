<%@ include file="init.jsp" %>

<liferay-ui:error key="com.placecube.digitalplace.local.waste.exception.WasteRetrievalException" message="waste-retrieval-exception" />

<portlet:renderURL var="backURL" />
<aui:button type="submit" icon="icon-angle-left" iconAlign="left" value="back" cssClass="submit-button" href="<%= backURL %>"></aui:button>
