package com.placecube.digitalplace.local.waste.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.local.waste.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.waste.web.constants.PortletKeys;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.web.service.CollectionDayService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.COLLECTION_DAY_FINDER, "mvc.command.name=" + MVCCommandKeys.GET_COLLECTION_DAYS }, service = MVCRenderCommand.class)
public class CollectionDayMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(CollectionDayMVCRenderCommand.class);

	@Reference
	private CollectionDayService collectionDayService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {
			collectionDayService.addBinCollectionsForProperty(renderRequest);
		} catch (WasteRetrievalException e) {
			LOG.error("Error getting bin collections for property: " + e.getMessage());
			LOG.debug(e);
			return "/collection-day-finder/view.jsp";
		}

		collectionDayService.addAddress(renderRequest);

		return "/collection-day-finder/collection-days.jsp";
	}

}
