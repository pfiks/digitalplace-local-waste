package com.placecube.digitalplace.local.waste.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.local.waste.web.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=portlet-bin-collection-day-finder", "com.liferay.portlet.display-category=category.waste",
		"com.liferay.portlet.instanceable=false", "javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/collection-day-finder/view.jsp",
		"javax.portlet.name=" + PortletKeys.COLLECTION_DAY_FINDER, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class CollectionDayFinderPortlet extends MVCPortlet {

}
