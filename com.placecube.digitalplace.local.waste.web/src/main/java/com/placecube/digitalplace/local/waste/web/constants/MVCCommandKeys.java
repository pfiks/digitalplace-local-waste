package com.placecube.digitalplace.local.waste.web.constants;

public final class MVCCommandKeys {

	public static final String GET_COLLECTION_DAYS = "/collection_day_finder/get_days";

	private MVCCommandKeys() {
	}

}