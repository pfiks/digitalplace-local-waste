package com.placecube.digitalplace.local.waste.web.constants;

public final class PortletKeys {

	public static final String COLLECTION_DAY_FINDER = "com_placecube_digitalplace_local_waste_portlet_CollectionDayFinderPortlet";

	private PortletKeys() {
	}

}