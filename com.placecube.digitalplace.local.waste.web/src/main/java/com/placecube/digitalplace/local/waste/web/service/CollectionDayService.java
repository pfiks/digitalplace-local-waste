package com.placecube.digitalplace.local.waste.web.service;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.service.WasteService;

@Component(immediate = true, service = CollectionDayService.class)
public class CollectionDayService {

	@Reference
	private WasteService wasteService;

	public void addAddress(RenderRequest renderRequest) {
		String fullAddress = HtmlUtil.escape(ParamUtil.getString(renderRequest, "fullAddress"));
		renderRequest.setAttribute("fullAddress", fullAddress);
	}

	public void addBinCollectionsForProperty(RenderRequest renderRequest) throws WasteRetrievalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String uprn = HtmlUtil.escape(ParamUtil.getString(renderRequest, "uprn"));
		renderRequest.setAttribute("binCollections", wasteService.getBinCollectionDatesByUPRN(themeDisplay.getCompanyId(), uprn));
	}
}