package com.placecube.digitalplace.local.waste.web.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Set;

import javax.portlet.RenderRequest;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.model.BinCollection;
import com.placecube.digitalplace.local.waste.service.WasteService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, HtmlUtil.class })
public class CollectionDayServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;

	private static final String FULL_ADDRESS = "1 Test Street, Test, TS1 1ST";

	private static final String UPRN = "12345678";

	@InjectMocks
	private CollectionDayService collectionDayService;

	@Mock
	private WasteService mockWasteService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Set<BinCollection> mockBinCollections;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, HtmlUtil.class);
	}

	@Test(expected = WasteRetrievalException.class)
	public void addBinCollectionsForProperty_WhenErrorGettingBinCollectionDaysFromWasteAPI_ThenThrowsWasteRetrievalException() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(ParamUtil.getString(mockRenderRequest, "uprn")).thenReturn(UPRN);

		when(HtmlUtil.escape(UPRN)).thenReturn(UPRN);

		when(mockWasteService.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenThrow(new WasteRetrievalException());

		collectionDayService.addBinCollectionsForProperty(mockRenderRequest);
	}

	@Test
	public void addBinCollectionsForProperty_WhenNoErrorGettingBinCollectionDaysFromWasteAPI_ThenAddsBinCollectionsToRequest() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(ParamUtil.getString(mockRenderRequest, "uprn")).thenReturn(UPRN);

		when(HtmlUtil.escape(UPRN)).thenReturn(UPRN);

		when(mockWasteService.getBinCollectionDatesByUPRN(COMPANY_ID, UPRN)).thenReturn(mockBinCollections);

		collectionDayService.addBinCollectionsForProperty(mockRenderRequest);

		verify(mockRenderRequest, times(1)).setAttribute("binCollections", mockBinCollections);
	}

	@Test
	public void addAddress_WhenNoError_ThenAddsAddressToRequest() throws Exception {

		when(ParamUtil.getString(mockRenderRequest, "fullAddress")).thenReturn(FULL_ADDRESS);

		when(HtmlUtil.escape(FULL_ADDRESS)).thenReturn(FULL_ADDRESS);

		collectionDayService.addAddress(mockRenderRequest);

		verify(mockRenderRequest, times(1)).setAttribute("fullAddress", FULL_ADDRESS);
	}
}