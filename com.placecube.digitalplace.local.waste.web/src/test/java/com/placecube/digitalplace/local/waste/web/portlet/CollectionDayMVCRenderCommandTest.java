package com.placecube.digitalplace.local.waste.web.portlet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.local.waste.exception.WasteRetrievalException;
import com.placecube.digitalplace.local.waste.web.service.CollectionDayService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, SessionErrors.class })
public class CollectionDayMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private CollectionDayMVCRenderCommand collectionDayMVCRenderCommand;

	@Mock
	private CollectionDayService mockCollectionDayService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Test
	public void render_WhenErrorAddingBinCollectionDays_ThenRedirectsToViewPage() throws Exception {

		doThrow(new WasteRetrievalException()).when(mockCollectionDayService).addBinCollectionsForProperty(mockRenderRequest);

		String result = collectionDayMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertEquals("/collection-day-finder/view.jsp", result);
	}

	@Test
	public void render_WhenNoErrorAddingBinCollectionDays_ThenAddsBinCollectionsToRequest() throws Exception {

		collectionDayMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockCollectionDayService, times(1)).addBinCollectionsForProperty(mockRenderRequest);
	}

	@Test
	public void render_WhenNoErrorAddingBinCollectionDays_ThenAddsFullAddressToRequest() throws Exception {

		collectionDayMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockCollectionDayService, times(1)).addAddress(mockRenderRequest);
	}

	@Test
	public void render_WhenNoErrorAddingBinCollectionDays_ThenRedirectToCollectionDaysPage() throws Exception {

		String result = collectionDayMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertEquals("/collection-day-finder/collection-days.jsp", result);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionErrors.class);
	}
}