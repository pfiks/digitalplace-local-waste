/**
 * Actions_type0.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_metrics_get_xsd;

/** Actions_type0 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Actions_type0 implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = Actions_type0
  Namespace URI = http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd
  Namespace Prefix = ns93
  */

  /** field for ActionID */
  protected int localActionID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getActionID() {
    return localActionID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ActionID
   */
  public void setActionID(int param) {

    this.localActionID = param;
  }

  /** field for ActionName */
  protected java.lang.String localActionName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localActionNameTracker = false;

  public boolean isActionNameSpecified() {
    return localActionNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getActionName() {
    return localActionName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ActionName
   */
  public void setActionName(java.lang.String param) {
    localActionNameTracker = param != null;

    this.localActionName = param;
  }

  /** field for PremisesCount */
  protected int localPremisesCount;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getPremisesCount() {
    return localPremisesCount;
  }

  /**
   * Auto generated setter method
   *
   * @param param PremisesCount
   */
  public void setPremisesCount(int param) {

    this.localPremisesCount = param;
  }

  /** field for JobsCount */
  protected int localJobsCount;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getJobsCount() {
    return localJobsCount;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobsCount
   */
  public void setJobsCount(int param) {

    this.localJobsCount = param;
  }

  /** field for JobsVolume */
  protected java.math.BigDecimal localJobsVolume;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getJobsVolume() {
    return localJobsVolume;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobsVolume
   */
  public void setJobsVolume(java.math.BigDecimal param) {

    this.localJobsVolume = param;
  }

  /** field for JobsWeight */
  protected java.math.BigDecimal localJobsWeight;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getJobsWeight() {
    return localJobsWeight;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobsWeight
   */
  public void setJobsWeight(java.math.BigDecimal param) {

    this.localJobsWeight = param;
  }

  /** field for NotStarted */
  protected int localNotStarted;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getNotStarted() {
    return localNotStarted;
  }

  /**
   * Auto generated setter method
   *
   * @param param NotStarted
   */
  public void setNotStarted(int param) {

    this.localNotStarted = param;
  }

  /** field for ClosedComplete */
  protected int localClosedComplete;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getClosedComplete() {
    return localClosedComplete;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosedComplete
   */
  public void setClosedComplete(int param) {

    this.localClosedComplete = param;
  }

  /** field for ClosedNotDone */
  protected int localClosedNotDone;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getClosedNotDone() {
    return localClosedNotDone;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosedNotDone
   */
  public void setClosedNotDone(int param) {

    this.localClosedNotDone = param;
  }

  /** field for ClosedNotRequired */
  protected int localClosedNotRequired;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getClosedNotRequired() {
    return localClosedNotRequired;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosedNotRequired
   */
  public void setClosedNotRequired(int param) {

    this.localClosedNotRequired = param;
  }

  /** field for ClosedPartDone */
  protected int localClosedPartDone;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getClosedPartDone() {
    return localClosedPartDone;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosedPartDone
   */
  public void setClosedPartDone(int param) {

    this.localClosedPartDone = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Actions_type0",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Actions_type0", xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "ActionID", xmlWriter);

    if (localActionID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ActionID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActionID));
    }

    xmlWriter.writeEndElement();
    if (localActionNameTracker) {
      namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
      writeStartElement(null, namespace, "ActionName", xmlWriter);

      if (localActionName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ActionName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localActionName);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "PremisesCount", xmlWriter);

    if (localPremisesCount == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("PremisesCount cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPremisesCount));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "JobsCount", xmlWriter);

    if (localJobsCount == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("JobsCount cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localJobsCount));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "JobsVolume", xmlWriter);

    if (localJobsVolume == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("JobsVolume cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localJobsVolume));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "JobsWeight", xmlWriter);

    if (localJobsWeight == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("JobsWeight cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localJobsWeight));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "NotStarted", xmlWriter);

    if (localNotStarted == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("NotStarted cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNotStarted));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "ClosedComplete", xmlWriter);

    if (localClosedComplete == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ClosedComplete cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClosedComplete));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "ClosedNotDone", xmlWriter);

    if (localClosedNotDone == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ClosedNotDone cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClosedNotDone));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "ClosedNotRequired", xmlWriter);

    if (localClosedNotRequired == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ClosedNotRequired cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClosedNotRequired));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd";
    writeStartElement(null, namespace, "ClosedPartDone", xmlWriter);

    if (localClosedPartDone == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ClosedPartDone cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClosedPartDone));
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd")) {
      return "ns93";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Actions_type0 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Actions_type0 object = new Actions_type0();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Actions_type0".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Actions_type0)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "ActionID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ActionID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setActionID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "ActionName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ActionName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setActionName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "PremisesCount")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PremisesCount" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPremisesCount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "JobsCount")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobsCount" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobsCount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "JobsVolume")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobsVolume" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobsVolume(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "JobsWeight")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobsWeight" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobsWeight(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "NotStarted")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "NotStarted" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setNotStarted(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "ClosedComplete")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosedComplete" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosedComplete(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "ClosedNotDone")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosedNotDone" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosedNotDone(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "ClosedNotRequired")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosedNotRequired" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosedNotRequired(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd", "ClosedPartDone")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosedPartDone" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosedPartDone(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
