/**
 * CtEvent.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** CtEvent bean class */
@SuppressWarnings({"unchecked", "unused"})
public class CtEvent implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ctEvent
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for ID */
  protected int localID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getID() {
    return localID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ID
   */
  public void setID(int param) {

    this.localID = param;
  }

  /** field for EventClass This was an Array! */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[] localEventClass;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventClassTracker = false;

  public boolean isEventClassSpecified() {
    return localEventClassTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[]
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[] getEventClass() {
    return localEventClass;
  }

  /** validate the array for EventClass */
  protected void validateEventClass(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[] param) {}

  /**
   * Auto generated setter method
   *
   * @param param EventClass
   */
  public void setEventClass(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[] param) {

    validateEventClass(param);

    localEventClassTracker = param != null;

    this.localEventClass = param;
  }

  /**
   * Auto generated add method for the array for convenience
   *
   * @param param com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass
   */
  public void addEventClass(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass param) {
    if (localEventClass == null) {
      localEventClass = new com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[] {};
    }

    // update the setting tracker
    localEventClassTracker = true;

    java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localEventClass);
    list.add(param);
    this.localEventClass =
        (com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[])
            list.toArray(new com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[list.size()]);
  }

  /** field for EventType */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType localEventType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventTypeTracker = false;

  public boolean isEventTypeSpecified() {
    return localEventTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType getEventType() {
    return localEventType;
  }

  /**
   * Auto generated setter method
   *
   * @param param EventType
   */
  public void setEventType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType param) {
    localEventTypeTracker = param != null;

    this.localEventType = param;
  }

  /** field for EventSubType This was an Array! */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] localEventSubType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventSubTypeTracker = false;

  public boolean isEventSubTypeSpecified() {
    return localEventSubTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[]
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] getEventSubType() {
    return localEventSubType;
  }

  /** validate the array for EventSubType */
  protected void validateEventSubType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] param) {}

  /**
   * Auto generated setter method
   *
   * @param param EventSubType
   */
  public void setEventSubType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] param) {

    validateEventSubType(param);

    localEventSubTypeTracker = param != null;

    this.localEventSubType = param;
  }

  /**
   * Auto generated add method for the array for convenience
   *
   * @param param com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType
   */
  public void addEventSubType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType param) {
    if (localEventSubType == null) {
      localEventSubType = new com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] {};
    }

    // update the setting tracker
    localEventSubTypeTracker = true;

    java.util.List list =
        org.apache.axis2.databinding.utils.ConverterUtil.toList(localEventSubType);
    list.add(param);
    this.localEventSubType =
        (com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[])
            list.toArray(new com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[list.size()]);
  }

  /** field for ExtendedData This was an Array! */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] localExtendedData;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExtendedDataTracker = false;

  public boolean isExtendedDataSpecified() {
    return localExtendedDataTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[]
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] getExtendedData() {
    return localExtendedData;
  }

  /** validate the array for ExtendedData */
  protected void validateExtendedData(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] param) {}

  /**
   * Auto generated setter method
   *
   * @param param ExtendedData
   */
  public void setExtendedData(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] param) {

    validateExtendedData(param);

    localExtendedDataTracker = param != null;

    this.localExtendedData = param;
  }

  /**
   * Auto generated add method for the array for convenience
   *
   * @param param com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord
   */
  public void addExtendedData(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord param) {
    if (localExtendedData == null) {
      localExtendedData = new com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] {};
    }

    // update the setting tracker
    localExtendedDataTracker = true;

    java.util.List list =
        org.apache.axis2.databinding.utils.ConverterUtil.toList(localExtendedData);
    list.add(param);
    this.localExtendedData =
        (com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[])
            list.toArray(new com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[list.size()]);
  }

  /** field for Notes */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventNote localNotes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localNotesTracker = false;

  public boolean isNotesSpecified() {
    return localNotesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventNote
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventNote getNotes() {
    return localNotes;
  }

  /**
   * Auto generated setter method
   *
   * @param param Notes
   */
  public void setNotes(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventNote param) {
    localNotesTracker = param != null;

    this.localNotes = param;
  }

  /** field for AttachedDocuments */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.AttachedDocuments_type6 localAttachedDocuments;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAttachedDocumentsTracker = false;

  public boolean isAttachedDocumentsSpecified() {
    return localAttachedDocumentsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.AttachedDocuments_type6
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.AttachedDocuments_type6 getAttachedDocuments() {
    return localAttachedDocuments;
  }

  /**
   * Auto generated setter method
   *
   * @param param AttachedDocuments
   */
  public void setAttachedDocuments(com.placecube.digitalplace.local.waste.bartec.axis.www.AttachedDocuments_type6 param) {
    localAttachedDocumentsTracker = param != null;

    this.localAttachedDocuments = param;
  }

  /** field for EventDate */
  protected java.util.Calendar localEventDate;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getEventDate() {
    return localEventDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param EventDate
   */
  public void setEventDate(java.util.Calendar param) {

    this.localEventDate = param;
  }

  /** field for Weight */
  protected java.math.BigDecimal localWeight;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getWeight() {
    return localWeight;
  }

  /**
   * Auto generated setter method
   *
   * @param param Weight
   */
  public void setWeight(java.math.BigDecimal param) {

    this.localWeight = param;
  }

  /** field for UPRN */
  protected java.math.BigDecimal localUPRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUPRN() {
    return localUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRN
   */
  public void setUPRN(java.math.BigDecimal param) {

    this.localUPRN = param;
  }

  /** field for Distance */
  protected java.math.BigDecimal localDistance;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getDistance() {
    return localDistance;
  }

  /**
   * Auto generated setter method
   *
   * @param param Distance
   */
  public void setDistance(java.math.BigDecimal param) {

    this.localDistance = param;
  }

  /** field for EventLocation */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint localEventLocation;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventLocationTracker = false;

  public boolean isEventLocationSpecified() {
    return localEventLocationTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint getEventLocation() {
    return localEventLocation;
  }

  /**
   * Auto generated setter method
   *
   * @param param EventLocation
   */
  public void setEventLocation(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint param) {
    localEventLocationTracker = param != null;

    this.localEventLocation = param;
  }

  /** field for Job */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtJob localJob;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localJobTracker = false;

  public boolean isJobSpecified() {
    return localJobTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtJob
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtJob getJob() {
    return localJob;
  }

  /**
   * Auto generated setter method
   *
   * @param param Job
   */
  public void setJob(com.placecube.digitalplace.local.waste.bartec.axis.www.CtJob param) {
    localJobTracker = param != null;

    this.localJob = param;
  }

  /** field for Features This was an Array! */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[] localFeatures;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFeaturesTracker = false;

  public boolean isFeaturesSpecified() {
    return localFeaturesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[]
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[] getFeatures() {
    return localFeatures;
  }

  /** validate the array for Features */
  protected void validateFeatures(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[] param) {}

  /**
   * Auto generated setter method
   *
   * @param param Features
   */
  public void setFeatures(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[] param) {

    validateFeatures(param);

    localFeaturesTracker = param != null;

    this.localFeatures = param;
  }

  /**
   * Auto generated add method for the array for convenience
   *
   * @param param com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature
   */
  public void addFeatures(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature param) {
    if (localFeatures == null) {
      localFeatures = new com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[] {};
    }

    // update the setting tracker
    localFeaturesTracker = true;

    java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localFeatures);
    list.add(param);
    this.localFeatures =
        (com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[])
            list.toArray(new com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[list.size()]);
  }

  /** field for Device */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice localDevice;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDeviceTracker = false;

  public boolean isDeviceSpecified() {
    return localDeviceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice getDevice() {
    return localDevice;
  }

  /**
   * Auto generated setter method
   *
   * @param param Device
   */
  public void setDevice(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice param) {
    localDeviceTracker = param != null;

    this.localDevice = param;
  }

  /** field for Crew */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew localCrew;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCrewTracker = false;

  public boolean isCrewSpecified() {
    return localCrewTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew getCrew() {
    return localCrew;
  }

  /**
   * Auto generated setter method
   *
   * @param param Crew
   */
  public void setCrew(com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew param) {
    localCrewTracker = param != null;

    this.localCrew = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ctEvent",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "ctEvent", xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ID", xmlWriter);

    if (localID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localID));
    }

    xmlWriter.writeEndElement();
    if (localEventClassTracker) {
      if (localEventClass != null) {
        for (int i = 0; i < localEventClass.length; i++) {
          if (localEventClass[i] != null) {
            localEventClass[i].serialize(
                new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventClass"),
                xmlWriter);
          } else {

            // we don't have to do any thing since minOccures is zero

          }
        }
      } else {

        throw new org.apache.axis2.databinding.ADBException("EventClass cannot be null!!");
      }
    }
    if (localEventTypeTracker) {
      if (localEventType == null) {
        throw new org.apache.axis2.databinding.ADBException("EventType cannot be null!!");
      }
      localEventType.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventType"), xmlWriter);
    }
    if (localEventSubTypeTracker) {
      if (localEventSubType != null) {
        for (int i = 0; i < localEventSubType.length; i++) {
          if (localEventSubType[i] != null) {
            localEventSubType[i].serialize(
                new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventSubType"),
                xmlWriter);
          } else {

            // we don't have to do any thing since minOccures is zero

          }
        }
      } else {

        throw new org.apache.axis2.databinding.ADBException("EventSubType cannot be null!!");
      }
    }
    if (localExtendedDataTracker) {
      if (localExtendedData != null) {
        for (int i = 0; i < localExtendedData.length; i++) {
          if (localExtendedData[i] != null) {
            localExtendedData[i].serialize(
                new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExtendedData"),
                xmlWriter);
          } else {

            // we don't have to do any thing since minOccures is zero

          }
        }
      } else {

        throw new org.apache.axis2.databinding.ADBException("ExtendedData cannot be null!!");
      }
    }
    if (localNotesTracker) {
      if (localNotes == null) {
        throw new org.apache.axis2.databinding.ADBException("Notes cannot be null!!");
      }
      localNotes.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Notes"), xmlWriter);
    }
    if (localAttachedDocumentsTracker) {
      if (localAttachedDocuments == null) {
        throw new org.apache.axis2.databinding.ADBException("AttachedDocuments cannot be null!!");
      }
      localAttachedDocuments.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "AttachedDocuments"),
          xmlWriter);
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "EventDate", xmlWriter);

    if (localEventDate == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("EventDate cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEventDate));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "Weight", xmlWriter);

    if (localWeight == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("Weight cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWeight));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "UPRN", xmlWriter);

    if (localUPRN == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("UPRN cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "Distance", xmlWriter);

    if (localDistance == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("Distance cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDistance));
    }

    xmlWriter.writeEndElement();
    if (localEventLocationTracker) {
      if (localEventLocation == null) {
        throw new org.apache.axis2.databinding.ADBException("EventLocation cannot be null!!");
      }
      localEventLocation.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventLocation"),
          xmlWriter);
    }
    if (localJobTracker) {
      if (localJob == null) {
        throw new org.apache.axis2.databinding.ADBException("Job cannot be null!!");
      }
      localJob.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Job"), xmlWriter);
    }
    if (localFeaturesTracker) {
      if (localFeatures != null) {
        for (int i = 0; i < localFeatures.length; i++) {
          if (localFeatures[i] != null) {
            localFeatures[i].serialize(
                new javax.xml.namespace.QName("http://www.bartec-systems.com", "Features"),
                xmlWriter);
          } else {

            // we don't have to do any thing since minOccures is zero

          }
        }
      } else {

        throw new org.apache.axis2.databinding.ADBException("Features cannot be null!!");
      }
    }
    if (localDeviceTracker) {
      if (localDevice == null) {
        throw new org.apache.axis2.databinding.ADBException("Device cannot be null!!");
      }
      localDevice.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Device"), xmlWriter);
    }
    if (localCrewTracker) {
      if (localCrew == null) {
        throw new org.apache.axis2.databinding.ADBException("Crew cannot be null!!");
      }
      localCrew.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Crew"), xmlWriter);
    }
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static CtEvent parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      CtEvent object = new CtEvent();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ctEvent".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (CtEvent)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        java.util.ArrayList list2 = new java.util.ArrayList();

        java.util.ArrayList list4 = new java.util.ArrayList();

        java.util.ArrayList list5 = new java.util.ArrayList();

        java.util.ArrayList list14 = new java.util.ArrayList();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventClass")
                .equals(reader.getName())) {

          // Process the array and step past its final element's end.

          list2.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass.Factory.parse(reader));

          // loop until we find a start element that is not part of this array
          boolean loopDone2 = false;
          while (!loopDone2) {
            // We should be at the end element, but make sure
            while (!reader.isEndElement()) reader.next();
            // Step out of this element
            reader.next();
            // Step to next element event.
            while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
            if (reader.isEndElement()) {
              // two continuous end elements means we are exiting the xml structure
              loopDone2 = true;
            } else {
              if (new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventClass")
                  .equals(reader.getName())) {
                list2.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass.Factory.parse(reader));

              } else {
                loopDone2 = true;
              }
            }
          }
          // call the converter utility  to convert and set the array

          object.setEventClass(
              (com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass[])
                  org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                      com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass.class, list2));

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventType")
                .equals(reader.getName())) {

          object.setEventType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventSubType")
                .equals(reader.getName())) {

          // Process the array and step past its final element's end.

          list4.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType.Factory.parse(reader));

          // loop until we find a start element that is not part of this array
          boolean loopDone4 = false;
          while (!loopDone4) {
            // We should be at the end element, but make sure
            while (!reader.isEndElement()) reader.next();
            // Step out of this element
            reader.next();
            // Step to next element event.
            while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
            if (reader.isEndElement()) {
              // two continuous end elements means we are exiting the xml structure
              loopDone4 = true;
            } else {
              if (new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventSubType")
                  .equals(reader.getName())) {
                list4.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType.Factory.parse(reader));

              } else {
                loopDone4 = true;
              }
            }
          }
          // call the converter utility  to convert and set the array

          object.setEventSubType(
              (com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[])
                  org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                      com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType.class, list4));

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExtendedData")
                .equals(reader.getName())) {

          // Process the array and step past its final element's end.

          list5.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord.Factory.parse(reader));

          // loop until we find a start element that is not part of this array
          boolean loopDone5 = false;
          while (!loopDone5) {
            // We should be at the end element, but make sure
            while (!reader.isEndElement()) reader.next();
            // Step out of this element
            reader.next();
            // Step to next element event.
            while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
            if (reader.isEndElement()) {
              // two continuous end elements means we are exiting the xml structure
              loopDone5 = true;
            } else {
              if (new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExtendedData")
                  .equals(reader.getName())) {
                list5.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord.Factory.parse(reader));

              } else {
                loopDone5 = true;
              }
            }
          }
          // call the converter utility  to convert and set the array

          object.setExtendedData(
              (com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[])
                  org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                      com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord.class, list5));

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Notes")
                .equals(reader.getName())) {

          object.setNotes(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventNote.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "AttachedDocuments")
                .equals(reader.getName())) {

          object.setAttachedDocuments(
              com.placecube.digitalplace.local.waste.bartec.axis.www.AttachedDocuments_type6.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "EventDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setEventDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Weight")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Weight" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWeight(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UPRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUPRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Distance")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Distance" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDistance(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventLocation")
                .equals(reader.getName())) {

          object.setEventLocation(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Job")
                .equals(reader.getName())) {

          object.setJob(com.placecube.digitalplace.local.waste.bartec.axis.www.CtJob.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Features")
                .equals(reader.getName())) {

          // Process the array and step past its final element's end.

          list14.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature.Factory.parse(reader));

          // loop until we find a start element that is not part of this array
          boolean loopDone14 = false;
          while (!loopDone14) {
            // We should be at the end element, but make sure
            while (!reader.isEndElement()) reader.next();
            // Step out of this element
            reader.next();
            // Step to next element event.
            while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
            if (reader.isEndElement()) {
              // two continuous end elements means we are exiting the xml structure
              loopDone14 = true;
            } else {
              if (new javax.xml.namespace.QName("http://www.bartec-systems.com", "Features")
                  .equals(reader.getName())) {
                list14.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature.Factory.parse(reader));

              } else {
                loopDone14 = true;
              }
            }
          }
          // call the converter utility  to convert and set the array

          object.setFeatures(
              (com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature[])
                  org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                      com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature.class, list14));

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Device")
                .equals(reader.getName())) {

          object.setDevice(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Crew")
                .equals(reader.getName())) {

          object.setCrew(com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
