/**
 * Job_type3.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd;

/** Job_type3 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Job_type3 extends com.placecube.digitalplace.local.waste.bartec.axis.www.CtJob
    implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = Job_type3
  Namespace URI = http://www.bartec-systems.com/Jobs_Detail_Get.xsd
  Namespace Prefix = ns90
  */

  /** field for Premises */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises localPremises;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPremisesTracker = false;

  public boolean isPremisesSpecified() {
    return localPremisesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises getPremises() {
    return localPremises;
  }

  /**
   * Auto generated setter method
   *
   * @param param Premises
   */
  public void setPremises(com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises param) {
    localPremisesTracker = param != null;

    this.localPremises = param;
  }

  /** field for JobReference */
  protected java.lang.String localJobReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localJobReferenceTracker = false;

  public boolean isJobReferenceSpecified() {
    return localJobReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getJobReference() {
    return localJobReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobReference
   */
  public void setJobReference(java.lang.String param) {
    localJobReferenceTracker = param != null;

    this.localJobReference = param;
  }

  /** field for SequenceNumber */
  protected int localSequenceNumber;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSequenceNumber() {
    return localSequenceNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param SequenceNumber
   */
  public void setSequenceNumber(int param) {

    this.localSequenceNumber = param;
  }

  /** field for ScheduledStart */
  protected java.util.Calendar localScheduledStart;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getScheduledStart() {
    return localScheduledStart;
  }

  /**
   * Auto generated setter method
   *
   * @param param ScheduledStart
   */
  public void setScheduledStart(java.util.Calendar param) {

    this.localScheduledStart = param;
  }

  /** field for ScheduledFinish */
  protected java.util.Calendar localScheduledFinish;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getScheduledFinish() {
    return localScheduledFinish;
  }

  /**
   * Auto generated setter method
   *
   * @param param ScheduledFinish
   */
  public void setScheduledFinish(java.util.Calendar param) {

    this.localScheduledFinish = param;
  }

  /** field for ScheduledMinutes */
  protected int localScheduledMinutes;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getScheduledMinutes() {
    return localScheduledMinutes;
  }

  /**
   * Auto generated setter method
   *
   * @param param ScheduledMinutes
   */
  public void setScheduledMinutes(int param) {

    this.localScheduledMinutes = param;
  }

  /** field for ScheduledCost */
  protected java.math.BigDecimal localScheduledCost;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localScheduledCostTracker = false;

  public boolean isScheduledCostSpecified() {
    return localScheduledCostTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getScheduledCost() {
    return localScheduledCost;
  }

  /**
   * Auto generated setter method
   *
   * @param param ScheduledCost
   */
  public void setScheduledCost(java.math.BigDecimal param) {
    localScheduledCostTracker = param != null;

    this.localScheduledCost = param;
  }

  /** field for ExpectedStart */
  protected java.util.Calendar localExpectedStart;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getExpectedStart() {
    return localExpectedStart;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExpectedStart
   */
  public void setExpectedStart(java.util.Calendar param) {

    this.localExpectedStart = param;
  }

  /** field for ExpectedFinish */
  protected java.util.Calendar localExpectedFinish;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getExpectedFinish() {
    return localExpectedFinish;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExpectedFinish
   */
  public void setExpectedFinish(java.util.Calendar param) {

    this.localExpectedFinish = param;
  }

  /** field for ActualStart */
  protected java.util.Calendar localActualStart;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getActualStart() {
    return localActualStart;
  }

  /**
   * Auto generated setter method
   *
   * @param param ActualStart
   */
  public void setActualStart(java.util.Calendar param) {

    this.localActualStart = param;
  }

  /** field for ActualFinish */
  protected java.util.Calendar localActualFinish;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getActualFinish() {
    return localActualFinish;
  }

  /**
   * Auto generated setter method
   *
   * @param param ActualFinish
   */
  public void setActualFinish(java.util.Calendar param) {

    this.localActualFinish = param;
  }

  /** field for ActualMinutes */
  protected int localActualMinutes;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getActualMinutes() {
    return localActualMinutes;
  }

  /**
   * Auto generated setter method
   *
   * @param param ActualMinutes
   */
  public void setActualMinutes(int param) {

    this.localActualMinutes = param;
  }

  /** field for ActualCost */
  protected java.math.BigDecimal localActualCost;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localActualCostTracker = false;

  public boolean isActualCostSpecified() {
    return localActualCostTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getActualCost() {
    return localActualCost;
  }

  /**
   * Auto generated setter method
   *
   * @param param ActualCost
   */
  public void setActualCost(java.math.BigDecimal param) {
    localActualCostTracker = param != null;

    this.localActualCost = param;
  }

  /** field for NoFurtherAction */
  protected boolean localNoFurtherAction;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getNoFurtherAction() {
    return localNoFurtherAction;
  }

  /**
   * Auto generated setter method
   *
   * @param param NoFurtherAction
   */
  public void setNoFurtherAction(boolean param) {

    this.localNoFurtherAction = param;
  }

  /** field for OnHold */
  protected boolean localOnHold;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getOnHold() {
    return localOnHold;
  }

  /**
   * Auto generated setter method
   *
   * @param param OnHold
   */
  public void setOnHold(boolean param) {

    this.localOnHold = param;
  }

  /** field for QAPass */
  protected boolean localQAPass;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getQAPass() {
    return localQAPass;
  }

  /**
   * Auto generated setter method
   *
   * @param param QAPass
   */
  public void setQAPass(boolean param) {

    this.localQAPass = param;
  }

  /** field for CtJobChoice_type1 */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtJobChoice_type1 localCtJobChoice_type1;

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtJobChoice_type1
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtJobChoice_type1 getCtJobChoice_type1() {
    return localCtJobChoice_type1;
  }

  /**
   * Auto generated setter method
   *
   * @param param CtJobChoice_type1
   */
  public void setCtJobChoice_type1(com.placecube.digitalplace.local.waste.bartec.axis.www.CtJobChoice_type1 param) {

    this.localCtJobChoice_type1 = param;
  }

  /** field for Tasks */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Tasks_type1 localTasks;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTasksTracker = false;

  public boolean isTasksSpecified() {
    return localTasksTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Tasks_type1
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Tasks_type1 getTasks() {
    return localTasks;
  }

  /**
   * Auto generated setter method
   *
   * @param param Tasks
   */
  public void setTasks(com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Tasks_type1 param) {
    localTasksTracker = param != null;

    this.localTasks = param;
  }

  /** field for AttachedDocuments */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.AttachedDocuments_type11
      localAttachedDocuments;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAttachedDocumentsTracker = false;

  public boolean isAttachedDocumentsSpecified() {
    return localAttachedDocumentsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.AttachedDocuments_type11
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.AttachedDocuments_type11
      getAttachedDocuments() {
    return localAttachedDocuments;
  }

  /**
   * Auto generated setter method
   *
   * @param param AttachedDocuments
   */
  public void setAttachedDocuments(
      com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.AttachedDocuments_type11 param) {
    localAttachedDocumentsTracker = param != null;

    this.localAttachedDocuments = param;
  }

  /** field for ExtendedData This was an Array! */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] localExtendedData;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExtendedDataTracker = false;

  public boolean isExtendedDataSpecified() {
    return localExtendedDataTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[]
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] getExtendedData() {
    return localExtendedData;
  }

  /** validate the array for ExtendedData */
  protected void validateExtendedData(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] param) {}

  /**
   * Auto generated setter method
   *
   * @param param ExtendedData
   */
  public void setExtendedData(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] param) {

    validateExtendedData(param);

    localExtendedDataTracker = param != null;

    this.localExtendedData = param;
  }

  /**
   * Auto generated add method for the array for convenience
   *
   * @param param com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord
   */
  public void addExtendedData(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord param) {
    if (localExtendedData == null) {
      localExtendedData = new com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[] {};
    }

    // update the setting tracker
    localExtendedDataTracker = true;

    java.util.List list =
        org.apache.axis2.databinding.utils.ConverterUtil.toList(localExtendedData);
    list.add(param);
    this.localExtendedData =
        (com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[])
            list.toArray(new com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[list.size()]);
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    java.lang.String namespacePrefix =
        registerPrefix(xmlWriter, "http://www.bartec-systems.com/Jobs_Detail_Get.xsd");
    if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
      writeAttribute(
          "xsi",
          "http://www.w3.org/2001/XMLSchema-instance",
          "type",
          namespacePrefix + ":Job_type3",
          xmlWriter);
    } else {
      writeAttribute(
          "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Job_type3", xmlWriter);
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ID", xmlWriter);

    if (localID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localID));
    }

    xmlWriter.writeEndElement();
    if (localNameTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Name", xmlWriter);

      if (localName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Name cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localName);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "UPRN", xmlWriter);

    if (localUPRN == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("UPRN cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "PhysicalUPRN", xmlWriter);

    if (localPhysicalUPRN == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("PhysicalUPRN cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPhysicalUPRN));
    }

    xmlWriter.writeEndElement();
    if (localDescriptionTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Description", xmlWriter);

      if (localDescription == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Description cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkPackTracker) {
      if (localWorkPack == null) {
        throw new org.apache.axis2.databinding.ADBException("WorkPack cannot be null!!");
      }
      localWorkPack.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "WorkPack"), xmlWriter);
    }
    if (localStatusTracker) {
      if (localStatus == null) {
        throw new org.apache.axis2.databinding.ADBException("Status cannot be null!!");
      }
      localStatus.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Status"), xmlWriter);
    }
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    if (localPremisesTracker) {
      if (localPremises == null) {
        throw new org.apache.axis2.databinding.ADBException("Premises cannot be null!!");
      }
      localPremises.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "Premises"),
          xmlWriter);
    }
    if (localJobReferenceTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
      writeStartElement(null, namespace, "JobReference", xmlWriter);

      if (localJobReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("JobReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localJobReference);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "SequenceNumber", xmlWriter);

    if (localSequenceNumber == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("SequenceNumber cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSequenceNumber));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "ScheduledStart", xmlWriter);

    if (localScheduledStart == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("ScheduledStart cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScheduledStart));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "ScheduledFinish", xmlWriter);

    if (localScheduledFinish == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("ScheduledFinish cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScheduledFinish));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "ScheduledMinutes", xmlWriter);

    if (localScheduledMinutes == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ScheduledMinutes cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScheduledMinutes));
    }

    xmlWriter.writeEndElement();
    if (localScheduledCostTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
      writeStartElement(null, namespace, "ScheduledCost", xmlWriter);

      if (localScheduledCost == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ScheduledCost cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScheduledCost));
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "ExpectedStart", xmlWriter);

    if (localExpectedStart == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("ExpectedStart cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpectedStart));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "ExpectedFinish", xmlWriter);

    if (localExpectedFinish == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("ExpectedFinish cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpectedFinish));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "ActualStart", xmlWriter);

    if (localActualStart == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("ActualStart cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualStart));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "ActualFinish", xmlWriter);

    if (localActualFinish == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("ActualFinish cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualFinish));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "ActualMinutes", xmlWriter);

    if (localActualMinutes == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ActualMinutes cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualMinutes));
    }

    xmlWriter.writeEndElement();
    if (localActualCostTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
      writeStartElement(null, namespace, "ActualCost", xmlWriter);

      if (localActualCost == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ActualCost cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualCost));
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "NoFurtherAction", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("NoFurtherAction cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNoFurtherAction));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "OnHold", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("OnHold cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnHold));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_Detail_Get.xsd";
    writeStartElement(null, namespace, "QAPass", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("QAPass cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQAPass));
    }

    xmlWriter.writeEndElement();

    if (localCtJobChoice_type1 == null) {
      throw new org.apache.axis2.databinding.ADBException("ctJobChoice_type1 cannot be null!!");
    }
    localCtJobChoice_type1.serialize(null, xmlWriter);
    if (localTasksTracker) {
      if (localTasks == null) {
        throw new org.apache.axis2.databinding.ADBException("Tasks cannot be null!!");
      }
      localTasks.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "Tasks"),
          xmlWriter);
    }
    if (localAttachedDocumentsTracker) {
      if (localAttachedDocuments == null) {
        throw new org.apache.axis2.databinding.ADBException("AttachedDocuments cannot be null!!");
      }
      localAttachedDocuments.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "AttachedDocuments"),
          xmlWriter);
    }
    if (localExtendedDataTracker) {
      if (localExtendedData != null) {
        for (int i = 0; i < localExtendedData.length; i++) {
          if (localExtendedData[i] != null) {
            localExtendedData[i].serialize(
                new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ExtendedData"),
                xmlWriter);
          } else {

            // we don't have to do any thing since minOccures is zero

          }
        }
      } else {

        throw new org.apache.axis2.databinding.ADBException("ExtendedData cannot be null!!");
      }
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/Jobs_Detail_Get.xsd")) {
      return "ns90";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Job_type3 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Job_type3 object = new Job_type3();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Job_type3".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Job_type3)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        java.util.ArrayList list28 = new java.util.ArrayList();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Name")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Name" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UPRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUPRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "PhysicalUPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PhysicalUPRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPhysicalUPRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Description")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Description" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDescription(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "WorkPack")
                .equals(reader.getName())) {

          object.setWorkPack(com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkPack.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Status")
                .equals(reader.getName())) {

          object.setStatus(com.placecube.digitalplace.local.waste.bartec.axis.www.CtJobStatus.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "Premises")
                .equals(reader.getName())) {

          object.setPremises(com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "JobReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "SequenceNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SequenceNumber" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSequenceNumber(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ScheduledStart")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ScheduledStart" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setScheduledStart(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ScheduledFinish")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ScheduledFinish" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setScheduledFinish(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ScheduledMinutes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ScheduledMinutes" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setScheduledMinutes(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ScheduledCost")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ScheduledCost" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setScheduledCost(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ExpectedStart")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExpectedStart" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExpectedStart(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ExpectedFinish")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExpectedFinish" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExpectedFinish(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ActualStart")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ActualStart" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setActualStart(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ActualFinish")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ActualFinish" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setActualFinish(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ActualMinutes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ActualMinutes" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setActualMinutes(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ActualCost")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ActualCost" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setActualCost(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "NoFurtherAction")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "NoFurtherAction" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setNoFurtherAction(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "OnHold")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "OnHold" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setOnHold(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "QAPass")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "QAPass" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setQAPass(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()) {

          object.setCtJobChoice_type1(
              com.placecube.digitalplace.local.waste.bartec.axis.www.CtJobChoice_type1.Factory.parse(reader));
        } // End of if for expected property start element

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "Tasks")
                .equals(reader.getName())) {

          object.setTasks(
              com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Tasks_type1.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "AttachedDocuments")
                .equals(reader.getName())) {

          object.setAttachedDocuments(
              com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.AttachedDocuments_type11.Factory.parse(
                  reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ExtendedData")
                .equals(reader.getName())) {

          // Process the array and step past its final element's end.

          list28.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord.Factory.parse(reader));

          // loop until we find a start element that is not part of this array
          boolean loopDone28 = false;
          while (!loopDone28) {
            // We should be at the end element, but make sure
            while (!reader.isEndElement()) reader.next();
            // Step out of this element
            reader.next();
            // Step to next element event.
            while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
            if (reader.isEndElement()) {
              // two continuous end elements means we are exiting the xml structure
              loopDone28 = true;
            } else {
              if (new javax.xml.namespace.QName(
                      "http://www.bartec-systems.com/Jobs_Detail_Get.xsd", "ExtendedData")
                  .equals(reader.getName())) {
                list28.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord.Factory.parse(reader));

              } else {
                loopDone28 = true;
              }
            }
          }
          // call the converter utility  to convert and set the array

          object.setExtendedData(
              (com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord[])
                  org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                      com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord.class, list28));

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
