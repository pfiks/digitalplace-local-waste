/**
 * CollectiveAPIWebServiceStub.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:03 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

import javax.xml.namespace.QName;

import org.apache.axis2.transport.TransportUtils;

/*
 *  CollectiveAPIWebServiceStub java implementation
 */

public class CollectiveAPIWebServiceStub extends org.apache.axis2.client.Stub implements CollectiveAPIWebService {

	private static int counter = 0;

	protected org.apache.axis2.description.AxisOperation[] _operations;
	private java.util.Map<org.apache.axis2.client.FaultMapKey, java.lang.String> faultExceptionClassNameMap = new java.util.HashMap<org.apache.axis2.client.FaultMapKey, java.lang.String>();
	// hashmaps to keep the fault mapping
	private java.util.Map<org.apache.axis2.client.FaultMapKey, java.lang.String> faultExceptionNameMap = new java.util.HashMap<org.apache.axis2.client.FaultMapKey, java.lang.String>();

	private java.util.Map<org.apache.axis2.client.FaultMapKey, java.lang.String> faultMessageMap = new java.util.HashMap<org.apache.axis2.client.FaultMapKey, java.lang.String>();

	private javax.xml.namespace.QName[] opNameArray = null;

	/** Default Constructor */
	public CollectiveAPIWebServiceStub() throws org.apache.axis2.AxisFault {

		this("https://collectiveapi.bartec-systems.com/API-R1604/CollectiveAPI.asmx");
	}

	/** Constructor taking the target endpoint */
	public CollectiveAPIWebServiceStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
		this(null, targetEndpoint);
	}

	/** Default Constructor */
	public CollectiveAPIWebServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {

		this(configurationContext, "https://collectiveapi.bartec-systems.com/API-R1604/CollectiveAPI.asmx");
	}

	/** Constructor that takes in a configContext */
	public CollectiveAPIWebServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
		this(configurationContext, targetEndpoint, false);
	}

	/** Constructor that takes in a configContext and useseperate listner */
	public CollectiveAPIWebServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext, java.lang.String targetEndpoint, boolean useSeparateListener)
			throws org.apache.axis2.AxisFault {
		// To populate AxisService
		populateAxisService();
		populateFaults();

		_serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext, _service);

		_serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(targetEndpoint));
		_serviceClient.getOptions().setUseSeparateListener(useSeparateListener);

		// Set the soap version
		_serviceClient.getOptions().setSoapVersionURI(org.apache.axiom.soap.SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
	}

	private static synchronized java.lang.String getUniqueSuffix() {
		// reset the counter if it is greater than 99999
		if (counter > 99999) {
			counter = 0;
		}
		counter = counter + 1;
		return java.lang.Long.toString(java.lang.System.currentTimeMillis()) + "_" + counter;
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#business_Address_Update
	 * @param business_Address_Update114
	 */
	@Override
	public Business_Address_UpdateResponse business_Address_Update(Business_Address_Update business_Address_Update114) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[57].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Business_Address_Update");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), business_Address_Update114,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "business_Address_Update")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Business_Address_Update"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Business_Address_UpdateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Business_Address_UpdateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Business_Address_Update"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Business_Address_Update"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Business_Address_Update"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#business_Document_Create
	 * @param business_Document_Create128
	 */
	@Override
	public Business_Document_CreateResponse business_Document_Create(Business_Document_Create business_Document_Create128) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[64].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Business_Document_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), business_Document_Create128,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "business_Document_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Business_Document_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Business_Document_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Business_Document_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Business_Document_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Business_Document_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Business_Document_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#businesses_Addresses_Get
	 * @param businesses_Addresses_Get4
	 */
	@Override
	public Businesses_Addresses_GetResponse businesses_Addresses_Get(Businesses_Addresses_Get businesses_Addresses_Get4) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Businesses_Addresses_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), businesses_Addresses_Get4,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Addresses_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Businesses_Addresses_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Businesses_Addresses_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Businesses_Addresses_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Addresses_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Addresses_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Addresses_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#businesses_Classes_Get
	 * @param businesses_Classes_Get106
	 */
	@Override
	public Businesses_Classes_GetResponse businesses_Classes_Get(Businesses_Classes_Get businesses_Classes_Get106) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[53].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Businesses_Classes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), businesses_Classes_Get106,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Classes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Businesses_Classes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Businesses_Classes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Businesses_Classes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Classes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Classes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Classes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#businesses_Documents_Get
	 * @param businesses_Documents_Get0
	 */
	@Override
	public Businesses_Documents_GetResponse businesses_Documents_Get(Businesses_Documents_Get businesses_Documents_Get0) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Businesses_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), businesses_Documents_Get0,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Businesses_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Businesses_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Businesses_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#businesses_Get
	 * @param businesses_Get134
	 */
	@Override
	public Businesses_GetResponse businesses_Get(Businesses_Get businesses_Get134) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[67].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Businesses_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), businesses_Get134,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "Businesses_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Businesses_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Businesses_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#businesses_Statuses_Get
	 * @param businesses_Statuses_Get92
	 */
	@Override
	public Businesses_Statuses_GetResponse businesses_Statuses_Get(Businesses_Statuses_Get businesses_Statuses_Get92) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[46].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Businesses_Statuses_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), businesses_Statuses_Get92,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Statuses_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Businesses_Statuses_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Businesses_Statuses_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Businesses_Statuses_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Statuses_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Statuses_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Statuses_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#businesses_SubStatuses_Get
	 * @param businesses_SubStatuses_Get46
	 */
	@Override
	public Businesses_SubStatuses_GetResponse businesses_SubStatuses_Get(Businesses_SubStatuses_Get businesses_SubStatuses_Get46) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[23].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Businesses_SubStatuses_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), businesses_SubStatuses_Get46,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_SubStatuses_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Businesses_SubStatuses_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Businesses_SubStatuses_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Businesses_SubStatuses_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_SubStatuses_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_SubStatuses_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_SubStatuses_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#businesses_Types_Get
	 * @param businesses_Types_Get158
	 */
	@Override
	public Businesses_Types_GetResponse businesses_Types_Get(Businesses_Types_Get businesses_Types_Get158) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[79].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Businesses_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), businesses_Types_Get158,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Businesses_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Businesses_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Businesses_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Businesses_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#cases_Documents_Get
	 * @param cases_Documents_Get42
	 */
	@Override
	public Cases_Documents_GetResponse cases_Documents_Get(Cases_Documents_Get cases_Documents_Get42) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[21].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Cases_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), cases_Documents_Get42,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "cases_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Cases_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Cases_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Cases_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Cases_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Cases_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Cases_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#crews_Get
	 * @param crews_Get24
	 */
	@Override
	public Crews_GetResponse crews_Get(Crews_Get crews_Get24) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[12].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Crews_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), crews_Get24, optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "crews_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Crews_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Crews_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Crews_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Crews_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Crews_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Crews_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Categories_Get
	 * @param features_Categories_Get62
	 */
	@Override
	public Features_Categories_GetResponse features_Categories_Get(Features_Categories_Get features_Categories_Get62) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[31].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Categories_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Categories_Get62,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Categories_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Categories_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Categories_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Categories_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Categories_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Categories_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Categories_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Classes_Get
	 * @param features_Classes_Get2
	 */
	@Override
	public Features_Classes_GetResponse features_Classes_Get(Features_Classes_Get features_Classes_Get2) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Classes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Classes_Get2,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Classes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Classes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Classes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Classes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Classes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Classes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Classes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Colours_Get
	 * @param features_Colours_Get154
	 */
	@Override
	public Features_Colours_GetResponse features_Colours_Get(Features_Colours_Get features_Colours_Get154) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[77].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Colours_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Colours_Get154,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Colours_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Colours_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Colours_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Colours_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Colours_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Colours_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Colours_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Conditions_Get
	 * @param features_Conditions_Get20
	 */
	@Override
	public Features_Conditions_GetResponse features_Conditions_Get(Features_Conditions_Get features_Conditions_Get20) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[10].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Conditions_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Conditions_Get20,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Conditions_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Conditions_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Conditions_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Conditions_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Conditions_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Conditions_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Conditions_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Get
	 * @param features_Get142
	 */
	@Override
	public Features_GetResponse features_Get(Features_Get features_Get142) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[71].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Get142,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Manufacturers_Get
	 * @param features_Manufacturers_Get68
	 */
	@Override
	public Features_Manufacturers_GetResponse features_Manufacturers_Get(Features_Manufacturers_Get features_Manufacturers_Get68) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[34].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Manufacturers_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Manufacturers_Get68,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Manufacturers_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Manufacturers_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Manufacturers_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Manufacturers_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Manufacturers_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Manufacturers_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Manufacturers_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Notes_Get
	 * @param features_Notes_Get44
	 */
	@Override
	public Features_Notes_GetResponse features_Notes_Get(Features_Notes_Get features_Notes_Get44) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[22].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Notes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Notes_Get44,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Notes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Notes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Notes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Notes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Notes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Notes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Notes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Schedules_Get
	 * @param features_Schedules_Get90
	 */
	@Override
	public Features_Schedules_GetResponse features_Schedules_Get(Features_Schedules_Get features_Schedules_Get90) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[45].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Schedules_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Schedules_Get90,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Schedules_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Schedules_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Schedules_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Schedules_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Schedules_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Schedules_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Schedules_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Schedules_Get2
	 * @param features_Schedules_Get284
	 */
	@Override
	public Features_Schedules_Get2Response features_Schedules_Get2(Features_Schedules_Get2 features_Schedules_Get284) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[42].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Schedules_Get2");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Schedules_Get284,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Schedules_Get2")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Schedules_Get2"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Schedules_Get2Response.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Schedules_Get2Response) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Schedules_Get2"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Schedules_Get2"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Schedules_Get2"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Statuses_Get
	 * @param features_Statuses_Get138
	 */
	@Override
	public Features_Statuses_GetResponse features_Statuses_Get(Features_Statuses_Get features_Statuses_Get138) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[69].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Statuses_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Statuses_Get138,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Statuses_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Statuses_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Statuses_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Statuses_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Statuses_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Statuses_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Statuses_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#features_Types_Get
	 * @param features_Types_Get54
	 */
	@Override
	public Features_Types_GetResponse features_Types_Get(Features_Types_Get features_Types_Get54) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[27].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Features_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), features_Types_Get54,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Features_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Features_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Features_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Features_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#inspections_Classes_Get
	 * @param inspections_Classes_Get96
	 */
	@Override
	public Inspections_Classes_GetResponse inspections_Classes_Get(Inspections_Classes_Get inspections_Classes_Get96) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[48].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Inspections_Classes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), inspections_Classes_Get96,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Classes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Inspections_Classes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Inspections_Classes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Inspections_Classes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Classes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Classes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Classes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#inspections_Documents_Get
	 * @param inspections_Documents_Get144
	 */
	@Override
	public Inspections_Documents_GetResponse inspections_Documents_Get(Inspections_Documents_Get inspections_Documents_Get144) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[72].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Inspections_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), inspections_Documents_Get144,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Inspections_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Inspections_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Inspections_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#inspections_Get
	 * @param inspections_Get32
	 */
	@Override
	public Inspections_GetResponse inspections_Get(Inspections_Get inspections_Get32) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[16].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Inspections_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), inspections_Get32,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "Inspections_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Inspections_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Inspections_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#inspections_Statuses_Get
	 * @param inspections_Statuses_Get174
	 */
	@Override
	public Inspections_Statuses_GetResponse inspections_Statuses_Get(Inspections_Statuses_Get inspections_Statuses_Get174) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[87].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Inspections_Statuses_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), inspections_Statuses_Get174,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Statuses_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Inspections_Statuses_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Inspections_Statuses_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Inspections_Statuses_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Statuses_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Statuses_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Statuses_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#inspections_Types_Get
	 * @param inspections_Types_Get116
	 */
	@Override
	public Inspections_Types_GetResponse inspections_Types_Get(Inspections_Types_Get inspections_Types_Get116) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[58].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Inspections_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), inspections_Types_Get116,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Inspections_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Inspections_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Inspections_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Inspections_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#jobs_Close
	 * @param jobs_Close58
	 */
	@Override
	public Jobs_CloseResponse jobs_Close(Jobs_Close jobs_Close58) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[29].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Jobs_Close");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), jobs_Close58, optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_Close")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Jobs_Close"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Jobs_CloseResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Jobs_CloseResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Close"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Close"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Close"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#jobs_Detail_Get
	 * @param jobs_Detail_Get130
	 */
	@Override
	public Jobs_Detail_GetResponse jobs_Detail_Get(Jobs_Detail_Get jobs_Detail_Get130) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[65].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Jobs_Detail_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), jobs_Detail_Get130,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_Detail_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "Jobs_Detail_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Jobs_Detail_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Jobs_Detail_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Detail_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Detail_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Detail_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#jobs_Documents_Get
	 * @param jobs_Documents_Get146
	 */
	@Override
	public Jobs_Documents_GetResponse jobs_Documents_Get(Jobs_Documents_Get jobs_Documents_Get146) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[73].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Jobs_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), jobs_Documents_Get146,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Jobs_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Jobs_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Jobs_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#jobs_FeatureScheduleDates_Get
	 * @param jobs_FeatureScheduleDates_Get80
	 */
	@Override
	public Jobs_FeatureScheduleDates_GetResponse jobs_FeatureScheduleDates_Get(Jobs_FeatureScheduleDates_Get jobs_FeatureScheduleDates_Get80) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[40].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Jobs_FeatureScheduleDates_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), jobs_FeatureScheduleDates_Get80,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_FeatureScheduleDates_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Jobs_FeatureScheduleDates_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Jobs_FeatureScheduleDates_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Jobs_FeatureScheduleDates_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_FeatureScheduleDates_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_FeatureScheduleDates_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_FeatureScheduleDates_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#jobs_Get
	 * @param jobs_Get56
	 */
	@Override
	public Jobs_GetResponse jobs_Get(Jobs_Get jobs_Get56) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[28].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Jobs_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), jobs_Get56, optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Jobs_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Jobs_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Jobs_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#jobs_WorkScheduleDates_Get
	 * @param jobs_WorkScheduleDates_Get112
	 */
	@Override
	public Jobs_WorkScheduleDates_GetResponse jobs_WorkScheduleDates_Get(Jobs_WorkScheduleDates_Get jobs_WorkScheduleDates_Get112) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[56].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Jobs_WorkScheduleDates_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), jobs_WorkScheduleDates_Get112,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_WorkScheduleDates_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Jobs_WorkScheduleDates_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Jobs_WorkScheduleDates_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Jobs_WorkScheduleDates_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_WorkScheduleDates_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_WorkScheduleDates_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Jobs_WorkScheduleDates_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#licences_Classes_Get
	 * @param licences_Classes_Get98
	 */
	@Override
	public Licences_Classes_GetResponse licences_Classes_Get(Licences_Classes_Get licences_Classes_Get98) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[49].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Licences_Classes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), licences_Classes_Get98,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Classes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Licences_Classes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Licences_Classes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Licences_Classes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Classes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Classes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Classes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#licences_Documents_Get
	 * @param licences_Documents_Get156
	 */
	@Override
	public Licences_Documents_GetResponse licences_Documents_Get(Licences_Documents_Get licences_Documents_Get156) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[78].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Licences_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), licences_Documents_Get156,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Licences_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Licences_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Licences_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#licences_Get
	 * @param licences_Get64
	 */
	@Override
	public Licences_GetResponse licences_Get(Licences_Get licences_Get64) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[32].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Licences_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), licences_Get64,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "Licences_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Licences_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Licences_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#licences_Statuses_Get
	 * @param licences_Statuses_Get164
	 */
	@Override
	public Licences_Statuses_GetResponse licences_Statuses_Get(Licences_Statuses_Get licences_Statuses_Get164) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[82].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Licences_Statuses_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), licences_Statuses_Get164,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Statuses_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Licences_Statuses_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Licences_Statuses_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Licences_Statuses_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Statuses_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Statuses_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Statuses_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#licences_Types_Get
	 * @param licences_Types_Get166
	 */
	@Override
	public Licences_Types_GetResponse licences_Types_Get(Licences_Types_Get licences_Types_Get166) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[83].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Licences_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), licences_Types_Get166,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Licences_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Licences_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Licences_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Licences_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Attribute_Create
	 * @param premises_Attribute_Create172
	 */
	@Override
	public Premises_Attribute_CreateResponse premises_Attribute_Create(Premises_Attribute_Create premises_Attribute_Create172) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[86].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Attribute_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Attribute_Create172,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Attribute_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Attribute_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Attribute_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Attribute_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attribute_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attribute_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attribute_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Attribute_Update
	 * @param premises_Attribute_Update108
	 */
	@Override
	public Premises_Attribute_UpdateResponse premises_Attribute_Update(Premises_Attribute_Update premises_Attribute_Update108) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[54].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Attribute_Update");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Attribute_Update108,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Attribute_Update")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Attribute_Update"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Attribute_UpdateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Attribute_UpdateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attribute_Update"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attribute_Update"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attribute_Update"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_AttributeDefinitions_Get
	 * @param premises_AttributeDefinitions_Get140
	 */
	@Override
	public Premises_AttributeDefinitions_GetResponse premises_AttributeDefinitions_Get(Premises_AttributeDefinitions_Get premises_AttributeDefinitions_Get140) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[70].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_AttributeDefinitions_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_AttributeDefinitions_Get140,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_AttributeDefinitions_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_AttributeDefinitions_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_AttributeDefinitions_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_AttributeDefinitions_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_AttributeDefinitions_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_AttributeDefinitions_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_AttributeDefinitions_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Attributes_Delete
	 * @param premises_Attributes_Delete136
	 */
	@Override
	public Premises_Attributes_DeleteResponse premises_Attributes_Delete(Premises_Attributes_Delete premises_Attributes_Delete136) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[68].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Attributes_Delete");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Attributes_Delete136,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Attributes_Delete")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Attributes_Delete"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Attributes_DeleteResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Attributes_DeleteResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attributes_Delete"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attributes_Delete"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attributes_Delete"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Attributes_Get
	 * @param premises_Attributes_Get152
	 */
	@Override
	public Premises_Attributes_GetResponse premises_Attributes_Get(Premises_Attributes_Get premises_Attributes_Get152) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[76].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Attributes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Attributes_Get152,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Attributes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Attributes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Attributes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Attributes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attributes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attributes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Attributes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Detail_Get
	 * @param premises_Detail_Get66
	 */
	@Override
	public Premises_Detail_GetResponse premises_Detail_Get(Premises_Detail_Get premises_Detail_Get66) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[33].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Detail_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Detail_Get66,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Detail_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Detail_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Detail_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Detail_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Detail_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Detail_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Detail_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Document_Create
	 * @param premises_Document_Create148
	 */
	@Override
	public Premises_Document_CreateResponse premises_Document_Create(Premises_Document_Create premises_Document_Create148) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[74].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Document_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Document_Create148,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Document_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Document_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Document_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Document_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Document_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Document_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Document_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Documents_Get
	 * @param premises_Documents_Get48
	 */
	@Override
	public Premises_Documents_GetResponse premises_Documents_Get(Premises_Documents_Get premises_Documents_Get48) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[24].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Documents_Get48,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Event_Create
	 * @param premises_Event_Create182
	 */
	@Override
	public Premises_Event_CreateResponse premises_Event_Create(Premises_Event_Create premises_Event_Create182) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[91].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Event_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Event_Create182,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Event_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Event_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Event_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Event_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Event_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Event_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Event_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Events_Classes_Get
	 * @param premises_Events_Classes_Get70
	 */
	@Override
	public Premises_Events_Classes_GetResponse premises_Events_Classes_Get(Premises_Events_Classes_Get premises_Events_Classes_Get70) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[35].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Events_Classes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Events_Classes_Get70,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Classes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Events_Classes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Events_Classes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Events_Classes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Classes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Classes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Classes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Events_Document_Create
	 * @param premises_Events_Document_Create104
	 */
	@Override
	public Premises_Events_Document_CreateResponse premises_Events_Document_Create(Premises_Events_Document_Create premises_Events_Document_Create104) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[52].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Events_Document_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Events_Document_Create104,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Document_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Events_Document_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Events_Document_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Events_Document_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Document_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Document_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Document_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Events_Documents_Get
	 * @param premises_Events_Documents_Get76
	 */
	@Override
	public Premises_Events_Documents_GetResponse premises_Events_Documents_Get(Premises_Events_Documents_Get premises_Events_Documents_Get76) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[38].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Events_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Events_Documents_Get76,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Events_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Events_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Events_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Events_Get
	 * @param premises_Events_Get6
	 */
	@Override
	public Premises_Events_GetResponse premises_Events_Get(Premises_Events_Get premises_Events_Get6) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[3].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Events_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Events_Get6,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Events_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Events_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Events_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Events_Types_Get
	 * @param premises_Events_Types_Get102
	 */
	@Override
	public Premises_Events_Types_GetResponse premises_Events_Types_Get(Premises_Events_Types_Get premises_Events_Types_Get102) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[51].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Events_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Events_Types_Get102,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Events_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_Events_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_Events_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Events_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_FutureWorkpacks_Get
	 * @param premises_FutureWorkpacks_Get8
	 */
	@Override
	public Premises_FutureWorkpacks_GetResponse premises_FutureWorkpacks_Get(Premises_FutureWorkpacks_Get premises_FutureWorkpacks_Get8) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[4].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_FutureWorkpacks_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_FutureWorkpacks_Get8,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_FutureWorkpacks_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_FutureWorkpacks_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_FutureWorkpacks_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_FutureWorkpacks_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_FutureWorkpacks_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_FutureWorkpacks_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_FutureWorkpacks_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#premises_Get
	 * @param premises_Get10
	 */
	@Override
	public Premises_GetResponse premises_Get(Premises_Get premises_Get10) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[5].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Premises_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), premises_Get10,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Premises_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Premises_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Premises_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#resources_Documents_Get
	 * @param resources_Documents_Get200
	 */
	@Override
	public Resources_Documents_GetResponse resources_Documents_Get(Resources_Documents_Get resources_Documents_Get200) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[100].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Resources_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), resources_Documents_Get200,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "resources_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Resources_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Resources_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Resources_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#resources_Get
	 * @param resources_Get188
	 */
	@Override
	public Resources_GetResponse resources_Get(Resources_Get resources_Get188) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[94].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Resources_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), resources_Get188,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "resources_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "Resources_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Resources_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Resources_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#resources_Types_Get
	 * @param resources_Types_Get94
	 */
	@Override
	public Resources_Types_GetResponse resources_Types_Get(Resources_Types_Get resources_Types_Get94) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[47].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Resources_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), resources_Types_Get94,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "resources_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Resources_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Resources_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Resources_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Resources_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Appointment_Reservation_Cancel
	 * @param serviceRequest_Appointment_Reservation_Cancel190
	 */
	@Override
	public ServiceRequest_Appointment_Reservation_CancelResponse serviceRequest_Appointment_Reservation_Cancel(
			ServiceRequest_Appointment_Reservation_Cancel serviceRequest_Appointment_Reservation_Cancel190) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[95].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Appointment_Reservation_Cancel");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Appointment_Reservation_Cancel190,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Appointment_Reservation_Cancel")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Appointment_Reservation_Cancel"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_Appointment_Reservation_CancelResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_Appointment_Reservation_CancelResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Cancel"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap
								.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Cancel"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Cancel"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Appointment_Reservation_Create
	 * @param serviceRequest_Appointment_Reservation_Create100
	 */
	@Override
	public ServiceRequest_Appointment_Reservation_CreateResponse serviceRequest_Appointment_Reservation_Create(
			ServiceRequest_Appointment_Reservation_Create serviceRequest_Appointment_Reservation_Create100) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[50].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Appointment_Reservation_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Appointment_Reservation_Create100,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Appointment_Reservation_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Appointment_Reservation_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_Appointment_Reservation_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_Appointment_Reservation_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap
								.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Appointment_Reservation_Extend
	 * @param serviceRequest_Appointment_Reservation_Extend74
	 */
	@Override
	public ServiceRequest_Appointment_Reservation_ExtendResponse serviceRequest_Appointment_Reservation_Extend(
			ServiceRequest_Appointment_Reservation_Extend serviceRequest_Appointment_Reservation_Extend74) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[37].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Appointment_Reservation_Extend");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Appointment_Reservation_Extend74,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Appointment_Reservation_Extend")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Appointment_Reservation_Extend"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_Appointment_Reservation_ExtendResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_Appointment_Reservation_ExtendResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Extend"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap
								.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Extend"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Reservation_Extend"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Appointment_Slot_Reserve
	 * @param serviceRequest_Appointment_Slot_Reserve162
	 */
	@Override
	public ServiceRequest_Appointment_Slot_ReserveResponse serviceRequest_Appointment_Slot_Reserve(ServiceRequest_Appointment_Slot_Reserve serviceRequest_Appointment_Slot_Reserve162)
			throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[81].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Appointment_Slot_Reserve");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Appointment_Slot_Reserve162,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Appointment_Slot_Reserve")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Appointment_Slot_Reserve"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_Appointment_Slot_ReserveResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_Appointment_Slot_ReserveResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Slot_Reserve"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Slot_Reserve"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Appointment_Slot_Reserve"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Cancel
	 * @param serviceRequest_Cancel178
	 */
	@Override
	public ServiceRequest_CancelResponse serviceRequest_Cancel(ServiceRequest_Cancel serviceRequest_Cancel178) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[89].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Cancel");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Cancel178,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Cancel")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Cancel"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_CancelResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_CancelResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Cancel"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Cancel"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Cancel"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Create
	 * @param serviceRequest_Create22
	 */
	@Override
	public ServiceRequest_CreateResponse serviceRequest_Create(ServiceRequest_Create serviceRequest_Create22) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[11].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Create22,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Document_Create
	 * @param serviceRequest_Document_Create26
	 */
	@Override
	public ServiceRequest_Document_CreateResponse serviceRequest_Document_Create(ServiceRequest_Document_Create serviceRequest_Document_Create26) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[13].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Document_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Document_Create26,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Document_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Document_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_Document_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_Document_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Document_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Document_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Document_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Note_Create
	 * @param serviceRequest_Note_Create126
	 */
	@Override
	public ServiceRequest_Note_CreateResponse serviceRequest_Note_Create(ServiceRequest_Note_Create serviceRequest_Note_Create126) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[63].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Note_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Note_Create126,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Note_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Note_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_Note_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_Note_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Note_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Note_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Note_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Status_Set
	 * @param serviceRequest_Status_Set202
	 */
	@Override
	public ServiceRequest_Status_SetResponse serviceRequest_Status_Set(ServiceRequest_Status_Set serviceRequest_Status_Set202) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[101].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Status_Set");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Status_Set202,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Status_Set")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Status_Set"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_Status_SetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_Status_SetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Status_Set"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Status_Set"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Status_Set"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequest_Update
	 * @param serviceRequest_Update196
	 */
	@Override
	public ServiceRequest_UpdateResponse serviceRequest_Update(ServiceRequest_Update serviceRequest_Update196) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[98].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequest_Update");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequest_Update196,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Update")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Update"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequest_UpdateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequest_UpdateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Update"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Update"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequest_Update"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Appointments_Availability_Get
	 * @param serviceRequests_Appointments_Availability_Get88
	 */
	@Override
	public ServiceRequests_Appointments_Availability_GetResponse serviceRequests_Appointments_Availability_Get(
			ServiceRequests_Appointments_Availability_Get serviceRequests_Appointments_Availability_Get88) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[44].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Appointments_Availability_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Appointments_Availability_Get88,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Appointments_Availability_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Appointments_Availability_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Appointments_Availability_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Appointments_Availability_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Appointments_Availability_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap
								.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Appointments_Availability_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Appointments_Availability_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Classes_Get
	 * @param serviceRequests_Classes_Get194
	 */
	@Override
	public ServiceRequests_Classes_GetResponse serviceRequests_Classes_Get(ServiceRequests_Classes_Get serviceRequests_Classes_Get194) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[97].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Classes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Classes_Get194,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Classes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Classes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Classes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Classes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Classes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Classes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Classes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Detail_Get
	 * @param serviceRequests_Detail_Get192
	 */
	@Override
	public ServiceRequests_Detail_GetResponse serviceRequests_Detail_Get(ServiceRequests_Detail_Get serviceRequests_Detail_Get192) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[96].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Detail_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Detail_Get192,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Detail_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Detail_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Detail_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Detail_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Detail_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Detail_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Detail_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Documents_Get
	 * @param serviceRequests_Documents_Get186
	 */
	@Override
	public ServiceRequests_Documents_GetResponse serviceRequests_Documents_Get(ServiceRequests_Documents_Get serviceRequests_Documents_Get186) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[93].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Documents_Get186,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Get
	 * @param serviceRequests_Get170
	 */
	@Override
	public ServiceRequests_GetResponse serviceRequests_Get(ServiceRequests_Get serviceRequests_Get170) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[85].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Get170,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_History_Get
	 * @param serviceRequests_History_Get124
	 */
	@Override
	public ServiceRequests_History_GetResponse serviceRequests_History_Get(ServiceRequests_History_Get serviceRequests_History_Get124) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[62].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_History_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_History_Get124,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_History_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_History_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_History_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_History_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_History_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_History_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_History_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Notes_Get
	 * @param serviceRequests_Notes_Get40
	 */
	@Override
	public ServiceRequests_Notes_GetResponse serviceRequests_Notes_Get(ServiceRequests_Notes_Get serviceRequests_Notes_Get40) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[20].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Notes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Notes_Get40,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Notes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Notes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Notes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Notes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Notes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Notes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Notes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Notes_Types_Get
	 * @param serviceRequests_Notes_Types_Get180
	 */
	@Override
	public ServiceRequests_Notes_Types_GetResponse serviceRequests_Notes_Types_Get(ServiceRequests_Notes_Types_Get serviceRequests_Notes_Types_Get180) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[90].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Notes_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Notes_Types_Get180,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Notes_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Notes_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Notes_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Notes_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Notes_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Notes_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Notes_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_SLAs_Get
	 * @param serviceRequests_SLAs_Get12
	 */
	@Override
	public ServiceRequests_SLAs_GetResponse serviceRequests_SLAs_Get(ServiceRequests_SLAs_Get serviceRequests_SLAs_Get12) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[6].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_SLAs_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_SLAs_Get12,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_SLAs_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_SLAs_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_SLAs_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_SLAs_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_SLAs_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_SLAs_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_SLAs_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Statuses_Get
	 * @param serviceRequests_Statuses_Get28
	 */
	@Override
	public ServiceRequests_Statuses_GetResponse serviceRequests_Statuses_Get(ServiceRequests_Statuses_Get serviceRequests_Statuses_Get28) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[14].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Statuses_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Statuses_Get28,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Statuses_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Statuses_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Statuses_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Statuses_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Statuses_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Statuses_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Statuses_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Types_Get
	 * @param serviceRequests_Types_Get60
	 */
	@Override
	public ServiceRequests_Types_GetResponse serviceRequests_Types_Get(ServiceRequests_Types_Get serviceRequests_Types_Get60) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[30].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Types_Get60,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#serviceRequests_Updates_Get
	 * @param serviceRequests_Updates_Get52
	 */
	@Override
	public ServiceRequests_Updates_GetResponse serviceRequests_Updates_Get(ServiceRequests_Updates_Get serviceRequests_Updates_Get52) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[26].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/ServiceRequests_Updates_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), serviceRequests_Updates_Get52,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Updates_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Updates_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), ServiceRequests_Updates_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (ServiceRequests_Updates_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Updates_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Updates_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "ServiceRequests_Updates_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#site_Document_Create
	 * @param site_Document_Create38
	 */
	@Override
	public Site_Document_CreateResponse site_Document_Create(Site_Document_Create site_Document_Create38) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[19].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Site_Document_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), site_Document_Create38,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "site_Document_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Site_Document_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Site_Document_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Site_Document_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Site_Document_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Site_Document_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Site_Document_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#sites_Documents_Get
	 * @param sites_Documents_Get72
	 */
	@Override
	public Sites_Documents_GetResponse sites_Documents_Get(Sites_Documents_Get sites_Documents_Get72) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[36].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Sites_Documents_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), sites_Documents_Get72,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "sites_Documents_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Sites_Documents_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Sites_Documents_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Sites_Documents_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Documents_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Documents_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Documents_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#sites_Documents_GetAll
	 * @param sites_Documents_GetAll150
	 */
	@Override
	public Sites_Documents_GetAllResponse sites_Documents_GetAll(Sites_Documents_GetAll sites_Documents_GetAll150) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[75].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Sites_Documents_GetAll");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), sites_Documents_GetAll150,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "sites_Documents_GetAll")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Sites_Documents_GetAll"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Sites_Documents_GetAllResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Sites_Documents_GetAllResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Documents_GetAll"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Documents_GetAll"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Documents_GetAll"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#sites_Get
	 * @param sites_Get168
	 */
	@Override
	public Sites_GetResponse sites_Get(Sites_Get sites_Get168) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[84].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Sites_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), sites_Get168, optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "sites_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Sites_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Sites_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Sites_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Sites_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#streets_Attributes_Get
	 * @param streets_Attributes_Get16
	 */
	@Override
	public Streets_Attributes_GetResponse streets_Attributes_Get(Streets_Attributes_Get streets_Attributes_Get16) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[8].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Streets_Attributes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), streets_Attributes_Get16,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Attributes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Streets_Attributes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Streets_Attributes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Streets_Attributes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Attributes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Attributes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Attributes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#streets_Cancellations_Get
	 * @param streets_Cancellations_Get184
	 */
	@Override
	public Streets_Cancellations_GetResponse streets_Cancellations_Get(Streets_Cancellations_Get streets_Cancellations_Get184) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[92].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Streets_Cancellations_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), streets_Cancellations_Get184,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Cancellations_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Streets_Cancellations_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Streets_Cancellations_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Streets_Cancellations_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Cancellations_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Cancellations_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Cancellations_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#streets_Detail_Get
	 * @param streets_Detail_Get50
	 */
	@Override
	public Streets_Detail_GetResponse streets_Detail_Get(Streets_Detail_Get streets_Detail_Get50) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[25].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Streets_Detail_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), streets_Detail_Get50,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Detail_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Streets_Detail_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Streets_Detail_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Streets_Detail_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Detail_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Detail_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Detail_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#streets_Events_Get
	 * @param streets_Events_Get198
	 */
	@Override
	public Streets_Events_GetResponse streets_Events_Get(Streets_Events_Get streets_Events_Get198) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[99].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Streets_Events_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), streets_Events_Get198,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Events_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Streets_Events_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Streets_Events_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Streets_Events_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Events_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Events_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Events_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#streets_Events_Types_Get
	 * @param streets_Events_Types_Get160
	 */
	@Override
	public Streets_Events_Types_GetResponse streets_Events_Types_Get(Streets_Events_Types_Get streets_Events_Types_Get160) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[80].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Streets_Events_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), streets_Events_Types_Get160,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Events_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Streets_Events_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Streets_Events_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Streets_Events_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Events_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Events_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Events_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#streets_Get
	 * @param streets_Get36
	 */
	@Override
	public Streets_GetResponse streets_Get(Streets_Get streets_Get36) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[18].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Streets_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), streets_Get36, optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Streets_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Streets_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Streets_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Streets_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#system_ExtendedDataDefinitions_Get
	 * @param system_ExtendedDataDefinitions_Get18
	 */
	@Override
	public System_ExtendedDataDefinitions_GetResponse system_ExtendedDataDefinitions_Get(System_ExtendedDataDefinitions_Get system_ExtendedDataDefinitions_Get18) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[9].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/System_ExtendedDataDefinitions_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), system_ExtendedDataDefinitions_Get18,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "system_ExtendedDataDefinitions_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "System_ExtendedDataDefinitions_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), System_ExtendedDataDefinitions_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (System_ExtendedDataDefinitions_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_ExtendedDataDefinitions_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_ExtendedDataDefinitions_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_ExtendedDataDefinitions_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#system_LandTypes_Get
	 * @param system_LandTypes_Get176
	 */
	@Override
	public System_LandTypes_GetResponse system_LandTypes_Get(System_LandTypes_Get system_LandTypes_Get176) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[88].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/System_LandTypes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), system_LandTypes_Get176,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "system_LandTypes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "System_LandTypes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), System_LandTypes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (System_LandTypes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_LandTypes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_LandTypes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_LandTypes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#system_WasteTypes_Get
	 * @param system_WasteTypes_Get132
	 */
	@Override
	public System_WasteTypes_GetResponse system_WasteTypes_Get(System_WasteTypes_Get system_WasteTypes_Get132) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[66].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/System_WasteTypes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), system_WasteTypes_Get132,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "system_WasteTypes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "System_WasteTypes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), System_WasteTypes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (System_WasteTypes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_WasteTypes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_WasteTypes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "System_WasteTypes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#vehicle_InspectionForms_Get
	 * @param vehicle_InspectionForms_Get110
	 */
	@Override
	public Vehicle_InspectionForms_GetResponse vehicle_InspectionForms_Get(Vehicle_InspectionForms_Get vehicle_InspectionForms_Get110) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[55].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Vehicle_InspectionForms_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), vehicle_InspectionForms_Get110,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "vehicle_InspectionForms_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Vehicle_InspectionForms_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Vehicle_InspectionForms_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Vehicle_InspectionForms_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_InspectionForms_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_InspectionForms_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_InspectionForms_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#vehicle_InspectionResults_Get
	 * @param vehicle_InspectionResults_Get118
	 */
	@Override
	public Vehicle_InspectionResults_GetResponse vehicle_InspectionResults_Get(Vehicle_InspectionResults_Get vehicle_InspectionResults_Get118) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[59].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Vehicle_InspectionResults_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), vehicle_InspectionResults_Get118,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "vehicle_InspectionResults_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Vehicle_InspectionResults_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Vehicle_InspectionResults_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Vehicle_InspectionResults_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_InspectionResults_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_InspectionResults_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_InspectionResults_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#vehicle_Message_Create
	 * @param vehicle_Message_Create82
	 */
	@Override
	public Vehicle_Message_CreateResponse vehicle_Message_Create(Vehicle_Message_Create vehicle_Message_Create82) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[41].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Vehicle_Message_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), vehicle_Message_Create82,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "vehicle_Message_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Vehicle_Message_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Vehicle_Message_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Vehicle_Message_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_Message_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_Message_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicle_Message_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#vehicles_Get
	 * @param vehicles_Get14
	 */
	@Override
	public Vehicles_GetResponse vehicles_Get(Vehicles_Get vehicles_Get14) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[7].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Vehicles_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), vehicles_Get14,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "vehicles_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "Vehicles_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Vehicles_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Vehicles_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicles_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicles_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Vehicles_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#workGroups_Get
	 * @param workGroups_Get86
	 */
	@Override
	public WorkGroups_GetResponse workGroups_Get(WorkGroups_Get workGroups_Get86) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[43].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/WorkGroups_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), workGroups_Get86,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "workGroups_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "WorkGroups_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), WorkGroups_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (WorkGroups_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WorkGroups_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WorkGroups_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WorkGroups_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#workpack_Note_Create
	 * @param workpack_Note_Create30
	 */
	@Override
	public Workpack_Note_CreateResponse workpack_Note_Create(Workpack_Note_Create workpack_Note_Create30) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[15].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Workpack_Note_Create");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), workpack_Note_Create30,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "workpack_Note_Create")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Workpack_Note_Create"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Workpack_Note_CreateResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Workpack_Note_CreateResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpack_Note_Create"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpack_Note_Create"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpack_Note_Create"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#workPacks_Get
	 * @param workPacks_Get34
	 */
	@Override
	public WorkPacks_GetResponse workPacks_Get(WorkPacks_Get workPacks_Get34) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[17].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/WorkPacks_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), workPacks_Get34,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "workPacks_Get")), new javax.xml.namespace.QName("http://bartec-systems.com/", "WorkPacks_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), WorkPacks_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (WorkPacks_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WorkPacks_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WorkPacks_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "WorkPacks_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#workpacks_Metrics_Get
	 * @param workpacks_Metrics_Get122
	 */
	@Override
	public Workpacks_Metrics_GetResponse workpacks_Metrics_Get(Workpacks_Metrics_Get workpacks_Metrics_Get122) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[61].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Workpacks_Metrics_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), workpacks_Metrics_Get122,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "workpacks_Metrics_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Workpacks_Metrics_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Workpacks_Metrics_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Workpacks_Metrics_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Metrics_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Metrics_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Metrics_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#workpacks_Notes_Get
	 * @param workpacks_Notes_Get120
	 */
	@Override
	public Workpacks_Notes_GetResponse workpacks_Notes_Get(Workpacks_Notes_Get workpacks_Notes_Get120) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[60].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Workpacks_Notes_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), workpacks_Notes_Get120,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "workpacks_Notes_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Workpacks_Notes_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Workpacks_Notes_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Workpacks_Notes_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Notes_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Notes_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Notes_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	/**
	 * Auto generated method signature
	 *
	 * @see com.placecube.digitalplace.local.waste.bartec.axis.CollectiveAPIWebService#workpacks_Notes_Types_Get
	 * @param workpacks_Notes_Types_Get78
	 */
	@Override
	public Workpacks_Notes_Types_GetResponse workpacks_Notes_Types_Get(Workpacks_Notes_Types_Get workpacks_Notes_Types_Get78) throws java.rmi.RemoteException {

		org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
		try {
			org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[39].getName());
			_operationClient.getOptions().setAction("http://bartec-systems.com/Workpacks_Notes_Types_Get");
			_operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

			addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");

			// create SOAP envelope with that payload
			org.apache.axiom.soap.SOAPEnvelope env = null;

			env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()), workpacks_Notes_Types_Get78,
					optimizeContent(new javax.xml.namespace.QName("http://bartec-systems.com/", "workpacks_Notes_Types_Get")),
					new javax.xml.namespace.QName("http://bartec-systems.com/", "Workpacks_Notes_Types_Get"));

			// adding SOAP soap_headers
			_serviceClient.addHeadersToEnvelope(env);
			// set the message context with that soap envelope
			_messageContext.setEnvelope(env);

			// add the message contxt to the operation client
			_operationClient.addMessageContext(_messageContext);

			// execute the operation client
			_operationClient.execute(true);

			org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
			org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
			_returnEnv.buildWithAttachments();

			java.lang.Object object = fromOM(_returnEnv.getBody().getFirstElement(), Workpacks_Notes_Types_GetResponse.class);
			TransportUtils.detachInputStream(_returnMessageContext);

			return (Workpacks_Notes_Types_GetResponse) object;

		} catch (org.apache.axis2.AxisFault f) {

			org.apache.axiom.om.OMElement faultElt = f.getDetail();
			if (faultElt != null) {
				if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Notes_Types_Get"))) {
					// make the fault by reflection
					try {
						java.lang.String exceptionClassName = faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Notes_Types_Get"));
						java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
						java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
						java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
						// message class
						java.lang.String messageClassName = faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "Workpacks_Notes_Types_Get"));
						java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
						java.lang.Object messageObject = fromOM(faultElt, messageClass);
						java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage", new java.lang.Class[] { messageClass });
						m.invoke(ex, new java.lang.Object[] { messageObject });

						throw new java.rmi.RemoteException(ex.getMessage(), ex);
					} catch (java.lang.ClassCastException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.ClassNotFoundException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.NoSuchMethodException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.reflect.InvocationTargetException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.IllegalAccessException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					} catch (java.lang.InstantiationException e) {
						// we cannot intantiate the class - throw the original
						// Axis fault
						throw f;
					}
				} else {
					throw f;
				}
			} else {
				throw f;
			}
		} finally {
			if (_messageContext.getTransportOut() != null) {
				_messageContext.getTransportOut().getSender().cleanup(_messageContext);
			}
		}
	}

	private java.lang.Object fromOM(org.apache.axiom.om.OMElement param, java.lang.Class type) throws org.apache.axis2.AxisFault {

		try {

			if (Business_Address_Update.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Business_Address_Update.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Business_Address_UpdateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Business_Address_UpdateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Business_Document_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Business_Document_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Business_Document_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Business_Document_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Addresses_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Addresses_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Addresses_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Addresses_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Classes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Classes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Classes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Classes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Statuses_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Statuses_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Statuses_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Statuses_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_SubStatuses_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_SubStatuses_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_SubStatuses_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_SubStatuses_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Businesses_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Businesses_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Cases_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Cases_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Cases_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Cases_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Crews_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Crews_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Crews_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Crews_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Categories_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Categories_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Categories_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Categories_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Classes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Classes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Classes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Classes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Colours_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Colours_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Colours_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Colours_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Conditions_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Conditions_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Conditions_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Conditions_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Manufacturers_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Manufacturers_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Manufacturers_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Manufacturers_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Notes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Notes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Notes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Notes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Schedules_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Schedules_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Schedules_Get2.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Schedules_Get2.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Schedules_Get2Response.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Schedules_Get2Response.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Schedules_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Schedules_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Statuses_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Statuses_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Statuses_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Statuses_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Features_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Features_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Classes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Classes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Classes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Classes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Statuses_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Statuses_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Statuses_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Statuses_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Inspections_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Inspections_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_Close.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_Close.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_CloseResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_CloseResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_Detail_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_Detail_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_Detail_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_Detail_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_FeatureScheduleDates_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_FeatureScheduleDates_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_FeatureScheduleDates_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_FeatureScheduleDates_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_WorkScheduleDates_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_WorkScheduleDates_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Jobs_WorkScheduleDates_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Jobs_WorkScheduleDates_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Classes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Classes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Classes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Classes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Statuses_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Statuses_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Statuses_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Statuses_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Licences_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Licences_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Attribute_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Attribute_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Attribute_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Attribute_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Attribute_Update.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Attribute_Update.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Attribute_UpdateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Attribute_UpdateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_AttributeDefinitions_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_AttributeDefinitions_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_AttributeDefinitions_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_AttributeDefinitions_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Attributes_Delete.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Attributes_Delete.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Attributes_DeleteResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Attributes_DeleteResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Attributes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Attributes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Attributes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Attributes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Detail_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Detail_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Detail_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Detail_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Document_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Document_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Document_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Document_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Event_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Event_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Event_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Event_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Classes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Classes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Classes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Classes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Document_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Document_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Document_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Document_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Events_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Events_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_FutureWorkpacks_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_FutureWorkpacks_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_FutureWorkpacks_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_FutureWorkpacks_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Premises_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Premises_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Resources_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Resources_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Resources_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Resources_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Resources_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Resources_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Resources_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Resources_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Resources_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Resources_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Resources_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Resources_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Appointment_Reservation_Cancel.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Appointment_Reservation_Cancel.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Appointment_Reservation_CancelResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Appointment_Reservation_CancelResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Appointment_Reservation_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Appointment_Reservation_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Appointment_Reservation_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Appointment_Reservation_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Appointment_Reservation_Extend.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Appointment_Reservation_Extend.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Appointment_Reservation_ExtendResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Appointment_Reservation_ExtendResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Appointment_Slot_Reserve.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Appointment_Slot_Reserve.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Appointment_Slot_ReserveResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Appointment_Slot_ReserveResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Cancel.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Cancel.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_CancelResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_CancelResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Document_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Document_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Document_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Document_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Note_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Note_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Note_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Note_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Status_Set.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Status_Set.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Status_SetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Status_SetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_Update.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_Update.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequest_UpdateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequest_UpdateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Appointments_Availability_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Appointments_Availability_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Appointments_Availability_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Appointments_Availability_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Classes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Classes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Classes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Classes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Detail_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Detail_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Detail_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Detail_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_History_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_History_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_History_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_History_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Notes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Notes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Notes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Notes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Notes_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Notes_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Notes_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Notes_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_SLAs_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_SLAs_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_SLAs_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_SLAs_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Statuses_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Statuses_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Statuses_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Statuses_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Updates_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Updates_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (ServiceRequests_Updates_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = ServiceRequests_Updates_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Site_Document_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Site_Document_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Site_Document_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Site_Document_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Sites_Documents_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Sites_Documents_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Sites_Documents_GetAll.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Sites_Documents_GetAll.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Sites_Documents_GetAllResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Sites_Documents_GetAllResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Sites_Documents_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Sites_Documents_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Sites_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Sites_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Sites_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Sites_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Attributes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Attributes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Attributes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Attributes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Cancellations_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Cancellations_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Cancellations_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Cancellations_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Detail_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Detail_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Detail_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Detail_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Events_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Events_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Events_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Events_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Events_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Events_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Events_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Events_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Streets_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Streets_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (System_ExtendedDataDefinitions_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = System_ExtendedDataDefinitions_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (System_ExtendedDataDefinitions_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = System_ExtendedDataDefinitions_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (System_LandTypes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = System_LandTypes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (System_LandTypes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = System_LandTypes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (System_WasteTypes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = System_WasteTypes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (System_WasteTypes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = System_WasteTypes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Vehicle_InspectionForms_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Vehicle_InspectionForms_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Vehicle_InspectionForms_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Vehicle_InspectionForms_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Vehicle_InspectionResults_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Vehicle_InspectionResults_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Vehicle_InspectionResults_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Vehicle_InspectionResults_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Vehicle_Message_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Vehicle_Message_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Vehicle_Message_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Vehicle_Message_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Vehicles_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Vehicles_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Vehicles_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Vehicles_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (WorkGroups_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = WorkGroups_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (WorkGroups_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = WorkGroups_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Workpack_Note_Create.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Workpack_Note_Create.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Workpack_Note_CreateResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Workpack_Note_CreateResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (WorkPacks_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = WorkPacks_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (WorkPacks_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = WorkPacks_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Workpacks_Metrics_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Workpacks_Metrics_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Workpacks_Metrics_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Workpacks_Metrics_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Workpacks_Notes_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Workpacks_Notes_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Workpacks_Notes_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Workpacks_Notes_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Workpacks_Notes_Types_Get.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Workpacks_Notes_Types_Get.Factory.parse(reader);
				reader.close();
				return result;
			}

			if (Workpacks_Notes_Types_GetResponse.class.equals(type)) {

				javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
				java.lang.Object result = Workpacks_Notes_Types_GetResponse.Factory.parse(reader);
				reader.close();
				return result;
			}

		} catch (java.lang.Exception e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
		return null;
	}

	private boolean optimizeContent(javax.xml.namespace.QName opName) {

		if (opNameArray == null) {
			return false;
		}
		for (QName element : opNameArray) {
			if (opName.equals(element)) {
				return true;
			}
		}
		return false;
	}

	private void populateAxisService() throws org.apache.axis2.AxisFault {

		// creating the Service with a unique name
		_service = new org.apache.axis2.description.AxisService("CollectiveAPIWebService" + getUniqueSuffix());
		addAnonymousOperations();

		// creating the operations
		org.apache.axis2.description.AxisOperation __operation;

		_operations = new org.apache.axis2.description.AxisOperation[102];

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Documents_Get"));
		_service.addOperation(__operation);

		_operations[0] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Classes_Get"));
		_service.addOperation(__operation);

		_operations[1] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Addresses_Get"));
		_service.addOperation(__operation);

		_operations[2] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Get"));
		_service.addOperation(__operation);

		_operations[3] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_FutureWorkpacks_Get"));
		_service.addOperation(__operation);

		_operations[4] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Get"));
		_service.addOperation(__operation);

		_operations[5] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_SLAs_Get"));
		_service.addOperation(__operation);

		_operations[6] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "vehicles_Get"));
		_service.addOperation(__operation);

		_operations[7] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Attributes_Get"));
		_service.addOperation(__operation);

		_operations[8] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "system_ExtendedDataDefinitions_Get"));
		_service.addOperation(__operation);

		_operations[9] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Conditions_Get"));
		_service.addOperation(__operation);

		_operations[10] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Create"));
		_service.addOperation(__operation);

		_operations[11] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "crews_Get"));
		_service.addOperation(__operation);

		_operations[12] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Document_Create"));
		_service.addOperation(__operation);

		_operations[13] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Statuses_Get"));
		_service.addOperation(__operation);

		_operations[14] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "workpack_Note_Create"));
		_service.addOperation(__operation);

		_operations[15] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Get"));
		_service.addOperation(__operation);

		_operations[16] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "workPacks_Get"));
		_service.addOperation(__operation);

		_operations[17] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Get"));
		_service.addOperation(__operation);

		_operations[18] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "site_Document_Create"));
		_service.addOperation(__operation);

		_operations[19] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Notes_Get"));
		_service.addOperation(__operation);

		_operations[20] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "cases_Documents_Get"));
		_service.addOperation(__operation);

		_operations[21] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Notes_Get"));
		_service.addOperation(__operation);

		_operations[22] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_SubStatuses_Get"));
		_service.addOperation(__operation);

		_operations[23] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Documents_Get"));
		_service.addOperation(__operation);

		_operations[24] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Detail_Get"));
		_service.addOperation(__operation);

		_operations[25] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Updates_Get"));
		_service.addOperation(__operation);

		_operations[26] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Types_Get"));
		_service.addOperation(__operation);

		_operations[27] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_Get"));
		_service.addOperation(__operation);

		_operations[28] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_Close"));
		_service.addOperation(__operation);

		_operations[29] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Types_Get"));
		_service.addOperation(__operation);

		_operations[30] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Categories_Get"));
		_service.addOperation(__operation);

		_operations[31] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Get"));
		_service.addOperation(__operation);

		_operations[32] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Detail_Get"));
		_service.addOperation(__operation);

		_operations[33] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Manufacturers_Get"));
		_service.addOperation(__operation);

		_operations[34] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Classes_Get"));
		_service.addOperation(__operation);

		_operations[35] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "sites_Documents_Get"));
		_service.addOperation(__operation);

		_operations[36] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Appointment_Reservation_Extend"));
		_service.addOperation(__operation);

		_operations[37] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Documents_Get"));
		_service.addOperation(__operation);

		_operations[38] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "workpacks_Notes_Types_Get"));
		_service.addOperation(__operation);

		_operations[39] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_FeatureScheduleDates_Get"));
		_service.addOperation(__operation);

		_operations[40] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "vehicle_Message_Create"));
		_service.addOperation(__operation);

		_operations[41] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Schedules_Get2"));
		_service.addOperation(__operation);

		_operations[42] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "workGroups_Get"));
		_service.addOperation(__operation);

		_operations[43] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Appointments_Availability_Get"));
		_service.addOperation(__operation);

		_operations[44] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Schedules_Get"));
		_service.addOperation(__operation);

		_operations[45] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Statuses_Get"));
		_service.addOperation(__operation);

		_operations[46] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "resources_Types_Get"));
		_service.addOperation(__operation);

		_operations[47] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Classes_Get"));
		_service.addOperation(__operation);

		_operations[48] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Classes_Get"));
		_service.addOperation(__operation);

		_operations[49] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Appointment_Reservation_Create"));
		_service.addOperation(__operation);

		_operations[50] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Types_Get"));
		_service.addOperation(__operation);

		_operations[51] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Events_Document_Create"));
		_service.addOperation(__operation);

		_operations[52] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Classes_Get"));
		_service.addOperation(__operation);

		_operations[53] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Attribute_Update"));
		_service.addOperation(__operation);

		_operations[54] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "vehicle_InspectionForms_Get"));
		_service.addOperation(__operation);

		_operations[55] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_WorkScheduleDates_Get"));
		_service.addOperation(__operation);

		_operations[56] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "business_Address_Update"));
		_service.addOperation(__operation);

		_operations[57] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Types_Get"));
		_service.addOperation(__operation);

		_operations[58] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "vehicle_InspectionResults_Get"));
		_service.addOperation(__operation);

		_operations[59] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "workpacks_Notes_Get"));
		_service.addOperation(__operation);

		_operations[60] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "workpacks_Metrics_Get"));
		_service.addOperation(__operation);

		_operations[61] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_History_Get"));
		_service.addOperation(__operation);

		_operations[62] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Note_Create"));
		_service.addOperation(__operation);

		_operations[63] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "business_Document_Create"));
		_service.addOperation(__operation);

		_operations[64] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_Detail_Get"));
		_service.addOperation(__operation);

		_operations[65] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "system_WasteTypes_Get"));
		_service.addOperation(__operation);

		_operations[66] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Get"));
		_service.addOperation(__operation);

		_operations[67] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Attributes_Delete"));
		_service.addOperation(__operation);

		_operations[68] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Statuses_Get"));
		_service.addOperation(__operation);

		_operations[69] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_AttributeDefinitions_Get"));
		_service.addOperation(__operation);

		_operations[70] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Get"));
		_service.addOperation(__operation);

		_operations[71] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Documents_Get"));
		_service.addOperation(__operation);

		_operations[72] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "jobs_Documents_Get"));
		_service.addOperation(__operation);

		_operations[73] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Document_Create"));
		_service.addOperation(__operation);

		_operations[74] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "sites_Documents_GetAll"));
		_service.addOperation(__operation);

		_operations[75] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Attributes_Get"));
		_service.addOperation(__operation);

		_operations[76] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "features_Colours_Get"));
		_service.addOperation(__operation);

		_operations[77] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Documents_Get"));
		_service.addOperation(__operation);

		_operations[78] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "businesses_Types_Get"));
		_service.addOperation(__operation);

		_operations[79] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Events_Types_Get"));
		_service.addOperation(__operation);

		_operations[80] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Appointment_Slot_Reserve"));
		_service.addOperation(__operation);

		_operations[81] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Statuses_Get"));
		_service.addOperation(__operation);

		_operations[82] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "licences_Types_Get"));
		_service.addOperation(__operation);

		_operations[83] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "sites_Get"));
		_service.addOperation(__operation);

		_operations[84] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Get"));
		_service.addOperation(__operation);

		_operations[85] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Attribute_Create"));
		_service.addOperation(__operation);

		_operations[86] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "inspections_Statuses_Get"));
		_service.addOperation(__operation);

		_operations[87] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "system_LandTypes_Get"));
		_service.addOperation(__operation);

		_operations[88] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Cancel"));
		_service.addOperation(__operation);

		_operations[89] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Notes_Types_Get"));
		_service.addOperation(__operation);

		_operations[90] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "premises_Event_Create"));
		_service.addOperation(__operation);

		_operations[91] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Cancellations_Get"));
		_service.addOperation(__operation);

		_operations[92] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Documents_Get"));
		_service.addOperation(__operation);

		_operations[93] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "resources_Get"));
		_service.addOperation(__operation);

		_operations[94] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Appointment_Reservation_Cancel"));
		_service.addOperation(__operation);

		_operations[95] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Detail_Get"));
		_service.addOperation(__operation);

		_operations[96] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequests_Classes_Get"));
		_service.addOperation(__operation);

		_operations[97] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Update"));
		_service.addOperation(__operation);

		_operations[98] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "streets_Events_Get"));
		_service.addOperation(__operation);

		_operations[99] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "resources_Documents_Get"));
		_service.addOperation(__operation);

		_operations[100] = __operation;

		__operation = new org.apache.axis2.description.OutInAxisOperation();

		__operation.setName(new javax.xml.namespace.QName("http://bartec-systems.com/", "serviceRequest_Status_Set"));
		_service.addOperation(__operation);

		_operations[101] = __operation;
	}

	// populates the faults
	private void populateFaults() {
	}

	/** get the default envelope */
	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
		return factory.getDefaultEnvelope();
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Business_Address_Update param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Business_Address_Update.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Business_Document_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Business_Document_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Businesses_Addresses_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Businesses_Addresses_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Businesses_Classes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Businesses_Classes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Businesses_Documents_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Businesses_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Businesses_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Businesses_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Businesses_Statuses_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Businesses_Statuses_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Businesses_SubStatuses_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Businesses_SubStatuses_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Businesses_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Businesses_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Cases_Documents_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Cases_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Crews_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Crews_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Categories_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Categories_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Classes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Classes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Colours_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Colours_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Conditions_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Conditions_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Manufacturers_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Manufacturers_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Notes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Notes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Schedules_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Schedules_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Schedules_Get2 param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Schedules_Get2.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Statuses_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Statuses_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Features_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Features_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Inspections_Classes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Inspections_Classes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Inspections_Documents_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Inspections_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Inspections_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Inspections_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Inspections_Statuses_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Inspections_Statuses_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Inspections_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Inspections_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Jobs_Close param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Jobs_Close.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Jobs_Detail_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Jobs_Detail_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Jobs_Documents_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Jobs_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Jobs_FeatureScheduleDates_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Jobs_FeatureScheduleDates_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Jobs_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Jobs_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Jobs_WorkScheduleDates_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Jobs_WorkScheduleDates_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Licences_Classes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Licences_Classes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Licences_Documents_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Licences_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Licences_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Licences_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Licences_Statuses_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Licences_Statuses_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Licences_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Licences_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Attribute_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Attribute_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Attribute_Update param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Attribute_Update.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_AttributeDefinitions_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_AttributeDefinitions_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Attributes_Delete param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Attributes_Delete.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Attributes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Attributes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Detail_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Detail_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Document_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Document_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Documents_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Event_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Event_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Events_Classes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Events_Classes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Events_Document_Create param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Events_Document_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Events_Documents_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Events_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Events_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Events_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Events_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Events_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_FutureWorkpacks_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_FutureWorkpacks_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Premises_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Premises_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Resources_Documents_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Resources_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Resources_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Resources_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Resources_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Resources_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Appointment_Reservation_Cancel param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Appointment_Reservation_Cancel.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Appointment_Reservation_Create param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Appointment_Reservation_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Appointment_Reservation_Extend param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Appointment_Reservation_Extend.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Appointment_Slot_Reserve param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Appointment_Slot_Reserve.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Cancel param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Cancel.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Document_Create param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Document_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Note_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Note_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Status_Set param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Status_Set.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequest_Update param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequest_Update.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Appointments_Availability_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Appointments_Availability_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Classes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Classes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Detail_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Detail_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Documents_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_History_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_History_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Notes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Notes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Notes_Types_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Notes_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_SLAs_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_SLAs_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Statuses_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Statuses_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ServiceRequests_Updates_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(ServiceRequests_Updates_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Site_Document_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Site_Document_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Sites_Documents_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Sites_Documents_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Sites_Documents_GetAll param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Sites_Documents_GetAll.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Sites_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Sites_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Streets_Attributes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Streets_Attributes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Streets_Cancellations_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Streets_Cancellations_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Streets_Detail_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Streets_Detail_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Streets_Events_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Streets_Events_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Streets_Events_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Streets_Events_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Streets_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Streets_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, System_ExtendedDataDefinitions_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(System_ExtendedDataDefinitions_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, System_LandTypes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(System_LandTypes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, System_WasteTypes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(System_WasteTypes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Vehicle_InspectionForms_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Vehicle_InspectionForms_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Vehicle_InspectionResults_Get param, boolean optimizeContent,
			javax.xml.namespace.QName elementQName) throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Vehicle_InspectionResults_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Vehicle_Message_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Vehicle_Message_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Vehicles_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Vehicles_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, WorkGroups_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(WorkGroups_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Workpack_Note_Create param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Workpack_Note_Create.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, WorkPacks_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(WorkPacks_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Workpacks_Metrics_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Workpacks_Metrics_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Workpacks_Notes_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Workpacks_Notes_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, Workpacks_Notes_Types_Get param, boolean optimizeContent, javax.xml.namespace.QName elementQName)
			throws org.apache.axis2.AxisFault {

		try {

			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
			emptyEnvelope.getBody().addChild(param.getOMElement(Workpacks_Notes_Types_Get.MY_QNAME, factory));
			return emptyEnvelope;
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Business_Address_Update param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Business_Address_Update.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Business_Address_UpdateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Business_Address_UpdateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Business_Document_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Business_Document_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Business_Document_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Business_Document_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Addresses_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Addresses_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Addresses_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Addresses_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Classes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Classes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Classes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Classes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	// https://collectiveapi.bartec-systems.com/API-R1604/CollectiveAPI.asmx
	private org.apache.axiom.om.OMElement toOM(Businesses_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Statuses_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Statuses_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Statuses_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Statuses_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_SubStatuses_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_SubStatuses_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_SubStatuses_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_SubStatuses_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Businesses_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Businesses_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Cases_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Cases_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Cases_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Cases_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Crews_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Crews_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Crews_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Crews_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Categories_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Categories_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Categories_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Categories_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Classes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Classes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Classes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Classes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Colours_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Colours_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Colours_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Colours_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Conditions_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Conditions_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Conditions_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Conditions_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Manufacturers_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Manufacturers_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Manufacturers_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Manufacturers_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Notes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Notes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Notes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Notes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Schedules_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Schedules_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Schedules_Get2 param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Schedules_Get2.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Schedules_Get2Response param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Schedules_Get2Response.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Schedules_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Schedules_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Statuses_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Statuses_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Statuses_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Statuses_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Features_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Features_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Classes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Classes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Classes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Classes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Statuses_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Statuses_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Statuses_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Statuses_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Inspections_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Inspections_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_Close param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_Close.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_CloseResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_CloseResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_Detail_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_Detail_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_Detail_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_Detail_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_FeatureScheduleDates_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_FeatureScheduleDates_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_FeatureScheduleDates_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_FeatureScheduleDates_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_WorkScheduleDates_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_WorkScheduleDates_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Jobs_WorkScheduleDates_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Jobs_WorkScheduleDates_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Classes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Classes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Classes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Classes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Statuses_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Statuses_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Statuses_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Statuses_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Licences_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Licences_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Attribute_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Attribute_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Attribute_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Attribute_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Attribute_Update param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Attribute_Update.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Attribute_UpdateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Attribute_UpdateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_AttributeDefinitions_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_AttributeDefinitions_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_AttributeDefinitions_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_AttributeDefinitions_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Attributes_Delete param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Attributes_Delete.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Attributes_DeleteResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Attributes_DeleteResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Attributes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Attributes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Attributes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Attributes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Detail_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Detail_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Detail_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Detail_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Document_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Document_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Document_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Document_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Event_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Event_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Event_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Event_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Classes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Classes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Classes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Classes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Document_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Document_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Document_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Document_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Premises_Events_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Premises_Events_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Events_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Premises_FutureWorkpacks_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_FutureWorkpacks_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Premises_FutureWorkpacks_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_FutureWorkpacks_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Premises_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Premises_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Premises_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Resources_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Resources_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Resources_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Resources_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Resources_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Resources_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Resources_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Resources_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Resources_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Resources_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Resources_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Resources_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Appointment_Reservation_Cancel param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Appointment_Reservation_Cancel.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Appointment_Reservation_CancelResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Appointment_Reservation_CancelResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Appointment_Reservation_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Appointment_Reservation_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Appointment_Reservation_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Appointment_Reservation_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Appointment_Reservation_Extend param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Appointment_Reservation_Extend.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Appointment_Reservation_ExtendResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Appointment_Reservation_ExtendResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Appointment_Slot_Reserve param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Appointment_Slot_Reserve.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Appointment_Slot_ReserveResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Appointment_Slot_ReserveResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Cancel param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Cancel.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_CancelResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_CancelResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Document_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Document_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Document_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Document_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Note_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Note_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Note_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Note_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Status_Set param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Status_Set.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Status_SetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Status_SetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_Update param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_Update.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequest_UpdateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequest_UpdateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Appointments_Availability_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Appointments_Availability_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Appointments_Availability_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Appointments_Availability_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Classes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Classes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Classes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Classes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Detail_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Detail_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Detail_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Detail_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_History_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_History_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_History_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_History_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Notes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Notes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Notes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Notes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Notes_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Notes_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Notes_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Notes_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_SLAs_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_SLAs_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_SLAs_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_SLAs_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Statuses_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Statuses_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Statuses_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Statuses_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Updates_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Updates_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(ServiceRequests_Updates_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(ServiceRequests_Updates_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Site_Document_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Site_Document_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Site_Document_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Site_Document_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Sites_Documents_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Sites_Documents_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Sites_Documents_GetAll param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Sites_Documents_GetAll.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Sites_Documents_GetAllResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Sites_Documents_GetAllResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Sites_Documents_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Sites_Documents_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Sites_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Sites_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Sites_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Sites_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Attributes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Attributes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Attributes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Attributes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Cancellations_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Cancellations_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Cancellations_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Cancellations_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Detail_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Detail_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Detail_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Detail_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Events_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Events_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Events_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Events_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Events_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Events_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Events_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Events_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Streets_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Streets_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(System_ExtendedDataDefinitions_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(System_ExtendedDataDefinitions_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(System_ExtendedDataDefinitions_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(System_ExtendedDataDefinitions_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(System_LandTypes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(System_LandTypes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(System_LandTypes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(System_LandTypes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(System_WasteTypes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(System_WasteTypes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(System_WasteTypes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(System_WasteTypes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Vehicle_InspectionForms_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Vehicle_InspectionForms_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Vehicle_InspectionForms_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Vehicle_InspectionForms_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Vehicle_InspectionResults_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Vehicle_InspectionResults_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Vehicle_InspectionResults_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Vehicle_InspectionResults_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Vehicle_Message_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Vehicle_Message_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Vehicle_Message_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Vehicle_Message_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Vehicles_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Vehicles_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Vehicles_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Vehicles_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(WorkGroups_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(WorkGroups_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(WorkGroups_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(WorkGroups_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Workpack_Note_Create param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Workpack_Note_Create.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Workpack_Note_CreateResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Workpack_Note_CreateResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(WorkPacks_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(WorkPacks_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(WorkPacks_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(WorkPacks_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Workpacks_Metrics_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Workpacks_Metrics_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Workpacks_Metrics_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Workpacks_Metrics_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Workpacks_Notes_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Workpacks_Notes_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Workpacks_Notes_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Workpacks_Notes_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	/* methods to provide back word compatibility */

	private org.apache.axiom.om.OMElement toOM(Workpacks_Notes_Types_Get param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Workpacks_Notes_Types_Get.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}

	private org.apache.axiom.om.OMElement toOM(Workpacks_Notes_Types_GetResponse param, boolean optimizeContent) throws org.apache.axis2.AxisFault {

		try {
			return param.getOMElement(Workpacks_Notes_Types_GetResponse.MY_QNAME, org.apache.axiom.om.OMAbstractFactory.getOMFactory());
		} catch (org.apache.axis2.databinding.ADBException e) {
			throw org.apache.axis2.AxisFault.makeFault(e);
		}
	}
}
