/**
 * Premises_Get.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/** Premises_Get bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Premises_Get implements org.apache.axis2.databinding.ADBBean {

  public static final javax.xml.namespace.QName MY_QNAME =
      new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Get", "ns99");

  /** field for Token */
  protected java.lang.String localToken;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTokenTracker = false;

  public boolean isTokenSpecified() {
    return localTokenTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getToken() {
    return localToken;
  }

  /**
   * Auto generated setter method
   *
   * @param param Token
   */
  public void setToken(java.lang.String param) {
    localTokenTracker = param != null;

    this.localToken = param;
  }

  /** field for UPRN */
  protected java.math.BigDecimal localUPRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUPRN() {
    return localUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRN
   */
  public void setUPRN(java.math.BigDecimal param) {

    this.localUPRN = param;
  }

  /** field for USRN */
  protected java.math.BigDecimal localUSRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUSRN() {
    return localUSRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param USRN
   */
  public void setUSRN(java.math.BigDecimal param) {

    this.localUSRN = param;
  }

  /** field for Address1 */
  protected java.lang.String localAddress1;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAddress1Tracker = false;

  public boolean isAddress1Specified() {
    return localAddress1Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAddress1() {
    return localAddress1;
  }

  /**
   * Auto generated setter method
   *
   * @param param Address1
   */
  public void setAddress1(java.lang.String param) {
    localAddress1Tracker = param != null;

    this.localAddress1 = param;
  }

  /** field for Address2 */
  protected java.lang.String localAddress2;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAddress2Tracker = false;

  public boolean isAddress2Specified() {
    return localAddress2Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAddress2() {
    return localAddress2;
  }

  /**
   * Auto generated setter method
   *
   * @param param Address2
   */
  public void setAddress2(java.lang.String param) {
    localAddress2Tracker = param != null;

    this.localAddress2 = param;
  }

  /** field for Street */
  protected java.lang.String localStreet;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localStreetTracker = false;

  public boolean isStreetSpecified() {
    return localStreetTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getStreet() {
    return localStreet;
  }

  /**
   * Auto generated setter method
   *
   * @param param Street
   */
  public void setStreet(java.lang.String param) {
    localStreetTracker = param != null;

    this.localStreet = param;
  }

  /** field for Locality */
  protected java.lang.String localLocality;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLocalityTracker = false;

  public boolean isLocalitySpecified() {
    return localLocalityTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getLocality() {
    return localLocality;
  }

  /**
   * Auto generated setter method
   *
   * @param param Locality
   */
  public void setLocality(java.lang.String param) {
    localLocalityTracker = param != null;

    this.localLocality = param;
  }

  /** field for Town */
  protected java.lang.String localTown;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTownTracker = false;

  public boolean isTownSpecified() {
    return localTownTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTown() {
    return localTown;
  }

  /**
   * Auto generated setter method
   *
   * @param param Town
   */
  public void setTown(java.lang.String param) {
    localTownTracker = param != null;

    this.localTown = param;
  }

  /** field for Postcode */
  protected java.lang.String localPostcode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPostcodeTracker = false;

  public boolean isPostcodeSpecified() {
    return localPostcodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPostcode() {
    return localPostcode;
  }

  /**
   * Auto generated setter method
   *
   * @param param Postcode
   */
  public void setPostcode(java.lang.String param) {
    localPostcodeTracker = param != null;

    this.localPostcode = param;
  }

  /** field for WardName */
  protected java.lang.String localWardName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWardNameTracker = false;

  public boolean isWardNameSpecified() {
    return localWardNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWardName() {
    return localWardName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WardName
   */
  public void setWardName(java.lang.String param) {
    localWardNameTracker = param != null;

    this.localWardName = param;
  }

  /** field for ParishName */
  protected java.lang.String localParishName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localParishNameTracker = false;

  public boolean isParishNameSpecified() {
    return localParishNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getParishName() {
    return localParishName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ParishName
   */
  public void setParishName(java.lang.String param) {
    localParishNameTracker = param != null;

    this.localParishName = param;
  }

  /** field for UserLabel */
  protected java.lang.String localUserLabel;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUserLabelTracker = false;

  public boolean isUserLabelSpecified() {
    return localUserLabelTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getUserLabel() {
    return localUserLabel;
  }

  /**
   * Auto generated setter method
   *
   * @param param UserLabel
   */
  public void setUserLabel(java.lang.String param) {
    localUserLabelTracker = param != null;

    this.localUserLabel = param;
  }

  /** field for ParentUPRN */
  protected java.math.BigDecimal localParentUPRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getParentUPRN() {
    return localParentUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param ParentUPRN
   */
  public void setParentUPRN(java.math.BigDecimal param) {

    this.localParentUPRN = param;
  }

  /** field for Bounds */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox localBounds;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localBoundsTracker = false;

  public boolean isBoundsSpecified() {
    return localBoundsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox getBounds() {
    return localBounds;
  }

  /**
   * Auto generated setter method
   *
   * @param param Bounds
   */
  public void setBounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox param) {
    localBoundsTracker = param != null;

    this.localBounds = param;
  }

  /** field for BLPUClass */
  protected java.lang.String localBLPUClass;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localBLPUClassTracker = false;

  public boolean isBLPUClassSpecified() {
    return localBLPUClassTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getBLPUClass() {
    return localBLPUClass;
  }

  /**
   * Auto generated setter method
   *
   * @param param BLPUClass
   */
  public void setBLPUClass(java.lang.String param) {
    localBLPUClassTracker = param != null;

    this.localBLPUClass = param;
  }

  /** field for BLPULStat */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfString localBLPULStat;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localBLPULStatTracker = false;

  public boolean isBLPULStatSpecified() {
    return localBLPULStatTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfString
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfString getBLPULStat() {
    return localBLPULStat;
  }

  /**
   * Auto generated setter method
   *
   * @param param BLPULStat
   */
  public void setBLPULStat(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfString param) {
    localBLPULStatTracker = param != null;

    this.localBLPULStat = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://bartec-systems.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Premises_Get",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Premises_Get", xmlWriter);
      }
    }
    if (localTokenTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "token", xmlWriter);

      if (localToken == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("token cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localToken);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "UPRN", xmlWriter);

    if (localUPRN == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
    }

    xmlWriter.writeEndElement();

    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "USRN", xmlWriter);

    if (localUSRN == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUSRN));
    }

    xmlWriter.writeEndElement();
    if (localAddress1Tracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "Address1", xmlWriter);

      if (localAddress1 == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Address1 cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAddress1);
      }

      xmlWriter.writeEndElement();
    }
    if (localAddress2Tracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "Address2", xmlWriter);

      if (localAddress2 == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Address2 cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAddress2);
      }

      xmlWriter.writeEndElement();
    }
    if (localStreetTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "Street", xmlWriter);

      if (localStreet == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Street cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localStreet);
      }

      xmlWriter.writeEndElement();
    }
    if (localLocalityTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "Locality", xmlWriter);

      if (localLocality == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Locality cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localLocality);
      }

      xmlWriter.writeEndElement();
    }
    if (localTownTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "Town", xmlWriter);

      if (localTown == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Town cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTown);
      }

      xmlWriter.writeEndElement();
    }
    if (localPostcodeTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "Postcode", xmlWriter);

      if (localPostcode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Postcode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localPostcode);
      }

      xmlWriter.writeEndElement();
    }
    if (localWardNameTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "WardName", xmlWriter);

      if (localWardName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WardName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localWardName);
      }

      xmlWriter.writeEndElement();
    }
    if (localParishNameTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "ParishName", xmlWriter);

      if (localParishName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ParishName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localParishName);
      }

      xmlWriter.writeEndElement();
    }
    if (localUserLabelTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "UserLabel", xmlWriter);

      if (localUserLabel == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("UserLabel cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localUserLabel);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "ParentUPRN", xmlWriter);

    if (localParentUPRN == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParentUPRN));
    }

    xmlWriter.writeEndElement();
    if (localBoundsTracker) {
      if (localBounds == null) {
        throw new org.apache.axis2.databinding.ADBException("Bounds cannot be null!!");
      }
      localBounds.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "Bounds"), xmlWriter);
    }
    if (localBLPUClassTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "BLPUClass", xmlWriter);

      if (localBLPUClass == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("BLPUClass cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localBLPUClass);
      }

      xmlWriter.writeEndElement();
    }
    if (localBLPULStatTracker) {
      if (localBLPULStat == null) {
        throw new org.apache.axis2.databinding.ADBException("BLPULStat cannot be null!!");
      }
      localBLPULStat.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "BLPULStat"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://bartec-systems.com/")) {
      return "ns99";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Premises_Get parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Premises_Get object = new Premises_Get();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Premises_Get".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Premises_Get)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "token")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "token" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setToken(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setUPRN(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "USRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setUSRN(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Address1")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Address1" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAddress1(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Address2")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Address2" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAddress2(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Street")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Street" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setStreet(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Locality")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Locality" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLocality(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Town")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Town" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTown(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Postcode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Postcode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPostcode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "WardName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WardName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWardName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ParishName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ParishName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setParishName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "UserLabel")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UserLabel" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUserLabel(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ParentUPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setParentUPRN(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Bounds")
                .equals(reader.getName())) {

          object.setBounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "BLPUClass")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "BLPUClass" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setBLPUClass(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "BLPULStat")
                .equals(reader.getName())) {

          object.setBLPULStat(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfString.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
