/**
 * CtEventType.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** CtEventType bean class */
@SuppressWarnings({"unchecked", "unused"})
public class CtEventType implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ctEventType
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for ID */
  protected int localID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getID() {
    return localID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ID
   */
  public void setID(int param) {

    this.localID = param;
  }

  /** field for Description */
  protected java.lang.String localDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDescriptionTracker = false;

  public boolean isDescriptionSpecified() {
    return localDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getDescription() {
    return localDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param Description
   */
  public void setDescription(java.lang.String param) {
    localDescriptionTracker = param != null;

    this.localDescription = param;
  }

  /** field for SystemEvent */
  protected boolean localSystemEvent;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getSystemEvent() {
    return localSystemEvent;
  }

  /**
   * Auto generated setter method
   *
   * @param param SystemEvent
   */
  public void setSystemEvent(boolean param) {

    this.localSystemEvent = param;
  }

  /** field for ClosingEvent */
  protected boolean localClosingEvent;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getClosingEvent() {
    return localClosingEvent;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosingEvent
   */
  public void setClosingEvent(boolean param) {

    this.localClosingEvent = param;
  }

  /** field for APIVisibility */
  protected java.lang.String localAPIVisibility;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAPIVisibilityTracker = false;

  public boolean isAPIVisibilitySpecified() {
    return localAPIVisibilityTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAPIVisibility() {
    return localAPIVisibility;
  }

  /**
   * Auto generated setter method
   *
   * @param param APIVisibility
   */
  public void setAPIVisibility(java.lang.String param) {
    localAPIVisibilityTracker = param != null;

    this.localAPIVisibility = param;
  }

  /** field for AvailableOnMobile */
  protected boolean localAvailableOnMobile;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getAvailableOnMobile() {
    return localAvailableOnMobile;
  }

  /**
   * Auto generated setter method
   *
   * @param param AvailableOnMobile
   */
  public void setAvailableOnMobile(boolean param) {

    this.localAvailableOnMobile = param;
  }

  /** field for MultipleSubEvents */
  protected boolean localMultipleSubEvents;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getMultipleSubEvents() {
    return localMultipleSubEvents;
  }

  /**
   * Auto generated setter method
   *
   * @param param MultipleSubEvents
   */
  public void setMultipleSubEvents(boolean param) {

    this.localMultipleSubEvents = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /** field for EventClass */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass localEventClass;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventClassTracker = false;

  public boolean isEventClassSpecified() {
    return localEventClassTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass getEventClass() {
    return localEventClass;
  }

  /**
   * Auto generated setter method
   *
   * @param param EventClass
   */
  public void setEventClass(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass param) {
    localEventClassTracker = param != null;

    this.localEventClass = param;
  }

  /** field for SubEventType This was an Array! */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] localSubEventType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSubEventTypeTracker = false;

  public boolean isSubEventTypeSpecified() {
    return localSubEventTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[]
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] getSubEventType() {
    return localSubEventType;
  }

  /** validate the array for SubEventType */
  protected void validateSubEventType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] param) {}

  /**
   * Auto generated setter method
   *
   * @param param SubEventType
   */
  public void setSubEventType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] param) {

    validateSubEventType(param);

    localSubEventTypeTracker = param != null;

    this.localSubEventType = param;
  }

  /**
   * Auto generated add method for the array for convenience
   *
   * @param param com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType
   */
  public void addSubEventType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType param) {
    if (localSubEventType == null) {
      localSubEventType = new com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[] {};
    }

    // update the setting tracker
    localSubEventTypeTracker = true;

    java.util.List list =
        org.apache.axis2.databinding.utils.ConverterUtil.toList(localSubEventType);
    list.add(param);
    this.localSubEventType =
        (com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[])
            list.toArray(new com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[list.size()]);
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ctEventType",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "ctEventType", xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ID", xmlWriter);

    if (localID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localID));
    }

    xmlWriter.writeEndElement();
    if (localDescriptionTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Description", xmlWriter);

      if (localDescription == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Description cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localDescription);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "SystemEvent", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("SystemEvent cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSystemEvent));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ClosingEvent", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("ClosingEvent cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClosingEvent));
    }

    xmlWriter.writeEndElement();
    if (localAPIVisibilityTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "APIVisibility", xmlWriter);

      if (localAPIVisibility == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("APIVisibility cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAPIVisibility);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "AvailableOnMobile", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("AvailableOnMobile cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAvailableOnMobile));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "MultipleSubEvents", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("MultipleSubEvents cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMultipleSubEvents));
    }

    xmlWriter.writeEndElement();
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    if (localEventClassTracker) {
      if (localEventClass == null) {
        throw new org.apache.axis2.databinding.ADBException("EventClass cannot be null!!");
      }
      localEventClass.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventClass"), xmlWriter);
    }
    if (localSubEventTypeTracker) {
      if (localSubEventType != null) {
        for (int i = 0; i < localSubEventType.length; i++) {
          if (localSubEventType[i] != null) {
            localSubEventType[i].serialize(
                new javax.xml.namespace.QName("http://www.bartec-systems.com", "SubEventType"),
                xmlWriter);
          } else {

            // we don't have to do any thing since minOccures is zero

          }
        }
      } else {

        throw new org.apache.axis2.databinding.ADBException("SubEventType cannot be null!!");
      }
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static CtEventType parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      CtEventType object = new CtEventType();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ctEventType".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (CtEventType)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        java.util.ArrayList list10 = new java.util.ArrayList();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Description")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Description" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDescription(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "SystemEvent")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SystemEvent" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSystemEvent(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ClosingEvent")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosingEvent" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosingEvent(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "APIVisibility")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "APIVisibility" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAPIVisibility(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "AvailableOnMobile")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AvailableOnMobile" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAvailableOnMobile(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "MultipleSubEvents")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "MultipleSubEvents" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setMultipleSubEvents(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "EventClass")
                .equals(reader.getName())) {

          object.setEventClass(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "SubEventType")
                .equals(reader.getName())) {

          // Process the array and step past its final element's end.

          list10.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType.Factory.parse(reader));

          // loop until we find a start element that is not part of this array
          boolean loopDone10 = false;
          while (!loopDone10) {
            // We should be at the end element, but make sure
            while (!reader.isEndElement()) reader.next();
            // Step out of this element
            reader.next();
            // Step to next element event.
            while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
            if (reader.isEndElement()) {
              // two continuous end elements means we are exiting the xml structure
              loopDone10 = true;
            } else {
              if (new javax.xml.namespace.QName("http://www.bartec-systems.com", "SubEventType")
                  .equals(reader.getName())) {
                list10.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType.Factory.parse(reader));

              } else {
                loopDone10 = true;
              }
            }
          }
          // call the converter utility  to convert and set the array

          object.setSubEventType(
              (com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType[])
                  org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                      com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType.class, list10));

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
