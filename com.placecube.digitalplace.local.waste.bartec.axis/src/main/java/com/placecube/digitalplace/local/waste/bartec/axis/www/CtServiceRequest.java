/**
 * CtServiceRequest.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** CtServiceRequest bean class */
@SuppressWarnings({"unchecked", "unused"})
public class CtServiceRequest implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ctServiceRequest
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for Id */
  protected int localId;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getId() {
    return localId;
  }

  /**
   * Auto generated setter method
   *
   * @param param Id
   */
  public void setId(int param) {

    this.localId = param;
  }

  /** field for ServiceCode */
  protected java.lang.String localServiceCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceCodeTracker = false;

  public boolean isServiceCodeSpecified() {
    return localServiceCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceCode() {
    return localServiceCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceCode
   */
  public void setServiceCode(java.lang.String param) {
    localServiceCodeTracker = param != null;

    this.localServiceCode = param;
  }

  /** field for Premises */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises localPremises;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPremisesTracker = false;

  public boolean isPremisesSpecified() {
    return localPremisesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises getPremises() {
    return localPremises;
  }

  /**
   * Auto generated setter method
   *
   * @param param Premises
   */
  public void setPremises(com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises param) {
    localPremisesTracker = param != null;

    this.localPremises = param;
  }

  /** field for Business */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness localBusiness;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localBusinessTracker = false;

  public boolean isBusinessSpecified() {
    return localBusinessTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness getBusiness() {
    return localBusiness;
  }

  /**
   * Auto generated setter method
   *
   * @param param Business
   */
  public void setBusiness(com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness param) {
    localBusinessTracker = param != null;

    this.localBusiness = param;
  }

  /** field for Individual */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtIndividual localIndividual;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIndividualTracker = false;

  public boolean isIndividualSpecified() {
    return localIndividualTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtIndividual
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtIndividual getIndividual() {
    return localIndividual;
  }

  /**
   * Auto generated setter method
   *
   * @param param Individual
   */
  public void setIndividual(com.placecube.digitalplace.local.waste.bartec.axis.www.CtIndividual param) {
    localIndividualTracker = param != null;

    this.localIndividual = param;
  }

  /** field for ServiceLocation */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint localServiceLocation;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceLocationTracker = false;

  public boolean isServiceLocationSpecified() {
    return localServiceLocationTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint getServiceLocation() {
    return localServiceLocation;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceLocation
   */
  public void setServiceLocation(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint param) {
    localServiceLocationTracker = param != null;

    this.localServiceLocation = param;
  }

  /** field for ServiceLocationDescription */
  protected java.lang.String localServiceLocationDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceLocationDescriptionTracker = false;

  public boolean isServiceLocationDescriptionSpecified() {
    return localServiceLocationDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceLocationDescription() {
    return localServiceLocationDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceLocationDescription
   */
  public void setServiceLocationDescription(java.lang.String param) {
    localServiceLocationDescriptionTracker = param != null;

    this.localServiceLocationDescription = param;
  }

  /** field for ReporterContact */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterContact_type0 localReporterContact;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localReporterContactTracker = false;

  public boolean isReporterContactSpecified() {
    return localReporterContactTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterContact_type0
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterContact_type0 getReporterContact() {
    return localReporterContact;
  }

  /**
   * Auto generated setter method
   *
   * @param param ReporterContact
   */
  public void setReporterContact(com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterContact_type0 param) {
    localReporterContactTracker = param != null;

    this.localReporterContact = param;
  }

  /** field for ReporterBusiness */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterBusiness_type0 localReporterBusiness;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localReporterBusinessTracker = false;

  public boolean isReporterBusinessSpecified() {
    return localReporterBusinessTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterBusiness_type0
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterBusiness_type0 getReporterBusiness() {
    return localReporterBusiness;
  }

  /**
   * Auto generated setter method
   *
   * @param param ReporterBusiness
   */
  public void setReporterBusiness(com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterBusiness_type0 param) {
    localReporterBusinessTracker = param != null;

    this.localReporterBusiness = param;
  }

  /** field for DateRequested */
  protected java.util.Calendar localDateRequested;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateRequested() {
    return localDateRequested;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateRequested
   */
  public void setDateRequested(java.util.Calendar param) {

    this.localDateRequested = param;
  }

  /** field for ServiceType */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceType localServiceType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceTypeTracker = false;

  public boolean isServiceTypeSpecified() {
    return localServiceTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceType
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceType getServiceType() {
    return localServiceType;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceType
   */
  public void setServiceType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceType param) {
    localServiceTypeTracker = param != null;

    this.localServiceType = param;
  }

  /** field for ServiceStatus */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceStatus localServiceStatus;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceStatusTracker = false;

  public boolean isServiceStatusSpecified() {
    return localServiceStatusTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceStatus
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceStatus getServiceStatus() {
    return localServiceStatus;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceStatus
   */
  public void setServiceStatus(com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceStatus param) {
    localServiceStatusTracker = param != null;

    this.localServiceStatus = param;
  }

  /** field for Source */
  protected java.lang.String localSource;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSourceTracker = false;

  public boolean isSourceSpecified() {
    return localSourceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSource() {
    return localSource;
  }

  /**
   * Auto generated setter method
   *
   * @param param Source
   */
  public void setSource(java.lang.String param) {
    localSourceTracker = param != null;

    this.localSource = param;
  }

  /** field for ExternalReference */
  protected java.lang.String localExternalReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExternalReferenceTracker = false;

  public boolean isExternalReferenceSpecified() {
    return localExternalReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getExternalReference() {
    return localExternalReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExternalReference
   */
  public void setExternalReference(java.lang.String param) {
    localExternalReferenceTracker = param != null;

    this.localExternalReference = param;
  }

  /** field for SLA */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.SLA_type0 localSLA;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSLATracker = false;

  public boolean isSLASpecified() {
    return localSLATracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.SLA_type0
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.SLA_type0 getSLA() {
    return localSLA;
  }

  /**
   * Auto generated setter method
   *
   * @param param SLA
   */
  public void setSLA(com.placecube.digitalplace.local.waste.bartec.axis.www.SLA_type0 param) {
    localSLATracker = param != null;

    this.localSLA = param;
  }

  /** field for ExtendedData */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtExtendedDataRecord localExtendedData;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExtendedDataTracker = false;

  public boolean isExtendedDataSpecified() {
    return localExtendedDataTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtExtendedDataRecord
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtExtendedDataRecord getExtendedData() {
    return localExtendedData;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExtendedData
   */
  public void setExtendedData(com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtExtendedDataRecord param) {
    localExtendedDataTracker = param != null;

    this.localExtendedData = param;
  }

  /** field for LicenceApplication */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.LicenceApplication_type0 localLicenceApplication;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLicenceApplicationTracker = false;

  public boolean isLicenceApplicationSpecified() {
    return localLicenceApplicationTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.LicenceApplication_type0
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.LicenceApplication_type0 getLicenceApplication() {
    return localLicenceApplication;
  }

  /**
   * Auto generated setter method
   *
   * @param param LicenceApplication
   */
  public void setLicenceApplication(com.placecube.digitalplace.local.waste.bartec.axis.www.LicenceApplication_type0 param) {
    localLicenceApplicationTracker = param != null;

    this.localLicenceApplication = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ctServiceRequest",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ctServiceRequest",
            xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "id", xmlWriter);

    if (localId == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
    }

    xmlWriter.writeEndElement();
    if (localServiceCodeTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "ServiceCode", xmlWriter);

      if (localServiceCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localPremisesTracker) {
      if (localPremises == null) {
        throw new org.apache.axis2.databinding.ADBException("Premises cannot be null!!");
      }
      localPremises.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Premises"), xmlWriter);
    }
    if (localBusinessTracker) {
      if (localBusiness == null) {
        throw new org.apache.axis2.databinding.ADBException("Business cannot be null!!");
      }
      localBusiness.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Business"), xmlWriter);
    }
    if (localIndividualTracker) {
      if (localIndividual == null) {
        throw new org.apache.axis2.databinding.ADBException("Individual cannot be null!!");
      }
      localIndividual.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Individual"), xmlWriter);
    }
    if (localServiceLocationTracker) {
      if (localServiceLocation == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceLocation cannot be null!!");
      }
      localServiceLocation.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceLocation"),
          xmlWriter);
    }
    if (localServiceLocationDescriptionTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "ServiceLocationDescription", xmlWriter);

      if (localServiceLocationDescription == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceLocationDescription cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceLocationDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localReporterContactTracker) {
      if (localReporterContact == null) {
        throw new org.apache.axis2.databinding.ADBException("ReporterContact cannot be null!!");
      }
      localReporterContact.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ReporterContact"),
          xmlWriter);
    }
    if (localReporterBusinessTracker) {
      if (localReporterBusiness == null) {
        throw new org.apache.axis2.databinding.ADBException("ReporterBusiness cannot be null!!");
      }
      localReporterBusiness.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ReporterBusiness"),
          xmlWriter);
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "DateRequested", xmlWriter);

    if (localDateRequested == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateRequested cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateRequested));
    }

    xmlWriter.writeEndElement();
    if (localServiceTypeTracker) {
      if (localServiceType == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceType cannot be null!!");
      }
      localServiceType.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceType"), xmlWriter);
    }
    if (localServiceStatusTracker) {
      if (localServiceStatus == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceStatus cannot be null!!");
      }
      localServiceStatus.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceStatus"),
          xmlWriter);
    }
    if (localSourceTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Source", xmlWriter);

      if (localSource == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Source cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSource);
      }

      xmlWriter.writeEndElement();
    }
    if (localExternalReferenceTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "ExternalReference", xmlWriter);

      if (localExternalReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ExternalReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localExternalReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localSLATracker) {
      if (localSLA == null) {
        throw new org.apache.axis2.databinding.ADBException("SLA cannot be null!!");
      }
      localSLA.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "SLA"), xmlWriter);
    }
    if (localExtendedDataTracker) {
      if (localExtendedData == null) {
        throw new org.apache.axis2.databinding.ADBException("ExtendedData cannot be null!!");
      }
      localExtendedData.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExtendedData"),
          xmlWriter);
    }
    if (localLicenceApplicationTracker) {
      if (localLicenceApplication == null) {
        throw new org.apache.axis2.databinding.ADBException("LicenceApplication cannot be null!!");
      }
      localLicenceApplication.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceApplication"),
          xmlWriter);
    }
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static CtServiceRequest parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      CtServiceRequest object = new CtServiceRequest();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ctServiceRequest".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (CtServiceRequest)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "id")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "id" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Premises")
                .equals(reader.getName())) {

          object.setPremises(com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Business")
                .equals(reader.getName())) {

          object.setBusiness(com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Individual")
                .equals(reader.getName())) {

          object.setIndividual(com.placecube.digitalplace.local.waste.bartec.axis.www.CtIndividual.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceLocation")
                .equals(reader.getName())) {

          object.setServiceLocation(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com", "ServiceLocationDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceLocationDescription" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceLocationDescription(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ReporterContact")
                .equals(reader.getName())) {

          object.setReporterContact(
              com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterContact_type0.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ReporterBusiness")
                .equals(reader.getName())) {

          object.setReporterBusiness(
              com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterBusiness_type0.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "DateRequested")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateRequested" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateRequested(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceType")
                .equals(reader.getName())) {

          object.setServiceType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceType.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceStatus")
                .equals(reader.getName())) {

          object.setServiceStatus(com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceStatus.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Source")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Source" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSource(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExternalReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExternalReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExternalReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "SLA")
                .equals(reader.getName())) {

          object.setSLA(com.placecube.digitalplace.local.waste.bartec.axis.www.SLA_type0.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExtendedData")
                .equals(reader.getName())) {

          object.setExtendedData(
              com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtExtendedDataRecord.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceApplication")
                .equals(reader.getName())) {

          object.setLicenceApplication(
              com.placecube.digitalplace.local.waste.bartec.axis.www.LicenceApplication_type0.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
