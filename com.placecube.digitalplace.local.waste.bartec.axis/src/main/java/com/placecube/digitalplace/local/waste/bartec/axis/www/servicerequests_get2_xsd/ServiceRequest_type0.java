/**
 * ServiceRequest_type0.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd;

/** ServiceRequest_type0 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class ServiceRequest_type0 extends com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceRequest
    implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ServiceRequest_type0
  Namespace URI = http://www.bartec-systems.com/ServiceRequests_Get2.xsd
  Namespace Prefix = ns23
  */

  /** field for Crew */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew localCrew;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCrewTracker = false;

  public boolean isCrewSpecified() {
    return localCrewTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew getCrew() {
    return localCrew;
  }

  /**
   * Auto generated setter method
   *
   * @param param Crew
   */
  public void setCrew(com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew param) {
    localCrewTracker = param != null;

    this.localCrew = param;
  }

  /** field for LandType */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtLandType localLandType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLandTypeTracker = false;

  public boolean isLandTypeSpecified() {
    return localLandTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtLandType
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtLandType getLandType() {
    return localLandType;
  }

  /**
   * Auto generated setter method
   *
   * @param param LandType
   */
  public void setLandType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtLandType param) {
    localLandTypeTracker = param != null;

    this.localLandType = param;
  }

  /** field for Notes */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtServiceNote localNotes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localNotesTracker = false;

  public boolean isNotesSpecified() {
    return localNotesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtServiceNote
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtServiceNote getNotes() {
    return localNotes;
  }

  /**
   * Auto generated setter method
   *
   * @param param Notes
   */
  public void setNotes(com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtServiceNote param) {
    localNotesTracker = param != null;

    this.localNotes = param;
  }

  /** field for AttachedDocuments */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.AttachedDocuments_type2
      localAttachedDocuments;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAttachedDocumentsTracker = false;

  public boolean isAttachedDocumentsSpecified() {
    return localAttachedDocumentsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.AttachedDocuments_type2
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.AttachedDocuments_type2
      getAttachedDocuments() {
    return localAttachedDocuments;
  }

  /**
   * Auto generated setter method
   *
   * @param param AttachedDocuments
   */
  public void setAttachedDocuments(
      com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.AttachedDocuments_type2 param) {
    localAttachedDocumentsTracker = param != null;

    this.localAttachedDocuments = param;
  }

  /** field for RelatedServiceRequests */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedServiceRequest
      localRelatedServiceRequests;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRelatedServiceRequestsTracker = false;

  public boolean isRelatedServiceRequestsSpecified() {
    return localRelatedServiceRequestsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedServiceRequest
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedServiceRequest
      getRelatedServiceRequests() {
    return localRelatedServiceRequests;
  }

  /**
   * Auto generated setter method
   *
   * @param param RelatedServiceRequests
   */
  public void setRelatedServiceRequests(
      com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedServiceRequest param) {
    localRelatedServiceRequestsTracker = param != null;

    this.localRelatedServiceRequests = param;
  }

  /** field for RelatedPremises */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedLocation
      localRelatedPremises;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRelatedPremisesTracker = false;

  public boolean isRelatedPremisesSpecified() {
    return localRelatedPremisesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedLocation
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedLocation
      getRelatedPremises() {
    return localRelatedPremises;
  }

  /**
   * Auto generated setter method
   *
   * @param param RelatedPremises
   */
  public void setRelatedPremises(
      com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedLocation param) {
    localRelatedPremisesTracker = param != null;

    this.localRelatedPremises = param;
  }

  /** field for DateClosed */
  protected java.util.Calendar localDateClosed;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateClosed() {
    return localDateClosed;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateClosed
   */
  public void setDateClosed(java.util.Calendar param) {

    this.localDateClosed = param;
  }

  /** field for ClosingCode */
  protected java.lang.String localClosingCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localClosingCodeTracker = false;

  public boolean isClosingCodeSpecified() {
    return localClosingCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getClosingCode() {
    return localClosingCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosingCode
   */
  public void setClosingCode(java.lang.String param) {
    localClosingCodeTracker = param != null;

    this.localClosingCode = param;
  }

  /** field for ClosingComments */
  protected java.lang.String localClosingComments;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localClosingCommentsTracker = false;

  public boolean isClosingCommentsSpecified() {
    return localClosingCommentsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getClosingComments() {
    return localClosingComments;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosingComments
   */
  public void setClosingComments(java.lang.String param) {
    localClosingCommentsTracker = param != null;

    this.localClosingComments = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    java.lang.String namespacePrefix =
        registerPrefix(xmlWriter, "http://www.bartec-systems.com/ServiceRequests_Get2.xsd");
    if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
      writeAttribute(
          "xsi",
          "http://www.w3.org/2001/XMLSchema-instance",
          "type",
          namespacePrefix + ":ServiceRequest_type0",
          xmlWriter);
    } else {
      writeAttribute(
          "xsi",
          "http://www.w3.org/2001/XMLSchema-instance",
          "type",
          "ServiceRequest_type0",
          xmlWriter);
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "id", xmlWriter);

    if (localId == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
    }

    xmlWriter.writeEndElement();
    if (localServiceCodeTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "ServiceCode", xmlWriter);

      if (localServiceCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localPremisesTracker) {
      if (localPremises == null) {
        throw new org.apache.axis2.databinding.ADBException("Premises cannot be null!!");
      }
      localPremises.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Premises"), xmlWriter);
    }
    if (localBusinessTracker) {
      if (localBusiness == null) {
        throw new org.apache.axis2.databinding.ADBException("Business cannot be null!!");
      }
      localBusiness.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Business"), xmlWriter);
    }
    if (localIndividualTracker) {
      if (localIndividual == null) {
        throw new org.apache.axis2.databinding.ADBException("Individual cannot be null!!");
      }
      localIndividual.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Individual"), xmlWriter);
    }
    if (localServiceLocationTracker) {
      if (localServiceLocation == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceLocation cannot be null!!");
      }
      localServiceLocation.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceLocation"),
          xmlWriter);
    }
    if (localServiceLocationDescriptionTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "ServiceLocationDescription", xmlWriter);

      if (localServiceLocationDescription == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "ServiceLocationDescription cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceLocationDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localReporterContactTracker) {
      if (localReporterContact == null) {
        throw new org.apache.axis2.databinding.ADBException("ReporterContact cannot be null!!");
      }
      localReporterContact.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ReporterContact"),
          xmlWriter);
    }
    if (localReporterBusinessTracker) {
      if (localReporterBusiness == null) {
        throw new org.apache.axis2.databinding.ADBException("ReporterBusiness cannot be null!!");
      }
      localReporterBusiness.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ReporterBusiness"),
          xmlWriter);
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "DateRequested", xmlWriter);

    if (localDateRequested == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateRequested cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateRequested));
    }

    xmlWriter.writeEndElement();
    if (localServiceTypeTracker) {
      if (localServiceType == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceType cannot be null!!");
      }
      localServiceType.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceType"), xmlWriter);
    }
    if (localServiceStatusTracker) {
      if (localServiceStatus == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceStatus cannot be null!!");
      }
      localServiceStatus.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceStatus"),
          xmlWriter);
    }
    if (localSourceTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Source", xmlWriter);

      if (localSource == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Source cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSource);
      }

      xmlWriter.writeEndElement();
    }
    if (localExternalReferenceTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "ExternalReference", xmlWriter);

      if (localExternalReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ExternalReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localExternalReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localSLATracker) {
      if (localSLA == null) {
        throw new org.apache.axis2.databinding.ADBException("SLA cannot be null!!");
      }
      localSLA.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "SLA"), xmlWriter);
    }
    if (localExtendedDataTracker) {
      if (localExtendedData == null) {
        throw new org.apache.axis2.databinding.ADBException("ExtendedData cannot be null!!");
      }
      localExtendedData.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExtendedData"),
          xmlWriter);
    }
    if (localLicenceApplicationTracker) {
      if (localLicenceApplication == null) {
        throw new org.apache.axis2.databinding.ADBException("LicenceApplication cannot be null!!");
      }
      localLicenceApplication.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceApplication"),
          xmlWriter);
    }
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    if (localCrewTracker) {
      if (localCrew == null) {
        throw new org.apache.axis2.databinding.ADBException("Crew cannot be null!!");
      }
      localCrew.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "Crew"),
          xmlWriter);
    }
    if (localLandTypeTracker) {
      if (localLandType == null) {
        throw new org.apache.axis2.databinding.ADBException("LandType cannot be null!!");
      }
      localLandType.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "LandType"),
          xmlWriter);
    }
    if (localNotesTracker) {
      if (localNotes == null) {
        throw new org.apache.axis2.databinding.ADBException("Notes cannot be null!!");
      }
      localNotes.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "Notes"),
          xmlWriter);
    }
    if (localAttachedDocumentsTracker) {
      if (localAttachedDocuments == null) {
        throw new org.apache.axis2.databinding.ADBException("AttachedDocuments cannot be null!!");
      }
      localAttachedDocuments.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "AttachedDocuments"),
          xmlWriter);
    }
    if (localRelatedServiceRequestsTracker) {
      if (localRelatedServiceRequests == null) {
        throw new org.apache.axis2.databinding.ADBException(
            "RelatedServiceRequests cannot be null!!");
      }
      localRelatedServiceRequests.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "RelatedServiceRequests"),
          xmlWriter);
    }
    if (localRelatedPremisesTracker) {
      if (localRelatedPremises == null) {
        throw new org.apache.axis2.databinding.ADBException("RelatedPremises cannot be null!!");
      }
      localRelatedPremises.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "RelatedPremises"),
          xmlWriter);
    }
    namespace = "http://www.bartec-systems.com/ServiceRequests_Get2.xsd";
    writeStartElement(null, namespace, "DateClosed", xmlWriter);

    if (localDateClosed == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateClosed cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateClosed));
    }

    xmlWriter.writeEndElement();
    if (localClosingCodeTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_Get2.xsd";
      writeStartElement(null, namespace, "ClosingCode", xmlWriter);

      if (localClosingCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ClosingCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localClosingCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localClosingCommentsTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_Get2.xsd";
      writeStartElement(null, namespace, "ClosingComments", xmlWriter);

      if (localClosingComments == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ClosingComments cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localClosingComments);
      }

      xmlWriter.writeEndElement();
    }
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "RecordStamp"),
          xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/ServiceRequests_Get2.xsd")) {
      return "ns23";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static ServiceRequest_type0 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      ServiceRequest_type0 object = new ServiceRequest_type0();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ServiceRequest_type0".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (ServiceRequest_type0)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "id")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "id" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Premises")
                .equals(reader.getName())) {

          object.setPremises(com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Business")
                .equals(reader.getName())) {

          object.setBusiness(com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Individual")
                .equals(reader.getName())) {

          object.setIndividual(com.placecube.digitalplace.local.waste.bartec.axis.www.CtIndividual.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceLocation")
                .equals(reader.getName())) {

          object.setServiceLocation(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com", "ServiceLocationDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceLocationDescription" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceLocationDescription(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ReporterContact")
                .equals(reader.getName())) {

          object.setReporterContact(
              com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterContact_type0.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ReporterBusiness")
                .equals(reader.getName())) {

          object.setReporterBusiness(
              com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterBusiness_type0.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "DateRequested")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateRequested" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateRequested(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceType")
                .equals(reader.getName())) {

          object.setServiceType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceType.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ServiceStatus")
                .equals(reader.getName())) {

          object.setServiceStatus(com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceStatus.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Source")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Source" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSource(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExternalReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExternalReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExternalReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "SLA")
                .equals(reader.getName())) {

          object.setSLA(com.placecube.digitalplace.local.waste.bartec.axis.www.SLA_type0.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExtendedData")
                .equals(reader.getName())) {

          object.setExtendedData(
              com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtExtendedDataRecord.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceApplication")
                .equals(reader.getName())) {

          object.setLicenceApplication(
              com.placecube.digitalplace.local.waste.bartec.axis.www.LicenceApplication_type0.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "Crew")
                .equals(reader.getName())) {

          object.setCrew(com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "LandType")
                .equals(reader.getName())) {

          object.setLandType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtLandType.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "Notes")
                .equals(reader.getName())) {

          object.setNotes(
              com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtServiceNote.Factory.parse(
                  reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "AttachedDocuments")
                .equals(reader.getName())) {

          object.setAttachedDocuments(
              com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.AttachedDocuments_type2.Factory.parse(
                  reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd",
                    "RelatedServiceRequests")
                .equals(reader.getName())) {

          object.setRelatedServiceRequests(
              com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedServiceRequest.Factory
                  .parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "RelatedPremises")
                .equals(reader.getName())) {

          object.setRelatedPremises(
              com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedLocation.Factory
                  .parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "DateClosed")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateClosed" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateClosed(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "ClosingCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosingCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosingCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "ClosingComments")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosingComments" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosingComments(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_Get2.xsd", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
