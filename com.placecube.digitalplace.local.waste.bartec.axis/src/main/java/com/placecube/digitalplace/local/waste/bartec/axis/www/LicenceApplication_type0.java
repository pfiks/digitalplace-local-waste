/**
 * LicenceApplication_type0.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** LicenceApplication_type0 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class LicenceApplication_type0 implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = LicenceApplication_type0
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for AccountReference */
  protected java.lang.String localAccountReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountReferenceTracker = false;

  public boolean isAccountReferenceSpecified() {
    return localAccountReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAccountReference() {
    return localAccountReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountReference
   */
  public void setAccountReference(java.lang.String param) {
    localAccountReferenceTracker = param != null;

    this.localAccountReference = param;
  }

  /** field for Fee */
  protected java.math.BigDecimal localFee;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFeeTracker = false;

  public boolean isFeeSpecified() {
    return localFeeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getFee() {
    return localFee;
  }

  /**
   * Auto generated setter method
   *
   * @param param Fee
   */
  public void setFee(java.math.BigDecimal param) {
    localFeeTracker = param != null;

    this.localFee = param;
  }

  /** field for Deposit */
  protected java.math.BigDecimal localDeposit;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDepositTracker = false;

  public boolean isDepositSpecified() {
    return localDepositTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getDeposit() {
    return localDeposit;
  }

  /**
   * Auto generated setter method
   *
   * @param param Deposit
   */
  public void setDeposit(java.math.BigDecimal param) {
    localDepositTracker = param != null;

    this.localDeposit = param;
  }

  /** field for PaymentType */
  protected java.lang.String localPaymentType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentTypeTracker = false;

  public boolean isPaymentTypeSpecified() {
    return localPaymentTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPaymentType() {
    return localPaymentType;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentType
   */
  public void setPaymentType(java.lang.String param) {
    localPaymentTypeTracker = param != null;

    this.localPaymentType = param;
  }

  /** field for PaymentReference */
  protected java.lang.String localPaymentReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPaymentReferenceTracker = false;

  public boolean isPaymentReferenceSpecified() {
    return localPaymentReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPaymentReference() {
    return localPaymentReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param PaymentReference
   */
  public void setPaymentReference(java.lang.String param) {
    localPaymentReferenceTracker = param != null;

    this.localPaymentReference = param;
  }

  /** field for LicenceNumber */
  protected java.lang.String localLicenceNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLicenceNumberTracker = false;

  public boolean isLicenceNumberSpecified() {
    return localLicenceNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getLicenceNumber() {
    return localLicenceNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param LicenceNumber
   */
  public void setLicenceNumber(java.lang.String param) {
    localLicenceNumberTracker = param != null;

    this.localLicenceNumber = param;
  }

  /** field for RequestDetails */
  protected java.lang.String localRequestDetails;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRequestDetailsTracker = false;

  public boolean isRequestDetailsSpecified() {
    return localRequestDetailsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getRequestDetails() {
    return localRequestDetails;
  }

  /**
   * Auto generated setter method
   *
   * @param param RequestDetails
   */
  public void setRequestDetails(java.lang.String param) {
    localRequestDetailsTracker = param != null;

    this.localRequestDetails = param;
  }

  /** field for WebPage */
  protected java.lang.String localWebPage;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWebPageTracker = false;

  public boolean isWebPageSpecified() {
    return localWebPageTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWebPage() {
    return localWebPage;
  }

  /**
   * Auto generated setter method
   *
   * @param param WebPage
   */
  public void setWebPage(java.lang.String param) {
    localWebPageTracker = param != null;

    this.localWebPage = param;
  }

  /** field for WebPageType */
  protected java.lang.String localWebPageType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWebPageTypeTracker = false;

  public boolean isWebPageTypeSpecified() {
    return localWebPageTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWebPageType() {
    return localWebPageType;
  }

  /**
   * Auto generated setter method
   *
   * @param param WebPageType
   */
  public void setWebPageType(java.lang.String param) {
    localWebPageTypeTracker = param != null;

    this.localWebPageType = param;
  }

  /** field for FeeAdjustment */
  protected java.math.BigDecimal localFeeAdjustment;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFeeAdjustmentTracker = false;

  public boolean isFeeAdjustmentSpecified() {
    return localFeeAdjustmentTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getFeeAdjustment() {
    return localFeeAdjustment;
  }

  /**
   * Auto generated setter method
   *
   * @param param FeeAdjustment
   */
  public void setFeeAdjustment(java.math.BigDecimal param) {
    localFeeAdjustmentTracker = param != null;

    this.localFeeAdjustment = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":LicenceApplication_type0",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "LicenceApplication_type0",
            xmlWriter);
      }
    }
    if (localAccountReferenceTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "AccountReference", xmlWriter);

      if (localAccountReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("AccountReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAccountReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localFeeTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Fee", xmlWriter);

      if (localFee == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Fee cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFee));
      }

      xmlWriter.writeEndElement();
    }
    if (localDepositTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Deposit", xmlWriter);

      if (localDeposit == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Deposit cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeposit));
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentTypeTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "PaymentType", xmlWriter);

      if (localPaymentType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("PaymentType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localPaymentType);
      }

      xmlWriter.writeEndElement();
    }
    if (localPaymentReferenceTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "PaymentReference", xmlWriter);

      if (localPaymentReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("PaymentReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localPaymentReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localLicenceNumberTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "LicenceNumber", xmlWriter);

      if (localLicenceNumber == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("LicenceNumber cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localLicenceNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localRequestDetailsTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "RequestDetails", xmlWriter);

      if (localRequestDetails == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("RequestDetails cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localRequestDetails);
      }

      xmlWriter.writeEndElement();
    }
    if (localWebPageTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "WebPage", xmlWriter);

      if (localWebPage == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WebPage cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localWebPage);
      }

      xmlWriter.writeEndElement();
    }
    if (localWebPageTypeTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "WebPageType", xmlWriter);

      if (localWebPageType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WebPageType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localWebPageType);
      }

      xmlWriter.writeEndElement();
    }
    if (localFeeAdjustmentTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "FeeAdjustment", xmlWriter);

      if (localFeeAdjustment == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("FeeAdjustment cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFeeAdjustment));
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static LicenceApplication_type0 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      LicenceApplication_type0 object = new LicenceApplication_type0();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"LicenceApplication_type0".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (LicenceApplication_type0)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "AccountReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Fee")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Fee" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setFee(org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Deposit")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Deposit" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDeposit(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "PaymentType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PaymentType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPaymentType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "PaymentReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PaymentReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPaymentReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "LicenceNumber" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLicenceNumber(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RequestDetails")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "RequestDetails" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setRequestDetails(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "WebPage")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WebPage" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWebPage(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "WebPageType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WebPageType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWebPageType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "FeeAdjustment")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "FeeAdjustment" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setFeeAdjustment(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
