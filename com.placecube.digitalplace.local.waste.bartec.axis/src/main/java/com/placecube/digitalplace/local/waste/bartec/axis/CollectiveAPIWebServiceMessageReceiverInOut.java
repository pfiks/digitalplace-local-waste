/**
 * CollectiveAPIWebServiceMessageReceiverInOut.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.0 Built on : Aug 01,
 * 2021 (07:27:19 HST)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/** CollectiveAPIWebServiceMessageReceiverInOut message receiver */
public class CollectiveAPIWebServiceMessageReceiverInOut
    extends org.apache.axis2.receivers.AbstractInOutMessageReceiver {

  public void invokeBusinessLogic(
      org.apache.axis2.context.MessageContext msgContext,
      org.apache.axis2.context.MessageContext newMsgContext)
      throws org.apache.axis2.AxisFault {

    try {

      // get the implementation class for the Web Service
      Object obj = getTheImplementationObject(msgContext);

      CollectiveAPIWebServiceSkeleton skel = (CollectiveAPIWebServiceSkeleton) obj;
      // Out Envelop
      org.apache.axiom.soap.SOAPEnvelope envelope = null;
      // Find the axisOperation that has been set by the Dispatch phase.
      org.apache.axis2.description.AxisOperation op =
          msgContext.getOperationContext().getAxisOperation();
      if (op == null) {
        throw new org.apache.axis2.AxisFault(
            "Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
      }

      java.lang.String methodName;
      if ((op.getName() != null)
          && ((methodName =
                  org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(
                      op.getName().getLocalPart()))
              != null)) {

        if ("businesses_Documents_Get".equals(methodName)) {

          Businesses_Documents_GetResponse businesses_Documents_GetResponse393 =
              null;
          Businesses_Documents_Get wrappedParam =
              (Businesses_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Businesses_Documents_Get.class);

          businesses_Documents_GetResponse393 = skel.businesses_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  businesses_Documents_GetResponse393,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Businesses_Documents_GetResponse"));
        } else if ("features_Classes_Get".equals(methodName)) {

          Features_Classes_GetResponse features_Classes_GetResponse395 = null;
          Features_Classes_Get wrappedParam =
              (Features_Classes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Classes_Get.class);

          features_Classes_GetResponse395 = skel.features_Classes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Classes_GetResponse395,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Classes_GetResponse"));
        } else if ("premises_Events_Get".equals(methodName)) {

          Premises_Events_GetResponse premises_Events_GetResponse397 = null;
          Premises_Events_Get wrappedParam =
              (Premises_Events_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Events_Get.class);

          premises_Events_GetResponse397 = skel.premises_Events_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Events_GetResponse397,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Events_GetResponse"));
        } else if ("premises_FutureWorkpacks_Get".equals(methodName)) {

          Premises_FutureWorkpacks_GetResponse
              premises_FutureWorkpacks_GetResponse399 = null;
          Premises_FutureWorkpacks_Get wrappedParam =
              (Premises_FutureWorkpacks_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_FutureWorkpacks_Get.class);

          premises_FutureWorkpacks_GetResponse399 = skel.premises_FutureWorkpacks_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_FutureWorkpacks_GetResponse399,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_FutureWorkpacks_GetResponse"));
        } else if ("premises_Get".equals(methodName)) {

          Premises_GetResponse premises_GetResponse401 = null;
          Premises_Get wrappedParam =
              (Premises_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Get.class);

          premises_GetResponse401 = skel.premises_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_GetResponse401,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_GetResponse"));
        } else if ("serviceRequests_SLAs_Get".equals(methodName)) {

          ServiceRequests_SLAs_GetResponse serviceRequests_SLAs_GetResponse403 =
              null;
          ServiceRequests_SLAs_Get wrappedParam =
              (ServiceRequests_SLAs_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_SLAs_Get.class);

          serviceRequests_SLAs_GetResponse403 = skel.serviceRequests_SLAs_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_SLAs_GetResponse403,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_SLAs_GetResponse"));
        } else if ("vehicles_Get".equals(methodName)) {

          Vehicles_GetResponse vehicles_GetResponse405 = null;
          Vehicles_Get wrappedParam =
              (Vehicles_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Vehicles_Get.class);

          vehicles_GetResponse405 = skel.vehicles_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  vehicles_GetResponse405,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Vehicles_GetResponse"));
        } else if ("streets_Attributes_Get".equals(methodName)) {

          Streets_Attributes_GetResponse streets_Attributes_GetResponse407 =
              null;
          Streets_Attributes_Get wrappedParam =
              (Streets_Attributes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Streets_Attributes_Get.class);

          streets_Attributes_GetResponse407 = skel.streets_Attributes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  streets_Attributes_GetResponse407,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Streets_Attributes_GetResponse"));
        } else if ("system_ExtendedDataDefinitions_Get".equals(methodName)) {

          System_ExtendedDataDefinitions_GetResponse
              system_ExtendedDataDefinitions_GetResponse409 = null;
          System_ExtendedDataDefinitions_Get wrappedParam =
              (System_ExtendedDataDefinitions_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      System_ExtendedDataDefinitions_Get.class);

          system_ExtendedDataDefinitions_GetResponse409 =
              skel.system_ExtendedDataDefinitions_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  system_ExtendedDataDefinitions_GetResponse409,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "System_ExtendedDataDefinitions_GetResponse"));
        } else if ("features_Conditions_Get".equals(methodName)) {

          Features_Conditions_GetResponse features_Conditions_GetResponse411 =
              null;
          Features_Conditions_Get wrappedParam =
              (Features_Conditions_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Conditions_Get.class);

          features_Conditions_GetResponse411 = skel.features_Conditions_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Conditions_GetResponse411,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Conditions_GetResponse"));
        } else if ("serviceRequest_Create".equals(methodName)) {

          ServiceRequest_CreateResponse serviceRequest_CreateResponse413 = null;
          ServiceRequest_Create wrappedParam =
              (ServiceRequest_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Create.class);

          serviceRequest_CreateResponse413 = skel.serviceRequest_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_CreateResponse413,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequest_CreateResponse"));
        } else if ("crews_Get".equals(methodName)) {

          Crews_GetResponse crews_GetResponse415 = null;
          Crews_Get wrappedParam =
              (Crews_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Crews_Get.class);

          crews_GetResponse415 = skel.crews_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  crews_GetResponse415,
                  false,
                  new javax.xml.namespace.QName("http://bartec-systems.com/", "Crews_GetResponse"));
        } else if ("serviceRequest_Document_Create".equals(methodName)) {

          ServiceRequest_Document_CreateResponse
              serviceRequest_Document_CreateResponse417 = null;
          ServiceRequest_Document_Create wrappedParam =
              (ServiceRequest_Document_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Document_Create.class);

          serviceRequest_Document_CreateResponse417 =
              skel.serviceRequest_Document_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_Document_CreateResponse417,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequest_Document_CreateResponse"));
        } else if ("serviceRequests_Statuses_Get".equals(methodName)) {

          ServiceRequests_Statuses_GetResponse
              serviceRequests_Statuses_GetResponse419 = null;
          ServiceRequests_Statuses_Get wrappedParam =
              (ServiceRequests_Statuses_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Statuses_Get.class);

          serviceRequests_Statuses_GetResponse419 = skel.serviceRequests_Statuses_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Statuses_GetResponse419,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_Statuses_GetResponse"));
        } else if ("workpack_Note_Create".equals(methodName)) {

          Workpack_Note_CreateResponse workpack_Note_CreateResponse421 = null;
          Workpack_Note_Create wrappedParam =
              (Workpack_Note_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Workpack_Note_Create.class);

          workpack_Note_CreateResponse421 = skel.workpack_Note_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  workpack_Note_CreateResponse421,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Workpack_Note_CreateResponse"));
        } else if ("inspections_Get".equals(methodName)) {

          Inspections_GetResponse inspections_GetResponse423 = null;
          Inspections_Get wrappedParam =
              (Inspections_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Inspections_Get.class);

          inspections_GetResponse423 = skel.inspections_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  inspections_GetResponse423,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Inspections_GetResponse"));
        } else if ("workPacks_Get".equals(methodName)) {

          WorkPacks_GetResponse workPacks_GetResponse425 = null;
          WorkPacks_Get wrappedParam =
              (WorkPacks_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      WorkPacks_Get.class);

          workPacks_GetResponse425 = skel.workPacks_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  workPacks_GetResponse425,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "WorkPacks_GetResponse"));
        } else if ("streets_Get".equals(methodName)) {

          Streets_GetResponse streets_GetResponse427 = null;
          Streets_Get wrappedParam =
              (Streets_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Streets_Get.class);

          streets_GetResponse427 = skel.streets_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  streets_GetResponse427,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Streets_GetResponse"));
        } else if ("site_Document_Create".equals(methodName)) {

          Site_Document_CreateResponse site_Document_CreateResponse429 = null;
          Site_Document_Create wrappedParam =
              (Site_Document_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Site_Document_Create.class);

          site_Document_CreateResponse429 = skel.site_Document_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  site_Document_CreateResponse429,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Site_Document_CreateResponse"));
        } else if ("serviceRequests_Notes_Get".equals(methodName)) {

          ServiceRequests_Notes_GetResponse
              serviceRequests_Notes_GetResponse431 = null;
          ServiceRequests_Notes_Get wrappedParam =
              (ServiceRequests_Notes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Notes_Get.class);

          serviceRequests_Notes_GetResponse431 = skel.serviceRequests_Notes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Notes_GetResponse431,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_Notes_GetResponse"));
        } else if ("cases_Documents_Get".equals(methodName)) {

          Cases_Documents_GetResponse cases_Documents_GetResponse433 = null;
          Cases_Documents_Get wrappedParam =
              (Cases_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Cases_Documents_Get.class);

          cases_Documents_GetResponse433 = skel.cases_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  cases_Documents_GetResponse433,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Cases_Documents_GetResponse"));
        } else if ("features_Notes_Get".equals(methodName)) {

          Features_Notes_GetResponse features_Notes_GetResponse435 = null;
          Features_Notes_Get wrappedParam =
              (Features_Notes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Notes_Get.class);

          features_Notes_GetResponse435 = skel.features_Notes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Notes_GetResponse435,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Notes_GetResponse"));
        } else if ("businesses_SubStatuses_Get".equals(methodName)) {

          Businesses_SubStatuses_GetResponse
              businesses_SubStatuses_GetResponse437 = null;
          Businesses_SubStatuses_Get wrappedParam =
              (Businesses_SubStatuses_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Businesses_SubStatuses_Get.class);

          businesses_SubStatuses_GetResponse437 = skel.businesses_SubStatuses_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  businesses_SubStatuses_GetResponse437,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Businesses_SubStatuses_GetResponse"));
        } else if ("premises_Documents_Get".equals(methodName)) {

          Premises_Documents_GetResponse premises_Documents_GetResponse439 =
              null;
          Premises_Documents_Get wrappedParam =
              (Premises_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Documents_Get.class);

          premises_Documents_GetResponse439 = skel.premises_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Documents_GetResponse439,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Documents_GetResponse"));
        } else if ("streets_Detail_Get".equals(methodName)) {

          Streets_Detail_GetResponse streets_Detail_GetResponse441 = null;
          Streets_Detail_Get wrappedParam =
              (Streets_Detail_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Streets_Detail_Get.class);

          streets_Detail_GetResponse441 = skel.streets_Detail_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  streets_Detail_GetResponse441,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Streets_Detail_GetResponse"));
        } else if ("serviceRequests_Updates_Get".equals(methodName)) {

          ServiceRequests_Updates_GetResponse
              serviceRequests_Updates_GetResponse443 = null;
          ServiceRequests_Updates_Get wrappedParam =
              (ServiceRequests_Updates_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Updates_Get.class);

          serviceRequests_Updates_GetResponse443 = skel.serviceRequests_Updates_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Updates_GetResponse443,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_Updates_GetResponse"));
        } else if ("features_Types_Get".equals(methodName)) {

          Features_Types_GetResponse features_Types_GetResponse445 = null;
          Features_Types_Get wrappedParam =
              (Features_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Types_Get.class);

          features_Types_GetResponse445 = skel.features_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Types_GetResponse445,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Types_GetResponse"));
        } else if ("jobs_Get".equals(methodName)) {

          Jobs_GetResponse jobs_GetResponse447 = null;
          Jobs_Get wrappedParam =
              (Jobs_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Jobs_Get.class);

          jobs_GetResponse447 = skel.jobs_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  jobs_GetResponse447,
                  false,
                  new javax.xml.namespace.QName("http://bartec-systems.com/", "Jobs_GetResponse"));
        } else if ("jobs_Close".equals(methodName)) {

          Jobs_CloseResponse jobs_CloseResponse449 = null;
          Jobs_Close wrappedParam =
              (Jobs_Close)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Jobs_Close.class);

          jobs_CloseResponse449 = skel.jobs_Close(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  jobs_CloseResponse449,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Jobs_CloseResponse"));
        } else if ("serviceRequests_Types_Get".equals(methodName)) {

          ServiceRequests_Types_GetResponse
              serviceRequests_Types_GetResponse451 = null;
          ServiceRequests_Types_Get wrappedParam =
              (ServiceRequests_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Types_Get.class);

          serviceRequests_Types_GetResponse451 = skel.serviceRequests_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Types_GetResponse451,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_Types_GetResponse"));
        } else if ("features_Categories_Get".equals(methodName)) {

          Features_Categories_GetResponse features_Categories_GetResponse453 =
              null;
          Features_Categories_Get wrappedParam =
              (Features_Categories_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Categories_Get.class);

          features_Categories_GetResponse453 = skel.features_Categories_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Categories_GetResponse453,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Categories_GetResponse"));
        } else if ("licences_Get".equals(methodName)) {

          Licences_GetResponse licences_GetResponse455 = null;
          Licences_Get wrappedParam =
              (Licences_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Licences_Get.class);

          licences_GetResponse455 = skel.licences_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  licences_GetResponse455,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Licences_GetResponse"));
        } else if ("premises_Detail_Get".equals(methodName)) {

          Premises_Detail_GetResponse premises_Detail_GetResponse457 = null;
          Premises_Detail_Get wrappedParam =
              (Premises_Detail_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Detail_Get.class);

          premises_Detail_GetResponse457 = skel.premises_Detail_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Detail_GetResponse457,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Detail_GetResponse"));
        } else if ("features_Manufacturers_Get".equals(methodName)) {

          Features_Manufacturers_GetResponse
              features_Manufacturers_GetResponse459 = null;
          Features_Manufacturers_Get wrappedParam =
              (Features_Manufacturers_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Manufacturers_Get.class);

          features_Manufacturers_GetResponse459 = skel.features_Manufacturers_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Manufacturers_GetResponse459,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Manufacturers_GetResponse"));
        } else if ("premises_Events_Classes_Get".equals(methodName)) {

          Premises_Events_Classes_GetResponse
              premises_Events_Classes_GetResponse461 = null;
          Premises_Events_Classes_Get wrappedParam =
              (Premises_Events_Classes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Events_Classes_Get.class);

          premises_Events_Classes_GetResponse461 = skel.premises_Events_Classes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Events_Classes_GetResponse461,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Events_Classes_GetResponse"));
        } else if ("sites_Documents_Get".equals(methodName)) {

          Sites_Documents_GetResponse sites_Documents_GetResponse463 = null;
          Sites_Documents_Get wrappedParam =
              (Sites_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Sites_Documents_Get.class);

          sites_Documents_GetResponse463 = skel.sites_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  sites_Documents_GetResponse463,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Sites_Documents_GetResponse"));
        } else if ("serviceRequest_Appointment_Reservation_Extend".equals(methodName)) {

          ServiceRequest_Appointment_Reservation_ExtendResponse
              serviceRequest_Appointment_Reservation_ExtendResponse465 = null;
          ServiceRequest_Appointment_Reservation_Extend wrappedParam =
              (ServiceRequest_Appointment_Reservation_Extend)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Appointment_Reservation_Extend.class);

          serviceRequest_Appointment_Reservation_ExtendResponse465 =
              skel.serviceRequest_Appointment_Reservation_Extend(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_Appointment_Reservation_ExtendResponse465,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/",
                      "ServiceRequest_Appointment_Reservation_ExtendResponse"));
        } else if ("premises_Events_Documents_Get".equals(methodName)) {

          Premises_Events_Documents_GetResponse
              premises_Events_Documents_GetResponse467 = null;
          Premises_Events_Documents_Get wrappedParam =
              (Premises_Events_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Events_Documents_Get.class);

          premises_Events_Documents_GetResponse467 =
              skel.premises_Events_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Events_Documents_GetResponse467,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Events_Documents_GetResponse"));
        } else if ("workpacks_Notes_Types_Get".equals(methodName)) {

          Workpacks_Notes_Types_GetResponse
              workpacks_Notes_Types_GetResponse469 = null;
          Workpacks_Notes_Types_Get wrappedParam =
              (Workpacks_Notes_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Workpacks_Notes_Types_Get.class);

          workpacks_Notes_Types_GetResponse469 = skel.workpacks_Notes_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  workpacks_Notes_Types_GetResponse469,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Workpacks_Notes_Types_GetResponse"));
        } else if ("jobs_FeatureScheduleDates_Get".equals(methodName)) {

          Jobs_FeatureScheduleDates_GetResponse
              jobs_FeatureScheduleDates_GetResponse471 = null;
          Jobs_FeatureScheduleDates_Get wrappedParam =
              (Jobs_FeatureScheduleDates_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Jobs_FeatureScheduleDates_Get.class);

          jobs_FeatureScheduleDates_GetResponse471 =
              skel.jobs_FeatureScheduleDates_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  jobs_FeatureScheduleDates_GetResponse471,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Jobs_FeatureScheduleDates_GetResponse"));
        } else if ("vehicle_Message_Create".equals(methodName)) {

          Vehicle_Message_CreateResponse vehicle_Message_CreateResponse473 =
              null;
          Vehicle_Message_Create wrappedParam =
              (Vehicle_Message_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Vehicle_Message_Create.class);

          vehicle_Message_CreateResponse473 = skel.vehicle_Message_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  vehicle_Message_CreateResponse473,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Vehicle_Message_CreateResponse"));
        } else if ("workGroups_Get".equals(methodName)) {

          WorkGroups_GetResponse workGroups_GetResponse475 = null;
          WorkGroups_Get wrappedParam =
              (WorkGroups_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      WorkGroups_Get.class);

          workGroups_GetResponse475 = skel.workGroups_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  workGroups_GetResponse475,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "WorkGroups_GetResponse"));
        } else if ("serviceRequests_Appointments_Availability_Get".equals(methodName)) {

          ServiceRequests_Appointments_Availability_GetResponse
              serviceRequests_Appointments_Availability_GetResponse477 = null;
          ServiceRequests_Appointments_Availability_Get wrappedParam =
              (ServiceRequests_Appointments_Availability_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Appointments_Availability_Get.class);

          serviceRequests_Appointments_Availability_GetResponse477 =
              skel.serviceRequests_Appointments_Availability_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Appointments_Availability_GetResponse477,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/",
                      "ServiceRequests_Appointments_Availability_GetResponse"));
        } else if ("features_Schedules_Get".equals(methodName)) {

          Features_Schedules_GetResponse features_Schedules_GetResponse479 =
              null;
          Features_Schedules_Get wrappedParam =
              (Features_Schedules_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Schedules_Get.class);

          features_Schedules_GetResponse479 = skel.features_Schedules_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Schedules_GetResponse479,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Schedules_GetResponse"));
        } else if ("businesses_Statuses_Get".equals(methodName)) {

          Businesses_Statuses_GetResponse businesses_Statuses_GetResponse481 =
              null;
          Businesses_Statuses_Get wrappedParam =
              (Businesses_Statuses_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Businesses_Statuses_Get.class);

          businesses_Statuses_GetResponse481 = skel.businesses_Statuses_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  businesses_Statuses_GetResponse481,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Businesses_Statuses_GetResponse"));
        } else if ("resources_Types_Get".equals(methodName)) {

          Resources_Types_GetResponse resources_Types_GetResponse483 = null;
          Resources_Types_Get wrappedParam =
              (Resources_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Resources_Types_Get.class);

          resources_Types_GetResponse483 = skel.resources_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  resources_Types_GetResponse483,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Resources_Types_GetResponse"));
        } else if ("inspections_Classes_Get".equals(methodName)) {

          Inspections_Classes_GetResponse inspections_Classes_GetResponse485 =
              null;
          Inspections_Classes_Get wrappedParam =
              (Inspections_Classes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Inspections_Classes_Get.class);

          inspections_Classes_GetResponse485 = skel.inspections_Classes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  inspections_Classes_GetResponse485,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Inspections_Classes_GetResponse"));
        } else if ("licences_Classes_Get".equals(methodName)) {

          Licences_Classes_GetResponse licences_Classes_GetResponse487 = null;
          Licences_Classes_Get wrappedParam =
              (Licences_Classes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Licences_Classes_Get.class);

          licences_Classes_GetResponse487 = skel.licences_Classes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  licences_Classes_GetResponse487,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Licences_Classes_GetResponse"));
        } else if ("serviceRequest_Appointment_Reservation_Create".equals(methodName)) {

          ServiceRequest_Appointment_Reservation_CreateResponse
              serviceRequest_Appointment_Reservation_CreateResponse489 = null;
          ServiceRequest_Appointment_Reservation_Create wrappedParam =
              (ServiceRequest_Appointment_Reservation_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Appointment_Reservation_Create.class);

          serviceRequest_Appointment_Reservation_CreateResponse489 =
              skel.serviceRequest_Appointment_Reservation_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_Appointment_Reservation_CreateResponse489,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/",
                      "ServiceRequest_Appointment_Reservation_CreateResponse"));
        } else if ("premises_Events_Types_Get".equals(methodName)) {

          Premises_Events_Types_GetResponse
              premises_Events_Types_GetResponse491 = null;
          Premises_Events_Types_Get wrappedParam =
              (Premises_Events_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Events_Types_Get.class);

          premises_Events_Types_GetResponse491 = skel.premises_Events_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Events_Types_GetResponse491,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Events_Types_GetResponse"));
        } else if ("premises_Events_Document_Create".equals(methodName)) {

          Premises_Events_Document_CreateResponse
              premises_Events_Document_CreateResponse493 = null;
          Premises_Events_Document_Create wrappedParam =
              (Premises_Events_Document_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Events_Document_Create.class);

          premises_Events_Document_CreateResponse493 =
              skel.premises_Events_Document_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Events_Document_CreateResponse493,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Events_Document_CreateResponse"));
        } else if ("businesses_Classes_Get".equals(methodName)) {

          Businesses_Classes_GetResponse businesses_Classes_GetResponse495 =
              null;
          Businesses_Classes_Get wrappedParam =
              (Businesses_Classes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Businesses_Classes_Get.class);

          businesses_Classes_GetResponse495 = skel.businesses_Classes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  businesses_Classes_GetResponse495,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Businesses_Classes_GetResponse"));
        } else if ("premises_Attribute_Update".equals(methodName)) {

          Premises_Attribute_UpdateResponse
              premises_Attribute_UpdateResponse497 = null;
          Premises_Attribute_Update wrappedParam =
              (Premises_Attribute_Update)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Attribute_Update.class);

          premises_Attribute_UpdateResponse497 = skel.premises_Attribute_Update(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Attribute_UpdateResponse497,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Attribute_UpdateResponse"));
        } else if ("vehicle_InspectionForms_Get".equals(methodName)) {

          Vehicle_InspectionForms_GetResponse
              vehicle_InspectionForms_GetResponse499 = null;
          Vehicle_InspectionForms_Get wrappedParam =
              (Vehicle_InspectionForms_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Vehicle_InspectionForms_Get.class);

          vehicle_InspectionForms_GetResponse499 = skel.vehicle_InspectionForms_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  vehicle_InspectionForms_GetResponse499,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Vehicle_InspectionForms_GetResponse"));
        } else if ("jobs_WorkScheduleDates_Get".equals(methodName)) {

          Jobs_WorkScheduleDates_GetResponse
              jobs_WorkScheduleDates_GetResponse501 = null;
          Jobs_WorkScheduleDates_Get wrappedParam =
              (Jobs_WorkScheduleDates_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Jobs_WorkScheduleDates_Get.class);

          jobs_WorkScheduleDates_GetResponse501 = skel.jobs_WorkScheduleDates_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  jobs_WorkScheduleDates_GetResponse501,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Jobs_WorkScheduleDates_GetResponse"));
        } else if ("vehicle_InspectionResults_Get".equals(methodName)) {

          Vehicle_InspectionResults_GetResponse
              vehicle_InspectionResults_GetResponse503 = null;
          Vehicle_InspectionResults_Get wrappedParam =
              (Vehicle_InspectionResults_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Vehicle_InspectionResults_Get.class);

          vehicle_InspectionResults_GetResponse503 =
              skel.vehicle_InspectionResults_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  vehicle_InspectionResults_GetResponse503,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Vehicle_InspectionResults_GetResponse"));
        } else if ("inspections_Types_Get".equals(methodName)) {

          Inspections_Types_GetResponse inspections_Types_GetResponse505 = null;
          Inspections_Types_Get wrappedParam =
              (Inspections_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Inspections_Types_Get.class);

          inspections_Types_GetResponse505 = skel.inspections_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  inspections_Types_GetResponse505,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Inspections_Types_GetResponse"));
        } else if ("workpacks_Notes_Get".equals(methodName)) {

          Workpacks_Notes_GetResponse workpacks_Notes_GetResponse507 = null;
          Workpacks_Notes_Get wrappedParam =
              (Workpacks_Notes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Workpacks_Notes_Get.class);

          workpacks_Notes_GetResponse507 = skel.workpacks_Notes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  workpacks_Notes_GetResponse507,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Workpacks_Notes_GetResponse"));
        } else if ("workpacks_Metrics_Get".equals(methodName)) {

          Workpacks_Metrics_GetResponse workpacks_Metrics_GetResponse509 = null;
          Workpacks_Metrics_Get wrappedParam =
              (Workpacks_Metrics_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Workpacks_Metrics_Get.class);

          workpacks_Metrics_GetResponse509 = skel.workpacks_Metrics_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  workpacks_Metrics_GetResponse509,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Workpacks_Metrics_GetResponse"));
        } else if ("serviceRequests_History_Get".equals(methodName)) {

          ServiceRequests_History_GetResponse
              serviceRequests_History_GetResponse511 = null;
          ServiceRequests_History_Get wrappedParam =
              (ServiceRequests_History_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_History_Get.class);

          serviceRequests_History_GetResponse511 = skel.serviceRequests_History_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_History_GetResponse511,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_History_GetResponse"));
        } else if ("serviceRequest_Note_Create".equals(methodName)) {

          ServiceRequest_Note_CreateResponse
              serviceRequest_Note_CreateResponse513 = null;
          ServiceRequest_Note_Create wrappedParam =
              (ServiceRequest_Note_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Note_Create.class);

          serviceRequest_Note_CreateResponse513 = skel.serviceRequest_Note_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_Note_CreateResponse513,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequest_Note_CreateResponse"));
        } else if ("business_Document_Create".equals(methodName)) {

          Business_Document_CreateResponse business_Document_CreateResponse515 =
              null;
          Business_Document_Create wrappedParam =
              (Business_Document_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Business_Document_Create.class);

          business_Document_CreateResponse515 = skel.business_Document_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  business_Document_CreateResponse515,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Business_Document_CreateResponse"));
        } else if ("jobs_Detail_Get".equals(methodName)) {

          Jobs_Detail_GetResponse jobs_Detail_GetResponse517 = null;
          Jobs_Detail_Get wrappedParam =
              (Jobs_Detail_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Jobs_Detail_Get.class);

          jobs_Detail_GetResponse517 = skel.jobs_Detail_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  jobs_Detail_GetResponse517,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Jobs_Detail_GetResponse"));
        } else if ("system_WasteTypes_Get".equals(methodName)) {

          System_WasteTypes_GetResponse system_WasteTypes_GetResponse519 = null;
          System_WasteTypes_Get wrappedParam =
              (System_WasteTypes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      System_WasteTypes_Get.class);

          system_WasteTypes_GetResponse519 = skel.system_WasteTypes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  system_WasteTypes_GetResponse519,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "System_WasteTypes_GetResponse"));
        } else if ("businesses_Get".equals(methodName)) {

          Businesses_GetResponse businesses_GetResponse521 = null;
          Businesses_Get wrappedParam =
              (Businesses_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Businesses_Get.class);

          businesses_GetResponse521 = skel.businesses_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  businesses_GetResponse521,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Businesses_GetResponse"));
        } else if ("premises_Attributes_Delete".equals(methodName)) {

          Premises_Attributes_DeleteResponse
              premises_Attributes_DeleteResponse523 = null;
          Premises_Attributes_Delete wrappedParam =
              (Premises_Attributes_Delete)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Attributes_Delete.class);

          premises_Attributes_DeleteResponse523 = skel.premises_Attributes_Delete(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Attributes_DeleteResponse523,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Attributes_DeleteResponse"));
        } else if ("features_Statuses_Get".equals(methodName)) {

          Features_Statuses_GetResponse features_Statuses_GetResponse525 = null;
          Features_Statuses_Get wrappedParam =
              (Features_Statuses_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Statuses_Get.class);

          features_Statuses_GetResponse525 = skel.features_Statuses_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Statuses_GetResponse525,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Statuses_GetResponse"));
        } else if ("premises_AttributeDefinitions_Get".equals(methodName)) {

          Premises_AttributeDefinitions_GetResponse
              premises_AttributeDefinitions_GetResponse527 = null;
          Premises_AttributeDefinitions_Get wrappedParam =
              (Premises_AttributeDefinitions_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_AttributeDefinitions_Get.class);

          premises_AttributeDefinitions_GetResponse527 =
              skel.premises_AttributeDefinitions_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_AttributeDefinitions_GetResponse527,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_AttributeDefinitions_GetResponse"));
        } else if ("features_Get".equals(methodName)) {

          Features_GetResponse features_GetResponse529 = null;
          Features_Get wrappedParam =
              (Features_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Get.class);

          features_GetResponse529 = skel.features_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_GetResponse529,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_GetResponse"));
        } else if ("inspections_Documents_Get".equals(methodName)) {

          Inspections_Documents_GetResponse
              inspections_Documents_GetResponse531 = null;
          Inspections_Documents_Get wrappedParam =
              (Inspections_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Inspections_Documents_Get.class);

          inspections_Documents_GetResponse531 = skel.inspections_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  inspections_Documents_GetResponse531,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Inspections_Documents_GetResponse"));
        } else if ("jobs_Documents_Get".equals(methodName)) {

          Jobs_Documents_GetResponse jobs_Documents_GetResponse533 = null;
          Jobs_Documents_Get wrappedParam =
              (Jobs_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Jobs_Documents_Get.class);

          jobs_Documents_GetResponse533 = skel.jobs_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  jobs_Documents_GetResponse533,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Jobs_Documents_GetResponse"));
        } else if ("premises_Document_Create".equals(methodName)) {

          Premises_Document_CreateResponse premises_Document_CreateResponse535 =
              null;
          Premises_Document_Create wrappedParam =
              (Premises_Document_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Document_Create.class);

          premises_Document_CreateResponse535 = skel.premises_Document_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Document_CreateResponse535,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Document_CreateResponse"));
        } else if ("sites_Documents_GetAll".equals(methodName)) {

          Sites_Documents_GetAllResponse sites_Documents_GetAllResponse537 =
              null;
          Sites_Documents_GetAll wrappedParam =
              (Sites_Documents_GetAll)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Sites_Documents_GetAll.class);

          sites_Documents_GetAllResponse537 = skel.sites_Documents_GetAll(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  sites_Documents_GetAllResponse537,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Sites_Documents_GetAllResponse"));
        } else if ("premises_Attributes_Get".equals(methodName)) {

          Premises_Attributes_GetResponse premises_Attributes_GetResponse539 =
              null;
          Premises_Attributes_Get wrappedParam =
              (Premises_Attributes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Attributes_Get.class);

          premises_Attributes_GetResponse539 = skel.premises_Attributes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Attributes_GetResponse539,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Attributes_GetResponse"));
        } else if ("features_Colours_Get".equals(methodName)) {

          Features_Colours_GetResponse features_Colours_GetResponse541 = null;
          Features_Colours_Get wrappedParam =
              (Features_Colours_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Features_Colours_Get.class);

          features_Colours_GetResponse541 = skel.features_Colours_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  features_Colours_GetResponse541,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Features_Colours_GetResponse"));
        } else if ("licences_Documents_Get".equals(methodName)) {

          Licences_Documents_GetResponse licences_Documents_GetResponse543 =
              null;
          Licences_Documents_Get wrappedParam =
              (Licences_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Licences_Documents_Get.class);

          licences_Documents_GetResponse543 = skel.licences_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  licences_Documents_GetResponse543,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Licences_Documents_GetResponse"));
        } else if ("businesses_Types_Get".equals(methodName)) {

          Businesses_Types_GetResponse businesses_Types_GetResponse545 = null;
          Businesses_Types_Get wrappedParam =
              (Businesses_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Businesses_Types_Get.class);

          businesses_Types_GetResponse545 = skel.businesses_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  businesses_Types_GetResponse545,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Businesses_Types_GetResponse"));
        } else if ("streets_Events_Types_Get".equals(methodName)) {

          Streets_Events_Types_GetResponse streets_Events_Types_GetResponse547 =
              null;
          Streets_Events_Types_Get wrappedParam =
              (Streets_Events_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Streets_Events_Types_Get.class);

          streets_Events_Types_GetResponse547 = skel.streets_Events_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  streets_Events_Types_GetResponse547,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Streets_Events_Types_GetResponse"));
        } else if ("serviceRequest_Appointment_Slot_Reserve".equals(methodName)) {

          ServiceRequest_Appointment_Slot_ReserveResponse
              serviceRequest_Appointment_Slot_ReserveResponse549 = null;
          ServiceRequest_Appointment_Slot_Reserve wrappedParam =
              (ServiceRequest_Appointment_Slot_Reserve)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Appointment_Slot_Reserve.class);

          serviceRequest_Appointment_Slot_ReserveResponse549 =
              skel.serviceRequest_Appointment_Slot_Reserve(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_Appointment_Slot_ReserveResponse549,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/",
                      "ServiceRequest_Appointment_Slot_ReserveResponse"));
        } else if ("licences_Statuses_Get".equals(methodName)) {

          Licences_Statuses_GetResponse licences_Statuses_GetResponse551 = null;
          Licences_Statuses_Get wrappedParam =
              (Licences_Statuses_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Licences_Statuses_Get.class);

          licences_Statuses_GetResponse551 = skel.licences_Statuses_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  licences_Statuses_GetResponse551,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Licences_Statuses_GetResponse"));
        } else if ("licences_Types_Get".equals(methodName)) {

          Licences_Types_GetResponse licences_Types_GetResponse553 = null;
          Licences_Types_Get wrappedParam =
              (Licences_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Licences_Types_Get.class);

          licences_Types_GetResponse553 = skel.licences_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  licences_Types_GetResponse553,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Licences_Types_GetResponse"));
        } else if ("sites_Get".equals(methodName)) {

          Sites_GetResponse sites_GetResponse555 = null;
          Sites_Get wrappedParam =
              (Sites_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Sites_Get.class);

          sites_GetResponse555 = skel.sites_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  sites_GetResponse555,
                  false,
                  new javax.xml.namespace.QName("http://bartec-systems.com/", "Sites_GetResponse"));
        } else if ("serviceRequests_Get".equals(methodName)) {

          ServiceRequests_GetResponse serviceRequests_GetResponse557 = null;
          ServiceRequests_Get wrappedParam =
              (ServiceRequests_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Get.class);

          serviceRequests_GetResponse557 = skel.serviceRequests_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_GetResponse557,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_GetResponse"));
        } else if ("premises_Attribute_Create".equals(methodName)) {

          Premises_Attribute_CreateResponse
              premises_Attribute_CreateResponse559 = null;
          Premises_Attribute_Create wrappedParam =
              (Premises_Attribute_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Attribute_Create.class);

          premises_Attribute_CreateResponse559 = skel.premises_Attribute_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Attribute_CreateResponse559,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Attribute_CreateResponse"));
        } else if ("inspections_Statuses_Get".equals(methodName)) {

          Inspections_Statuses_GetResponse inspections_Statuses_GetResponse561 =
              null;
          Inspections_Statuses_Get wrappedParam =
              (Inspections_Statuses_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Inspections_Statuses_Get.class);

          inspections_Statuses_GetResponse561 = skel.inspections_Statuses_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  inspections_Statuses_GetResponse561,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Inspections_Statuses_GetResponse"));
        } else if ("system_LandTypes_Get".equals(methodName)) {

          System_LandTypes_GetResponse system_LandTypes_GetResponse563 = null;
          System_LandTypes_Get wrappedParam =
              (System_LandTypes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      System_LandTypes_Get.class);

          system_LandTypes_GetResponse563 = skel.system_LandTypes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  system_LandTypes_GetResponse563,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "System_LandTypes_GetResponse"));
        } else if ("serviceRequest_Cancel".equals(methodName)) {

          ServiceRequest_CancelResponse serviceRequest_CancelResponse565 = null;
          ServiceRequest_Cancel wrappedParam =
              (ServiceRequest_Cancel)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Cancel.class);

          serviceRequest_CancelResponse565 = skel.serviceRequest_Cancel(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_CancelResponse565,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequest_CancelResponse"));
        } else if ("serviceRequests_Notes_Types_Get".equals(methodName)) {

          ServiceRequests_Notes_Types_GetResponse
              serviceRequests_Notes_Types_GetResponse567 = null;
          ServiceRequests_Notes_Types_Get wrappedParam =
              (ServiceRequests_Notes_Types_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Notes_Types_Get.class);

          serviceRequests_Notes_Types_GetResponse567 =
              skel.serviceRequests_Notes_Types_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Notes_Types_GetResponse567,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_Notes_Types_GetResponse"));
        } else if ("premises_Event_Create".equals(methodName)) {

          Premises_Event_CreateResponse premises_Event_CreateResponse569 = null;
          Premises_Event_Create wrappedParam =
              (Premises_Event_Create)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Premises_Event_Create.class);

          premises_Event_CreateResponse569 = skel.premises_Event_Create(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  premises_Event_CreateResponse569,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Premises_Event_CreateResponse"));
        } else if ("serviceRequests_Documents_Get".equals(methodName)) {

          ServiceRequests_Documents_GetResponse
              serviceRequests_Documents_GetResponse571 = null;
          ServiceRequests_Documents_Get wrappedParam =
              (ServiceRequests_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Documents_Get.class);

          serviceRequests_Documents_GetResponse571 =
              skel.serviceRequests_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Documents_GetResponse571,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_Documents_GetResponse"));
        } else if ("resources_Get".equals(methodName)) {

          Resources_GetResponse resources_GetResponse573 = null;
          Resources_Get wrappedParam =
              (Resources_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Resources_Get.class);

          resources_GetResponse573 = skel.resources_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  resources_GetResponse573,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Resources_GetResponse"));
        } else if ("serviceRequest_Appointment_Reservation_Cancel".equals(methodName)) {

          ServiceRequest_Appointment_Reservation_CancelResponse
              serviceRequest_Appointment_Reservation_CancelResponse575 = null;
          ServiceRequest_Appointment_Reservation_Cancel wrappedParam =
              (ServiceRequest_Appointment_Reservation_Cancel)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Appointment_Reservation_Cancel.class);

          serviceRequest_Appointment_Reservation_CancelResponse575 =
              skel.serviceRequest_Appointment_Reservation_Cancel(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_Appointment_Reservation_CancelResponse575,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/",
                      "ServiceRequest_Appointment_Reservation_CancelResponse"));
        } else if ("serviceRequests_Detail_Get".equals(methodName)) {

          ServiceRequests_Detail_GetResponse
              serviceRequests_Detail_GetResponse577 = null;
          ServiceRequests_Detail_Get wrappedParam =
              (ServiceRequests_Detail_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Detail_Get.class);

          serviceRequests_Detail_GetResponse577 = skel.serviceRequests_Detail_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Detail_GetResponse577,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_Detail_GetResponse"));
        } else if ("serviceRequests_Classes_Get".equals(methodName)) {

          ServiceRequests_Classes_GetResponse
              serviceRequests_Classes_GetResponse579 = null;
          ServiceRequests_Classes_Get wrappedParam =
              (ServiceRequests_Classes_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequests_Classes_Get.class);

          serviceRequests_Classes_GetResponse579 = skel.serviceRequests_Classes_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequests_Classes_GetResponse579,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequests_Classes_GetResponse"));
        } else if ("serviceRequest_Update".equals(methodName)) {

          ServiceRequest_UpdateResponse serviceRequest_UpdateResponse581 = null;
          ServiceRequest_Update wrappedParam =
              (ServiceRequest_Update)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Update.class);

          serviceRequest_UpdateResponse581 = skel.serviceRequest_Update(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_UpdateResponse581,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequest_UpdateResponse"));
        } else if ("streets_Events_Get".equals(methodName)) {

          Streets_Events_GetResponse streets_Events_GetResponse583 = null;
          Streets_Events_Get wrappedParam =
              (Streets_Events_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Streets_Events_Get.class);

          streets_Events_GetResponse583 = skel.streets_Events_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  streets_Events_GetResponse583,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Streets_Events_GetResponse"));
        } else if ("resources_Documents_Get".equals(methodName)) {

          Resources_Documents_GetResponse resources_Documents_GetResponse585 =
              null;
          Resources_Documents_Get wrappedParam =
              (Resources_Documents_Get)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      Resources_Documents_Get.class);

          resources_Documents_GetResponse585 = skel.resources_Documents_Get(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  resources_Documents_GetResponse585,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "Resources_Documents_GetResponse"));
        } else if ("serviceRequest_Status_Set".equals(methodName)) {

          ServiceRequest_Status_SetResponse
              serviceRequest_Status_SetResponse587 = null;
          ServiceRequest_Status_Set wrappedParam =
              (ServiceRequest_Status_Set)
                  fromOM(
                      msgContext.getEnvelope().getBody().getFirstElement(),
                      ServiceRequest_Status_Set.class);

          serviceRequest_Status_SetResponse587 = skel.serviceRequest_Status_Set(wrappedParam);

          envelope =
              toEnvelope(
                  getSOAPFactory(msgContext),
                  serviceRequest_Status_SetResponse587,
                  false,
                  new javax.xml.namespace.QName(
                      "http://bartec-systems.com/", "ServiceRequest_Status_SetResponse"));

        } else {
          throw new java.lang.RuntimeException("method not found");
        }

        newMsgContext.setEnvelope(envelope);
      }
    } catch (java.lang.Exception e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  //
  private org.apache.axiom.om.OMElement toOM(
      Businesses_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Classes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Classes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Classes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Classes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_FutureWorkpacks_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_FutureWorkpacks_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_FutureWorkpacks_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_FutureWorkpacks_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_SLAs_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_SLAs_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_SLAs_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_SLAs_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Vehicles_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Vehicles_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Vehicles_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Vehicles_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Attributes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Attributes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Attributes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Attributes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      System_ExtendedDataDefinitions_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          System_ExtendedDataDefinitions_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      System_ExtendedDataDefinitions_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          System_ExtendedDataDefinitions_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Conditions_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Conditions_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Conditions_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Conditions_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Crews_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Crews_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Crews_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Crews_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Document_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Document_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Document_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Document_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Statuses_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Statuses_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Statuses_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Statuses_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Workpack_Note_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Workpack_Note_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Workpack_Note_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Workpack_Note_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      WorkPacks_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          WorkPacks_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      WorkPacks_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          WorkPacks_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Site_Document_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Site_Document_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Site_Document_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Site_Document_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Notes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Notes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Notes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Notes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Cases_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Cases_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Cases_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Cases_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Notes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Notes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Notes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Notes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_SubStatuses_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_SubStatuses_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_SubStatuses_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_SubStatuses_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Detail_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Detail_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Detail_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Detail_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Updates_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Updates_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Updates_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Updates_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_Close param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_Close.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_CloseResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_CloseResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Categories_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Categories_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Categories_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Categories_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Detail_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Detail_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Detail_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Detail_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Manufacturers_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Manufacturers_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Manufacturers_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Manufacturers_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Classes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Classes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Classes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Classes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Sites_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Sites_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Sites_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Sites_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Appointment_Reservation_Extend param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Appointment_Reservation_Extend.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Appointment_Reservation_ExtendResponse param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Appointment_Reservation_ExtendResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Workpacks_Notes_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Workpacks_Notes_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Workpacks_Notes_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Workpacks_Notes_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_FeatureScheduleDates_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_FeatureScheduleDates_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_FeatureScheduleDates_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_FeatureScheduleDates_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Vehicle_Message_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Vehicle_Message_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Vehicle_Message_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Vehicle_Message_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      WorkGroups_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          WorkGroups_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      WorkGroups_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          WorkGroups_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Appointments_Availability_Get param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Appointments_Availability_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Appointments_Availability_GetResponse param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Appointments_Availability_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Schedules_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Schedules_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Schedules_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Schedules_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_Statuses_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Statuses_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_Statuses_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Statuses_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Resources_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Resources_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Resources_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Resources_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Classes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Classes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Classes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Classes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Classes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Classes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Classes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Classes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Appointment_Reservation_Create param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Appointment_Reservation_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Appointment_Reservation_CreateResponse param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Appointment_Reservation_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Document_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Document_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Events_Document_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Events_Document_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_Classes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Classes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_Classes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Classes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Attribute_Update param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Attribute_Update.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Attribute_UpdateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Attribute_UpdateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Vehicle_InspectionForms_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Vehicle_InspectionForms_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Vehicle_InspectionForms_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Vehicle_InspectionForms_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_WorkScheduleDates_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_WorkScheduleDates_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_WorkScheduleDates_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_WorkScheduleDates_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Vehicle_InspectionResults_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Vehicle_InspectionResults_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Vehicle_InspectionResults_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Vehicle_InspectionResults_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Workpacks_Notes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Workpacks_Notes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Workpacks_Notes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Workpacks_Notes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Workpacks_Metrics_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Workpacks_Metrics_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Workpacks_Metrics_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Workpacks_Metrics_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_History_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_History_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_History_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_History_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Note_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Note_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Note_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Note_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Business_Document_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Business_Document_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Business_Document_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Business_Document_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_Detail_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_Detail_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_Detail_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_Detail_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      System_WasteTypes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          System_WasteTypes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      System_WasteTypes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          System_WasteTypes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Attributes_Delete param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Attributes_Delete.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Attributes_DeleteResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Attributes_DeleteResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Statuses_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Statuses_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Statuses_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Statuses_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_AttributeDefinitions_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_AttributeDefinitions_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_AttributeDefinitions_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_AttributeDefinitions_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Jobs_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Jobs_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Document_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Document_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Document_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Document_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Sites_Documents_GetAll param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Sites_Documents_GetAll.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Sites_Documents_GetAllResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Sites_Documents_GetAllResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Attributes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Attributes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Attributes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Attributes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Colours_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Colours_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Features_Colours_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Features_Colours_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Businesses_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Businesses_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Events_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Events_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Events_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Events_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Appointment_Slot_Reserve param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Appointment_Slot_Reserve.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Appointment_Slot_ReserveResponse param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Appointment_Slot_ReserveResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Statuses_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Statuses_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Statuses_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Statuses_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Licences_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Licences_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Sites_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Sites_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Sites_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Sites_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Attribute_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Attribute_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Attribute_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Attribute_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Statuses_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Statuses_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Inspections_Statuses_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Inspections_Statuses_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      System_LandTypes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          System_LandTypes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      System_LandTypes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          System_LandTypes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Cancel param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Cancel.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_CancelResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_CancelResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Notes_Types_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Notes_Types_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Notes_Types_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Notes_Types_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Event_Create param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Event_Create.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Premises_Event_CreateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Premises_Event_CreateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Resources_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Resources_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Resources_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Resources_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Appointment_Reservation_Cancel param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Appointment_Reservation_Cancel.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Appointment_Reservation_CancelResponse param,
      boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Appointment_Reservation_CancelResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Detail_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Detail_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Detail_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Detail_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Classes_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Classes_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequests_Classes_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequests_Classes_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Update param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Update.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_UpdateResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_UpdateResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Events_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Events_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Streets_Events_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Streets_Events_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Resources_Documents_Get param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Resources_Documents_Get.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      Resources_Documents_GetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          Resources_Documents_GetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Status_Set param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Status_Set.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.om.OMElement toOM(
      ServiceRequest_Status_SetResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {

    try {
      return param.getOMElement(
          ServiceRequest_Status_SetResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Businesses_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Businesses_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Businesses_Documents_GetResponse wrapBusinesses_Documents_Get() {
    Businesses_Documents_GetResponse wrappedElement =
        new Businesses_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Classes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Features_Classes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Classes_GetResponse wrapFeatures_Classes_Get() {
    Features_Classes_GetResponse wrappedElement =
        new Features_Classes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Events_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Premises_Events_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Events_GetResponse wrapPremises_Events_Get() {
    Premises_Events_GetResponse wrappedElement =
        new Premises_Events_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_FutureWorkpacks_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_FutureWorkpacks_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_FutureWorkpacks_GetResponse
      wrapPremises_FutureWorkpacks_Get() {
    Premises_FutureWorkpacks_GetResponse wrappedElement =
        new Premises_FutureWorkpacks_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Premises_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_GetResponse wrapPremises_Get() {
    Premises_GetResponse wrappedElement =
        new Premises_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_SLAs_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_SLAs_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_SLAs_GetResponse wrapServiceRequests_SLAs_Get() {
    ServiceRequests_SLAs_GetResponse wrappedElement =
        new ServiceRequests_SLAs_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Vehicles_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Vehicles_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Vehicles_GetResponse wrapVehicles_Get() {
    Vehicles_GetResponse wrappedElement =
        new Vehicles_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Streets_Attributes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Streets_Attributes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Streets_Attributes_GetResponse wrapStreets_Attributes_Get() {
    Streets_Attributes_GetResponse wrappedElement =
        new Streets_Attributes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      System_ExtendedDataDefinitions_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  System_ExtendedDataDefinitions_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private System_ExtendedDataDefinitions_GetResponse
      wrapSystem_ExtendedDataDefinitions_Get() {
    System_ExtendedDataDefinitions_GetResponse wrappedElement =
        new System_ExtendedDataDefinitions_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Conditions_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Features_Conditions_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Conditions_GetResponse wrapFeatures_Conditions_Get() {
    Features_Conditions_GetResponse wrappedElement =
        new Features_Conditions_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_CreateResponse wrapServiceRequest_Create() {
    ServiceRequest_CreateResponse wrappedElement =
        new ServiceRequest_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Crews_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Crews_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Crews_GetResponse wrapCrews_Get() {
    Crews_GetResponse wrappedElement =
        new Crews_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_Document_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_Document_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_Document_CreateResponse
      wrapServiceRequest_Document_Create() {
    ServiceRequest_Document_CreateResponse wrappedElement =
        new ServiceRequest_Document_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Statuses_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Statuses_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Statuses_GetResponse
      wrapServiceRequests_Statuses_Get() {
    ServiceRequests_Statuses_GetResponse wrappedElement =
        new ServiceRequests_Statuses_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Workpack_Note_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Workpack_Note_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Workpack_Note_CreateResponse wrapWorkpack_Note_Create() {
    Workpack_Note_CreateResponse wrappedElement =
        new Workpack_Note_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Inspections_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Inspections_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Inspections_GetResponse wrapInspections_Get() {
    Inspections_GetResponse wrappedElement =
        new Inspections_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      WorkPacks_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(WorkPacks_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private WorkPacks_GetResponse wrapWorkPacks_Get() {
    WorkPacks_GetResponse wrappedElement =
        new WorkPacks_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Streets_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Streets_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Streets_GetResponse wrapStreets_Get() {
    Streets_GetResponse wrappedElement =
        new Streets_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Site_Document_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Site_Document_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Site_Document_CreateResponse wrapSite_Document_Create() {
    Site_Document_CreateResponse wrappedElement =
        new Site_Document_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Notes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Notes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Notes_GetResponse wrapServiceRequests_Notes_Get() {
    ServiceRequests_Notes_GetResponse wrappedElement =
        new ServiceRequests_Notes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Cases_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Cases_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Cases_Documents_GetResponse wrapCases_Documents_Get() {
    Cases_Documents_GetResponse wrappedElement =
        new Cases_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Notes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Features_Notes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Notes_GetResponse wrapFeatures_Notes_Get() {
    Features_Notes_GetResponse wrappedElement =
        new Features_Notes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Businesses_SubStatuses_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Businesses_SubStatuses_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Businesses_SubStatuses_GetResponse wrapBusinesses_SubStatuses_Get() {
    Businesses_SubStatuses_GetResponse wrappedElement =
        new Businesses_SubStatuses_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Documents_GetResponse wrapPremises_Documents_Get() {
    Premises_Documents_GetResponse wrappedElement =
        new Premises_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Streets_Detail_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Streets_Detail_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Streets_Detail_GetResponse wrapStreets_Detail_Get() {
    Streets_Detail_GetResponse wrappedElement =
        new Streets_Detail_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Updates_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Updates_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Updates_GetResponse wrapServiceRequests_Updates_Get() {
    ServiceRequests_Updates_GetResponse wrappedElement =
        new ServiceRequests_Updates_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Features_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Types_GetResponse wrapFeatures_Types_Get() {
    Features_Types_GetResponse wrappedElement =
        new Features_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Jobs_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Jobs_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Jobs_GetResponse wrapJobs_Get() {
    Jobs_GetResponse wrappedElement = new Jobs_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Jobs_CloseResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Jobs_CloseResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Jobs_CloseResponse wrapJobs_Close() {
    Jobs_CloseResponse wrappedElement =
        new Jobs_CloseResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Types_GetResponse wrapServiceRequests_Types_Get() {
    ServiceRequests_Types_GetResponse wrappedElement =
        new ServiceRequests_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Categories_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Features_Categories_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Categories_GetResponse wrapFeatures_Categories_Get() {
    Features_Categories_GetResponse wrappedElement =
        new Features_Categories_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Licences_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Licences_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Licences_GetResponse wrapLicences_Get() {
    Licences_GetResponse wrappedElement =
        new Licences_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Detail_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Premises_Detail_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Detail_GetResponse wrapPremises_Detail_Get() {
    Premises_Detail_GetResponse wrappedElement =
        new Premises_Detail_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Manufacturers_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Features_Manufacturers_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Manufacturers_GetResponse wrapFeatures_Manufacturers_Get() {
    Features_Manufacturers_GetResponse wrappedElement =
        new Features_Manufacturers_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Events_Classes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Events_Classes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Events_Classes_GetResponse wrapPremises_Events_Classes_Get() {
    Premises_Events_Classes_GetResponse wrappedElement =
        new Premises_Events_Classes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Sites_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Sites_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Sites_Documents_GetResponse wrapSites_Documents_Get() {
    Sites_Documents_GetResponse wrappedElement =
        new Sites_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_Appointment_Reservation_ExtendResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_Appointment_Reservation_ExtendResponse.MY_QNAME,
                  factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_Appointment_Reservation_ExtendResponse
      wrapServiceRequest_Appointment_Reservation_Extend() {
    ServiceRequest_Appointment_Reservation_ExtendResponse wrappedElement =
        new ServiceRequest_Appointment_Reservation_ExtendResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Events_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Events_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Events_Documents_GetResponse
      wrapPremises_Events_Documents_Get() {
    Premises_Events_Documents_GetResponse wrappedElement =
        new Premises_Events_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Workpacks_Notes_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Workpacks_Notes_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Workpacks_Notes_Types_GetResponse wrapWorkpacks_Notes_Types_Get() {
    Workpacks_Notes_Types_GetResponse wrappedElement =
        new Workpacks_Notes_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Jobs_FeatureScheduleDates_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Jobs_FeatureScheduleDates_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Jobs_FeatureScheduleDates_GetResponse
      wrapJobs_FeatureScheduleDates_Get() {
    Jobs_FeatureScheduleDates_GetResponse wrappedElement =
        new Jobs_FeatureScheduleDates_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Vehicle_Message_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Vehicle_Message_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Vehicle_Message_CreateResponse wrapVehicle_Message_Create() {
    Vehicle_Message_CreateResponse wrappedElement =
        new Vehicle_Message_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      WorkGroups_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(WorkGroups_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private WorkGroups_GetResponse wrapWorkGroups_Get() {
    WorkGroups_GetResponse wrappedElement =
        new WorkGroups_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Appointments_Availability_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Appointments_Availability_GetResponse.MY_QNAME,
                  factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Appointments_Availability_GetResponse
      wrapServiceRequests_Appointments_Availability_Get() {
    ServiceRequests_Appointments_Availability_GetResponse wrappedElement =
        new ServiceRequests_Appointments_Availability_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Schedules_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Features_Schedules_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Schedules_GetResponse wrapFeatures_Schedules_Get() {
    Features_Schedules_GetResponse wrappedElement =
        new Features_Schedules_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Businesses_Statuses_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Businesses_Statuses_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Businesses_Statuses_GetResponse wrapBusinesses_Statuses_Get() {
    Businesses_Statuses_GetResponse wrappedElement =
        new Businesses_Statuses_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Resources_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Resources_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Resources_Types_GetResponse wrapResources_Types_Get() {
    Resources_Types_GetResponse wrappedElement =
        new Resources_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Inspections_Classes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Inspections_Classes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Inspections_Classes_GetResponse wrapInspections_Classes_Get() {
    Inspections_Classes_GetResponse wrappedElement =
        new Inspections_Classes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Licences_Classes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Licences_Classes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Licences_Classes_GetResponse wrapLicences_Classes_Get() {
    Licences_Classes_GetResponse wrappedElement =
        new Licences_Classes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_Appointment_Reservation_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_Appointment_Reservation_CreateResponse.MY_QNAME,
                  factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_Appointment_Reservation_CreateResponse
      wrapServiceRequest_Appointment_Reservation_Create() {
    ServiceRequest_Appointment_Reservation_CreateResponse wrappedElement =
        new ServiceRequest_Appointment_Reservation_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Events_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Events_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Events_Types_GetResponse wrapPremises_Events_Types_Get() {
    Premises_Events_Types_GetResponse wrappedElement =
        new Premises_Events_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Events_Document_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Events_Document_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Events_Document_CreateResponse
      wrapPremises_Events_Document_Create() {
    Premises_Events_Document_CreateResponse wrappedElement =
        new Premises_Events_Document_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Businesses_Classes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Businesses_Classes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Businesses_Classes_GetResponse wrapBusinesses_Classes_Get() {
    Businesses_Classes_GetResponse wrappedElement =
        new Businesses_Classes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Attribute_UpdateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Attribute_UpdateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Attribute_UpdateResponse wrapPremises_Attribute_Update() {
    Premises_Attribute_UpdateResponse wrappedElement =
        new Premises_Attribute_UpdateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Vehicle_InspectionForms_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Vehicle_InspectionForms_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Vehicle_InspectionForms_GetResponse wrapVehicle_InspectionForms_Get() {
    Vehicle_InspectionForms_GetResponse wrappedElement =
        new Vehicle_InspectionForms_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Jobs_WorkScheduleDates_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Jobs_WorkScheduleDates_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Jobs_WorkScheduleDates_GetResponse wrapJobs_WorkScheduleDates_Get() {
    Jobs_WorkScheduleDates_GetResponse wrappedElement =
        new Jobs_WorkScheduleDates_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Vehicle_InspectionResults_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Vehicle_InspectionResults_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Vehicle_InspectionResults_GetResponse
      wrapVehicle_InspectionResults_Get() {
    Vehicle_InspectionResults_GetResponse wrappedElement =
        new Vehicle_InspectionResults_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Inspections_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Inspections_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Inspections_Types_GetResponse wrapInspections_Types_Get() {
    Inspections_Types_GetResponse wrappedElement =
        new Inspections_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Workpacks_Notes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Workpacks_Notes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Workpacks_Notes_GetResponse wrapWorkpacks_Notes_Get() {
    Workpacks_Notes_GetResponse wrappedElement =
        new Workpacks_Notes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Workpacks_Metrics_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Workpacks_Metrics_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Workpacks_Metrics_GetResponse wrapWorkpacks_Metrics_Get() {
    Workpacks_Metrics_GetResponse wrappedElement =
        new Workpacks_Metrics_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_History_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_History_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_History_GetResponse wrapServiceRequests_History_Get() {
    ServiceRequests_History_GetResponse wrappedElement =
        new ServiceRequests_History_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_Note_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_Note_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_Note_CreateResponse wrapServiceRequest_Note_Create() {
    ServiceRequest_Note_CreateResponse wrappedElement =
        new ServiceRequest_Note_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Business_Document_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Business_Document_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Business_Document_CreateResponse wrapBusiness_Document_Create() {
    Business_Document_CreateResponse wrappedElement =
        new Business_Document_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Jobs_Detail_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Jobs_Detail_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Jobs_Detail_GetResponse wrapJobs_Detail_Get() {
    Jobs_Detail_GetResponse wrappedElement =
        new Jobs_Detail_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      System_WasteTypes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  System_WasteTypes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private System_WasteTypes_GetResponse wrapSystem_WasteTypes_Get() {
    System_WasteTypes_GetResponse wrappedElement =
        new System_WasteTypes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Businesses_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Businesses_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Businesses_GetResponse wrapBusinesses_Get() {
    Businesses_GetResponse wrappedElement =
        new Businesses_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Attributes_DeleteResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Attributes_DeleteResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Attributes_DeleteResponse wrapPremises_Attributes_Delete() {
    Premises_Attributes_DeleteResponse wrappedElement =
        new Premises_Attributes_DeleteResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Statuses_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Features_Statuses_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Statuses_GetResponse wrapFeatures_Statuses_Get() {
    Features_Statuses_GetResponse wrappedElement =
        new Features_Statuses_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_AttributeDefinitions_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_AttributeDefinitions_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_AttributeDefinitions_GetResponse
      wrapPremises_AttributeDefinitions_Get() {
    Premises_AttributeDefinitions_GetResponse wrappedElement =
        new Premises_AttributeDefinitions_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Features_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_GetResponse wrapFeatures_Get() {
    Features_GetResponse wrappedElement =
        new Features_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Inspections_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Inspections_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Inspections_Documents_GetResponse wrapInspections_Documents_Get() {
    Inspections_Documents_GetResponse wrappedElement =
        new Inspections_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Jobs_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Jobs_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Jobs_Documents_GetResponse wrapJobs_Documents_Get() {
    Jobs_Documents_GetResponse wrappedElement =
        new Jobs_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Document_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Document_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Document_CreateResponse wrapPremises_Document_Create() {
    Premises_Document_CreateResponse wrappedElement =
        new Premises_Document_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Sites_Documents_GetAllResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Sites_Documents_GetAllResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Sites_Documents_GetAllResponse wrapSites_Documents_GetAll() {
    Sites_Documents_GetAllResponse wrappedElement =
        new Sites_Documents_GetAllResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Attributes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Attributes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Attributes_GetResponse wrapPremises_Attributes_Get() {
    Premises_Attributes_GetResponse wrappedElement =
        new Premises_Attributes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Features_Colours_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Features_Colours_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Features_Colours_GetResponse wrapFeatures_Colours_Get() {
    Features_Colours_GetResponse wrappedElement =
        new Features_Colours_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Licences_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Licences_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Licences_Documents_GetResponse wrapLicences_Documents_Get() {
    Licences_Documents_GetResponse wrappedElement =
        new Licences_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Businesses_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Businesses_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Businesses_Types_GetResponse wrapBusinesses_Types_Get() {
    Businesses_Types_GetResponse wrappedElement =
        new Businesses_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Streets_Events_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Streets_Events_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Streets_Events_Types_GetResponse wrapStreets_Events_Types_Get() {
    Streets_Events_Types_GetResponse wrappedElement =
        new Streets_Events_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_Appointment_Slot_ReserveResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_Appointment_Slot_ReserveResponse.MY_QNAME,
                  factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_Appointment_Slot_ReserveResponse
      wrapServiceRequest_Appointment_Slot_Reserve() {
    ServiceRequest_Appointment_Slot_ReserveResponse wrappedElement =
        new ServiceRequest_Appointment_Slot_ReserveResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Licences_Statuses_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Licences_Statuses_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Licences_Statuses_GetResponse wrapLicences_Statuses_Get() {
    Licences_Statuses_GetResponse wrappedElement =
        new Licences_Statuses_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Licences_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Licences_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Licences_Types_GetResponse wrapLicences_Types_Get() {
    Licences_Types_GetResponse wrappedElement =
        new Licences_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Sites_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Sites_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Sites_GetResponse wrapSites_Get() {
    Sites_GetResponse wrappedElement =
        new Sites_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(ServiceRequests_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_GetResponse wrapServiceRequests_Get() {
    ServiceRequests_GetResponse wrappedElement =
        new ServiceRequests_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Attribute_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Attribute_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Attribute_CreateResponse wrapPremises_Attribute_Create() {
    Premises_Attribute_CreateResponse wrappedElement =
        new Premises_Attribute_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Inspections_Statuses_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Inspections_Statuses_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Inspections_Statuses_GetResponse wrapInspections_Statuses_Get() {
    Inspections_Statuses_GetResponse wrappedElement =
        new Inspections_Statuses_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      System_LandTypes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  System_LandTypes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private System_LandTypes_GetResponse wrapSystem_LandTypes_Get() {
    System_LandTypes_GetResponse wrappedElement =
        new System_LandTypes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_CancelResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_CancelResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_CancelResponse wrapServiceRequest_Cancel() {
    ServiceRequest_CancelResponse wrappedElement =
        new ServiceRequest_CancelResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Notes_Types_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Notes_Types_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Notes_Types_GetResponse
      wrapServiceRequests_Notes_Types_Get() {
    ServiceRequests_Notes_Types_GetResponse wrappedElement =
        new ServiceRequests_Notes_Types_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Premises_Event_CreateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Premises_Event_CreateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Premises_Event_CreateResponse wrapPremises_Event_Create() {
    Premises_Event_CreateResponse wrappedElement =
        new Premises_Event_CreateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Documents_GetResponse
      wrapServiceRequests_Documents_Get() {
    ServiceRequests_Documents_GetResponse wrappedElement =
        new ServiceRequests_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Resources_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(param.getOMElement(Resources_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Resources_GetResponse wrapResources_Get() {
    Resources_GetResponse wrappedElement =
        new Resources_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_Appointment_Reservation_CancelResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_Appointment_Reservation_CancelResponse.MY_QNAME,
                  factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_Appointment_Reservation_CancelResponse
      wrapServiceRequest_Appointment_Reservation_Cancel() {
    ServiceRequest_Appointment_Reservation_CancelResponse wrappedElement =
        new ServiceRequest_Appointment_Reservation_CancelResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Detail_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Detail_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Detail_GetResponse wrapServiceRequests_Detail_Get() {
    ServiceRequests_Detail_GetResponse wrappedElement =
        new ServiceRequests_Detail_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequests_Classes_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequests_Classes_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequests_Classes_GetResponse wrapServiceRequests_Classes_Get() {
    ServiceRequests_Classes_GetResponse wrappedElement =
        new ServiceRequests_Classes_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_UpdateResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_UpdateResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_UpdateResponse wrapServiceRequest_Update() {
    ServiceRequest_UpdateResponse wrappedElement =
        new ServiceRequest_UpdateResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Streets_Events_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(Streets_Events_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Streets_Events_GetResponse wrapStreets_Events_Get() {
    Streets_Events_GetResponse wrappedElement =
        new Streets_Events_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      Resources_Documents_GetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  Resources_Documents_GetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private Resources_Documents_GetResponse wrapResources_Documents_Get() {
    Resources_Documents_GetResponse wrappedElement =
        new Resources_Documents_GetResponse();
    return wrappedElement;
  }

  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(
      org.apache.axiom.soap.SOAPFactory factory,
      ServiceRequest_Status_SetResponse param,
      boolean optimizeContent,
      javax.xml.namespace.QName elementQName)
      throws org.apache.axis2.AxisFault {
    try {
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();

      emptyEnvelope
          .getBody()
          .addChild(
              param.getOMElement(
                  ServiceRequest_Status_SetResponse.MY_QNAME, factory));

      return emptyEnvelope;
    } catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
  }

  private ServiceRequest_Status_SetResponse wrapServiceRequest_Status_Set() {
    ServiceRequest_Status_SetResponse wrappedElement =
        new ServiceRequest_Status_SetResponse();
    return wrappedElement;
  }

  /** get the default envelope */
  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
    return factory.getDefaultEnvelope();
  }

  private java.lang.Object fromOM(org.apache.axiom.om.OMElement param, java.lang.Class type)
      throws org.apache.axis2.AxisFault {

    try {

      if (Business_Document_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Business_Document_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Business_Document_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Business_Document_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Classes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Businesses_Classes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Classes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Businesses_Classes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Businesses_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Businesses_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Businesses_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Businesses_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Statuses_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Businesses_Statuses_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Statuses_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Businesses_Statuses_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_SubStatuses_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Businesses_SubStatuses_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_SubStatuses_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Businesses_SubStatuses_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Businesses_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Businesses_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Businesses_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Cases_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Cases_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Cases_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Cases_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Crews_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Crews_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Crews_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Crews_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Categories_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Categories_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Categories_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Categories_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Classes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Classes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Classes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Classes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Colours_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Colours_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Colours_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Colours_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Conditions_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Conditions_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Conditions_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Conditions_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Manufacturers_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Manufacturers_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Manufacturers_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Manufacturers_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Notes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Notes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Notes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Notes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Schedules_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Schedules_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Schedules_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Schedules_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Statuses_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Statuses_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Statuses_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Statuses_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Features_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Features_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Features_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Classes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Inspections_Classes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Classes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Inspections_Classes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Inspections_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Inspections_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Inspections_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Inspections_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Statuses_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Inspections_Statuses_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Statuses_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Inspections_Statuses_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Inspections_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Inspections_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Inspections_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_Close.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Jobs_Close.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_CloseResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Jobs_CloseResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_Detail_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Jobs_Detail_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_Detail_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Jobs_Detail_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Jobs_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Jobs_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_FeatureScheduleDates_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Jobs_FeatureScheduleDates_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_FeatureScheduleDates_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Jobs_FeatureScheduleDates_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Jobs_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Jobs_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_WorkScheduleDates_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Jobs_WorkScheduleDates_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Jobs_WorkScheduleDates_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Jobs_WorkScheduleDates_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Classes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Licences_Classes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Classes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Licences_Classes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Licences_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Licences_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Licences_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Licences_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Statuses_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Licences_Statuses_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Statuses_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Licences_Statuses_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Licences_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Licences_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Licences_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Attribute_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Attribute_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Attribute_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Attribute_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Attribute_Update.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Attribute_Update.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Attribute_UpdateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Attribute_UpdateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_AttributeDefinitions_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_AttributeDefinitions_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_AttributeDefinitions_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_AttributeDefinitions_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Attributes_Delete.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Attributes_Delete.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Attributes_DeleteResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Attributes_DeleteResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Attributes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Premises_Attributes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Attributes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Attributes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Detail_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Premises_Detail_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Detail_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Detail_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Document_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Premises_Document_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Document_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Document_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Premises_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Event_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Premises_Event_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Event_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Event_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Classes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_Classes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Classes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_Classes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Document_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_Document_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Document_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_Document_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Premises_Events_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Events_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_Events_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_FutureWorkpacks_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_FutureWorkpacks_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_FutureWorkpacks_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Premises_FutureWorkpacks_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Premises_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Premises_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Premises_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Resources_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Resources_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Resources_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Resources_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Resources_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Resources_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Resources_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Resources_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Resources_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Resources_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Resources_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Resources_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Appointment_Reservation_Cancel.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Appointment_Reservation_Cancel.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Appointment_Reservation_CancelResponse.class.equals(
          type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Appointment_Reservation_CancelResponse.Factory.parse(
                reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Appointment_Reservation_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Appointment_Reservation_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Appointment_Reservation_CreateResponse.class.equals(
          type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Appointment_Reservation_CreateResponse.Factory.parse(
                reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Appointment_Reservation_Extend.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Appointment_Reservation_Extend.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Appointment_Reservation_ExtendResponse.class.equals(
          type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Appointment_Reservation_ExtendResponse.Factory.parse(
                reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Appointment_Slot_Reserve.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Appointment_Slot_Reserve.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Appointment_Slot_ReserveResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Appointment_Slot_ReserveResponse.Factory.parse(
                reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Cancel.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = ServiceRequest_Cancel.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_CancelResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_CancelResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = ServiceRequest_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Document_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Document_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Document_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Document_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Note_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Note_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Note_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Note_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Status_Set.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Status_Set.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Status_SetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_Status_SetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_Update.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = ServiceRequest_Update.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequest_UpdateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequest_UpdateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Appointments_Availability_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Appointments_Availability_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Appointments_Availability_GetResponse.class.equals(
          type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Appointments_Availability_GetResponse.Factory.parse(
                reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Classes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Classes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Classes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Classes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Detail_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Detail_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Detail_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Detail_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = ServiceRequests_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_History_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_History_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_History_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_History_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Notes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Notes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Notes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Notes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Notes_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Notes_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Notes_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Notes_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_SLAs_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = ServiceRequests_SLAs_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_SLAs_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_SLAs_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Statuses_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Statuses_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Statuses_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Statuses_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Updates_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Updates_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (ServiceRequests_Updates_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            ServiceRequests_Updates_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Site_Document_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Site_Document_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Site_Document_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Site_Document_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Sites_Documents_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Sites_Documents_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Sites_Documents_GetAll.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Sites_Documents_GetAll.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Sites_Documents_GetAllResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Sites_Documents_GetAllResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Sites_Documents_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Sites_Documents_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Sites_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Sites_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Sites_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Sites_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Attributes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Streets_Attributes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Attributes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Streets_Attributes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Detail_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Streets_Detail_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Detail_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Streets_Detail_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Events_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Streets_Events_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Events_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Streets_Events_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Events_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Streets_Events_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Events_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Streets_Events_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Streets_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Streets_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Streets_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (System_ExtendedDataDefinitions_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            System_ExtendedDataDefinitions_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (System_ExtendedDataDefinitions_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            System_ExtendedDataDefinitions_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (System_LandTypes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = System_LandTypes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (System_LandTypes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            System_LandTypes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (System_WasteTypes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = System_WasteTypes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (System_WasteTypes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            System_WasteTypes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Vehicle_InspectionForms_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Vehicle_InspectionForms_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Vehicle_InspectionForms_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Vehicle_InspectionForms_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Vehicle_InspectionResults_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Vehicle_InspectionResults_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Vehicle_InspectionResults_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Vehicle_InspectionResults_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Vehicle_Message_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Vehicle_Message_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Vehicle_Message_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Vehicle_Message_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Vehicles_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Vehicles_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Vehicles_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Vehicles_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (WorkGroups_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = WorkGroups_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (WorkGroups_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = WorkGroups_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Workpack_Note_Create.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Workpack_Note_Create.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Workpack_Note_CreateResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Workpack_Note_CreateResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (WorkPacks_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = WorkPacks_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (WorkPacks_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = WorkPacks_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Workpacks_Metrics_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Workpacks_Metrics_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Workpacks_Metrics_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Workpacks_Metrics_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Workpacks_Notes_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result = Workpacks_Notes_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Workpacks_Notes_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Workpacks_Notes_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Workpacks_Notes_Types_Get.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Workpacks_Notes_Types_Get.Factory.parse(reader);
        reader.close();
        return result;
      }

      if (Workpacks_Notes_Types_GetResponse.class.equals(type)) {

        javax.xml.stream.XMLStreamReader reader = param.getXMLStreamReaderWithoutCaching();
        java.lang.Object result =
            Workpacks_Notes_Types_GetResponse.Factory.parse(reader);
        reader.close();
        return result;
      }

    } catch (java.lang.Exception e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    return null;
  }

  private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
    org.apache.axis2.AxisFault f;
    Throwable cause = e.getCause();
    if (cause != null) {
      f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
    } else {
      f = new org.apache.axis2.AxisFault(e.getMessage());
    }

    return f;
  }
} // end of class
