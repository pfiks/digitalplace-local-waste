/**
 * AuthenticationAPIWebService.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.0 Built on : Aug 01,
 * 2021 (07:27:19 HST)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/*
 *  AuthenticationAPIWebService java interface
 */

public interface AuthenticationAPIWebService {

  /**
   * Auto generated method signature
   *
   * @param authenticateIFrame
   */
  public AuthenticateIFrameResponse authenticateIFrame(
      AuthenticateIFrame authenticateIFrame) throws java.rmi.RemoteException;

  /**
   * Auto generated method signature
   *
   * @param authenticate
   */
  public AuthenticateResponse authenticate(
      Authenticate authenticate) throws java.rmi.RemoteException;

  //
}
