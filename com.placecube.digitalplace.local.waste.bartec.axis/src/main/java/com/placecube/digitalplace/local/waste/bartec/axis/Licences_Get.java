/**
 * Licences_Get.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/** Licences_Get bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Licences_Get implements org.apache.axis2.databinding.ADBBean {

  public static final javax.xml.namespace.QName MY_QNAME =
      new javax.xml.namespace.QName("http://bartec-systems.com/", "Licences_Get", "ns99");

  /** field for Token */
  protected java.lang.String localToken;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTokenTracker = false;

  public boolean isTokenSpecified() {
    return localTokenTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getToken() {
    return localToken;
  }

  /**
   * Auto generated setter method
   *
   * @param param Token
   */
  public void setToken(java.lang.String param) {
    localTokenTracker = param != null;

    this.localToken = param;
  }

  /** field for SiteID */
  protected int localSiteID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSiteID() {
    return localSiteID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteID
   */
  public void setSiteID(int param) {

    this.localSiteID = param;
  }

  /** field for SiteName */
  protected java.lang.String localSiteName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSiteNameTracker = false;

  public boolean isSiteNameSpecified() {
    return localSiteNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSiteName() {
    return localSiteName;
  }

  /**
   * Auto generated setter method
   *
   * @param param SiteName
   */
  public void setSiteName(java.lang.String param) {
    localSiteNameTracker = param != null;

    this.localSiteName = param;
  }

  /** field for AccountNumber */
  protected java.lang.String localAccountNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAccountNumberTracker = false;

  public boolean isAccountNumberSpecified() {
    return localAccountNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAccountNumber() {
    return localAccountNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param AccountNumber
   */
  public void setAccountNumber(java.lang.String param) {
    localAccountNumberTracker = param != null;

    this.localAccountNumber = param;
  }

  /** field for UPRNs */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal localUPRNs;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUPRNsTracker = false;

  public boolean isUPRNsSpecified() {
    return localUPRNsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal getUPRNs() {
    return localUPRNs;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRNs
   */
  public void setUPRNs(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal param) {
    localUPRNsTracker = param != null;

    this.localUPRNs = param;
  }

  /** field for LicenceTypes */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt localLicenceTypes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLicenceTypesTracker = false;

  public boolean isLicenceTypesSpecified() {
    return localLicenceTypesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt getLicenceTypes() {
    return localLicenceTypes;
  }

  /**
   * Auto generated setter method
   *
   * @param param LicenceTypes
   */
  public void setLicenceTypes(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt param) {
    localLicenceTypesTracker = param != null;

    this.localLicenceTypes = param;
  }

  /** field for LicenceStatuses */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt localLicenceStatuses;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLicenceStatusesTracker = false;

  public boolean isLicenceStatusesSpecified() {
    return localLicenceStatusesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt getLicenceStatuses() {
    return localLicenceStatuses;
  }

  /**
   * Auto generated setter method
   *
   * @param param LicenceStatuses
   */
  public void setLicenceStatuses(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt param) {
    localLicenceStatusesTracker = param != null;

    this.localLicenceStatuses = param;
  }

  /** field for ServiceRequestID */
  protected int localServiceRequestID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceRequestID() {
    return localServiceRequestID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceRequestID
   */
  public void setServiceRequestID(int param) {

    this.localServiceRequestID = param;
  }

  /** field for StartDate */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery localStartDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localStartDateTracker = false;

  public boolean isStartDateSpecified() {
    return localStartDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery getStartDate() {
    return localStartDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param StartDate
   */
  public void setStartDate(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery param) {
    localStartDateTracker = param != null;

    this.localStartDate = param;
  }

  /** field for IssueDate */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery localIssueDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localIssueDateTracker = false;

  public boolean isIssueDateSpecified() {
    return localIssueDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery getIssueDate() {
    return localIssueDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param IssueDate
   */
  public void setIssueDate(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery param) {
    localIssueDateTracker = param != null;

    this.localIssueDate = param;
  }

  /** field for DateCreated */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery localDateCreated;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDateCreatedTracker = false;

  public boolean isDateCreatedSpecified() {
    return localDateCreatedTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery getDateCreated() {
    return localDateCreated;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateCreated
   */
  public void setDateCreated(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery param) {
    localDateCreatedTracker = param != null;

    this.localDateCreated = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://bartec-systems.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Licences_Get",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Licences_Get", xmlWriter);
      }
    }
    if (localTokenTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "token", xmlWriter);

      if (localToken == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("token cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localToken);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "SiteID", xmlWriter);

    if (localSiteID == java.lang.Integer.MIN_VALUE) {

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSiteID));
    }

    xmlWriter.writeEndElement();
    if (localSiteNameTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "SiteName", xmlWriter);

      if (localSiteName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("SiteName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSiteName);
      }

      xmlWriter.writeEndElement();
    }
    if (localAccountNumberTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "AccountNumber", xmlWriter);

      if (localAccountNumber == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("AccountNumber cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAccountNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localUPRNsTracker) {
      if (localUPRNs == null) {
        throw new org.apache.axis2.databinding.ADBException("UPRNs cannot be null!!");
      }
      localUPRNs.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "UPRNs"), xmlWriter);
    }
    if (localLicenceTypesTracker) {
      if (localLicenceTypes == null) {
        throw new org.apache.axis2.databinding.ADBException("LicenceTypes cannot be null!!");
      }
      localLicenceTypes.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "LicenceTypes"), xmlWriter);
    }
    if (localLicenceStatusesTracker) {
      if (localLicenceStatuses == null) {
        throw new org.apache.axis2.databinding.ADBException("LicenceStatuses cannot be null!!");
      }
      localLicenceStatuses.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "LicenceStatuses"),
          xmlWriter);
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "ServiceRequestID", xmlWriter);

    if (localServiceRequestID == java.lang.Integer.MIN_VALUE) {

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceRequestID));
    }

    xmlWriter.writeEndElement();
    if (localStartDateTracker) {
      if (localStartDate == null) {
        throw new org.apache.axis2.databinding.ADBException("StartDate cannot be null!!");
      }
      localStartDate.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "StartDate"), xmlWriter);
    }
    if (localIssueDateTracker) {
      if (localIssueDate == null) {
        throw new org.apache.axis2.databinding.ADBException("IssueDate cannot be null!!");
      }
      localIssueDate.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "IssueDate"), xmlWriter);
    }
    if (localDateCreatedTracker) {
      if (localDateCreated == null) {
        throw new org.apache.axis2.databinding.ADBException("DateCreated cannot be null!!");
      }
      localDateCreated.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "DateCreated"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://bartec-systems.com/")) {
      return "ns99";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Licences_Get parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Licences_Get object = new Licences_Get();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Licences_Get".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Licences_Get)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "token")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "token" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setToken(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "SiteID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setSiteID(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          } else {

            object.setSiteID(java.lang.Integer.MIN_VALUE);

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "SiteName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SiteName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSiteName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "AccountNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AccountNumber" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAccountNumber(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "UPRNs")
                .equals(reader.getName())) {

          object.setUPRNs(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "LicenceTypes")
                .equals(reader.getName())) {

          object.setLicenceTypes(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "LicenceStatuses")
                .equals(reader.getName())) {

          object.setLicenceStatuses(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequestID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setServiceRequestID(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          } else {

            object.setServiceRequestID(java.lang.Integer.MIN_VALUE);

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "StartDate")
                .equals(reader.getName())) {

          object.setStartDate(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "IssueDate")
                .equals(reader.getName())) {

          object.setIssueDate(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "DateCreated")
                .equals(reader.getName())) {

          object.setDateCreated(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
