/**
 * Street_type1.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.0 Built on : Aug 01,
 * 2021 (07:27:43 HST)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.streets_detail_get_xsd;

import com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper;

/** Street_type1 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Street_type1 implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = Street_type1
  Namespace URI = http://www.bartec-systems.com/Streets_Detail_Get.xsd
  Namespace Prefix = ns72
  */

  /** field for USRN */
  protected java.math.BigDecimal localUSRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUSRN() {
    return localUSRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param USRN
   */
  public void setUSRN(java.math.BigDecimal param) {

    this.localUSRN = param;
  }

  /** field for Street */
  protected org.apache.axiom.om.OMElement localStreet;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localStreetTracker = false;

  public boolean isStreetSpecified() {
    return localStreetTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getStreet() {
    return localStreet;
  }

  /**
   * Auto generated setter method
   *
   * @param param Street
   */
  public void setStreet(org.apache.axiom.om.OMElement param) {
    localStreetTracker = param != null;

    this.localStreet = param;
  }

  /** field for Locality */
  protected org.apache.axiom.om.OMElement localLocality;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLocalityTracker = false;

  public boolean isLocalitySpecified() {
    return localLocalityTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getLocality() {
    return localLocality;
  }

  /**
   * Auto generated setter method
   *
   * @param param Locality
   */
  public void setLocality(org.apache.axiom.om.OMElement param) {
    localLocalityTracker = param != null;

    this.localLocality = param;
  }

  /** field for Town */
  protected org.apache.axiom.om.OMElement localTown;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTownTracker = false;

  public boolean isTownSpecified() {
    return localTownTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getTown() {
    return localTown;
  }

  /**
   * Auto generated setter method
   *
   * @param param Town
   */
  public void setTown(org.apache.axiom.om.OMElement param) {
    localTownTracker = param != null;

    this.localTown = param;
  }

  /** field for UserDefinedName */
  protected org.apache.axiom.om.OMElement localUserDefinedName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUserDefinedNameTracker = false;

  public boolean isUserDefinedNameSpecified() {
    return localUserDefinedNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getUserDefinedName() {
    return localUserDefinedName;
  }

  /**
   * Auto generated setter method
   *
   * @param param UserDefinedName
   */
  public void setUserDefinedName(org.apache.axiom.om.OMElement param) {
    localUserDefinedNameTracker = param != null;

    this.localUserDefinedName = param;
  }

  /** field for AttributeCount */
  protected int localAttributeCount;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAttributeCount() {
    return localAttributeCount;
  }

  /**
   * Auto generated setter method
   *
   * @param param AttributeCount
   */
  public void setAttributeCount(int param) {

    this.localAttributeCount = param;
  }

  /** field for DocumentCount */
  protected int localDocumentCount;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getDocumentCount() {
    return localDocumentCount;
  }

  /**
   * Auto generated setter method
   *
   * @param param DocumentCount
   */
  public void setDocumentCount(int param) {

    this.localDocumentCount = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://www.bartec-systems.com/Streets_Detail_Get.xsd");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Street_type1",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "Street_type1", xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com/Streets_Detail_Get.xsd";
    writeStartElement(null, namespace, "USRN", xmlWriter);

    if (localUSRN == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("USRN cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUSRN));
    }

    xmlWriter.writeEndElement();
    if (localStreetTracker) {
      namespace = "http://www.bartec-systems.com/Streets_Detail_Get.xsd";
      writeStartElement(null, namespace, "Street", xmlWriter);

      if (localStreet == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Street cannot be null!!");

      } else {

        localStreet.serialize(xmlWriter);
      }

      xmlWriter.writeEndElement();
    }
    if (localLocalityTracker) {
      namespace = "http://www.bartec-systems.com/Streets_Detail_Get.xsd";
      writeStartElement(null, namespace, "Locality", xmlWriter);

      if (localLocality == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Locality cannot be null!!");

      } else {

        localLocality.serialize(xmlWriter);
      }

      xmlWriter.writeEndElement();
    }
    if (localTownTracker) {
      namespace = "http://www.bartec-systems.com/Streets_Detail_Get.xsd";
      writeStartElement(null, namespace, "Town", xmlWriter);

      if (localTown == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Town cannot be null!!");

      } else {

        localTown.serialize(xmlWriter);
      }

      xmlWriter.writeEndElement();
    }
    if (localUserDefinedNameTracker) {
      namespace = "http://www.bartec-systems.com/Streets_Detail_Get.xsd";
      writeStartElement(null, namespace, "UserDefinedName", xmlWriter);

      if (localUserDefinedName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("UserDefinedName cannot be null!!");

      } else {

        localUserDefinedName.serialize(xmlWriter);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/Streets_Detail_Get.xsd";
    writeStartElement(null, namespace, "AttributeCount", xmlWriter);

    if (localAttributeCount == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("AttributeCount cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAttributeCount));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Streets_Detail_Get.xsd";
    writeStartElement(null, namespace, "DocumentCount", xmlWriter);

    if (localDocumentCount == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("DocumentCount cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDocumentCount));
    }

    xmlWriter.writeEndElement();
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/Streets_Detail_Get.xsd", "RecordStamp"),
          xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/Streets_Detail_Get.xsd")) {
      return "ns72";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Street_type1 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Street_type1 object = new Street_type1();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Street_type1".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Street_type1)
                  ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Streets_Detail_Get.xsd", "USRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "USRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUSRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Street" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          org.apache.axiom.om.OMFactory fac = org.apache.axiom.om.OMAbstractFactory.getOMFactory();
          org.apache.axiom.om.OMNamespace omNs =
              fac.createOMNamespace("http://www.bartec-systems.com/Streets_Detail_Get.xsd", "");
          org.apache.axiom.om.OMElement _valueStreet = fac.createOMElement("Street", omNs);
          _valueStreet.addChild(fac.createOMText(_valueStreet, content));
          object.setStreet(_valueStreet);

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Locality" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          org.apache.axiom.om.OMFactory fac = org.apache.axiom.om.OMAbstractFactory.getOMFactory();
          org.apache.axiom.om.OMNamespace omNs =
              fac.createOMNamespace("http://www.bartec-systems.com/Streets_Detail_Get.xsd", "");
          org.apache.axiom.om.OMElement _valueLocality = fac.createOMElement("Locality", omNs);
          _valueLocality.addChild(fac.createOMText(_valueLocality, content));
          object.setLocality(_valueLocality);

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Town" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          org.apache.axiom.om.OMFactory fac = org.apache.axiom.om.OMAbstractFactory.getOMFactory();
          org.apache.axiom.om.OMNamespace omNs =
              fac.createOMNamespace("http://www.bartec-systems.com/Streets_Detail_Get.xsd", "");
          org.apache.axiom.om.OMElement _valueTown = fac.createOMElement("Town", omNs);
          _valueTown.addChild(fac.createOMText(_valueTown, content));
          object.setTown(_valueTown);

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UserDefinedName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          org.apache.axiom.om.OMFactory fac = org.apache.axiom.om.OMAbstractFactory.getOMFactory();
          org.apache.axiom.om.OMNamespace omNs =
              fac.createOMNamespace("http://www.bartec-systems.com/Streets_Detail_Get.xsd", "");
          org.apache.axiom.om.OMElement _valueUserDefinedName =
              fac.createOMElement("UserDefinedName", omNs);
          _valueUserDefinedName.addChild(fac.createOMText(_valueUserDefinedName, content));
          object.setUserDefinedName(_valueUserDefinedName);

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Streets_Detail_Get.xsd", "AttributeCount")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AttributeCount" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAttributeCount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Streets_Detail_Get.xsd", "DocumentCount")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DocumentCount" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDocumentCount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Streets_Detail_Get.xsd", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
