/**
 * Premises_Events_Get.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/** Premises_Events_Get bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Premises_Events_Get implements org.apache.axis2.databinding.ADBBean {

  public static final javax.xml.namespace.QName MY_QNAME =
      new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Events_Get", "ns99");

  /** field for Token */
  protected java.lang.String localToken;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTokenTracker = false;

  public boolean isTokenSpecified() {
    return localTokenTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getToken() {
    return localToken;
  }

  /**
   * Auto generated setter method
   *
   * @param param Token
   */
  public void setToken(java.lang.String param) {
    localTokenTracker = param != null;

    this.localToken = param;
  }

  /** field for UPRN */
  protected java.math.BigDecimal localUPRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUPRN() {
    return localUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRN
   */
  public void setUPRN(java.math.BigDecimal param) {

    this.localUPRN = param;
  }

  /** field for USRN */
  protected java.math.BigDecimal localUSRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUSRN() {
    return localUSRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param USRN
   */
  public void setUSRN(java.math.BigDecimal param) {

    this.localUSRN = param;
  }

  /** field for DateRange */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery localDateRange;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDateRangeTracker = false;

  public boolean isDateRangeSpecified() {
    return localDateRangeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery getDateRange() {
    return localDateRange;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateRange
   */
  public void setDateRange(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery param) {
    localDateRangeTracker = param != null;

    this.localDateRange = param;
  }

  /** field for Postcode */
  protected java.lang.String localPostcode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPostcodeTracker = false;

  public boolean isPostcodeSpecified() {
    return localPostcodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPostcode() {
    return localPostcode;
  }

  /**
   * Auto generated setter method
   *
   * @param param Postcode
   */
  public void setPostcode(java.lang.String param) {
    localPostcodeTracker = param != null;

    this.localPostcode = param;
  }

  /** field for WardName */
  protected java.lang.String localWardName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWardNameTracker = false;

  public boolean isWardNameSpecified() {
    return localWardNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWardName() {
    return localWardName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WardName
   */
  public void setWardName(java.lang.String param) {
    localWardNameTracker = param != null;

    this.localWardName = param;
  }

  /** field for ParishName */
  protected java.lang.String localParishName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localParishNameTracker = false;

  public boolean isParishNameSpecified() {
    return localParishNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getParishName() {
    return localParishName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ParishName
   */
  public void setParishName(java.lang.String param) {
    localParishNameTracker = param != null;

    this.localParishName = param;
  }

  /** field for Premises_Bounds */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox localPremises_Bounds;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPremises_BoundsTracker = false;

  public boolean isPremises_BoundsSpecified() {
    return localPremises_BoundsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox getPremises_Bounds() {
    return localPremises_Bounds;
  }

  /**
   * Auto generated setter method
   *
   * @param param Premises_Bounds
   */
  public void setPremises_Bounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox param) {
    localPremises_BoundsTracker = param != null;

    this.localPremises_Bounds = param;
  }

  /** field for Events_Bounds */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox localEvents_Bounds;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEvents_BoundsTracker = false;

  public boolean isEvents_BoundsSpecified() {
    return localEvents_BoundsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox getEvents_Bounds() {
    return localEvents_Bounds;
  }

  /**
   * Auto generated setter method
   *
   * @param param Events_Bounds
   */
  public void setEvents_Bounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox param) {
    localEvents_BoundsTracker = param != null;

    this.localEvents_Bounds = param;
  }

  /** field for EventType */
  protected java.lang.String localEventType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventTypeTracker = false;

  public boolean isEventTypeSpecified() {
    return localEventTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getEventType() {
    return localEventType;
  }

  /**
   * Auto generated setter method
   *
   * @param param EventType
   */
  public void setEventType(java.lang.String param) {
    localEventTypeTracker = param != null;

    this.localEventType = param;
  }

  /** field for EventClass */
  protected java.lang.String localEventClass;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventClassTracker = false;

  public boolean isEventClassSpecified() {
    return localEventClassTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getEventClass() {
    return localEventClass;
  }

  /**
   * Auto generated setter method
   *
   * @param param EventClass
   */
  public void setEventClass(java.lang.String param) {
    localEventClassTracker = param != null;

    this.localEventClass = param;
  }

  /** field for EventSubType */
  protected java.lang.String localEventSubType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventSubTypeTracker = false;

  public boolean isEventSubTypeSpecified() {
    return localEventSubTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getEventSubType() {
    return localEventSubType;
  }

  /**
   * Auto generated setter method
   *
   * @param param EventSubType
   */
  public void setEventSubType(java.lang.String param) {
    localEventSubTypeTracker = param != null;

    this.localEventSubType = param;
  }

  /** field for MinDistance */
  protected java.math.BigDecimal localMinDistance;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getMinDistance() {
    return localMinDistance;
  }

  /**
   * Auto generated setter method
   *
   * @param param MinDistance
   */
  public void setMinDistance(java.math.BigDecimal param) {

    this.localMinDistance = param;
  }

  /** field for MaxDistance */
  protected java.math.BigDecimal localMaxDistance;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getMaxDistance() {
    return localMaxDistance;
  }

  /**
   * Auto generated setter method
   *
   * @param param MaxDistance
   */
  public void setMaxDistance(java.math.BigDecimal param) {

    this.localMaxDistance = param;
  }

  /** field for DeviceType */
  protected java.lang.String localDeviceType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDeviceTypeTracker = false;

  public boolean isDeviceTypeSpecified() {
    return localDeviceTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getDeviceType() {
    return localDeviceType;
  }

  /**
   * Auto generated setter method
   *
   * @param param DeviceType
   */
  public void setDeviceType(java.lang.String param) {
    localDeviceTypeTracker = param != null;

    this.localDeviceType = param;
  }

  /** field for DeviceName */
  protected java.lang.String localDeviceName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDeviceNameTracker = false;

  public boolean isDeviceNameSpecified() {
    return localDeviceNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getDeviceName() {
    return localDeviceName;
  }

  /**
   * Auto generated setter method
   *
   * @param param DeviceName
   */
  public void setDeviceName(java.lang.String param) {
    localDeviceNameTracker = param != null;

    this.localDeviceName = param;
  }

  /** field for WorkPack */
  protected java.lang.String localWorkPack;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkPackTracker = false;

  public boolean isWorkPackSpecified() {
    return localWorkPackTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkPack() {
    return localWorkPack;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkPack
   */
  public void setWorkPack(java.lang.String param) {
    localWorkPackTracker = param != null;

    this.localWorkPack = param;
  }

  /** field for IncludeRelated */
  protected boolean localIncludeRelated;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getIncludeRelated() {
    return localIncludeRelated;
  }

  /**
   * Auto generated setter method
   *
   * @param param IncludeRelated
   */
  public void setIncludeRelated(boolean param) {

    this.localIncludeRelated = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://bartec-systems.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Premises_Events_Get",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "Premises_Events_Get",
            xmlWriter);
      }
    }
    if (localTokenTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "token", xmlWriter);

      if (localToken == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("token cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localToken);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "UPRN", xmlWriter);

    if (localUPRN == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
    }

    xmlWriter.writeEndElement();

    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "USRN", xmlWriter);

    if (localUSRN == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUSRN));
    }

    xmlWriter.writeEndElement();
    if (localDateRangeTracker) {
      if (localDateRange == null) {
        throw new org.apache.axis2.databinding.ADBException("DateRange cannot be null!!");
      }
      localDateRange.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "DateRange"), xmlWriter);
    }
    if (localPostcodeTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "Postcode", xmlWriter);

      if (localPostcode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Postcode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localPostcode);
      }

      xmlWriter.writeEndElement();
    }
    if (localWardNameTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "WardName", xmlWriter);

      if (localWardName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WardName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localWardName);
      }

      xmlWriter.writeEndElement();
    }
    if (localParishNameTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "ParishName", xmlWriter);

      if (localParishName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ParishName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localParishName);
      }

      xmlWriter.writeEndElement();
    }
    if (localPremises_BoundsTracker) {
      if (localPremises_Bounds == null) {
        throw new org.apache.axis2.databinding.ADBException("Premises_Bounds cannot be null!!");
      }
      localPremises_Bounds.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Bounds"),
          xmlWriter);
    }
    if (localEvents_BoundsTracker) {
      if (localEvents_Bounds == null) {
        throw new org.apache.axis2.databinding.ADBException("Events_Bounds cannot be null!!");
      }
      localEvents_Bounds.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "Events_Bounds"), xmlWriter);
    }
    if (localEventTypeTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "EventType", xmlWriter);

      if (localEventType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("EventType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localEventType);
      }

      xmlWriter.writeEndElement();
    }
    if (localEventClassTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "EventClass", xmlWriter);

      if (localEventClass == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("EventClass cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localEventClass);
      }

      xmlWriter.writeEndElement();
    }
    if (localEventSubTypeTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "EventSubType", xmlWriter);

      if (localEventSubType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("EventSubType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localEventSubType);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "MinDistance", xmlWriter);

    if (localMinDistance == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinDistance));
    }

    xmlWriter.writeEndElement();

    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "MaxDistance", xmlWriter);

    if (localMaxDistance == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxDistance));
    }

    xmlWriter.writeEndElement();
    if (localDeviceTypeTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "DeviceType", xmlWriter);

      if (localDeviceType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("DeviceType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localDeviceType);
      }

      xmlWriter.writeEndElement();
    }
    if (localDeviceNameTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "DeviceName", xmlWriter);

      if (localDeviceName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("DeviceName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localDeviceName);
      }

      xmlWriter.writeEndElement();
    }
    if (localWorkPackTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "WorkPack", xmlWriter);

      if (localWorkPack == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WorkPack cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localWorkPack);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "IncludeRelated", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("IncludeRelated cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeRelated));
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://bartec-systems.com/")) {
      return "ns99";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Premises_Events_Get parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Premises_Events_Get object = new Premises_Events_Get();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Premises_Events_Get".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Premises_Events_Get)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "token")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "token" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setToken(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setUPRN(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "USRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setUSRN(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "DateRange")
                .equals(reader.getName())) {

          object.setDateRange(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Postcode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Postcode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPostcode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "WardName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WardName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWardName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ParishName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ParishName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setParishName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Premises_Bounds")
                .equals(reader.getName())) {

          object.setPremises_Bounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Events_Bounds")
                .equals(reader.getName())) {

          object.setEvents_Bounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "EventType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "EventType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setEventType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "EventClass")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "EventClass" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setEventClass(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "EventSubType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "EventSubType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setEventSubType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "MinDistance")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setMinDistance(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "MaxDistance")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setMaxDistance(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "DeviceType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DeviceType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDeviceType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "DeviceName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DeviceName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDeviceName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "WorkPack")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkPack" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkPack(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "IncludeRelated")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IncludeRelated" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIncludeRelated(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
