/**
 * CtCrew.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** CtCrew bean class */
@SuppressWarnings({"unchecked", "unused"})
public class CtCrew implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ctCrew
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for ID */
  protected int localID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getID() {
    return localID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ID
   */
  public void setID(int param) {

    this.localID = param;
  }

  /** field for CrewNumber */
  protected int localCrewNumber;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getCrewNumber() {
    return localCrewNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param CrewNumber
   */
  public void setCrewNumber(int param) {

    this.localCrewNumber = param;
  }

  /** field for WorkGroup */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkGroup localWorkGroup;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkGroupTracker = false;

  public boolean isWorkGroupSpecified() {
    return localWorkGroupTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkGroup
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkGroup getWorkGroup() {
    return localWorkGroup;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkGroup
   */
  public void setWorkGroup(com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkGroup param) {
    localWorkGroupTracker = param != null;

    this.localWorkGroup = param;
  }

  /** field for Description */
  protected java.lang.String localDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDescriptionTracker = false;

  public boolean isDescriptionSpecified() {
    return localDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getDescription() {
    return localDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param Description
   */
  public void setDescription(java.lang.String param) {
    localDescriptionTracker = param != null;

    this.localDescription = param;
  }

  /** field for DefaultSize */
  protected int localDefaultSize;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getDefaultSize() {
    return localDefaultSize;
  }

  /**
   * Auto generated setter method
   *
   * @param param DefaultSize
   */
  public void setDefaultSize(int param) {

    this.localDefaultSize = param;
  }

  /** field for StartTime */
  protected org.apache.axis2.databinding.types.Time localStartTime;

  /**
   * Auto generated getter method
   *
   * @return org.apache.axis2.databinding.types.Time
   */
  public org.apache.axis2.databinding.types.Time getStartTime() {
    return localStartTime;
  }

  /**
   * Auto generated setter method
   *
   * @param param StartTime
   */
  public void setStartTime(org.apache.axis2.databinding.types.Time param) {

    this.localStartTime = param;
  }

  /** field for EndTime */
  protected org.apache.axis2.databinding.types.Time localEndTime;

  /**
   * Auto generated getter method
   *
   * @return org.apache.axis2.databinding.types.Time
   */
  public org.apache.axis2.databinding.types.Time getEndTime() {
    return localEndTime;
  }

  /**
   * Auto generated setter method
   *
   * @param param EndTime
   */
  public void setEndTime(org.apache.axis2.databinding.types.Time param) {

    this.localEndTime = param;
  }

  /** field for WorkingMinutes */
  protected int localWorkingMinutes;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getWorkingMinutes() {
    return localWorkingMinutes;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkingMinutes
   */
  public void setWorkingMinutes(int param) {

    this.localWorkingMinutes = param;
  }

  /** field for NonworkingMinutes */
  protected int localNonworkingMinutes;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getNonworkingMinutes() {
    return localNonworkingMinutes;
  }

  /**
   * Auto generated setter method
   *
   * @param param NonworkingMinutes
   */
  public void setNonworkingMinutes(int param) {

    this.localNonworkingMinutes = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ctCrew",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "ctCrew", xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ID", xmlWriter);

    if (localID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "CrewNumber", xmlWriter);

    if (localCrewNumber == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("CrewNumber cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCrewNumber));
    }

    xmlWriter.writeEndElement();
    if (localWorkGroupTracker) {
      if (localWorkGroup == null) {
        throw new org.apache.axis2.databinding.ADBException("WorkGroup cannot be null!!");
      }
      localWorkGroup.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "WorkGroup"), xmlWriter);
    }
    if (localDescriptionTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Description", xmlWriter);

      if (localDescription == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Description cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localDescription);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "DefaultSize", xmlWriter);

    if (localDefaultSize == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("DefaultSize cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefaultSize));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "StartTime", xmlWriter);

    if (localStartTime == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("StartTime cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartTime));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "EndTime", xmlWriter);

    if (localEndTime == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("EndTime cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndTime));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "WorkingMinutes", xmlWriter);

    if (localWorkingMinutes == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("WorkingMinutes cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWorkingMinutes));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "NonworkingMinutes", xmlWriter);

    if (localNonworkingMinutes == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("NonworkingMinutes cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNonworkingMinutes));
    }

    xmlWriter.writeEndElement();
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static CtCrew parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
      CtCrew object = new CtCrew();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ctCrew".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (CtCrew) com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "CrewNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "CrewNumber" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCrewNumber(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "WorkGroup")
                .equals(reader.getName())) {

          object.setWorkGroup(com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkGroup.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Description")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Description" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDescription(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "DefaultSize")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DefaultSize" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDefaultSize(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "StartTime")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "StartTime" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setStartTime(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "EndTime")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "EndTime" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setEndTime(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "WorkingMinutes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkingMinutes" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkingMinutes(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "NonworkingMinutes")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "NonworkingMinutes" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setNonworkingMinutes(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
