/**
 * CtVehicleInspectionResult.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** CtVehicleInspectionResult bean class */
@SuppressWarnings({"unchecked", "unused"})
public class CtVehicleInspectionResult implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ctVehicleInspectionResult
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for ID */
  protected int localID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getID() {
    return localID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ID
   */
  public void setID(int param) {

    this.localID = param;
  }

  /** field for DeviceID */
  protected int localDeviceID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getDeviceID() {
    return localDeviceID;
  }

  /**
   * Auto generated setter method
   *
   * @param param DeviceID
   */
  public void setDeviceID(int param) {

    this.localDeviceID = param;
  }

  /** field for VehicleID */
  protected int localVehicleID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getVehicleID() {
    return localVehicleID;
  }

  /**
   * Auto generated setter method
   *
   * @param param VehicleID
   */
  public void setVehicleID(int param) {

    this.localVehicleID = param;
  }

  /** field for DriverID */
  protected int localDriverID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getDriverID() {
    return localDriverID;
  }

  /**
   * Auto generated setter method
   *
   * @param param DriverID
   */
  public void setDriverID(int param) {

    this.localDriverID = param;
  }

  /** field for Mileage */
  protected int localMileage;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getMileage() {
    return localMileage;
  }

  /**
   * Auto generated setter method
   *
   * @param param Mileage
   */
  public void setMileage(int param) {

    this.localMileage = param;
  }

  /** field for UserName */
  protected java.lang.String localUserName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUserNameTracker = false;

  public boolean isUserNameSpecified() {
    return localUserNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getUserName() {
    return localUserName;
  }

  /**
   * Auto generated setter method
   *
   * @param param UserName
   */
  public void setUserName(java.lang.String param) {
    localUserNameTracker = param != null;

    this.localUserName = param;
  }

  /** field for ForeName */
  protected java.lang.String localForeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localForeNameTracker = false;

  public boolean isForeNameSpecified() {
    return localForeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getForeName() {
    return localForeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ForeName
   */
  public void setForeName(java.lang.String param) {
    localForeNameTracker = param != null;

    this.localForeName = param;
  }

  /** field for Surname */
  protected java.lang.String localSurname;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSurnameTracker = false;

  public boolean isSurnameSpecified() {
    return localSurnameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSurname() {
    return localSurname;
  }

  /**
   * Auto generated setter method
   *
   * @param param Surname
   */
  public void setSurname(java.lang.String param) {
    localSurnameTracker = param != null;

    this.localSurname = param;
  }

  /** field for InspectionForm */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionForm localInspectionForm;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localInspectionFormTracker = false;

  public boolean isInspectionFormSpecified() {
    return localInspectionFormTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionForm
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionForm getInspectionForm() {
    return localInspectionForm;
  }

  /**
   * Auto generated setter method
   *
   * @param param InspectionForm
   */
  public void setInspectionForm(com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionForm param) {
    localInspectionFormTracker = param != null;

    this.localInspectionForm = param;
  }

  /** field for Device */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice localDevice;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDeviceTracker = false;

  public boolean isDeviceSpecified() {
    return localDeviceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice getDevice() {
    return localDevice;
  }

  /**
   * Auto generated setter method
   *
   * @param param Device
   */
  public void setDevice(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice param) {
    localDeviceTracker = param != null;

    this.localDevice = param;
  }

  /** field for VehicleInspectionResultItems */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtVehicleInspectionResultItem
      localVehicleInspectionResultItems;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localVehicleInspectionResultItemsTracker = false;

  public boolean isVehicleInspectionResultItemsSpecified() {
    return localVehicleInspectionResultItemsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtVehicleInspectionResultItem
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtVehicleInspectionResultItem
      getVehicleInspectionResultItems() {
    return localVehicleInspectionResultItems;
  }

  /**
   * Auto generated setter method
   *
   * @param param VehicleInspectionResultItems
   */
  public void setVehicleInspectionResultItems(
      com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtVehicleInspectionResultItem param) {
    localVehicleInspectionResultItemsTracker = param != null;

    this.localVehicleInspectionResultItems = param;
  }

  /** field for AutomaticDefectReport */
  protected boolean localAutomaticDefectReport;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getAutomaticDefectReport() {
    return localAutomaticDefectReport;
  }

  /**
   * Auto generated setter method
   *
   * @param param AutomaticDefectReport
   */
  public void setAutomaticDefectReport(boolean param) {

    this.localAutomaticDefectReport = param;
  }

  /** field for RequireMileageDeclaration */
  protected boolean localRequireMileageDeclaration;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getRequireMileageDeclaration() {
    return localRequireMileageDeclaration;
  }

  /**
   * Auto generated setter method
   *
   * @param param RequireMileageDeclaration
   */
  public void setRequireMileageDeclaration(boolean param) {

    this.localRequireMileageDeclaration = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ctVehicleInspectionResult",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ctVehicleInspectionResult",
            xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ID", xmlWriter);

    if (localID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "DeviceID", xmlWriter);

    if (localDeviceID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("DeviceID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeviceID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "VehicleID", xmlWriter);

    if (localVehicleID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("VehicleID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVehicleID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "DriverID", xmlWriter);

    if (localDriverID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("DriverID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDriverID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "Mileage", xmlWriter);

    if (localMileage == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("Mileage cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMileage));
    }

    xmlWriter.writeEndElement();
    if (localUserNameTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "UserName", xmlWriter);

      if (localUserName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("UserName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localUserName);
      }

      xmlWriter.writeEndElement();
    }
    if (localForeNameTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "ForeName", xmlWriter);

      if (localForeName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ForeName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localForeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localSurnameTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Surname", xmlWriter);

      if (localSurname == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Surname cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSurname);
      }

      xmlWriter.writeEndElement();
    }
    if (localInspectionFormTracker) {
      if (localInspectionForm == null) {
        throw new org.apache.axis2.databinding.ADBException("InspectionForm cannot be null!!");
      }
      localInspectionForm.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "InspectionForm"),
          xmlWriter);
    }
    if (localDeviceTracker) {
      if (localDevice == null) {
        throw new org.apache.axis2.databinding.ADBException("Device cannot be null!!");
      }
      localDevice.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Device"), xmlWriter);
    }
    if (localVehicleInspectionResultItemsTracker) {
      if (localVehicleInspectionResultItems == null) {
        throw new org.apache.axis2.databinding.ADBException(
            "VehicleInspectionResultItems cannot be null!!");
      }
      localVehicleInspectionResultItems.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com", "VehicleInspectionResultItems"),
          xmlWriter);
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "AutomaticDefectReport", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("AutomaticDefectReport cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localAutomaticDefectReport));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "RequireMileageDeclaration", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException(
          "RequireMileageDeclaration cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localRequireMileageDeclaration));
    }

    xmlWriter.writeEndElement();
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static CtVehicleInspectionResult parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      CtVehicleInspectionResult object = new CtVehicleInspectionResult();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ctVehicleInspectionResult".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (CtVehicleInspectionResult)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "DeviceID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DeviceID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDeviceID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "VehicleID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "VehicleID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setVehicleID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "DriverID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DriverID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDriverID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Mileage")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Mileage" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setMileage(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "UserName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UserName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUserName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ForeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ForeName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setForeName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Surname")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Surname" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSurname(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "InspectionForm")
                .equals(reader.getName())) {

          object.setInspectionForm(
              com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionForm.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Device")
                .equals(reader.getName())) {

          object.setDevice(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com", "VehicleInspectionResultItems")
                .equals(reader.getName())) {

          object.setVehicleInspectionResultItems(
              com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtVehicleInspectionResultItem.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com", "AutomaticDefectReport")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AutomaticDefectReport" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAutomaticDefectReport(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com", "RequireMileageDeclaration")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "RequireMileageDeclaration" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setRequireMileageDeclaration(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
