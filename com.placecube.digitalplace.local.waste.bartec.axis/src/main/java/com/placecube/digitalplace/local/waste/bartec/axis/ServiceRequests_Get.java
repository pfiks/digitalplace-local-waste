/**
 * ServiceRequests_Get.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/** ServiceRequests_Get bean class */
@SuppressWarnings({"unchecked", "unused"})
public class ServiceRequests_Get implements org.apache.axis2.databinding.ADBBean {

  public static final javax.xml.namespace.QName MY_QNAME =
      new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequests_Get", "ns99");

  /** field for Token */
  protected java.lang.String localToken;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTokenTracker = false;

  public boolean isTokenSpecified() {
    return localTokenTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getToken() {
    return localToken;
  }

  /**
   * Auto generated setter method
   *
   * @param param Token
   */
  public void setToken(java.lang.String param) {
    localTokenTracker = param != null;

    this.localToken = param;
  }

  /** field for ServiceCode */
  protected java.lang.String localServiceCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceCodeTracker = false;

  public boolean isServiceCodeSpecified() {
    return localServiceCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceCode() {
    return localServiceCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceCode
   */
  public void setServiceCode(java.lang.String param) {
    localServiceCodeTracker = param != null;

    this.localServiceCode = param;
  }

  /** field for ExternalRef */
  protected java.lang.String localExternalRef;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExternalRefTracker = false;

  public boolean isExternalRefSpecified() {
    return localExternalRefTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getExternalRef() {
    return localExternalRef;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExternalRef
   */
  public void setExternalRef(java.lang.String param) {
    localExternalRefTracker = param != null;

    this.localExternalRef = param;
  }

  /** field for UPRNs */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal localUPRNs;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localUPRNsTracker = false;

  public boolean isUPRNsSpecified() {
    return localUPRNsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal getUPRNs() {
    return localUPRNs;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRNs
   */
  public void setUPRNs(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal param) {
    localUPRNsTracker = param != null;

    this.localUPRNs = param;
  }

  /** field for ContactName */
  protected java.lang.String localContactName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactNameTracker = false;

  public boolean isContactNameSpecified() {
    return localContactNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactName() {
    return localContactName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactName
   */
  public void setContactName(java.lang.String param) {
    localContactNameTracker = param != null;

    this.localContactName = param;
  }

  /** field for ContactPhoneNo */
  protected java.lang.String localContactPhoneNo;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactPhoneNoTracker = false;

  public boolean isContactPhoneNoSpecified() {
    return localContactPhoneNoTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactPhoneNo() {
    return localContactPhoneNo;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactPhoneNo
   */
  public void setContactPhoneNo(java.lang.String param) {
    localContactPhoneNoTracker = param != null;

    this.localContactPhoneNo = param;
  }

  /** field for ContactEmail */
  protected java.lang.String localContactEmail;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactEmailTracker = false;

  public boolean isContactEmailSpecified() {
    return localContactEmailTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactEmail() {
    return localContactEmail;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactEmail
   */
  public void setContactEmail(java.lang.String param) {
    localContactEmailTracker = param != null;

    this.localContactEmail = param;
  }

  /** field for PremisesBounds */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox localPremisesBounds;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPremisesBoundsTracker = false;

  public boolean isPremisesBoundsSpecified() {
    return localPremisesBoundsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox getPremisesBounds() {
    return localPremisesBounds;
  }

  /**
   * Auto generated setter method
   *
   * @param param PremisesBounds
   */
  public void setPremisesBounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox param) {
    localPremisesBoundsTracker = param != null;

    this.localPremisesBounds = param;
  }

  /** field for RequestBounds */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox localRequestBounds;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRequestBoundsTracker = false;

  public boolean isRequestBoundsSpecified() {
    return localRequestBoundsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox getRequestBounds() {
    return localRequestBounds;
  }

  /**
   * Auto generated setter method
   *
   * @param param RequestBounds
   */
  public void setRequestBounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox param) {
    localRequestBoundsTracker = param != null;

    this.localRequestBounds = param;
  }

  /** field for RequestDate */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery localRequestDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRequestDateTracker = false;

  public boolean isRequestDateSpecified() {
    return localRequestDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery getRequestDate() {
    return localRequestDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param RequestDate
   */
  public void setRequestDate(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery param) {
    localRequestDateTracker = param != null;

    this.localRequestDate = param;
  }

  /** field for ServiceTypes */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt localServiceTypes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceTypesTracker = false;

  public boolean isServiceTypesSpecified() {
    return localServiceTypesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt getServiceTypes() {
    return localServiceTypes;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceTypes
   */
  public void setServiceTypes(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt param) {
    localServiceTypesTracker = param != null;

    this.localServiceTypes = param;
  }

  /** field for LandTypes */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt localLandTypes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLandTypesTracker = false;

  public boolean isLandTypesSpecified() {
    return localLandTypesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt getLandTypes() {
    return localLandTypes;
  }

  /**
   * Auto generated setter method
   *
   * @param param LandTypes
   */
  public void setLandTypes(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt param) {
    localLandTypesTracker = param != null;

    this.localLandTypes = param;
  }

  /** field for ServiceStatuses */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt localServiceStatuses;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceStatusesTracker = false;

  public boolean isServiceStatusesSpecified() {
    return localServiceStatusesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt getServiceStatuses() {
    return localServiceStatuses;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceStatuses
   */
  public void setServiceStatuses(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt param) {
    localServiceStatusesTracker = param != null;

    this.localServiceStatuses = param;
  }

  /** field for Crews */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt localCrews;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCrewsTracker = false;

  public boolean isCrewsSpecified() {
    return localCrewsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt getCrews() {
    return localCrews;
  }

  /**
   * Auto generated setter method
   *
   * @param param Crews
   */
  public void setCrews(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt param) {
    localCrewsTracker = param != null;

    this.localCrews = param;
  }

  /** field for SLAStatus */
  protected java.lang.String localSLAStatus;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSLAStatusTracker = false;

  public boolean isSLAStatusSpecified() {
    return localSLAStatusTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSLAStatus() {
    return localSLAStatus;
  }

  /**
   * Auto generated setter method
   *
   * @param param SLAStatus
   */
  public void setSLAStatus(java.lang.String param) {
    localSLAStatusTracker = param != null;

    this.localSLAStatus = param;
  }

  /** field for SLABreachTime */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery localSLABreachTime;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSLABreachTimeTracker = false;

  public boolean isSLABreachTimeSpecified() {
    return localSLABreachTimeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery getSLABreachTime() {
    return localSLABreachTime;
  }

  /**
   * Auto generated setter method
   *
   * @param param SLABreachTime
   */
  public void setSLABreachTime(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery param) {
    localSLABreachTimeTracker = param != null;

    this.localSLABreachTime = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://bartec-systems.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ServiceRequests_Get",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ServiceRequests_Get",
            xmlWriter);
      }
    }
    if (localTokenTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "token", xmlWriter);

      if (localToken == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("token cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localToken);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceCodeTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "ServiceCode", xmlWriter);

      if (localServiceCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localExternalRefTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "ExternalRef", xmlWriter);

      if (localExternalRef == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ExternalRef cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localExternalRef);
      }

      xmlWriter.writeEndElement();
    }
    if (localUPRNsTracker) {
      if (localUPRNs == null) {
        throw new org.apache.axis2.databinding.ADBException("UPRNs cannot be null!!");
      }
      localUPRNs.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "UPRNs"), xmlWriter);
    }
    if (localContactNameTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "ContactName", xmlWriter);

      if (localContactName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactPhoneNoTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "ContactPhoneNo", xmlWriter);

      if (localContactPhoneNo == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactPhoneNo cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactPhoneNo);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactEmailTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "ContactEmail", xmlWriter);

      if (localContactEmail == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactEmail cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactEmail);
      }

      xmlWriter.writeEndElement();
    }
    if (localPremisesBoundsTracker) {
      if (localPremisesBounds == null) {
        throw new org.apache.axis2.databinding.ADBException("PremisesBounds cannot be null!!");
      }
      localPremisesBounds.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "PremisesBounds"), xmlWriter);
    }
    if (localRequestBoundsTracker) {
      if (localRequestBounds == null) {
        throw new org.apache.axis2.databinding.ADBException("RequestBounds cannot be null!!");
      }
      localRequestBounds.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "RequestBounds"), xmlWriter);
    }
    if (localRequestDateTracker) {
      if (localRequestDate == null) {
        throw new org.apache.axis2.databinding.ADBException("RequestDate cannot be null!!");
      }
      localRequestDate.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "RequestDate"), xmlWriter);
    }
    if (localServiceTypesTracker) {
      if (localServiceTypes == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceTypes cannot be null!!");
      }
      localServiceTypes.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceTypes"), xmlWriter);
    }
    if (localLandTypesTracker) {
      if (localLandTypes == null) {
        throw new org.apache.axis2.databinding.ADBException("LandTypes cannot be null!!");
      }
      localLandTypes.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "LandTypes"), xmlWriter);
    }
    if (localServiceStatusesTracker) {
      if (localServiceStatuses == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceStatuses cannot be null!!");
      }
      localServiceStatuses.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceStatuses"),
          xmlWriter);
    }
    if (localCrewsTracker) {
      if (localCrews == null) {
        throw new org.apache.axis2.databinding.ADBException("Crews cannot be null!!");
      }
      localCrews.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "Crews"), xmlWriter);
    }
    if (localSLAStatusTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "SLAStatus", xmlWriter);

      if (localSLAStatus == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("SLAStatus cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSLAStatus);
      }

      xmlWriter.writeEndElement();
    }
    if (localSLABreachTimeTracker) {
      if (localSLABreachTime == null) {
        throw new org.apache.axis2.databinding.ADBException("SLABreachTime cannot be null!!");
      }
      localSLABreachTime.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "SLABreachTime"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://bartec-systems.com/")) {
      return "ns99";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static ServiceRequests_Get parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      ServiceRequests_Get object = new ServiceRequests_Get();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ServiceRequests_Get".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (ServiceRequests_Get)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "token")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "token" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setToken(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ExternalRef")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExternalRef" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExternalRef(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "UPRNs")
                .equals(reader.getName())) {

          object.setUPRNs(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ContactName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ContactPhoneNo")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactPhoneNo" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactPhoneNo(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ContactEmail")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactEmail" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactEmail(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "PremisesBounds")
                .equals(reader.getName())) {

          object.setPremisesBounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "RequestBounds")
                .equals(reader.getName())) {

          object.setRequestBounds(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "RequestDate")
                .equals(reader.getName())) {

          object.setRequestDate(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceTypes")
                .equals(reader.getName())) {

          object.setServiceTypes(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "LandTypes")
                .equals(reader.getName())) {

          object.setLandTypes(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceStatuses")
                .equals(reader.getName())) {

          object.setServiceStatuses(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "Crews")
                .equals(reader.getName())) {

          object.setCrews(com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "SLAStatus")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SLAStatus" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSLAStatus(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "SLABreachTime")
                .equals(reader.getName())) {

          object.setSLABreachTime(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
