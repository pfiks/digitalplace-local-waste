/**
 * CollectiveAPIWebServiceCallbackHandler.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.0 Built on : Aug 01,
 * 2021 (07:27:19 HST)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/**
 * CollectiveAPIWebServiceCallbackHandler Callback class, Users can extend this class and implement
 * their own receiveResult and receiveError methods.
 */
public abstract class CollectiveAPIWebServiceCallbackHandler {

  protected Object clientData;

  /**
   * User can pass in any object that needs to be accessed once the NonBlocking Web service call is
   * finished and appropriate method of this CallBack is called.
   *
   * @param clientData Object mechanism by which the user can pass in user data that will be
   *     avilable at the time this callback is called.
   */
  public CollectiveAPIWebServiceCallbackHandler(Object clientData) {
    this.clientData = clientData;
  }

  /** Please use this constructor if you don't want to set any clientData */
  public CollectiveAPIWebServiceCallbackHandler() {
    this.clientData = null;
  }

  /** Get the client data */
  public Object getClientData() {
    return clientData;
  }

  /**
   * auto generated Axis2 call back method for businesses_Documents_Get method override this method
   * for handling normal response from businesses_Documents_Get operation
   */
  public void receiveResultbusinesses_Documents_Get(
      Businesses_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * businesses_Documents_Get operation
   */
  public void receiveErrorbusinesses_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Classes_Get method override this method for
   * handling normal response from features_Classes_Get operation
   */
  public void receiveResultfeatures_Classes_Get(
      Features_Classes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Classes_Get operation
   */
  public void receiveErrorfeatures_Classes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Events_Get method override this method for
   * handling normal response from premises_Events_Get operation
   */
  public void receiveResultpremises_Events_Get(
      Premises_Events_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Events_Get operation
   */
  public void receiveErrorpremises_Events_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_FutureWorkpacks_Get method override this
   * method for handling normal response from premises_FutureWorkpacks_Get operation
   */
  public void receiveResultpremises_FutureWorkpacks_Get(
      Premises_FutureWorkpacks_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_FutureWorkpacks_Get operation
   */
  public void receiveErrorpremises_FutureWorkpacks_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Get method override this method for handling
   * normal response from premises_Get operation
   */
  public void receiveResultpremises_Get(Premises_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Get operation
   */
  public void receiveErrorpremises_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_SLAs_Get method override this method
   * for handling normal response from serviceRequests_SLAs_Get operation
   */
  public void receiveResultserviceRequests_SLAs_Get(
      ServiceRequests_SLAs_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_SLAs_Get operation
   */
  public void receiveErrorserviceRequests_SLAs_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for vehicles_Get method override this method for handling
   * normal response from vehicles_Get operation
   */
  public void receiveResultvehicles_Get(Vehicles_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * vehicles_Get operation
   */
  public void receiveErrorvehicles_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for streets_Attributes_Get method override this method
   * for handling normal response from streets_Attributes_Get operation
   */
  public void receiveResultstreets_Attributes_Get(
      Streets_Attributes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * streets_Attributes_Get operation
   */
  public void receiveErrorstreets_Attributes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for system_ExtendedDataDefinitions_Get method override
   * this method for handling normal response from system_ExtendedDataDefinitions_Get operation
   */
  public void receiveResultsystem_ExtendedDataDefinitions_Get(
      System_ExtendedDataDefinitions_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * system_ExtendedDataDefinitions_Get operation
   */
  public void receiveErrorsystem_ExtendedDataDefinitions_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Conditions_Get method override this method
   * for handling normal response from features_Conditions_Get operation
   */
  public void receiveResultfeatures_Conditions_Get(
      Features_Conditions_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Conditions_Get operation
   */
  public void receiveErrorfeatures_Conditions_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Create method override this method for
   * handling normal response from serviceRequest_Create operation
   */
  public void receiveResultserviceRequest_Create(
      ServiceRequest_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Create operation
   */
  public void receiveErrorserviceRequest_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for crews_Get method override this method for handling
   * normal response from crews_Get operation
   */
  public void receiveResultcrews_Get(Crews_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * crews_Get operation
   */
  public void receiveErrorcrews_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Document_Create method override this
   * method for handling normal response from serviceRequest_Document_Create operation
   */
  public void receiveResultserviceRequest_Document_Create(
      ServiceRequest_Document_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Document_Create operation
   */
  public void receiveErrorserviceRequest_Document_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Statuses_Get method override this
   * method for handling normal response from serviceRequests_Statuses_Get operation
   */
  public void receiveResultserviceRequests_Statuses_Get(
      ServiceRequests_Statuses_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Statuses_Get operation
   */
  public void receiveErrorserviceRequests_Statuses_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for workpack_Note_Create method override this method for
   * handling normal response from workpack_Note_Create operation
   */
  public void receiveResultworkpack_Note_Create(
      Workpack_Note_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * workpack_Note_Create operation
   */
  public void receiveErrorworkpack_Note_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for inspections_Get method override this method for
   * handling normal response from inspections_Get operation
   */
  public void receiveResultinspections_Get(Inspections_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * inspections_Get operation
   */
  public void receiveErrorinspections_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for workPacks_Get method override this method for
   * handling normal response from workPacks_Get operation
   */
  public void receiveResultworkPacks_Get(WorkPacks_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * workPacks_Get operation
   */
  public void receiveErrorworkPacks_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for streets_Get method override this method for handling
   * normal response from streets_Get operation
   */
  public void receiveResultstreets_Get(Streets_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * streets_Get operation
   */
  public void receiveErrorstreets_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for site_Document_Create method override this method for
   * handling normal response from site_Document_Create operation
   */
  public void receiveResultsite_Document_Create(
      Site_Document_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * site_Document_Create operation
   */
  public void receiveErrorsite_Document_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Notes_Get method override this method
   * for handling normal response from serviceRequests_Notes_Get operation
   */
  public void receiveResultserviceRequests_Notes_Get(
      ServiceRequests_Notes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Notes_Get operation
   */
  public void receiveErrorserviceRequests_Notes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for cases_Documents_Get method override this method for
   * handling normal response from cases_Documents_Get operation
   */
  public void receiveResultcases_Documents_Get(
      Cases_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * cases_Documents_Get operation
   */
  public void receiveErrorcases_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Notes_Get method override this method for
   * handling normal response from features_Notes_Get operation
   */
  public void receiveResultfeatures_Notes_Get(
      Features_Notes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Notes_Get operation
   */
  public void receiveErrorfeatures_Notes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for businesses_SubStatuses_Get method override this
   * method for handling normal response from businesses_SubStatuses_Get operation
   */
  public void receiveResultbusinesses_SubStatuses_Get(
      Businesses_SubStatuses_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * businesses_SubStatuses_Get operation
   */
  public void receiveErrorbusinesses_SubStatuses_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Documents_Get method override this method
   * for handling normal response from premises_Documents_Get operation
   */
  public void receiveResultpremises_Documents_Get(
      Premises_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Documents_Get operation
   */
  public void receiveErrorpremises_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for streets_Detail_Get method override this method for
   * handling normal response from streets_Detail_Get operation
   */
  public void receiveResultstreets_Detail_Get(
      Streets_Detail_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * streets_Detail_Get operation
   */
  public void receiveErrorstreets_Detail_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Updates_Get method override this
   * method for handling normal response from serviceRequests_Updates_Get operation
   */
  public void receiveResultserviceRequests_Updates_Get(
      ServiceRequests_Updates_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Updates_Get operation
   */
  public void receiveErrorserviceRequests_Updates_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Types_Get method override this method for
   * handling normal response from features_Types_Get operation
   */
  public void receiveResultfeatures_Types_Get(
      Features_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Types_Get operation
   */
  public void receiveErrorfeatures_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for jobs_Get method override this method for handling
   * normal response from jobs_Get operation
   */
  public void receiveResultjobs_Get(Jobs_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * jobs_Get operation
   */
  public void receiveErrorjobs_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for jobs_Close method override this method for handling
   * normal response from jobs_Close operation
   */
  public void receiveResultjobs_Close(Jobs_CloseResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * jobs_Close operation
   */
  public void receiveErrorjobs_Close(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Types_Get method override this method
   * for handling normal response from serviceRequests_Types_Get operation
   */
  public void receiveResultserviceRequests_Types_Get(
      ServiceRequests_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Types_Get operation
   */
  public void receiveErrorserviceRequests_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Categories_Get method override this method
   * for handling normal response from features_Categories_Get operation
   */
  public void receiveResultfeatures_Categories_Get(
      Features_Categories_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Categories_Get operation
   */
  public void receiveErrorfeatures_Categories_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for licences_Get method override this method for handling
   * normal response from licences_Get operation
   */
  public void receiveResultlicences_Get(Licences_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * licences_Get operation
   */
  public void receiveErrorlicences_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Detail_Get method override this method for
   * handling normal response from premises_Detail_Get operation
   */
  public void receiveResultpremises_Detail_Get(
      Premises_Detail_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Detail_Get operation
   */
  public void receiveErrorpremises_Detail_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Manufacturers_Get method override this
   * method for handling normal response from features_Manufacturers_Get operation
   */
  public void receiveResultfeatures_Manufacturers_Get(
      Features_Manufacturers_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Manufacturers_Get operation
   */
  public void receiveErrorfeatures_Manufacturers_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Events_Classes_Get method override this
   * method for handling normal response from premises_Events_Classes_Get operation
   */
  public void receiveResultpremises_Events_Classes_Get(
      Premises_Events_Classes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Events_Classes_Get operation
   */
  public void receiveErrorpremises_Events_Classes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for sites_Documents_Get method override this method for
   * handling normal response from sites_Documents_Get operation
   */
  public void receiveResultsites_Documents_Get(
      Sites_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * sites_Documents_Get operation
   */
  public void receiveErrorsites_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Appointment_Reservation_Extend method
   * override this method for handling normal response from
   * serviceRequest_Appointment_Reservation_Extend operation
   */
  public void receiveResultserviceRequest_Appointment_Reservation_Extend(
      ServiceRequest_Appointment_Reservation_ExtendResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Appointment_Reservation_Extend operation
   */
  public void receiveErrorserviceRequest_Appointment_Reservation_Extend(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Events_Documents_Get method override this
   * method for handling normal response from premises_Events_Documents_Get operation
   */
  public void receiveResultpremises_Events_Documents_Get(
      Premises_Events_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Events_Documents_Get operation
   */
  public void receiveErrorpremises_Events_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for workpacks_Notes_Types_Get method override this method
   * for handling normal response from workpacks_Notes_Types_Get operation
   */
  public void receiveResultworkpacks_Notes_Types_Get(
      Workpacks_Notes_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * workpacks_Notes_Types_Get operation
   */
  public void receiveErrorworkpacks_Notes_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for jobs_FeatureScheduleDates_Get method override this
   * method for handling normal response from jobs_FeatureScheduleDates_Get operation
   */
  public void receiveResultjobs_FeatureScheduleDates_Get(
      Jobs_FeatureScheduleDates_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * jobs_FeatureScheduleDates_Get operation
   */
  public void receiveErrorjobs_FeatureScheduleDates_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for vehicle_Message_Create method override this method
   * for handling normal response from vehicle_Message_Create operation
   */
  public void receiveResultvehicle_Message_Create(
      Vehicle_Message_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * vehicle_Message_Create operation
   */
  public void receiveErrorvehicle_Message_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for workGroups_Get method override this method for
   * handling normal response from workGroups_Get operation
   */
  public void receiveResultworkGroups_Get(WorkGroups_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * workGroups_Get operation
   */
  public void receiveErrorworkGroups_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Appointments_Availability_Get method
   * override this method for handling normal response from
   * serviceRequests_Appointments_Availability_Get operation
   */
  public void receiveResultserviceRequests_Appointments_Availability_Get(
      ServiceRequests_Appointments_Availability_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Appointments_Availability_Get operation
   */
  public void receiveErrorserviceRequests_Appointments_Availability_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Schedules_Get method override this method
   * for handling normal response from features_Schedules_Get operation
   */
  public void receiveResultfeatures_Schedules_Get(
      Features_Schedules_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Schedules_Get operation
   */
  public void receiveErrorfeatures_Schedules_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for businesses_Statuses_Get method override this method
   * for handling normal response from businesses_Statuses_Get operation
   */
  public void receiveResultbusinesses_Statuses_Get(
      Businesses_Statuses_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * businesses_Statuses_Get operation
   */
  public void receiveErrorbusinesses_Statuses_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for resources_Types_Get method override this method for
   * handling normal response from resources_Types_Get operation
   */
  public void receiveResultresources_Types_Get(
      Resources_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * resources_Types_Get operation
   */
  public void receiveErrorresources_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for inspections_Classes_Get method override this method
   * for handling normal response from inspections_Classes_Get operation
   */
  public void receiveResultinspections_Classes_Get(
      Inspections_Classes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * inspections_Classes_Get operation
   */
  public void receiveErrorinspections_Classes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for licences_Classes_Get method override this method for
   * handling normal response from licences_Classes_Get operation
   */
  public void receiveResultlicences_Classes_Get(
      Licences_Classes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * licences_Classes_Get operation
   */
  public void receiveErrorlicences_Classes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Appointment_Reservation_Create method
   * override this method for handling normal response from
   * serviceRequest_Appointment_Reservation_Create operation
   */
  public void receiveResultserviceRequest_Appointment_Reservation_Create(
      ServiceRequest_Appointment_Reservation_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Appointment_Reservation_Create operation
   */
  public void receiveErrorserviceRequest_Appointment_Reservation_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Events_Types_Get method override this method
   * for handling normal response from premises_Events_Types_Get operation
   */
  public void receiveResultpremises_Events_Types_Get(
      Premises_Events_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Events_Types_Get operation
   */
  public void receiveErrorpremises_Events_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Events_Document_Create method override this
   * method for handling normal response from premises_Events_Document_Create operation
   */
  public void receiveResultpremises_Events_Document_Create(
      Premises_Events_Document_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Events_Document_Create operation
   */
  public void receiveErrorpremises_Events_Document_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for businesses_Classes_Get method override this method
   * for handling normal response from businesses_Classes_Get operation
   */
  public void receiveResultbusinesses_Classes_Get(
      Businesses_Classes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * businesses_Classes_Get operation
   */
  public void receiveErrorbusinesses_Classes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Attribute_Update method override this method
   * for handling normal response from premises_Attribute_Update operation
   */
  public void receiveResultpremises_Attribute_Update(
      Premises_Attribute_UpdateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Attribute_Update operation
   */
  public void receiveErrorpremises_Attribute_Update(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for vehicle_InspectionForms_Get method override this
   * method for handling normal response from vehicle_InspectionForms_Get operation
   */
  public void receiveResultvehicle_InspectionForms_Get(
      Vehicle_InspectionForms_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * vehicle_InspectionForms_Get operation
   */
  public void receiveErrorvehicle_InspectionForms_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for jobs_WorkScheduleDates_Get method override this
   * method for handling normal response from jobs_WorkScheduleDates_Get operation
   */
  public void receiveResultjobs_WorkScheduleDates_Get(
      Jobs_WorkScheduleDates_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * jobs_WorkScheduleDates_Get operation
   */
  public void receiveErrorjobs_WorkScheduleDates_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for vehicle_InspectionResults_Get method override this
   * method for handling normal response from vehicle_InspectionResults_Get operation
   */
  public void receiveResultvehicle_InspectionResults_Get(
      Vehicle_InspectionResults_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * vehicle_InspectionResults_Get operation
   */
  public void receiveErrorvehicle_InspectionResults_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for inspections_Types_Get method override this method for
   * handling normal response from inspections_Types_Get operation
   */
  public void receiveResultinspections_Types_Get(
      Inspections_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * inspections_Types_Get operation
   */
  public void receiveErrorinspections_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for workpacks_Notes_Get method override this method for
   * handling normal response from workpacks_Notes_Get operation
   */
  public void receiveResultworkpacks_Notes_Get(
      Workpacks_Notes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * workpacks_Notes_Get operation
   */
  public void receiveErrorworkpacks_Notes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for workpacks_Metrics_Get method override this method for
   * handling normal response from workpacks_Metrics_Get operation
   */
  public void receiveResultworkpacks_Metrics_Get(
      Workpacks_Metrics_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * workpacks_Metrics_Get operation
   */
  public void receiveErrorworkpacks_Metrics_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_History_Get method override this
   * method for handling normal response from serviceRequests_History_Get operation
   */
  public void receiveResultserviceRequests_History_Get(
      ServiceRequests_History_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_History_Get operation
   */
  public void receiveErrorserviceRequests_History_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Note_Create method override this
   * method for handling normal response from serviceRequest_Note_Create operation
   */
  public void receiveResultserviceRequest_Note_Create(
      ServiceRequest_Note_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Note_Create operation
   */
  public void receiveErrorserviceRequest_Note_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for business_Document_Create method override this method
   * for handling normal response from business_Document_Create operation
   */
  public void receiveResultbusiness_Document_Create(
      Business_Document_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * business_Document_Create operation
   */
  public void receiveErrorbusiness_Document_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for jobs_Detail_Get method override this method for
   * handling normal response from jobs_Detail_Get operation
   */
  public void receiveResultjobs_Detail_Get(Jobs_Detail_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * jobs_Detail_Get operation
   */
  public void receiveErrorjobs_Detail_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for system_WasteTypes_Get method override this method for
   * handling normal response from system_WasteTypes_Get operation
   */
  public void receiveResultsystem_WasteTypes_Get(
      System_WasteTypes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * system_WasteTypes_Get operation
   */
  public void receiveErrorsystem_WasteTypes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for businesses_Get method override this method for
   * handling normal response from businesses_Get operation
   */
  public void receiveResultbusinesses_Get(Businesses_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * businesses_Get operation
   */
  public void receiveErrorbusinesses_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Attributes_Delete method override this
   * method for handling normal response from premises_Attributes_Delete operation
   */
  public void receiveResultpremises_Attributes_Delete(
      Premises_Attributes_DeleteResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Attributes_Delete operation
   */
  public void receiveErrorpremises_Attributes_Delete(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Statuses_Get method override this method for
   * handling normal response from features_Statuses_Get operation
   */
  public void receiveResultfeatures_Statuses_Get(
      Features_Statuses_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Statuses_Get operation
   */
  public void receiveErrorfeatures_Statuses_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_AttributeDefinitions_Get method override
   * this method for handling normal response from premises_AttributeDefinitions_Get operation
   */
  public void receiveResultpremises_AttributeDefinitions_Get(
      Premises_AttributeDefinitions_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_AttributeDefinitions_Get operation
   */
  public void receiveErrorpremises_AttributeDefinitions_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Get method override this method for handling
   * normal response from features_Get operation
   */
  public void receiveResultfeatures_Get(Features_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Get operation
   */
  public void receiveErrorfeatures_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for inspections_Documents_Get method override this method
   * for handling normal response from inspections_Documents_Get operation
   */
  public void receiveResultinspections_Documents_Get(
      Inspections_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * inspections_Documents_Get operation
   */
  public void receiveErrorinspections_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for jobs_Documents_Get method override this method for
   * handling normal response from jobs_Documents_Get operation
   */
  public void receiveResultjobs_Documents_Get(
      Jobs_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * jobs_Documents_Get operation
   */
  public void receiveErrorjobs_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Document_Create method override this method
   * for handling normal response from premises_Document_Create operation
   */
  public void receiveResultpremises_Document_Create(
      Premises_Document_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Document_Create operation
   */
  public void receiveErrorpremises_Document_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for sites_Documents_GetAll method override this method
   * for handling normal response from sites_Documents_GetAll operation
   */
  public void receiveResultsites_Documents_GetAll(
      Sites_Documents_GetAllResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * sites_Documents_GetAll operation
   */
  public void receiveErrorsites_Documents_GetAll(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Attributes_Get method override this method
   * for handling normal response from premises_Attributes_Get operation
   */
  public void receiveResultpremises_Attributes_Get(
      Premises_Attributes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Attributes_Get operation
   */
  public void receiveErrorpremises_Attributes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for features_Colours_Get method override this method for
   * handling normal response from features_Colours_Get operation
   */
  public void receiveResultfeatures_Colours_Get(
      Features_Colours_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * features_Colours_Get operation
   */
  public void receiveErrorfeatures_Colours_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for licences_Documents_Get method override this method
   * for handling normal response from licences_Documents_Get operation
   */
  public void receiveResultlicences_Documents_Get(
      Licences_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * licences_Documents_Get operation
   */
  public void receiveErrorlicences_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for businesses_Types_Get method override this method for
   * handling normal response from businesses_Types_Get operation
   */
  public void receiveResultbusinesses_Types_Get(
      Businesses_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * businesses_Types_Get operation
   */
  public void receiveErrorbusinesses_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for streets_Events_Types_Get method override this method
   * for handling normal response from streets_Events_Types_Get operation
   */
  public void receiveResultstreets_Events_Types_Get(
      Streets_Events_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * streets_Events_Types_Get operation
   */
  public void receiveErrorstreets_Events_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Appointment_Slot_Reserve method
   * override this method for handling normal response from serviceRequest_Appointment_Slot_Reserve
   * operation
   */
  public void receiveResultserviceRequest_Appointment_Slot_Reserve(
      ServiceRequest_Appointment_Slot_ReserveResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Appointment_Slot_Reserve operation
   */
  public void receiveErrorserviceRequest_Appointment_Slot_Reserve(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for licences_Statuses_Get method override this method for
   * handling normal response from licences_Statuses_Get operation
   */
  public void receiveResultlicences_Statuses_Get(
      Licences_Statuses_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * licences_Statuses_Get operation
   */
  public void receiveErrorlicences_Statuses_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for licences_Types_Get method override this method for
   * handling normal response from licences_Types_Get operation
   */
  public void receiveResultlicences_Types_Get(
      Licences_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * licences_Types_Get operation
   */
  public void receiveErrorlicences_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for sites_Get method override this method for handling
   * normal response from sites_Get operation
   */
  public void receiveResultsites_Get(Sites_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * sites_Get operation
   */
  public void receiveErrorsites_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Get method override this method for
   * handling normal response from serviceRequests_Get operation
   */
  public void receiveResultserviceRequests_Get(
      ServiceRequests_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Get operation
   */
  public void receiveErrorserviceRequests_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Attribute_Create method override this method
   * for handling normal response from premises_Attribute_Create operation
   */
  public void receiveResultpremises_Attribute_Create(
      Premises_Attribute_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Attribute_Create operation
   */
  public void receiveErrorpremises_Attribute_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for inspections_Statuses_Get method override this method
   * for handling normal response from inspections_Statuses_Get operation
   */
  public void receiveResultinspections_Statuses_Get(
      Inspections_Statuses_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * inspections_Statuses_Get operation
   */
  public void receiveErrorinspections_Statuses_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for system_LandTypes_Get method override this method for
   * handling normal response from system_LandTypes_Get operation
   */
  public void receiveResultsystem_LandTypes_Get(
      System_LandTypes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * system_LandTypes_Get operation
   */
  public void receiveErrorsystem_LandTypes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Cancel method override this method for
   * handling normal response from serviceRequest_Cancel operation
   */
  public void receiveResultserviceRequest_Cancel(
      ServiceRequest_CancelResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Cancel operation
   */
  public void receiveErrorserviceRequest_Cancel(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Notes_Types_Get method override this
   * method for handling normal response from serviceRequests_Notes_Types_Get operation
   */
  public void receiveResultserviceRequests_Notes_Types_Get(
      ServiceRequests_Notes_Types_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Notes_Types_Get operation
   */
  public void receiveErrorserviceRequests_Notes_Types_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for premises_Event_Create method override this method for
   * handling normal response from premises_Event_Create operation
   */
  public void receiveResultpremises_Event_Create(
      Premises_Event_CreateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * premises_Event_Create operation
   */
  public void receiveErrorpremises_Event_Create(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Documents_Get method override this
   * method for handling normal response from serviceRequests_Documents_Get operation
   */
  public void receiveResultserviceRequests_Documents_Get(
      ServiceRequests_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Documents_Get operation
   */
  public void receiveErrorserviceRequests_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for resources_Get method override this method for
   * handling normal response from resources_Get operation
   */
  public void receiveResultresources_Get(Resources_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * resources_Get operation
   */
  public void receiveErrorresources_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Appointment_Reservation_Cancel method
   * override this method for handling normal response from
   * serviceRequest_Appointment_Reservation_Cancel operation
   */
  public void receiveResultserviceRequest_Appointment_Reservation_Cancel(
      ServiceRequest_Appointment_Reservation_CancelResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Appointment_Reservation_Cancel operation
   */
  public void receiveErrorserviceRequest_Appointment_Reservation_Cancel(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Detail_Get method override this
   * method for handling normal response from serviceRequests_Detail_Get operation
   */
  public void receiveResultserviceRequests_Detail_Get(
      ServiceRequests_Detail_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Detail_Get operation
   */
  public void receiveErrorserviceRequests_Detail_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequests_Classes_Get method override this
   * method for handling normal response from serviceRequests_Classes_Get operation
   */
  public void receiveResultserviceRequests_Classes_Get(
      ServiceRequests_Classes_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequests_Classes_Get operation
   */
  public void receiveErrorserviceRequests_Classes_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Update method override this method for
   * handling normal response from serviceRequest_Update operation
   */
  public void receiveResultserviceRequest_Update(
      ServiceRequest_UpdateResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Update operation
   */
  public void receiveErrorserviceRequest_Update(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for streets_Events_Get method override this method for
   * handling normal response from streets_Events_Get operation
   */
  public void receiveResultstreets_Events_Get(
      Streets_Events_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * streets_Events_Get operation
   */
  public void receiveErrorstreets_Events_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for resources_Documents_Get method override this method
   * for handling normal response from resources_Documents_Get operation
   */
  public void receiveResultresources_Documents_Get(
      Resources_Documents_GetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * resources_Documents_Get operation
   */
  public void receiveErrorresources_Documents_Get(java.lang.Exception e) {}

  /**
   * auto generated Axis2 call back method for serviceRequest_Status_Set method override this method
   * for handling normal response from serviceRequest_Status_Set operation
   */
  public void receiveResultserviceRequest_Status_Set(
      ServiceRequest_Status_SetResponse result) {}

  /**
   * auto generated Axis2 Error handler override this method for handling error response from
   * serviceRequest_Status_Set operation
   */
  public void receiveErrorserviceRequest_Status_Set(java.lang.Exception e) {}
}
