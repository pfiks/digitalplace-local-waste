/**
 * EventTypes_type1.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd;

/** EventTypes_type1 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class EventTypes_type1 implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = EventTypes_type1
  Namespace URI = http://www.bartec-systems.com/Premises_Events_Types_Get.xsd
  Namespace Prefix = ns70
  */

  /** field for EventType This was an Array! */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[] localEventType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventTypeTracker = false;

  public boolean isEventTypeSpecified() {
    return localEventTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[]
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[] getEventType() {
    return localEventType;
  }

  /** validate the array for EventType */
  protected void validateEventType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[] param) {}

  /**
   * Auto generated setter method
   *
   * @param param EventType
   */
  public void setEventType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[] param) {

    validateEventType(param);

    localEventTypeTracker = param != null;

    this.localEventType = param;
  }

  /**
   * Auto generated add method for the array for convenience
   *
   * @param param com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType
   */
  public void addEventType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType param) {
    if (localEventType == null) {
      localEventType = new com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[] {};
    }

    // update the setting tracker
    localEventTypeTracker = true;

    java.util.List list = org.apache.axis2.databinding.utils.ConverterUtil.toList(localEventType);
    list.add(param);
    this.localEventType =
        (com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[])
            list.toArray(new com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[list.size()]);
  }

  /** field for SubEventTypes */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.SubEventTypes_type1
      localSubEventTypes;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSubEventTypesTracker = false;

  public boolean isSubEventTypesSpecified() {
    return localSubEventTypesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.SubEventTypes_type1
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.SubEventTypes_type1
      getSubEventTypes() {
    return localSubEventTypes;
  }

  /**
   * Auto generated setter method
   *
   * @param param SubEventTypes
   */
  public void setSubEventTypes(
      com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.SubEventTypes_type1 param) {
    localSubEventTypesTracker = param != null;

    this.localSubEventTypes = param;
  }

  /** field for EventClasses */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventClasses_type3
      localEventClasses;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEventClassesTracker = false;

  public boolean isEventClassesSpecified() {
    return localEventClassesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventClasses_type3
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventClasses_type3 getEventClasses() {
    return localEventClasses;
  }

  /**
   * Auto generated setter method
   *
   * @param param EventClasses
   */
  public void setEventClasses(
      com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventClasses_type3 param) {
    localEventClassesTracker = param != null;

    this.localEventClasses = param;
  }

  /** field for RecordCount This was an Attribute! */
  protected int localRecordCount;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getRecordCount() {
    return localRecordCount;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordCount
   */
  public void setRecordCount(int param) {

    this.localRecordCount = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://www.bartec-systems.com/Premises_Events_Types_Get.xsd");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":EventTypes_type1",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "EventTypes_type1",
            xmlWriter);
      }
    }

    if (localRecordCount != java.lang.Integer.MIN_VALUE) {

      writeAttribute(
          "",
          "RecordCount",
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordCount),
          xmlWriter);

    } else {
      throw new org.apache.axis2.databinding.ADBException(
          "required attribute localRecordCount is null");
    }
    if (localEventTypeTracker) {
      if (localEventType != null) {
        for (int i = 0; i < localEventType.length; i++) {
          if (localEventType[i] != null) {
            localEventType[i].serialize(
                new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Premises_Events_Types_Get.xsd", "EventType"),
                xmlWriter);
          } else {

            // we don't have to do any thing since minOccures is zero

          }
        }
      } else {

        throw new org.apache.axis2.databinding.ADBException("EventType cannot be null!!");
      }
    }
    if (localSubEventTypesTracker) {
      if (localSubEventTypes == null) {
        throw new org.apache.axis2.databinding.ADBException("SubEventTypes cannot be null!!");
      }
      localSubEventTypes.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/Premises_Events_Types_Get.xsd", "SubEventTypes"),
          xmlWriter);
    }
    if (localEventClassesTracker) {
      if (localEventClasses == null) {
        throw new org.apache.axis2.databinding.ADBException("EventClasses cannot be null!!");
      }
      localEventClasses.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/Premises_Events_Types_Get.xsd", "EventClasses"),
          xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/Premises_Events_Types_Get.xsd")) {
      return "ns70";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static EventTypes_type1 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      EventTypes_type1 object = new EventTypes_type1();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"EventTypes_type1".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (EventTypes_type1)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        // handle attribute "RecordCount"
        java.lang.String tempAttribRecordCount = reader.getAttributeValue(null, "RecordCount");

        if (tempAttribRecordCount != null) {
          java.lang.String content = tempAttribRecordCount;

          object.setRecordCount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(tempAttribRecordCount));

        } else {

          throw new org.apache.axis2.databinding.ADBException(
              "Required attribute RecordCount is missing");
        }
        handledAttributes.add("RecordCount");

        reader.next();

        java.util.ArrayList list1 = new java.util.ArrayList();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Premises_Events_Types_Get.xsd", "EventType")
                .equals(reader.getName())) {

          // Process the array and step past its final element's end.

          list1.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType.Factory.parse(reader));

          // loop until we find a start element that is not part of this array
          boolean loopDone1 = false;
          while (!loopDone1) {
            // We should be at the end element, but make sure
            while (!reader.isEndElement()) reader.next();
            // Step out of this element
            reader.next();
            // Step to next element event.
            while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
            if (reader.isEndElement()) {
              // two continuous end elements means we are exiting the xml structure
              loopDone1 = true;
            } else {
              if (new javax.xml.namespace.QName(
                      "http://www.bartec-systems.com/Premises_Events_Types_Get.xsd", "EventType")
                  .equals(reader.getName())) {
                list1.add(com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType.Factory.parse(reader));

              } else {
                loopDone1 = true;
              }
            }
          }
          // call the converter utility  to convert and set the array

          object.setEventType(
              (com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType[])
                  org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                      com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType.class, list1));

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Premises_Events_Types_Get.xsd", "SubEventTypes")
                .equals(reader.getName())) {

          object.setSubEventTypes(
              com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.SubEventTypes_type1.Factory
                  .parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Premises_Events_Types_Get.xsd", "EventClasses")
                .equals(reader.getName())) {

          object.setEventClasses(
              com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventClasses_type3.Factory.parse(
                  reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
