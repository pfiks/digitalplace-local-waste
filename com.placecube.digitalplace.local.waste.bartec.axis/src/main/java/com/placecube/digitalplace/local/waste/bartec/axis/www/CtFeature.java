/**
 * CtFeature.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** CtFeature bean class */
@SuppressWarnings({"unchecked", "unused"})
public class CtFeature implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ctFeature
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for ID */
  protected int localID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getID() {
    return localID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ID
   */
  public void setID(int param) {

    this.localID = param;
  }

  /** field for FeatureCode */
  protected java.lang.String localFeatureCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFeatureCodeTracker = false;

  public boolean isFeatureCodeSpecified() {
    return localFeatureCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getFeatureCode() {
    return localFeatureCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param FeatureCode
   */
  public void setFeatureCode(java.lang.String param) {
    localFeatureCodeTracker = param != null;

    this.localFeatureCode = param;
  }

  /** field for Name */
  protected java.lang.String localName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localNameTracker = false;

  public boolean isNameSpecified() {
    return localNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getName() {
    return localName;
  }

  /**
   * Auto generated setter method
   *
   * @param param Name
   */
  public void setName(java.lang.String param) {
    localNameTracker = param != null;

    this.localName = param;
  }

  /** field for Description */
  protected java.lang.String localDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDescriptionTracker = false;

  public boolean isDescriptionSpecified() {
    return localDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getDescription() {
    return localDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param Description
   */
  public void setDescription(java.lang.String param) {
    localDescriptionTracker = param != null;

    this.localDescription = param;
  }

  /** field for UPRN */
  protected java.math.BigDecimal localUPRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUPRN() {
    return localUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRN
   */
  public void setUPRN(java.math.BigDecimal param) {

    this.localUPRN = param;
  }

  /** field for Status */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureStatus localStatus;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localStatusTracker = false;

  public boolean isStatusSpecified() {
    return localStatusTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureStatus
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureStatus getStatus() {
    return localStatus;
  }

  /**
   * Auto generated setter method
   *
   * @param param Status
   */
  public void setStatus(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureStatus param) {
    localStatusTracker = param != null;

    this.localStatus = param;
  }

  /** field for FeatureType */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureType localFeatureType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFeatureTypeTracker = false;

  public boolean isFeatureTypeSpecified() {
    return localFeatureTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureType
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureType getFeatureType() {
    return localFeatureType;
  }

  /**
   * Auto generated setter method
   *
   * @param param FeatureType
   */
  public void setFeatureType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureType param) {
    localFeatureTypeTracker = param != null;

    this.localFeatureType = param;
  }

  /** field for Manufacturer */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureManufacturer localManufacturer;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localManufacturerTracker = false;

  public boolean isManufacturerSpecified() {
    return localManufacturerTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureManufacturer
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureManufacturer getManufacturer() {
    return localManufacturer;
  }

  /**
   * Auto generated setter method
   *
   * @param param Manufacturer
   */
  public void setManufacturer(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureManufacturer param) {
    localManufacturerTracker = param != null;

    this.localManufacturer = param;
  }

  /** field for Condition */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureCondition localCondition;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localConditionTracker = false;

  public boolean isConditionSpecified() {
    return localConditionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureCondition
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureCondition getCondition() {
    return localCondition;
  }

  /**
   * Auto generated setter method
   *
   * @param param Condition
   */
  public void setCondition(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureCondition param) {
    localConditionTracker = param != null;

    this.localCondition = param;
  }

  /** field for Colour */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureColour localColour;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localColourTracker = false;

  public boolean isColourSpecified() {
    return localColourTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureColour
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureColour getColour() {
    return localColour;
  }

  /**
   * Auto generated setter method
   *
   * @param param Colour
   */
  public void setColour(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureColour param) {
    localColourTracker = param != null;

    this.localColour = param;
  }

  /** field for QuantityVolume */
  protected java.math.BigDecimal localQuantityVolume;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getQuantityVolume() {
    return localQuantityVolume;
  }

  /**
   * Auto generated setter method
   *
   * @param param QuantityVolume
   */
  public void setQuantityVolume(java.math.BigDecimal param) {

    this.localQuantityVolume = param;
  }

  /** field for QuantityArea */
  protected java.math.BigDecimal localQuantityArea;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getQuantityArea() {
    return localQuantityArea;
  }

  /**
   * Auto generated setter method
   *
   * @param param QuantityArea
   */
  public void setQuantityArea(java.math.BigDecimal param) {

    this.localQuantityArea = param;
  }

  /** field for QuantityLinear */
  protected java.math.BigDecimal localQuantityLinear;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getQuantityLinear() {
    return localQuantityLinear;
  }

  /**
   * Auto generated setter method
   *
   * @param param QuantityLinear
   */
  public void setQuantityLinear(java.math.BigDecimal param) {

    this.localQuantityLinear = param;
  }

  /** field for QuantityEach */
  protected java.math.BigDecimal localQuantityEach;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getQuantityEach() {
    return localQuantityEach;
  }

  /**
   * Auto generated setter method
   *
   * @param param QuantityEach
   */
  public void setQuantityEach(java.math.BigDecimal param) {

    this.localQuantityEach = param;
  }

  /** field for QuantityWeight */
  protected java.math.BigDecimal localQuantityWeight;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getQuantityWeight() {
    return localQuantityWeight;
  }

  /**
   * Auto generated setter method
   *
   * @param param QuantityWeight
   */
  public void setQuantityWeight(java.math.BigDecimal param) {

    this.localQuantityWeight = param;
  }

  /** field for Longitude */
  protected java.math.BigDecimal localLongitude;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getLongitude() {
    return localLongitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param Longitude
   */
  public void setLongitude(java.math.BigDecimal param) {

    this.localLongitude = param;
  }

  /** field for Latitude */
  protected java.math.BigDecimal localLatitude;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getLatitude() {
    return localLatitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param Latitude
   */
  public void setLatitude(java.math.BigDecimal param) {

    this.localLatitude = param;
  }

  /** field for AssetTag */
  protected java.lang.String localAssetTag;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAssetTagTracker = false;

  public boolean isAssetTagSpecified() {
    return localAssetTagTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAssetTag() {
    return localAssetTag;
  }

  /**
   * Auto generated setter method
   *
   * @param param AssetTag
   */
  public void setAssetTag(java.lang.String param) {
    localAssetTagTracker = param != null;

    this.localAssetTag = param;
  }

  /** field for RFIDTag */
  protected java.lang.String localRFIDTag;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRFIDTagTracker = false;

  public boolean isRFIDTagSpecified() {
    return localRFIDTagTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getRFIDTag() {
    return localRFIDTag;
  }

  /**
   * Auto generated setter method
   *
   * @param param RFIDTag
   */
  public void setRFIDTag(java.lang.String param) {
    localRFIDTagTracker = param != null;

    this.localRFIDTag = param;
  }

  /** field for SerialNumber */
  protected java.lang.String localSerialNumber;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSerialNumberTracker = false;

  public boolean isSerialNumberSpecified() {
    return localSerialNumberTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSerialNumber() {
    return localSerialNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param SerialNumber
   */
  public void setSerialNumber(java.lang.String param) {
    localSerialNumberTracker = param != null;

    this.localSerialNumber = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ctFeature",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "ctFeature", xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ID", xmlWriter);

    if (localID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localID));
    }

    xmlWriter.writeEndElement();
    if (localFeatureCodeTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "FeatureCode", xmlWriter);

      if (localFeatureCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("FeatureCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localFeatureCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localNameTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Name", xmlWriter);

      if (localName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Name cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localName);
      }

      xmlWriter.writeEndElement();
    }
    if (localDescriptionTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Description", xmlWriter);

      if (localDescription == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Description cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localDescription);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "UPRN", xmlWriter);

    if (localUPRN == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("UPRN cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
    }

    xmlWriter.writeEndElement();
    if (localStatusTracker) {
      if (localStatus == null) {
        throw new org.apache.axis2.databinding.ADBException("Status cannot be null!!");
      }
      localStatus.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Status"), xmlWriter);
    }
    if (localFeatureTypeTracker) {
      if (localFeatureType == null) {
        throw new org.apache.axis2.databinding.ADBException("FeatureType cannot be null!!");
      }
      localFeatureType.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "FeatureType"), xmlWriter);
    }
    if (localManufacturerTracker) {
      if (localManufacturer == null) {
        throw new org.apache.axis2.databinding.ADBException("Manufacturer cannot be null!!");
      }
      localManufacturer.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Manufacturer"),
          xmlWriter);
    }
    if (localConditionTracker) {
      if (localCondition == null) {
        throw new org.apache.axis2.databinding.ADBException("Condition cannot be null!!");
      }
      localCondition.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Condition"), xmlWriter);
    }
    if (localColourTracker) {
      if (localColour == null) {
        throw new org.apache.axis2.databinding.ADBException("Colour cannot be null!!");
      }
      localColour.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Colour"), xmlWriter);
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "QuantityVolume", xmlWriter);

    if (localQuantityVolume == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityVolume));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "QuantityArea", xmlWriter);

    if (localQuantityArea == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityArea));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "QuantityLinear", xmlWriter);

    if (localQuantityLinear == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityLinear));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "QuantityEach", xmlWriter);

    if (localQuantityEach == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityEach));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "QuantityWeight", xmlWriter);

    if (localQuantityWeight == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityWeight));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "Longitude", xmlWriter);

    if (localLongitude == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("Longitude cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLongitude));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "Latitude", xmlWriter);

    if (localLatitude == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("Latitude cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLatitude));
    }

    xmlWriter.writeEndElement();
    if (localAssetTagTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "AssetTag", xmlWriter);

      if (localAssetTag == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("AssetTag cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAssetTag);
      }

      xmlWriter.writeEndElement();
    }
    if (localRFIDTagTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "RFIDTag", xmlWriter);

      if (localRFIDTag == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("RFIDTag cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localRFIDTag);
      }

      xmlWriter.writeEndElement();
    }
    if (localSerialNumberTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "SerialNumber", xmlWriter);

      if (localSerialNumber == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("SerialNumber cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSerialNumber);
      }

      xmlWriter.writeEndElement();
    }
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static CtFeature parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      CtFeature object = new CtFeature();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ctFeature".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (CtFeature)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "FeatureCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "FeatureCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setFeatureCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Name")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Name" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setName(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Description")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Description" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDescription(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UPRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUPRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Status")
                .equals(reader.getName())) {

          object.setStatus(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureStatus.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "FeatureType")
                .equals(reader.getName())) {

          object.setFeatureType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureType.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Manufacturer")
                .equals(reader.getName())) {

          object.setManufacturer(
              com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureManufacturer.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Condition")
                .equals(reader.getName())) {

          object.setCondition(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureCondition.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Colour")
                .equals(reader.getName())) {

          object.setColour(com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureColour.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "QuantityVolume")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setQuantityVolume(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "QuantityArea")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setQuantityArea(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "QuantityLinear")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setQuantityLinear(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "QuantityEach")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setQuantityEach(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "QuantityWeight")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setQuantityWeight(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Longitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Longitude" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLongitude(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Latitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Latitude" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLatitude(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "AssetTag")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "AssetTag" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAssetTag(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RFIDTag")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "RFIDTag" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setRFIDTag(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "SerialNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SerialNumber" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSerialNumber(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
