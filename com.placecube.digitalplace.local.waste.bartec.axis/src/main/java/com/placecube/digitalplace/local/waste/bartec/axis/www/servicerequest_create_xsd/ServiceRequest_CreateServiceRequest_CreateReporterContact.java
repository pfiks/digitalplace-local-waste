/**
 * ServiceRequest_CreateServiceRequest_CreateReporterContact.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd;

/** ServiceRequest_CreateServiceRequest_CreateReporterContact bean class */
@SuppressWarnings({"unchecked", "unused"})
public class ServiceRequest_CreateServiceRequest_CreateReporterContact
    implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ServiceRequest_CreateServiceRequest_CreateReporterContact
  Namespace URI = http://www.bartec-systems.com/ServiceRequest_Create.xsd
  Namespace Prefix = ns27
  */

  /** field for Title */
  protected java.lang.String localTitle;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTitleTracker = false;

  public boolean isTitleSpecified() {
    return localTitleTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTitle() {
    return localTitle;
  }

  /**
   * Auto generated setter method
   *
   * @param param Title
   */
  public void setTitle(java.lang.String param) {
    localTitleTracker = param != null;

    this.localTitle = param;
  }

  /** field for Forename */
  protected java.lang.String localForename;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localForenameTracker = false;

  public boolean isForenameSpecified() {
    return localForenameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getForename() {
    return localForename;
  }

  /**
   * Auto generated setter method
   *
   * @param param Forename
   */
  public void setForename(java.lang.String param) {
    localForenameTracker = param != null;

    this.localForename = param;
  }

  /** field for OtherNames */
  protected java.lang.String localOtherNames;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localOtherNamesTracker = false;

  public boolean isOtherNamesSpecified() {
    return localOtherNamesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getOtherNames() {
    return localOtherNames;
  }

  /**
   * Auto generated setter method
   *
   * @param param OtherNames
   */
  public void setOtherNames(java.lang.String param) {
    localOtherNamesTracker = param != null;

    this.localOtherNames = param;
  }

  /** field for Surname */
  protected java.lang.String localSurname;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSurnameTracker = false;

  public boolean isSurnameSpecified() {
    return localSurnameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSurname() {
    return localSurname;
  }

  /**
   * Auto generated setter method
   *
   * @param param Surname
   */
  public void setSurname(java.lang.String param) {
    localSurnameTracker = param != null;

    this.localSurname = param;
  }

  /** field for Email */
  protected java.lang.String localEmail;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localEmailTracker = false;

  public boolean isEmailSpecified() {
    return localEmailTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getEmail() {
    return localEmail;
  }

  /**
   * Auto generated setter method
   *
   * @param param Email
   */
  public void setEmail(java.lang.String param) {
    localEmailTracker = param != null;

    this.localEmail = param;
  }

  /** field for Telephone */
  protected java.lang.String localTelephone;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTelephoneTracker = false;

  public boolean isTelephoneSpecified() {
    return localTelephoneTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTelephone() {
    return localTelephone;
  }

  /**
   * Auto generated setter method
   *
   * @param param Telephone
   */
  public void setTelephone(java.lang.String param) {
    localTelephoneTracker = param != null;

    this.localTelephone = param;
  }

  /** field for Telephone2 */
  protected java.lang.String localTelephone2;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTelephone2Tracker = false;

  public boolean isTelephone2Specified() {
    return localTelephone2Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTelephone2() {
    return localTelephone2;
  }

  /**
   * Auto generated setter method
   *
   * @param param Telephone2
   */
  public void setTelephone2(java.lang.String param) {
    localTelephone2Tracker = param != null;

    this.localTelephone2 = param;
  }

  /** field for Telephone3 */
  protected java.lang.String localTelephone3;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTelephone3Tracker = false;

  public boolean isTelephone3Specified() {
    return localTelephone3Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTelephone3() {
    return localTelephone3;
  }

  /**
   * Auto generated setter method
   *
   * @param param Telephone3
   */
  public void setTelephone3(java.lang.String param) {
    localTelephone3Tracker = param != null;

    this.localTelephone3 = param;
  }

  /** field for SpecialCommunicationNeeds */
  protected java.lang.String localSpecialCommunicationNeeds;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSpecialCommunicationNeedsTracker = false;

  public boolean isSpecialCommunicationNeedsSpecified() {
    return localSpecialCommunicationNeedsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSpecialCommunicationNeeds() {
    return localSpecialCommunicationNeeds;
  }

  /**
   * Auto generated setter method
   *
   * @param param SpecialCommunicationNeeds
   */
  public void setSpecialCommunicationNeeds(java.lang.String param) {
    localSpecialCommunicationNeedsTracker = param != null;

    this.localSpecialCommunicationNeeds = param;
  }

  /** field for ExternalReference */
  protected java.lang.String localExternalReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExternalReferenceTracker = false;

  public boolean isExternalReferenceSpecified() {
    return localExternalReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getExternalReference() {
    return localExternalReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExternalReference
   */
  public void setExternalReference(java.lang.String param) {
    localExternalReferenceTracker = param != null;

    this.localExternalReference = param;
  }

  /** field for ReporterType */
  protected java.lang.String localReporterType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localReporterTypeTracker = false;

  public boolean isReporterTypeSpecified() {
    return localReporterTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getReporterType() {
    return localReporterType;
  }

  /**
   * Auto generated setter method
   *
   * @param param ReporterType
   */
  public void setReporterType(java.lang.String param) {
    localReporterTypeTracker = param != null;

    this.localReporterType = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://www.bartec-systems.com/ServiceRequest_Create.xsd");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ServiceRequest_CreateServiceRequest_CreateReporterContact",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ServiceRequest_CreateServiceRequest_CreateReporterContact",
            xmlWriter);
      }
    }
    if (localTitleTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "Title", xmlWriter);

      if (localTitle == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Title cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTitle);
      }

      xmlWriter.writeEndElement();
    }
    if (localForenameTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "Forename", xmlWriter);

      if (localForename == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Forename cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localForename);
      }

      xmlWriter.writeEndElement();
    }
    if (localOtherNamesTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "OtherNames", xmlWriter);

      if (localOtherNames == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("OtherNames cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localOtherNames);
      }

      xmlWriter.writeEndElement();
    }
    if (localSurnameTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "Surname", xmlWriter);

      if (localSurname == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Surname cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSurname);
      }

      xmlWriter.writeEndElement();
    }
    if (localEmailTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "Email", xmlWriter);

      if (localEmail == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Email cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localEmail);
      }

      xmlWriter.writeEndElement();
    }
    if (localTelephoneTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "Telephone", xmlWriter);

      if (localTelephone == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Telephone cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTelephone);
      }

      xmlWriter.writeEndElement();
    }
    if (localTelephone2Tracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "Telephone2", xmlWriter);

      if (localTelephone2 == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Telephone2 cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTelephone2);
      }

      xmlWriter.writeEndElement();
    }
    if (localTelephone3Tracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "Telephone3", xmlWriter);

      if (localTelephone3 == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Telephone3 cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTelephone3);
      }

      xmlWriter.writeEndElement();
    }
    if (localSpecialCommunicationNeedsTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "SpecialCommunicationNeeds", xmlWriter);

      if (localSpecialCommunicationNeeds == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "SpecialCommunicationNeeds cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSpecialCommunicationNeeds);
      }

      xmlWriter.writeEndElement();
    }
    if (localExternalReferenceTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "ExternalReference", xmlWriter);

      if (localExternalReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ExternalReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localExternalReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localReporterTypeTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Create.xsd";
      writeStartElement(null, namespace, "ReporterType", xmlWriter);

      if (localReporterType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ReporterType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localReporterType);
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/ServiceRequest_Create.xsd")) {
      return "ns27";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static ServiceRequest_CreateServiceRequest_CreateReporterContact parse(
        javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
      ServiceRequest_CreateServiceRequest_CreateReporterContact object =
          new ServiceRequest_CreateServiceRequest_CreateReporterContact();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ServiceRequest_CreateServiceRequest_CreateReporterContact".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (ServiceRequest_CreateServiceRequest_CreateReporterContact)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "Title")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Title" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTitle(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "Forename")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Forename" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setForename(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "OtherNames")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "OtherNames" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setOtherNames(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "Surname")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Surname" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSurname(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "Email")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Email" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setEmail(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "Telephone")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Telephone" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTelephone(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "Telephone2")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Telephone2" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTelephone2(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "Telephone3")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Telephone3" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTelephone3(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd",
                    "SpecialCommunicationNeeds")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SpecialCommunicationNeeds" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSpecialCommunicationNeeds(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "ExternalReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExternalReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExternalReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Create.xsd", "ReporterType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ReporterType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setReporterType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
