/**
 * CtExtendedDataFieldDefinition.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** CtExtendedDataFieldDefinition bean class */
@SuppressWarnings({"unchecked", "unused"})
public class CtExtendedDataFieldDefinition implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ctExtendedDataFieldDefinition
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for ID */
  protected int localID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getID() {
    return localID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ID
   */
  public void setID(int param) {

    this.localID = param;
  }

  /** field for SequenceNumber */
  protected int localSequenceNumber;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSequenceNumber() {
    return localSequenceNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param SequenceNumber
   */
  public void setSequenceNumber(int param) {

    this.localSequenceNumber = param;
  }

  /** field for FieldName */
  protected java.lang.String localFieldName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFieldNameTracker = false;

  public boolean isFieldNameSpecified() {
    return localFieldNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getFieldName() {
    return localFieldName;
  }

  /**
   * Auto generated setter method
   *
   * @param param FieldName
   */
  public void setFieldName(java.lang.String param) {
    localFieldNameTracker = param != null;

    this.localFieldName = param;
  }

  /** field for DataType */
  protected java.lang.String localDataType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDataTypeTracker = false;

  public boolean isDataTypeSpecified() {
    return localDataTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getDataType() {
    return localDataType;
  }

  /**
   * Auto generated setter method
   *
   * @param param DataType
   */
  public void setDataType(java.lang.String param) {
    localDataTypeTracker = param != null;

    this.localDataType = param;
  }

  /** field for Mandatory */
  protected boolean localMandatory;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getMandatory() {
    return localMandatory;
  }

  /**
   * Auto generated setter method
   *
   * @param param Mandatory
   */
  public void setMandatory(boolean param) {

    this.localMandatory = param;
  }

  /** field for UniqueValue */
  protected boolean localUniqueValue;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getUniqueValue() {
    return localUniqueValue;
  }

  /**
   * Auto generated setter method
   *
   * @param param UniqueValue
   */
  public void setUniqueValue(boolean param) {

    this.localUniqueValue = param;
  }

  /** field for Caption */
  protected java.lang.String localCaption;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCaptionTracker = false;

  public boolean isCaptionSpecified() {
    return localCaptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getCaption() {
    return localCaption;
  }

  /**
   * Auto generated setter method
   *
   * @param param Caption
   */
  public void setCaption(java.lang.String param) {
    localCaptionTracker = param != null;

    this.localCaption = param;
  }

  /** field for Locked */
  protected boolean localLocked;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getLocked() {
    return localLocked;
  }

  /**
   * Auto generated setter method
   *
   * @param param Locked
   */
  public void setLocked(boolean param) {

    this.localLocked = param;
  }

  /** field for Tooltip */
  protected java.lang.String localTooltip;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTooltipTracker = false;

  public boolean isTooltipSpecified() {
    return localTooltipTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTooltip() {
    return localTooltip;
  }

  /**
   * Auto generated setter method
   *
   * @param param Tooltip
   */
  public void setTooltip(java.lang.String param) {
    localTooltipTracker = param != null;

    this.localTooltip = param;
  }

  /** field for InboundXMLTag */
  protected java.lang.String localInboundXMLTag;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localInboundXMLTagTracker = false;

  public boolean isInboundXMLTagSpecified() {
    return localInboundXMLTagTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getInboundXMLTag() {
    return localInboundXMLTag;
  }

  /**
   * Auto generated setter method
   *
   * @param param InboundXMLTag
   */
  public void setInboundXMLTag(java.lang.String param) {
    localInboundXMLTagTracker = param != null;

    this.localInboundXMLTag = param;
  }

  /** field for Numeric */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits localNumeric;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localNumericTracker = false;

  public boolean isNumericSpecified() {
    return localNumericTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits getNumeric() {
    return localNumeric;
  }

  /**
   * Auto generated setter method
   *
   * @param param Numeric
   */
  public void setNumeric(com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits param) {
    localNumericTracker = param != null;

    this.localNumeric = param;
  }

  /** field for Date */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateLimits localDate;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDateTracker = false;

  public boolean isDateSpecified() {
    return localDateTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateLimits
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateLimits getDate() {
    return localDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param Date
   */
  public void setDate(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateLimits param) {
    localDateTracker = param != null;

    this.localDate = param;
  }

  /** field for String */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits localString;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localStringTracker = false;

  public boolean isStringSpecified() {
    return localStringTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits getString() {
    return localString;
  }

  /**
   * Auto generated setter method
   *
   * @param param String
   */
  public void setString(com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits param) {
    localStringTracker = param != null;

    this.localString = param;
  }

  /** field for ValueList */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataValueList localValueList;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localValueListTracker = false;

  public boolean isValueListSpecified() {
    return localValueListTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataValueList
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataValueList getValueList() {
    return localValueList;
  }

  /**
   * Auto generated setter method
   *
   * @param param ValueList
   */
  public void setValueList(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataValueList param) {
    localValueListTracker = param != null;

    this.localValueList = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ctExtendedDataFieldDefinition",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ctExtendedDataFieldDefinition",
            xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ID", xmlWriter);

    if (localID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "SequenceNumber", xmlWriter);

    if (localSequenceNumber == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("SequenceNumber cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSequenceNumber));
    }

    xmlWriter.writeEndElement();
    if (localFieldNameTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "FieldName", xmlWriter);

      if (localFieldName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("FieldName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localFieldName);
      }

      xmlWriter.writeEndElement();
    }
    if (localDataTypeTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "DataType", xmlWriter);

      if (localDataType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("DataType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localDataType);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "Mandatory", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("Mandatory cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMandatory));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "UniqueValue", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("UniqueValue cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUniqueValue));
    }

    xmlWriter.writeEndElement();
    if (localCaptionTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Caption", xmlWriter);

      if (localCaption == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Caption cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localCaption);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "Locked", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("Locked cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocked));
    }

    xmlWriter.writeEndElement();
    if (localTooltipTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Tooltip", xmlWriter);

      if (localTooltip == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Tooltip cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTooltip);
      }

      xmlWriter.writeEndElement();
    }
    if (localInboundXMLTagTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "InboundXMLTag", xmlWriter);

      if (localInboundXMLTag == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("InboundXMLTag cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localInboundXMLTag);
      }

      xmlWriter.writeEndElement();
    }
    if (localNumericTracker) {
      if (localNumeric == null) {
        throw new org.apache.axis2.databinding.ADBException("Numeric cannot be null!!");
      }
      localNumeric.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Numeric"), xmlWriter);
    }
    if (localDateTracker) {
      if (localDate == null) {
        throw new org.apache.axis2.databinding.ADBException("Date cannot be null!!");
      }
      localDate.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "Date"), xmlWriter);
    }
    if (localStringTracker) {
      if (localString == null) {
        throw new org.apache.axis2.databinding.ADBException("String cannot be null!!");
      }
      localString.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "String"), xmlWriter);
    }
    if (localValueListTracker) {
      if (localValueList == null) {
        throw new org.apache.axis2.databinding.ADBException("ValueList cannot be null!!");
      }
      localValueList.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "ValueList"), xmlWriter);
    }
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static CtExtendedDataFieldDefinition parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      CtExtendedDataFieldDefinition object = new CtExtendedDataFieldDefinition();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ctExtendedDataFieldDefinition".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (CtExtendedDataFieldDefinition)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "SequenceNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SequenceNumber" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSequenceNumber(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "FieldName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "FieldName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setFieldName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "DataType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DataType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDataType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Mandatory")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Mandatory" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setMandatory(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "UniqueValue")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UniqueValue" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUniqueValue(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Caption")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Caption" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCaption(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Locked")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Locked" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLocked(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Tooltip")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Tooltip" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTooltip(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "InboundXMLTag")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "InboundXMLTag" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setInboundXMLTag(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Numeric")
                .equals(reader.getName())) {

          object.setNumeric(com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Date")
                .equals(reader.getName())) {

          object.setDate(com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateLimits.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "String")
                .equals(reader.getName())) {

          object.setString(com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ValueList")
                .equals(reader.getName())) {

          object.setValueList(com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataValueList.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
