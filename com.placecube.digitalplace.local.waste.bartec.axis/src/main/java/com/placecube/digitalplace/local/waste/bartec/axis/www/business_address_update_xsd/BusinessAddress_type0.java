/**
 * BusinessAddress_type0.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.business_address_update_xsd;

/** BusinessAddress_type0 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class BusinessAddress_type0 implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = BusinessAddress_type0
  Namespace URI = http://www.bartec-systems.com/Business_Address_Update.xsd
  Namespace Prefix = ns39
  */

  /** field for BusinessAddressID */
  protected int localBusinessAddressID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getBusinessAddressID() {
    return localBusinessAddressID;
  }

  /**
   * Auto generated setter method
   *
   * @param param BusinessAddressID
   */
  public void setBusinessAddressID(int param) {

    this.localBusinessAddressID = param;
  }

  /** field for Type */
  protected java.lang.String localType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTypeTracker = false;

  public boolean isTypeSpecified() {
    return localTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getType() {
    return localType;
  }

  /**
   * Auto generated setter method
   *
   * @param param Type
   */
  public void setType(java.lang.String param) {
    localTypeTracker = param != null;

    this.localType = param;
  }

  /** field for Address1 */
  protected java.lang.String localAddress1;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAddress1Tracker = false;

  public boolean isAddress1Specified() {
    return localAddress1Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAddress1() {
    return localAddress1;
  }

  /**
   * Auto generated setter method
   *
   * @param param Address1
   */
  public void setAddress1(java.lang.String param) {
    localAddress1Tracker = param != null;

    this.localAddress1 = param;
  }

  /** field for Address2 */
  protected java.lang.String localAddress2;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAddress2Tracker = false;

  public boolean isAddress2Specified() {
    return localAddress2Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAddress2() {
    return localAddress2;
  }

  /**
   * Auto generated setter method
   *
   * @param param Address2
   */
  public void setAddress2(java.lang.String param) {
    localAddress2Tracker = param != null;

    this.localAddress2 = param;
  }

  /** field for Street */
  protected java.lang.String localStreet;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localStreetTracker = false;

  public boolean isStreetSpecified() {
    return localStreetTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getStreet() {
    return localStreet;
  }

  /**
   * Auto generated setter method
   *
   * @param param Street
   */
  public void setStreet(java.lang.String param) {
    localStreetTracker = param != null;

    this.localStreet = param;
  }

  /** field for Locality */
  protected java.lang.String localLocality;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLocalityTracker = false;

  public boolean isLocalitySpecified() {
    return localLocalityTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getLocality() {
    return localLocality;
  }

  /**
   * Auto generated setter method
   *
   * @param param Locality
   */
  public void setLocality(java.lang.String param) {
    localLocalityTracker = param != null;

    this.localLocality = param;
  }

  /** field for Town */
  protected java.lang.String localTown;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTownTracker = false;

  public boolean isTownSpecified() {
    return localTownTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTown() {
    return localTown;
  }

  /**
   * Auto generated setter method
   *
   * @param param Town
   */
  public void setTown(java.lang.String param) {
    localTownTracker = param != null;

    this.localTown = param;
  }

  /** field for PostCode */
  protected java.lang.String localPostCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPostCodeTracker = false;

  public boolean isPostCodeSpecified() {
    return localPostCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPostCode() {
    return localPostCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param PostCode
   */
  public void setPostCode(java.lang.String param) {
    localPostCodeTracker = param != null;

    this.localPostCode = param;
  }

  /** field for ContactName */
  protected java.lang.String localContactName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactNameTracker = false;

  public boolean isContactNameSpecified() {
    return localContactNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactName() {
    return localContactName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactName
   */
  public void setContactName(java.lang.String param) {
    localContactNameTracker = param != null;

    this.localContactName = param;
  }

  /** field for ContactTelephone */
  protected java.lang.String localContactTelephone;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactTelephoneTracker = false;

  public boolean isContactTelephoneSpecified() {
    return localContactTelephoneTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactTelephone() {
    return localContactTelephone;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactTelephone
   */
  public void setContactTelephone(java.lang.String param) {
    localContactTelephoneTracker = param != null;

    this.localContactTelephone = param;
  }

  /** field for ContactEmail */
  protected java.lang.String localContactEmail;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactEmailTracker = false;

  public boolean isContactEmailSpecified() {
    return localContactEmailTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactEmail() {
    return localContactEmail;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactEmail
   */
  public void setContactEmail(java.lang.String param) {
    localContactEmailTracker = param != null;

    this.localContactEmail = param;
  }

  /** field for ContactFax */
  protected java.lang.String localContactFax;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactFaxTracker = false;

  public boolean isContactFaxSpecified() {
    return localContactFaxTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactFax() {
    return localContactFax;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactFax
   */
  public void setContactFax(java.lang.String param) {
    localContactFaxTracker = param != null;

    this.localContactFax = param;
  }

  /** field for BillingAddress */
  protected boolean localBillingAddress;

  /**
   * Auto generated getter method
   *
   * @return boolean
   */
  public boolean getBillingAddress() {
    return localBillingAddress;
  }

  /**
   * Auto generated setter method
   *
   * @param param BillingAddress
   */
  public void setBillingAddress(boolean param) {

    this.localBillingAddress = param;
  }

  /** field for Comments */
  protected java.lang.String localComments;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCommentsTracker = false;

  public boolean isCommentsSpecified() {
    return localCommentsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getComments() {
    return localComments;
  }

  /**
   * Auto generated setter method
   *
   * @param param Comments
   */
  public void setComments(java.lang.String param) {
    localCommentsTracker = param != null;

    this.localComments = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://www.bartec-systems.com/Business_Address_Update.xsd");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":BusinessAddress_type0",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "BusinessAddress_type0",
            xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
    writeStartElement(null, namespace, "BusinessAddressID", xmlWriter);

    if (localBusinessAddressID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("BusinessAddressID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBusinessAddressID));
    }

    xmlWriter.writeEndElement();
    if (localTypeTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "Type", xmlWriter);

      if (localType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Type cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localType);
      }

      xmlWriter.writeEndElement();
    }
    if (localAddress1Tracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "Address1", xmlWriter);

      if (localAddress1 == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Address1 cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAddress1);
      }

      xmlWriter.writeEndElement();
    }
    if (localAddress2Tracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "Address2", xmlWriter);

      if (localAddress2 == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Address2 cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAddress2);
      }

      xmlWriter.writeEndElement();
    }
    if (localStreetTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "Street", xmlWriter);

      if (localStreet == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Street cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localStreet);
      }

      xmlWriter.writeEndElement();
    }
    if (localLocalityTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "Locality", xmlWriter);

      if (localLocality == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Locality cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localLocality);
      }

      xmlWriter.writeEndElement();
    }
    if (localTownTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "Town", xmlWriter);

      if (localTown == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Town cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTown);
      }

      xmlWriter.writeEndElement();
    }
    if (localPostCodeTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "PostCode", xmlWriter);

      if (localPostCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("PostCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localPostCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactNameTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "ContactName", xmlWriter);

      if (localContactName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactTelephoneTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "ContactTelephone", xmlWriter);

      if (localContactTelephone == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactTelephone cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactTelephone);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactEmailTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "ContactEmail", xmlWriter);

      if (localContactEmail == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactEmail cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactEmail);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactFaxTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "ContactFax", xmlWriter);

      if (localContactFax == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactFax cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactFax);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
    writeStartElement(null, namespace, "BillingAddress", xmlWriter);

    if (false) {

      throw new org.apache.axis2.databinding.ADBException("BillingAddress cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBillingAddress));
    }

    xmlWriter.writeEndElement();
    if (localCommentsTracker) {
      namespace = "http://www.bartec-systems.com/Business_Address_Update.xsd";
      writeStartElement(null, namespace, "Comments", xmlWriter);

      if (localComments == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Comments cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localComments);
      }

      xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/Business_Address_Update.xsd")) {
      return "ns39";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static BusinessAddress_type0 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      BusinessAddress_type0 object = new BusinessAddress_type0();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"BusinessAddress_type0".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (BusinessAddress_type0)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd",
                    "BusinessAddressID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "BusinessAddressID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setBusinessAddressID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "Type")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Type" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setType(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "Address1")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Address1" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAddress1(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "Address2")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Address2" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAddress2(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "Street")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Street" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setStreet(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "Locality")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Locality" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLocality(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "Town")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Town" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTown(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "PostCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PostCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPostCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "ContactName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "ContactTelephone")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactTelephone" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactTelephone(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "ContactEmail")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactEmail" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactEmail(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "ContactFax")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactFax" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactFax(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "BillingAddress")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "BillingAddress" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setBillingAddress(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Business_Address_Update.xsd", "Comments")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Comments" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setComments(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
