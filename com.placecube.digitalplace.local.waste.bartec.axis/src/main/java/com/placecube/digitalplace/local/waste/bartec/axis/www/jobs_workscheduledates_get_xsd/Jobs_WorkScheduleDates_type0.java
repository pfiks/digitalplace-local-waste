/**
 * Jobs_WorkScheduleDates_type0.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_workscheduledates_get_xsd;

/** Jobs_WorkScheduleDates_type0 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class Jobs_WorkScheduleDates_type0 implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = Jobs_WorkScheduleDates_type0
  Namespace URI = http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd
  Namespace Prefix = ns85
  */

  /** field for JobID */
  protected int localJobID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getJobID() {
    return localJobID;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobID
   */
  public void setJobID(int param) {

    this.localJobID = param;
  }

  /** field for JobName */
  protected java.lang.String localJobName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localJobNameTracker = false;

  public boolean isJobNameSpecified() {
    return localJobNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getJobName() {
    return localJobName;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobName
   */
  public void setJobName(java.lang.String param) {
    localJobNameTracker = param != null;

    this.localJobName = param;
  }

  /** field for JobDescription */
  protected java.lang.String localJobDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localJobDescriptionTracker = false;

  public boolean isJobDescriptionSpecified() {
    return localJobDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getJobDescription() {
    return localJobDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobDescription
   */
  public void setJobDescription(java.lang.String param) {
    localJobDescriptionTracker = param != null;

    this.localJobDescription = param;
  }

  /** field for JobStatus */
  protected java.lang.String localJobStatus;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localJobStatusTracker = false;

  public boolean isJobStatusSpecified() {
    return localJobStatusTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getJobStatus() {
    return localJobStatus;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobStatus
   */
  public void setJobStatus(java.lang.String param) {
    localJobStatusTracker = param != null;

    this.localJobStatus = param;
  }

  /** field for PreviousDate */
  protected java.util.Calendar localPreviousDate;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getPreviousDate() {
    return localPreviousDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param PreviousDate
   */
  public void setPreviousDate(java.util.Calendar param) {

    this.localPreviousDate = param;
  }

  /** field for NextDate */
  protected java.util.Calendar localNextDate;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getNextDate() {
    return localNextDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param NextDate
   */
  public void setNextDate(java.util.Calendar param) {

    this.localNextDate = param;
  }

  /** field for RescheduledDate */
  protected java.util.Calendar localRescheduledDate;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getRescheduledDate() {
    return localRescheduledDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param RescheduledDate
   */
  public void setRescheduledDate(java.util.Calendar param) {

    this.localRescheduledDate = param;
  }

  /** field for RescheduledReason */
  protected java.lang.String localRescheduledReason;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRescheduledReasonTracker = false;

  public boolean isRescheduledReasonSpecified() {
    return localRescheduledReasonTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getRescheduledReason() {
    return localRescheduledReason;
  }

  /**
   * Auto generated setter method
   *
   * @param param RescheduledReason
   */
  public void setRescheduledReason(java.lang.String param) {
    localRescheduledReasonTracker = param != null;

    this.localRescheduledReason = param;
  }

  /** field for UPRN */
  protected java.math.BigDecimal localUPRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUPRN() {
    return localUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRN
   */
  public void setUPRN(java.math.BigDecimal param) {

    this.localUPRN = param;
  }

  /** field for USRN */
  protected java.math.BigDecimal localUSRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUSRN() {
    return localUSRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param USRN
   */
  public void setUSRN(java.math.BigDecimal param) {

    this.localUSRN = param;
  }

  /** field for Address1 */
  protected java.lang.String localAddress1;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAddress1Tracker = false;

  public boolean isAddress1Specified() {
    return localAddress1Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAddress1() {
    return localAddress1;
  }

  /**
   * Auto generated setter method
   *
   * @param param Address1
   */
  public void setAddress1(java.lang.String param) {
    localAddress1Tracker = param != null;

    this.localAddress1 = param;
  }

  /** field for Address2 */
  protected java.lang.String localAddress2;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localAddress2Tracker = false;

  public boolean isAddress2Specified() {
    return localAddress2Tracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getAddress2() {
    return localAddress2;
  }

  /**
   * Auto generated setter method
   *
   * @param param Address2
   */
  public void setAddress2(java.lang.String param) {
    localAddress2Tracker = param != null;

    this.localAddress2 = param;
  }

  /** field for Street */
  protected java.lang.String localStreet;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localStreetTracker = false;

  public boolean isStreetSpecified() {
    return localStreetTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getStreet() {
    return localStreet;
  }

  /**
   * Auto generated setter method
   *
   * @param param Street
   */
  public void setStreet(java.lang.String param) {
    localStreetTracker = param != null;

    this.localStreet = param;
  }

  /** field for Locality */
  protected java.lang.String localLocality;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLocalityTracker = false;

  public boolean isLocalitySpecified() {
    return localLocalityTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getLocality() {
    return localLocality;
  }

  /**
   * Auto generated setter method
   *
   * @param param Locality
   */
  public void setLocality(java.lang.String param) {
    localLocalityTracker = param != null;

    this.localLocality = param;
  }

  /** field for Town */
  protected java.lang.String localTown;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTownTracker = false;

  public boolean isTownSpecified() {
    return localTownTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTown() {
    return localTown;
  }

  /**
   * Auto generated setter method
   *
   * @param param Town
   */
  public void setTown(java.lang.String param) {
    localTownTracker = param != null;

    this.localTown = param;
  }

  /** field for PostCode */
  protected java.lang.String localPostCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localPostCodeTracker = false;

  public boolean isPostCodeSpecified() {
    return localPostCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getPostCode() {
    return localPostCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param PostCode
   */
  public void setPostCode(java.lang.String param) {
    localPostCodeTracker = param != null;

    this.localPostCode = param;
  }

  /** field for Easting */
  protected java.math.BigDecimal localEasting;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getEasting() {
    return localEasting;
  }

  /**
   * Auto generated setter method
   *
   * @param param Easting
   */
  public void setEasting(java.math.BigDecimal param) {

    this.localEasting = param;
  }

  /** field for Northing */
  protected java.math.BigDecimal localNorthing;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getNorthing() {
    return localNorthing;
  }

  /**
   * Auto generated setter method
   *
   * @param param Northing
   */
  public void setNorthing(java.math.BigDecimal param) {

    this.localNorthing = param;
  }

  /** field for Longitude */
  protected java.math.BigDecimal localLongitude;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getLongitude() {
    return localLongitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param Longitude
   */
  public void setLongitude(java.math.BigDecimal param) {

    this.localLongitude = param;
  }

  /** field for Latitude */
  protected java.math.BigDecimal localLatitude;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getLatitude() {
    return localLatitude;
  }

  /**
   * Auto generated setter method
   *
   * @param param Latitude
   */
  public void setLatitude(java.math.BigDecimal param) {

    this.localLatitude = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":Jobs_WorkScheduleDates_type0",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "Jobs_WorkScheduleDates_type0",
            xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "JobID", xmlWriter);

    if (localJobID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("JobID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localJobID));
    }

    xmlWriter.writeEndElement();
    if (localJobNameTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "JobName", xmlWriter);

      if (localJobName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("JobName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localJobName);
      }

      xmlWriter.writeEndElement();
    }
    if (localJobDescriptionTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "JobDescription", xmlWriter);

      if (localJobDescription == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("JobDescription cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localJobDescription);
      }

      xmlWriter.writeEndElement();
    }
    if (localJobStatusTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "JobStatus", xmlWriter);

      if (localJobStatus == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("JobStatus cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localJobStatus);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "PreviousDate", xmlWriter);

    if (localPreviousDate == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("PreviousDate cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPreviousDate));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "NextDate", xmlWriter);

    if (localNextDate == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("NextDate cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextDate));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "RescheduledDate", xmlWriter);

    if (localRescheduledDate == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("RescheduledDate cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRescheduledDate));
    }

    xmlWriter.writeEndElement();
    if (localRescheduledReasonTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "RescheduledReason", xmlWriter);

      if (localRescheduledReason == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("RescheduledReason cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localRescheduledReason);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "UPRN", xmlWriter);

    if (localUPRN == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("UPRN cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "USRN", xmlWriter);

    if (localUSRN == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("USRN cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUSRN));
    }

    xmlWriter.writeEndElement();
    if (localAddress1Tracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "Address1", xmlWriter);

      if (localAddress1 == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Address1 cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAddress1);
      }

      xmlWriter.writeEndElement();
    }
    if (localAddress2Tracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "Address2", xmlWriter);

      if (localAddress2 == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Address2 cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localAddress2);
      }

      xmlWriter.writeEndElement();
    }
    if (localStreetTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "Street", xmlWriter);

      if (localStreet == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Street cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localStreet);
      }

      xmlWriter.writeEndElement();
    }
    if (localLocalityTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "Locality", xmlWriter);

      if (localLocality == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Locality cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localLocality);
      }

      xmlWriter.writeEndElement();
    }
    if (localTownTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "Town", xmlWriter);

      if (localTown == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Town cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTown);
      }

      xmlWriter.writeEndElement();
    }
    if (localPostCodeTracker) {
      namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
      writeStartElement(null, namespace, "PostCode", xmlWriter);

      if (localPostCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("PostCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localPostCode);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "Easting", xmlWriter);

    if (localEasting == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("Easting cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEasting));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "Northing", xmlWriter);

    if (localNorthing == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("Northing cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNorthing));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "Longitude", xmlWriter);

    if (localLongitude == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("Longitude cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLongitude));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd";
    writeStartElement(null, namespace, "Latitude", xmlWriter);

    if (localLatitude == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("Latitude cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLatitude));
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd")) {
      return "ns85";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static Jobs_WorkScheduleDates_type0 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      Jobs_WorkScheduleDates_type0 object = new Jobs_WorkScheduleDates_type0();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"Jobs_WorkScheduleDates_type0".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (Jobs_WorkScheduleDates_type0)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "JobID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "JobName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd",
                    "JobDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobDescription" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobDescription(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "JobStatus")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobStatus" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobStatus(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "PreviousDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PreviousDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPreviousDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "NextDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "NextDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setNextDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd",
                    "RescheduledDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "RescheduledDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setRescheduledDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd",
                    "RescheduledReason")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "RescheduledReason" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setRescheduledReason(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UPRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUPRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "USRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "USRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUSRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Address1")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Address1" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAddress1(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Address2")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Address2" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setAddress2(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Street")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Street" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setStreet(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Locality")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Locality" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLocality(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Town")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Town" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTown(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "PostCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "PostCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setPostCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Easting")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Easting" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setEasting(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Northing")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Northing" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setNorthing(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Longitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Longitude" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLongitude(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd", "Latitude")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Latitude" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLatitude(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
