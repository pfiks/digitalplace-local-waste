/**
 * ServiceRequest_Create.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/** ServiceRequest_Create bean class */
@SuppressWarnings({"unchecked", "unused"})
public class ServiceRequest_Create implements org.apache.axis2.databinding.ADBBean {

  public static final javax.xml.namespace.QName MY_QNAME =
      new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Create", "ns99");

  /** field for Token */
  protected java.lang.String localToken;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTokenTracker = false;

  public boolean isTokenSpecified() {
    return localTokenTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getToken() {
    return localToken;
  }

  /**
   * Auto generated setter method
   *
   * @param param Token
   */
  public void setToken(java.lang.String param) {
    localTokenTracker = param != null;

    this.localToken = param;
  }

  /** field for UPRN */
  protected java.math.BigDecimal localUPRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUPRN() {
    return localUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRN
   */
  public void setUPRN(java.math.BigDecimal param) {

    this.localUPRN = param;
  }

  /** field for ServiceRequest_Location */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set localServiceRequest_Location;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceRequest_LocationTracker = false;

  public boolean isServiceRequest_LocationSpecified() {
    return localServiceRequest_LocationTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set getServiceRequest_Location() {
    return localServiceRequest_Location;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceRequest_Location
   */
  public void setServiceRequest_Location(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set param) {
    localServiceRequest_LocationTracker = param != null;

    this.localServiceRequest_Location = param;
  }

  /** field for ServiceLocationDescription */
  protected java.lang.String localServiceLocationDescription;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceLocationDescriptionTracker = false;

  public boolean isServiceLocationDescriptionSpecified() {
    return localServiceLocationDescriptionTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceLocationDescription() {
    return localServiceLocationDescription;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceLocationDescription
   */
  public void setServiceLocationDescription(java.lang.String param) {
    localServiceLocationDescriptionTracker = param != null;

    this.localServiceLocationDescription = param;
  }

  /** field for DateRequested */
  protected java.util.Calendar localDateRequested;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateRequested() {
    return localDateRequested;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateRequested
   */
  public void setDateRequested(java.util.Calendar param) {

    this.localDateRequested = param;
  }

  /** field for ServiceTypeID */
  protected int localServiceTypeID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceTypeID() {
    return localServiceTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceTypeID
   */
  public void setServiceTypeID(int param) {

    this.localServiceTypeID = param;
  }

  /** field for ServiceStatusID */
  protected int localServiceStatusID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceStatusID() {
    return localServiceStatusID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceStatusID
   */
  public void setServiceStatusID(int param) {

    this.localServiceStatusID = param;
  }

  /** field for ReporterContact */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd
          .ServiceRequest_CreateServiceRequest_CreateReporterContact
      localReporterContact;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localReporterContactTracker = false;

  public boolean isReporterContactSpecified() {
    return localReporterContactTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd
          .ServiceRequest_CreateServiceRequest_CreateReporterContact
      getReporterContact() {
    return localReporterContact;
  }

  /**
   * Auto generated setter method
   *
   * @param param ReporterContact
   */
  public void setReporterContact(
      com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd
              .ServiceRequest_CreateServiceRequest_CreateReporterContact
          param) {
    localReporterContactTracker = param != null;

    this.localReporterContact = param;
  }

  /** field for ReporterBusiness */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd
          .ServiceRequest_CreateServiceRequest_CreateReporterBusiness
      localReporterBusiness;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localReporterBusinessTracker = false;

  public boolean isReporterBusinessSpecified() {
    return localReporterBusinessTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterBusiness
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd
          .ServiceRequest_CreateServiceRequest_CreateReporterBusiness
      getReporterBusiness() {
    return localReporterBusiness;
  }

  /**
   * Auto generated setter method
   *
   * @param param ReporterBusiness
   */
  public void setReporterBusiness(
      com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd
              .ServiceRequest_CreateServiceRequest_CreateReporterBusiness
          param) {
    localReporterBusinessTracker = param != null;

    this.localReporterBusiness = param;
  }

  /** field for Source */
  protected java.lang.String localSource;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localSourceTracker = false;

  public boolean isSourceSpecified() {
    return localSourceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getSource() {
    return localSource;
  }

  /**
   * Auto generated setter method
   *
   * @param param Source
   */
  public void setSource(java.lang.String param) {
    localSourceTracker = param != null;

    this.localSource = param;
  }

  /** field for ExternalReference */
  protected java.lang.String localExternalReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExternalReferenceTracker = false;

  public boolean isExternalReferenceSpecified() {
    return localExternalReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getExternalReference() {
    return localExternalReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExternalReference
   */
  public void setExternalReference(java.lang.String param) {
    localExternalReferenceTracker = param != null;

    this.localExternalReference = param;
  }

  /** field for LandTypeID */
  protected int localLandTypeID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getLandTypeID() {
    return localLandTypeID;
  }

  /**
   * Auto generated setter method
   *
   * @param param LandTypeID
   */
  public void setLandTypeID(int param) {

    this.localLandTypeID = param;
  }

  /** field for SLAID */
  protected int localSLAID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSLAID() {
    return localSLAID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SLAID
   */
  public void setSLAID(int param) {

    this.localSLAID = param;
  }

  /** field for CrewID */
  protected int localCrewID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getCrewID() {
    return localCrewID;
  }

  /**
   * Auto generated setter method
   *
   * @param param CrewID
   */
  public void setCrewID(int param) {

    this.localCrewID = param;
  }

  /** field for ExtendedData */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields
      localExtendedData;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExtendedDataTracker = false;

  public boolean isExtendedDataSpecified() {
    return localExtendedDataTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields
      getExtendedData() {
    return localExtendedData;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExtendedData
   */
  public void setExtendedData(
      com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields param) {
    localExtendedDataTracker = param != null;

    this.localExtendedData = param;
  }

  /** field for RelatedServiceRequests */
  protected com.placecube.digitalplace.local.waste.bartec.axis
          .ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest
      localRelatedServiceRequests;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRelatedServiceRequestsTracker = false;

  public boolean isRelatedServiceRequestsSpecified() {
    return localRelatedServiceRequestsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return
   *     com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest
      getRelatedServiceRequests() {
    return localRelatedServiceRequests;
  }

  /**
   * Auto generated setter method
   *
   * @param param RelatedServiceRequests
   */
  public void setRelatedServiceRequests(
      com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest
          param) {
    localRelatedServiceRequestsTracker = param != null;

    this.localRelatedServiceRequests = param;
  }

  /** field for RelatedPremises */
  protected com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation
      localRelatedPremises;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRelatedPremisesTracker = false;

  public boolean isRelatedPremisesSpecified() {
    return localRelatedPremisesTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation
      getRelatedPremises() {
    return localRelatedPremises;
  }

  /**
   * Auto generated setter method
   *
   * @param param RelatedPremises
   */
  public void setRelatedPremises(
      com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation param) {
    localRelatedPremisesTracker = param != null;

    this.localRelatedPremises = param;
  }

  /** field for AppointmentReservationID */
  protected int localAppointmentReservationID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getAppointmentReservationID() {
    return localAppointmentReservationID;
  }

  /**
   * Auto generated setter method
   *
   * @param param AppointmentReservationID
   */
  public void setAppointmentReservationID(int param) {

    this.localAppointmentReservationID = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://bartec-systems.com/");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ServiceRequest_Create",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ServiceRequest_Create",
            xmlWriter);
      }
    }
    if (localTokenTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "token", xmlWriter);

      if (localToken == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("token cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localToken);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "UPRN", xmlWriter);

    if (localUPRN == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
    }

    xmlWriter.writeEndElement();
    if (localServiceRequest_LocationTracker) {
      if (localServiceRequest_Location == null) {
        throw new org.apache.axis2.databinding.ADBException(
            "ServiceRequest_Location cannot be null!!");
      }
      localServiceRequest_Location.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceRequest_Location"),
          xmlWriter);
    }
    if (localServiceLocationDescriptionTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "serviceLocationDescription", xmlWriter);

      if (localServiceLocationDescription == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException(
            "serviceLocationDescription cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceLocationDescription);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "DateRequested", xmlWriter);

    if (localDateRequested == null) {
      // write the nil attribute

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateRequested));
    }

    xmlWriter.writeEndElement();

    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "ServiceTypeID", xmlWriter);

    if (localServiceTypeID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ServiceTypeID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceTypeID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "ServiceStatusID", xmlWriter);

    if (localServiceStatusID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ServiceStatusID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceStatusID));
    }

    xmlWriter.writeEndElement();
    if (localReporterContactTracker) {
      if (localReporterContact == null) {
        throw new org.apache.axis2.databinding.ADBException("reporterContact cannot be null!!");
      }
      localReporterContact.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "reporterContact"),
          xmlWriter);
    }
    if (localReporterBusinessTracker) {
      if (localReporterBusiness == null) {
        throw new org.apache.axis2.databinding.ADBException("reporterBusiness cannot be null!!");
      }
      localReporterBusiness.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "reporterBusiness"),
          xmlWriter);
    }
    if (localSourceTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "source", xmlWriter);

      if (localSource == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localSource);
      }

      xmlWriter.writeEndElement();
    }
    if (localExternalReferenceTracker) {
      namespace = "http://bartec-systems.com/";
      writeStartElement(null, namespace, "ExternalReference", xmlWriter);

      if (localExternalReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ExternalReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localExternalReference);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "LandTypeID", xmlWriter);

    if (localLandTypeID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("LandTypeID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLandTypeID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "SLAID", xmlWriter);

    if (localSLAID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("SLAID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSLAID));
    }

    xmlWriter.writeEndElement();

    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "CrewID", xmlWriter);

    if (localCrewID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("CrewID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCrewID));
    }

    xmlWriter.writeEndElement();
    if (localExtendedDataTracker) {
      if (localExtendedData == null) {
        throw new org.apache.axis2.databinding.ADBException("extendedData cannot be null!!");
      }
      localExtendedData.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "extendedData"), xmlWriter);
    }
    if (localRelatedServiceRequestsTracker) {
      if (localRelatedServiceRequests == null) {
        throw new org.apache.axis2.databinding.ADBException(
            "relatedServiceRequests cannot be null!!");
      }
      localRelatedServiceRequests.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "relatedServiceRequests"),
          xmlWriter);
    }
    if (localRelatedPremisesTracker) {
      if (localRelatedPremises == null) {
        throw new org.apache.axis2.databinding.ADBException("relatedPremises cannot be null!!");
      }
      localRelatedPremises.serialize(
          new javax.xml.namespace.QName("http://bartec-systems.com/", "relatedPremises"),
          xmlWriter);
    }
    namespace = "http://bartec-systems.com/";
    writeStartElement(null, namespace, "appointmentReservationID", xmlWriter);

    if (localAppointmentReservationID == java.lang.Integer.MIN_VALUE) {

      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(
              localAppointmentReservationID));
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://bartec-systems.com/")) {
      return "ns99";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static ServiceRequest_Create parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      ServiceRequest_Create object = new ServiceRequest_Create();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ServiceRequest_Create".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (ServiceRequest_Create)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "token")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "token" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setToken(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setUPRN(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://bartec-systems.com/", "ServiceRequest_Location")
                .equals(reader.getName())) {

          object.setServiceRequest_Location(
              com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://bartec-systems.com/", "serviceLocationDescription")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "serviceLocationDescription" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceLocationDescription(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "DateRequested")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setDateRequested(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          } else {

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ServiceStatusID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceStatusID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceStatusID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "reporterContact")
                .equals(reader.getName())) {

          object.setReporterContact(
              com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd
                  .ServiceRequest_CreateServiceRequest_CreateReporterContact.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "reporterBusiness")
                .equals(reader.getName())) {

          object.setReporterBusiness(
              com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd
                  .ServiceRequest_CreateServiceRequest_CreateReporterBusiness.Factory.parse(
                  reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "source")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "source" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSource(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "ExternalReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExternalReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExternalReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "LandTypeID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "LandTypeID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLandTypeID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "SLAID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SLAID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSLAID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "CrewID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "CrewID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCrewID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "extendedData")
                .equals(reader.getName())) {

          object.setExtendedData(
              com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields.Factory
                  .parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "relatedServiceRequests")
                .equals(reader.getName())) {

          object.setRelatedServiceRequests(
              com.placecube.digitalplace.local.waste.bartec.axis
                  .ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest.Factory
                  .parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://bartec-systems.com/", "relatedPremises")
                .equals(reader.getName())) {

          object.setRelatedPremises(
              com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation.Factory
                  .parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://bartec-systems.com/", "appointmentReservationID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {

            java.lang.String content = reader.getElementText();

            object.setAppointmentReservationID(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          } else {

            object.setAppointmentReservationID(java.lang.Integer.MIN_VALUE);

            reader.getElementText(); // throw away text nodes if any.
          }

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
