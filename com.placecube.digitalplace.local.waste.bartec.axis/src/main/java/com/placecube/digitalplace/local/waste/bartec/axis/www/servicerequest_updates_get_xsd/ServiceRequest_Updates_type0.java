/**
 * ServiceRequest_Updates_type0.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_updates_get_xsd;

/** ServiceRequest_Updates_type0 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class ServiceRequest_Updates_type0 implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ServiceRequest_Updates_type0
  Namespace URI = http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd
  Namespace Prefix = ns35
  */

  /** field for ServiceRequestID */
  protected int localServiceRequestID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getServiceRequestID() {
    return localServiceRequestID;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceRequestID
   */
  public void setServiceRequestID(int param) {

    this.localServiceRequestID = param;
  }

  /** field for ServiceCode */
  protected java.lang.String localServiceCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceCodeTracker = false;

  public boolean isServiceCodeSpecified() {
    return localServiceCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceCode() {
    return localServiceCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceCode
   */
  public void setServiceCode(java.lang.String param) {
    localServiceCodeTracker = param != null;

    this.localServiceCode = param;
  }

  /** field for JobReference */
  protected java.lang.String localJobReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localJobReferenceTracker = false;

  public boolean isJobReferenceSpecified() {
    return localJobReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getJobReference() {
    return localJobReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param JobReference
   */
  public void setJobReference(java.lang.String param) {
    localJobReferenceTracker = param != null;

    this.localJobReference = param;
  }

  /** field for ServiceCategory */
  protected java.lang.String localServiceCategory;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceCategoryTracker = false;

  public boolean isServiceCategorySpecified() {
    return localServiceCategoryTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceCategory() {
    return localServiceCategory;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceCategory
   */
  public void setServiceCategory(java.lang.String param) {
    localServiceCategoryTracker = param != null;

    this.localServiceCategory = param;
  }

  /** field for ServiceClass */
  protected java.lang.String localServiceClass;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceClassTracker = false;

  public boolean isServiceClassSpecified() {
    return localServiceClassTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceClass() {
    return localServiceClass;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceClass
   */
  public void setServiceClass(java.lang.String param) {
    localServiceClassTracker = param != null;

    this.localServiceClass = param;
  }

  /** field for ServiceType */
  protected java.lang.String localServiceType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceTypeTracker = false;

  public boolean isServiceTypeSpecified() {
    return localServiceTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceType() {
    return localServiceType;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceType
   */
  public void setServiceType(java.lang.String param) {
    localServiceTypeTracker = param != null;

    this.localServiceType = param;
  }

  /** field for DateAdded */
  protected java.util.Calendar localDateAdded;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateAdded() {
    return localDateAdded;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateAdded
   */
  public void setDateAdded(java.util.Calendar param) {

    this.localDateAdded = param;
  }

  /** field for DateChanged */
  protected java.util.Calendar localDateChanged;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateChanged() {
    return localDateChanged;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateChanged
   */
  public void setDateChanged(java.util.Calendar param) {

    this.localDateChanged = param;
  }

  /** field for RecordCount This was an Attribute! */
  protected int localRecordCount;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getRecordCount() {
    return localRecordCount;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordCount
   */
  public void setRecordCount(int param) {

    this.localRecordCount = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(xmlWriter, "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ServiceRequest_Updates_type0",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ServiceRequest_Updates_type0",
            xmlWriter);
      }
    }

    if (localRecordCount != java.lang.Integer.MIN_VALUE) {

      writeAttribute(
          "",
          "RecordCount",
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordCount),
          xmlWriter);

    } else {
      throw new org.apache.axis2.databinding.ADBException(
          "required attribute localRecordCount is null");
    }

    namespace = "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd";
    writeStartElement(null, namespace, "ServiceRequestID", xmlWriter);

    if (localServiceRequestID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("ServiceRequestID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localServiceRequestID));
    }

    xmlWriter.writeEndElement();
    if (localServiceCodeTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd";
      writeStartElement(null, namespace, "ServiceCode", xmlWriter);

      if (localServiceCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localJobReferenceTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd";
      writeStartElement(null, namespace, "JobReference", xmlWriter);

      if (localJobReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("JobReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localJobReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceCategoryTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd";
      writeStartElement(null, namespace, "ServiceCategory", xmlWriter);

      if (localServiceCategory == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceCategory cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceCategory);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceClassTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd";
      writeStartElement(null, namespace, "ServiceClass", xmlWriter);

      if (localServiceClass == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceClass cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceClass);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceTypeTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd";
      writeStartElement(null, namespace, "ServiceType", xmlWriter);

      if (localServiceType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceType);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd";
    writeStartElement(null, namespace, "DateAdded", xmlWriter);

    if (localDateAdded == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateAdded cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateAdded));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd";
    writeStartElement(null, namespace, "DateChanged", xmlWriter);

    if (localDateChanged == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateChanged cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateChanged));
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd")) {
      return "ns35";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static ServiceRequest_Updates_type0 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      ServiceRequest_Updates_type0 object = new ServiceRequest_Updates_type0();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ServiceRequest_Updates_type0".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (ServiceRequest_Updates_type0)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        // handle attribute "RecordCount"
        java.lang.String tempAttribRecordCount = reader.getAttributeValue(null, "RecordCount");

        if (tempAttribRecordCount != null) {
          java.lang.String content = tempAttribRecordCount;

          object.setRecordCount(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(tempAttribRecordCount));

        } else {

          throw new org.apache.axis2.databinding.ADBException(
              "Required attribute RecordCount is missing");
        }
        handledAttributes.add("RecordCount");

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd",
                    "ServiceRequestID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceRequestID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceRequestID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd", "ServiceCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd", "JobReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "JobReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setJobReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd",
                    "ServiceCategory")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceCategory" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceCategory(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd", "ServiceClass")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceClass" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceClass(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd", "ServiceType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd", "DateAdded")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateAdded" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateAdded(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd", "DateChanged")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateChanged" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateChanged(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
