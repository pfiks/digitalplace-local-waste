/**
 * CtLicence.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www;

/** CtLicence bean class */
@SuppressWarnings({"unchecked", "unused"})
public class CtLicence implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ctLicence
  Namespace URI = http://www.bartec-systems.com
  Namespace Prefix = ns1
  */

  /** field for Id */
  protected int localId;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getId() {
    return localId;
  }

  /**
   * Auto generated setter method
   *
   * @param param Id
   */
  public void setId(int param) {

    this.localId = param;
  }

  /** field for DateRequested */
  protected java.util.Calendar localDateRequested;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateRequested() {
    return localDateRequested;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateRequested
   */
  public void setDateRequested(java.util.Calendar param) {

    this.localDateRequested = param;
  }

  /** field for LicenceType */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceType localLicenceType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLicenceTypeTracker = false;

  public boolean isLicenceTypeSpecified() {
    return localLicenceTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceType
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceType getLicenceType() {
    return localLicenceType;
  }

  /**
   * Auto generated setter method
   *
   * @param param LicenceType
   */
  public void setLicenceType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceType param) {
    localLicenceTypeTracker = param != null;

    this.localLicenceType = param;
  }

  /** field for LicenceStatus */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceStatus localLicenceStatus;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLicenceStatusTracker = false;

  public boolean isLicenceStatusSpecified() {
    return localLicenceStatusTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceStatus
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceStatus getLicenceStatus() {
    return localLicenceStatus;
  }

  /**
   * Auto generated setter method
   *
   * @param param LicenceStatus
   */
  public void setLicenceStatus(com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceStatus param) {
    localLicenceStatusTracker = param != null;

    this.localLicenceStatus = param;
  }

  /** field for StartDate */
  protected java.util.Calendar localStartDate;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getStartDate() {
    return localStartDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param StartDate
   */
  public void setStartDate(java.util.Calendar param) {

    this.localStartDate = param;
  }

  /** field for IssueDate */
  protected java.util.Calendar localIssueDate;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getIssueDate() {
    return localIssueDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param IssueDate
   */
  public void setIssueDate(java.util.Calendar param) {

    this.localIssueDate = param;
  }

  /** field for ExpiryDate */
  protected java.util.Calendar localExpiryDate;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getExpiryDate() {
    return localExpiryDate;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExpiryDate
   */
  public void setExpiryDate(java.util.Calendar param) {

    this.localExpiryDate = param;
  }

  /** field for Term */
  protected java.lang.String localTerm;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localTermTracker = false;

  public boolean isTermSpecified() {
    return localTermTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getTerm() {
    return localTerm;
  }

  /**
   * Auto generated setter method
   *
   * @param param Term
   */
  public void setTerm(java.lang.String param) {
    localTermTracker = param != null;

    this.localTerm = param;
  }

  /** field for BespokeConditions */
  protected java.lang.String localBespokeConditions;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localBespokeConditionsTracker = false;

  public boolean isBespokeConditionsSpecified() {
    return localBespokeConditionsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getBespokeConditions() {
    return localBespokeConditions;
  }

  /**
   * Auto generated setter method
   *
   * @param param BespokeConditions
   */
  public void setBespokeConditions(java.lang.String param) {
    localBespokeConditionsTracker = param != null;

    this.localBespokeConditions = param;
  }

  /** field for CustomerID */
  protected java.lang.String localCustomerID;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localCustomerIDTracker = false;

  public boolean isCustomerIDSpecified() {
    return localCustomerIDTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getCustomerID() {
    return localCustomerID;
  }

  /**
   * Auto generated setter method
   *
   * @param param CustomerID
   */
  public void setCustomerID(java.lang.String param) {
    localCustomerIDTracker = param != null;

    this.localCustomerID = param;
  }

  /** field for Fee */
  protected java.math.BigDecimal localFee;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localFeeTracker = false;

  public boolean isFeeSpecified() {
    return localFeeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getFee() {
    return localFee;
  }

  /**
   * Auto generated setter method
   *
   * @param param Fee
   */
  public void setFee(java.math.BigDecimal param) {
    localFeeTracker = param != null;

    this.localFee = param;
  }

  /** field for Deposit */
  protected java.math.BigDecimal localDeposit;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localDepositTracker = false;

  public boolean isDepositSpecified() {
    return localDepositTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getDeposit() {
    return localDeposit;
  }

  /**
   * Auto generated setter method
   *
   * @param param Deposit
   */
  public void setDeposit(java.math.BigDecimal param) {
    localDepositTracker = param != null;

    this.localDeposit = param;
  }

  /** field for InvoicingBand */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtInvoicingBand localInvoicingBand;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localInvoicingBandTracker = false;

  public boolean isInvoicingBandSpecified() {
    return localInvoicingBandTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtInvoicingBand
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtInvoicingBand getInvoicingBand() {
    return localInvoicingBand;
  }

  /**
   * Auto generated setter method
   *
   * @param param InvoicingBand
   */
  public void setInvoicingBand(com.placecube.digitalplace.local.waste.bartec.axis.www.CtInvoicingBand param) {
    localInvoicingBandTracker = param != null;

    this.localInvoicingBand = param;
  }

  /** field for BillingAccount */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness localBillingAccount;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localBillingAccountTracker = false;

  public boolean isBillingAccountSpecified() {
    return localBillingAccountTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness getBillingAccount() {
    return localBillingAccount;
  }

  /**
   * Auto generated setter method
   *
   * @param param BillingAccount
   */
  public void setBillingAccount(com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness param) {
    localBillingAccountTracker = param != null;

    this.localBillingAccount = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.bartec-systems.com");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ctLicence",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "ctLicence", xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "id", xmlWriter);

    if (localId == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "DateRequested", xmlWriter);

    if (localDateRequested == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateRequested cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateRequested));
    }

    xmlWriter.writeEndElement();
    if (localLicenceTypeTracker) {
      if (localLicenceType == null) {
        throw new org.apache.axis2.databinding.ADBException("LicenceType cannot be null!!");
      }
      localLicenceType.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceType"), xmlWriter);
    }
    if (localLicenceStatusTracker) {
      if (localLicenceStatus == null) {
        throw new org.apache.axis2.databinding.ADBException("LicenceStatus cannot be null!!");
      }
      localLicenceStatus.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceStatus"),
          xmlWriter);
    }
    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "StartDate", xmlWriter);

    if (localStartDate == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("StartDate cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "IssueDate", xmlWriter);

    if (localIssueDate == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("IssueDate cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIssueDate));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com";
    writeStartElement(null, namespace, "ExpiryDate", xmlWriter);

    if (localExpiryDate == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("ExpiryDate cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpiryDate));
    }

    xmlWriter.writeEndElement();
    if (localTermTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Term", xmlWriter);

      if (localTerm == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Term cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localTerm);
      }

      xmlWriter.writeEndElement();
    }
    if (localBespokeConditionsTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "BespokeConditions", xmlWriter);

      if (localBespokeConditions == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("BespokeConditions cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localBespokeConditions);
      }

      xmlWriter.writeEndElement();
    }
    if (localCustomerIDTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "CustomerID", xmlWriter);

      if (localCustomerID == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("CustomerID cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localCustomerID);
      }

      xmlWriter.writeEndElement();
    }
    if (localFeeTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Fee", xmlWriter);

      if (localFee == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Fee cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFee));
      }

      xmlWriter.writeEndElement();
    }
    if (localDepositTracker) {
      namespace = "http://www.bartec-systems.com";
      writeStartElement(null, namespace, "Deposit", xmlWriter);

      if (localDeposit == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("Deposit cannot be null!!");

      } else {

        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeposit));
      }

      xmlWriter.writeEndElement();
    }
    if (localInvoicingBandTracker) {
      if (localInvoicingBand == null) {
        throw new org.apache.axis2.databinding.ADBException("InvoicingBand cannot be null!!");
      }
      localInvoicingBand.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "InvoicingBand"),
          xmlWriter);
    }
    if (localBillingAccountTracker) {
      if (localBillingAccount == null) {
        throw new org.apache.axis2.databinding.ADBException("BillingAccount cannot be null!!");
      }
      localBillingAccount.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "BillingAccount"),
          xmlWriter);
    }
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp"), xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com")) {
      return "ns1";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static CtLicence parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      CtLicence object = new CtLicence();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ctLicence".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (CtLicence)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "id")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "id" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "DateRequested")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateRequested" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateRequested(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceType")
                .equals(reader.getName())) {

          object.setLicenceType(com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceType.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "LicenceStatus")
                .equals(reader.getName())) {

          object.setLicenceStatus(com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceStatus.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "StartDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "StartDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setStartDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "IssueDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "IssueDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setIssueDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "ExpiryDate")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExpiryDate" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExpiryDate(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Term")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Term" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setTerm(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "BespokeConditions")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "BespokeConditions" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setBespokeConditions(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "CustomerID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "CustomerID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCustomerID(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Fee")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Fee" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setFee(org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "Deposit")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "Deposit" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDeposit(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "InvoicingBand")
                .equals(reader.getName())) {

          object.setInvoicingBand(com.placecube.digitalplace.local.waste.bartec.axis.www.CtInvoicingBand.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "BillingAccount")
                .equals(reader.getName())) {

          object.setBillingAccount(com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName("http://www.bartec-systems.com", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
