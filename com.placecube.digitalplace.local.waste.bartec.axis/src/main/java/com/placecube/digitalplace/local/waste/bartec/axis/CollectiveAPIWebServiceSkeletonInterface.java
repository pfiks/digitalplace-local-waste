/**
 * CollectiveAPIWebServiceSkeletonInterface.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.0 Built on : Aug 01,
 * 2021 (07:27:19 HST)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;
/** CollectiveAPIWebServiceSkeletonInterface java skeleton interface for the axisService */
public interface CollectiveAPIWebServiceSkeletonInterface {

  /**
   * Auto generated method signature
   *
   * @param businesses_Documents_Get
   */
  public Businesses_Documents_GetResponse businesses_Documents_Get(
      Businesses_Documents_Get businesses_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Classes_Get
   */
  public Features_Classes_GetResponse features_Classes_Get(
      Features_Classes_Get features_Classes_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Events_Get
   */
  public Premises_Events_GetResponse premises_Events_Get(
      Premises_Events_Get premises_Events_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_FutureWorkpacks_Get
   */
  public Premises_FutureWorkpacks_GetResponse premises_FutureWorkpacks_Get(
      Premises_FutureWorkpacks_Get premises_FutureWorkpacks_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Get
   */
  public Premises_GetResponse premises_Get(
      Premises_Get premises_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_SLAs_Get
   */
  public ServiceRequests_SLAs_GetResponse serviceRequests_SLAs_Get(
      ServiceRequests_SLAs_Get serviceRequests_SLAs_Get);

  /**
   * Auto generated method signature
   *
   * @param vehicles_Get
   */
  public Vehicles_GetResponse vehicles_Get(
      Vehicles_Get vehicles_Get);

  /**
   * Auto generated method signature
   *
   * @param streets_Attributes_Get
   */
  public Streets_Attributes_GetResponse streets_Attributes_Get(
      Streets_Attributes_Get streets_Attributes_Get);

  /**
   * Auto generated method signature
   *
   * @param system_ExtendedDataDefinitions_Get
   */
  public System_ExtendedDataDefinitions_GetResponse
      system_ExtendedDataDefinitions_Get(
          System_ExtendedDataDefinitions_Get system_ExtendedDataDefinitions_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Conditions_Get
   */
  public Features_Conditions_GetResponse features_Conditions_Get(
      Features_Conditions_Get features_Conditions_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Create
   */
  public ServiceRequest_CreateResponse serviceRequest_Create(
      ServiceRequest_Create serviceRequest_Create);

  /**
   * Auto generated method signature
   *
   * @param crews_Get
   */
  public Crews_GetResponse crews_Get(Crews_Get crews_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Document_Create
   */
  public ServiceRequest_Document_CreateResponse serviceRequest_Document_Create(
      ServiceRequest_Document_Create serviceRequest_Document_Create);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Statuses_Get
   */
  public ServiceRequests_Statuses_GetResponse serviceRequests_Statuses_Get(
      ServiceRequests_Statuses_Get serviceRequests_Statuses_Get);

  /**
   * Auto generated method signature
   *
   * @param workpack_Note_Create
   */
  public Workpack_Note_CreateResponse workpack_Note_Create(
      Workpack_Note_Create workpack_Note_Create);

  /**
   * Auto generated method signature
   *
   * @param inspections_Get
   */
  public Inspections_GetResponse inspections_Get(
      Inspections_Get inspections_Get);

  /**
   * Auto generated method signature
   *
   * @param workPacks_Get
   */
  public WorkPacks_GetResponse workPacks_Get(
      WorkPacks_Get workPacks_Get);

  /**
   * Auto generated method signature
   *
   * @param streets_Get
   */
  public Streets_GetResponse streets_Get(
      Streets_Get streets_Get);

  /**
   * Auto generated method signature
   *
   * @param site_Document_Create
   */
  public Site_Document_CreateResponse site_Document_Create(
      Site_Document_Create site_Document_Create);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Notes_Get
   */
  public ServiceRequests_Notes_GetResponse serviceRequests_Notes_Get(
      ServiceRequests_Notes_Get serviceRequests_Notes_Get);

  /**
   * Auto generated method signature
   *
   * @param cases_Documents_Get
   */
  public Cases_Documents_GetResponse cases_Documents_Get(
      Cases_Documents_Get cases_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Notes_Get
   */
  public Features_Notes_GetResponse features_Notes_Get(
      Features_Notes_Get features_Notes_Get);

  /**
   * Auto generated method signature
   *
   * @param businesses_SubStatuses_Get
   */
  public Businesses_SubStatuses_GetResponse businesses_SubStatuses_Get(
      Businesses_SubStatuses_Get businesses_SubStatuses_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Documents_Get
   */
  public Premises_Documents_GetResponse premises_Documents_Get(
      Premises_Documents_Get premises_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param streets_Detail_Get
   */
  public Streets_Detail_GetResponse streets_Detail_Get(
      Streets_Detail_Get streets_Detail_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Updates_Get
   */
  public ServiceRequests_Updates_GetResponse serviceRequests_Updates_Get(
      ServiceRequests_Updates_Get serviceRequests_Updates_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Types_Get
   */
  public Features_Types_GetResponse features_Types_Get(
      Features_Types_Get features_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param jobs_Get
   */
  public Jobs_GetResponse jobs_Get(Jobs_Get jobs_Get);

  /**
   * Auto generated method signature
   *
   * @param jobs_Close
   */
  public Jobs_CloseResponse jobs_Close(Jobs_Close jobs_Close);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Types_Get
   */
  public ServiceRequests_Types_GetResponse serviceRequests_Types_Get(
      ServiceRequests_Types_Get serviceRequests_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Categories_Get
   */
  public Features_Categories_GetResponse features_Categories_Get(
      Features_Categories_Get features_Categories_Get);

  /**
   * Auto generated method signature
   *
   * @param licences_Get
   */
  public Licences_GetResponse licences_Get(
      Licences_Get licences_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Detail_Get
   */
  public Premises_Detail_GetResponse premises_Detail_Get(
      Premises_Detail_Get premises_Detail_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Manufacturers_Get
   */
  public Features_Manufacturers_GetResponse features_Manufacturers_Get(
      Features_Manufacturers_Get features_Manufacturers_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Events_Classes_Get
   */
  public Premises_Events_Classes_GetResponse premises_Events_Classes_Get(
      Premises_Events_Classes_Get premises_Events_Classes_Get);

  /**
   * Auto generated method signature
   *
   * @param sites_Documents_Get
   */
  public Sites_Documents_GetResponse sites_Documents_Get(
      Sites_Documents_Get sites_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Appointment_Reservation_Extend
   */
  public ServiceRequest_Appointment_Reservation_ExtendResponse
      serviceRequest_Appointment_Reservation_Extend(
          ServiceRequest_Appointment_Reservation_Extend
              serviceRequest_Appointment_Reservation_Extend);

  /**
   * Auto generated method signature
   *
   * @param premises_Events_Documents_Get
   */
  public Premises_Events_Documents_GetResponse premises_Events_Documents_Get(
      Premises_Events_Documents_Get premises_Events_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param workpacks_Notes_Types_Get
   */
  public Workpacks_Notes_Types_GetResponse workpacks_Notes_Types_Get(
      Workpacks_Notes_Types_Get workpacks_Notes_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param jobs_FeatureScheduleDates_Get
   */
  public Jobs_FeatureScheduleDates_GetResponse jobs_FeatureScheduleDates_Get(
      Jobs_FeatureScheduleDates_Get jobs_FeatureScheduleDates_Get);

  /**
   * Auto generated method signature
   *
   * @param vehicle_Message_Create
   */
  public Vehicle_Message_CreateResponse vehicle_Message_Create(
      Vehicle_Message_Create vehicle_Message_Create);

  /**
   * Auto generated method signature
   *
   * @param workGroups_Get
   */
  public WorkGroups_GetResponse workGroups_Get(
      WorkGroups_Get workGroups_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Appointments_Availability_Get
   */
  public ServiceRequests_Appointments_Availability_GetResponse
      serviceRequests_Appointments_Availability_Get(
          ServiceRequests_Appointments_Availability_Get
              serviceRequests_Appointments_Availability_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Schedules_Get
   */
  public Features_Schedules_GetResponse features_Schedules_Get(
      Features_Schedules_Get features_Schedules_Get);

  /**
   * Auto generated method signature
   *
   * @param businesses_Statuses_Get
   */
  public Businesses_Statuses_GetResponse businesses_Statuses_Get(
      Businesses_Statuses_Get businesses_Statuses_Get);

  /**
   * Auto generated method signature
   *
   * @param resources_Types_Get
   */
  public Resources_Types_GetResponse resources_Types_Get(
      Resources_Types_Get resources_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param inspections_Classes_Get
   */
  public Inspections_Classes_GetResponse inspections_Classes_Get(
      Inspections_Classes_Get inspections_Classes_Get);

  /**
   * Auto generated method signature
   *
   * @param licences_Classes_Get
   */
  public Licences_Classes_GetResponse licences_Classes_Get(
      Licences_Classes_Get licences_Classes_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Appointment_Reservation_Create
   */
  public ServiceRequest_Appointment_Reservation_CreateResponse
      serviceRequest_Appointment_Reservation_Create(
          ServiceRequest_Appointment_Reservation_Create
              serviceRequest_Appointment_Reservation_Create);

  /**
   * Auto generated method signature
   *
   * @param premises_Events_Types_Get
   */
  public Premises_Events_Types_GetResponse premises_Events_Types_Get(
      Premises_Events_Types_Get premises_Events_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Events_Document_Create
   */
  public Premises_Events_Document_CreateResponse premises_Events_Document_Create(
      Premises_Events_Document_Create premises_Events_Document_Create);

  /**
   * Auto generated method signature
   *
   * @param businesses_Classes_Get
   */
  public Businesses_Classes_GetResponse businesses_Classes_Get(
      Businesses_Classes_Get businesses_Classes_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Attribute_Update
   */
  public Premises_Attribute_UpdateResponse premises_Attribute_Update(
      Premises_Attribute_Update premises_Attribute_Update);

  /**
   * Auto generated method signature
   *
   * @param vehicle_InspectionForms_Get
   */
  public Vehicle_InspectionForms_GetResponse vehicle_InspectionForms_Get(
      Vehicle_InspectionForms_Get vehicle_InspectionForms_Get);

  /**
   * Auto generated method signature
   *
   * @param jobs_WorkScheduleDates_Get
   */
  public Jobs_WorkScheduleDates_GetResponse jobs_WorkScheduleDates_Get(
      Jobs_WorkScheduleDates_Get jobs_WorkScheduleDates_Get);

  /**
   * Auto generated method signature
   *
   * @param vehicle_InspectionResults_Get
   */
  public Vehicle_InspectionResults_GetResponse vehicle_InspectionResults_Get(
      Vehicle_InspectionResults_Get vehicle_InspectionResults_Get);

  /**
   * Auto generated method signature
   *
   * @param inspections_Types_Get
   */
  public Inspections_Types_GetResponse inspections_Types_Get(
      Inspections_Types_Get inspections_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param workpacks_Notes_Get
   */
  public Workpacks_Notes_GetResponse workpacks_Notes_Get(
      Workpacks_Notes_Get workpacks_Notes_Get);

  /**
   * Auto generated method signature
   *
   * @param workpacks_Metrics_Get
   */
  public Workpacks_Metrics_GetResponse workpacks_Metrics_Get(
      Workpacks_Metrics_Get workpacks_Metrics_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_History_Get
   */
  public ServiceRequests_History_GetResponse serviceRequests_History_Get(
      ServiceRequests_History_Get serviceRequests_History_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Note_Create
   */
  public ServiceRequest_Note_CreateResponse serviceRequest_Note_Create(
      ServiceRequest_Note_Create serviceRequest_Note_Create);

  /**
   * Auto generated method signature
   *
   * @param business_Document_Create
   */
  public Business_Document_CreateResponse business_Document_Create(
      Business_Document_Create business_Document_Create);

  /**
   * Auto generated method signature
   *
   * @param jobs_Detail_Get
   */
  public Jobs_Detail_GetResponse jobs_Detail_Get(
      Jobs_Detail_Get jobs_Detail_Get);

  /**
   * Auto generated method signature
   *
   * @param system_WasteTypes_Get
   */
  public System_WasteTypes_GetResponse system_WasteTypes_Get(
      System_WasteTypes_Get system_WasteTypes_Get);

  /**
   * Auto generated method signature
   *
   * @param businesses_Get
   */
  public Businesses_GetResponse businesses_Get(
      Businesses_Get businesses_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Attributes_Delete
   */
  public Premises_Attributes_DeleteResponse premises_Attributes_Delete(
      Premises_Attributes_Delete premises_Attributes_Delete);

  /**
   * Auto generated method signature
   *
   * @param features_Statuses_Get
   */
  public Features_Statuses_GetResponse features_Statuses_Get(
      Features_Statuses_Get features_Statuses_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_AttributeDefinitions_Get
   */
  public Premises_AttributeDefinitions_GetResponse
      premises_AttributeDefinitions_Get(
          Premises_AttributeDefinitions_Get premises_AttributeDefinitions_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Get
   */
  public Features_GetResponse features_Get(
      Features_Get features_Get);

  /**
   * Auto generated method signature
   *
   * @param inspections_Documents_Get
   */
  public Inspections_Documents_GetResponse inspections_Documents_Get(
      Inspections_Documents_Get inspections_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param jobs_Documents_Get
   */
  public Jobs_Documents_GetResponse jobs_Documents_Get(
      Jobs_Documents_Get jobs_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Document_Create
   */
  public Premises_Document_CreateResponse premises_Document_Create(
      Premises_Document_Create premises_Document_Create);

  /**
   * Auto generated method signature
   *
   * @param sites_Documents_GetAll
   */
  public Sites_Documents_GetAllResponse sites_Documents_GetAll(
      Sites_Documents_GetAll sites_Documents_GetAll);

  /**
   * Auto generated method signature
   *
   * @param premises_Attributes_Get
   */
  public Premises_Attributes_GetResponse premises_Attributes_Get(
      Premises_Attributes_Get premises_Attributes_Get);

  /**
   * Auto generated method signature
   *
   * @param features_Colours_Get
   */
  public Features_Colours_GetResponse features_Colours_Get(
      Features_Colours_Get features_Colours_Get);

  /**
   * Auto generated method signature
   *
   * @param licences_Documents_Get
   */
  public Licences_Documents_GetResponse licences_Documents_Get(
      Licences_Documents_Get licences_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param businesses_Types_Get
   */
  public Businesses_Types_GetResponse businesses_Types_Get(
      Businesses_Types_Get businesses_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param streets_Events_Types_Get
   */
  public Streets_Events_Types_GetResponse streets_Events_Types_Get(
      Streets_Events_Types_Get streets_Events_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Appointment_Slot_Reserve
   */
  public ServiceRequest_Appointment_Slot_ReserveResponse
      serviceRequest_Appointment_Slot_Reserve(
          ServiceRequest_Appointment_Slot_Reserve
              serviceRequest_Appointment_Slot_Reserve);

  /**
   * Auto generated method signature
   *
   * @param licences_Statuses_Get
   */
  public Licences_Statuses_GetResponse licences_Statuses_Get(
      Licences_Statuses_Get licences_Statuses_Get);

  /**
   * Auto generated method signature
   *
   * @param licences_Types_Get
   */
  public Licences_Types_GetResponse licences_Types_Get(
      Licences_Types_Get licences_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param sites_Get
   */
  public Sites_GetResponse sites_Get(Sites_Get sites_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Get
   */
  public ServiceRequests_GetResponse serviceRequests_Get(
      ServiceRequests_Get serviceRequests_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Attribute_Create
   */
  public Premises_Attribute_CreateResponse premises_Attribute_Create(
      Premises_Attribute_Create premises_Attribute_Create);

  /**
   * Auto generated method signature
   *
   * @param inspections_Statuses_Get
   */
  public Inspections_Statuses_GetResponse inspections_Statuses_Get(
      Inspections_Statuses_Get inspections_Statuses_Get);

  /**
   * Auto generated method signature
   *
   * @param system_LandTypes_Get
   */
  public System_LandTypes_GetResponse system_LandTypes_Get(
      System_LandTypes_Get system_LandTypes_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Cancel
   */
  public ServiceRequest_CancelResponse serviceRequest_Cancel(
      ServiceRequest_Cancel serviceRequest_Cancel);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Notes_Types_Get
   */
  public ServiceRequests_Notes_Types_GetResponse serviceRequests_Notes_Types_Get(
      ServiceRequests_Notes_Types_Get serviceRequests_Notes_Types_Get);

  /**
   * Auto generated method signature
   *
   * @param premises_Event_Create
   */
  public Premises_Event_CreateResponse premises_Event_Create(
      Premises_Event_Create premises_Event_Create);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Documents_Get
   */
  public ServiceRequests_Documents_GetResponse serviceRequests_Documents_Get(
      ServiceRequests_Documents_Get serviceRequests_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param resources_Get
   */
  public Resources_GetResponse resources_Get(
      Resources_Get resources_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Appointment_Reservation_Cancel
   */
  public ServiceRequest_Appointment_Reservation_CancelResponse
      serviceRequest_Appointment_Reservation_Cancel(
          ServiceRequest_Appointment_Reservation_Cancel
              serviceRequest_Appointment_Reservation_Cancel);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Detail_Get
   */
  public ServiceRequests_Detail_GetResponse serviceRequests_Detail_Get(
      ServiceRequests_Detail_Get serviceRequests_Detail_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequests_Classes_Get
   */
  public ServiceRequests_Classes_GetResponse serviceRequests_Classes_Get(
      ServiceRequests_Classes_Get serviceRequests_Classes_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Update
   */
  public ServiceRequest_UpdateResponse serviceRequest_Update(
      ServiceRequest_Update serviceRequest_Update);

  /**
   * Auto generated method signature
   *
   * @param streets_Events_Get
   */
  public Streets_Events_GetResponse streets_Events_Get(
      Streets_Events_Get streets_Events_Get);

  /**
   * Auto generated method signature
   *
   * @param resources_Documents_Get
   */
  public Resources_Documents_GetResponse resources_Documents_Get(
      Resources_Documents_Get resources_Documents_Get);

  /**
   * Auto generated method signature
   *
   * @param serviceRequest_Status_Set
   */
  public ServiceRequest_Status_SetResponse serviceRequest_Status_Set(
      ServiceRequest_Status_Set serviceRequest_Status_Set);
}
