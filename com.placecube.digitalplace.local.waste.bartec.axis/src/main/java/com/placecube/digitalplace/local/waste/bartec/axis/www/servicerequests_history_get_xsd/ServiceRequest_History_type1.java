/**
 * ServiceRequest_History_type1.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_history_get_xsd;

/** ServiceRequest_History_type1 bean class */
@SuppressWarnings({"unchecked", "unused"})
public class ServiceRequest_History_type1 implements org.apache.axis2.databinding.ADBBean {
  /* This type was generated from the piece of schema that had
  name = ServiceRequest_History_type1
  Namespace URI = http://www.bartec-systems.com/ServiceRequests_History_Get.xsd
  Namespace Prefix = ns29
  */

  /** field for Id */
  protected int localId;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getId() {
    return localId;
  }

  /**
   * Auto generated setter method
   *
   * @param param Id
   */
  public void setId(int param) {

    this.localId = param;
  }

  /** field for ServiceCode */
  protected java.lang.String localServiceCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceCodeTracker = false;

  public boolean isServiceCodeSpecified() {
    return localServiceCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceCode() {
    return localServiceCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceCode
   */
  public void setServiceCode(java.lang.String param) {
    localServiceCodeTracker = param != null;

    this.localServiceCode = param;
  }

  /** field for UPRN */
  protected java.math.BigDecimal localUPRN;

  /**
   * Auto generated getter method
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getUPRN() {
    return localUPRN;
  }

  /**
   * Auto generated setter method
   *
   * @param param UPRN
   */
  public void setUPRN(java.math.BigDecimal param) {

    this.localUPRN = param;
  }

  /** field for ServiceLocation */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint localServiceLocation;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceLocationTracker = false;

  public boolean isServiceLocationSpecified() {
    return localServiceLocationTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint getServiceLocation() {
    return localServiceLocation;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceLocation
   */
  public void setServiceLocation(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint param) {
    localServiceLocationTracker = param != null;

    this.localServiceLocation = param;
  }

  /** field for LandType */
  protected java.lang.String localLandType;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localLandTypeTracker = false;

  public boolean isLandTypeSpecified() {
    return localLandTypeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getLandType() {
    return localLandType;
  }

  /**
   * Auto generated setter method
   *
   * @param param LandType
   */
  public void setLandType(java.lang.String param) {
    localLandTypeTracker = param != null;

    this.localLandType = param;
  }

  /** field for DateRequested */
  protected java.util.Calendar localDateRequested;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateRequested() {
    return localDateRequested;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateRequested
   */
  public void setDateRequested(java.util.Calendar param) {

    this.localDateRequested = param;
  }

  /** field for DateChanged */
  protected java.util.Calendar localDateChanged;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateChanged() {
    return localDateChanged;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateChanged
   */
  public void setDateChanged(java.util.Calendar param) {

    this.localDateChanged = param;
  }

  /** field for DateClosed */
  protected java.util.Calendar localDateClosed;

  /**
   * Auto generated getter method
   *
   * @return java.util.Calendar
   */
  public java.util.Calendar getDateClosed() {
    return localDateClosed;
  }

  /**
   * Auto generated setter method
   *
   * @param param DateClosed
   */
  public void setDateClosed(java.util.Calendar param) {

    this.localDateClosed = param;
  }

  /** field for ClosingCode */
  protected java.lang.String localClosingCode;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localClosingCodeTracker = false;

  public boolean isClosingCodeSpecified() {
    return localClosingCodeTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getClosingCode() {
    return localClosingCode;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosingCode
   */
  public void setClosingCode(java.lang.String param) {
    localClosingCodeTracker = param != null;

    this.localClosingCode = param;
  }

  /** field for ClosingComments */
  protected java.lang.String localClosingComments;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localClosingCommentsTracker = false;

  public boolean isClosingCommentsSpecified() {
    return localClosingCommentsTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getClosingComments() {
    return localClosingComments;
  }

  /**
   * Auto generated setter method
   *
   * @param param ClosingComments
   */
  public void setClosingComments(java.lang.String param) {
    localClosingCommentsTracker = param != null;

    this.localClosingComments = param;
  }

  /** field for ServiceClassName */
  protected java.lang.String localServiceClassName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceClassNameTracker = false;

  public boolean isServiceClassNameSpecified() {
    return localServiceClassNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceClassName() {
    return localServiceClassName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceClassName
   */
  public void setServiceClassName(java.lang.String param) {
    localServiceClassNameTracker = param != null;

    this.localServiceClassName = param;
  }

  /** field for ServiceTypeName */
  protected java.lang.String localServiceTypeName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceTypeNameTracker = false;

  public boolean isServiceTypeNameSpecified() {
    return localServiceTypeNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceTypeName() {
    return localServiceTypeName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceTypeName
   */
  public void setServiceTypeName(java.lang.String param) {
    localServiceTypeNameTracker = param != null;

    this.localServiceTypeName = param;
  }

  /** field for ServiceStatusName */
  protected java.lang.String localServiceStatusName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localServiceStatusNameTracker = false;

  public boolean isServiceStatusNameSpecified() {
    return localServiceStatusNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getServiceStatusName() {
    return localServiceStatusName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ServiceStatusName
   */
  public void setServiceStatusName(java.lang.String param) {
    localServiceStatusNameTracker = param != null;

    this.localServiceStatusName = param;
  }

  /** field for ExternalReference */
  protected java.lang.String localExternalReference;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localExternalReferenceTracker = false;

  public boolean isExternalReferenceSpecified() {
    return localExternalReferenceTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getExternalReference() {
    return localExternalReference;
  }

  /**
   * Auto generated setter method
   *
   * @param param ExternalReference
   */
  public void setExternalReference(java.lang.String param) {
    localExternalReferenceTracker = param != null;

    this.localExternalReference = param;
  }

  /** field for ContactName */
  protected java.lang.String localContactName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactNameTracker = false;

  public boolean isContactNameSpecified() {
    return localContactNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactName() {
    return localContactName;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactName
   */
  public void setContactName(java.lang.String param) {
    localContactNameTracker = param != null;

    this.localContactName = param;
  }

  /** field for ContactTelephone */
  protected java.lang.String localContactTelephone;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactTelephoneTracker = false;

  public boolean isContactTelephoneSpecified() {
    return localContactTelephoneTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactTelephone() {
    return localContactTelephone;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactTelephone
   */
  public void setContactTelephone(java.lang.String param) {
    localContactTelephoneTracker = param != null;

    this.localContactTelephone = param;
  }

  /** field for ContactEmail */
  protected java.lang.String localContactEmail;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localContactEmailTracker = false;

  public boolean isContactEmailSpecified() {
    return localContactEmailTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getContactEmail() {
    return localContactEmail;
  }

  /**
   * Auto generated setter method
   *
   * @param param ContactEmail
   */
  public void setContactEmail(java.lang.String param) {
    localContactEmailTracker = param != null;

    this.localContactEmail = param;
  }

  /** field for SLAID */
  protected int localSLAID;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getSLAID() {
    return localSLAID;
  }

  /**
   * Auto generated setter method
   *
   * @param param SLAID
   */
  public void setSLAID(int param) {

    this.localSLAID = param;
  }

  /** field for WorkGroupName */
  protected java.lang.String localWorkGroupName;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localWorkGroupNameTracker = false;

  public boolean isWorkGroupNameSpecified() {
    return localWorkGroupNameTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return java.lang.String
   */
  public java.lang.String getWorkGroupName() {
    return localWorkGroupName;
  }

  /**
   * Auto generated setter method
   *
   * @param param WorkGroupName
   */
  public void setWorkGroupName(java.lang.String param) {
    localWorkGroupNameTracker = param != null;

    this.localWorkGroupName = param;
  }

  /** field for CrewNumber */
  protected int localCrewNumber;

  /**
   * Auto generated getter method
   *
   * @return int
   */
  public int getCrewNumber() {
    return localCrewNumber;
  }

  /**
   * Auto generated setter method
   *
   * @param param CrewNumber
   */
  public void setCrewNumber(int param) {

    this.localCrewNumber = param;
  }

  /** field for RecordStamp */
  protected com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp localRecordStamp;

  /*  This tracker boolean wil be used to detect whether the user called the set method
   *   for this attribute. It will be used to determine whether to include this field
   *   in the serialized XML
   */
  protected boolean localRecordStampTracker = false;

  public boolean isRecordStampSpecified() {
    return localRecordStampTracker;
  }

  /**
   * Auto generated getter method
   *
   * @return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp
   */
  public com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp getRecordStamp() {
    return localRecordStamp;
  }

  /**
   * Auto generated setter method
   *
   * @param param RecordStamp
   */
  public void setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp param) {
    localRecordStampTracker = param != null;

    this.localRecordStamp = param;
  }

  /**
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  public org.apache.axiom.om.OMElement getOMElement(
      final javax.xml.namespace.QName parentQName, final org.apache.axiom.om.OMFactory factory)
      throws org.apache.axis2.databinding.ADBException {

    return factory.createOMElement(
        new org.apache.axis2.databinding.ADBDataSource(this, parentQName));
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }

  public void serialize(
      final javax.xml.namespace.QName parentQName,
      javax.xml.stream.XMLStreamWriter xmlWriter,
      boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {

    java.lang.String prefix = null;
    java.lang.String namespace = null;

    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

    if (serializeType) {

      java.lang.String namespacePrefix =
          registerPrefix(
              xmlWriter, "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            namespacePrefix + ":ServiceRequest_History_type1",
            xmlWriter);
      } else {
        writeAttribute(
            "xsi",
            "http://www.w3.org/2001/XMLSchema-instance",
            "type",
            "ServiceRequest_History_type1",
            xmlWriter);
      }
    }

    namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
    writeStartElement(null, namespace, "id", xmlWriter);

    if (localId == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
    }

    xmlWriter.writeEndElement();
    if (localServiceCodeTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ServiceCode", xmlWriter);

      if (localServiceCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceCode);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
    writeStartElement(null, namespace, "UPRN", xmlWriter);

    if (localUPRN == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("UPRN cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUPRN));
    }

    xmlWriter.writeEndElement();
    if (localServiceLocationTracker) {
      if (localServiceLocation == null) {
        throw new org.apache.axis2.databinding.ADBException("ServiceLocation cannot be null!!");
      }
      localServiceLocation.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "ServiceLocation"),
          xmlWriter);
    }
    if (localLandTypeTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "LandType", xmlWriter);

      if (localLandType == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("LandType cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localLandType);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
    writeStartElement(null, namespace, "DateRequested", xmlWriter);

    if (localDateRequested == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateRequested cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateRequested));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
    writeStartElement(null, namespace, "DateChanged", xmlWriter);

    if (localDateChanged == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateChanged cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateChanged));
    }

    xmlWriter.writeEndElement();

    namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
    writeStartElement(null, namespace, "DateClosed", xmlWriter);

    if (localDateClosed == null) {
      // write the nil attribute

      throw new org.apache.axis2.databinding.ADBException("DateClosed cannot be null!!");

    } else {

      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateClosed));
    }

    xmlWriter.writeEndElement();
    if (localClosingCodeTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ClosingCode", xmlWriter);

      if (localClosingCode == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ClosingCode cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localClosingCode);
      }

      xmlWriter.writeEndElement();
    }
    if (localClosingCommentsTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ClosingComments", xmlWriter);

      if (localClosingComments == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ClosingComments cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localClosingComments);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceClassNameTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ServiceClassName", xmlWriter);

      if (localServiceClassName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceClassName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceClassName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceTypeNameTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ServiceTypeName", xmlWriter);

      if (localServiceTypeName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceTypeName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceTypeName);
      }

      xmlWriter.writeEndElement();
    }
    if (localServiceStatusNameTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ServiceStatusName", xmlWriter);

      if (localServiceStatusName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ServiceStatusName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localServiceStatusName);
      }

      xmlWriter.writeEndElement();
    }
    if (localExternalReferenceTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ExternalReference", xmlWriter);

      if (localExternalReference == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ExternalReference cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localExternalReference);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactNameTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ContactName", xmlWriter);

      if (localContactName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactName);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactTelephoneTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ContactTelephone", xmlWriter);

      if (localContactTelephone == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactTelephone cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactTelephone);
      }

      xmlWriter.writeEndElement();
    }
    if (localContactEmailTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "ContactEmail", xmlWriter);

      if (localContactEmail == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("ContactEmail cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localContactEmail);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
    writeStartElement(null, namespace, "SLAID", xmlWriter);

    if (localSLAID == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("SLAID cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSLAID));
    }

    xmlWriter.writeEndElement();
    if (localWorkGroupNameTracker) {
      namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
      writeStartElement(null, namespace, "WorkGroupName", xmlWriter);

      if (localWorkGroupName == null) {
        // write the nil attribute

        throw new org.apache.axis2.databinding.ADBException("WorkGroupName cannot be null!!");

      } else {

        xmlWriter.writeCharacters(localWorkGroupName);
      }

      xmlWriter.writeEndElement();
    }
    namespace = "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd";
    writeStartElement(null, namespace, "CrewNumber", xmlWriter);

    if (localCrewNumber == java.lang.Integer.MIN_VALUE) {

      throw new org.apache.axis2.databinding.ADBException("CrewNumber cannot be null!!");

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCrewNumber));
    }

    xmlWriter.writeEndElement();
    if (localRecordStampTracker) {
      if (localRecordStamp == null) {
        throw new org.apache.axis2.databinding.ADBException("RecordStamp cannot be null!!");
      }
      localRecordStamp.serialize(
          new javax.xml.namespace.QName(
              "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "RecordStamp"),
          xmlWriter);
    }
    xmlWriter.writeEndElement();
  }

  private static java.lang.String generatePrefix(java.lang.String namespace) {
    if (namespace.equals("http://www.bartec-systems.com/ServiceRequests_History_Get.xsd")) {
      return "ns29";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }

  /** Utility method to write an element start tag. */
  private void writeStartElement(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String localPart,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
    } else {
      if (namespace.length() == 0) {
        prefix = "";
      } else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }

      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }

  /** Util method to write an attribute with the ns prefix */
  private void writeAttribute(
      java.lang.String prefix,
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeAttribute(writerPrefix, namespace, attName, attValue);
    } else {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
      xmlWriter.writeAttribute(prefix, namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      java.lang.String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    } else {
      xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace, attName, attValue);
    }
  }

  /** Util method to write an attribute without the ns prefix */
  private void writeQNameAttribute(
      java.lang.String namespace,
      java.lang.String attName,
      javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    java.lang.String attributeNamespace = qname.getNamespaceURI();
    java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    java.lang.String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    } else {
      attributeValue = qname.getLocalPart();
    }

    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    } else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
    }
  }
  /** method to handle Qnames */
  private void writeQName(
      javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }

      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(
            prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      } else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(
            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }

    } else {
      xmlWriter.writeCharacters(
          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }

  private void writeQNames(
      javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {

    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
      java.lang.String namespaceURI = null;
      java.lang.String prefix = null;

      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }

          if (prefix.trim().length() > 0) {
            stringToWrite
                .append(prefix)
                .append(":")
                .append(
                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          } else {
            stringToWrite.append(
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        } else {
          stringToWrite.append(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
  }

  /** Register a namespace prefix */
  private java.lang.String registerPrefix(
      javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace)
      throws javax.xml.stream.XMLStreamException {
    java.lang.String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        java.lang.String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }

  /** Factory class that keeps the parse method */
  public static class Factory {
    private static org.apache.commons.logging.Log log =
        org.apache.commons.logging.LogFactory.getLog(Factory.class);

    /**
     * static method to create the object Precondition: If this object is an element, the current or
     * next start element starts this object and any intervening reader events are ignorable If this
     * object is not an element, it is a complex type and the reader is at the event just after the
     * outer start element Postcondition: If this object is an element, the reader is positioned at
     * its end element If this object is a complex type, the reader is positioned at the end element
     * of its outer element
     */
    public static ServiceRequest_History_type1 parse(javax.xml.stream.XMLStreamReader reader)
        throws java.lang.Exception {
      ServiceRequest_History_type1 object = new ServiceRequest_History_type1();

      int event;
      javax.xml.namespace.QName currentQName = null;
      java.lang.String nillableValue = null;
      java.lang.String prefix = "";
      java.lang.String namespaceuri = "";
      try {

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        currentQName = reader.getName();

        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          java.lang.String fullTypeName =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            java.lang.String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;

            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

            if (!"ServiceRequest_History_type1".equals(type)) {
              // find namespace for the prefix
              java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (ServiceRequest_History_type1)
                  com.placecube.digitalplace.local.waste.bartec.axis.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }

        // Note all attributes that were handled. Used to differ normal attributes
        // from anyAttributes.
        java.util.Vector handledAttributes = new java.util.Vector();

        reader.next();

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "id")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "id" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setId(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "ServiceCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "UPRN")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "UPRN" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setUPRN(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "ServiceLocation")
                .equals(reader.getName())) {

          object.setServiceLocation(com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "LandType")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "LandType" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setLandType(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "DateRequested")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateRequested" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateRequested(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "DateChanged")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateChanged" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateChanged(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "DateClosed")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "DateClosed" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setDateClosed(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "ClosingCode")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosingCode" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosingCode(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "ClosingComments")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ClosingComments" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setClosingComments(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "ServiceClassName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceClassName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceClassName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "ServiceTypeName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceTypeName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceTypeName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "ServiceStatusName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ServiceStatusName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setServiceStatusName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "ExternalReference")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ExternalReference" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setExternalReference(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "ContactName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "ContactTelephone")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactTelephone" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactTelephone(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "ContactEmail")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "ContactEmail" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setContactEmail(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "SLAID")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "SLAID" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setSLAID(org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd",
                    "WorkGroupName")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "WorkGroupName" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setWorkGroupName(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "CrewNumber")
                .equals(reader.getName())) {

          nillableValue =
              reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if ("true".equals(nillableValue) || "1".equals(nillableValue)) {
            throw new org.apache.axis2.databinding.ADBException(
                "The element: " + "CrewNumber" + "  cannot be null");
          }

          java.lang.String content = reader.getElementText();

          object.setCrewNumber(
              org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));

          reader.next();

        } // End of if for expected property start element
        else {
          // 1 - A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());
        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement()
            && new javax.xml.namespace.QName(
                    "http://www.bartec-systems.com/ServiceRequests_History_Get.xsd", "RecordStamp")
                .equals(reader.getName())) {

          object.setRecordStamp(com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader));

          reader.next();

        } // End of if for expected property start element
        else {

        }

        while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

        if (reader.isStartElement())
          // 2 - A start element we are not expecting indicates a trailing invalid property

          throw new org.apache.axis2.databinding.ADBException(
              "Unexpected subelement " + reader.getName());

      } catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }

      return object;
    }
  } // end of factory class
}
