/**
 * ExtensionMapper.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis2 version: 1.8.2 Built on : Jul 13,
 * 2022 (06:38:18 EDT)
 */
package com.placecube.digitalplace.local.waste.bartec.axis;

/** ExtensionMapper class */
@SuppressWarnings({ "unchecked", "unused" })
public class ExtensionMapper {

	public static java.lang.Object getTypeObject(java.lang.String namespaceURI, java.lang.String typeName, javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {

		if ("http://www.bartec-systems.com/Premises_AttributeDefinitions_Get.xsd".equals(namespaceURI) && "Premises_AttributeDefinitions_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributedefinitions_get_xsd.Premises_AttributeDefinitions_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ServiceRequest_CreateServiceRequest_CreateFields".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateFields.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctVehicleStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Get.xsd".equals(namespaceURI) && "Inspection_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_get_xsd.Inspection_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Get.xsd".equals(namespaceURI) && "Inspection_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_get_xsd.Inspection_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Documents_Get.xsd".equals(namespaceURI) && "Premises_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_documents_get_xsd.Premises_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_AttributeDefinitions_Get.xsd".equals(namespaceURI) && "AttributeDefinition_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributedefinitions_get_xsd.AttributeDefinition_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_AttributeDefinitions_Get.xsd".equals(namespaceURI) && "AttributeDefinition_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributedefinitions_get_xsd.AttributeDefinition_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Crews_Get.xsd".equals(namespaceURI) && "Crews_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.crews_get_xsd.Crews_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctLicenceStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctBusiness".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusiness.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Statuses_Get.xsd".equals(namespaceURI) && "ServiceRequests_Statuses_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_statuses_get_xsd.ServiceRequests_Statuses_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Statuses_Get.xsd".equals(namespaceURI) && "Businesses_Statuses_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_statuses_get_xsd.Businesses_Statuses_GetResult_type0.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldLocation".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldLocation.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attributes_Delete.xsd".equals(namespaceURI) && "Premises_Attributes_DeleteResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributes_delete_xsd.Premises_Attributes_DeleteResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Create.xsd".equals(namespaceURI) && "Features_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_create_xsd.Features_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Create.xsd".equals(namespaceURI) && "Features_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_create_xsd.Features_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Get.xsd".equals(namespaceURI) && "Sites_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_get_xsd.Sites_GetResult_type0.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldRelatedServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldRelatedServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctExtendedDataRecord".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecord.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Detail_Get.xsd".equals(namespaceURI) && "Streets_Detail_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_detail_get_xsd.Streets_Detail_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctResponseType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtResponseType.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfServiceRequest_CreateServiceRequest_CreateLocation".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Appointment_Reservation_Extend.xsd".equals(namespaceURI) && "ServiceRequest_Appointment_Reservation_ExtendResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_appointment_reservation_extend_xsd.ServiceRequest_Appointment_Reservation_ExtendResult_type0.Factory
					.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Document_Create.xsd".equals(namespaceURI) && "ServiceRequest_Document_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_document_create_xsd.ServiceRequest_Document_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "RelatedPremises_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.RelatedPremises_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "RelatedPremises_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.RelatedPremises_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ServiceRequest_Create_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_Create_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Document_Create.xsd".equals(namespaceURI) && "Document_type4".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_document_create_xsd.Document_type4.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ServiceRequest_Create_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_Create_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Vehicle_InspectionResults_Get.xsd".equals(namespaceURI) && "Vehicle_InspectionResults_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.vehicle_inspectionresults_get_xsd.Vehicle_InspectionResults_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Document_Create.xsd".equals(namespaceURI) && "Document_type9".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_document_create_xsd.Document_type9.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctLandType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtLandType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Workpacks_Notes_Get.xsd".equals(namespaceURI) && "Workpacks_Notes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_notes_get_xsd.Workpacks_Notes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctWard".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtWard.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctAttachedDocument".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtAttachedDocument.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Classes_Get.xsd".equals(namespaceURI) && "Businesses_Classes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_classes_get_xsd.Businesses_Classes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Addresses_Get.xsd".equals(namespaceURI) && "Businesses_Addresses_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_addresses_get_xsd.Businesses_Addresses_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get2.xsd".equals(namespaceURI) && "ArrayOfCtServiceNote".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtServiceNote.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Classes_Get.xsd".equals(namespaceURI) && "Features_Classes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_classes_get_xsd.Features_Classes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldFields".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldFields.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Document_Get.xsd".equals(namespaceURI) && "Premises_Events_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_document_get_xsd.Premises_Events_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attributes_Delete.xsd".equals(namespaceURI) && "Attribute_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributes_delete_xsd.Attribute_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd".equals(namespaceURI) && "Actions_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_metrics_get_xsd.Actions_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get2.xsd".equals(namespaceURI) && "AttachedDocuments_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.AttachedDocuments_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd".equals(namespaceURI) && "Actions_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_metrics_get_xsd.Actions_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctExtendedDataRecordType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecordType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "Field_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.Field_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctVehicleInspectionResultItem".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionResultItem.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attributes_Delete.xsd".equals(namespaceURI) && "Attribute_type5".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributes_delete_xsd.Attribute_type5.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Documents_GetAll.xsd".equals(namespaceURI) && "Document_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_documents_getall_xsd.Document_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctDateQuery".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateQuery.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctVehicleInspectionItem".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionItem.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfInt".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfInt.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Resources_Documents_Get.xsd".equals(namespaceURI) && "Resources_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.resources_documents_get_xsd.Resources_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ReporterBusiness_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ReporterBusiness_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Create.xsd".equals(namespaceURI) && "Note_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_create_xsd.Note_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctInspectionStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtInspectionStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ReporterBusiness_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ReporterBusiness_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ReporterBusiness_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterBusiness_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Create.xsd".equals(namespaceURI) && "Note_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_create_xsd.Note_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Detail_Get.xsd".equals(namespaceURI) && "Tasks_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Tasks_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type5".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.AttachedDocuments_type5.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Detail_Get.xsd".equals(namespaceURI) && "Tasks_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Tasks_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "Premises_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.Premises_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "Premises_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.Premises_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctRelatedServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRelatedServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctStreetEventType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtStreetEventType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctExtendedDataRecordDefinition".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataRecordDefinition.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd".equals(namespaceURI) && "Jobs_WorkScheduleDates_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_workscheduledates_get_xsd.Jobs_WorkScheduleDates_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctSubEventType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtSubEventType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Vehicles_Get.xsd".equals(namespaceURI) && "Vehicles_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.vehicles_get_xsd.Vehicles_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd".equals(namespaceURI) && "Jobs_WorkScheduleDates_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_workscheduledates_get_xsd.Jobs_WorkScheduleDates_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Conditions_Get.xsd".equals(namespaceURI) && "Features_Conditions_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_conditions_get_xsd.Features_Conditions_GetResult_type0.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "Fields_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.Fields_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Detail_Get.xsd".equals(namespaceURI) && "Jobs_Detail_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Jobs_Detail_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctBLPULogicalStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBLPULogicalStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Addresses_Get.xsd".equals(namespaceURI) && "BusinessAddresses_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_addresses_get_xsd.BusinessAddresses_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Addresses_Get.xsd".equals(namespaceURI) && "BusinessAddresses_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_addresses_get_xsd.BusinessAddresses_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctWorkPack".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkPack.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_FutureWorkpacks_Get.xsd".equals(namespaceURI) && "Premises_FutureWorkpacks_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_futureworkpacks_get_xsd.Premises_FutureWorkpacks_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctAddress".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtAddress.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Get.xsd".equals(namespaceURI) && "Sites_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_get_xsd.Sites_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctLicence".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicence.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_History_Get.xsd".equals(namespaceURI) && "ServiceRequest_History_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_history_get_xsd.ServiceRequest_History_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Get.xsd".equals(namespaceURI) && "Sites_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_get_xsd.Sites_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_History_Get.xsd".equals(namespaceURI) && "ServiceRequest_History_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_history_get_xsd.ServiceRequest_History_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctResource".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtResource.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get2.xsd".equals(namespaceURI) && "ServiceRequest_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ServiceRequest_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get2.xsd".equals(namespaceURI) && "ServiceRequest_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ServiceRequest_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_FutureWorkpacks_Get.xsd".equals(namespaceURI) && "Premises_FutureWorkPack_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_futureworkpacks_get_xsd.Premises_FutureWorkPack_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_FutureWorkpacks_Get.xsd".equals(namespaceURI) && "Premises_FutureWorkPack_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_futureworkpacks_get_xsd.Premises_FutureWorkPack_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Create.xsd".equals(namespaceURI) && "Premises_Event_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_create_xsd.Premises_Event_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Create.xsd".equals(namespaceURI) && "Attribute_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_create_xsd.Attribute_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctVehicle".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicle.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Cases_Documents_Get.xsd".equals(namespaceURI) && "Cases_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.cases_documents_get_xsd.Cases_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Create.xsd".equals(namespaceURI) && "Attribute_type9".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_create_xsd.Attribute_type9.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Detail_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type7".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.AttachedDocuments_type7.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Documents_Get.xsd".equals(namespaceURI) && "Sites_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_documents_get_xsd.Sites_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ArrayOfServiceRequest_CreateServiceRequest_CreateLocation".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ArrayOfServiceRequest_CreateServiceRequest_CreateLocation.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Types_Get.xsd".equals(namespaceURI) && "EventClasses_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventClasses_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Types_Get.xsd".equals(namespaceURI) && "EventClasses_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventClasses_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequest_UpdateServiceRequest_UpdateFieldLocation".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequest_UpdateServiceRequest_UpdateFieldLocation.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "LicenceApplication_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.LicenceApplication_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Notes_Get.xsd".equals(namespaceURI) && "Features_Notes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_notes_get_xsd.Features_Notes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Manufacturers_Get.xsd".equals(namespaceURI) && "Features_Manufacturers_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_manufacturers_get_xsd.Features_Manufacturers_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Documents_Get.xsd".equals(namespaceURI) && "ServiceRequests_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_documents_get_xsd.ServiceRequests_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfString".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfString.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureColour".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureColour.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctDeviceType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDeviceType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctAppointmentSlot".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtAppointmentSlot.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Events_Get.xsd".equals(namespaceURI) && "Streets_Events_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_events_get_xsd.Streets_Events_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "AttachedDocuments_type6".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.AttachedDocuments_type6.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctValueLimits".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtValueLimits.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "BNG_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.BNG_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_History_Get.xsd".equals(namespaceURI) && "ServiceRequests_History_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_history_get_xsd.ServiceRequests_History_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Statuses_Get.xsd".equals(namespaceURI) && "Inspections_Statuses_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_statuses_get_xsd.Inspections_Statuses_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctWorkGroup".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkGroup.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd".equals(namespaceURI) && "Workpacks_Metrics_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_metrics_get_xsd.Workpacks_Metrics_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctBLPUClassification".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBLPUClassification.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd".equals(namespaceURI) && "ServiceRequests_Updates_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_updates_get_xsd.ServiceRequests_Updates_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Resources_Get.xsd".equals(namespaceURI) && "Resource_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.resources_get_xsd.Resource_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Close.xsd".equals(namespaceURI) && "Jobs_CloseResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_close_xsd.Jobs_CloseResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Resources_Get.xsd".equals(namespaceURI) && "Resource_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.resources_get_xsd.Resource_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "Location_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.Location_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Document_Create.xsd".equals(namespaceURI) && "Premises_Events_Document_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_document_create_xsd.Premises_Events_Document_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Get.xsd".equals(namespaceURI) && "Licences_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_get_xsd.Licences_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ServiceRequest_CreateServiceRequest_CreateLocation".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateLocation.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctServiceNoteType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceNoteType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequest_UpdateServiceRequest_UpdateFieldReporterBusiness".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequest_UpdateServiceRequest_UpdateFieldReporterBusiness.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Types_Get.xsd".equals(namespaceURI) && "Resources_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_types_get_xsd.Resources_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Cancellations_Get.xsd".equals(namespaceURI) && "Street_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_cancellations_get_xsd.Street_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Get.xsd".equals(namespaceURI) && "Inspections_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_get_xsd.Inspections_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Colours_Get.xsd".equals(namespaceURI) && "Features_Colours_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_colours_get_xsd.Features_Colours_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureManufacturer".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureManufacturer.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Cancellations_Get.xsd".equals(namespaceURI) && "Street_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_cancellations_get_xsd.Street_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctIndividual".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtIndividual.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctEventType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "RelatedServiceRequest_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.RelatedServiceRequest_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ArrayOfCtVehicleInspectionResultItem".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtVehicleInspectionResultItem.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Classes_Get.xsd".equals(namespaceURI) && "Inspections_Classes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_classes_get_xsd.Inspections_Classes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ArrayOfCtVehicleInspectionItem".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtVehicleInspectionItem.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Document_Create.xsd".equals(namespaceURI) && "Premises_Document_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_document_create_xsd.Premises_Document_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attributes_Get.xsd".equals(namespaceURI) && "Premises_Attributes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributes_get_xsd.Premises_Attributes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Events_Types_Get.xsd".equals(namespaceURI) && "Streets_Events_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_events_types_get_xsd.Streets_Events_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctJobStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtJobStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureCategory".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureCategory.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Workpack_Note_Create.xsd".equals(namespaceURI) && "Note_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpack_note_create_xsd.Note_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Workpack_Note_Create.xsd".equals(namespaceURI) && "Note_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpack_note_create_xsd.Note_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Get.xsd".equals(namespaceURI) && "Streets_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_get_xsd.Streets_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctLicenceType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Documents_Get.xsd".equals(namespaceURI) && "Businesses_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_documents_get_xsd.Businesses_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Update.xsd".equals(namespaceURI) && "Attribute_type8".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_update_xsd.Attribute_type8.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequests_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequests_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctAttribute".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtAttribute.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Update.xsd".equals(namespaceURI) && "Attribute_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_update_xsd.Attribute_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctTaskStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtTaskStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Attributes_Get.xsd".equals(namespaceURI) && "Streets_Attributes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_attributes_get_xsd.Streets_Attributes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get2.xsd".equals(namespaceURI) && "AttachedDocuments_type14".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.AttachedDocuments_type14.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureNote".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureNote.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type10".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.AttachedDocuments_type10.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Vehicle_InspectionForms_Get.xsd".equals(namespaceURI) && "Vehicle_InspectionForms_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.vehicle_inspectionforms_get_xsd.Vehicle_InspectionForms_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctMapPoint".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Get.xsd".equals(namespaceURI) && "Business_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_get_xsd.Business_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_FutureWorkpacks_Get.xsd".equals(namespaceURI) && "Action_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_futureworkpacks_get_xsd.Action_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Get.xsd".equals(namespaceURI) && "Business_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_get_xsd.Business_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctJob".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtJob.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctSite".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtSite.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Get.xsd".equals(namespaceURI) && "Premises_Events_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_get_xsd.Premises_Events_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Classes_Get.xsd".equals(namespaceURI) && "EventClasses_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_classes_get_xsd.EventClasses_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Classes_Get.xsd".equals(namespaceURI) && "EventClasses_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_classes_get_xsd.EventClasses_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Get.xsd".equals(namespaceURI) && "Jobs_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_get_xsd.Jobs_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctInspectionClass".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtInspectionClass.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Site_Document_Create.xsd".equals(namespaceURI) && "Document_type8".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.site_document_create_xsd.Document_type8.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctDevice".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDevice.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeature".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeature.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctTaskReason".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtTaskReason.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Site_Document_Create.xsd".equals(namespaceURI) && "Document_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.site_document_create_xsd.Document_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Schedules_Get.xsd".equals(namespaceURI) && "Features_Schedules_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_schedules_get_xsd.Features_Schedules_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Document_Create.xsd".equals(namespaceURI) && "Document_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_document_create_xsd.Document_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/WorkGroups_Get.xsd".equals(namespaceURI) && "WorkGroups_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workgroups_get_xsd.WorkGroups_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Create.xsd".equals(namespaceURI) && "ServiceRequest_Note_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_create_xsd.ServiceRequest_Note_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/WorkPacks_Get.xsd".equals(namespaceURI) && "WorkPacks_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_get_xsd.WorkPacks_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Status_Set.xsd".equals(namespaceURI) && "ServiceRequest_Status_SetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_status_set_xsd.ServiceRequest_Status_SetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ReporterContact_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ReporterContact_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Document_Create.xsd".equals(namespaceURI) && "Document_type6".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_document_create_xsd.Document_type6.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ReporterContact_type4".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ReporterContact_type4.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Document_Create.xsd".equals(namespaceURI) && "Business_Document_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_document_create_xsd.Business_Document_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Get.xsd".equals(namespaceURI) && "Features_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_get_xsd.Features_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctBusinessStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusinessStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctResourceType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtResourceType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctWorkpackNote".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkpackNote.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Document_Create.xsd".equals(namespaceURI) && "Document_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_document_create_xsd.Document_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Update.xsd".equals(namespaceURI) && "Premises_Attribute_UpdateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_update_xsd.Premises_Attribute_UpdateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Events_Types_Get.xsd".equals(namespaceURI) && "ArrayOfCtStreetEventType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_events_types_get_xsd.ArrayOfCtStreetEventType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctWasteType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtWasteType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Appointment_Reservation_Cancel.xsd".equals(namespaceURI) && "ServiceRequest_Appointment_Reservation_CancelResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_appointment_reservation_cancel_xsd.ServiceRequest_Appointment_Reservation_CancelResult_type0.Factory
					.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "Values_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.Values_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Detail_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type11".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.AttachedDocuments_type11.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Appointments_Slot_Create.xsd".equals(namespaceURI) && "ServiceRequest_Appointment_Slot_ReserveResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_appointments_slot_create_xsd.ServiceRequest_Appointment_Slot_ReserveResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Statuses_Get.xsd".equals(namespaceURI) && "Licences_Statuses_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_statuses_get_xsd.Licences_Statuses_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctInspection".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtInspection.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Attributes_Get.xsd".equals(namespaceURI) && "Attribute_type4".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_attributes_get_xsd.Attribute_type4.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctExtendedDataValueList".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataValueList.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Types_Get.xsd".equals(namespaceURI) && "Businesses_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_types_get_xsd.Businesses_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Get.xsd".equals(namespaceURI) && "Premises_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_get_xsd.Premises_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Types_Get.xsd".equals(namespaceURI) && "Licences_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_types_get_xsd.Licences_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Attributes_Get.xsd".equals(namespaceURI) && "Attribute_type6".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_attributes_get_xsd.Attribute_type6.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Vehicle_Message_Create.xsd".equals(namespaceURI) && "Vehicle_Message_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.vehicle_message_create_xsd.Vehicle_Message_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Get.xsd".equals(namespaceURI) && "Job_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_get_xsd.Job_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Classes_Get.xsd".equals(namespaceURI) && "ServiceRequests_Classes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_classes_get_xsd.ServiceRequests_Classes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Get.xsd".equals(namespaceURI) && "Job_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_get_xsd.Job_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Notes_Get.xsd".equals(namespaceURI) && "ServiceRequests_Notes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_notes_get_xsd.ServiceRequests_Notes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/System_LandTypes_Get.xsd".equals(namespaceURI) && "System_LandTypes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.system_landtypes_get_xsd.System_LandTypes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Notes_Types_Get.xsd".equals(namespaceURI) && "ServiceRequests_Notes_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_notes_types_get_xsd.ServiceRequests_Notes_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Site_Document_Create.xsd".equals(namespaceURI) && "Site_Document_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.site_document_create_xsd.Site_Document_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd".equals(namespaceURI) && "WorkPack_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_metrics_get_xsd.WorkPack_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/WorkPacks_Metrics_Get.xsd".equals(namespaceURI) && "WorkPack_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_metrics_get_xsd.WorkPack_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Workpacks_Notes_Types_Get.xsd".equals(namespaceURI) && "Workpacks_Notes_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpacks_notes_types_get_xsd.Workpacks_Notes_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequest_UpdateServiceRequest_UpdateFieldRelatedServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequest_UpdateServiceRequest_UpdateFieldRelatedServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd".equals(namespaceURI) && "ServiceRequest_Updates_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_updates_get_xsd.ServiceRequest_Updates_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Updates_Get.xsd".equals(namespaceURI) && "ServiceRequest_Updates_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_updates_get_xsd.ServiceRequest_Updates_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Detail_Get.xsd".equals(namespaceURI) && "Job_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Job_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Detail_Get.xsd".equals(namespaceURI) && "Job_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_detail_get_xsd.Job_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ArrayOfCtExtendedDataRecord".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtExtendedDataRecord.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "RelatedServiceRequest_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.RelatedServiceRequest_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ServiceRequest_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctBusinessSubStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusinessSubStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctMapBox".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapBox.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Documents_Get.xsd".equals(namespaceURI) && "Inspections_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_documents_get_xsd.Inspections_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_FutureWorkpacks_Get.xsd".equals(namespaceURI) && "ArrayOfPremises_FutureWorkpacks_GetPremises_FutureWorkPackAction".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_futureworkpacks_get_xsd.ArrayOfPremises_FutureWorkpacks_GetPremises_FutureWorkPackAction.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequest_UpdateServiceRequest_UpdateFieldFields".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequest_UpdateServiceRequest_UpdateFieldFields.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctBusinessType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusinessType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctServiceType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctDateLimits".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDateLimits.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "ChildUPRNs_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.ChildUPRNs_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Document_Create.xsd".equals(namespaceURI) && "Document_type7".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_document_create_xsd.Document_type7.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Schedules_Get.xsd".equals(namespaceURI) && "Features_Schedules_Get2Result_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_schedules_get_xsd.Features_Schedules_Get2Result_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "Location_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.Location_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "ChildUPRNs_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.ChildUPRNs_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Document_Create.xsd".equals(namespaceURI) && "Document_type5".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_document_create_xsd.Document_type5.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Documents_GetAll.xsd".equals(namespaceURI) && "Document_type11".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_documents_getall_xsd.Document_type11.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Get.xsd".equals(namespaceURI) && "Businesses_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_get_xsd.Businesses_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get2.xsd".equals(namespaceURI) && "ArrayOfCtRelatedLocation".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedLocation.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type13".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_get_xsd.AttachedDocuments_type13.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Address_Update.xsd".equals(namespaceURI) && "BusinessAddress_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_address_update_xsd.BusinessAddress_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Get.xsd".equals(namespaceURI) && "Streets_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_get_xsd.Streets_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctServiceLevelAgreement".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceLevelAgreement.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Address_Update.xsd".equals(namespaceURI) && "BusinessAddress_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_address_update_xsd.BusinessAddress_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctEventNote".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventNote.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_get_xsd.AttachedDocuments_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_SLAs_Get.xsd".equals(namespaceURI) && "ServiceRequests_SLAs_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_slas_get_xsd.ServiceRequests_SLAs_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Types_Get.xsd".equals(namespaceURI) && "SubEventTypes_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.SubEventTypes_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Get.xsd".equals(namespaceURI) && "Streets_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_get_xsd.Streets_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Types_Get.xsd".equals(namespaceURI) && "SubEventTypes_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.SubEventTypes_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Cancellations_Get.xsd".equals(namespaceURI) && "StreetCancellation_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_cancellations_get_xsd.StreetCancellation_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Cancellations_Get.xsd".equals(namespaceURI) && "StreetCancellation_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_cancellations_get_xsd.StreetCancellation_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Types_Get.xsd".equals(namespaceURI) && "EventTypes_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventTypes_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Documents_GetAll.xsd".equals(namespaceURI) && "Sites_Documents_GetAllResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_documents_getall_xsd.Sites_Documents_GetAllResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Types_Get.xsd".equals(namespaceURI) && "EventTypes_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.EventTypes_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get2.xsd".equals(namespaceURI) && "ArrayOfCtRelatedServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ArrayOfCtRelatedServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_get_xsd.AttachedDocuments_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctBusinessAddress".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusinessAddress.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctMapPointQuery".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPointQuery.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctServiceClass".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceClass.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/System_ExtendedDataDefinitions_Get.xsd".equals(namespaceURI) && "System_ExtendedDataDefinitions_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.system_extendeddatadefinitions_get_xsd.System_ExtendedDataDefinitions_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctEventClass".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventClass.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequest_UpdateServiceRequest_UpdateFieldReporterContact".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequest_UpdateServiceRequest_UpdateFieldReporterContact.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureCondition".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureCondition.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ReporterContact_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.ReporterContact_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "Premises_Detail_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.Premises_Detail_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Statuses_Get.xsd".equals(namespaceURI) && "Features_Statuses_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_statuses_get_xsd.Features_Statuses_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ReporterContact_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ReporterContact_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_WorkScheduleDates_Get.xsd".equals(namespaceURI) && "Jobs_WorkScheduleDates_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_workscheduledates_get_xsd.Jobs_WorkScheduleDates_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ReporterContact_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ReporterContact_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctBusinessClass".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtBusinessClass.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attributes_Get.xsd".equals(namespaceURI) && "Attribute_type7".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributes_get_xsd.Attribute_type7.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureClass".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureClass.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_get_xsd.AttachedDocuments_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attributes_Get.xsd".equals(namespaceURI) && "Attribute_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attributes_get_xsd.Attribute_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctServiceNote".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceNote.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Cancellations_Get.xsd".equals(namespaceURI) && "Streets_Cancellations_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_cancellations_get_xsd.Streets_Cancellations_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctRecordStamp".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRecordStamp.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Attribute_Create.xsd".equals(namespaceURI) && "Premises_Attribute_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_attribute_create_xsd.Premises_Attribute_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ReporterBusiness_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ReporterBusiness_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctWorkpackNoteType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtWorkpackNoteType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctExtendedDataFieldDefinition".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtExtendedDataFieldDefinition.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ReporterBusiness_type4".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ReporterBusiness_type4.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/System_WasteTypes_Get.xsd".equals(namespaceURI) && "System_WasteTypes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.system_wastetypes_get_xsd.System_WasteTypes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Cancel.xsd".equals(namespaceURI) && "ServiceRequest_CancelResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_cancel_xsd.ServiceRequest_CancelResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctLicenceClass".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtLicenceClass.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Types_Get.xsd".equals(namespaceURI) && "Premises_Events_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_types_get_xsd.Premises_Events_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Types_Get.xsd".equals(namespaceURI) && "Inspections_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_types_get_xsd.Inspections_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ServiceRequest_CreateServiceRequest_CreateReporterContact".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterContact.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Types_Get.xsd".equals(namespaceURI) && "Features_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_types_get_xsd.Features_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ArrayOfServiceRequest_CreateServiceRequest_CreateFields".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ArrayOfServiceRequest_CreateServiceRequest_CreateFields.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Resources_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type9".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.resources_get_xsd.AttachedDocuments_type9.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Documents_Get.xsd".equals(namespaceURI) && "Licences_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_documents_get_xsd.Licences_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Resources_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type8".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.resources_get_xsd.AttachedDocuments_type8.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "Metric_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.Metric_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ServiceRequest_CreateServiceRequest_CreateReporterBusiness".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateReporterBusiness.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctVehicleInspectionResult".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionResult.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_SubStatuses_Get.xsd".equals(namespaceURI) && "Businesses_SubStatuses_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_substatuses_get_xsd.Businesses_SubStatuses_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Addresses_Get.xsd".equals(namespaceURI) && "Business_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_addresses_get_xsd.Business_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctRelatedLocation".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtRelatedLocation.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get2.xsd".equals(namespaceURI) && "ServiceRequests_Detail_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get2_xsd.ServiceRequests_Detail_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctPointMetric".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtPointMetric.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfDecimal".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfDecimal.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctTask".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtTask.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Features_Categories_Get.xsd".equals(namespaceURI) && "Features_Categories_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.features_categories_get_xsd.Features_Categories_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctError".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtError.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Addresses_Get.xsd".equals(namespaceURI) && "Business_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_addresses_get_xsd.Business_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "ChildUPRN_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.ChildUPRN_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Sites_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type4".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.sites_get_xsd.AttachedDocuments_type4.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "ChildUPRN_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.ChildUPRN_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Appointments_Availability_Get.xsd".equals(namespaceURI) && "ServiceRequests_Appointments_Availability_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_appointments_availability_get_xsd.ServiceRequests_Appointments_Availability_GetResult_type0.Factory
					.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_FeatureScheduleDates_Get.xsd".equals(namespaceURI) && "Jobs_FeatureScheduleDates_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_featurescheduledates_get_xsd.Jobs_FeatureScheduleDates_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_FeatureScheduleDates_Get.xsd".equals(namespaceURI) && "Jobs_FeatureScheduleDates_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_featurescheduledates_get_xsd.Jobs_FeatureScheduleDates_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctServiceStatus".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceStatus.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Events_Get.xsd".equals(namespaceURI) && "Event_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_events_get_xsd.Event_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Events_Get.xsd".equals(namespaceURI) && "Event_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_events_get_xsd.Event_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctPremises".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtPremises.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Resources_Get.xsd".equals(namespaceURI) && "Resources_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.resources_get_xsd.Resources_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctMapPoint_Set".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtMapPoint_Set.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "RelatedPremise_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.RelatedPremise_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Detail_Get.xsd".equals(namespaceURI) && "RelatedPremise_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_detail_get_xsd.RelatedPremise_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Inspections_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type16".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.inspections_get_xsd.AttachedDocuments_type16.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Workpack_Note_Create.xsd".equals(namespaceURI) && "Workpack_Note_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.workpack_note_create_xsd.Workpack_Note_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Addresses_Get.xsd".equals(namespaceURI) && "Sites_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_addresses_get_xsd.Sites_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureSchedule".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureSchedule.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "ServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.ServiceRequest_CreateServiceRequest_CreateRelatedServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Addresses_Get.xsd".equals(namespaceURI) && "Sites_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_addresses_get_xsd.Sites_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldRelatedServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldRelatedServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldLocation".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldLocation.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequest_UpdateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequest_UpdateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctServiceRequest".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtServiceRequest.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Get.xsd".equals(namespaceURI) && "Jobs_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_get_xsd.Jobs_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctCrew".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtCrew.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Get.xsd".equals(namespaceURI) && "Jobs_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_get_xsd.Jobs_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctDocument".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtDocument.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Businesses_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type15".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.businesses_get_xsd.AttachedDocuments_type15.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "SLA_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.SLA_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctPointBNG".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtPointBNG.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Events_Classes_Get.xsd".equals(namespaceURI) && "Premises_Events_Classes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_events_classes_get_xsd.Premises_Events_Classes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequest_Create.xsd".equals(namespaceURI) && "Fields_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequest_create_xsd.Fields_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctVehicleInspectionForm".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleInspectionForm.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctFeatureNoteType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtFeatureNoteType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Detail_Get.xsd".equals(namespaceURI) && "Street_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_detail_get_xsd.Street_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequest_UpdateField_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequest_UpdateField_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Cancellations_Get.xsd".equals(namespaceURI) && "Action_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_cancellations_get_xsd.Action_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Types_Get.xsd".equals(namespaceURI) && "ServiceRequests_Types_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_types_get_xsd.ServiceRequests_Types_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ServiceRequest_UpdateField_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ServiceRequest_UpdateField_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Cancellations_Get.xsd".equals(namespaceURI) && "Action_type2".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_cancellations_get_xsd.Action_type2.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Address_Update.xsd".equals(namespaceURI) && "Business_Address_UpdateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_address_update_xsd.Business_Address_UpdateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctVehicleType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtVehicleType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctParish".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtParish.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Streets_Detail_Get.xsd".equals(namespaceURI) && "Street_type3".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.streets_detail_get_xsd.Street_type3.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Classes_Get.xsd".equals(namespaceURI) && "Licences_Classes_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_classes_get_xsd.Licences_Classes_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctEventNoteType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEventNoteType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_FeatureScheduleDates_Get.xsd".equals(namespaceURI) && "Jobs_FeatureScheduleDates_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_featurescheduledates_get_xsd.Jobs_FeatureScheduleDates_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Get.xsd".equals(namespaceURI) && "Licence_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_get_xsd.Licence_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Get.xsd".equals(namespaceURI) && "Licence_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_get_xsd.Licence_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctInvoicingBand".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtInvoicingBand.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Create.xsd".equals(namespaceURI) && "SubEvents_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_create_xsd.SubEvents_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Premises_Event_Create.xsd".equals(namespaceURI) && "SubEvents_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.premises_event_create_xsd.SubEvents_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctEvent".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtEvent.Factory.parse(reader);
		}

		if ("http://bartec-systems.com/".equals(namespaceURI) && "ArrayOfServiceRequest_CreateServiceRequest_CreateFields".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.ArrayOfServiceRequest_CreateServiceRequest_CreateFields.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Business_Document_Create.xsd".equals(namespaceURI) && "Document_type10".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.business_document_create_xsd.Document_type10.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Appointments_Availability_Get.xsd".equals(namespaceURI) && "AppointmentSlot_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_appointments_availability_get_xsd.AppointmentSlot_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Appointments_Availability_Get.xsd".equals(namespaceURI) && "AppointmentSlot_type1".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_appointments_availability_get_xsd.AppointmentSlot_type1.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctInspectionType".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtInspectionType.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Licences_Get.xsd".equals(namespaceURI) && "AttachedDocuments_type12".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.licences_get_xsd.AttachedDocuments_type12.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/Jobs_Documents_Get.xsd".equals(namespaceURI) && "Jobs_Documents_GetResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.jobs_documents_get_xsd.Jobs_Documents_GetResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ArrayOfCtExtendedDataFieldDefinition".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.ArrayOfCtExtendedDataFieldDefinition.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Appointments_Slot_Create.xsd".equals(namespaceURI) && "ServiceRequest_Appointment_Reservation_CreateResult_type0".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_appointments_slot_create_xsd.ServiceRequest_Appointment_Reservation_CreateResult_type0.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com".equals(namespaceURI) && "ctStreetAttributeDefinition".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.CtStreetAttributeDefinition.Factory.parse(reader);
		}

		if ("http://www.bartec-systems.com/ServiceRequests_Get.xsd".equals(namespaceURI) && "ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldFields".equals(typeName)) {

			return com.placecube.digitalplace.local.waste.bartec.axis.www.servicerequests_get_xsd.ArrayOfServiceRequest_UpdateServiceRequest_UpdateFieldFields.Factory.parse(reader);
		}

		throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
	}
}
